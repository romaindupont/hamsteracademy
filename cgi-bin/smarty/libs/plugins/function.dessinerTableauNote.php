<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty function plugin
 *
 * Type:     function<br>
 * Name:     dessinerTableauNote<br>
 */
function smarty_function_dessinerTableauNote($params, &$smarty) {
    
    $note = $params['note'];
    $noteMax = $params['noteMax'];
    $hauteurCase=5;
    $largeurCase=10;
    
    $couleur = "bgcolor=\"black\"";
    if ($note < $noteMax/3) {
        $couleur = "bgcolor=\"red\"";
    }
    else if ($note < 2*$noteMax/3) {
        $couleur = "class=\"barresNiveauxOrange\" " ;
    }
    else
        $couleur = "class=\"barresNiveauxVert\" " ;
        
    $txt = "<table width=\"".($largeurCase*10)."\" border=\"1\" bordercolor=\"#bbbbbb\" cellpadding=\"0\" cellspacing=\"0\"><tr><td>" ;
    $txt .= "<table width=\"".($largeurCase*10)."\" border=\"0\" bordercolor=\"#bbbbbb\" cellpadding=\"0\" cellspacing=\"0\"><tr>" ;
    for($i=0;$i<$noteMax;$i++ ){
        
        if ($i < $note ) 
            $txt .= "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" ".$couleur."></td>";
        else
            $txt .= "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" bgcolor=\"white\"></td>";
            
    }
    $txt .= "</tr></table>";
    $txt .= "</td></tr></table>";
    
    return $txt;
}



?>
