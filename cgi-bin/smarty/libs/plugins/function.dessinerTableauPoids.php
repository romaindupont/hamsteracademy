<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty function plugin
 *
 * Type:     function<br>
 * Name:     dessinerTableauPoids<br>
 */
function smarty_function_dessinerTableauPoids($params, &$smarty) {
    
    $poidsMoyen = 120;
    $poids = $params['poids'];
    $largeurCase = 10;
    $hauteurCase = 5;
    
    $couleur = "bgcolor=\"black\"";
    if ( abs($poids - $poidsMoyen) > 30) {
        $couleur = "red";
    }
    else if (abs($poids - $poidsMoyen) > 15) {
        $couleur = "class=\"barresNiveauxOrange\" " ;
    }
    else
        $couleur = "class=\"barresNiveauxVert\" " ;
    
    $txt = "<table width=\"".(10*$largeurCase)."\" style=\"border:1px solid #bbbbbb; border-spacing:0px; border-collapse: collapse;\"><tr><td>" ;
    $txt .= "<table width=\"".(10*$largeurCase)."\" style=\"border:0px; border-spacing:0px; border-collapse: collapse;\"><tr>" ;
    for($i=0;$i<10;$i++ ){
        
        if ($i < (($poids-79)/8) )
            $txt .= "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" ".$couleur."></td>";
        else
            $txt .= "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" bgcolor=\"white\"></td>";
            
    }
    $txt .= "</tr></table>";
    $txt .= "</td></tr></table>";
    
    return $txt;
}



?>
