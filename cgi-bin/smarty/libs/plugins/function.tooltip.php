<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */


/**
 * Smarty function plugin
 *
 * Type:     function<br>
 * Name:     tooltip<br>
 */

function smarty_function_tooltip($params, &$smarty) {
    
     $retval =' onmouseover="return tooltip.show(this);" onmouseout="tooltip.hide(this);" ';

    if (!empty($params['text'])) {
      $retval .= ' title="'.htmlspecialchars($params['text']).'"';
    }
    return $retval;
}



?>
