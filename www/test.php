<?php 

define('IN_HT', true);
error_reporting( E_ALL );

echo "debut du test : <br/>";

include "common.php";
include "gestion.php";
include "gestionUser.php";
include "lstAccessoires.php";

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo "Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

error_reporting( E_ALL );

$depuis24H = $dateActuelle-3600*24;

$allOk = true;

// test l'ajout d'un joueur
echo "1) Ajout d'un joueur<br/>";
$joueur_id = ajouterJoueurDansBDD("testPseudo244", '11213', "test@fre.fr", 0, 12, 0, 83, 0) ;

if ($joueur_id == -1) {
    echo "erreur inscription joueur " ;
    $allOk = false;
}
else {            
    // on rajoute la cage et quelques objets
    echo "2) Ajout d'une cage<br/>";
    $nomCage = "Cage de test";
    $cage_id = ajouterCageDansBDD($joueur_id, $typeCageInscription, $etagesInscription, $fondInscription, $propreteInscription, $toitInscription, $colonnesInscription, $profondeursInscription, $nomCage);

    if ($cage_id == -1) {
        echo "Erreur ajout de cage <br>" ;
        $allOk = false;
    }
    else {
                
        // on ajoute un objet (biberon)
        echo "3) Ajout de 2 accessoires<br/>";
        $acc_id = ajouterAccessoireDansBDD(1,$joueur_id, $cage_id,230,0,0,1);
        $acc_id2 = ajouterAccessoireDansBDD(ACC_ECUELLE,$joueur_id, $cage_id,157,0,0,1);
        
        $userdata = array("joueur_id" => $joueur_id);
        updateListeAccessoires();
        
        // on ajoute l'hamster
        echo "4) Ajout d'un hamster<br/>";
        $hamster_id = ajouterHamsterDansBDD("testPrenomHamster", 0, 0, 1, 0, $cage_id, $joueur_id, 604800); // 1 semaine d'age

        if ($hamster_id == -1) {
            echo "Erreur ajout d'hamster<br>" ;
            $allOk = false;
        }
        else {                        
                    
        }
    }
}        

// on supprime les accessoires
echo "5) Supprime 2 accessoires : $acc_id et $acc_id2<br/>";
reduireQuantiteAccessoire($acc_id,1);
reduireQuantiteAccessoire($acc_id2,1);

// on vérifie qu'ils ont bien disparu
$query = "SELECT COUNT(accessoire_id) FROM accessoires WHERE accessoire_id IN (".$acc_id.",".$acc_id2.")";
$result = $dbHT->sql_query($query);
$row = $dbHT->sql_fetchrow($result);
$nbAcc = $row[0];
$dbHT->sql_freeresult($result);

if ($nbAcc > 0){
    echo "suppression des 2 accessoires incompletes ! nbAcc = $nbAcc, query = $query <br/>";
    $allOk = false;
}

// on supprime la cage
echo "6) Supprime la cage<br/>";
$query = "DELETE FROM cage WHERE cage_id = $cage_id LIMIT 1";
$result = $dbHT->sql_query($query);
$row = $dbHT->sql_fetchrow($result);
$nbAcc = $row[0];
$dbHT->sql_freeresult($result);

// on supprime le compte
echo "7) Supprime le compte du joueur<br/>";
supprimerCompte($joueur_id,true);

// on vérifie que le joueur a bien disparu
$query = "SELECT COUNT(joueur_id) FROM joueurs WHERE joueur_id = $joueur_id LIMIT 1";
$result = $dbHT->sql_query($query);
$row = $dbHT->sql_fetchrow($result);
$nbJoueurs = $row[0];
$dbHT->sql_freeresult($result);

if ($nbJoueurs > 0){
    echo "suppression du joueur incomplète !";
    $allOk = false;
}

$dbHT->sql_close();
                    
echo "<br/>";

if ($allOk)
    echo "ok";
else {
    echo "pb !";   
}

?>
