<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

if (isset($dbHT)){
    $dbHT->sql_close();
}
error_reporting(E_ALL);
    
$codeAnalytics = "UA-2189291-1";
if ($server_langue == "en")
    $codeAnalytics = "UA-10152005-1";

$smarty->assign('codeanalytics', $codeAnalytics);
$smarty->assign('email_contact', $email_contact);
$smarty->display('footer.tpl');

if (0 && isset($userdata) && sizeof($userdata) > 0 && $userdata['droits'] >= 2) {
    
    $temps_fin = microtime_float();
    /* Affichage du temps d'exécution, arrondi à 4 chiffres après la virgule */
    if (isset($temps_mid))
        echo 'Temps mid du script : '.round($temps_mid - $temps_debut, 4)."<br/>";
    if (isset($temps_mid2))
        echo 'Temps mid2 du script : '.round($temps_mid2 - $temps_mid, 4)."<br/>";
    echo 'Temps total du script : '.round($temps_fin - $temps_debut, 4);
    
    //Affiche les requêtes
    $saved_queries = $dbHT->sql_get_saved_queries();
    $nb_queries = count($saved_queries);
    echo "<div id='requete'>\n";
    if($nb_queries > 0) {
        
        echo "<table>\n";
        echo "<caption>Informations d'accès à la base de données</caption>\n";
        echo "<tr><th>N°</th><th>Durée (s)</th><th style='width:656px;'>Requête</th></tr>\n";
        $query_time_total = 0.0;
        for($i=0;$i < $nb_queries;$i++) {
            list(, $cur_query) = each($saved_queries);
            $query_time_total += $cur_query[1];
            echo "<tr><td style='text-align:right;'>".($i+1)."</td>";
            echo "<td style='text-align:center;'>".(($cur_query[1] != 0) ? sprintf('%.6f',$cur_query[1]) : '&nbsp;')."</td>";
            echo "<td>".htmlspecialchars($cur_query[0])."</td></tr>\n";
        }
        echo "<tr><td colspan='3' style='text-align:center;'>Durée totale des requêtes : ".sprintf('%.6f',$query_time_total)." s</td></tr>\n";
        echo "</table>\n";
    }
    echo "</div>\n";
}
    
?>