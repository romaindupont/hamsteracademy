<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";
include "lstAccessoires.php";

if (isset($_GET['joueur_id'])){
    
    $joueur_id = intval($_GET['joueur_id']);
    miseAJourQuotidienne($joueur_id);
    return ;
}

$depuis24H = $dateActuelle-3600*24;

// on sélectionne tous les joueurs qui n'ont pas eu de mise à jour depuis 1 jour
// date_maj_hamster < ".$depuis24H." AND 
$query = "SELECT joueur_id, date_maj_hamster FROM joueurs WHERE date_maj_hamster < $depuis24H LIMIT 5000";

if ( !($result = $dbHT->sql_query($query)) ){

	message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;

while($row=$dbHT->sql_fetchrow($result)) {

    miseAJourQuotidienne($row['joueur_id']);

}
$dbHT->sql_freeresult($result);

echo "nb de joueurs mis a jour : " . $nbJoueurs. "<br/>";

?>

