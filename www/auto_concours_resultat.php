<?php 

define('IN_HT', true);

include "common.php";
include "lstAccessoires.php";
include "gestion.php";

error_reporting( E_ALL );

// s'il n'y pas a le concours mensuel de la cage
if (getConfig("concours_cage_auto_active") == 0)
    return;

$contenuMail = "Résultat du concours de plus belle cage: \n\n";

// cadeau pour les votants
// -----------------------
$contenuMail .= "Envoi des cadeaux aux votants : salade envoyée à \n";
$query = "SELECT joueur_id FROM joueurs WHERE a_vote = 1";

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
}

$joueur_exp_id = 1371; // HamsterAcademy

while($row=$dbHT->sql_fetchrow($result)) {
    
    // petit cadeau : 5 feuilles de salade
    $type = ACC_SALADE ;
    ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
    envoyerMessagePrive($row['joueur_id'], "Comme promis, on t offre un petit cadeau pour avoir voté au concours : plein de salade !! Les hamsters adorent ça et c est super bon pour la santé ! A bientôt pour un prochain concours ! :-) ", $joueur_exp_id, false);
    $contenuMail .= $row['joueur_id'].",";
}
$dbHT->sql_freeresult($result);

$contenuMail .= "\n\n";

// réinit du statut a_vote des joueurs
// -----------------------------------
$queryVote = "UPDATE joueurs SET a_vote = 0";
if ( ! $dbHT->sql_query($queryVote) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $queryVote);
}
$contenuMail .= "Vote des joueurs remis à zéro\n";

// affichage des résultats : il fait par le module d'affichage du concours directement dans Hamster Academy
$queryEnvoyerCadeau = "UPDATE config SET valeurInt = 1 WHERE parametre = 'cadeauConcoursEnvoye'";
if ( ! $dbHT->sql_query($queryEnvoyerCadeau) ){
    message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $queryEnvoyerCadeau);
}
$contenuMail .= "Parametre d'envoi des cadeaux mis à 1\n";

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$h  = "From: contact@hamsteracademy.fr";
$subject = "Concours: résultat des votes" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= $contenuMail;
$message .= "--\n";
$mail_sent = @mail($TO, $subject, $message, $h);

echo $contenuMail ;
?>
