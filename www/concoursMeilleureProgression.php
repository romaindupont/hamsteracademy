<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

define('MODE_AUCUN',0);
define('MODE_EN_COURS',1);
define('MODE_RESULTAT',2);

$modeConcours = MODE_RESULTAT ;

if ($modeConcours == MODE_RESULTAT) {
    
    /*
    instructions pour finaliser le concours du hamster le plus fort (ou le plus "qqchose")
    1) CREATE TABLE `hamsteracademy`.`lst_joueurs_concours` (
        `joueur_id` INT NOT NULL ,
        `hamster_id` INT NOT NULL ,
        `puissance` FLOAT NOT NULL ,
        `pseudo_joueur` VARCHAR( 25 ) NOT NULL ,
        `nom_hamster` VARCHAR( 25 ) NOT NULL
        ) ENGINE = MYISAM ;
    2) vider la table
    3) mettre if (1 && ...) puis insérer les participants
    */
    if (0 && $userdata['pseudo'] == "César") {
        $query = "TRUNCATE TABLE lst_joueurs_concours";
        $dbHT->sql_query($query);
        
        $query = "SELECT joueur_id, pseudo, (note/inscritConcours),inscritConcours,note  FROM joueurs WHERE inscritConcours > 0 order by (note/inscritConcours) DESC";
        $result = $dbHT->sql_query($query);
        while($rowJoueur=$dbHT->sql_fetchrow($result)) {
            
            $queryInsert = "INSERT INTO lst_joueurs_concours VALUES(".$rowJoueur['joueur_id'].",".$rowJoueur['inscritConcours'].",'".$rowJoueur[2]."','".$rowJoueur['pseudo']."','".$rowJoueur['note']."')";
            $dbHT->sql_query($queryInsert);
        }
        $dbHT->sql_freeresult($result);
    }
    /*
    4) mettre le if(0 && ...) et trier par "lst_joueurs_concours" par le champ "puissance"
    */
    if (1 || $userdata['pseudo'] == "César") {
    
        $queryResultats = "SELECT * FROM lst_joueurs_concours ORDER BY puissance DESC";
        if (! isset($_GET['tous_les_joueurs']))
            $queryResultats .= " LIMIT 100";
        $resultResultats = $dbHT->sql_query($queryResultats);
        
        $rowVainqueur = $dbHT->sql_fetchrow($resultResultats);
        $rowSecond = $dbHT->sql_fetchrow($resultResultats);
        
        $msg .= "<br/>&nbsp;<br/><div class=ham1Nouvelle><h2 align=center>".T_("Résultats du concours du joueur qui a le mieux progressé")."</h2>";
        $msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"\"></div><br/>";
        $msg .= "<strong>".T_("Félicitations au vainqueur")." ".$rowVainqueur['pseudo_joueur']." ! ".T_("Il gagne 400 pièces et un diplôme !")."<br/>";
        $msg .= "</strong><br/>".T_("Le deuxième joueur (").$rowSecond['pseudo_joueur'].") ".T_("gagne 200 pièces (bravo à lui aussi !").")<br/>&nbsp;<br/>";
        $msg .= T_("Bravo tout le monde !");
        $msg .= " ".T_("Notez dès maintenant que d'autres concours vont arriver, vous allez pouvoir retenter votre chance ! Voici le classement !");

        if (!isset($_GET['tous_les_joueurs'])) {
            $msg .= "<h3 align=center>".T_("Les 100 premiers")."</h3>";
            $query .= " LIMIT 100";
        }
        else
            $msg .= "<h3 align=center>".T_("Tous les joueurs")."</h3>";

        
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
        
        $msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>".T_("Classement")."</strong></td><td><strong>".T_("Nom du joueur")."</strong></td>";
        $msg .= "<td align=center><strong>".T_("Son score")."</strong></td><td align=center><strong>".T_("Il passe ... à ...")."</strong></td></tr>";
        $classement = 1;
        $precNote = -1;
        $row = $rowVainqueur;
        while ($row) {
            
            $msg .= "<tr><td align=center>";
            if ($row['puissance'] == $precNote)
                $msg .= "ex-aequo";
            else
                $msg .= $classement;
                
            $msg .= "</td><td>".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo_joueur'],20))."</td><td align=center>".$row['puissance']."</td><td align=center>".$row['hamster_id']." ".T_("à")." ".$row['nom_hamster']." ".T_("points")."</td></tr>";
            
            // pour la distribution des cadeaux (à faire 1 seule fois !!)
            $joueur_exp_id = 1371; // HamsterAcademy
            if (0 && $userdata['pseudo'] == "César" && $lang == "fr") {
                if ($classement == 1) {
                    crediterPiecesBDD($row['joueur_id'],400);
                    envoyerMessagePrive($row['joueur_id'], T_("Félicitations")." ".$row['pseudo_joueur']." ! ".T_("Tu as gagné le concours ! Tu remportes 400 pièces d or et un diplome !"), $joueur_exp_id, false);
                }
                else if ($classement == 2) {
                    crediterPiecesBDD($row['joueur_id'],200);
                    envoyerMessagePrive($row['joueur_id'], T_("Félicitations")." ".$row['pseudo_joueur']." ! ".T_("Tu es second au concours ! Tu remportes 200 pièces d or !"), $joueur_exp_id, false);
                }
                else if ($classement <= 50) {
                    crediterPiecesBDD($row['joueur_id'],50);
                    envoyerMessagePrive($row['joueur_id'], T_("Félicitations")." ".$row['pseudo_joueur']." ! ".T_("Tu es")." ".$classement.T_("ème au concours ! Tu remportes 50 pièces d or !"), $joueur_exp_id, false);
                }
            }
            
            $classement ++;
            $precNote = $row['puissance'];
            
            if ($classement == 2)
                $row = $rowSecond;
            else
                $row = $dbHT->sql_fetchrow($resultResultats);
        }
        $dbHT->sql_freeresult($resultResultats);
        $msg .= "</table><br/>";
        
        $msg .= "<div align=center><a href=\"jeu.php?mode=m_concours&tous_les_joueurs=1\">".T_("Voir le classement de tous les autres joueurs")."</a></div>" ;
        
        echo $msg;
    }
}