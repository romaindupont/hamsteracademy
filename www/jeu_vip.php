<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

echo "<div style=\"font-size:9pt;\">".T_("Cette page permet de voir si tu es VIP (en anglais, <i>Very Important Person</i>) et comment le devenir.")."
</div><br/>";

echo "<div class=\"blocOptions\">";

if ($userdata['niveau'] < NIVEAU_POTAGE){
    echo T_("Accessible au niveau ")." ".NIVEAU_POTAGE."... ".T_("Finis vite les première missions !")."<br/>&nbsp;<br/>";
    //echo "</div>";
}
else {
    
    echo "<strong>".T_("Mois de ").($mois_txt[$month]).": ";
    if ($userdata['vip']) {

        echo T_("tu es VIP !")."</strong> " ; 
        echo "<br/>&nbsp;<br/>";
    }
    else {
        echo T_("tu n'es pas VIP")."...</strong>
        <ul>
        <li>".T_("il faut attendre le mois suivant, si tu remplis les critères ci-dessous")."</li>
        <li>".T_("ou")." <a href=\"banque.php?codePagePrec=vip\" title=\"".T_("Devenir VIP en achetant un Pass")."\">".T_("achète un pass")."</a>.</li></ul>" ; 
    }
    echo "<div style=\"font-size:9pt;\">".T_("Rappel des avantages si tu es VIP :")."
<ul>
<li>".T_("Ton profil sera parmi ceux affichés sur la page d'accueil du site")."</li>
<li>".T_("Tes hamsters sont en tête de liste pour la recherche des maris et bébés")."</li>
<li>".T_("Tu as 15% de réduction sur tes achats en boutique")."</li>
<li>".T_("Tu as 10% de points en plus")."</li>
</ul></div><br/>";

    echo "<strong>".T_("Serai-je VIP au mois de ").($mois_txt[($month+1)%12])." ?</strong>";
    if (isVIP($userdata['joueur_id']))
        echo T_(" oui ! ") ;
    else
        echo T_(" pas encore ... Pour cela, il faut :");
    echo "<br/>&nbsp;<br/>";
    
    // nb de connexions
    if ($userdata['nbConnectionsDansLeMois'] >= $vip_nbConnexionsMin)
        echo IMG_COCHER_OK;
    else
        echo IMG_COCHER_NON;
    echo T_("Plus de")." $vip_nbConnexionsMin ".T_("connexions au jeu depuis le début du mois (actuellement, tu  t'es connecté ").$userdata['nbConnectionsDansLeMois']." ".T_("fois").")<br/>";
    
    // niveau > 7
    if ($userdata['niveau'] >= $vip_niveauMin)
        echo IMG_COCHER_OK;
    else
        echo IMG_COCHER_NON;
    echo T_("Niveau supérieur à ")." ".$vip_niveauMin.T_(" (ton niveau est ").$userdata['niveau'].")<br/>";

    // nb de messages envoyés
    $query = "SELECT COUNT(message_id) as message_id FROM messages WHERE exp_joueur_id = ".$userdata['joueur_id']." AND dest_joueur_id != ".$userdata['joueur_id']." AND date > ".(mktime(0,0,0,$month,1));
    $result = $dbHT->sql_query($query);
    $row = $dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    
    if ($row[0] >= $vip_nbMessagesMin)
        echo IMG_COCHER_OK;
    else
        echo IMG_COCHER_NON;
    echo T_("Plus de ")."$vip_nbMessagesMin ".T_("messages envoyés par la poste depuis le début du mois (actuellement : ").$row[0]." message".($row[0] <= 1 ? "" : "s").")<br/>";
    
    // note profil
    if ($userdata['noteProfil'] >= $vip_noteProfilMin)
        echo IMG_COCHER_OK;
    else
        echo IMG_COCHER_NON;
    echo T_("Note de profil supérieure à ").$vip_noteProfilMin." / 5 (".T_("ta note actuelle est ").$userdata['noteProfil'].")<br/>";

    // nb de parrainages
    if ($userdata['nb_parrainages'] >= $vip_nbParainagesMin)
        echo IMG_COCHER_OK;
    else
        echo IMG_COCHER_NON;
    echo T_("Au moins ")."$vip_nbParainagesMin ".T_("joueurs inscrits grâce à toi depuis le début du mois (actuellement, il y en a ").$userdata['nb_parrainages'].")<br/>";
    ?>
    </div>
    <br/>
    <div class="blocOptions">
    <strong><?php echo T_("Comment inscrire des joueurs grâce à toi ?");?></strong>
    <br/>&nbsp;<br/><?php echo T_("Invite tes amis à jouer à Hamster Academy ! Deux possibilités :");?> 
    <br/>1) <?php echo T_("lors de l'inscription, ils doivent indiquer le pseudo de leur parrain, donc le tien");?> (<?php echo $userdata['pseudo'] ;?>)
    <br/>2) <?php echo T_("ou tu leur donnes le lien d'inscription spécial :");?> <input type="text" name="lien_profil" size="60" value="<?php echo $base_site_url; ?>inscription.php?parrain=<?php echo $userdata['joueur_id']; ?>" readonly="readonly" onclick="javascript:this.form.lien_profil.focus();this.form.lien_profil.select();" style="background:#fdde7b;" /><br/>&nbsp; &nbsp; <?php echo T_("que tu peux mettre sur ton blog par exemple ou dans tes mails")?>.
    <br/>&nbsp;<br/><?php echo T_("Et attention");?>, <span style="border-bottom:1px dashed black;"><?php echo T_("le multi-compte est strictement interdit</span> ainsi que l'inscription d'amis imaginaires ! Hamster Academy peut les détecter et les modérateurs surveillent en permanence !");?>
    <?php
    } // test si le niveau pour VIP est ok
    ?>

</div>

<br />
<div class="blocOptions">
<strong><?php echo T_("Qui est VIP ?");?></strong><br/>&nbsp;<br/>
<?php echo T_("Pour connaître la liste de toutes les personnes qui sont VIP, va en");?> <a href="jeu.php?mode=m_eleveurs&amp;univers=0"><?php echo T_("Ville puis à la Mairie");?></a> !
</div>