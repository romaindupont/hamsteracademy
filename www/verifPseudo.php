<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

if (isset($_POST['age'])) 
    $age=intval($_POST['age']);
if (isset($_POST['naissance_jour'])) 
    $naissance_jour=intval($_POST['naissance_jour']);
if (isset($_POST['naissance_mois'])) 
    $naissance_mois=intval($_POST['naissance_mois']);
if (isset($_POST['naissance_annee'])) 
    $naissance_annee=intval($_POST['naissance_annee']);
if (isset($_POST['sexe'])) 
    $sexe=intval($_POST['sexe']);    
if (isset($_POST['ville'])) 
    $ville=escapeRequest($_POST['ville']);
if (isset($_POST['blog'])) 
    $blog=mysql_real_escape_string($_POST['blog']);    
if (isset($_POST['pays'])) 
    $pays=intval($_POST['pays']);
if (isset($_POST['langue'])) 
    $langue=mysql_real_escape_string($_POST['langue']);
if ($mode == "inscription" && isset($_POST['pseudo'])) 
    $pseudo=escapeRequest($_POST['pseudo']);
if (isset($_POST['passwd'])) 
    $passwd=mysql_real_escape_string($_POST['passwd']);
if (isset($_POST['email'])) 
    $email=trim(mysql_real_escape_string($_POST['email']));
if (isset($_POST['presentation'])){
    $presentation=$_POST['presentation'];
    $presentationForBDD=mysql_real_escape_string($_POST['presentation']);
}
if (isset($_POST['liste_rouge_chk'])){
    if (isset($_POST['liste_rouge']))
        $liste_rouge=1;
    else
        $liste_rouge=0;
}
if (isset($_POST['montrer_profil_chk'])){
    if (isset($_POST['montrer_profil']))
        $montrer_profil=1;
    else
        $montrer_profil=0;
}
if (isset($_POST['prevenir_nouveau_message_chk'])){
    if (isset($_POST['prevenir_nouveau_message']))
        $prevenir_nouveau_message=1;
    else
        $prevenir_nouveau_message=0;
}

// on vérifie pour la charte
if (isset($_POST['presenceCharte'])){
    if (isset($_POST['validCharte']))
        $charte=1;
    else
        $charte=0;
}

// a l'inscription, on verifie la validite et la disponibilite du pseudo
if ($mode == "inscription") {
    if ($pseudo == "" || $pseudo == "admin" || $pseudo == "HamsterAcademy") {
        $erreur++;
        $erreurPseudo = 1;
    }
    else if (is_numeric($pseudo)) {
        $erreur++;
        $erreurPseudo = 3;
    }
    else if (! ereg('^[[:alnum:]_.]{3,50}$', $pseudo)) {
        $erreur++;
        $erreurPseudo = 4;
    }
    else {
        
        $query = "SELECT pseudo FROM joueurs WHERE pseudo = '".$pseudo."' LIMIT 1";
        $result = $dbHT->sql_query($query);
        if( ! $result )
        { 
            echo "Erreur SQL : ne peut traiter la requete ".$query."<br/>";
        }
        if ($dbHT->sql_numrows($result) > 0) {
            $erreur++;
            $erreurPseudo = 2;
            $message = $erreur.") Le pseudonyme ".$pseudo." a déjà été choisi. Choisis-en un autre<br/>";
        }
    }
    if ($charte == 0) {
        $erreur++;
        $erreurCharte = 1;
        $message = "Tu n'as pas validé la charte !<br/>";
    }
}

// doit-on vérifier le mot de passe ?
if ($mode == "inscription" || isset($_POST['okPourChangerMotDePasse'])) {
    if ($passwd == "") {
        $erreurPasswd = 1;
        $erreur ++ ;
    }
    else if (! ereg('^[[:alnum:]_.]{6,20}$', $passwd)) {
        $erreur++;
        $erreurPasswd = 2;
        $passwd = "";
    }
    else {

        // on vérifie qu'il a bien saisi 2 fois le même mot de passe
        if (isset($_POST['passwd_check']))
            $passwd_chk=mysql_real_escape_string($_POST['passwd_check']);

        if ($passwd_chk != $passwd){
            $erreur++;
            $erreurPasswd = 3;
            $passwd = "";
        }
    }
}

if ($email != "")
    $okPourEmail = true;

// puis le mail
// à l'inscription ou si l'email a changé, on le vérifie
if ($email != "" || ($mode == "options" && $email != $userdata['email']) ){
//    $email_chk = "";
//    if (isset($_POST['email_check']))
//        $email_chk=mysql_real_escape_string($_POST['email_check']);

//    if ($email_chk != $email){
//        $erreur++;
//        $erreurMail = 3;
//    }
    
    if (validate_mail_address($email) == 0) {
        $erreur ++;
        $erreurMail = 1;
        $email = "";
    }
    else {
        if ($mode == "inscription" ||($mode == "options" && ($email != $userdata['email']))) {
            
            // on vérifie que l'email n'existe pas déjà à l'inscription
            // ou, si le joueur est déjà inscrit, et change donc son email via le profil, on vérifie qu'il ne prend pas l'adresse d'un autre joueur
            
            $query = "SELECT email FROM joueurs WHERE email = '".$email."' LIMIT 1";
            $result = $dbHT->sql_query($query);
            if( ! $result )
            { 
                echo "Erreur SQL : ne peut traiter la requete ".$query."<br/>";
            }
            if ($dbHT->sql_numrows($result) > 0) {
                $erreur++;
                $erreurMail = 2;
                $message = $erreur.") L'email ".$email." est déjà pris. Choisis-en une autre<br/>";
            }
        }
    }
}
?>
