<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

echo "<div style=\"font-size:9pt;\"><span style=\"font-size:12pt; font-weight:bold;\">".T_("C</span>ette page permet de voir tous tes amis et d'en trouver de nouveaux").":
<ul>
<li>".T_("Tu peux voir tes amis connectés en même temps que toi (sur la page d'accueil)")."</li>
<li>".T_("Tu peux leur donner un objet de ton inventaire (voir la page d'inventaire)")."</li>
</ul>
</div><br/>";

// liste des amis
echo "<div class=\"blocOptions\"><h3>".T_("Mes amis")."</h3>";

echo afficherObjet("oeufs",3)."<br/>"; 

if ($userdata['niveau'] < NIVEAU_DEBUT_ACADEMY){
    echo str_replace("#1",NIVEAU_DEBUT_ACADEMY,T_("Accessible au niveau #1... Finis vite les première missions !"))."<br/>&nbsp;<br/>";
    echo "</div>";
}
else {

    $query = "SELECT a.ami_de, j.pseudo, j.joueur_id, j.image, j.liste_rouge FROM amis a, joueurs j WHERE a.joueur_id=".$userdata['joueur_id']." AND j.joueur_id=a.ami_de ORDER BY j.pseudo";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
    }
    $nbAmis = $dbHT->sql_numrows($result) ;
    
    if ($nbAmis == 0) { // si pas d'ami
        echo T_("Tu n'as pas encore d'ami")."...";
    }
    else { // on affiche la liste des amis
    
        while($rowAmi=$dbHT->sql_fetchrow($result)) {
            
            $image_src = "silhouette.gif" ;
            if ($rowAmi['image'] > 0)
                $image_src = "joueurs/".$rowAmi['joueur_id'].".jpg";
                
            echo "\n<span class=\"avatarSilhouette\"><img src=\"images/".$image_src."\" alt=\"\" /></span> ";
            afficherLienProfil($rowAmi['joueur_id'], $rowAmi['pseudo']);
            echo " : ".returnLienLuiEcrire($rowAmi['joueur_id'])." - ";
            echo "<a href=\"options.php?mode=m_amis&amp;actionInteraction=enleverAmi&amp;ami_id=".$rowAmi['joueur_id']."\">".IMG_SUPPRIMER.T_(" enlever de la liste")."</a><br/>";
        }
    }
    $dbHT->sql_freeresult($result);
    echo "</div>";
    
    // liste des amis en attente
    $query = "SELECT a.sollicite_id, j.pseudo, j.joueur_id, j.image, j.liste_rouge FROM amis_attente a, joueurs j WHERE a.demandeur_id=".$userdata['joueur_id']." AND j.joueur_id = a.sollicite_id";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
    }
    $nbAmisAtttente = $dbHT->sql_numrows($result) ;
    
    if ($nbAmisAtttente > 0) { 
        
        echo "<br/>&nbsp;<br/><div class=\"blocOptions\"><h3>".T_("En attente de réponse")." :</h3>";
        
        while($rowAmi=$dbHT->sql_fetchrow($result)) {
            
            $image_src = "silhouette.gif" ;
            if ($rowAmi['image'] > 0)
                $image_src = "joueurs/".$rowAmi['joueur_id'].".jpg";
                
            echo "<span class=\"avatarSilhouette\"><img src=\"images/".$image_src."\" alt=\"\" /></span> ";
            echo T_("Tu as demandé à ");
            afficherLienProfil($rowAmi['joueur_id'], $rowAmi['pseudo']);
            echo T_(" à devenir ton ami.");
            echo "<a href=\"options.php?mode=m_amis&amp;actionInteraction=enleverDemandeAmi&amp;sollicite_id=".$rowAmi['sollicite_id']."\">".IMG_SUPPRIMER." ".T_("ne plus demander")."</a><br/>";
        }
        echo "</div>";
    }
    $dbHT->sql_freeresult($result);
    
    // liste des demandes en attente
    $query = "SELECT a.demandeur_id, j.pseudo, j.joueur_id, j.image, j.liste_rouge FROM amis_attente a, joueurs j WHERE a.sollicite_id=".$userdata['joueur_id']." AND j.joueur_id = a.demandeur_id";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
    }
    $nbAmisAtttente = $dbHT->sql_numrows($result) ;
    
    if ($nbAmisAtttente > 0) { 
        
        echo "<br/>&nbsp;<br/><div class=\"blocOptions\"><h3>".T_("Demandes en cours")." :</h3>";
        
        while($rowAmi=$dbHT->sql_fetchrow($result)) {
            
            $image_src = "silhouette.gif" ;
            if ($rowAmi['image'] > 0)
                $image_src = "joueurs/".$rowAmi['joueur_id'].".jpg";
                
            echo "<span class=\"avatarSilhouette\"><img src=\"images/".$image_src."\" alt=\"\" /></span> ";
            afficherLienProfil($rowAmi['joueur_id'], $rowAmi['pseudo']);
            echo T_(" a demandé à être ton ami.");
            echo "<a href=\"options.php?mode=m_amis&amp;actionInteraction=accepterDemandeAmi&amp;demandeur_id=".$rowAmi['demandeur_id']."\">".T_("Accepter !")."</a> ".T_("ou ");
            echo "<a href=\"options.php?mode=m_amis&amp;actionInteraction=refuserDemandeAmi&amp;demandeur_id=".$rowAmi['demandeur_id']."\">".IMG_SUPPRIMER." ".T_("Refuser")."</a><br/>";
        }
         echo "</div>";
    }
    $dbHT->sql_freeresult($result);
        
    // possibilité d'ajouter un ami
    ?>
    <br/>&nbsp;<br/>
    <form action="options.php" method="get">
    <input type="hidden" name="mode" value="m_amis" />
    <input type="hidden" name="actionInteraction" value="ajouterAmi" />

    <?php echo T_("Ajouter un ami");?> : <input type="text" value="<?php echo T_("son pseudo");?>" name="pseudoAmi" />
    <input type="submit" value="<?php echo T_("Ajouter");?>" />
    </form>
<?php
} // test si le niveau pour les amis est ok
?>