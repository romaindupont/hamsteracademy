<?php 

define('IN_HT', true);

include "common.php";

$univers = UNIVERS_ELEVEUR;

$qui_alerte = "";
$qui_alerte_id = -1;
$alerte_lieu = T_("Dans le tchat ? Forum ? Message à La Poste ?");
$alerte_qui = T_("Indique ici son pseudo");
$alerte_quoi = "";
$joueur_id_plainte = -1;

$userdata = session_pagestart($user_ip);
if($userdata['session_logged_in'] ) { 
    $qui_alerte = $userdata['pseudo'] ;
    $qui_alerte_id = $userdata['joueur_id'] ;
}

if (isset($_GET['alerte_lieu']))
    $alerte_lieu = mysql_real_escape_string($_GET['alerte_lieu']) ;
if (isset($_GET['alerte_qui']))
    $alerte_qui = mysql_real_escape_string($_GET['alerte_qui']) ;
if (isset($_GET['alerte_quoi']))
    $alerte_quoi = mysql_real_escape_string($_GET['alerte_quoi']);
    
if ($alerte_lieu == "tchat")
    $alerte_lieu= T_("Dans le tchat (partie publique)");
else if ($alerte_lieu == "tchat_prive")
    $alerte_lieu= T_("Dans le tchat (message privé)");
else if ($alerte_lieu == "laposte")
    $alerte_lieu= T_("Message reçu à la Poste");
else if ($alerte_lieu == "forum")
    $alerte_lieu= T_("Message dans le forum");        
    
if (isset($_GET['alerte_submit'])) {
    if ($alerte_lieu != "" || $alerte_qui != "") {
        
        if ($alerte_qui == "LASARDINE" || $alerte_qui == "HamsterAcademy" || $alerte_qui == "César" || $alerte_qui == "goody"  || $alerte_qui == "J&U" || $alerte_qui == "Yey milie" || $alerte_qui == "clc" || $alerte_qui == "tigrette8294" || $alerte_qui == "qwerty72" )
            $alerte_qui = "";
        
        // on essaye de retrouver "alerte_qui"
        $query = "SELECT joueur_id, pseudo, nb_plaintes, email, email_active, suspendu FROM joueurs WHERE pseudo='".$alerte_qui."' LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining joueur', '', __LINE__, __FILE__, $query);
        }
        if ($dbHT->sql_numrows($result) == 1) {
            $joueurData=$dbHT->sql_fetchrow($result);
            $nb_plaintes = $joueurData['nb_plaintes'];
            $joueur_id_plainte = $joueurData['joueur_id'];
            $alerte_qui .= " (pseudo valide)";
            $dbHT->sql_freeresult($result);
            
            $nb_plaintes ++ ; 
            
            // on met à jour son compteur
            $query2 = "UPDATE joueurs SET nb_plaintes = ".$nb_plaintes." WHERE joueur_id=".$joueur_id_plainte;
            $dbHT->sql_query($query2);
            
            // pénalité éventuelle
            if ($nb_plaintes == 3 || $nb_plaintes == 5 || $nb_plaintes >= 10) {
                $dateLimiteSuspension = -1;
                
                if ($nb_plaintes == 3) // suspension 2 jours si entre 3 et 5 plaintes
                    $dateLimiteSuspension = max($dateActuelle+3600*48,$joueurData['suspendu']+3600*48);
                else if ($nb_plaintes == 5)
                    $dateLimiteSuspension = max($dateActuelle+3600*24*7,$joueurData['suspendu']+3600*24*7);
                else
                    $dateLimiteSuspension = 1;
                
                $query3 = "UPDATE joueurs SET suspendu = ".$dateLimiteSuspension." WHERE joueur_id=".$joueur_id_plainte;
                $dbHT->sql_query($query3);
                
                // on envoit un email au joueur
                if ($joueurData['email_active']){
                    $TO2 = $joueurData['email'];
                    $subject2 = T_("Hamster Academy  : Suspension de compte");
                    $message2 = str_replace("#1",$joueurData['pseudo'],T_("Bonjour #1").",\n\n".T_("Plusieurs joueurs se sont plaints d'insultes ou de grossiertés venant de ta part")."\n\n");
                    if ($dateLimiteSuspension == 1)
                        $message2 .= T_("Nous avons décidé de suspendre ton compte.");
                    else
                        $message2 .= T_("Nous avons décidé de suspendre ton compte jusqu'au ").date("d/m/Y H:i",$dateLimiteSuspension);
                    $message2 .= "\n\n".T_("Si tu penses qu'il s'agit de dénonciations mensongères et infondées, contacte nous au plus vite.")."\n\n";
                    $message2 .= "--\n".T_("L'équipe de Hamster Academy")."\nhttp://www.hamsteracademy.fr\n";
                    envoyerMail($TO2,$subject2,$message2,"alerter");
                    //$mail_sent = @mail($TO2, $subject2, $message2, $headerMailFromHamsterAcademy);
                }
            }
        }
        
        // ajout de l'alerte dans la BDD
        $query = "SELECT MAX(alerte_id) as alerte_id FROM alertes";
        $result = $dbHT->sql_query($query);
        if ( ! $result ) {
            echo "Erreur SQL : ".$query."<br>" ;
        }
        $row = $dbHT->sql_fetchrow($result);
        $nouveauId = $row[0] + 1;
        $dbHT->sql_freeresult($result);

        $query = "INSERT INTO alertes VALUES (".$nouveauId.",'".$qui_alerte."',".$qui_alerte_id.",'".$alerte_qui."',".$joueur_id_plainte." , '".$alerte_quoi."','".$alerte_lieu."',".$dateActuelle.")" ;
        if ( ! $dbHT->sql_query($query) ) {
            echo "Erreur SQL : ".$query."<br>" ;
        }
    }
}

$pagetitle = "Hamster Academy - ".T_("Alerter !");
$description = T_("Il y a un probleme ? Un joueur dit des insultes ou est grossier ? Alerte Hamster Academy !");

$liste_styles = "style.css,styleEleveur.css,styleIndex.css";    
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>
    <div align="center">
        <h1><?php echo T_("Alerter Hamster Academy");?></h1>
        <br/>
        
        <img src="images/police_tel.gif" alt="" />
        
        <br/>&nbsp;<br/>
        
        <?php if (! isset($_GET['alerte_submit'])) {
        
            echo T_("Un joueur dit des insultes ou des grossiertés ou pose des mauvaises questions ? Préviens-nous !")."<br/>&nbsp;<br/>";
        ?>
        <form action="alerter.php" method="get">
            
            <table>
                <tr>
                    <td><?php echo T_("Ton pseudo est <br/>(pour qu'on puisse te tenir informé)")." : </td><td>";
                            if ($qui_alerte == "")
                                echo "<input type=\"text\" name=\"qui_alerte\" size=\"50\" value=\"".$qui_alerte."\" /></td>";
                            else
                                echo $qui_alerte ;
                        ?></td>
                </tr>
                <tr>
                    <td>&nbsp;</td><td>&nbsp;</td>
                </tr>
                <tr>
                    <td><?php echo T_("Quel est le joueur qui insulte<br/>ou qui dit des grossiertés") ;?> : </td><td><input type="text" name="alerte_qui" size="50" value="<?php echo $alerte_qui; ?>" /></td>
                </tr>
                <tr>
                    <td><?php echo T_("Où"); ?> : </td><td><input type="text" name="alerte_lieu" size="50" value="<?php echo $alerte_lieu; ?>" /></td>
                </tr>                
                <tr>
                    <td><?php echo T_("Précisions<br/>(ce qui a été dit par exemple...)"); ?></td><td><textarea name="alerte_quoi" cols="50" rows="4"><?php echo $alerte_quoi; ?></textarea></td>
                </tr>            
            </table>
            
            <input type="submit" value="<?php echo T_("Envoyer à Hamster Academy");?>" name="alerte_submit" />
        
        </form>
        
        <?php 
    }
    else { ?>
        <br/>&nbsp;<br/>
        <?php echo T_(" Merci de nous avoir prévenu ! On te tient au courant."); ?>
        <br/>&nbsp;<br/>
        <a href="#" onclick="javascript:window.close();"><?php echo T_("Fermer cette fenêtre");?></a>
        <br/>&nbsp;<br/>
    <?php } ?>

<br/>&nbsp;<br/>
</div>
<?php require "footer.php" ; ?>

