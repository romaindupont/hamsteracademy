<?php 
define('IN_HT', true);
include "common.php";

// deconnection du jeu
$userdata = session_pagestart($user_ip);
session_end($userdata['session_id'], $userdata['joueur_id']);
session_clean($userdata['session_id']);

// deconnection spéciale phpBB pour le forum
define('PUN_ROOT', './forum/');
require PUN_ROOT.'include/common.php';
pun_setcookie(1, random_pass(8), time() + 31536000);

redirectHT("index.php");

?>