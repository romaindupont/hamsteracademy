<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

function afficherCage($cageRow, $editable=true, $lstAccessoiresDeLaCage, $nbAccessoires) {

	global $lstTypesCages, $dbHT, $taillePixelColonne, $taillePixelProfondeur,  $offsetDecalageDuLeftPourChaqueProfondeur;
	global $tailleBordureCageGauche, $tailleBordureCageBas, $ratioInclinaison3D, $userdata, $infosCage ;
	global $nbHamstersDansLaCage,$lstHamstersDansLaCage,$lst_hamsters, $nbHamsters, $lstCrottes1, $lstCrottes2, $lstCrottes3, $dateActuelle;
    
    $dessinCage = "";
    
    $tailleCage = $cageRow['colonnes'];
	
	$largeurImageCage = $infosCage[$tailleCage][INFO_CAGE_LARGEUR_IMAGE];
	$hauteurImageCage = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_IMAGE];
	$hauteurImageGrille = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_GRILLE];
	$largeurBac = $infosCage[$tailleCage][INFO_CAGE_LARGEUR_BAC];
	$hauteurBac = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_BAC];
	$ratioInclinaison = $infosCage[$tailleCage][INFO_CAGE_RATIO];
	
	// on ne dessine pas de hamster si ceux-ci sont au refuge....
	if ($nbHamsters == 0 || $lst_hamsters[0]['pause'] > 0)
		$nbHamstersDansLaCage = 0;
	
	$lstEtagesHamster = array();
	for($ha=0;$ha<$nbHamstersDansLaCage;$ha++) {
		$etageHamster = rand(0,$cageRow['etages']-1);  // choix de l'étage où sera l'hamster
		array_push($lstEtagesHamster,$etageHamster);
	}
	
	$extensionImages = ".png";
	//$user_agent=$_SERVER["HTTP_USER_AGENT"];
	//if (0 && ereg("MSIE 6", $user_agent)) 
  //  $extensionImages = ".gif"; // si le joueur possede ie 6
    
	$couleurImage = "_bleu";
	if ($cageRow['fond'] > 0) {
		if ($cageRow['fond'] == FOND_CAGE_ROSE)
			$couleurImage = "_rose";
		else if ($cageRow['fond'] == FOND_CAGE_BLANC)
			$couleurImage = "_blanc";
		else if ($cageRow['fond'] == FOND_CAGE_NOIR)
			$couleurImage = "_noir";
        else if ($cageRow['fond'] == FOND_CAGE_COEUR)
            $couleurImage = "_coeur";
        else if ($cageRow['fond'] == FOND_CAGE_FEU)
            $couleurImage = "_feu";            
        else if ($cageRow['fond'] == FOND_CAGE_HALLOWEEN)
            $couleurImage = "_halloween";            
        else if ($cageRow['fond'] == FOND_CAGE_MER)
            $couleurImage = "_mer";            
        else if ($cageRow['fond'] == FOND_CAGE_NUIT)
            $couleurImage = "_nuit";
        else if ($cageRow['fond'] == FOND_CAGE_NEIGE)
            $couleurImage = "_neige";
	}    
	
	$dessinCage .= "<div class=\"blocCage\" style=\"width:".$largeurImageCage."px;\">\n";
	
	// dessine le toit
	// ---------------
	$dessinCage .= "<div class=\"blocEtageCageToit\" style=\"z-index:1000000;\">" ;
	$dessinCage .= "<img src=\"images/cage/grille_top_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"".$hauteurImageGrille."\" alt=\"\" />" ;
	$dessinCage .= "</div>";
	
	// dessine chaque etage
	// --------------------
	for($etage=$cageRow['etages']-1;$etage>=0;$etage--)  {
	
		$dessinCage .= "\n<div class=\"blocEtageCage\" id=\"etage_$etage\" " ;
		// on calcule le z-index, de sorte que les etages superieurs soient devant
		$zIndexEtage = 2+($etage*$largeurImageCage*($hauteurImageCage+100)) ;
		
		// etage du bac
		if ($etage == 0) {
			$dessinCage .= "style=\"border:0px blue solid; height:".($hauteurImageCage)."px ; z-index:".$zIndexEtage ." ; top: 10px;\" >\n" ;
			$dessinCage .= "<div class=\"textureFond\" ";
			$dessinCage .= "style=\"border:0px red solid; height:".($hauteurImageCage)."px ; \" " ;
			$dessinCage .= "><img src=\"images/cage/fond_cage_copeaux_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"100%\" alt=\"\" /></div>";
		}
		else { // etage normal
			$dessinCage .= "style=\"border:0px blue solid; height:".$hauteurImageGrille."px ; z-index:".$zIndexEtage ." ; top: 10px;margin-bottom:10px;\">\n" ;
			$dessinCage .= "<div class=\"textureFond\" ";
			$dessinCage .= "style=\"border:0px red solid; height:".$hauteurImageGrille."px ; \" " ;
			$dessinCage .= "><img src=\"images/cage/grille_etage_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"100%\"alt=\"\" /></div>";	
		}
		
		for($ac=0;$ac<$nbAccessoires;$ac++) {
			$rowAcc = $lstAccessoiresDeLaCage[$ac];
			
			if ($rowAcc['cage_id'] == $cageRow['cage_id'] && $rowAcc['etage'] == $etage) {
			
				// cas special pour l'ecuelle : si elle est pleine, on met l'image pleine.
				if ($rowAcc['type'] == ACC_ECUELLE && $userdata['aliments'] > 0){
					$rowAcc['image'] = "ecuelle_pleine_reduc.gif";
				}
                else if ($rowAcc['type'] == ACC_EOLIENNE_ENERGIE && $cageRow['eolienne_activee'] == 0){
                    $rowAcc['image'] = "eolienne_arret.gif";
                }
                else if ($rowAcc['type'] == ACC_PISCINE && $cageRow['fond'] == FOND_CAGE_NEIGE) {
                    $rowAcc['image'] = "piscine2_neige.png";
                }
                else if ($rowAcc['type'] == ACC_PISCINE && $cageRow['proprete'] < 3) {
                    $rowAcc['image'] = "piscine2_sale.png";
                }
				
				// cas special pour le nid : s'il y a des bébés, on affiche le nid avec bébés
				if ($rowAcc['type'] == ACC_NID && $nbHamstersDansLaCage > 0 ){
					// on regarde s'il y a des bébés
					$yabebe = 0;
					for($ha=0;$ha<$nbHamstersDansLaCage;$ha++) {
						if ($lst_hamsters[hamsterCorrespondant($lstHamstersDansLaCage[$ha])]['date_naissance'] > ($dateActuelle - 172800) ) {// 2 jours
							$yabebe = 1;
							break;
						}
					}
					if ($yabebe == 1) {
						$query = "SELECT * FROM config_accessoires WHERE type=".ACC_NID_BEBE." LIMIT 1";
						if ( !($result = $dbHT->sql_query($query)) ){
							message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
						}
						$rowAccTmp=$dbHT->sql_fetchrow($result); // on écrase l'objet existant
                        $rowAcc['image'] = $rowAccTmp['image'];
						$dbHT->sql_freeresult($result);
					}
				}
				
				// calcul des coordonnées de chaque objet dans la cage
				//$baseDuLeftPourLaProfondeurDonnee = $rowAccessoires['profondeur'] *  $offsetDecalageDuLeftPourChaqueProfondeur ; 
				$posTop = $rowAcc['posY'];
				$posLeft = $rowAcc['posX'];
				
				$imgWidth = intval($rowAcc['img_width'] * $rowAcc['echelle']) ;
				$imgHeight = intval($rowAcc['img_height'] * $rowAcc['echelle']) ;
				
				// en fonction du size ratio, deplacer l'objet
				//$posTop = $posTop - intval(($infoAcc[ACC_SIZE_RATIO]-1.) * 50) ;
					
				// on calcule le z-index, de sorte que les objets plus bas soient devant et que les etages superieurs soient devant
				$zIndexAccessoire = 2+($etage*$largeurImageCage*($hauteurImageCage+100))+ $largeurImageCage*(100+($posTop+$imgHeight)) ;	
				$dessinCage .= "\n<div class=\"cageAccessoire\" id=\"acc_".$rowAcc['accessoire_id']."\"";
				$dessinCage .= " style=\"width:".($imgWidth)."px;height:".($imgHeight)."px;left:".($posLeft)."px; top:".($posTop)."px; z-index:".$zIndexAccessoire." ;  ";
				if ($rowAcc['type'] == 126)
                    $dessinCage .= "background-image: url('images/".$rowAcc['image']."') ; ";
                $dessinCage .= "\"";
				//echo " background-image: url(images/".$rowAcc['image'].") ; background-repeat:no-repeat; \" ";
				
				$lienJavascript = "";
				$lienJavascript2 = "";
				//if ($editable)
					//$lienJavascript = "onclick=\"afficherDeplacerAccessoire('".$rowAcc['image']."',".$imgWidth.",".$cageRow['cage_id'].",".$rowAcc['accessoire_id'].") ; \" " ;
                    
				if ($rowAcc['type'] == ACC_SAPIN2){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/sapindenoel2_lumiere.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/sapindenoel2.gif' ; return true; \" " ;
				}
				else if ($rowAcc['type'] == ACC_SAPIN){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/sapindenoel_lumiere.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/sapindenoel.gif' ; return true; \" " ;
				}
				else if ($rowAcc['type'] == ACC_SAPIN3){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/sapindenoel3_etoile.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/sapindenoel3.gif' ; return true; \" " ;
				}
				else if ($rowAcc['type'] == ACC_CADEAU_NOEL){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/cadeauNoel_bouge.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/cadeauNoel.gif' ; return true; \" " ;
				}
				else if ($rowAcc['type'] == ACC_CACTUS){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/cactus_lumiere.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/cactus.gif' ; return true; \" " ;
				}
				else if ($rowAcc['type'] == ACC_ARBRE){
					$lienJavascript2 = "onmouseover=\"document.img".$ac.".src='images/arbre_anim.gif' ; return true; \" " ;
					$lienJavascript2 .= "onmouseout=\"document.img".$ac.".src='images/arbre.gif' ; return true; \" " ;
				}									
				if ($rowAcc['type'] != 126)
                    $dessinCage .= " ><img name=\"img".$ac."\" src=\"images/".$rowAcc['image']."\" width=\"100%\" ".$lienJavascript." ".$lienJavascript2." alt=\"\" /></div>";
                else{
                    $dessinCage .= " ></div>";
                }

			}
		}
		
		// afficher l'hamster à une position aléatoire
		for($ha=0;$ha<$nbHamstersDansLaCage;$ha++) {
			$hamRow = $lst_hamsters[hamsterCorrespondant($lstHamstersDansLaCage[$ha])];
			
            if ($userdata['niveau'] == NIVEAU_EVASION && $ha == 0) // mission de l'évasion du hamster, on ne l'affiche pas! 
                continue;
                        
            if ($etage == $lstEtagesHamster[$ha] && $hamRow['sante'] > 0 && $hamRow['date_naissance'] < ($dateActuelle - 172800) ) { // 2 jours
				$posLeft = rand(0,$largeurImageCage);
				$posTop = rand(0,$hauteurImageCage);
				$zIndexHamster = 2+($etage*$largeurImageCage*($hauteurImageCage+100))+ $largeurImageCage*(100+($posTop+42)) ;
				$widthHamster=58;
				$heightHamster=42;
				if ($hamRow['type']==2 || $hamRow['type']==3){
					$imageHamster = "hamster_t4_".rand(1,2).".gif";
					$widthHamster=41;
					$heightHamster=42;
				}
				else{
					$imageHamster = "hamster_t2_".rand(1,2).".gif";
					$widthHamster=58;
					$heightHamster=42;
				}
				$etageFactive = $etage;
				corrigePositionAcccessoire($posLeft,$posTop,$etageFactive, $cageRow, 58, 42, 20);
				$dessinCage .= "<div class=\"cageAccessoire\" style=\"width:".$widthHamster."px;height:".$heightHamster."px;left:".($posLeft)."px; top:".($posTop)."px; z-index:".$zIndexHamster."; \"";
				$dessinCage .= tooltip("<img src='images/".$imageHamster."' width='29' style='vertical-align:middle;' /> ".$hamRow['nom']);
				$dessinCage .= " onclick=\"javascript: document.location='jeu.php?mode=m_hamster&amp;hamster_id='+".$lstHamstersDansLaCage[$ha]." ; \"";
				$dessinCage .= "><img src=\"images/".$imageHamster."\" width=\"100%\" alt=\"\" /></div>";
			}
		}
		
		if ($etage == 0) {
			
			$imageCrotte = "";
			$crotteHeight = 0;
			
			$nbCrottes = (10-$cageRow['proprete']) * 2 ;
			$lstCrottes = &$lstCrottes1;
			if ($cageRow['colonnes'] == 2)
				$lstCrottes = &$lstCrottes2;
			else if($cageRow['colonnes'] == 3)
				$lstCrottes = &$lstCrottes3;
				
			// afficher les petites crottes selon la propreté de la cage
			for($crotte=0;$crotte<$nbCrottes;$crotte++) {
				$posLeft = $lstCrottes[$crotte*2];
				$posTop = $lstCrottes[$crotte*2+1];
				if ( ($crotte%3) == 0){
					$imageCrotte = "crotte1.gif";
					$crotteHeight = 3;
				}
				else{
					$imageCrotte = "crotte".($crotte%3).".gif";
					$crotteHeight = 7;
				}
				$dessinCage .= "\n<div class=\"cageAccessoire\" style=\"width:10px;height:3px;left:".($posLeft)."px; top:".($posTop)."px; z-index:2; \"";
				$dessinCage .= "><img src=\"images/".$imageCrotte."\" width=\"10\" height=\"3\" alt=\"\" /></div>";
			}
		}

		$dessinCage .= "</div>";	
	}
	$dessinCage .= "</div>\n" ;
    
    return $dessinCage ;
}

$lstCrottes1 = array(
154,26,
212,40,
112,80,
168,76,
226,82,
62,84,
340,20,
242,80,
336,28,
60,84,
198,20,
105,64,
144,82,
271,20,
327,26,
211,84,
291,56,
342,38,
128,78,
170,82,
236,68,
102,88,
330,28,
80,94,
232,82
);

$lstCrottes2 = array(
154,26,
212,40,
112,80,
168,76,
326,82,
362,34,
340,20,
242,80,
336,28,
260,84,
198,20,
105,64,
144,82,
271,20,
327,26,
211,84,
291,56,
342,38,
128,78,
170,82,
236,68,
102,88,
330,28,
80,94,
232,82
);

$lstCrottes3 = array(
254,26,
312,100,
212,80,
368,76,
226,82,
262,120,
340,20,
442,80,
336,28,
360,44,
398,20,
205,64,
144,82,
271,20,
327,26,
211,84,
391,56,
342,38,
128,78,
170,82,
236,68,
102,88,
330,28,
80,94,
232,82
);


?>
