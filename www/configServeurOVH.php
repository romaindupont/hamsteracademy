<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

$serveur='xxx'; 
$user='xxx'; 
$password='xxx'; 
$base='xxx';  

$serveurForum='xxx'; 
$userForum='xxx'; 
$passwordForum='xxx'; 
$baseForum='xxx';  


$board_config['server_name'] = "www.hamsteracademy.fr";
$base_site_url = "http://www.hamsteracademy.fr/";

if (! isset($template_dir))
    $template_dir = 'templates';

if (! isset($smarty_dir))
    $smarty_dir = '../cgi-bin/smarty';
    
if ( ! defined('NO_SMARTY') )
{
    require($smarty_dir.'/libs/Smarty.class.php');
    
    $smarty = new Smarty();
    $smarty->template_dir = $template_dir;
    $smarty->compile_dir = $template_dir.'/templates_c';
    $smarty->cache_dir = $template_dir.'/cache';
    $smarty->config_dir = $template_dir.'/configs';
    $smarty->caching = 0;    
}

?>
