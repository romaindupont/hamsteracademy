<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

define('HAMSTER_NOM',0) ;
define('HAMSTER_IMAGE',1) ;
define('HAMSTER_YEUX',2) ;
define('HAMSTER_PELAGE',3) ;
define('HAMSTER_DESCRIPTION',4) ;

if ( ! isset($user_lang))
    $lang = "fr";

include "lstHamsters_".$lang.".php";

$nbInfosHamsters = sizeof($infosHamsters);

?>