<?php 
define('IN_HT', true);
include "common.php";
include "lstAccessoires.php";
include "lstMessages.php";
include "lstMetiers.php";
include "lstHamsters.php" ;
include "lstNiveaux.php" ;
include "gestion.php";

$userdata = session_pagestart($user_ip);
	
if( ! $userdata['session_logged_in'] ) { 
	echo "Erreur de connection";
	redirectHT("index.php",3);
}

$univers = UNIVERS_ELEVEUR;
$erreurAction = 0;
$errorSport = 0;
$errorVitamines = 0;
$erreurBeaute = 0;
$errorStage = 0 ;
$action = -1;
$msg = "" ; // messages adresses au joueur
$salaireVerse = false;

// liste des hamsters
// ----------------------
$query = "SELECT * FROM hamster WHERE joueur_id='".$userdata['joueur_id']."'";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamsters = $dbHT->sql_numrows($result) ;
$lst_hamsters=array();
while($row=$dbHT->sql_fetchrow($result)) {
	array_push($lst_hamsters,$row);
}
$dbHT->sql_freeresult($result);

// liste des cages
// ----------------------
$query = "SELECT * FROM cage WHERE joueur_id='".$userdata['joueur_id']."'";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;
$lst_cages=array();
while($row=$dbHT->sql_fetchrow($result)) {
	array_push($row,0);
	$row['nbAccessoires'] = 0;
	array_push($lst_cages,$row);
}
$dbHT->sql_freeresult($result);

// liste des accessoires pour les cages
// ------------------------------------
$query = "SELECT * FROM accessoires WHERE joueur_id='".$userdata['joueur_id']."' AND cage_id IN (";
for($i=0;$i<$nbCages;$i++) {
	$cage_row = $lst_cages[$i] ;
	$query = $query."'".$cage_row['cage_id']."',";
}
$query=$query."'-1') ORDER BY type" ;
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
}
$nbAccessoires = $dbHT->sql_numrows($result) ;
$lst_accessoires=array();
while($row=$dbHT->sql_fetchrow($result)) {
	array_push($lst_accessoires,$row);
	// on cherche la cage qui possederait cet accessoire pour le comptage
	$cage = cageCorrespondante($row['cage_id']);
	if ($cage != -1) 
		$lst_cages[$cage]['nbAccessoires'] ++ ;
}
$dbHT->sql_freeresult($result);

// recuperation de la cage/hamster/accessoire courant (id et index : id = dans la BDD, index = en memoire)
$pagePrecedente = "m_accueil";
if (isset($_GET['pagePrecedente']))
	$pagePrecedente = mysql_real_escape_string($_GET['pagePrecedente']);	
$hamster_id = -1 ;
$hamsterIndex = -1;
if (isset($_GET['hamster_id'])) {
	$hamster_id = intval($_GET['hamster_id']);
	$hamsterIndex = hamsterCorrespondant($hamster_id) ;
}
$cage_id = -1 ;
$cageIndex = -1;
if (isset($_GET['cage_id'])) {
	$cage_id = intval($_GET['cage_id']);
	$cageIndex = cageCorrespondante($cage_id) ;
}
$accessoire_id = -1;
$accessoireIndex = -1;
if (isset($_GET['accessoire_id'])){
	$accessoire_id = intval($_GET['accessoire_id']) ;
	$accessoireIndex = accessoireCorrespondant($accessoire_id);
}
$modeAffichage="m_accueil";
if (isset($_GET['mode'])) 
	$modeAffichage=mysql_real_escape_string($_GET['mode']);

// verification sur hamster_id, accessoire_id et cage_id
if ($cage_id != -1 && $cageIndex == -1) 
	redirectHT("index.php?triche=1",10);
if ($hamster_id != -1 && $hamsterIndex == -1)
	redirectHT("index.php?triche=2",10);
if ($accessoire_id != -1 && $accessoireIndex == -1)
	redirectHT("index.php?triche=3",10);

if (isset($_GET['inscription'])) { // si inscription du joueur ...
	$inscription = intval($_GET['inscription']);
	majEtapeInscription($inscription) ;
}
else { // on attend la fin de l'inscription pour faire quoique ce soit ...
	
	// update salaire (hebdo seulement et seulement si le joueur se trouve sur la place accueil)
	// --------------------------------
	$salaireVerse = false;
	if (($dateActuelle - $userdata['date_dernier_salaire']) > 604800) {
		$nbSalaires = floor(($dateActuelle - $userdata['date_dernier_salaire']) / 604800);
		crediterPieces($nbSalaires * $lstMetiers[$userdata['metier']][1],true) ;
		$salaireVerse = true;
		$msgTmp = str_replace("#1",$lstMetiers[$userdata['metier']][1],$lstMessages[8]) ;
		$msg .= $msgTmp. "<br/>&nbsp;<br/>" ;
	}
	
	// Achat
	if (isset($_GET['achat'])) {
		$objet_a_acheter = intval($_GET['achat']);
		require("achat.php");
	}
	
	// Action
	else if (isset($_GET['action'])) {
		$action = mysql_real_escape_string($_GET['action']);
		require("action.php");
	}
	
	// on fait évoluer les hamsters toutes les 24 heures
	$nbJoursDepuisPrecedenteMAJ = round (($dateActuelle - $userdata['date_maj_hamster']) / 86400) ; //(3600*24)
	if ($nbJoursDepuisPrecedenteMAJ > 0) {
		miseAJourQuotidienne($nbJoursDepuisPrecedenteMAJ);
	}
}
	
$hamster_a_afficher=-1;
if (isset($_GET['hamster_index'])) 
	$hamster_a_afficher=intval($_GET['hamster_index']);
else if (isset($_GET['hamster_id'])) {
	$hamster_a_afficher=hamsterCorrespondant(intval($_GET['hamster_id']));
}
if ($hamster_a_afficher==-1)
	$hamster_a_afficher=0 ; // par defaut, on affiche le premier hamster

if (!isset($cage_a_afficher)) {
	$cage_a_afficher=0;
	if (isset($_GET['cage_index'])) 
		$cage_a_afficher=intval($_GET['cage_index']);
	else if (isset($_GET['cage_id'])) 
		$cage_a_afficher=cageCorrespondante(intval($_GET['cage_id']));
}
?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hamster Academy</title>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-15">
<meta name="description" content="Hamster Academy, Eleve un hamster, Fabrique sa cage, Et vit à sa place, Jeu de role, jeu gratuit ">
<meta name="keywords" content="Hamster, Academy, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="icon" type="image/ico" href="/favicon.ico" />
<link rel="stylesheet" href="css/style.css" type="text/css">
</head>

<script>
function pop(lien,targ,largeur,hauteur)
{
	var larg = largeur;
	var haut = hauteur;
	targ=window.open(lien,
	targ,'toolbar=0,location=0,status=0,menubar=0,scrollbars=1,resizable=1,width='+ larg + ',height='+ haut ); targ.moveTo(0,0);
}

function calculeCoutCage()
{
	var nbEtages = (document.getElementsByName('nbEtages'))[0].value ;
	var nbColonnes = (document.getElementsByName('nbColonnes'))[0].value ;
	var nbProfondeurs = (document.getElementsByName('nbProfondeurs'))[0].value ;
	var cout = <?php echo $coutCube ; ?> * nbColonnes * nbEtages * nbProfondeurs ;
	document.getElementById('coutCage').innerHTML = cout ;
}

function verifPseudo()
{
	var string1 = new String((document.getElementsByName('prenomHamster'))[0].value) ;
	var string2 = new String("Prenom");

	if (string1.toString() == string2.toString()) {
		alert('Choisis un autre pseudo que \"Prenom\" ! ');
		return false;
	}
	else {
		return true;
	}
}

function afficherDeplacerAccessoire(imageSrc, imgWidth, cage_id, accessoire_id) {
	
	var imgSrc = '<img src=images/'+imageSrc+' width='+imgWidth+'>';
	var deplacerVersGauche = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=0><img src=images/gauche.gif alt=gauche border=0></a>' ;
	var deplacerVersDroite = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=1><img src=images/droite.gif alt=droite border=0></a>' ;
	var deplacerVersHaut = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=2><img src=images/haut.gif alt=haut border=0></a>' ;
	var deplacerVersBas = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=3><img src=images/bas.gif alt=bas border=0></a>' ;
	var deplacerVersPlusPres = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=5><img src=images/pluspres.gif alt=pres border=0></a>' ;
	var deplacerVersPlusLoin = '<a href=jeu.php?mode=m_cage&cage_id='+cage_id+'&accessoire_id='+accessoire_id+'&action=deplacerAccessoire&deplacer_sens=4><img src=images/plusloin.gif alt=loin border=0></a>' ;
	document.getElementById('acc_a_deplacer').innerHTML=imgSrc + deplacerVersGauche + deplacerVersDroite + deplacerVersHaut + deplacerVersBas + deplacerVersPlusPres + deplacerVersPlusLoin;
}

</script>

<body>

<div align=center>
	
	<h1>Hamster Academy</h1>
  <br>
</div>
<div id="header">
  <ul>
  	<li class=menuPrincipal <?php if ($modeAffichage == "m_accueil") echo "id=\"current\"" ;?> ><a href="jeu.php?mode=m_accueil">Accueil</a></li>
    <?php 
    	if ($userdata['etape_inscription'] > 5) { // on attend la fin de l'inscription pour tout afficher
    		echo "<li class=menuPrincipal ";
    		if ($modeAffichage == "m_hamster") echo "id=\"current\"" ;
    		echo " ><a href=\"jeu.php?mode=m_hamster\">";
    		if ($nbHamsters == 1) echo "Mon hamster" ; else echo "Mes hamsters" ;
    		echo "</a></li>" ;
    		
    		echo "<li class=menuPrincipal ";
    		if ($modeAffichage == "m_cage") echo "id=\"current\"" ;
    		echo " ><a href=\"jeu.php?mode=m_cage\">";
    		if ($nbCages == 1) echo "Ma cage" ; else echo "Mes cages" ;
    		echo "</a></li>" ;
    		echo "<li class=menuPrincipal ";
    		if ($modeAffichage == "m_objet") echo "id=\"current\"" ;
    		echo " ><a href=\"jeu.php?mode=m_objet\">Mes objets</a></li>";
		  	if ($userdata['niveau'] >= NIVEAU_METIER_NID ) {
		    	echo "<li class=menuPrincipal " ;
		    	if ($modeAffichage == "m_metier") echo "id=\"current\"" ;
		    	echo " ><a href=\"jeu.php?mode=m_metier\">Mon métier</a></li> ";
		    }
		    if ($userdata['niveau'] >= NIVEAU_GESTION_ETAGE ) {
		  		echo "<li class=menuPrincipalDecal " ;
		  		if ($modeAffichage == "m_achat")  echo "id=\"current\"" ;
		  		echo " ><a href=\"jeu.php?mode=m_achat\">La boutique</a></li> " ;
		  	}
			}
			?>
		<li class=menuPrincipalDroite><a href="deconnecter.php">Quitter</a></li>
		<li class=menuPrincipalDroite <?php if ($modeAffichage == "m_aide") echo "id=\"current\"" ;?> ><a href="jeu.php?mode=m_aide">Aide</a></li>
  </ul>
</div>  	

<?php 

if ($modeAffichage == "m_hamster" && $nbHamsters > 1) {   // on affiche tous les hamsters :
	echo "
	<div id=\"header\">
 		<ul>\n";
 		for($h=0;$h<$nbHamsters;$h++) {
	  	$hamRow = $lst_hamsters[$h];
	  	echo "<li ";
	  	if ($hamster_a_afficher == $h) 
	  		echo "id=\"current\"";
	  	echo "><a href=\"jeu.php?mode=m_hamster&hamster_index=".$h."\"><img src=\"images/hamster_reduc.gif\" alt=\"\" align=\"absmiddle\" border=0 >".$hamRow['nom']."</a></li>";
		}			
	  echo "</ul>
			</div>";
}
else if ($modeAffichage == "m_cage" && $nbCages > 1) {   // on affiche toutes les cages :
	echo "
	<div id=\"header\">
 		<ul>\n";
 		for($c=0;$c<$nbCages;$c++) {
	  	$cageRow = $lst_cages[$c];
	  	echo "<li ";
	  	if ($cage_a_afficher == $c) 
	  		echo "id=\"current\"";
	  	echo "><a href=\"jeu.php?mode=m_cage&cage_index=".$c."\"><img src=\"images/cage1.gif\" alt=\"\" align=\"absmiddle\"  border=0 height=20 > ".$cageRow['nom']."</a></li>";
		}
	  echo "</ul>
	</div>";
}
if ($modeAffichage == "m_academy" && $nbHamsters > 1) {   // on affiche tous les hamsters :
	echo "
	<div id=\"header\">
 		<ul>\n";
 		for($h=0;$h<$nbHamsters;$h++) {
	  	$hamRow = $lst_hamsters[$h];
	  	echo "<li ";
	  	if ($hamsterIndex == $h) 
	  		echo "id=\"current\"";
	  	echo "><a href=\"jeu.php?mode=m_hamster&hamster_id=".$hamRow['hamster_id']."\"><img src=\"images/hamster_reduc.gif\" alt=\"\" align=\"absmiddle\" border=0 >".$hamRow['nom']."</a></li>";
		}			
	  echo "</ul>
			</div>";
}

?>

<br/>&nbsp;<br/>
  	
<?php 
if ($modeAffichage == "") { // récapitulatif
	$cage_a_afficher = 0;
	require "jeu_hamster.php";
}
else if ($modeAffichage == "m_hamster") { // hamster
	require "jeu_hamster.php";
}
else if ($modeAffichage == "m_cage") { // cage
	require "jeu_cage.php";
}
else if ($modeAffichage == "m_objet") { // accessoires
	require "jeu_objets.php";
}
else if ($modeAffichage == "m_achat") { // achats
	require "jeu_achat.php";
}	
else if ($modeAffichage == "m_metier") { // metiers
	require "jeu_metier.php";
}
else if ($modeAffichage == "m_accueil") { // metiers
	require "jeu_accueil.php";
}	
else if ($modeAffichage == "m_aide") { // aide
	require "jeu_aide.php";
}	
else if ($modeAffichage == "m_academy") { // l'academy
	require "jeu_academy.php";
}	
?>

<p>&nbsp;</p>

<div align=center>&copy; Hamster Academy</div>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2189291-1";
urchinTracker();
</script>

</body>
</html>

<?php $dbHT->sql_close(); ?>

