<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require_once "gestionAcademy.php";

$hamRow = $lst_hamsters[$hamsterIndex];

if ($action == "inscrireAcademy" || $action == "inscrireAcademyFoot") {

    // on verifie que l'hamster n'est pas deja inscrit
    if ($hamRow['inscrit_academy'] > 0){
        $erreurAction = 2;
    }
    else {

        $resultat = debiterPieces($coutInscriptionAcademy, "inscription à l academy") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            // on inscrit le hamster
            if ($action == "inscrireAcademy") {
                $lst_hamsters[$hamsterIndex]['inscrit_academy'] = 1;

                // on met a jour l'hamster si c'est la première fois
                if ($lst_hamsters[$hamsterIndex]['danse'] < 10) {
                    $popularite_init = rand(1,3);
                    $chant_init = rand(1,3);
                    $musique_init = rand(1,3);
                    $sociabilite_init = rand(1,3);
                    $humour_init = rand(1,3);
                    $danse_init = rand(1,3);
                    
                    $lst_hamsters[$hamsterIndex]['popularite'] = $popularite_init;
                    $lst_hamsters[$hamsterIndex]['danse'] = $danse_init;
                    $lst_hamsters[$hamsterIndex]['chant'] = $chant_init;
                    $lst_hamsters[$hamsterIndex]['musique'] = $musique_init;
                    $lst_hamsters[$hamsterIndex]['sociabilite'] = $sociabilite_init;
                    $lst_hamsters[$hamsterIndex]['humour'] = $humour_init;
                    $lst_hamsters[$hamsterIndex]['experience'] ++;
                    $lst_hamsters[$hamsterIndex]['specialite']=-1;
            
                    $query = "UPDATE hamster SET inscrit_academy = 1 ,
                        popularite=".$popularite_init.",
                        danse=".$danse_init.",            
                        chant=".$chant_init.",
                        musique=".$musique_init.",
                        sociabilite=".$sociabilite_init.",
                        humour=".$humour_init.",
                        experience=".$lst_hamsters[$hamsterIndex]['experience'].",
                        derniere_activite = ".$dateActuelle.",
                        specialite = -1
                        WHERE hamster_id=".$hamster_id;
                }
                else {
                    $query = "UPDATE hamster SET inscrit_academy = 1 ,
                        derniere_activite = ".$dateActuelle."
                        WHERE hamster_id=".$hamster_id;
                }
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                }
            }
            else {
                // inscription à l'academy des sports
                $lst_hamsters[$hamsterIndex]['inscrit_academy'] = 2;

                // on met a jour l'hamster
                // on met a jour l'hamster si c'est la première fois
                if ($lst_hamsters[$hamsterIndex]['agilite'] < 10) {
                    $agilite_init = rand(1,3);
                    $rapidite_init = rand(1,3);
                    $defense_init = rand(1,3);
                    $endurance_init = rand(1,3);
                    $attaque_init = rand(1,3);
                    $gardien_init = rand(1,3);
                    
                    $lst_hamsters[$hamsterIndex]['agilite'] = $agilite_init;
                    $lst_hamsters[$hamsterIndex]['rapidite'] = $rapidite_init;
                    $lst_hamsters[$hamsterIndex]['defense'] = $defense_init;
                    $lst_hamsters[$hamsterIndex]['attaque'] = $attaque_init;
                    $lst_hamsters[$hamsterIndex]['gardien'] = $gardien_init;
                    $lst_hamsters[$hamsterIndex]['experience'] ++;
                    $lst_hamsters[$hamsterIndex]['specialite']=-1;
                    
                    $query = "UPDATE hamster SET inscrit_academy = 2 ,
                        agilite=".$agilite_init.",
                        rapidite=".$rapidite_init.",            
                        defense=".$defense_init.",
                        endurance=".$endurance_init.",
                        attaque=".$attaque_init.",
                        gardien=".$gardien_init.",
                        experience=".$lst_hamsters[$hamsterIndex]['experience'].",
                        derniere_activite = ".$dateActuelle.",
                        specialite = -1
                        WHERE hamster_id=".$hamster_id;
                }
                else {
                    $query = "UPDATE hamster SET inscrit_academy = 2 ,
                        derniere_activite = ".$dateActuelle."
                        WHERE hamster_id=".$hamster_id;
                }
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster_puisance', '', __LINE__, __FILE__, $query);
                }
            }       
            
            $erreurAction = 0;
            
            // on met a jour l'objectif a atteindre du niveau 
            if ($userdata['niveau'] == NIVEAU_DEBUT_ACADEMY)
                updateNiveauJoueur(0,true) ;
        }
        else {
            $erreurAction = 1;
        }
    }
}
else if ($action == "coursChant") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
    
        $resultat = debiterPieces($coutCoursChant,"cours de chant") ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            $chant = $hamRow['chant'];

            $chant += ($hamRow['sante'] / 10.0); // si mauvaise santé, mauvais progrès

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['chant'] = $chant;
            $lst_hamsters[$hamsterIndex]['musique'] += (0.5 * min(1.,($hamRow['sante'] / $hamRow['maxSante']))); // si mauvaise santé, mauvais progrès
            $lst_hamsters[$hamsterIndex]['experience'] ++;

            $query = "UPDATE hamster SET chant = '".$chant."',
                musique='".$lst_hamsters[$hamsterIndex]['musique']."',
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;

            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            $erreurAction = 0;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "coursInstrument") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
        $resultat = debiterPieces($coutCoursGuitare,"cours instrument") ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            $instrument = $hamRow['musique'];
            $instrument += (1.0 * min(1.,($hamRow['sante'] / $hamRow['maxSante']))); // si mauvaise santé, mauvais progrès

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['musique'] = $instrument;
            $lst_hamsters[$hamsterIndex]['experience'] ++;

            $query = "UPDATE hamster SET musique = '".$instrument."' ,
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            $erreurAction = 0;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "apprendreBlague") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
        $resultat = debiterPieces($coutCoursBlague,"apprentissage de blagues") ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            $blague = $hamRow['humour'];
            $boncopain = $hamRow['sociabilite'];
            $effet = 1. * min(1.,($hamRow['moral'] / $hamRow['maxMoral'])); // si mauvaise moral, mauvaises blagues
            $blague += $effet;
            $boncopain += $effet;

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['humour'] = $blague;
            $lst_hamsters[$hamsterIndex]['sociabilite'] = $boncopain;
            $lst_hamsters[$hamsterIndex]['experience'] ++;

            $query = "UPDATE hamster SET humour = '".$blague."',
                sociabilite = '".$boncopain."',
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            
            if ($hamRow['moral'] < 5)
                $erreurAction = 2;
            else
                $erreurAction = 0;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "techniqueImpro") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
        $resultat = debiterPieces($coutCoursMatchImpro,"cours impro") ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            $hamRow = $hamRow;
            $blague = $hamRow['humour'];
            $sociabilite = $hamRow['sociabilite'];
            $blague += (1. * min(1.,($hamRow['moral'] / $hamRow['maxMoral']))); // si mauvaise moral, mauvaises blagues
            $sociabilite += (1. * min(1.,($hamRow['moral'] / $hamRow['maxMoral']))); // si mauvaise moral, mauvaises blagues

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['humour'] = $blague;
            $lst_hamsters[$hamsterIndex]['sociabilite'] = $sociabilite;
            $lst_hamsters[$hamsterIndex]['experience'] ++;

            $query = "UPDATE hamster SET humour = '".$blague."' ,
                sociabilite = '".$sociabilite."',
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            $erreurAction = 0;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "coursDanse" || $action == "coursDanseTektonic") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
        $coutCours = 0;
        if ($action == "coursDanse")
            $coutCours = $coutCoursDanse;
        else
            $coutCours = $coutCoursDanseTektonic;
            
        $resultat = debiterPieces($coutCours,"cours danse") ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            $danse= $hamRow['danse'];
            $sociabilite = $hamRow['sociabilite'];
            $musique = $hamRow['musique'];
            $danse += (1. * min(1.,($hamRow['puissance'] / $hamRow['maxForce']))); // si l'hamster est peu musclé, pas de gros progrès
            $sociabilite += (0.5 * min(1.,($hamRow['moral'] / $hamRow['maxMoral']))); // si mauvaise moral, mauvaises relations
            $musique += (0.5 * min(1.,($hamRow['sante'] / $hamRow['maxSante']))); // si mauvaise santé, mauvais progrès

            if ($action == "coursDanseTektonic"){
                $lst_hamsters[$hamsterIndex]['experience'] ++;
                $danse += (1. * min(1.,($hamRow['puissance'] / $hamRow['maxForce'])));
            }

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['danse'] = $danse;
            $lst_hamsters[$hamsterIndex]['sociabilite'] = $sociabilite;
            $lst_hamsters[$hamsterIndex]['musique'] = $musique;
            $lst_hamsters[$hamsterIndex]['experience'] ++;
            
            $query = "UPDATE hamster SET danse = '".$danse."' ,
                sociabilite = '".$sociabilite."',
                musique = '".$musique."',
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            $erreurAction = 0;
            
            // on met a jour l'objectif a atteindre du niveau 4
            if ($userdata['niveau'] == NIVEAU_DEBUT_ACADEMY)
                updateNiveauJoueur(1,true) ;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "coursDribble") {
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energieMusique,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = $erreurActionEnergie;
    }
    else {
        $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot*2,$action) ;
            
        if ($resultat == 1) { // si le joueur a assez d'argent
            
            $agilite = $hamRow['agilite'];
            $rapidite = $hamRow['rapidite'];
            $attaque = $hamRow['attaque'];

            $agilite += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès
            $rapidite += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès
            $attaque += ($hamRow['sante'] / (2.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès

            // on met a jour l'hamster 
            $lst_hamsters[$hamsterIndex]['agilite'] = $agilite;
            $lst_hamsters[$hamsterIndex]['rapidite'] = $rapidite;
            $lst_hamsters[$hamsterIndex]['attaque'] = $attaque;
            $lst_hamsters[$hamsterIndex]['experience'] ++;

            $query = "UPDATE hamster SET agilite = '".$agilite."',
                rapidite = ".$rapidite.",
                derniere_activite = ".$dateActuelle.",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;

            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
            $erreurAction = 0;
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "coursPasse") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $agilite = $hamRow['agilite'];
        $rapidite = $hamRow['rapidite'];

        $agilite += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès
        $rapidite += ($hamRow['sante'] / (2.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['agilite'] = $agilite;
        $lst_hamsters[$hamsterIndex]['rapidite'] = $rapidite;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET agilite = '".$agilite."',
            rapidite = ".$rapidite.",
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursDefense") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $defense = $hamRow['defense'];

        $defense += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['defense'] = $defense;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET defense = '".$defense."',
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursTackle") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $defense = $hamRow['defense'];
        $agilite = $hamRow['agilite'];

        $defense += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès
        $agilite += ($hamRow['sante'] / (2.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['defense'] = $defense;
        $lst_hamsters[$hamsterIndex]['agilite'] = $agilite;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET defense = '".$defense."',
            agilite = ".$agilite.",
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursEndurance") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $endurance = $hamRow['endurance'];
        $defense = $hamRow['defense'];
        $attaque = $hamRow['attaque'];

        $endurance += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès
        $attaque += ($hamRow['sante'] / (3.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès
        $defense += ($hamRow['sante'] / (3.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['endurance'] = $endurance;
        $lst_hamsters[$hamsterIndex]['defense'] = $defense;
        $lst_hamsters[$hamsterIndex]['attaque'] = $attaque;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET endurance = '".$endurance."',
            derniere_activite = ".$dateActuelle.",
            defense = ".$defense.",
            attaque = ".$attaque.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursCoupsFrancs") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $attaque = $hamRow['attaque'];

        $attaque += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['attaque'] = $attaque;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET attaque = '".$attaque."',
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursTirsButs") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $attaque = $hamRow['attaque'];
        $agilite = $hamRow['agilite'];

        $attaque += ($hamRow['sante'] / (2.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès
        $agilite += ($hamRow['sante'] / (2.*$hamRow['maxSante'])); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['attaque'] = $attaque;
        $lst_hamsters[$hamsterIndex]['agilite'] = $agilite;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET attaque = '".$attaque."',
            agilite = ".$agilite.",
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursTactiques") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot*2,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $experience = $hamRow['experience'];

        $experience += ($hamRow['sante'] / ($hamRow['maxSante']/4)); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['experience'] = $experience;

        $query = "UPDATE hamster SET experience = '".$experience."',
            derniere_activite = ".$dateActuelle."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "coursGardien") {
    
    $resultat = debiterPieces(($hamRow['niveau']+1)*$coutCoursFoot,$action) ;
        
    if ($resultat == 1) { // si le joueur a assez d'argent
        
        $gardien = $hamRow['gardien'];

        $gardien += ($hamRow['sante'] / $hamRow['maxSante']); // si mauvaise santé, mauvais progrès

        // on met a jour l'hamster 
        $lst_hamsters[$hamsterIndex]['gardien'] = $gardien;
        $lst_hamsters[$hamsterIndex]['experience'] ++;

        $query = "UPDATE hamster SET gardien = '".$gardien."',
            derniere_activite = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;

        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        $erreurAction = 0;
    }
    else
        $erreurAction = 1;
}
else if ($action == "examen") {
    
    if ($dateActuelle - ($intervalleEntreDeuxExamen*86400) < $lst_hamsters[$hamsterIndex]['dernier_examen'] ){
            $erreurAction = 1;
    }
    else  { // peut passer l'exam'
    
        $hamRow = $lst_hamsters[$hamsterIndex];
        $niveau = $hamRow['niveau'] ;
        
        // performances du joueur : notes du jury
        $performanceChant = round($hamRow['chant'] + rand(0,5),1);
        $performanceMusique = round($hamRow['musique'] + rand(0,5),1);
        $performanceHumour = round($hamRow['humour'] + rand(0,5) ,1);
        
        if ($niveau > 0) {
            $performanceDanse = round($hamRow['danse'] + rand(0,5) ,1);
            $noteMoyenne = ($performanceChant + $performanceDanse + $performanceMusique + $performanceHumour) ;
            $noteMoyenne = round($noteMoyenne/4,1);
        }
        else {
            $noteMoyenne = ($performanceChant + $performanceMusique + $performanceHumour) ;
            $noteMoyenne = round($noteMoyenne/3,1);
        }
                
        $noteMinimale = 10;
        if ($hamRow['niveau'] == 0)
            $noteMinimale = 10;
        else if ($hamRow['niveau'] == 1)
            $noteMinimale = 15;
        else if ($hamRow['niveau'] == 2)
            $noteMinimale = 25;
        else if ($hamRow['niveau'] == 3)
            $noteMinimale = 40;
        else
            $noteMinimale = 80;
            
        if ($noteMoyenne >= $noteMinimale) { // le joueur a passé avec succès l'examen, il passe au niveau suivant
             $msg .= str_replace("#1",$hamRow['nom'],T_("<div>Bravo ! #1 a réussi avec succès l'examen ! Il passe au niveau suivant !</div>"));
        }
        else {// le joueur a échoué
            $msg .= str_replace("#1",$hamRow['nom'],T_("<div>Aïe aïe aïe ! #1 n'a pas réussi l'examen !</div>"));
            $erreurAction = 2;
        }        
        
        $msg .="<br/>";
        
        $msg .= T_("Les notes attribuées par le jury sont : ");
        $msg .= "<div style=\"margin-left:20px;\">".T_("Chant")." : ".$performanceChant."</div>";
        if ($niveau > 0)
            $msg .= "<div style=\"margin-left:20px;\">".T_("Danse")." : ".$performanceDanse."</div>";
        $msg .= "<div style=\"margin-left:20px;\">".T_("Musique")." : ".$performanceMusique."</div>";
        $msg .= "<div style=\"margin-left:20px;\">".T_("Humour")." : ".$performanceHumour."</div>";
        $msg .= "<br/><div style=\"margin-left:20px;\">".T_("Moyenne générale")." : ".$noteMoyenne."</div>";
        
        $msg .="<br/>";
        
        $msg .= str_replace("#1",$noteMinimale,T_("<div>Pour passer au niveau suivant, il faut atteindre une note moyenne de #1. Reprends-vite l'entraînement !</div>"));
        
        // mise à jour du niveau
        if ($noteMoyenne >= $noteMinimale)  
            $niveau = min(5,$hamRow['niveau']+1) ;
        
        if ($hamRow['niveau'] == 5) 
            $msg .= T_("<div>Tu as atteint le niveau maximum. A toi de devenir le meilleur de l'Academy. Regarde dans les Classements l'évolution de ton hamster.</div>");
        
        if ($noteMoyenne >= $noteMinimale)  {
            $msg .= T_("<div>Pour te féliciter, l'Academy t'offre 10 pièces ! </div>");
            crediterPieces(10,false);
        }
        
        // mise à jour de l'expérience
        $experience = $hamRow['experience'] + 2;
        
        // mise à jour de la popularité
        if ($noteMoyenne >= $noteMinimale) 
            $popularite = $hamRow['popularite'] + 2 ;
        else
            $popularite = $hamRow['popularite'] -1 ;
        
        $lst_hamsters[$hamsterIndex]['experience'] = $experience;
        $lst_hamsters[$hamsterIndex]['niveau'] = $niveau;
        $lst_hamsters[$hamsterIndex]['dernier_examen'] = $dateActuelle;
        $lst_hamsters[$hamsterIndex]['popularite'] = $popularite;
                
        $query = "UPDATE hamster 
            SET niveau = ".$niveau.",
            dernier_examen = ".$dateActuelle.",
            popularite = ".$popularite.",
            derniere_activite = ".$dateActuelle.",
            experience = ".$experience."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "confesser") {
    
    if ($dateActuelle - ($intervalleEntreDeuxConfessions*86400) < $lst_hamsters[$hamsterIndex]['derniere_confession'] ){
            $erreurAction = 1;
    }
    else  { // peut se confesser
        
        $hamRow = $lst_hamsters[$hamsterIndex];
        
        // mise à jour de l'expérience
        $experience = $hamRow['experience'] + 1;        
        $popularite = $hamRow['popularite'] + 2 ;
        
        $lst_hamsters[$hamsterIndex]['experience'] = $experience;
        $lst_hamsters[$hamsterIndex]['popularite'] = $popularite;
        $lst_hamsters[$hamsterIndex]['derniere_confession'] = $dateActuelle;
                
        $query = "UPDATE hamster 
            SET popularite = ".$popularite.",
            derniere_confession = ".$dateActuelle.",
            derniere_activite = ".$dateActuelle.",
            experience = ".$experience."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
        
        // on met a jour l'objectif a atteindre du niveau 
        if ($userdata['niveau'] == NIVEAU_POPULARITE)
            updateNiveauJoueur(0,true) ;
    }
}
else if ($action == "raconterBlagueCouloir") {
    
    $hamRow = $lst_hamsters[$hamsterIndex];
    $nouveauMoral = $hamRow['moral'];
    
    // sa blague marche ou pas ? 4/5 chances
    $chance = rand(1,10);
    if ($chance < 3) {
        // soit chez le directeur, soit un autre académicien l'a humilié
        if ($chance == 1) {
            // chez le directeur ... popularité en baisse, le moral en prend un coup
            $popularite = $hamRow['popularite'] - 4 ;    
            $nouveauMoral = seuillerMoral($hamRow['moral'] - 2,$hamRow['maxMoral']) ;    
            $erreurAction = 1;
        }
        else {
            // un hamster l'humilie
            $popularite = $hamRow['popularite'] - 4 ;    
            $nouveauMoral = seuillerMoral($hamRow['moral'] - 2,$hamRow['maxMoral']) ;    
            $erreurAction = 2;
        }
    }
    else {
        $popularite = $hamRow['popularite'] + 1 ;    
    }
    
    // mise à jour de l'expérience
    //$experience = $hamRow['experience'] + 1;
    
    $lst_hamsters[$hamsterIndex]['popularite'] = $popularite;
    $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral;
            
    $query = "UPDATE hamster 
        SET popularite = ".$popularite.",
        derniere_activite = ".$dateActuelle.",
        moral = ".$nouveauMoral."
        WHERE hamster_id=".$hamster_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
    }

}
else if ($action == "choisirInstrument") {

    $instrument = -1;
    if (isset($_GET['instrument']))
        $instrument_id = intval($_GET['instrument']);

    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = accessoireCorrespondant($instrument_id) ;
    
    if ($accessoireIndex == -1) { // pas d'instrument en stock
        $erreurAction = 1 ;
    }
    else { 

        // on vérifie que le hamster associé n'a pas déjà un instrument
        $hamsterIndex = hamsterCorrespondant($hamster_id);
        
        if ($lst_hamsters[$hamsterIndex]['specialite'] != -1)
            $erreurAction = 2;
        else {

            // on recherche l'instrument associé à l'accessoire choisi
            $instChoisi=-1;
            for($inst=0;$inst<$nbInstruments;$inst++){
                if ($lstInstruments[$inst][INSTRUMENT_REF_OBJET] == $lst_accessoires[$accessoireIndex]['type']){
                    $instChoisi = $inst;
                    break;
                }
            }
            $hamRow = $lst_hamsters[$hamsterIndex];
            
            // mise à jour de sa spécialité
            $lst_hamsters[$hamsterIndex]['specialite'] = $instChoisi;
        
            $query = "UPDATE hamster 
                SET specialite = ".$instChoisi.",
                derniere_activite = ".$dateActuelle."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
    
            // on supprime l'accessoire de la liste
            reduireQuantiteAccessoire($instrument_id,1);
        }    
    }
}
else if ($action == "changerInstrument") {

    $hamRow = $lst_hamsters[$hamsterIndex];
            
    // mise à jour de sa spécialité
    $lst_hamsters[$hamsterIndex]['specialite'] = -1;
    $lst_hamsters[$hamsterIndex]['experience'] /= 2;

    $nouvelleExperience = round($lst_hamsters[$hamsterIndex]['experience']);
    
    $query = "UPDATE hamster 
        SET specialite = -1,
        derniere_activite = ".$dateActuelle.",
        experience = $nouvelleExperience
        WHERE hamster_id=".$hamster_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
    }
}
else if ($action == "creerGroupe") {
    
    if ($hamRow['groupe_id'] != -1) {
        $erreurAction = 1;
    }
    else if ($hamRow['inscrit_academy'] == 1 && $hamRow['specialite'] == -1) {
        $erreurAction = 2;
    }
    else {

    $resultat = debiterPieces($coutCreationGroupe,"creation de groupe") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent    
                
            $query = "SELECT MAX(groupe_id) as groupe_id FROM groupes";
            $result = $dbHT->sql_query($query);
            if ( ! $result ) {
                echo "Erreur SQL : ".$query."<br>" ;
            }
            $row = $dbHT->sql_fetchrow($result);
            $id = $row[0] + 1;
            $dbHT->sql_freeresult($result);
            
            if (isset($_GET['nomDuGroupe'])){
                $nomDuGroupe = mysql_real_escape_string($_GET['nomDuGroupe']);
                    
                $description = mysql_real_escape_string(T_("Description")." ".$typeGroupes[$hamRow['inscrit_academy']]['DE_GROUPE']);
                $image = -1; $score = 0; $nbMembres = 1;
                $experience = 0; $derniere_activite = $dateActuelle;
                $dernier_concert = 0;$edition_membre = true;
                $inscritTournoi = -1;
                $dateInscriptionTournoi = 0;
                $tournoi_pool = -1;
                $score_pool = -1;
                $classement_pool = -1;
                $niveau = 0;
                $type = $hamRow['inscrit_academy'];
                $ancienne_experience = 0;
                $possede_manager = 0;
                
                $query = "INSERT INTO groupes VALUES (".$id.", '".$nomDuGroupe."' ,".$hamRow['hamster_id']." , '".$description."' ,$dateActuelle,$image,$score,$nbMembres,$experience,$derniere_activite,$dernier_concert,$edition_membre,$inscritTournoi,$dateInscriptionTournoi,$tournoi_pool,$score_pool,$classement_pool,$niveau,$type,$ancienne_experience,$possede_manager,'$lang') ";
                $result = $dbHT->sql_query($query);
                if ( ! $result ) {
                    echo "Erreur SQL : ".$query."<br>" ;
                    return;
                }
                
                $query = "UPDATE hamster SET groupe_id = ".$id." WHERE hamster_id = ".$hamRow['hamster_id'];
                $result = $dbHT->sql_query($query);
                if ( ! $result ) {
                    echo "Erreur SQL : ".$query."<br>" ;
                    return;
                }
                $lst_hamsters[$hamsterIndex]['groupe_id'] = $id;
            }
        }
    }
}
else if ($action == "modifGroupe") {
    
    if ($hamRow['groupe_id'] == -1) {
        $erreurAction = 1;
    }
    else {
        $query = "SELECT leader_id, edition_membre FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " lIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        
        if ($nbGroupes != 1){
            $erreurAction = 2;
        }
        else {            
            $rowGroupe=$dbHT->sql_fetchrow($result);
            
            // on vérifie que le hamster est bien leader ou que les membres peuvent éditer
            if ($rowGroupe['leader_id'] != $hamRow['hamster_id'] && ! $rowGroupe['edition_membre'] ) {
                $erreurAction = 3;
            }
            else {
                if (isset($_GET['description']) || isset($_POST['description'])){
                    
                    $description = "";
                    if (isset($_GET['description']))
                        $description = mysql_real_escape_string(stripslashes($_GET['description'])) ;
                    else
                        $description = mysql_real_escape_string(stripslashes($_POST['description'])) ;
                    
                    $query = "UPDATE groupes SET description = '".$description."' ,
                    derniere_activite = ".$dateActuelle."     
                    WHERE groupe_id = ".$hamRow['groupe_id'];
                    $result = $dbHT->sql_query($query);
                    if ( ! $result ) {
                        echo "Erreur SQL : ".$query."<br>" ;
                        return;
                    }
                }
            }
        }
    }
}
else if ($action == "modifImageGroupe") {
    
    if ($hamRow['groupe_id'] == -1) {
        $erreurAction = 1;
    }
    else {
        
        // on vérifie que le joueur est bien leader de ce groupe et membre de ce groupe
        $query = "SELECT leader_id,edition_membre FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " lIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        
        if ($nbGroupes != 1){
            $erreurAction = 2;
        }
        else {
            
            $rowGroupe=$dbHT->sql_fetchrow($result);
            
            // on vérifie que le hamster est bien leader
            if ($rowGroupe['leader_id'] != $hamRow['hamster_id'] && ! $rowGroupe['edition_membre'] ) {
                $erreurAction = 3;
            }
            else {
                
                $max_size   = 100000;     // Taille max en octets du fichier
                $width_max  = 300;        // Largeur max de l'image en pixels
                $height_max = 300;        // Hauteur max de l'image en pixels
                
                
                if(!empty($_FILES['fichierImage']['name'])) {
                    
                        $nom_file   = $_FILES['fichierImage']['name'];
                        $taille     = $_FILES['fichierImage']['size'];
                        $tmp        = $_FILES['fichierImage']['tmp_name']; 
                
                // On vérifie l'extension du fichier
                if(substr($nom_file, -3) == "jpg") {
                    // On récupère les dimensions du fichier
                    $infos_img = getimagesize($_FILES['fichierImage']['tmp_name']);
                    
                    // On vérifie les dimensions et taille de l'image
                    if(($infos_img[0] <= $width_max) && ($infos_img[1] <= $height_max) && ($_FILES['fichierImage']['size'] <= $max_size)) {                
                        // Si c'est OK, on teste l'upload
                        if(move_uploaded_file($_FILES['fichierImage']['tmp_name'],"images/groupes/".$hamRow['groupe_id'].".jpg")) {
                            // Si upload OK alors on affiche le message de réussite
                            $msg .= "<b>".T_("Image envoyé avec succès !")."</b><br/>(".T_("pour être sûr de voir la nouvelle image, il faut appuyer sur F5").")";
                            
                            $query = "UPDATE groupes SET image = 1 ,
                            derniere_activite = ".$dateActuelle."
                            WHERE groupe_id = ".$hamRow['groupe_id'];
                                                $result = $dbHT->sql_query($query);
                                                if ( ! $result ) {
                                                    echo "Erreur SQL : ".$query."<br>" ;
                                                    return;
                                                }
                                    } else {
                            // Sinon on affiche une erreur système
                            $msg .= "<b>".T_("Problème lors de l'envoi !")."</b><br /><br /><b>,".$_FILES['fichierImage']['error']."</b><br /><br />";
                        }
                    } else {
                        // Sinon on affiche une erreur pour les dimensions et taille de l'image
                        $msg .= "<b>".T_("Problème dans les dimensions ou taille de l'image !")."</b><br /><br />";
                    }
                } else {
                    // Sinon on affiche une erreur pour l'extension
                    $msg .= "<b>".T_("Votre image ne comporte pas l'extension .jpg !")."</b><br /><br />";
                }
              }
            else {
                // Sinon on affiche une erreur pour le champ vide
                $msg .= "<b>".T_("Le champ du formulaire est vide !")."</b><br /><br />";
            }                 
            }
        }
    }
}
else if ($action == "quitterGroupe") {
    $query = "UPDATE hamster SET groupe_id = -1, inscrit_repet = 0, inscrit_concert = 0 WHERE hamster_id = ".$hamster_id;
    if ( !$dbHT->sql_query($query) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $lst_hamsters[$hamsterIndex]['groupe_id'] = -1;
    $lst_hamsters[$hamsterIndex]['inscrit_concert'] = 0;
    $lst_hamsters[$hamsterIndex]['inscrit_concert'] = 0;


    // mise à jour du nombre de membres
    $query = "SELECT nb_membres FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " lIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $rowGroupe=$dbHT->sql_fetchrow($result);;
    $nbMembres = $rowGroupe['nb_membres'];
    
    $query = "UPDATE groupes SET nb_membres = ".($nbMembres-1).",
        derniere_activite = ".$dateActuelle."
        WHERE groupe_id = ".$hamRow['groupe_id'];
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
        return;
    }
    
    ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], $hamRow['nom'].T_(" a quitté le groupe."));
}
else if ($action == "dissoudreGroupe") {
    
    // on vérifie que le hamster est bien leader
    $query = "SELECT leader_id, nom, image FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " lIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    if ($nbGroupes != 1){
        $erreurAction = 1;
    }
    else {
        
        $rowGroupe=$dbHT->sql_fetchrow($result);
        
        // on vérifie que le hamster est bien leader
        if ($rowGroupe['leader_id'] != $hamRow['hamster_id']) {
            $erreurAction = 2;
        }
        else {
            
            $groupe_id_a_dissoudre = $hamRow['groupe_id'];
            
            // on dissout le groupe : pour tous les membres, on remet à -1 leur groupe_id et on leur envoie un message
            dissoudreGroupe($groupe_id_a_dissoudre,$rowGroupe['leader_id']);
        }
    }
}
else if ($action == "virerMembre") {
    
    // on vérifie que le hamster est bien leader
    $query = "SELECT leader_id, nom, nb_membres FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    if ($nbGroupes != 1){
        $erreurAction = 1;
    }
    else {
        
        if (isset($_GET['hamsterAVirer_id'])) {
            
            $hamsterAVirer_id = intval($_GET['hamsterAVirer_id']);
        
            $rowGroupe=$dbHT->sql_fetchrow($result);
            $dbHT->sql_freeresult($result);
            
            // on vérifie que le hamster est bien leader
            if ($rowGroupe['leader_id'] != $hamRow['hamster_id']) {
                $erreurAction = 2;
            }
            else {
                
                // on vire le membre : on remet à -1 son groupe_id et on lui envoie un message
                $query = "SELECT joueur_id, nom FROM hamster WHERE hamster_id = ".$hamsterAVirer_id." LIMIT 1";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                }
                $messageAEnvoyer = str_replace("#1",$rowGroupe['nom'],T_("(Message automatique) Le leader du groupe #1 a décidé de se séparer de toi. Tu n'as plus de groupe."));
                $messageAEnvoyer = mysql_real_escape_string($messageAEnvoyer);
                
                $rowMembre=$dbHT->sql_fetchrow($result);
                $dbHT->sql_freeresult($result);
                
                // on récupère l'id du joueur possédant le hamster leader 
                $query = "SELECT joueur_id, nom FROM hamster WHERE hamster_id = ".$rowGroupe['leader_id']." LIMIT 1";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                }
                $joueurLeaderId = $dbHT->sql_fetchrow($result);
                $dbHT->sql_freeresult($result);
                
                // envoi d'un message pour prévenir
                envoyerMessagePrive($rowMembre['joueur_id'], $messageAEnvoyer, $joueurLeaderId['joueur_id']) ;
                
                // on vire le hamster
                $query = "UPDATE hamster SET groupe_id = -1, inscrit_repet = 0, inscrit_concert = 0 WHERE hamster_id = ".$hamsterAVirer_id;
                if ( !$dbHT->sql_query($query)){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                }
                
                $query = "UPDATE groupes SET nb_membres = ".($rowGroupe['nb_membres']-1).",
                    derniere_activite = ".$dateActuelle."
                    WHERE groupe_id = ".$hamRow['groupe_id'];
                $result = $dbHT->sql_query($query);
                if ( ! $result ) {
                    echo "Erreur SQL : ".$query."<br>" ;
                    return;
                }
                ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], $rowMembre['nom'].T_(" a dû quitter le groupe."));
            }
        }
    }
}
else if ($action == "changerStatusFoot") {
    
    if (isset($_GET['changerStatusFoot'])) {
        
        $joueurFoot_id = intval($_GET['changerStatusFoot']);
        $specialite= -1;
        if (isset($_GET['changerStatusFoot_'.$joueurFoot_id])){
            $specialite = intval($_GET['changerStatusFoot_'.$joueurFoot_id]);
            
            // on vérifie que cet hamster appartient bien au groupe du leader    
            $query = "UPDATE hamster SET specialite = ".$specialite." WHERE hamster_id = ".$joueurFoot_id." AND groupe_id = ".$hamRow['groupe_id']. " LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $dbHT->sql_freeresult($result);
            
            ajouterBulletin($hamRow['groupe_id'],$hamRow['hamster_id'],$hamRow['nom'],getNomHamsterFromId($joueurFoot_id).T_(" devient ").$lstSpecialitesFoot[$specialite]);
        }        
    }    
}
else if ($action == "rejoindreGroupe") {
    
    $typeGroupe = $hamRow['inscrit_academy'];
    
    if ($typeGroupe == 0 || $hamRow['groupe_id'] != -1) {
        $erreurAction = 1;
    }
    else if ($typeGroupe == 1 && $hamRow['specialite'] == -1) {
        $erreurAction = 2;
    }
    else if (isset($_GET['groupe_id'])) {

        $groupe_id = intval($_GET['groupe_id']) ;

        $query = "SELECT nom FROM groupes WHERE groupe_id = ".$groupe_id. " lIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        
        if ($nbGroupes == 1){
            
            // on vérifie qu'on n'a pas atteint le nombre de limite des membres
            $query = "SELECT hamster_id FROM hamster WHERE groupe_id = ".$groupe_id. " LIMIT 15";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbMembres = $dbHT->sql_numrows($result) ;
            
            if ($nbMembres >= $typeGroupes[$typeGroupe]['NB_MAX_JOUEURS']) {
                $erreurAction = 3;
            }
            else {
                //ajouterInteraction($hamRow['hamster_id'],$groupe_id,REJOINDRE_GROUPE,"Rejoindre le groupe",0);
                $query = "UPDATE hamster SET groupe_id = ".$groupe_id." WHERE hamster_id = ".$hamster_id;
                $result = $dbHT->sql_query($query);
                if ( ! $result ) {
                    echo "Erreur SQL : ".$query."<br>" ;
                    return;
                }
                $lst_hamsters[$hamsterIndex]['groupe_id'] = $groupe_id;
                
                $query = "UPDATE groupes SET nb_membres = ".($nbMembres+1).",
                    derniere_activite = ".$dateActuelle."
                    WHERE groupe_id = ".$groupe_id;
                $result = $dbHT->sql_query($query);
                if ( ! $result ) {
                    echo "Erreur SQL : ".$query."<br>" ;
                    return;
                }
                ajouterBulletin($groupe_id, $hamRow['hamster_id'], $hamRow['nom'], $hamRow['nom'].T_(" a rejoint le groupe !"));
            }
        }
    }
}
else if ($action == "accepterMembre") {
    
    
    
}
else if ($action == "ajouterBulletin") {
    
    if ($hamRow['groupe_id'] == -1)
        return;

    $texte = "";
    if (isset($_GET['bulletinTexte']))
        $texte = mysql_real_escape_string(stripslashes($_GET['bulletinTexte']));
        
    $alaposte = isset($_POST['alaposte']) || isset($_GET['alaposte']);
    
    ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], $texte, $alaposte);

}
else if ($action == "nouveauNomGroupe"){
    
    // on vérifie que le hamster est bien leader
    $query = "SELECT leader_id FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " AND leader_id = ".$hamRow['hamster_id']." lIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    if ($nbGroupes != 1){
        $erreurAction = 1;
    }
    else {
        $nouveauNom = "";
        if (isset($_GET['nom'])){
            $nouveauNom = mysql_real_escape_string($_GET['nom'] );
            if ($nouveauNom == "null")
                $nouveauNom = "";
        }
        if ($nouveauNom != "") {
            $query = "UPDATE groupes 
                SET nom = '".$nouveauNom."',
                derniere_activite = ".$dateActuelle."
                WHERE groupe_id=".$hamRow['groupe_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating cage _ nom', '', __LINE__, __FILE__, $query);
            }
        }
    }
}
else if ($action == "inscrireRepet"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
    
    // on vérifie que le hamster n'est pas déjà inscrit
    if ($hamRow['inscrit_repet'])
        $erreurAction = 1;
    else {
        // que le joueur possède assez d'argent
        $resultat = debiterPieces(5,"inscription repet") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent
            $query = "UPDATE hamster SET inscrit_repet = 1,
            derniere_activite = ".$dateActuelle."
            WHERE hamster_id=".$hamRow['hamster_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ inscrit_repet', '', __LINE__, __FILE__, $query);
            }
            $lst_hamsters[$hamsterIndex]['inscrit_repet'] = 1;
            
            // on met a jour l'objectif a atteindre du niveau 
            if ($userdata['niveau'] == NIVEAU_GROUPE_MUSIQUE)
                updateNiveauJoueur(0,true) ;
        }
        else
            $erreurAction = 2;
    }
}
else if ($action == "inscrireConcert"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
    
    // on vérifie que le hamster n'est pas déjà inscrit
    if ($hamRow['inscrit_concert'])
        $erreurAction = 1;
    else {
        
        // on récupère l'expérience du groupe
        $query2 = "SELECT experience FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query2);
        }
        $rowGroupe = $dbHT->sql_fetchrow($result2) ;
        $experienceGroupe = $rowGroupe['experience'];        
        $dbHT->sql_freeresult($result2);
        
        // que le joueur possède assez d'argent (si on change la formule du cout du concert, il faut la changer dans l'affichage du groupe)
        //$resultat = debiterPieces(5 * (1+ ($experienceGroupe/5) ),"inscription concert") ;
        $coutInscriptionConcert = 10;
        $resultat = debiterPieces($coutInscriptionConcert,"inscription concert") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent
            $query = "UPDATE hamster SET inscrit_concert = 1,
            derniere_activite = ".$dateActuelle." 
            WHERE hamster_id=".$hamRow['hamster_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ inscrit_repet', '', __LINE__, __FILE__, $query);
            }
            $lst_hamsters[$hamsterIndex]['inscrit_concert'] = 1;
        }
        else
            $erreurAction = 2;
    }
}
else if ($action == "repeterEnGroupe"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
    
    // on dresse la liste des hamsters inscrit à ce groupe et à la répèt
    $query = "SELECT hamster_id, experience, musique FROM hamster WHERE groupe_id = ".$hamRow['groupe_id']. " AND inscrit_repet=1 LIMIT 8";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
    }
    $nbMembres = $dbHT->sql_numrows($result) ;
    
    // est-ce que les 3/4 des membres sont inscrits ?
    if ($nbMembres < 3 || $nbMembres*4 < $nbMembres*3 ) {
        $erreurAction = 1;
    }
    else {
        
        // on augmente l'expérience du groupe
        $query2 = "SELECT experience, niveau FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query2);
        }
        $rowGroupe = $dbHT->sql_fetchrow($result2) ;
        $experience = $rowGroupe['experience'];        
        $dbHT->sql_freeresult($result2);
        
        // le joueur possède t-il la partition RockRollHamsters ?
        if (possedeAccessoire(ACC_PARTITION_ROCKROLLHAMSTERS,$userdata['joueur_id']) != -1) {
            $experience += $nbMembres;
            $erreurAction = 2;
            ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], str_replace("#1",$nbMembres,T_("J ai déclenché la répétition en apportant la partoche de <i>RockRollAndHamsters in the Trees</i>. Il déchire ce tube et on l a bien bossé, on devrait pouvoir la jouer en concert ! Il y a eu #1 musiciens à la répétition.")));
            
            // on met a jour l'objectif a atteindre du niveau 
            if ($userdata['niveau'] == NIVEAU_GROUPE_MUSIQUE)
                updateNiveauJoueur(1,true) ;
        }
        else {
            $experience += $nbMembres/2;
            ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], str_replace("#1",$nbMembres,T_("J ai déclenché la répétition, elle s est bien passée ! Il y a eu #1 musiciens à la répétition.")));
        }

        $query2 = "UPDATE groupes SET 
            experience = ".$experience.",
            derniere_activite = ".$dateActuelle." 
            WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        $dbHT->sql_query($query2);
        
        while($rowMembreInscrit = $dbHT->sql_fetchrow($result)) {
            // on donne de l'expérience à chaque hamster (1 pt)
            $experience = $rowMembreInscrit['experience'] + 1;
            $musique = $rowMembreInscrit['musique'] + 1;
            
            $query2 = "UPDATE hamster SET 
                experience = ".$experience.",
                musique = ".$musique.", 
                inscrit_repet = 0 
                WHERE hamster_id = ".$rowMembreInscrit['hamster_id']." LIMIT 1";
            $dbHT->sql_query($query2);
            
            if ($rowMembreInscrit['hamster_id'] == $hamRow['hamster_id'])
                $lst_hamsters[$hamsterIndex]['inscrit_repet'] = 0;
        }
        $dbHT->sql_freeresult($result);
    }    
}
else if ($action == "entrainementFoot"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
        
    // on dresse la liste des hamsters inscrit à ce groupe et à la répèt
    $query = "SELECT hamster_id, experience, nom, joueur_id, note, inscrit_repet FROM hamster WHERE 
        groupe_id = ".$hamRow['groupe_id']. " 
        AND inscrit_repet = 1 LIMIT 21";
        
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
    }
    $nbMembres = $dbHT->sql_numrows($result) ;
    
    if ($nbMembres < 11) {
        $erreurAction = 1;
    }
    else {
        // on vérifie qu'il y a bien assez d'expérience et 24h entre chaque
        $query2 = "SELECT * FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query2);
        }
        $rowGroupe = $dbHT->sql_fetchrow($result2) ;
        $experience = $rowGroupe['experience'];        
        $dbHT->sql_freeresult($result2);
           
        // on augmente l'expérience du groupe
        $experience ++;
        
        $query2 = "UPDATE groupes SET 
            experience = ".$experience.",
            derniere_activite = ".$dateActuelle." 
            WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        $dbHT->sql_query($query2);
        
        $lstMembres = array();
        while($rowMembreInscrit = $dbHT->sql_fetchrow($result)) {
            array_push($lstMembres,$rowMembreInscrit);
        }
        
        // on augmente l'expérience de chaque joueur
        for($m=0;$m<$nbMembres;$m++) {
            
            // on donne de l'expérience à chaque hamster (1 pt)
            if ($lstMembres[$m]['inscrit_repet']){
            
               $experience = $lstMembres[$m]['experience'] + 1;
                
                $query2 = "UPDATE hamster SET 
                    experience = ".$experience.", 
                    inscrit_repet = 0 
                    WHERE hamster_id = ".$lstMembres[$m]['hamster_id']." LIMIT 1";
                $dbHT->sql_query($query2);
            }
            
            if ($lstMembres[$m]['hamster_id'] == $hamRow['hamster_id'])
                $lst_hamsters[$hamsterIndex]['inscrit_repet'] = 0;
                
        }
                        
        ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], T_("L entrainement a bien eu lieu ! L équipe progresse !"));
        $msg .= afficherNouvelle("ballon_foot.gif", T_("L entrainement a bien eu lieu ! L équipe progresse !"));
    }
    
    $dbHT->sql_freeresult($result);
}
else if ($action == "footMatchAmical"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
        
    // on dresse la liste des hamsters inscrit à ce groupe et à la répèt
    $query = "SELECT hamster_id, experience, nom, joueur_id, note, inscrit_concert FROM hamster WHERE 
        groupe_id = ".$hamRow['groupe_id']. " 
        AND inscrit_concert=1 LIMIT 21";
        
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
    }
    $nbMembres = $dbHT->sql_numrows($result) ;
    
    // est-ce que les 3/4 des membres sont inscrits ?
    if ($nbMembres < 11) {
        $erreurAction = 1;
    }
    else {
        // on vérifie qu'il y a bien assez d'expérience et 24h entre chaque
        $query2 = "SELECT * FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query2);
        }
        $rowGroupe = $dbHT->sql_fetchrow($result2) ;
        $experience = $rowGroupe['experience'];        
        $dbHT->sql_freeresult($result2);
        
        // 1 concert par 24h
        if($dateActuelle - (24*3600) < $rowGroupe['dernier_concert'] ){
            $erreurAction = 3;
        }
        else  {
            
            if ($experience < 20) {
                $erreurAction = 2;
            }
            else {
            
                // on augmente l'expérience du groupe
                $experience ++;
                $bilanMatch = "";
                $gainMatch = 0;
                
                $lstMembres = array();
                while($rowMembreInscrit = $dbHT->sql_fetchrow($result)) {
                    array_push($lstMembres,$rowMembreInscrit);
                }
                
                $gainConcert = round($lstNiveauxGroupe[$rowGroupe['niveau']][3]);
                
                $bilanConcert .= T_("Chaque joueur inscrit au match gagne %d pièces !");
                $bilanConcert = sprintf($bilanConcert,abs($gainConcert));
                
                $query2 = "UPDATE groupes SET 
                    experience = ".$experience.",
                    dernier_concert = ".$dateActuelle.", 
                    derniere_activite = ".$dateActuelle." 
                    WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
                $dbHT->sql_query($query2);
                
                // on augmente l'expérience de chaque joueur
                for($m=0;$m<$nbMembres;$m++) {
                    
                    // on donne de l'expérience à chaque hamster (1 pt)
                    if ($lstMembres[$m]['inscrit_concert']){
                    
                       $experience = $lstMembres[$m]['experience'] + 1;
                        
                        $query2 = "UPDATE hamster SET 
                            experience = ".$experience.", 
                            inscrit_concert = 0 
                            WHERE hamster_id = ".$lstMembres[$m]['hamster_id']." LIMIT 1";
                        $dbHT->sql_query($query2);
                    }
                    
                    if ($lstMembres[$m]['hamster_id'] == $hamRow['hamster_id'])
                        $lst_hamsters[$hamsterIndex]['inscrit_concert'] = 0;
                        
                        
                    crediterPiecesBDD($lstMembres[$m]['joueur_id'],$gainConcert);
                }
                                
                ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], $bilanConcert);
                $msg .= afficherNouvelle("ballon_foot.gif",$bilanConcert);
                
                checkNiveauGroupe($rowGroupe);
            }
            $dbHT->sql_freeresult($result);
        }
    }        
}
else if ($action == "concertEnGroupe"){
    
    if ($hamRow['groupe_id'] == -1)
        return;
        
    // on dresse la liste des hamsters inscrit à ce groupe et à la répèt
    $query = "SELECT hamster_id, experience, nom, joueur_id, note, inscrit_concert FROM hamster WHERE 
        groupe_id = ".$hamRow['groupe_id']. " 
        AND inscrit_concert=1 LIMIT 8";
        
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
    }
    $nbMembres = $dbHT->sql_numrows($result) ;
    
    // est-ce que les 3/4 des membres sont inscrits ?
    if ($nbMembres < 3 || $nbMembres*4 < $nbMembres*3) {
        $erreurAction = 1;
    }
    else {
        // on vérifie qu'il y a bien assez d'expérience et 24h entre chaque
        $query2 = "SELECT * FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query2);
        }
        $rowGroupe = $dbHT->sql_fetchrow($result2) ;
        $experience = $rowGroupe['experience'];        
        $dbHT->sql_freeresult($result2);
        
        // 1 concert par 24h
        if($dateActuelle - (24*3600) < $rowGroupe['dernier_concert'] ){
            $erreurAction = 3;
        }
        else  {
            
            if ($experience < 20) {
                $erreurAction = 2;
            }
            else {
            
                // on augmente l'expérience du groupe
                $experience ++;
                $bilanConcert = "";
                $gainConcert = 0;
                
                $lstMembres = array();
                while($rowMembreInscrit = $dbHT->sql_fetchrow($result)) {
                    array_push($lstMembres,$rowMembreInscrit);
                }
                
                // écart-type des musiciens
                $noteMoyenne = 0;
                for($m=0;$m<$nbMembres;$m++)
                    $noteMoyenne += $lstMembres[$m]['note'] ;
                $noteMoyenne /= $nbMembres;
                
                $ecartMoyen = 0;
                $ecartMax = 0;
                $nomDuHamsterPasTresBon = "";
                for($m=0;$m<$nbMembres;$m++){
                    $ecartSigneJoueur = $noteMoyenne - $lstMembres[$m]['note'] ;
                    $ecartJoueur = abs($ecartSigneJoueur) ;
                    $ecartMoyen +=  $ecartJoueur;
                    if ($ecartMax < $ecartSigneJoueur) {
                        $ecartMax = $ecartSigneJoueur;
                        if ($ecartSigneJoueur > 0) {
                            $nomDuHamsterPasTresBon = $lstMembres[$m]['nom'];
                        }
                    }
                }
                $ecartMoyen /= $nbMembres;
                
                if ($ecartMoyen < $noteMoyenne/5) {
                    if ($ecartMax < $noteMoyenne/5) {
                        $gainConcert = 1.;
                        $bilanConcert = T_("Le groupe a bien joué dans l ensemble. Le public a beaucoup apprécié ! ");
                    }
                    else {
                        $gainConcert = 0.75;
                        $bilanConcert = T_("Le groupe a bien joué dans l ensemble sauf ").$nomDuHamsterPasTresBon.T_(" qui a un niveau inférieur et qui a fait plein de fausses notes. Il faut qu il s entraine !");
                    }
                }
                else {
                    //$gainConcert = - floor((1+(log($experience) / 5))*(5+rand(0,5))) / 5;
                    $gainConcert = 0.5;
                    $bilanConcert = T_("Il y a trop de niveaux différents dans ce groupe, trop de fausses notes, heureusement rattrapées par certains musiciens mais ça ne suffit pas. Il faut reprendre l entrainement pour les plus faibles.");
                }
                
                if ($nbMembres == 8) {
                    $experience++;
                    $gainConcert *= 1.25;
                }
                
                $gainConcert = round( (1.+$gainConcert)* $lstNiveauxGroupe[$rowGroupe['niveau']][3]);
                
                $bilanConcert .= T_("Chaque musicien inscrit au concert gagne %d pièces !");
                $bilanConcert = sprintf($bilanConcert,abs($gainConcert));
                
                $query2 = "UPDATE groupes SET 
                    experience = ".$experience.",
                    dernier_concert = ".$dateActuelle.", 
                    derniere_activite = ".$dateActuelle." 
                    WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT 1";
                $dbHT->sql_query($query2);
                
                // on augmente l'expérience de chaque joueur
                for($m=0;$m<$nbMembres;$m++) {
                    
                    // on donne de l'expérience à chaque hamster (1 pt)
                    if ($lstMembres[$m]['inscrit_concert']){
                    
                       $experience = $lstMembres[$m]['experience'] + 1;
                        
                        $query2 = "UPDATE hamster SET 
                            experience = ".$experience.", 
                            inscrit_concert = 0 
                            WHERE hamster_id = ".$lstMembres[$m]['hamster_id']." LIMIT 1";
                        $dbHT->sql_query($query2);
                    }
                    
                    if ($lstMembres[$m]['hamster_id'] == $hamRow['hamster_id'])
                        $lst_hamsters[$hamsterIndex]['inscrit_concert'] = 0;
                        
                        
                    crediterPiecesBDD($lstMembres[$m]['joueur_id'],$gainConcert);
                }
                                
                ajouterBulletin($hamRow['groupe_id'], $hamRow['hamster_id'], $hamRow['nom'], $bilanConcert);
                $msg .= afficherNouvelle("studiomusique.gif",$bilanConcert);
                
                // on met a jour l'objectif a atteindre du niveau 
                if ($userdata['niveau'] == NIVEAU_GROUPE_MUSIQUE)
                    updateNiveauJoueur(2,true) ;
                    
                checkNiveauGroupe($rowGroupe);
            }
            $dbHT->sql_freeresult($result);
        }
    }    
}
else if ($action == "modifEditionMembre") {

    if (isset($_GET['groupe_id'])) {
        
        $groupe_id = intval($_GET['groupe_id']);
        
        if (isLeader($groupe_id,$hamster_id)) {
    
            $editionMembre = 0;
            
            // le joueur a-t-il coché la case ?
            if (isset($_GET['checkEditionMembre'])) {
                $editionMembre = 1;
                ajouterBulletin($groupe_id,$hamster_id,"Leader",T_("Le leader a autorisé tous les membres à modifier la description du groupe !"));
            }
            else {
                ajouterBulletin($groupe_id,$hamster_id,"Leader",T_("Le leader n autorise plus tous les membres à modifier la description du groupe..."));
            }

            $query = "UPDATE groupes SET edition_membre = ".$editionMembre." WHERE groupe_id=".$groupe_id ;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
            }
        }
        else {
            $msg .= T_("Il faut être leader du groupe pour modifier cette option<br/>");   
        }
    }
}
else if ($action == "inscrireTournoi") {
    
    if ($hamRow['groupe_id'] == -1) {
        $erreurAction = 1;
    }
    else {
        
        if (isset($_GET['tournoi_id'])) {
            $tournoi_id = intval($_GET['tournoi_id']) ;
            $query = "UPDATE groupes SET 
                inscrit_tournoi = $tournoi_id,
                date_inscription_tournoi = $dateActuelle
             WHERE groupe_id = ".$hamRow['groupe_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
            }
            ajouterBulletin($hamRow['groupe_id'],$hamRow['hamster_id'],$hamRow['nom'],T_("Le groupe a été inscrit à un tournoi !"));
        }
    }
}
else if ($action == "choisirJoueurFoot" || $action == "choisirGardienFoot") {
    
    if ($hamRow['specialite'] != -1)
        $erreurAction = 1;
    else {
        $specialite_choisie = -1;
        if ($action == "choisirJoueurFoot")
            $specialite_choisie = 0;
        else
            $specialite_choisie = 1;
            
        $query = "UPDATE hamster SET 
                specialite = $specialite_choisie
                WHERE hamster_id = ".$hamRow['hamster_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
        }
        
        $lst_hamsters[$hamsterIndex]['specialite'] = $specialite_choisie;
    }    
}
else if ($action == "attribuerManager") {

    if (possedeAccessoire(ACC_MANAGER_MUSIC) != -1) {
        
        // la requête SQL vérifie que le groupe existe bien et que son niveau est bien supérieur à 3
        $query = "UPDATE groupes SET possede_manager = true WHERE groupe_id = ".$hamRow['groupe_id']. " AND niveau >= 3";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        if ($nbGroupes == 1){
            reduireQuantiteAccessoire(ACC_MANAGER_MUSIC,1);
            ajouterBulletin($hamRow['groupe_id'],$hamRow['hamster_id'],$hamRow['nom'],T_("Le groupe a dorénavant un manager ! C'est plus de points pour nous !"));
        }
        $dbHT->sql_freeresult($result);
    }
}
else if ($action == "changerAcademy") {
    $query = "UPDATE hamster SET 
            inscrit_academy = 0
            WHERE hamster_id = ".$hamRow['hamster_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, '', '', __LINE__, __FILE__, $query);
    }
    $lst_hamsters[$hamsterIndex]['inscrit_academy'] = 0;
}

// on met a jour l'objectif de popularité a atteindre du niveau 5
if ($userdata['niveau'] == NIVEAU_POPULARITE && $lst_hamsters[$hamsterIndex]['popularite'] > 9)
    updateNiveauJoueur(1,true) ;

?> 
