<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require_once "gestionUser.php";

if ($action == "halteres") {
    
    if ($hamster_id != -1) {
        
        // assez d'énergie
        $energieRestante = debiterEnergie($energieHalteres,$hamsterIndex);
        if ($energieRestante == -1) {
            $erreurAction = 1;
        }
        else {
            // on verifie que le joueur possede bien l'accessoire
            $accessoireIndex = possedeAccessoire(ACC_HALTERES) ;
            
            if ($accessoireIndex == -1) { // pas d'haltères en stock
                $erreurAction = 2 ;
            }
            else { // peut faire du sport
                $hamRow = $lst_hamsters[$hamsterIndex];
                
                // on vérifie que le hamster n'est pas enceinte
                if ($hamRow['sexe'] > 1) {
                    $erreurAction = 3;
                }
                else {
                    $precedenteForce = $hamRow['puissance'] ;
                    $nouvelleForce  = seuillerForce($precedenteForce + 1,$hamRow['maxForce']) ;
                    $lst_hamsters[$hamsterIndex]['puissance'] = $nouvelleForce;
                    $lst_hamsters[$hamsterIndex]['dernier_sport'] = $dateActuelle;
                    
                    $nouveauPoids = ameliorerPoids(2,$hamRow['poids']) ;
                    $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;
                    $lst_hamsters[$hamsterIndex]['experience'] ++;
                    
                    $query = "UPDATE hamster 
                        SET puissance = '".$nouvelleForce."',
                        poids = ".$nouveauPoids.",
                        experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                        WHERE hamster_id=".$hamster_id;
                    if ( !($dbHT->sql_query($query)) ){
                        message_die(GENERAL_ERROR, 'Error updating hamster_puisance', '', __LINE__, __FILE__, $query);
                    }
                }
            }
        }
    }
}
else if ($action == "mettreDansRoueMotorisee") {
    
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']) ;
    
        $hamsterCorrespondant = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = yatilAccessoireDansLaCage($lst_hamsters[$hamsterCorrespondant]['cage_id'],ACC_ROUE_AVEC_ENGRENAGE_PILE_FIL,$userdata['joueur_id']) ;
        
        if ($accessoireIndex == -1) { // pas de roue motorisée en stock
            $erreurAction = 1;
        }
        else { // peut faire de la roue motorisée et perdre ainsi tous ses kilos en trop
            $hamRow = $lst_hamsters[$hamsterCorrespondant];
                
            $precedenteForce = $hamRow['puissance'] ;
            $nouvelleForce  = seuillerForce($precedenteForce + 2, $hamRow['maxForce'])  ;
            $lst_hamsters[$hamsterCorrespondant]['puissance'] = $nouvelleForce;
            
            $nouveauPoids = $hamRow['poids'];
            if ($hamRow['poids'] > 120) 
                $nouveauPoids = 120;
            $lst_hamsters[$hamsterCorrespondant]['poids'] = $nouveauPoids;
            $lst_hamsters[$hamsterCorrespondant]['experience'] ++;
            
            $query = "UPDATE hamster 
                SET puissance = ".$nouvelleForce.",
                poids = ".$nouveauPoids.",
                experience = ".$lst_hamsters[$hamsterCorrespondant]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster_puisance', '', __LINE__, __FILE__, $query);
            }
            
            // on supprime la pile
            changerTypeAccessoire($lst_accessoires[$accessoireIndex]['accessoire_id'], ACC_ROUE_AVEC_ENGRENAGE) ;
            $lst_accessoires[$accessoireIndex]['type'] = ACC_ROUE_AVEC_ENGRENAGE;
        }
    }    
    else
        message_die(GENERAL_ERROR, 'bad hamster_ id ', '', __LINE__, __FILE__, $query);

}
else if ($action == "nourrir") {
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']) ;
        $hamster_nourri = nourrirHamster($hamster_id);    
        if ($hamster_nourri == 1 && $userdata['niveau']==NIVEAU_NOURRIR_COPEAUX) {
            updateNiveauJoueur(0,true) ;
        }
    }
}
else if ($action == "nourrirtousleshamsters") {
    
    if ($userdata['aliments'] < $quantiteNourritureQuotidienneAvalee * $nbHamsters) {
        $erreurAction = 1;
    }
    else {
        for($h=0;$h<$nbHamsters;$h++) {
          nourrirHamster($lst_hamsters[$h]['hamster_id']);
        }
    }
}
else if ($action == "ajouterEtage") {
    
    $cage_index = cageCorrespondante($cage_id) ;
    $cageRow = $lst_cages[$cage_index];
    $coutAgrandissementHauteur = intval($coutFabrication + $coutCube * $cageRow['colonnes'] * $cageRow['profondeurs']);
    $resultat = debiterPieces($coutAgrandissementHauteur,"ajout d un etage") ;
    
    if ($resultat == 1) { // si le joueur a assez d'argent
        $lst_cages[$cage_index]['etages'] = $lst_cages[$cage_index]['etages'] + 1;
        $query = "UPDATE cage 
        SET etages = '".$lst_cages[$cage_index]['etages']."'
        WHERE cage_id='".$cage_id."'";
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating cage_data', '', __LINE__, __FILE__, $query);
        }
        setCageToBeRebuilt($cage_id);
        
        // on modifie eventuellement le niveau 2 du joueur
        if ($userdata['niveau']==NIVEAU_GESTION_ETAGE) { 
            updateNiveauJoueur(0,true) ;
        }
        $erreurAction = 0;
    }
    else {
        $erreurAction = ERREUR_ACTION_CAGE_ETAGE;
    }
}
else if ($action == "agrandirSurface") {
    
    $cage_index = cageCorrespondante($cage_id) ;
    $cageRow = $lst_cages[$cage_index];
    
    if ($cageRow['colonnes'] > 2) // la taille de la cage doit être inférieure à 4
        $erreurAction = ERREUR_ACTION_CAGE_COLONNE;
    else {
        $coutAgrandissementSurface = intval($coutFabrication + $coutCube * $cageRow['etages'] * $cageRow['profondeurs'] * $cageRow['colonnes']);
        $resultat = debiterPieces($coutAgrandissementSurface, "agrandissement de la surface de la cage") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent
            $lst_cages[$cage_index]['colonnes'] = $lst_cages[$cage_index]['colonnes'] + 1;
            $lst_cages[$cage_index]['profondeurs'] = $lst_cages[$cage_index]['profondeurs'] + 1;
            $query = "UPDATE cage 
            SET colonnes = '".$lst_cages[$cage_index]['colonnes']."',
            profondeurs = '".$lst_cages[$cage_index]['profondeurs']."'
            WHERE cage_id='".$cage_id."'";
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating cage_data', '', __LINE__, __FILE__, $query);
            }
            setCageToBeRebuilt($cage_id);
            $erreurAction = 0;
        }
        else {
            $erreurAction = ERREUR_ACTION_CAGE_COLONNE;
        }
    }
}
else if ($action == "recup_veto") {
    $resultatAchat = debiterPieces($coutVeto,"Veto") ;
    if ($resultatAchat == 1) {
        // remettre la sante a 5/10
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        $lst_hamsters[$hamster_index]['sante'] = $lst_hamsters[$hamster_index]['maxSante']/2 ;
        $lst_hamsters[$hamster_index]['puissance'] = max($lst_hamsters[$hamster_index]['puissance'],3) ;
        $lst_hamsters[$hamster_index]['moral'] = max($lst_hamsters[$hamster_index]['moral'],3) ;
        $lst_hamsters[$hamster_index]['poids'] = 100 ;
        $lst_hamsters[$hamster_index]['experience'] ++;
        
        $query = "UPDATE hamster 
            SET sante = 5 ,
            moral = ".$lst_hamsters[$hamster_index]['moral'].",
            puissance = ".$lst_hamsters[$hamster_index]['puissance'].",
            poids = 100,
            chezleveto = 0,
            experience = ".$lst_hamsters[$hamster_index]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
        }
    }
    else {
        $msgTmp = T_("Tu n'as pas assez d'argent pour payer le vétérinaire car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
        $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_HAMSTER);
    }
}
else if ($action == "mettreObjetDans") {

    $accessoire_id = -1;
    if (isset($_GET['accessoire_id']))
        $accessoire_id = intval($_GET['accessoire_id']) ;
    $accessoire_index = accessoireCorrespondant($accessoire_id);

    $pourlacage_id = -1;
    if (isset($_GET['deplacer_dans_cage_id'])) 
        $pourlacage_id = intval($_GET['deplacer_dans_cage_id']) ;
        
    $precedenteCage_id = -1;
    if (isset($_GET['precedenteCage_id']))
        $precedenteCage_id = intval($_GET['precedenteCage_id']) ;
    
    mettreAccessoireDansLaCage($accessoire_id, $precedenteCage_id, $pourlacage_id) ;
    setCageToBeRebuilt($pourlacage_id);
    setCageToBeRebuilt($precedenteCage_id);
}
else if ($action == "remplacerCopeaux") {
    require "remplacerCopeaux.php" ;
}
else if ($action == "changertouslescopeaux") {
    
    if ($userdata['copeaux'] < $nbCages){
        $erreurAction = 1;   
    }
    else {
        for($c=0;$c<$nbCages;$c++) {
            $cage_id = $lst_cages[$c]['cage_id'];
            require "remplacerCopeaux.php" ;
        }
    }
}
else if ($action == "vitamines") {
    
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que les precedentes vitamines n'ont pas ete prises trop recemment
        if ($dateActuelle - ($intervalleEntreDeuxVitamines*3600) < $lst_hamsters[$hamster_index]['dernier_vitamines'] ){
            $erreurAction = 1;
        }
        else  { 
            
            if ($lst_hamsters[$hamster_index]['sante'] == 0)
                return;
            
            // on verifie que le joueur possede bien l'accessoire
            $accessoireIndex = possedeAccessoire(ACC_VITAMINES) ;
            
            if ($accessoireIndex == -1)
                $accessoireIndex = possedeAccessoire(ACC_VITAMINES_PACK5) ;
            
            if ($accessoireIndex == -1) { // pas de vitamines en stock
                $erreurAction = 2 ;
            }
            else { // peut prendre des vitamines
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
                $nouvelleSante = $lst_hamsters[$hamster_index]['sante'] + 2;
                $nouvelleSante = seuillerSante($nouvelleSante,$lst_hamsters[$hamster_index]['maxSante']) ;    
                $lst_hamsters[$hamster_index]['sante'] = $nouvelleSante ;
                $lst_hamsters[$hamster_index]['dernier_vitamines'] = $dateActuelle;
                $lst_hamsters[$hamster_index]['experience'] ++;
                
                $query = "UPDATE hamster 
                    SET sante = ".$nouvelleSante." ,
                    dernier_vitamines = ".$dateActuelle.",
                    experience = ".$lst_hamsters[$hamster_index]['experience']."
                    WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
                }
                $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
            }
        }
}
else if ($action == "mettrecreme") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
        
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_CREME) ;
    
    if ($accessoireIndex == -1) { // pas de creme en stock
        $erreurAction = 2 ;
    }
    else { // peut prendre de la creme
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouvelleBeaute = seuillerBeaute($lst_hamsters[$hamster_index]['beaute'] + 1, $lst_hamsters[$hamster_index]['maxBeaute']) ;
        $lst_hamsters[$hamster_index]['beaute'] = $nouvelleBeaute ;
        $lst_hamsters[$hamster_index]['dernier_soins_beaute'] = $dateActuelle;
        $lst_hamsters[$hamster_index]['experience'] ++;
        
        $query = "UPDATE hamster 
            SET beaute = '".$nouvelleBeaute."',
            dernier_soins_beaute = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamster_index]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
    }
}
else if ($action == "mettrehamsteropoil") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
        
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_CREME_HAMSTEROPOIL) ;
    
    if ($accessoireIndex == -1) { // pas de creme en stock
        $erreurAction = 2 ;
    }
    else { // peut prendre de la creme
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouvelleBeaute = seuillerBeaute($lst_hamsters[$hamster_index]['beaute'] + 2, $lst_hamsters[$hamster_index]['maxBeaute']) ;
        $lst_hamsters[$hamster_index]['beaute'] = $nouvelleBeaute ;
        $lst_hamsters[$hamster_index]['dernier_soins_beaute'] = $dateActuelle;
        $lst_hamsters[$hamster_index]['experience'] ++;
        
        $query = "UPDATE hamster 
            SET beaute = '".$nouvelleBeaute."',
            dernier_soins_beaute = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamster_index]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
    }
}
else if ($action == "brosser") {
    
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que les precedentes vitamines n'ont pas ete prises trop recemment
        if ($dateActuelle - ($intervalleEntreDeuxBeaute*3600) < $lst_hamsters[$hamster_index]['dernier_soins_beaute'] ){
            $erreurAction = 1;
        }
        else  { 
            // on verifie que le joueur possede bien l'accessoire
            $accessoireIndex = possedeAccessoire(ACC_BROSSE) ;
            
            if ($accessoireIndex == -1) { // pas de brosse en stock
                $erreurAction = 2 ;
            }
            else { // peut se faire brosser
                $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
                $nouvelleBeaute = seuillerBeaute($lst_hamsters[$hamster_index]['beaute'] + 0.5, $lst_hamsters[$hamster_index]['maxBeaute']) ;
                
                $lst_hamsters[$hamster_index]['beaute'] = $nouvelleBeaute ;
                $lst_hamsters[$hamster_index]['dernier_soins_beaute'] = $dateActuelle;
                $lst_hamsters[$hamster_index]['experience'] ++;
                
                $query = "UPDATE hamster 
                    SET beaute = '".$nouvelleBeaute."' ,
                    dernier_soins_beaute = ".$dateActuelle.",
                    experience = ".$lst_hamsters[$hamster_index]['experience']."
                    WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
                }
            }
        }
}
else if ($action == "stagepleinair") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_STAGE_PLEIN_AIR) ;
    
    if ($accessoireIndex == -1) { // pas de stages en stock
        $erreurAction = 1 ;
    }
    else { // peut partir en stage
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 2;
        $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
        $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
        $lst_hamsters[$hamster_index]['experience'] += 2;
        
        $query = "UPDATE hamster 
            SET moral = ".$nouveauMoral.",
            experience = ".$lst_hamsters[$hamster_index]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
        }
        reduireQuantiteAccessoire($accessoire_id,1) ;
    }
}
else if ($action == "supprimerAcc") {
    supprimerAccessoire($accessoire_id);
}

else if ($action == "vendreAcc") {
    
    vendreAccessoire($accessoire_id);
    redirectHT("jeu.php?mode=m_objet");
}
else if ($action == "nouveauNomCage"){
    
    $nouveauNom = "";
    if (isset($_GET['nom'])){
        $nouveauNom = mysql_real_escape_string($_GET['nom'] );
        if ($nouveauNom == "null")
            $nouveauNom = "";
    }
    if ($nouveauNom != "") {
        $cageIndex = cageCorrespondante($cage_id);
        $lst_cages[$cageIndex]['nom'] = $nouveauNom ;
        $query = "UPDATE cage 
            SET nom = '".$nouveauNom."' 
            WHERE cage_id='".$cage_id."'";
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating cage _ nom', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "nouveauNomHamster"){
    
    $nouveauNom = "";
    if (isset($_GET['nom'])){
        $nouveauNom = mysql_real_escape_string($_GET['nom'] );
        if ($nouveauNom == "null")
            $nouveauNom = "";
    }
    if ($nouveauNom != "") {
        $hamsterIndex = hamsterCorrespondant($hamster_id);
        $lst_hamsters[$hamsterIndex]['nom'] = $nouveauNom ;
        $query = "UPDATE hamster 
            SET nom = '".$nouveauNom."' 
            WHERE hamster_id='".$hamster_id."'";
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ nom', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "deplacerAccessoire") {
    require "deplacerAccessoire.php";
}
else if ($action == "deplacerHamster") {

    $nouvelle_cage_id = -1;
    if (isset($_GET['cageHamster'])) {
        $nouvelle_cage_id = intval($_GET['cageHamster']) ;
        $cageIndex = cageCorrespondante($nouvelle_cage_id);
    }
    if ($cageIndex == -1)
        redirectHT("index.php?triche=4",2); // verification anti-triche
        
    $precedenteCage_id = $lst_hamsters[$hamsterIndex]['cage_id'] ;
    
    $lst_hamsters[$hamsterIndex]['cage_id'] =  $nouvelle_cage_id;
    $query = "UPDATE hamster
            SET cage_id = '".$nouvelle_cage_id."' 
            WHERE hamster_id='".$hamster_id."'";
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster _ cage', '', __LINE__, __FILE__, $query);
    }
}
else if ($action == "changerMetier"){
    
    require_once "lstMetiers.php";
    
    if (isset($_GET['metierIndex'])){
        $metierIndex = intval($_GET['metierIndex']);
        
        // on vérifie que le joueur peut accéder à ce métier
        if ($userdata['niveau'] >= NIVEAU_METIER_NID && $lstMetiers[$metierIndex][METIER_NIVEAU_ACCESSIBLE] <= $userdata['niveau']) {
        
            $cout = $lstMetiers[$metierIndex][METIER_COUT_FORMATION] ;
            $resultatAchat = debiterPieces($cout, "changement de metier vers ".$lstMetiers[$metierIndex][METIER_NOM]);
            if ($resultatAchat == 0) {
                $msgTmp = T_("Tu n'as pas assez d'argent pour suivre cette formation car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
                $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_METIER);
            }
            else {
                $userdata['metier'] = $metierIndex;
                $query = "UPDATE joueurs
                    SET metier = '".$metierIndex."' 
                    WHERE joueur_id='".$userdata['joueur_id']."'";
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur _ metier', '', __LINE__, __FILE__, $query);
                }
                $msg .= str_replace("#1",$lstMetiers[$metierIndex][METIER_NOM],T_("Félicitations ! Tu as suivi avec succès la formation pour devenir #1."));
                
                if ($userdata['niveau'] == NIVEAU_METIER_NID)
                    updateNiveauJoueur(0,true) ;            
            }
        }
    }
}
else if ($action == "donnerBonbon") {
    
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_BONBON) ;
        
        if ($accessoireIndex == -1) { // pas de bonbon en stock
            $erreurAction = 1 ;
        }
        else { // peut prendre des bonbons
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 1;
            $nouveauPoids = $lst_hamsters[$hamster_index]['poids'] + 7;
            $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
            $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
            $lst_hamsters[$hamster_index]['poids'] = $nouveauPoids ;
            $query = "UPDATE hamster 
            SET moral = ".$nouveauMoral." ,
            poids = ".$nouveauPoids."
            WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
            }
            reduireQuantiteAccessoire($accessoire_id,1) ;
        }
}
else if ($action == "donnerLivreBeaute") {
    
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_LIVRE_BEAUTE) ;
        
        if ($accessoireIndex == -1) { // pas de livre en stock
            $erreurAction = 1 ;
        }
        else { // peut lire
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            $lst_hamsters[$hamster_index]['init_coquet'] = 5;
            
            $query = "UPDATE hamster SET init_coquet = 5 WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
            }
            reduireQuantiteAccessoire($accessoire_id,1) ;
        }
}
else if ($action == "donnerCoton") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_COTON) ;
    
    if ($accessoireIndex == -1) { // pas de coton en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['possede_coton']=1;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $query = "UPDATE hamster 
        SET possede_coton = 1    WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ coton', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        // on vérifie s'il peut commencer la construction du nid
        verifierIngredientsNid($hamster_id) ;
    }
}
else if ($action == "donnerPaille") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_PAILLE) ;
    
    if ($accessoireIndex == -1) { // pas de paille en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['possede_paille']=1;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $query = "UPDATE hamster 
        SET possede_paille = 1    WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ paille', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        // on vérifie s'il peut commencer la construction du nid
        verifierIngredientsNid($hamster_id) ;
    }
}
else if ($action == "donnerBrindilles") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_BRINDILLES) ;
    
    if ($accessoireIndex == -1) { // pas de brindilles en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['possede_brindilles']=1;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $query = "UPDATE hamster 
        SET possede_brindilles = 1    WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ brindilles', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        // on vérifie s'il peut commencer la construction du nid
        verifierIngredientsNid($hamster_id) ;
    }
}
else if ($action == "donnerFormationBatisseur") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_FORMATION_BATISSEUR) ;
    
    if ($accessoireIndex == -1) { // pas de formation en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['init_batisseur']=5;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $lst_hamsters[$hamsterIndex]['experience'] += 2;
        $query = "UPDATE hamster 
        SET init_batisseur = 5    ,
        experience = ".$lst_hamsters[$hamsterIndex]['experience']."
        WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ init_batisseur', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
    }
}
else if ($action == "sortieRugby") {
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_RUGBY_JEU) ;
    
    if ($jourDansLaSemaine != 6)
        $erreurAction = 3;
    else if ($accessoireIndex == -1) { // pas de sortie rugby en stock
        $erreurAction = 2 ;
    }
    else { 
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex2 = possedeAccessoire(ACC_BALLON_RUGBY) ;
        
        if ($accessoireIndex2 == -1) { // pas de ballon de rugby en stock
            $erreurAction = 1 ;
        }
        else { 
            
            // assez d'énergie
            $energieRestante = debiterEnergie($energieRugby,$hamsterIndex);
            if ($energieRestante == -1) {
                $erreurAction = 4;
            }
            else {
                        
                $lst_hamsters[$hamsterIndex]['puissance'] = seuillerForce($lst_hamsters[$hamsterIndex]['puissance'] + 1, $lst_hamsters[$hamsterIndex]['maxForce']) ;
                $lst_hamsters[$hamsterIndex]['moral'] = seuillerMoral($lst_hamsters[$hamsterIndex]['moral']+1,$lst_hamsters[$hamsterIndex]['maxMoral']);
                $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
                $nouveauPoids = ameliorerPoids(2,$lst_hamsters[$hamsterIndex]['poids']) ;
                $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;
                $lst_hamsters[$hamsterIndex]['experience'] += 2;
                $query = "UPDATE hamster 
                SET puissance = ".$lst_hamsters[$hamsterIndex]['puissance']."    ,
                moral = ".$lst_hamsters[$hamsterIndex]['moral'].",
                poids = ".$lst_hamsters[$hamsterIndex]['poids'].",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster _ rugby', '', __LINE__, __FILE__, $query);
                }
                reduireQuantiteAccessoire($accessoire_id,1) ;
            }
        }
    }
}
else if ($action == "sortieFoot") {
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_FOOT_JEU) ;
        
    if ($jourDansLaSemaine != 3 && $jourDansLaSemaine != 0)
        $erreurAction = 3;
    else if ($accessoireIndex == -1) { // pas de sortie foot en stock
        $erreurAction = 2 ;
    }
    else { 
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex2 = possedeAccessoire(ACC_BALLON_FOOT) ;
        
        if ($accessoireIndex2 == -1) { // pas de ballon de foot en stock
            $erreurAction = 1 ;
        }
        else {
            // assez d'énergie
            $energieRestante = debiterEnergie($energieFoot,$hamsterIndex);
            if ($energieRestante == -1) {
                $erreurAction = 4;
            }
            else {
                
                $lst_hamsters[$hamsterIndex]['puissance']  = seuillerForce($lst_hamsters[$hamsterIndex]['puissance'] + 0.5, $lst_hamsters[$hamsterIndex]['maxForce']) ;
                $lst_hamsters[$hamsterIndex]['moral'] = seuillerMoral($lst_hamsters[$hamsterIndex]['moral'] + 0.5,$lst_hamsters[$hamsterIndex]['maxMoral']);
                $nouveauPoids = ameliorerPoids(2,$lst_hamsters[$hamsterIndex]['poids']) ;
                $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;
                $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
                $lst_hamsters[$hamsterIndex]['experience'] += 2;
                $query = "UPDATE hamster 
                SET puissance = '".$lst_hamsters[$hamsterIndex]['puissance']."',
                moral = '".$lst_hamsters[$hamsterIndex]['moral']."',
                poids = ".$lst_hamsters[$hamsterIndex]['poids'].",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster _ foot', '', __LINE__, __FILE__, $query);
                }
                reduireQuantiteAccessoire($accessoire_id,1) ;
            }
        }
    }
}
else if ($action == "partieCatch") {
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_TENUE_CATCH) ;
        
    if ($jourDansLaSemaine != 1 && $jourDansLaSemaine != 4)
        $erreurAction = 3;
    else if ($accessoireIndex == -1) { // pas de masque en stock
        $erreurAction = 1 ;
    }
    else { 
        // assez d'énergie
        $energieRestante = debiterEnergie($energieCatch,$hamsterIndex);
        if ($energieRestante == -1) {
            $erreurAction = 2;
        }
        else {
            $lst_hamsters[$hamsterIndex]['puissance']  = seuillerForce($lst_hamsters[$hamsterIndex]['puissance'] + 1, $lst_hamsters[$hamsterIndex]['maxForce']) ;
            $lst_hamsters[$hamsterIndex]['beaute']  = seuillerBeaute($lst_hamsters[$hamsterIndex]['beaute'] - 0.5, $lst_hamsters[$hamsterIndex]['maxBeaute']) ;
            $lst_hamsters[$hamsterIndex]['moral'] = seuillerMoral($lst_hamsters[$hamsterIndex]['moral'] + 0.5,$lst_hamsters[$hamsterIndex]['maxMoral']);
            $nouveauPoids = ameliorerPoids(2,$lst_hamsters[$hamsterIndex]['poids']) ;
            $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            $lst_hamsters[$hamsterIndex]['experience'] += 2;
            $query = "UPDATE hamster 
            SET puissance = '".$lst_hamsters[$hamsterIndex]['puissance']."',
            beaute = '".$lst_hamsters[$hamsterIndex]['beaute']."',
            moral = '".$lst_hamsters[$hamsterIndex]['moral']."',
            poids = ".$lst_hamsters[$hamsterIndex]['poids'].",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ catch' , __LINE__, __FILE__, $query);
            }
        }
    }
}
else if ($action == "sortieTennis") {
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_TENNIS_JEU) ;
        
   if ($accessoireIndex == -1) { // pas de sortie tennis en stock
        $erreurAction = 2 ;
    }
    else { 
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex2 = possedeAccessoire(ACC_RAQUETTE_TENNIS) ;
        $accessoireIndex3 = possedeAccessoire(ACC_BALLES_TENNIS) ;
        
        if ($accessoireIndex2 == -1 || $accessoireIndex3 == -1) { // pas de raquette de tennis en stock ou pas de balles
            $erreurAction = 1 ;
        }
        else {
            // assez d'énergie
            $energieRestante = debiterEnergie($energieTennis,$hamsterIndex);
            if ($energieRestante == -1) {
                $erreurAction = 4;
            }
            else {
                
                $lst_hamsters[$hamsterIndex]['puissance']  = seuillerForce($lst_hamsters[$hamsterIndex]['puissance'] + 0.5, $lst_hamsters[$hamsterIndex]['maxForce']) ;
                $lst_hamsters[$hamsterIndex]['moral'] = seuillerMoral($lst_hamsters[$hamsterIndex]['moral'] + 0.5,$lst_hamsters[$hamsterIndex]['maxMoral']);
                $nouveauPoids = ameliorerPoids(2,$lst_hamsters[$hamsterIndex]['poids']) ;
                $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;
                $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
                $lst_hamsters[$hamsterIndex]['experience'] += 2;
                $query = "UPDATE hamster 
                SET puissance = '".$lst_hamsters[$hamsterIndex]['puissance']."',
                moral = '".$lst_hamsters[$hamsterIndex]['moral']."',
                poids = ".$lst_hamsters[$hamsterIndex]['poids'].",
                experience = ".$lst_hamsters[$hamsterIndex]['experience']."
                WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster _ foot', '', __LINE__, __FILE__, $query);
                }
                reduireQuantiteAccessoire($accessoire_id,1) ;
            }
        }
    }
}
else if ($action == "vendreCaisseCocos"){

    $caisseCocoIndex = possedeAccessoire(ACC_CAISSE_COCO);
    $caisseCocoId = $lst_accessoires[$caisseCocoIndex]['accessoire_id'] ;
    $erreurAction = 1;
    
    if ($caisseCocoId != -1) {
        
        $nbCocos = getNbAccessoires(ACC_NOIX_COCO);
        if ($nbCocos >= 20) {
            
            reduireQuantiteAccessoire($caisseCocoId,1);
            $query = "DELETE FROM accessoires WHERE type=".ACC_NOIX_COCO." AND joueur_id=".$userdata['joueur_id']." LIMIT 10";
             if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ foot', '', __LINE__, __FILE__, $query);
            }
            crediterPieces(30,false);
            $erreurAction = 0;
        }
    }   
}
else if ($action == "peindre") {
    
    if (isset($_GET['couleur'])){
        $couleurCage = intval($_GET['couleur'] );
        
        $cout = $coutPeinture;
        if ($couleurCage == FOND_CAGE_COEUR)
            $cout = $coutPeintureCoeur;
        else if ($couleurCage == FOND_CAGE_HALLOWEEN)
            $cout = $coutPeintureHalloween;
        else if ($couleurCage == FOND_CAGE_MER)
            $cout = $coutPeintureMer;
            
        $resultatAchat = debiterPieces($cout,"nouvelle peinture pour la cage") ;
        if ($resultatAchat == 1) {
            
            
            $cage_index = cageCorrespondante($cage_id) ;
            $lst_cages[$cage_index]['fond'] = $couleurCage ;
            $query = "UPDATE cage 
            SET fond = ".$couleurCage." WHERE cage_id=".$cage_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating cage color', '', __LINE__, __FILE__, $query);
            }
            setCageToBeRebuilt($cage_id);
        }
        else {
            $erreurAction = 1;
            $msgTmp = T_("Il te manque ").$cout." ".IMG_PIECE.T_(" pour payer la peinture.");
            $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_CAGE);
        }
    }
}
else if ($action == "caresser") {
    
    // on verifie que les precedentes caresses n'ont pas ete faites trop recemment
    if ($dateActuelle - ($intervalleEntreDeuxCaresses*3600) < $lst_hamsters[$hamsterIndex]['dernieres_caresses'] ){
        $erreurAction = 1;
    }
    else  { 
        $nouveauMoral = $lst_hamsters[$hamsterIndex]['moral'] + 1;
        $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamsterIndex]['maxMoral']) ;
        $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral ;
        $lst_hamsters[$hamsterIndex]['dernieres_caresses'] = $dateActuelle;
        $lst_hamsters[$hamsterIndex]['experience'] ++;
        
        $query = "UPDATE hamster 
            SET moral = '".$nouveauMoral."',
            dernieres_caresses = ".$dateActuelle.",
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ moral', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "deposerRefuge") {
    $resultatAchat = debiterPieces($coutRefuge, T_("depot des hamsters au refuge")) ;
    if ($resultatAchat == 1) {
        
        // tous les hamsters du joueur passent en mode pause et on enregistre la date/heure de pause
        for($hamster=0;$hamster<$nbHamsters;$hamster++) {
            $lst_hamsters[$hamster]['pause'] = $dateActuelle;
        }
        $query = "UPDATE hamster 
            SET pause = ".$dateActuelle." WHERE joueur_id=".$userdata['joueur_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster pause', '', __LINE__, __FILE__, $query);
        }
    }
    else {
        $erreurAction = 1;
        $msgTmp = T_("Tu n'as pas assez d'argent pour déposer tes hamsters au refuge.");
        $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_CAGE);
    }
}
else if ($action == "recupererRefuge") {
    // tous les hamsters du joueur sortent du mode pause (et on les nourrit !)
    for($hamster=0;$hamster<$nbHamsters;$hamster++) {
        $lst_hamsters[$hamster]['pause'] = 0;        
        $lst_hamsters[$hamster]['dernier_repas'] = $dateActuelle;
    }
    $query = "UPDATE hamster 
        SET pause = 0 ,
        dernier_repas = ".$dateActuelle."
        WHERE joueur_id=".$userdata['joueur_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster pause', '', __LINE__, __FILE__, $query);
    }
    // on fixe la date du dernier salaire à aujourd'hui
    $userdata['date_dernier_salaire'] = $dateActuelle;
    $userdata['date_maj_hamster'] = $dateActuelle;    
    $query = "UPDATE joueurs SET 
        date_maj_hamster = ".$dateActuelle.",
        date_dernier_salaire = ".$dateActuelle." WHERE joueur_id=".$userdata['joueur_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}
else if ($action == "donnerBois") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_BOIS) ;
    
    if ($accessoireIndex == -1) { // pas de bois en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['possede_bois']=1;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $query = "UPDATE hamster 
        SET possede_bois = 1    WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ bois', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        // on vérifie s'il peut commencer la construction de la cabane
        verifierIngredientsCabane($hamster_id) ;
    }
}
else if ($action == "donnerPlanCabane") {
    
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_PLAN_CABANE) ;
    
    if ($accessoireIndex == -1) { // pas de bois en stock
        $erreurAction = 1 ;
    }
    else { 
        $lst_hamsters[$hamsterIndex]['possede_plan_cabane']=1;
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $lst_hamsters[$hamsterIndex]['experience'] ++;
        $query = "UPDATE hamster 
            SET possede_plan_cabane = 1    ,
            experience = ".$lst_hamsters[$hamsterIndex]['experience']."
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ cabane', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        // on vérifie s'il peut commencer la construction de la cabane
        verifierIngredientsCabane($hamster_id) ;
    }
}
else if ($action == "donnerSalade") {
    
        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_SALADE) ;
        
        if ($accessoireIndex == -1) { // pas de salade en stock
            $erreurAction = 1 ;
        }
        else { // peut prendre des bonbons
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 0.2;
            $nouvelleSante = $lst_hamsters[$hamster_index]['sante'] + 0.2;
            $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
            $nouvelleSante = seuillerSante($nouvelleSante,$lst_hamsters[$hamster_index]['maxSante']) ;            
            $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
            $lst_hamsters[$hamster_index]['sante'] = $nouvelleSante ;
            $lst_hamsters[$hamster_index]['experience'] ++;
            $query = "UPDATE hamster 
                SET moral = '".$nouveauMoral."' ,
                sante = '".$nouvelleSante."',
                experience = ".$lst_hamsters[$hamster_index]['experience']."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ salade', '', __LINE__, __FILE__, $query);
            }
            $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        }
}
else if ($action == "bouleRose" || $action == "bouleNoire") {
        
    $hamsterCorrespondant = hamsterCorrespondant($hamster_id) ;
    $hamRow = $lst_hamsters[$hamsterCorrespondant];
    
    if ($jourDansLaSemaine != 1 && $jourDansLaSemaine != 4)
        $erreurAction = 3;
    else  {
        $energieRestante = debiterEnergie($energieBoule,$hamsterIndex);
        if ($energieRestante == -1) {
            $erreurAction = 1;
        }
        else {    
    
            // on verifie que le joueur possede bien l'accessoire
            $accessoireIndex = possedeAccessoire(ACC_BOULE_ROSE) ;
            if ($accessoireIndex == -1)
                $accessoireIndex = possedeAccessoire(ACC_BOULE_NOIRE) ;
            
            if ($accessoireIndex == -1) { // pas de boules
                $erreurAction = 2 ;
            }
            else {  // peut faire de la boule
                $precedenteForce = $hamRow['puissance'] ;
                $precedentMoral = $hamRow['moral'] ;
                // concours
                $nouvelleForce  = seuillerForce($precedenteForce + 0.5, $hamRow['maxForce'])  ;
                $nouveauMoral  = seuillerMoral($precedentMoral + 0.5,$hamRow['maxMoral']) ;
                $lst_hamsters[$hamsterCorrespondant]['puissance'] = $nouvelleForce;
                $lst_hamsters[$hamsterCorrespondant]['moral'] = $nouveauMoral;
                $lst_hamsters[$hamsterCorrespondant]['experience'] ++;
                
                $nouveauPoids = ameliorerPoids(1,$hamRow['poids']) ;
                $lst_hamsters[$hamsterCorrespondant]['poids'] = $nouveauPoids;
                
                $query = "UPDATE hamster 
                    SET puissance = '".$nouvelleForce."',
                    moral = '". $nouveauMoral."',
                    experience = ".$lst_hamsters[$hamsterCorrespondant]['experience'].",
                    poids = ".$nouveauPoids."
                    WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                }
            }
        }
    }
}
else if ($action == "promenerHamster") {
        
    $hamRow = $lst_hamsters[$hamsterIndex];
    $infoAction = 0;    
    
    // assez d'énergie
    $energieRestante = debiterEnergie($energiePromenade,$hamsterIndex);
    if ($energieRestante == -1) {
        $erreurAction = 1;
    }
    else {    
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_LAISSE) ;
        if ($accessoireIndex == -1)
            $accessoireIndex = possedeAccessoire(ACC_LAISSE_ROSE) ;
        
        if ($accessoireIndex == -1) { // pas de laisse
            $erreurAction = 2 ;
        }
        else {  // peut faire une promenade
            
            // resultat de la promenade
             $evenement = rand(1,16);
            
            $nouveauMoral = $hamRow['moral'] ;
            $nouvelleSante = $hamRow['sante'] ;
            $nouvelleForce = $hamRow['puissance'] ;
            $dernier_repas = $hamRow['dernier_repas'] ;
            
            if ($userdata['niveau'] == NIVEAU_CABANE_BEBE_AVATAR && possedeAccessoire(ACC_BOIS) == -1) {
                $infoAction = 4;
                ajouterAccessoireDansBDD(ACC_BOIS,$userdata['joueur_id'],-1,-1,-1,-1,1);
            }
            else if ($userdata['niveau'] == NIVEAU_EVASION) {
                $infoAction = 6;
                updateNiveauJoueur(1,true);
            }
            else if ($userdata['niveau'] == NIVEAU_TENNIS) {
                $infoAction = 7;
                updateNiveauJoueur(0,true);
            }
            else {
                if ($evenement < 4) { // il a trouvé des glands qu'il mange
                    $infoAction = 1;
                    $dernier_repas = time();
                    $nouveauMoral  = seuillerMoral($nouveauMoral + 0.5,$hamRow['maxMoral']) ;
                }
                else if ($evenement == 4) { // il a chopé une maladie
                    $infoAction = 2;
                    if ($nouvelleSante > 0.5)
                        $nouvelleSante = max(0.5,$nouvelleSante-3);
                }
                else if ($evenement == 16) { // il a trouvé du bois
                    
                    if ($userdata['niveau'] >= NIVEAU_CABANE_BEBE_AVATAR){
                        $infoAction = 4;
                        ajouterAccessoireDansBDD(ACC_BOIS,$userdata['joueur_id'],-1,-1,-1,-1,1);
                    }
                    else
                         $infoAction = 5; // = il a trouvé du bois mais ne peut pas le ramener
                }
                else { // promenade classique
                    $infoAction = 3;
                    $nouveauMoral  = seuillerMoral($nouveauMoral + 0.5,$hamRow['maxMoral']) ;
                    $nouvelleForce = seuillerForce($nouvelleForce + 0.3,$hamRow['maxForce']) ;
                }
            }
            
            $lst_hamsters[$hamsterIndex]['puissance'] = $nouvelleForce;
            $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral;
            $lst_hamsters[$hamsterIndex]['sante'] = $nouvelleSante;
            $lst_hamsters[$hamsterIndex]['dernier_repas'] = $dernier_repas;
            $lst_hamsters[$hamsterIndex]['experience'] ++;
            
            $query = "UPDATE hamster 
                SET moral = '". $nouveauMoral."',
                sante = '". $nouvelleSante."',
                puissance = '". $nouvelleForce."',
                experience = ".$lst_hamsters[$hamsterIndex]['experience'].",
                dernier_repas = ".$dernier_repas."
                WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }
        }
    }
}
else if ($action == "envoyerMessage") {

    // on récupère l'id du destinataire
    $dest_joueur_id = -1;
    
    // soit l'id a été précisé
    if (isset($_POST['messageDestJoueurId'])) {
        $dest_joueur_id = intval($_POST['messageDestJoueurId']);
    }
    // soit le pseudo a été précisé
    else if (isset($_POST['pseudoDestinataire'])) {
        
        // on recherche l'id équivalent
        $pseudo = $_POST['pseudoDestinataire'] ;
        $dest_joueur_id = getJoueurIdFromPseudo(mysql_real_escape_string($pseudo));
        if ($dest_joueur_id == -1) {
            $msg .= str_replace("#1",$pseudo,T_("<div>Désolé, il n'y a pas de joueur avec le pseudo #1. Vérifie l'orthographe...</div>"));
            $erreurAction = 1;
        }
    }
            
    // si l'ID est bien valide
    if ($dest_joueur_id != -1) {
        // on récupère le texte
        if (isset($_POST['messageTxt'])) {
            $messageTxt = mysql_real_escape_string(stripslashes($_POST['messageTxt']));
            
            // on enlève "ton message ici"  que les joueurs ont tendance à laisser
            if (strncmp($messageTxt, T_("Ton message ici ..."),19) == 0 ) {
                $messageTxt = substr($messageTxt,19);
            }
            $messageTxt = utf8_encode($messageTxt);
            
            $resultat = envoyerMessagePrive($dest_joueur_id, $messageTxt, $userdata['joueur_id']);
            $msg .= "<div>".T_("Ton message a bien été envoyé !")."</div>";
        }
        else
            $erreurAction = 2;
    }
}
else if ($action == "supprimerCage") {
    
}
else if ($action == "donnerChocolatAvent") {
    
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_CALENDRIER_CHOCOLAT) ;
        
        if ($accessoireIndex == -1) { // pas de chocolat en stock
            $erreurAction = 1 ;
        }
        else { // peut donner un chocolat
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            $nouveauMoral = seuillerMoral($lst_hamsters[$hamsterIndex]['moral'] + 0.3,$lst_hamsters[$hamsterIndex]['maxMoral']);
            $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral ;
            $query = "UPDATE hamster 
            SET moral = '".$nouveauMoral."' 
            WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
            }
            reduireQuantiteAccessoire($accessoire_id,1) ;
        }
}
else if ($action == "abandonHamster") {
    
    // on vérifie que ce n'est pas le dernier hamster (il en faut au moins 1 par joueur)
    if ($nbHamsters == 1){
        $erreurAction = 2;
    }
    else {
        $resultat = debiterPieces($coutAbandonHamster, "abandon de hamster") ;
        
        if ($resultat == 1) { // si le joueur a assez d'argent
                // suppression des hamsters
            $query = "DELETE FROM hamster
                WHERE hamster_id=".$hamster_id;    
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error deleting hamster ', '', __LINE__, __FILE__, $query);
            }    
            $hamster_id = -1;
            redirectHT("jeu.php?mode=m_accueil");
        }
        else {
            $erreurAction = 1;
        }
    }
}
else if ($action == "vendreHamster") {
    
    // on vérifie que ce n'est pas le dernier hamster (il en faut au moins 1 par joueur)
    if ($nbHamsters == 1){
        $erreurAction = 2;
    }
    else {
        
        // on vérifie que le hamster n'est pas malade et suffisamment agé (3 semaines)
        if (($dateActuelle - $lst_hamsters[$hamsterIndex]['date_naissance']) > 1814400 && $lst_hamsters[$hamsterIndex]['sante'] > 0) {
        
            crediterPieces($prixVenteHamster, false) ;
        
            // suppression des hamsters
            $query = "DELETE FROM hamster
                WHERE hamster_id=".$hamster_id;    
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error deleting hamster ', '', __LINE__, __FILE__, $query);
            }    
            $hamster_id = -1;
            redirectHT("jeu.php?mode=m_accueil");
        }
        else {
            $erreurAction = 1;
        }
    }
}
else if ($action == "inscrireConcours") {
    
    if ($userdata['inscritConcours']== 0) { // on vérifie que le joueur n'est pas déjà inscrit
    
        // on vérifie que son compte est activé
        if (0 && $userdata['email_active']== 0) {
            $msg .= "<span class=\"txtErreur\">".T_("Il reste 1 étape pour finir l'inscription au concours.")."</span> ".T_("Pour participer au concours, tu dois d'abord valider ton adresse email. Pour cela, tu dois aller dans")." <a href=\"options.php\" title=\"".T_("Pour activer son compte/email")."\">".T_("Mon compte")."</a> ".T_("et cliquer sur le bouton <strong>Activer maintenant !</strong> Et reviens-vite t'inscire ;-) <br/>&nbsp;<br/>(rassure-toi, tes 5 pièces ne sont pas encore débitées.)");
        }
        else {
            $resultatAchat = debiterPieces(20, "inscription au concours") ;
            if ($resultatAchat == 1) {
                
                $userdata['inscritConcours'] = 1;//$userdata['note'];
                
                $query = "UPDATE joueurs 
                    SET inscritConcours = ".$userdata['inscritConcours']." WHERE joueur_id=".$userdata['joueur_id'];
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating bd', '', __LINE__, __FILE__, $query);
                }
            }
            else {
                $msgTmp = T_("Tu n'as pas assez d'argent pour t'inscrire au concours");
                $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_ACCUEIL);
            }
        }
    }
}
else if ($action == "inscrireConcoursFlash") {
    
    $query = "INSERT INTO lst_joueurs_concours VALUES (".$userdata['joueur_id'].",0,'0','".$userdata['pseudo']."','')";
    $dbHT->sql_query($query);
    
}
else if ($action == "inscrireCageConcours") { // désactivé
    
    if ($userdata['inscritConcours'] > 0) { // on vérifie que le joueur est déjà inscrit
    
        $userdata['inscritConcours'] = $cage_id;
            
        $query = "UPDATE joueurs 
            SET inscritConcours = ".$cage_id." WHERE joueur_id=".$userdata['joueur_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating bd', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "consolejeu") {                                                          
                                              
    $hamRow = $lst_hamsters[$hamsterIndex];                                                                    
                                                                                                                    
    if ($jourDansLaSemaine != 3 && $jourDansLaSemaine != 0)                                                         
        $erreurAction = 3;
    else  {
        // assez d'énergie
        $energieRestante = debiterEnergie($energieConsole,$hamsterIndex);
        if ($energieRestante == -1) {
            $erreurAction = 1;
        }
        else {
            
            // on verifie que le joueur possede bien l'accessoire                                                         
            $accessoireIndex = possedeAccessoire(ACC_CONSOLE_JEU) ;                                             
                                                                                                                          
            if ($accessoireIndex == -1) { // pas de console                                                                
                $erreurAction = 2 ;                                                                                          
            }                                                                                                             
            else {  // peut faire de la console                                                                           
                $precedentMoral = $hamRow['moral'] ;                                                       
                $nouveauMoral  = seuillerMoral($precedentMoral + 1,$hamRow['maxMoral']) ;
                $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral;                                              
                $lst_hamsters[$hamsterIndex]['experience'] ++;
                                                                                                                    
                $query = "UPDATE hamster                                                                                    
                    SET moral = ". $nouveauMoral.", 
                    experience = ".$lst_hamsters[$hamsterIndex]['experience']."                            
                    WHERE hamster_id=".$hamster_id;                                                                           
                if ( !($dbHT->sql_query($query)) ){                                                                         
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);                     
                }
            }
        } 
    }
}
else if ($action == "donnerPartGalette") {

        $hamster_index = hamsterCorrespondant($hamster_id) ;
        
        // on verifie que le joueur possede bien l'accessoire
        $accessoireIndex = possedeAccessoire(ACC_GALETTE_ROIS) ;
        
        if ($accessoireIndex == -1) { // pas de galette en stock
            $accessoireIndex = possedeAccessoire(ACC_PART_GALETTE) ;
            if ($accessoireIndex == -1) { // pas de part de galette en stock
                $erreurAction = 2 ;
            }
        }
        if ($accessoireIndex != -1) { // peut prendre une part
            $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
            
            // une chance sur 6 d'avoir la fève
            if (rand(1,6) == 1) {
                $erreurAction = 1;
                crediterPieces(10,false);
                $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 2;
            }
            else
                $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 1;
            
            $nouveauPoids = $lst_hamsters[$hamster_index]['poids'] + 3;
            $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
            $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
            $lst_hamsters[$hamster_index]['poids'] = $nouveauPoids ;
            $query = "UPDATE hamster 
            SET moral = ".$nouveauMoral." ,
            poids = ".$nouveauPoids."
            WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
            }
            reduireQuantiteAccessoire($accessoire_id,1) ;
        }
}
else if ($action == "donnerFioleEnergie") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_FIOLE) ;
    
    if ($accessoireIndex != -1) { // peut donner la fiole
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        
        crediterEnergie($energieFiole,$hamster_index);
        reduireQuantiteAccessoire($accessoire_id,1) ;
    }
}
else if ($action == "assembler") {
    
    // on récupère l'indice dans lst_accessoires des 2 objets à assembler
    if (isset($_GET['objet1'])) {
        $objet1 = intval($_GET['objet1']);
        $objet1_infos = $lst_accessoires[$objet1];
        
        if (isset($_GET['objet2'])) {
            $objet2 = intval($_GET['objet2']);
            $objet2_infos = &$lst_accessoires[$objet2];
            
            // assemblage roue + engrenage
            if ($objet1_infos['type'] == ACC_ENGRENAGE && $objet2_infos['type'] == ACC_ROUE3) {
                
                $erreurAction = 1;
                
                // on supprime l'engrenage et on remplace la roue par roue+engrenage
                reduireQuantiteAccessoire($objet1_infos['accessoire_id'],1) ;
                $lst_accessoires[$objet2] = changerTypeAccessoire($objet2_infos['accessoire_id'], ACC_ROUE_AVEC_ENGRENAGE) ;
            }
            // assemblage roue/engrenage + pile
            if ($objet1_infos['type'] == ACC_PILE && $objet2_infos['type'] == ACC_ROUE_AVEC_ENGRENAGE) {
                
                $erreurAction = 2;
                
                // on supprime la pile et on ajoute la pile
                reduireQuantiteAccessoire($objet1_infos['accessoire_id'],1) ;
                $lst_accessoires[$objet2] = changerTypeAccessoire($objet2_infos['accessoire_id'], ACC_ROUE_AVEC_ENGRENAGE_PILE) ;
            }
            // assemblage roue/engrenage/pile + fil
            if ($objet1_infos['type'] == ACC_FIL && $objet2_infos['type'] == ACC_ROUE_AVEC_ENGRENAGE_PILE) {
                
                $erreurAction = 3;
                
                // on ajoute le fil
                $lst_accessoires[$objet2] = changerTypeAccessoire($objet2_infos['accessoire_id'], ACC_ROUE_AVEC_ENGRENAGE_PILE_FIL) ;
                
                if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12) {
                    updateNiveauJoueur(0,true) ;
                }
            }            
        }
    }
}
else if ($action == "vendreCage") {

    // on vérifie que cage_id ne contient pas de hamster
    $query = "SELECT COUNT(hamster_id) as hamster_id FROM hamster WHERE cage_id = ".$cage_id." LIMIT 1";
    $result = $dbHT->sql_query($query);
    $row = $dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);    
    
    if ($row[0] > 0) {
        $erreurAction = 1;
    }
    else {
            // on vérifie que cage_id ne contient pas de objet
            $query = "SELECT COUNT(accessoire_id) as accessoire_id FROM accessoires WHERE cage_id = ".$cage_id;
            $result = $dbHT->sql_query($query);
            $row = $dbHT->sql_fetchrow($result);
            $dbHT->sql_freeresult($result);    
            
            if ($row[0] > 0) {
                $erreurAction = 2;
            }
            else {
                    // on supprime la cage
                    $cage_index = cageCorrespondante($cage_id) ;
                    if ($cage_index != -1){
                        $cageRow = $lst_cages[$cage_index];
                        $coutVente = intval($coutCube * $cageRow['colonnes'] * $cageRow['profondeurs'] / 2);
                        crediterPieces($coutVente,false);
                        $query = "DELETE FROM cage WHERE cage_id = ".$cage_id;
                        $dbHT->sql_query($query);
                        redirectHT("jeu.php?mode=m_cage");
                    }
            }
    }
}
else if ($action == "selectionnerCageConcours") {
    $query = "UPDATE joueurs SET inscritConcours = ".$cage_id. " WHERE joueur_id = ".$userdata['joueur_id'];
    $dbHT->sql_query($query);
    $userdata['inscritConcours'] = $cage_id;
}
else if ($action == "corrigerPlacements" ) {
 
    for($acc=0;$acc<$nbAccessoires;$acc++) {
        
        $accessoire_a_deplacer = $lst_accessoires[$acc];
        
        if ($accessoire_a_deplacer['cage_id'] == $cage_id) {
            
            $posX = $accessoire_a_deplacer['posX'] ; 
            
            $etage_courant = $accessoire_a_deplacer['etage'] ;
            $posY = $accessoire_a_deplacer['posY'] ; 

            $accWidth = $accessoire_a_deplacer['img_width'] * $accessoire_a_deplacer['echelle'];
            $accHeight = $accessoire_a_deplacer['img_height'] * $accessoire_a_deplacer['echelle'];
            $accBaseY = $accessoire_a_deplacer['decal_y'] * $accessoire_a_deplacer['echelle'];
    
            $cageRow = $lst_cages[cageCorrespondante($cage_id)] ;
            
            corrigePositionAcccessoire($posX,$posY,$etage_courant, $cageRow, $accWidth, $accHeight,$accBaseY);
            
            $lst_accessoires[$acc]['posX'] = $posX;
            $lst_accessoires[$acc]['posY'] = $posY;
            $lst_accessoires[$acc]['etage'] = $etage_courant;

            // mise à jour de la colonne + profondeur + étage
            $query = "UPDATE accessoires SET posX = ".$posX.",
                    etage = ".$etage_courant.",     
                    posY=".$posY." 
                    WHERE accessoire_id=".$accessoire_a_deplacer['accessoire_id'];
                    
            if ( !($result = $dbHT->sql_query($query)) )
                message_die(GENERAL_ERROR, 'Errorin obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            
            setCageToBeRebuilt($cage_id);
        }
    }
    
    // pour la mission de décoration chinoise, on compte le nbre d'objets chinois
    if ($userdata['niveau'] == NIVEAU_CHINE){
    
        for($etage=0;$etage<$lst_cages[$cageIndex]['etages'];$etage++) {
            
            $nbAccessoiresChinoix = 0;
            for($acc=0;$acc<$nbAccessoires;$acc++) {
                if ($lst_accessoires[$acc]['danslacage'] != 1 || $lst_accessoires[$acc]['cage_id'] != $lst_cages[$cageIndex]['cage_id'] || $lst_accessoires[$acc]['etage'] != $etage)
                    continue;
                if ($lst_accessoires[$acc]['type'] >= 109 && $lst_accessoires[$acc]['type'] <= 117)
                    $nbAccessoiresChinoix++;
            }
            
            if ($nbAccessoiresChinoix >= 10){
                updateNiveauJoueur(0,true) ;
                updateNiveauJoueur(1,true) ; // objectifs terminés
                break;
            }
        }
    }
}
else if ($action == "ingredientTrouve") {
    
    // pouyr la mission de la recette de cuisine (soupe)
    if (isset($_GET['ingredient'])) {
        $ingredient = intval($_GET['ingredient']) ;
        
        for($ingredientIndex=0;$ingredientIndex<sizeof($lstIngredients);$ingredientIndex++){
            if ($lstIngredients[$ingredientIndex] == $ingredient) {
                // le joueur a trouvé un ingredient
                $msg .= "<div><img src=\"images/ingredients/ingredient".$ingredientIndex.".gif\" align=absmiddle> ".T_("Tu as trouvé un ingrédient")." ! </div>";
                if ($userdata['niveau']==NIVEAU_POTAGE) {
                    updateNiveauJoueur($ingredientIndex,true) ;
                }                
                break;
            }            
        }
    }
}
else if($action == "faireRecettePotage") {

   if ($userdata['niveau']==NIVEAU_POTAGE) {
       $msg .= "<div><img src=\"images/ingredients/marmite.gif\" /> ".T_("Le Potage est prêt ! Mmmm... il sent super bon les noisettes, la châtaigne ! Donne-en à ton hamster !</div>");
       updateNiveauJoueur(7,true) ;
   }
}
else if($action == "donnerPotageRongeur") {

   if ($userdata['niveau']==NIVEAU_POTAGE) {
       
       if (possedeAccessoire(ACC_LOUCHE) == -1) {
           $msg .= "<div><img src=\"images/ingredients/louche.gif\" /> ".T_("Tu vas avoir besoin d'une louche pour donner le potage au hamster !</div>");
       }
       else {
           nourrirHamster($hamster_id);
           $hamsterCorrespondant = hamsterCorrespondant($hamster_id) ;
           $nouveauMoral = $lst_hamsters[$hamsterCorrespondant]['moral'] + 2;
           $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamsterCorrespondant]['maxMoral']) ;
           $lst_hamsters[$hamsterCorrespondant]['moral'] = $nouveauMoral ;
           $lst_hamsters[$hamsterCorrespondant]['experience'] ++;
            
           $query = "UPDATE hamster 
                SET moral = ".$lst_hamsters[$hamsterCorrespondant]['moral'].",
                experience = ".$lst_hamsters[$hamsterCorrespondant]['experience']."
                WHERE hamster_id=".$hamster_id;
           if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ sante', '', __LINE__, __FILE__, $query);
           }
           
           $msg .= "<div><img src=\"images/ingredients/marmite.gif\" /> ".T_("Divin, tous ces parfums de châtaigne, noisette ... tous les rongeurs en sont fans, et ton hamster ne déroge pas à la règle : il adore ça !!! Son moral est en hausse et il n'a plus faim !")."</div><br/>&nbsp;<br/>";
           updateNiveauJoueur(8,true) ;
       }
   }
}
else if ($action == "TennisContreHammer") {
            
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex2 = possedeAccessoire(ACC_RAQUETTE_TENNIS) ;
    $accessoireIndex3 = possedeAccessoire(ACC_BALLES_TENNIS) ;
    
    if ($accessoireIndex2 == -1 || $accessoireIndex3 == -1) { // pas de raquette de tennis en stock ou pas de balles
        $erreurAction = 1 ;
    }
    else {
        // assez d'énergie
        $energieRestante = debiterEnergie($energieTennisHammer,$hamsterIndex);
        if ($energieRestante == -1) {
            $erreurAction = 4;
        }
        else {

            // est-il assez fort pour battre Hammer ? 
            if ($lst_hamsters[$hamsterIndex]['moral'] < 22 || $lst_hamsters[$hamsterIndex]['puissance'] < 25 || $lst_hamsters[$hamsterIndex]['sante'] < 22) {
                $erreurAction = 2;
            }
            else {
                updateNiveauJoueur(1,true) ;
            }
        }
    }
}
else if ($action == "donnerMedicChorinthe") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_MEDIC_CHORINTHE) ;
    
    if ($accessoireIndex == -1 || $lst_hamsters[$hamster_index]['maladie'] != MALADIE_CHORINTHE) { // pas de medic en stock et le hamster a bien la chorinthe !
        $erreurAction = 1 ;
    }
    else { // peut prendre le médicament
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 1;
        $nouvelleSante = $lst_hamsters[$hamster_index]['sante'] + 2;
        $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
        $nouvelleSante = seuillerSante($nouvelleSante,$lst_hamsters[$hamster_index]['maxSante']) ;            
        $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
        $lst_hamsters[$hamster_index]['sante'] = $nouvelleSante ;
        $lst_hamsters[$hamster_index]['maladie'] = 0;
        $query = "UPDATE hamster 
            SET moral = '".$nouveauMoral."' ,
            sante = '".$nouvelleSante."',
            maladie = 0
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ salade', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        if ($userdata['niveau'] == NIVEAU_MALADIE_CHORINTHE)
            updateNiveauJoueur(0,true) ;
    }
}
else if ($action == "donnerMedicBraiseNoisette") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_MEDIC_BRAISE_NOISETTE) ;
    
    if ($accessoireIndex == -1 || $lst_hamsters[$hamster_index]['maladie'] != MALADIE_STALACTIC) { // pas de medic en stock et le hamster a bien la chorinthe !
        $erreurAction = 1 ;
    }
    else { // peut prendre le médicament
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 1;
        $nouvelleSante = $lst_hamsters[$hamster_index]['sante'] + 2;
        $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
        $nouvelleSante = seuillerSante($nouvelleSante,$lst_hamsters[$hamster_index]['maxSante']) ;            
        $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
        $lst_hamsters[$hamster_index]['sante'] = $nouvelleSante ;
        $lst_hamsters[$hamster_index]['maladie'] = 0;
        $query = "UPDATE hamster 
            SET moral = '".$nouveauMoral."' ,
            sante = '".$nouvelleSante."',
            maladie = 0
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ salade', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        if ($userdata['niveau'] == NIVEAU_MALADIE_STALACTIC)
            updateNiveauJoueur(0,true) ;
    }
}
else if ($action == "echangerPassPieces") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        crediterPieces($nbPiecesParAchatBanque,false); // on crédite (mais ce n'est pas un salaire => false)
        ajouterStats($userdata['joueur_id'],"echangePassPieces",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "echangerPassGrossesse") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        $hamsterConcerneId = intval($_GET['hamsterConcerne']);
        $query = "UPDATE hamster 
            SET sexe = 2 WHERE hamster_id=".$hamsterConcerneId; // on met "2" comme date de début de grossesse pour être certain que la grossesse est finie
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ grossesse', '', __LINE__, __FILE__, $query);
        }
        ajouterStats($userdata['joueur_id'],"echangerPassGrossesse",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "echangerPassCabane") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        $hamsterConcerneId = intval($_GET['hamsterConcerne']);
        $query = "UPDATE hamster 
            SET construction_cabane = 1 WHERE hamster_id=".$hamsterConcerneId; // on met "1" comme date de début de construction pour être certain que la cabane est finie
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ construction_cabane', '', __LINE__, __FILE__, $query);
        }
        ajouterStats($userdata['joueur_id'],"echangerPassCabane",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "echangerPassNid") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        $hamsterConcerneId = intval($_GET['hamsterConcerne']);
        $query = "UPDATE hamster 
            SET construction_nid = 1 WHERE hamster_id=".$hamsterConcerneId; // on met "1" comme date de début de construction pour être certain que le nid est fini
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ construction_nid', '', __LINE__, __FILE__, $query);
        }
        ajouterStats($userdata['joueur_id'],"echangerPassNid",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "echangerPassNiveauSuperieur") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        if ($userdata['niveau'] < $nbNiveaux-1) {
    
            $nbreObjectifs = sizeof ($infosNiveaux[$userdata['niveau']][NIVEAU_OBJECTIFS]);
            $tousobjectisfinis = $maskObjectifs[$nbreObjectifs] ;
            
            $query = "UPDATE joueurs
                SET niveau_objectif = ".$tousobjectisfinis." WHERE joueur_id=".$userdata['joueur_id']; 
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating joueur _ niveau', '', __LINE__, __FILE__, $query);
            }
        }
        ajouterStats($userdata['joueur_id'],"echangerPassNiveauSuperieur",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "offrirPass") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        
        $pseudo = mysql_real_escape_string($_GET['pseudoPassOffert']);
        $joueur_receveur_id = getJoueurIdFromPseudo($pseudo);
            
        if ($joueur_receveur_id == -1)
            $erreurAction = 2;
        else {
            
            ajouterPass($joueur_receveur_id);
            envoyerMessagePrive($joueur_receveur_id,"<img src=\"images/cadeauNoel.gif\" alt=\"\" style=\"vertical-align:bottom; \" /> ".addslashes(T_("(Message automatique) C'est ton jour de chance, tu as reçu un cadeau de la part de "))."<strong>".
                $userdata['pseudo']."</strong> ! ".
                addslashes(T_("Il t'offre un <i>Pass</i> ! Un Pass te permet d'avoir des pièces d'or ou de devenir VIP ou d'accélérer une construction, une grossesse ou de passer au niveau suivant ! Pour l'utiliser, va vite en <strong>Ville->La Banque</strong> ! N'oublie pas de le remercier !")),HAMSTER_ACADEMY_ID,true);
            supprimerPass($userdata['joueur_id']);
            ajouterStats($userdata['joueur_id'],"offrirPass",1,$dateActuelle);
        }
    }
}
else if ($action == "echangerPassVIP") {
    
    if ($userdata['nb_pass'] == 0) {
        $erreurAction = 1;
    }
    else {
        $query = "UPDATE joueurs
            SET vip = 1 WHERE joueur_id=".$userdata['joueur_id']; 
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating vip', '', __LINE__, __FILE__, $query);
        }
        ajouterStats($userdata['joueur_id'],"echangerPassVIP",1,$dateActuelle);
        supprimerPass($userdata['joueur_id']);
    }
}
else if ($action == "allumerEolienne") {
 
    $resultat = debiterPieces(1,"eolienne") ;
    if ($resultat == 1) {
        $query = "UPDATE cage SET eolienne_activee = ".$dateActuelle." WHERE cage_id=".$lst_cages[$cageIndex]['cage_id']; 
         $dbHT->sql_query($query);
         $lst_cages[$cageIndex]['eolienne_activee'] = $dateActuelle;
    }
}
else if ($action == "eteindreEolienne") {
 
     $query = "UPDATE cage SET eolienne_activee = 0 WHERE cage_id=".$lst_cages[$cageIndex]['cage_id']; 
     $dbHT->sql_query($query);
     $lst_cages[$cageIndex]['eolienne_activee'] = 0;
}
else if ($action == "prendreTemperature") {
    
    if ($lst_hamsters[$hamsterIndex]['moral'] < 5) {
        // moral insuffisant pour qu'il veuille prendre la température
        $erreurAction = 1;   
    }
    else{
        if ($userdata['niveau'] == NIVEAU_MALADIE_FIEVRE){
            $temperature = rand(39,40);
            updateNiveauJoueur(0,true);
        }
        else
            $temperature = 37;
    }
}
else if ($action == "donnerMedicMentheGlace") {
    
    $hamster_index = hamsterCorrespondant($hamster_id) ;
    
    // on verifie que le joueur possede bien l'accessoire
    $accessoireIndex = possedeAccessoire(ACC_MEDIC_MENTHE_GLACE) ;
    
    if ($accessoireIndex == -1 || $lst_hamsters[$hamster_index]['maladie'] != MALADIE_FIEVRE) { // pas de medic en stock et le hamster a bien la chorinthe !
        $erreurAction = 1 ;
    }
    else { // peut prendre le médicament
        $accessoire_id = $lst_accessoires[$accessoireIndex]['accessoire_id'];
        $nouveauMoral = $lst_hamsters[$hamster_index]['moral'] + 1;
        $nouvelleSante = $lst_hamsters[$hamster_index]['sante'] + 2;
        $nouveauMoral = seuillerMoral($nouveauMoral,$lst_hamsters[$hamster_index]['maxMoral']) ;
        $nouvelleSante = seuillerSante($nouvelleSante,$lst_hamsters[$hamster_index]['maxSante']) ;            
        $lst_hamsters[$hamster_index]['moral'] = $nouveauMoral ;
        $lst_hamsters[$hamster_index]['sante'] = $nouvelleSante ;
        $lst_hamsters[$hamster_index]['maladie'] = 0;
        $query = "UPDATE hamster 
            SET moral = '".$nouveauMoral."' ,
            sante = '".$nouvelleSante."',
            maladie = 0
            WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ salade', '', __LINE__, __FILE__, $query);
        }
        $infoAction = reduireQuantiteAccessoire($accessoire_id,1) ;
        
        if ($userdata['niveau'] == NIVEAU_MALADIE_CHORINTHE)
            updateNiveauJoueur(0,true) ;
        else if ($userdata['niveau'] == NIVEAU_MALADIE_FIEVRE)
            updateNiveauJoueur(1,true) ;
    }
}