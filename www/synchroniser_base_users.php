<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";

define('PUN_ROOT', './forum/');
require PUN_ROOT.'include/common.php';

$userdata = session_pagestart($user_ip);
    
if( ! $userdata['session_logged_in'] ) { 
    echo "Erreur de connection, veuillez contacter l'Hamster Academy si le problème persiste";
    redirectHT("index.php");
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hamster Academy - Synchroniser </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Hamster Academy, Eleve un hamster, Fabrique sa cage, Et vit à sa place, Jeu de role, jeu gratuit ">
<meta name="keywords" content="Hamster, Academy, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="icon" type="image/ico" href="/favicon.ico" />
<link rel="stylesheet" href="style.css" type="text/css">
<link rel="stylesheet" href="styleEleveur.css" type="text/css">

</head>

<body>

Synchronise .... 

<?php 

    // on liste tous les joueurs
    $query = "SELECT * FROM joueurs WHERE joueur_id NOT IN (-1,1) ";

    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueurs_data', '', __LINE__, __FILE__, $query);
    }
    $nbJoueurs = $dbHT->sql_numrows($result) ;
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        // on regarde si le joueur est inscrit dans la base punBB
        $query2 = "SELECT id FROM pun_users WHERE id=".$row['joueur_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining joueurs_data', '', __LINE__, __FILE__, $query2);
        }
        $nbJoueursPunBB = $dbHT->sql_numrows($result2) ;
        if ($nbJoueursPunBB == 0) {
            // le joueur n'est pas sur punBB : on ajoute son compte à punBB
            $password_hash = pun_hash($row['passwd']) ;
            $email_setting = 1;
            $save_pass = 1;
            $timezone = 0;
            $language = "French";
            $db->query('INSERT INTO '.$db->prefix.'users (id, username, group_id, password, email, email_setting, save_pass, timezone, language, style, registered, registration_ip, last_visit) VALUES(\''.$row['joueur_id'].'\',\''.$row['pseudo'].'\', 4, \''.$password_hash.'\', \''.$row['email'].'\', '.$email_setting.', '.$save_pass.', '.$timezone.' , \''.$db->escape($language).'\', \''.$pun_config['o_default_style'].'\', '.$dateActuelle.', \''.get_remote_address().'\', '.$dateActuelle.')') or error('Unable to create user', __FILE__, __LINE__, $db->error());
            
            echo "Joueur ".$row['pseudo']." ajouté à la base punBB.<br>";
        }
        else {
            echo "Joueur ".$row['pseudo']." déjà ajouté à la base punBB.<br>";
        }
        $dbHT->sql_freeresult($result2);
    }
    
    $dbHT->sql_freeresult($result);
    
?>


</body>
</html>

<?php $dbHT->sql_close(); ?>
