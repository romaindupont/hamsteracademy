<?php 
define('IN_HT', true);

//    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="refresh" content="3; url=jeu.php" /><title>Patientez svp ...</title></head><body>';
//    echo "<div align=center>Le jeu ne peut afficher aujourd'hui les options de compte, d'amis et de VIP. Ce sera rétablit dans la soirée.</div>" ;
//    echo '</body></html>';
//        
//    exit;

include "common.php";

include "lstMessages.php";
include "gestion.php";
include "gestionUser.php";
include "facebook.php";

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo "Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

$userdata = session_pagestart($user_ip);

$msg = "";
$action = "";
    
if( ! $userdata['session_logged_in'] ) { 
    echo "Erreur de connection";
    redirectHT("index.php",3);
}

$modeAffichage="m_profil";
if (isset($_POST['mode'])) 
    $modeAffichage=$_POST['mode'];
else if (isset($_GET['mode'])) 
    $modeAffichage=$_GET['mode'];    
$action = "";
    
// si suppression ou reinit de compte demandée :
if (isset($_POST['action'])) 
    $action=mysql_real_escape_string($_POST['action']);
else if (isset($_GET['action']))
    $action = mysql_real_escape_string($_GET['action']);
    
// on supprime le compte du joueur
if ($action=="supprimerCompte") {
    
    // on vérifie si le joueur a bien coché la case
    if (isset($_POST['checkConfirm'])) {
        $checkboxValue = mysql_real_escape_string($_POST['checkConfirm']);
        $messageExplication = $_POST['messageExplication'];
        if ($checkboxValue == "ok") {
            supprimerCompte($userdata['joueur_id'],0,$messageExplication);
            redirectHT("index.php?compteSupprime=1");
        }
    }
}
else if ($action=="reinitialiserCompte") {
    
    // on vérifie si le joueur a bien coché la case
    if (isset($_POST['checkConfirm'])) {
        $checkboxValue = mysql_real_escape_string($_POST['checkConfirm']);
        if ($checkboxValue == "ok") {
            reinitialiserCompte($userdata['joueur_id']);
            redirectHT("jeu.php?mode=m_accueil");
        }
    }
}
else if ($action=="modifNewsletter") {
    
    $newsletter = 0;
    
    // le joueur a-t-il coché la case ?
    if (isset($_POST['checkNewsLetter']))
        $newsletter = 1;

    $query = "UPDATE joueurs SET newsletter = ".$newsletter." WHERE joueur_id=".$userdata['joueur_id'] ;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }
    $userdata['newsletter'] = $newsletter;
}
else if ($action=="associerCompteFacebook") {
    
    $facebook_id = -1;
    
    if (isset($_GET['facebook_id']) && is_numeric($_GET['facebook_id']) ) {
        $facebook_id = $_GET['facebook_id'];
    
        $query = "INSERT INTO facebook VALUES ( ".$userdata['joueur_id'].",".$facebook_id." ) ";
        if ( !($dbHT->sql_query($query)) ){
            //echo 'Error adding facebook : '.$query;
        }
    }
}
else if ($action=="supprimerCompteFacebook") {
    $query = "DELETE FROM facebook
        WHERE joueur_id=".$userdata['joueur_id'];    
    if ( !($dbHT->sql_query($query)) ){
        //echo 'Error deleting facebook : '.$query;
    }
}
else {
    if (isset($_REQUEST['actionInteraction'])) {
        $action = mysql_real_escape_string($_REQUEST['actionInteraction']);
        require("actionInteraction.php");
    }
}

if (isset($_POST['submitImage'])) {
            
    $max_size   = 100000;     // Taille max en octets du fichier
    $width_max  = 100;        // Largeur max de l'image en pixels
    $height_max = 100;        // Hauteur max de l'image en pixels
    
    if(!empty($_FILES['fichierImage']['name'])) {
    
        $nom_file   = $_FILES['fichierImage']['name'];
        $taille     = $_FILES['fichierImage']['size'];
        $tmp        = $_FILES['fichierImage']['tmp_name']; 
        
           // On vérifie l'extension du fichier
        if(substr($nom_file, -3) == "jpg" || substr($nom_file, -3) == "JPG" || substr($nom_file, -3) == "jpeg" || substr($nom_file, -3) == "JEPG") {
            // On récupère les dimensions du fichier
            $infos_img = getimagesize($_FILES['fichierImage']['tmp_name']);
            
            // On vérifie les dimensions et taille de l'image
            if ($infos_img[0] > $width_max || $infos_img[1] > $height_max){
                
                $newX = $infos_img[0] ;
                $newY = $infos_img[1] ;
                
                if ($infos_img[0] > $width_max){
                    $newX = $width_max;
                    $newY = intval($infos_img[1] * $width_max / $infos_img[0]);
                }
                else{
                    $newY = $height_max;
                    $newX = intval($infos_img[0] * $height_max / $infos_img[1]);
                }
                
                $img_big = imagecreatefromjpeg($_FILES['fichierImage']['tmp_name']); # On ouvre l'image d'origine
                $img_new = imagecreate($newX, $newY);
                # création de la miniature
                $img_mini = imagecreatetruecolor($newX, $newY) or   $img_mini = imagecreate($newX, $newY);
                // copie de l'image, avec le redimensionnement.
                imagecopyresampled($img_mini,$img_big,0,0,0,0,$newX,$newY,$infos_img[0],$infos_img[1]);
                imagejpeg($img_mini,"images/joueurs/".$userdata['joueur_id'].".jpg" );
            }
            else
                // sinon, copie simple
                move_uploaded_file($_FILES['fichierImage']['tmp_name'],"images/joueurs/".$userdata['joueur_id'].".jpg");
            
            // Si upload OK alors on affiche le message de réussite
            $msg .= "<b>".T_("Image envoyé avec succès ! Si elle n'apparait pas, appuie sur la touche F5 ou le bouton Actualiser").".</b>";

            $query = "UPDATE joueurs SET image = 1 WHERE joueur_id = ".$userdata['joueur_id'];
            $result = $dbHT->sql_query($query);
            if ( ! $result ) {
                echo "Erreur SQL : ".$query."<br>" ;
                return;
            }
            $userdata['image'] = 1;
        } 
        else {
            // Sinon on affiche une erreur pour les dimensions et taille de l'image
            $msg .= "<span class=txtErreur><b>".T_("Problème dans les dimensions ou taille de l'image ! Ton image a pour dimensions ").$infos_img[0]."x".$infos_img[1]." ".T_("pixels et sa taille fait ").round(($_FILES['fichierImageBonus']['size'])/1024)." ko.</b></span><br /><br />";
        }
    }
    else {
        // Sinon on affiche une erreur pour l'extension
        $msg .= "<span class=txtErreur><b>".T_("Votre image ne comporte pas l'extension .jpg !")."</b></span><br /><br />";
    }
}
else if (isset($_POST['submitImageBonus'])) {
        
    $max_size   = 200000;     // Taille max en octets du fichier
    $width_max  = 1024;        // Largeur max de l'image en pixels
    $height_max = 768;        // Hauteur max de l'image en pixels
    
    if(!empty($_FILES['fichierImageBonus']['name'])) {
    
        $nom_file   = $_FILES['fichierImageBonus']['name'];
        $taille     = $_FILES['fichierImageBonus']['size'];
        $tmp        = $_FILES['fichierImageBonus']['tmp_name']; 
        
       // On vérifie l'extension du fichier
    if(substr($nom_file, -3) == "jpg" || substr($nom_file, -3) == "JPG") {
        // On récupère les dimensions du fichier
        $infos_img = getimagesize($_FILES['fichierImageBonus']['tmp_name']);
        
        // On vérifie les dimensions et taille de l'image
        if(($infos_img[0] <= $width_max) && ($infos_img[1] <= $height_max) && ($_FILES['fichierImageBonus']['size'] <= $max_size)) {
            // Si c'est OK, on teste l'upload
            if(move_uploaded_file($_FILES['fichierImageBonus']['tmp_name'],"images/joueurs/".$userdata['joueur_id']."_bonus.jpg" )) {
              // Si upload OK alors on affiche le message de réussite
              $msg .= "<b>".T_("Image envoyé avec succès ! Si elle n'apparait pas, appuie sur la touche F5 ou le bouton Actualiser").".</b>";
              
              $query = "UPDATE joueurs SET image_bonus = 1 WHERE joueur_id = ".$userdata['joueur_id'];
                            $result = $dbHT->sql_query($query);
                            if ( ! $result ) {
                                echo "Erreur SQL : ".$query."<br>" ;
                                return;
                            }
                            $userdata['image_bonus'] = 1;
                    } else {
                // Sinon on affiche une erreur système
                $msg .= "<span class=txtErreur><b>".T_("Problème lors de l'envoi !")."</b><br /><br /><b>,".$_FILES['fichierImageBonus']['error']."</b></span><br /><br />";
            }
        } else {
            // Sinon on affiche une erreur pour les dimensions et taille de l'image
            $msg .= "<span class=txtErreur><b>".T_("Problème dans les dimensions ou taille de l'image ! Ton image a pour dimensions ").$infos_img[0]."x".$infos_img[1]." ".T_("pixels et sa taille fait ").round(($_FILES['fichierImageBonus']['size'])/1024)." ko.</b></span><br /><br />";
        }
    }
    else {
        // Sinon on affiche une erreur pour l'extension
        $msg .= "<span class=txtErreur><b>".T_("Votre image ne comporte pas l'extension .jpg !")."</b></span><br /><br />";
    }
  }
  else{
      //$msg .= "<span class=txtErreur><b>Pas d'image envoyée</b></span><br /><br />";
  }
}

$mode = "options" ; // utilisee par formulaireChoixPseudo
$erreurPseudo = 0;
$erreurPasswd = 0;
$erreurMail = 0;
$statutActivation = 0;
$erreur=0;
$message = '';
$erreurMsg = '';
$parrain = -1;
$okPourEmail = 0;

$pseudo = $userdata['pseudo'];
$passwd = $userdata['passwd'];
$passwd_chk = $passwd;
$email = $userdata['email'];
$age = intval($userdata['age']);
$naissance_jour = intval($userdata['naissance_jour']);
$naissance_mois = intval($userdata['naissance_mois']);
$naissance_annee = intval($userdata['naissance_annee']);
$sexe = $userdata['sexe'];
$pays = $userdata['pays'];
$ville = $userdata['ville'];
$blog = $userdata['blog'];
$langue = $userdata['langue'];
$presentation = $userdata['presentation'];
$presentationForBDD = $presentation;
$liste_rouge = intval($userdata['liste_rouge']);
$montrer_profil = intval($userdata['montrer_profil']);
$prevenir_nouveau_message = intval($userdata['prevenir_nouveau_message']);
$email_active = intval($userdata['email_active']);

// on récupère les nouveaux paramètres du joueur (vérification à la volée aussi)
require "verifPseudo.php" ;

// on met à jour userdata
$userdata['pays'] = $pays;
$userdata['ville'] = $ville;
$userdata['blog'] = $blog;
$userdata['langue'] = $langue;
$userdata['sexe'] = $sexe;
$userdata['age'] = $age;
$userdata['naissance_jour'] = $naissance_jour;
$userdata['naissance_mois'] = $naissance_mois;
$userdata['naissance_annee'] = $naissance_annee;

// on regarde si le mail a changé (sans erreur apparente), auquel cas on remet à zéro le statut de validation
$mailErrone = $email ;
if ($erreurMail > 0) {
    $email = $userdata['email'] ; // on remet l'ancien mail
}
else if ($userdata['email'] != $email) {
    $email_active = 0;
    $userdata['email_active'] = 0;
}
if (isset($_POST['demanderActivation'])) {
    // on envoie un email d'activation (sauf si l'adresse mail est invalide)
    if ($email != "" && validate_mail_address($email) && $erreurMail == 0 ) {
        $TO = $email;
        $subject = T_("Hamster Academy : activation de ton email") ;
        $message = T_("Bonjour")." ".$pseudo.",\n\n";
        $message .= T_("Tu es inscrit(e) à Hamster Academy et tu as demandé la validation de ton adresse email. Pour cela, tu dois cliquer le lien ci-dessous (ou le recopier dans la barre d'adresse) :")."\n\n";
        $message .= $base_site_url."activer.php?joueur_id=".$userdata['joueur_id']."&cle=".calculCodeActivation($userdata['joueur_id'], $email);
        $message .= "\n\n".T_("A bientôt dans le jeu !")."\n\n--\n".T_("L'équipe Hamster Academy")."\n";
        envoyerMail($TO,$subject,$message,"activation");
        $statutActivation = 1; // activation ok
    }
    else {
        $statutActivation = 2; // erreur dans l'activation
    }
}

// on regarde si le mot de passe a changé
if ($userdata['passwd'] != $passwd && $passwd != '') {
    
    // on doit faire une MAJ de la base du forum => on ferme la base principale
    $dbForum->sql_activate();
    
    define('PUN_ROOT', './forum/');
    require PUN_ROOT.'include/common.php';
    
    $query = "UPDATE pun_users SET password = '".pun_hash($passwd)."' WHERE id=".$userdata['joueur_id'];
    if (! $dbForum->sql_query($query)) {
        echo "Connexion impossible avec le forum" ;
    }
    
    // on reouvre la base principale
    $dbHT->sql_activate();
}

// on regarde s'il y a le cheat-code dans sa présentation
if ($userdata['cheat_code'] == 0) { // si le joueur ne l'a pas déjà trouvé
    
    if (strpos($presentationForBDD,"HamsterOuf") !== FALSE) {
        $msg .= "<div style=\"padding-bottom:40px;\">".afficherNouvelle("coffre_or.gif",T_("Bravo ! Tu as trouvé le coffre d'or ! Il contient 50 pièces !"))."</div>";
        crediterPieces(50,false);
        $presentationForBDD = str_replace("HamsterOuf","",$presentationForBDD) ;
        $presentation = str_replace("HamsterOuf","",$presentation) ;
        $userdata['presentation'] = $presentation;
        $userdata['cheat_code'] = 1;
    
        $query = "UPDATE joueurs SET 
            cheat_code = 1 ,
            presentation = '".$presentationForBDD."' 
            WHERE joueur_id=".$userdata['joueur_id'];
        $dbHT->sql_query($query);
    }
}

if ((isset($_POST['submitProfil']) || isset($_POST['demanderActivation']) ) && $erreur == 0) {
        
    $query = "UPDATE joueurs 
        SET passwd = '".$passwd."',
        email = '".$email."',
        age = ".$age.",
        naissance_jour = ".$naissance_jour.",
        naissance_mois = ".$naissance_mois.",
        naissance_annee = ".$naissance_annee.",
        pays = ".$pays.",
        ville = '".$ville."',
        blog = '".$blog."',
        langue = '".$langue."',
        sexe = ".$sexe.",
        presentation = '".$presentationForBDD."',
        liste_rouge = ".$liste_rouge.",
        montrer_profil = ".$montrer_profil.",
        prevenir_nouveau_message = ".$prevenir_nouveau_message.",
        email_active = ".$email_active."
        WHERE joueur_id=".$userdata['joueur_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur profil', '', __LINE__, __FILE__, $query);
    }
}

$pagetitle = "Hamster Academy - ".T_("Options");
$univers = UNIVERS_ELEVEUR ;
if (isset($_GET['univers']))
    $univers = intval($_GET['univers']);
$description = T_("Options de profil pour Hamster Academy : decris-toi, choisis un avatar, choisis tes options de jeux...");

$liste_styles = "style.css,styleEleveur.css";
$liste_scripts = "tooltip.js,gestionHamster.js,jquery.js";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

// changement du macaron
if ($lang == "en"){
    $imageMacaron = "macaron-en.png";
    echo "<style content=\"text/css\">
        .macaron {
            background: transparent url('../images/$imageMacaron');
        }
        </style>";
}

?>

<div class="logoMain">
    <div class="logoImage">     
        <img src="images/logoHamsterAcademy<?php if ($univers==UNIVERS_ACADEMY) echo "_ac" ; ?>.gif" alt="Hamster Academy" />
    </div>
    <div class="macaron"></div>
</div>

<div class="header">
  <ul>
      <li class="menuPrincipal" <?php if ($modeAffichage == "m_profil") echo "id=\"current\"" ;?> ><a href="options.php?mode=m_profil"><?php echo T_("Mon profil");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_compte") echo "id=\"current\"" ; ?> ><a href="options.php?mode=m_compte"><?php echo T_("Mon compte");?></a></li>
    <?php if ($userdata['niveau'] >= 4) { ?>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_amis") echo "id=\"current\"" ; ?> ><a href="options.php?mode=m_amis"><?php echo T_("Mes amis");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_vip") echo "id=\"current\"" ; ?> ><a href="options.php?mode=m_vip"><?php echo T_("VIP");?></a></li>
    <?php } ?>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_facebook") echo "id=\"current\"" ; ?> ><a href="options.php?mode=m_facebook">Facebook</a></li>
    <li class="menuPrincipalDroite"><a href="jeu.php?mode=m_accueil&amp;univers=<?php echo $univers; ?>"><img src="images/hamster_reduc.gif" style="height:16px; vertical-align:middle;" alt="" /> <?php echo T_("Revenir au jeu") ;?></a></li>
  </ul>
</div> 

<div class="mainPageUnderMenu">

<br/>&nbsp;<br/>

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">

<?php 

if ($msg != "")
    echo "<br/><div class=\"hamBlocColonne\">".$msg."</div>" ;

if ($modeAffichage == "m_profil") { //profil du joueur    
    ?>
    <!-- TinyMCE -->
        <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
        <script type="text/javascript">
            tinyMCE.init({
                mode : "textareas",
                theme : "advanced",
                plugins : "safari,advhr,advimage,advlink,emotions,iespell,media,contextmenu",
        
                // Theme options
                theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
                theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,blockquote,|,undo,redo,|,link,unlink,image,|,forecolor,backcolor,|,hr,emotions",
                theme_advanced_buttons3 : "",
                theme_advanced_toolbar_location : "top",
                theme_advanced_toolbar_align : "left",
                theme_advanced_statusbar_location : "bottom",
                theme_advanced_resizing : true,
        
                // Drop lists for link/image/media/template dialogs
                template_external_list_url : "lists/template_list.js",
                external_link_list_url : "lists/link_list.js",
                external_image_list_url : "lists/image_list.js"
            });
        </script>
    <!-- /TinyMCE -->

    <?php
    
    require("formulaireChoixPseudo.php");
}
else if ($modeAffichage == "m_compte") { // compte

    echo "<div class=\"blocOptions\"><strong>".T_("Options de compte")."</strong><br/>&nbsp;<br/>";
    echo "<form method=\"post\" action=\"options.php\">";
    echo "<input type=\"hidden\" name=\"mode\" value=\"m_compte\" />";
    echo "<input type=\"hidden\" name=\"action\" value=\"modifNewsletter\" />";
    echo T_("Recevoir la lettre d'information de Hamster Academy (dernières nouveautés, cadeaux, etc.)")." : <input type=\"checkbox\" name=\"checkNewsLetter\" value=\"ok\"".($userdata['newsletter']? "checked=\"checked\"":"\"\"")." /><br/>&nbsp;<br/>";
    echo "<input type=\"submit\" value=\"".T_("Enregistrer les options")."\" />";
    echo "</form>";
    echo "</div>";  
    
    echo "<div class=\"blocOptions\"><strong>".T_("Recommencer le jeu depuis le début (je me retrouve au niveau 1, avec 1 hamster et 1 cage)")."</strong><br/>&nbsp;<br/>";
    echo "<form method=\"post\" action=\"options.php\">";
    echo "<input type=\"hidden\" name=\"action\" value=\"reinitialiserCompte\" />";
    echo T_("Recommencer le jeu depuis le début")." : <input type=\"checkbox\" name=\"checkConfirm\" value=\"ok\" /><br/>&nbsp;<br/>";
    echo "<input type=\"submit\" value=\"".T_("Recommencer le jeu")."\" />";
    echo "</form></div>";
    
    echo "<div class=\"blocOptions\"><strong>".T_("Suppression de compte")."</strong><br/>&nbsp;<br/>";
    echo "<form method=\"post\" action=\"options.php\">";
    echo "<input type=\"hidden\" name=\"action\" value=\"supprimerCompte\" />";
    echo T_("Supprimer mon compte")." : <input type=\"checkbox\" name=\"checkConfirm\" value=\"ok\" /><br/>&nbsp;<br/>";
    echo T_("Si tu veux dire pourquoi tu quittes Hamster Academy")." :<br/><textarea name=\"messageExplication\" rows=\"4\" cols=\"40\"></textarea><br/>&nbsp;<br/>";
    echo "<input type=\"submit\" value=\"".T_("Valider la suppression de compte")."\" />";
    echo "</form>";
    echo "</div>";
    
    echo T_("Si tu as des questions ou des doutes, n'hésite pas à")." <a href=\"".T_("aide")."/doku.php?id=www.hamsteracademy.fr_aide_generale\">".T_("consulter l'aide ou à nous contacter")." !</a>";

}
else if ($modeAffichage == "m_amis") { // amis

    require "jeu_amis.php";

}  // fin du dernier else if
else if ($modeAffichage == "m_vip") {
    require "jeu_vip.php";
}
else if ($modeAffichage == "m_parrainage") {
    if ($userdata['pseudo'] == "César"){
        require "jeu_parrainage.php";
    }   
}
else if ($modeAffichage == "m_facebook") {
    require "jeu_facebook.php";
}
else {
    echo "..."; 
}  
?>

  <div style="clear:both;">&nbsp;</div>

  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>

</div> <!-- fin du mainPageUnderMenu -->

<?php require "footer.php"; ?>