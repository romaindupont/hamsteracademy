<?php
/***************************************************************************
 *                                sessions.php
 *                            -------------------
 *   begin                : Saturday, Feb 13, 2001
 *   copyright            : (C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: sessions.php,v 1.58.2.16 2005/10/30 15:17:14 acydburn Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 ***************************************************************************/

//
// Adds/updates a new session to the database for the given userid.
// Returns the new session ID on success.
//
function session_begin($joueur_id, $user_ip)
{
	global $dbHT, $board_config;
	global $HTTP_COOKIE_VARS;

	$cookiename = "hamsters_touffous";
	$cookiepath = "/";
	$cookiedomain = "";
	$cookiesecure = 0;

	$session_id = isset($HTTP_COOKIE_VARS[$cookiename . '_sid']) ? $HTTP_COOKIE_VARS[$cookiename . '_sid'] : '';
	$sessiondata = isset($HTTP_COOKIE_VARS[$cookiename . '_data']) ? unserialize(stripslashes($HTTP_COOKIE_VARS[$cookiename . '_data'])) : array();
	$sessionmethod = SESSION_METHOD_COOKIE;

	//
	if (!preg_match('/^[A-Za-z0-9]*$/', $session_id)) 
	{
		$session_id = '';
	}

	$last_visit = 0;
	$current_time = time();

	//
	// At this point either $userdata should be populated or
	// one of the below is true
	// * User does not exist

	$sessiondata['userid'] = $joueur_id;
	$login = 1;

	$sql = 'SELECT *
		FROM joueurs
		WHERE joueur_id = ' . (int) $joueur_id;
	if (!($result = $dbHT->sql_query($sql)))
	{
		message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
	}

	$userdata = $dbHT->sql_fetchrow($result);
	$dbHT->sql_freeresult($result);

	//
	// Create the session
	//
	list($sec, $usec) = explode(' ', microtime());
	mt_srand((float) $sec + ((float) $usec * 100000));
	$session_id = md5(uniqid(mt_rand(), true));

	$sql = "INSERT INTO sessions
		(session_id, session_user_id, session_start, session_time, session_ip, session_logged_in)
		VALUES ('$session_id', $joueur_id, $current_time, $current_time, '$user_ip',$login)";
	if ( !$dbHT->sql_query($sql) )
	{
		message_die(CRITICAL_ERROR, 'Error creating new session', '', __LINE__, __FILE__, $sql);
	}

	$last_visit = ( $userdata['user_session_time'] > 0 ) ? $userdata['user_session_time'] : $current_time; 

	$sql = "UPDATE joueurs
		SET user_session_time = $current_time, user_lastvisit = $last_visit
		WHERE joueur_id = $joueur_id";
	if ( !$dbHT->sql_query($sql) )
	{
		message_die(CRITICAL_ERROR, 'Error updating last visit time', '', __LINE__, __FILE__, $sql);
	}

	$userdata['user_lastvisit'] = $last_visit;

	$sessiondata['userid'] = $joueur_id;
	$userdata['session_id'] = $session_id;
	$userdata['session_ip'] = $user_ip;
	$userdata['session_user_id'] = $joueur_id;
	$userdata['session_logged_in'] = $login;
	$userdata['session_start'] = $current_time;
	$userdata['session_time'] = $current_time;

	setcookie($cookiename . '_data', serialize($sessiondata), $current_time + 31536000, $cookiepath, $cookiedomain, $cookiesecure);
	setcookie($cookiename . '_sid', $session_id, 0, $cookiepath, $cookiedomain, $cookiesecure);

	return $userdata['session_id'];
}

//
// Checks for a given user session, tidies session table and updates user
// sessions at each page refresh
//
function session_pagestart($user_ip)
{
	global $dbHT, $board_config;
	global $HTTP_COOKIE_VARS;
	
	$cookiename = "hamsters_touffous";
	$cookiepath = "/";
	$cookiedomain = "";
	$cookiesecure = 0;
	
	$current_time = time();
	unset($userdata);

	$sessiondata = isset( $HTTP_COOKIE_VARS[$cookiename . '_data'] ) ? unserialize(stripslashes($HTTP_COOKIE_VARS[$cookiename . '_data'])) : array();
	$session_id = isset( $HTTP_COOKIE_VARS[$cookiename . '_sid'] ) ? $HTTP_COOKIE_VARS[$cookiename . '_sid'] : '';
	$sessionmethod = SESSION_METHOD_COOKIE;

	// 
	if (!preg_match('/^[A-Za-z0-9]*$/', $session_id))
	{
		$session_id = '';
	}

	//
	// Does a session exist?
	//
	if ( !empty($session_id) )
	{
		//
		// session_id exists so go ahead and attempt to grab all
		// data in preparation
		//
		$query = "SELECT u.*, s.*
			FROM sessions s, joueurs u
			WHERE s.session_id = '$session_id'
				AND u.joueur_id = s.session_user_id";
		if ( !($result = $dbHT->sql_query($query)) )
		{
			message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $query);
		}
		
		$userdata = $dbHT->sql_fetchrow($result);

		//
		// Did the session exist in the DB?
		//
		if ( isset($userdata['joueur_id']) )
		{
			//
			// Do not check IP assuming equivalence, if IPv4 we'll check only first 24
			// bits ... I've been told (by vHiker) this should alleviate problems with 
			// load balanced et al proxies while retaining some reliance on IP security.
			//
			$ip_check_s = substr($userdata['session_ip'], 0, 6);
			$ip_check_u = substr($user_ip, 0, 6);

			if ($ip_check_s == $ip_check_u)
			{
				//
				// Only update session DB a minute or so after last update
				//
				if ( $current_time - $userdata['session_time'] > 60 )
				{
					$sql = "UPDATE sessions
						SET session_time = $current_time
						WHERE session_id = '" . $userdata['session_id'] . "'";
					if ( !$dbHT->sql_query($sql) )
					{
						message_die(CRITICAL_ERROR, 'Error updating sessions table', '', __LINE__, __FILE__, $sql);
					}

					$sql = "UPDATE joueurs
						SET user_session_time = $current_time
						WHERE joueur_id = " . $userdata['joueur_id'];
					if ( !$dbHT->sql_query($sql) )
					{
						message_die(CRITICAL_ERROR, 'Error updating sessions table', '', __LINE__, __FILE__, $sql);
					}
										
					session_clean($userdata['session_id']);
					
					setcookie($cookiename . '_data', serialize($sessiondata), $current_time + 31536000, $cookiepath, $cookiedomain, $cookiesecure);
					setcookie($cookiename . '_sid', $session_id, 0, $cookiepath, $cookiedomain, $cookiesecure);
				}
				return $userdata;
			}
            redirectHT("index.php?err=notsameip");
		}
	}

	// no valid session
	redirectHT("index.php?err=session_expiree");
}

//
// session_end closes out a session
// deleting the corresponding entry
// in the sessions table
//
function session_end($session_id, $joueur_id)
{
	global $dbHT, $board_config;
	global $HTTP_COOKIE_VARS;

	$cookiename = "hamsters_touffous";
	$cookiepath = "/";
	$cookiedomain = "";
	$cookiesecure = 0;

	$current_time = time();

	if (!preg_match('/^[A-Za-z0-9]*$/', $session_id))
	{
		return;
	}
	
	//
	// Delete existing session
	//
	$sql = "DELETE FROM sessions
		WHERE session_id = '$session_id' 
			AND session_user_id = $joueur_id";
	if ( !$dbHT->sql_query($sql) )
	{
		message_die(CRITICAL_ERROR, 'Error removing user session', '', __LINE__, __FILE__, $sql);
	}

	setcookie($cookiename . '_data', '', $current_time - 31536000, $cookiepath, $cookiedomain, $cookiesecure);
	setcookie($cookiename . '_sid', '', $current_time - 31536000, $cookiepath, $cookiedomain, $cookiesecure);

	return true;
}

/**
* Removes expired sessions
*/
function session_clean($session_id)
{
	global $board_config, $dbHT;

	//
	// Delete expired sessions
	//
	$sql = 'DELETE FROM sessions
		WHERE session_time < ' . (time() - (int) $board_config['session_length']) . " 
			AND session_id <> '$session_id'";
	if ( !$dbHT->sql_query($sql) )
	{
		message_die(CRITICAL_ERROR, 'Error clearing sessions table', '', __LINE__, __FILE__, $sql);
	}

	return true;
}

?>