<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";
include "lstInstruments.php";
include "lstAccessoires.php";

error_reporting( E_ALL );

// on donne un nouvel hamster � tous les inscrits

$query = "SELECT * FROM lst_joueurs_concours";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining info', '', __LINE__, __FILE__, $query);
}
while($row=$dbHT->sql_fetchrow($result)) {
    
    echo "Joueur ".$row['pseudo_joueur']."<br/>";
    
    $hamster_id = $row['hamster_id'];
    
    if ($hamster_id == 0) {
        echo "joueur a corriger :<br/>";
        $queryHamster = "SELECT * FROM hamster WHERE nom LIKE 'HamsterFlash%' AND joueur_id=".$row['joueur_id'];
        $resultHamster = $dbHT->sql_query($queryHamster);
        $nbHamsters = $dbHT->sql_numrows($resultHamster);
        if ($nbHamsters > 0) {
            $rowHamster=$dbHT->sql_fetchrow($resultHamster);
            
            $hamster_id = $rowHamster['hamster_id'];
            
            $queryCorrection = "UPDATE lst_joueurs_concours SET hamster_id = $hamster_id WHERE joueur_id = ".$row['joueur_id'] ;
            $dbHT->sql_query($queryCorrection);
            
        }
        else
            echo "non corrige... car pas de HamsterFlash<br/>";
    }
    
    $queryHamster = "SELECT * FROM hamster WHERE hamster_id = $hamster_id";
    $resultHamster = $dbHT->sql_query($queryHamster);
    $nbHamsters = $dbHT->sql_numrows($resultHamster);
    echo "nb hamsters : $nbHamsters<br/>";
    if ($nbHamsters > 0){
        $rowHamster=$dbHT->sql_fetchrow($resultHamster);
        
        echo "rowHamster = ".$rowHamster['nom']."<br/>";
        $noteHamster = 0;
        if (possedeAccessoire(ACC_AMULETTE_FORCE,$row['joueur_id']) != -1 )
            $rowHamster['puissance'] *= $puissanceAmuletteForce;
        
        $noteHamster = $rowHamster['sante'] + $rowHamster['puissance'] + $rowHamster['beaute'] + $rowHamster['moral'] ;
        $noteHamster += $rowHamster['popularite'] + $rowHamster['experience']+ $rowHamster['danse']+ $rowHamster['chant']+ $rowHamster['sociabilite']+ $rowHamster['humour']+ $rowHamster['musique'];
        if ($rowHamster['specialite'] != -1)
            $noteHamster += $lstInstruments[$rowHamster['specialite']][INSTRUMENT_POINTS];
            
        if ($rowHamster['compagnon_id'] > 0) // 10 % de bonus si le hamster est mari� 
            $noteHamster = $noteHamster + ($noteHamster/10);
            
        if ($rowHamster['sante'] == 0){ // 1 hamster malade => les points sont divis�s par 2
            $noteHamster /= 2;
        }
        echo "score : ".$noteHamster."<br/>";
        $queryScore = "UPDATE lst_joueurs_concours SET puissance = '$noteHamster' WHERE joueur_id = ".$row['joueur_id'] ;
        $dbHT->sql_query($queryScore);
    }
    echo "<br/>";
}
$dbHT->sql_freeresult($result);

?>