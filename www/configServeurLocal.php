<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

$serveur='xxx'; 
$user='xxx'; 
$password='xxx'; 
$base='xxx';  

$serveurForum='xxx'; 
$userForum='xxx'; 
$passwordForum='xxx'; 
$baseForum='xxx';  

if (! isset($template_dir))
    $template_dir = 'templates';

if (! isset($smarty_dir))
    $smarty_dir = '../cgi-bin/smarty';
    
$board_config['server_name'] = "localhost:8080/";
$base_site_url = "http://localhost:8080/";

if ( ! defined('NO_SMARTY') )
{
    require($smarty_dir.'/libs/Smarty.class.php');
    
    $smarty = new Smarty();
    $smarty->template_dir = $template_dir;
    $smarty->compile_dir = $template_dir.'/templates_c';
    $smarty->cache_dir = $template_dir.'/cache';
    $smarty->config_dir = $template_dir.'/configs';
    $smarty->caching = 0;    
}

?>
