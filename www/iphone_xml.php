<?php 

define('IN_HT', true);

include "common.php";
include "lstAccessoires.php";
include "lstHamsters.php" ;
include "lstNiveaux.php" ;
include "gestion.php";


//error_reporting( E_ALL );

require "array_to_xml.php";

// login du joueur
$login = 0;
if (isset($_POST['login']) || isset($_GET['login'])) {
    
    $login = 1;

    require "login.php";
 
    // à la connexion du joueur, on met à jour son user_lastvisit et son ip
    $queryIP = "UPDATE joueurs SET user_lastvisit = ".$dateActuelle.", last_ip = '".$user_ip."' WHERE joueur_id = ".$joueur_id." LIMIT 1";
    if ( ! $dbHT->sql_query($queryIP)){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $queryIP);
    }
}
else
    $userdata = session_pagestart($user_ip);
    
if( ! $userdata['session_logged_in'] ) { 
    echo T_("Erreur de connection, contacte Hamster Academy si le problème persiste.");
    redirectHT("index.php");
}

// liste des hamsters
// ----------------------
$lst_groupes = array();
$query = "SELECT * FROM hamster WHERE joueur_id=".$userdata['joueur_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamsters = $dbHT->sql_numrows($result) ;
$lst_hamsters=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($lst_hamsters,$row);
    
    if ($row['groupe_id'] != -1)
        array_push($lst_groupes,$row['groupe_id']);
}
$dbHT->sql_freeresult($result);

// liste des cages
// ----------------------
$query = "SELECT * FROM cage WHERE joueur_id=".$userdata['joueur_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;
$lst_cages=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($row,0);
    $row['nbAccessoires'] = 0;
    array_push($lst_cages,$row);
}
$dbHT->sql_freeresult($result);

// liste des accessoires pour les cages
// ------------------------------------
 $query = "SELECT * FROM accessoires WHERE joueur_id=".$userdata['joueur_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
}
$nbAccessoires = $dbHT->sql_numrows($result) ;
$lst_accessoires=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($lst_accessoires,$row);
    // on cherche la cage qui possederait cet accessoire pour le comptage
    $cage = cageCorrespondante($row['cage_id']);
    if ($cage != -1) 
        $lst_cages[$cage]['nbAccessoires'] ++ ;
}
$dbHT->sql_freeresult($result);  

$lst_hamsters_xml = array();
for($h=0;$h<$nbHamsters;$h++){
    $array_tmp = array("hamster".$h => $lst_hamsters[$h]);
    //$array_tmp = array("hamster" => $lst_hamsters[$h]);
    $lst_hamsters_xml = array_merge($lst_hamsters_xml,$array_tmp);
}

$lst_cages_xml = array();
for($c=0;$c<$nbCages;$c++){
    $array_tmp = array("cage".$c => $lst_cages[$c]);
    //$array_tmp = array("cage" => $lst_cages[$c]);
    $lst_cages_xml = array_merge($lst_cages_xml,$array_tmp);
}

$lst_accessoires_xml = array();
for($c=0;$c<$nbAccessoires;$c++){
    $array_tmp = array("accessoire".$c => $lst_accessoires[$c]);
    //$array_tmp = array("accessoire" => $lst_accessoires[$c]);
    $lst_accessoires_xml = array_merge($lst_accessoires_xml,$array_tmp);
}

// pour éviter tout pb avec !
$userdata['passwd'] = " ";

$toutesinfos = array(
"donnees" => 
array( 
    "userdata" => $userdata ,
    //"nbHamsters" => $nbHamsters,
    "hamsters" => $lst_hamsters_xml,
    //"nbCages" => $nbCages,
    "cages" => $lst_cages_xml,
    //"nbAccessoires" => $nbAccessoires,
    "lstAccessoires" => $lst_accessoires_xml
    )
);

//header("Content-Type: text/xml" );

// Initiate the class
$xml = new xml();

// Set the array so the class knows what to create the XML from
$xml->setArray($toutesinfos);

// Print the XML to screen
$xml->outputXML('echo'); 

?>