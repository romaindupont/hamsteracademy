<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

function seuillerForce($force,$maxForce) {
    
    global $userdata;
    
    //if (isset($userdata) && $userdata['inscritConcours'] == 1)
    //    $maxForce = 1e8;
    
    if ($force > $maxForce)
        return $maxForce;
    else if ($force < 0)
        return 0;
    else
        return round($force,2);
}

function seuillerSante($sante, $maxSante=10) {
    
    if ($sante > $maxSante)
        return $maxSante;
    else if ($sante < 0)
        return 0;
    else
        return round($sante,2);
}


function seuillerBeaute($beaute, $maxBeaute=10) {
    
    if ($beaute > $maxBeaute)
        return $maxBeaute;
    else if ($beaute < 0)
        return 0;
    else
        return round($beaute,2);
}

function seuillerMoral($moral, $maxMoral=10) {    
    
    if ($moral > $maxMoral)
        return $maxMoral;
    else if ($moral < 0)
        return 0;
    else
        return round($moral,2);      
}

// si c'est lie au salaire, on le precise
function crediterPieces($nbPieces, $isSalaire) {
    
    global $userdata, $dbHT;
    $userdata['nb_pieces'] = $userdata['nb_pieces'] + $nbPieces;
    majPiecesBDD($isSalaire);
}

// on le precise
function crediterPiecesBDD($joueur_id, $nbPieces) {
    
    global $dbHT;
    $query = "SELECT nb_pieces FROM joueurs WHERE joueur_id=".$joueur_id;    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueur', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result) ;

    $query = "UPDATE joueurs 
        SET nb_pieces = ".($row['nb_pieces'] + $nbPieces)."
        WHERE joueur_id=".$joueur_id;    
        
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

// débite sans vérification ... 
function debiterPiecesBDD($joueur_id, $nbPieces) {
    
    global $dbHT;
    $query = "SELECT nb_pieces FROM joueurs WHERE joueur_id=".$joueur_id;    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueur', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result) ;

    // au moins 0 pièce !
    $query = "UPDATE joueurs 
        SET nb_pieces = ".max(0,($row['nb_pieces'] - $nbPieces))."
        WHERE joueur_id=".$joueur_id;    
        
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

function debiterPieces($nbPieces,$objetConcerne = "inconnu") {
    
    global $userdata, $dbHT;
    
    // on teste si le joueur a assez de pieces
    if ($userdata['nb_pieces'] < $nbPieces) {
        return 0; // pas assez de piece, on renvoit 0
    }
    else {
        $userdata['nb_pieces'] = $userdata['nb_pieces'] - $nbPieces;
        majPiecesBDD(false);
        enregistrerPremierAchat($objetConcerne);
        
        return 1 ; // debit possible et effectué
    }
}

function majPiecesBDD($isSalaire) { // si c'est lie au salaire, on le precise afin de mettre a jour correctement la BD

    global $userdata, $dbHT,$dateActuelle, $dateActuelleMinuit;

    $query = "";

    if ($isSalaire == true) {
        // on calcule la date de versement de salaire : on remet à zéro à minuit et qq secondes
        $query = "UPDATE joueurs 
            SET nb_pieces = ".$userdata['nb_pieces'].",  
            date_dernier_salaire = ".$dateActuelleMinuit."
            WHERE joueur_id=".$userdata['joueur_id'];
    }
    else {
        $query = "UPDATE joueurs 
            SET nb_pieces = ".$userdata['nb_pieces']."
            WHERE joueur_id=".$userdata['joueur_id'];    
    }
        
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

function miseAJourQuotidienne($joueur_id) {

    global $dbHT, $dateActuelle;
    global $poidsMoyen, $dateActuelleMinuit, $lstInstruments;
    $soif = 0 ;
    $hamstersAuRefuge = false;
        
    $queryUser = "SELECT date_maj_hamster, inscritConcours, user_lastvisit FROM joueurs WHERE joueur_id=".$joueur_id." LIMIT 1";
    
    if ( !($resultUser = $dbHT->sql_query($queryUser)) ){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $queryUser);
    }
    $rowUser=$dbHT->sql_fetchrow($resultUser);  
    $dbHT->sql_freeresult($resultUser);        
    
    //echo "Mise a jour du joueur ".$joueur_id."<br/>";
        
    if ($rowUser['date_maj_hamster'] > ($dateActuelle - 86400)) // 3600*24
        return ;
    
    $query = "SELECT * FROM hamster WHERE joueur_id=".$joueur_id;
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
    }
    
    while($rowHamster=$dbHT->sql_fetchrow($result)) {
        
        // on vérifie que l'hamster n'est pas en position bloquée (gardiennage par exemple)
        if ($rowHamster['pause'] > 0)  {
            $hamstersAuRefuge = true;
            continue;
        }
        
        // chez le véto ? si depuis trop longtemps, il va au cimetière
        if ($rowHamster['chezleveto'] > 0) {
         
            if ($dateActuelle - $rowHamster['chezleveto'] > 1209600) {
                
                // trop longtemps => fin
                //envoyerCimetiere($rowHamster['hamster_id'], $rowHamster['nom'], $joueur_id) ;
            }
            continue;   
        }
            
        // soif ?
        // ------
        if (yatilBiberonDansLaCage($rowHamster['cage_id'], $joueur_id) == -1)
            $soif = 1;
        
        // affamé ?
        // --------
        $faim = faim($rowHamster['dernier_repas']) ;
        
        // calcul de sa santé
        // ------------------
        $diffPoidsMoyen = abs($rowHamster['poids'] - $poidsMoyen);
        $santeActuelle = $rowHamster['sante'] ;
        $deltaSante = 0;
        if ($santeActuelle > 0) { // on ne met à jour la santé que si le hamster n'est pas déjà malade
    
            // en fonction du mode affamé ou non
            if ($faim==0) 
                $deltaSante -= 1 ;
            
            // en fonction de la propreté de la cage
            $propreteCageAssociee = getProprete($rowHamster['cage_id']);
            if ($propreteCageAssociee < 3)
                $deltaSante -= 1;
            
            // si pas de biberon
            if ($soif == 1) 
                $deltaSante -= 1 ;
                
            // si le hamster a une maladie
            if ($rowHamster['maladie'] > 0) {
                $deltaSante -= $lstMaladies[$rowHamster['maladie']][MALADIE_GRAVITE] ;
            }
            
            $deltaSante += ((float) $rowHamster['init_resistant']) / 5. ; // on prend en compte les caractéristiques de naissance
            
            // en fonction du poids
            if ($diffPoidsMoyen > 30)
                $deltaSante -= 1 ;
            else if ($diffPoidsMoyen > 15)
                $deltaSante -= 0.5 ;
            
            // on multiplie l'influence par le nombre de jours sans que le joueur aie joué
            if ($santeActuelle > 5 && $deltaSante > 0)
                $deltaSante /= ($santeActuelle-4.75)/2.;
        }
        $nouvelleSante = $santeActuelle + $deltaSante;        
        $nouvelleSante = seuillerSante($nouvelleSante,$rowHamster['maxSante']) ;
        
        if ($nouvelleSante == 0) { // si le hamster est malade => chez le véto
            
            $queryVeto = "UPDATE hamster SET chezleveto = ".$dateActuelle." WHERE hamster_id = ".$rowHamster['hamster_id']; 
            if ( !($dbHT->sql_query($queryVeto)) ){
               message_die(GENERAL_ERROR, 'Error updating hamster : ', '', __LINE__, __FILE__, $queryVeto);
            }
        }
        
        // calcul forme physique 
        // ---------------------
        $physiqueActuel =  $rowHamster['puissance'];
        $deltaPhysique = 0;
        
        // si pas de roue
        if (yatilRoueDansLaCage($rowHamster['cage_id'], $joueur_id) == -1) {
            $deltaPhysique -= 1 ;
        }
        else {
            // possede de l'huile ?
            if (possedeAccessoire(ACC_HUILE,$joueur_id) != -1)
                $deltaPhysique += 0.5 ;
        }
        
        // si mauvaise santé :
        if ($nouvelleSante < $rowHamster['maxSante']/3)
            $deltaPhysique -= 1;
        else if ($nouvelleSante < $rowHamster['maxSante']/2)
            $deltaPhysique -= 0.5;
        
        $deltaPhysique += ((float) $rowHamster['init_fort']) / 5. ; // on prend en compte les caractéristiques de naissance
        
        // en fonction du poids
        if ($diffPoidsMoyen > 30)
            $deltaPhysique -= 1 ;
        else if ($diffPoidsMoyen > 15)
            $deltaPhysique -= 0.5 ;
            
        if ($physiqueActuel > 5 && $deltaPhysique > 0)
            $deltaPhysique /= ($physiqueActuel-4.75)/2.;
        
        if ($rowUser['inscritConcours'] == 1)
            $rowHamster['maxForce'] = 1e8;
        $nouveauPhysique = seuillerForce($physiqueActuel + $deltaPhysique, $rowHamster['maxForce']);
        
        // calcul beauté 
        // -------------
        $beauteActuelle =  $rowHamster['beaute'];
        $deltaBeaute = 0;
        
        // si mauvaise santé
        if ($nouvelleSante < 3) 
            $deltaBeaute -= 1 ;

        // si hamster affamé :
        if ($faim==0) 
            $deltaBeaute -= 1 ;
        
        $deltaBeaute += ((float) $rowHamster['init_coquet']) / 5. ; // on prend en compte les caractéristiques de naissance
        $deltaBeaute += ((float) $rowHamster['init_dragueur']) / 10. ; // on prend en compte les caractéristiques de naissance
        
        // en fonction du poids
        if ($diffPoidsMoyen > 30)
            $deltaBeaute -= 2 ;
        else if ($diffPoidsMoyen > 15)
            $deltaBeaute -= 1 ;
            
        // y-a-t-il un miroir
        if (yatilAccessoireDansLaCage($rowHamster['cage_id'],ACC_MIROIR_ROSE,$joueur_id) || yatilAccessoireDansLaCage($rowHamster['cage_id'],ACC_MIROIR_NOIR,$joueur_id))
            $deltaBeaute += .3 ;
            
        if ($beauteActuelle > 5 && $deltaBeaute > 0)
            $deltaBeaute /= ($beauteActuelle-4.75)/2.;
        
        $nouvelleBeaute = seuillerBeaute($beauteActuelle + $deltaBeaute, $rowHamster['maxBeaute']) ;
        
        // calcul moral 
        // ------------
        $moralActuel =  $rowHamster['moral'];
        $deltaMoral = 0;
        
        if ( ($dateActuelle - $rowUser['user_lastvisit']) > 172800) // 48 heures sans être venu
            $deltaMoral -= 0.5; // il faut que le joueur s'occupe de lui tous les jours, sinon son moral baisse
        
        // si mauvaise santé
        if ($nouvelleSante < $rowHamster['maxSante']/3) 
            $deltaMoral -= 1 ;
        
        // si pas de roue
        if (yatilRoueDansLaCage($rowHamster['cage_id'],$joueur_id) == -1)
            $deltaMoral -= 1 ;
        
        // si trop d'hamsters dans la même cage
        $deltaMoral -= ((5-$rowHamster['init_sociable']) * (nbHamstersDansLaCage($rowHamster['cage_id'],$joueur_id)-1)) / 5. ;

        $deltaMoral += ((float) $rowHamster['init_sociable']) / 10. ; // on prend en compte les caractéristiques de naissance
        
        if ($moralActuel > 5 && $deltaMoral > 0) // pour que ce soit de plus en plus dur d'avoir un moral à 10
            $deltaMoral /= ($moralActuel-4.75)/2.;
        
        $nouveauMoral = seuillerMoral($moralActuel + $deltaMoral, $rowHamster['maxMoral']);

        // calcul du poids 
        // ---------------
        $poidsActuel =  $rowHamster['poids'];
        $deltaPoids = 0;
        
        // si mauvaise santé
        if ($nouvelleSante < 3)
            $deltaPoids -= 5 ;
        if ($faim == 0)
            $deltaPoids -= 5 ;
        
        // si pas de roue => l'hamster grossit
        if (yatilRoueDansLaCage($rowHamster['cage_id'],$joueur_id) == -1) 
            $deltaPoids += 5 ;
            
        $nouveauPoids = $poidsActuel + $deltaPoids;
        
        if ($nouveauPoids < 80)
            $nouveauPoids = 80 ;
        else if ($nouveauPoids > 160)
            $nouveauPoids = 160 ;
            
        // calcul de la popularite
        $derniereMajPopularite = $rowHamster['derniere_maj_popularite'];
        $popularite = $rowHamster['popularite'];
        if ($rowHamster['inscrit_academy'] == 1){
            if ( ($dateActuelle - $derniereMajPopularite) > 259200) { // 3 jours
                $derniereMajPopularite = $dateActuelleMinuit;
                $popularite += intval( ($rowHamster['chant'] + $rowHamster['danse'] + $rowHamster['musique'] + $rowHamster['humour']) / (10.*($rowHamster['niveau']+1)));
            }
        }
        
        // mise à jour pour l'hamster
        // --------------------------
        $rowHamster['experience'] ++;
                                        
        $query = "UPDATE hamster 
            SET sante = '".$nouvelleSante."',
            puissance = '".$nouveauPhysique."',
            beaute = '".$nouvelleBeaute."',
            moral =' ".$nouveauMoral."',
            poids = ".$nouveauPoids.",
            derniere_maj_popularite = ".$derniereMajPopularite.",
            popularite = ".$popularite.",
            experience = ".$rowHamster['experience']."
            WHERE hamster_id=".$rowHamster['hamster_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster : ', '', __LINE__, __FILE__, $query);
        }
    }
    $dbHT->sql_freeresult($result);
    
    // mise à jour des cages
    if ($hamstersAuRefuge == false) {
        $query = "SELECT cage_id, proprete FROM cage WHERE joueur_id=".$joueur_id;
        
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
        }
        
        while($rowCage=$dbHT->sql_fetchrow($result)) {
            
            // proprete de la cage en baisse
            $nbHamstersDansCetteCage = nbHamstersDansLaCage($rowCage['cage_id'],$joueur_id) ;
            $nouvelleProprete = $rowCage['proprete'] - $nbHamstersDansCetteCage;
            if ($nouvelleProprete < 0)
                $nouvelleProprete = 0;
            $query = "UPDATE cage 
            SET proprete = ".$nouvelleProprete."
            WHERE cage_id=".$rowCage['cage_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
            }        
        }
        $dbHT->sql_freeresult($result);
    }
    
    // update de la date de la dernière MAJ quotidienne
    $query = "UPDATE joueurs SET date_maj_hamster = ".$dateActuelle." WHERE joueur_id=".$joueur_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

function miseAJourPointsJoueur($joueur_id) {
    
    require_once "lstInstruments.php";
    
    global $dbHT,$dateActuelle, $lstInstruments, $puissanceAmuletteForce;
    $noteJoueur = 0;
    $penalitesJoueur = 0;
    
    $queryUser = "SELECT vip, image FROM joueurs WHERE joueur_id=".$joueur_id." LIMIT 1";
    
    if ( !($resultUser = $dbHT->sql_query($queryUser)) ){
        echo "Error sql : ".$query;
    }
    $rowUser=$dbHT->sql_fetchrow($resultUser);
    $dbHT->sql_freeresult($resultUser);    
    
    $query = "SELECT hamster_id, groupe_id, sante, pause, specialite, compagnon_id, puissance, beaute, moral, popularite, experience, danse, chant, sociabilite, humour, musique FROM hamster WHERE joueur_id=".$joueur_id;
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    
    while($rowHamster=$dbHT->sql_fetchrow($result)) {
        
        if ($rowHamster['pause'] > 0) // on vérifie que l'hamster n'est pas en position bloquée (gardiennage par exemple)
            continue;
        
        if (possedeAccessoire(ACC_AMULETTE_FORCE,$joueur_id) != -1 )
            $rowHamster['puissance'] *= $puissanceAmuletteForce;
        
        $noteHamster = $rowHamster['sante'] + $rowHamster['puissance'] + $rowHamster['beaute'] + $rowHamster['moral'] ;
        $noteHamster += $rowHamster['popularite'] + $rowHamster['experience']+ $rowHamster['danse']+ $rowHamster['chant']+ $rowHamster['sociabilite']+ $rowHamster['humour']+ $rowHamster['musique'];
        if ($rowHamster['specialite'] != -1)
            $noteHamster += $lstInstruments[$rowHamster['specialite']][INSTRUMENT_POINTS];
            
        if ($rowHamster['compagnon_id'] > 0) // 10 % de bonus si le hamster est marié 
            $noteHamster = $noteHamster + ($noteHamster/10);
            
        if ($rowHamster['sante'] == 0){ // 1 hamster malade => les points sont divisés par 2
            $noteHamster /= 2;
            $penalitesJoueur ++ ;
        }
        
        // si le joueur a un groupe
        if ($rowHamster['groupe_id'] != -1) {
            $queryGroupe = "SELECT note FROM groupes WHERE groupe_id = ".$rowHamster['groupe_id']. " LIMIT 1" ;
            if ( !($resultGroupe = $dbHT->sql_query($queryGroupe)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryGroupe);
            }
            $nbGroupes = $dbHT->sql_numrows($resultGroupe) ;
            
            if ($nbGroupes == 1){
                $rowGroupe=$dbHT->sql_fetchrow($resultGroupe);
                $noteHamster += $rowGroupe['note'];
            }
            $dbHT->sql_freeresult($resultGroupe);    
        }
        $noteHamster = round($noteHamster,0);
        $noteJoueur += $noteHamster;
        
        $query = "UPDATE hamster 
            SET note = ".$noteHamster."
            WHERE hamster_id=".$rowHamster['hamster_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
    }
    $dbHT->sql_freeresult($result);
    
    $queryCage = "SELECT proprete, etages, colonnes FROM cage WHERE joueur_id=".$joueur_id;
    if ( !($resultCage = $dbHT->sql_query($queryCage)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $queryCage);
    }
    while($rowCage=$dbHT->sql_fetchrow($resultCage)) {
        
        $noteJoueur += $rowCage['proprete']+($rowCage['etages'] * $rowCage['colonnes']);
    }
    $dbHT->sql_freeresult($resultCage);
    
    $queryAcc = "SELECT accessoire_id FROM accessoires WHERE joueur_id=".$joueur_id;
    if ( !($resultAcc = $dbHT->sql_query($queryAcc)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
    }
    $nbAccessoires = $dbHT->sql_numrows($resultAcc) ;
    $dbHT->sql_freeresult($resultAcc);
    $noteJoueur += $nbAccessoires;
        
    // si le joueur possede des hamsters malades, on le penalise en point
    if ($penalitesJoueur == 1)
        $noteJoueur *= 0.75 ;
    else if ($penalitesJoueur == 2)
        $noteJoueur *= 0.5 ;
    else if ($penalitesJoueur > 2)
        $noteJoueur *= 0.25 ;
        
    // si le joueur a une image dans son profil, 10% de points en plus
    if ($rowUser['image'] > 0)
        $noteJoueur = (110*$noteJoueur) / 100;
        
    // si le joueur est vip, 10% de points en plus
    if ($rowUser['vip'])
        $noteJoueur = (110*$noteJoueur) / 100;
    
    $noteJoueur = round($noteJoueur);
    
    // fin
    $query = "UPDATE joueurs 
        SET note = ".$noteJoueur.",
        date_maj_note = ".$dateActuelle."
        WHERE joueur_id=".$joueur_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    
    return $noteJoueur;
}

function miseAJourPointsGroupe($groupe_id){
 
    global $dbHT,$dateActuelle;
    $noteGroupe = 0;
    require_once "lstInstruments.php";
    
    $queryGroupe = "SELECT * FROM groupes WHERE groupe_id=".$groupe_id." LIMIT 1";
    
    if ( !($resultGroupe = $dbHT->sql_query($queryGroupe)) ){
        echo "Error sql : ".$queryGroupe;
    }
    $rowGroupe=$dbHT->sql_fetchrow($resultGroupe);
    $dbHT->sql_freeresult($resultGroupe);    
               
    // on cherche la liste des membres du groupe
    $query = "SELECT specialite FROM hamster WHERE groupe_id = ".$groupe_id;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbMembres = $dbHT->sql_numrows($result) ;
    
    // pour le calcul du combo
    $nbBatterie=$nbGuitares=$nbBasses=$nbPercu=$nbChants=$nbSaxo=$nbTrombone=0;
                    
    // passage en revue de tous les membres : on récupère les instruments, les points etc.
    while($rowMembre=$dbHT->sql_fetchrow($result)) {
        
        // instruments, combos
        if ($rowMembre['specialite'] == INST_BATTERIE)
            $nbBatterie++;
        else if ($rowMembre['specialite'] == INST_GUITARE || $rowMembre['specialite'] == INST_GUITARE2 || $rowMembre['specialite'] == INST_GUITARE_FOLK)
            $nbGuitares++;
        else if ($rowMembre['specialite'] == INST_CONTREBASSE || $rowMembre['specialite'] == INST_BASSE)
            $nbBasses++;
        else if ($rowMembre['specialite'] == INST_DJEMBE)
            $nbPercu++;
        else if ($rowMembre['specialite'] == INST_DJEMBE)
            $nbPercu++;
        else if ($rowMembre['specialite'] == INST_CHANT)
            $nbChants++;
        else if ($rowMembre['specialite'] == INST_SAXO)
            $nbSaxo++;
        else if ($rowMembre['specialite'] == INST_TROMBONE)
            $nbTrombone++;
    }
    
    // en fonction de l'expérience, il y a des points
    $noteGroupe += ($rowGroupe['experience']+$rowGroupe['ancienne_experience']);
    
    if ($rowGroupe['image'] > 0){ // si le groupe a une image, 20 % de points en plus
        $noteGroupe += ($noteGroupe / 5) ;
    }
    if (strlen($rowGroupe['description']) > 1000){ // si le groupe a une longue description, 10% de points en plus
        $noteGroupe += ($noteGroupe / 10) ;
    }        
    
    $combo = 0;
    $nbPointsCombo = 0;
    
    // combo rock :
    if ($nbBatterie > 0 && $nbGuitares > 1 && $nbBasses > 0 && $nbChants > 0){
        // combo groupe rock
        $nbPointsCombo =($noteGroupe/2);
        $noteGroupe += $nbPointsCombo;
        $combo = 1;
    }
    // combo jazz :
    if ($nbBatterie > 0 && $nbGuitares > 1 && $nbBasses > 0 && $nbSaxo > 0){
        // combo groupe jazz
        $nbPointsCombo =($noteGroupe/2);
        $noteGroupe += $nbPointsCombo;
        $combo = 2;
    }
    // combo chorale :
    if ($nbChants > 7){
        // combo groupe chorale
        $nbPointsCombo =($noteGroupe/2);
        $noteGroupe += $nbPointsCombo;
        $combo = 3;
    }
    
    // si dernière activité trop vieille, baisse des points
    $tempsEcoule = $dateActuelle - $rowGroupe['derniere_activite'];
    if ($tempsEcoule > 90000) { // plus d'1 jour sans activité
        $nbJoursSansActivite = ($tempsEcoule / 86400);
        $noteGroupe = $noteGroupe * (1. - min(10,$nbJoursSansActivite)*0.1) ; // on divise le score par le nombre de jour sans activité
    }    
    
    // si dernier concert trop vieux, baisse des points
    $tempsEcoule = $dateActuelle - $rowGroupe['dernier_concert'];
    if ($tempsEcoule > 259200) { // plus de 3 jours sans concert
        $nbJoursSansConcert = ($tempsEcoule / 259200);
        $noteGroupe = $noteGroupe * (1. - min(3,$nbJoursSansConcert)*0.1) ; // on divise le score par le nombre de jour sans concert
    }
    
    // si le groupe possède un manager, +20%
    if ($rowGroupe['possede_manager'])
        $noteGroupe = $noteGroupe * 1.2;
    
    $noteGroupe = round ( $noteGroupe + ( $noteGroupe * min(8,$nbMembres) / 8) ) ; // + il y a des membres, + la note se rapproche du double
    
    // mise à jour dans la base de données
    // et on en profite pour mettre à jour la date de dernière activité....
    $query = "UPDATE groupes SET note = ".$noteGroupe." WHERE groupe_id = ".$groupe_id;
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
        return;
    }
    $dbHT->sql_freeresult($result);
    
    return $noteGroupe;
}

function ajouterCageDansBDD($joueur_id, $type, $etages, $fond, $proprete, $toit, $colonnes, $profondeurs, $nom) {

    global $dbHT, $dateActuelle;
    
    // creation de la cage
    $query = "SELECT MAX(cage_id) as cage_id FROM cage";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    $query = "INSERT INTO cage VALUES ( '".$nouveauId."' , '".$joueur_id."' , '".$etages."' , '".$profondeurs."' , 
        '".$type."' , '".$fond."' , '".$proprete."', '".$toit."' , '".$colonnes."', '".$nom."' , 1 , 0
        ) ";
    $result = $dbHT->sql_query($query);
        
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    
    return $nouveauId;
}

// ajoute un accessoire : si etage ou colonne ou profondeur vaut -1, le placement dans la cage devient automatique
// (i.e. premiere place libre)
function ajouterAccessoireDansBDD($type, $joueur_id, $cage_id, $posX, $etage, $posY, $quantite) {
    
    global $dbHT, $dateActuelle;
    
    // verification de la position
    if ($etage == -1 || $posX == -1 || $posY == -1) {
        $query = "SELECT * FROM cage WHERE joueur_id='".$joueur_id."'";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
        }
        $cageRow=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
    }
    
    $query = "SELECT MAX(accessoire_id) as accessoire_id FROM accessoires";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $id = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    $query = "INSERT INTO accessoires VALUES ( '".$id."' , '".$type."' , '".$joueur_id."' , 
    '".$cage_id."' , '".$posX."' , '".$etage."' , '".$posY."'  , 
    ".$quantite." , '".$dateActuelle."'
    ) ";
    $result = $dbHT->sql_query($query);
    
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
        return -1;
    }
    else
        return $id;
}

// on peut indiquer l'age du hamster en secondes
function ajouterHamsterDansBDD($pseudo, $sexe, $caract1, $caract2, $type, $cage_id, $joueur_id, $age=0, $pere_id = -1, $mere_id = -1, $niveauJoueur = 1) {

    global $dbHT, $dateActuelle, $poidsMoyen ;
    
    $coton=0;
    $paille=0;
    $brindilles=0;
    $bois=0;
    $planCabane=0;
    $construction_nid = 0; // 0 correspond à une fabrication pas encore commencée du nid
    $construction_cabane = 0; // 0 correspond à une fabrication pas encore commencée de la cabane
        
    // creation de l'hamster
    $query = "SELECT MAX(hamster_id) as hamster_id FROM hamster";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        message_die(GENERAL_ERROR, 'Error in obtaining max hamster id', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    $date_naissance = $dateActuelle;
    if ($age > 0)
        $date_naissance -= $age; // on fixe l'age du hamster
    
    $coquet = 0;
    $fort = 0;
    $resistant = 0;
    $sociable = 0;
    $dragueur = 0;
    $batisseur = 0;
    
    // le max de chacun est 5
    if ($caract1 == "coquet")
        $coquet = 3;
    if ($caract1 == "fort")
        $fort = 3;
    if ($caract1 == "resistant")
        $resistant = 3;
    if ($caract1 == "sociable")
        $sociable = 3;
    if ($caract1 == "drague")
        $dragueur = 3;
    if ($caract1 == "batisseur")
        $batisseur = 3;
        
    if ($caract2 == "coquet")
        $coquet = 3;
    if ($caract2 == "fort")
        $fort = 3;
    if ($caract2 == "resistant")
        $resistant = 3;
    if ($caract2 == "sociable")
        $sociable = 3;
    if ($caract2 == "drague")
        $dragueur = 3;
    if ($caract2 == "batisseur")
        $batisseur = 3;

    $coquet = $coquet + rand(0,2) ;
    $fort = $fort + rand(0,2) ;
    $resistant = $resistant + rand(0,2) ;
    $sociable = $sociable + rand(0,2) ;
    $dragueur = $dragueur + rand(0,2) ;
    $batisseur = $batisseur + rand(0,2) ;
    
    $niveau = 0;
    $dernier_repas = $dateActuelle -  (3600*48) ; // on donne un peu de faim a l'hamster
    $dernier_soin_beaute = 0 ;
    $dernier_medicaments = 0 ;
    $dernieres_caresses = 0;
    $pause = 0;
    $inscrit_academy = 0;
    $poids = $poidsMoyen;
    $derniere_maj_popularite = 0;
    
    $dernier_examen=$experience=$academy_popularite=$academy_danse=$academy_chant=$academy_musique=$academy_sociabilite=$academy_humour=0;
    $derniere_confession=0;$note=0;
    $specialite = -1; $groupe_id = -1; $compagnon_id = 0;
    $fecondateur_id = -1;
    $inscrit_repet = 0;
    $inscrit_concert = 0;
    $derniere_activite = $dateActuelle;
    
    $energie = 15;            
    
    if ($niveauJoueur < NIVEAU_ROUE_MOTORISEE_12) {
        $maxSante = 10;
        $maxForce = 10;
        $maxMoral = 10;
        $maxBeaute = 10;
        $maxEnergie = 10;
        $sante = rand(5,7);
        $puissance = rand(5,7);
        $beaute_poil = rand(5,7);
        $moral = rand(5,7);
    }
    else if ($niveauJoueur < NIVEAU_DEFIS5_18) {
        $maxSante = 15;
        $maxForce = 15;
        $maxMoral = 15;
        $maxBeaute = 15;
        $maxEnergie = 15;
        $sante = rand(9,12);
        $puissance = rand(9,12);
        $beaute_poil = rand(9,12);
        $moral = rand(9,12);
    }
    else if ($niveauJoueur < NIVEAU_GROUPE_MUSIQUE) {
        $maxSante = 20;
        $maxForce = 20;
        $maxMoral = 20;
        $maxBeaute = 20;
        $maxEnergie = 20;
        $sante = rand(12,15);
        $puissance = rand(12,15);
        $beaute_poil = rand(12,15);
        $moral = rand(12,15);
    }
    else {
        $maxSante = 25;
        $maxForce = 25;
        $maxMoral = 25;
        $maxBeaute = 25;
        $maxEnergie = 25;
        $sante = rand(15,18);
        $puissance = rand(15,18);
        $beaute_poil = rand(15,18);
        $moral = rand(15,18);
    }
    
    $majEnergie = $dateActuelle;
    $chezleveto = 0;
    $agilite_init = 0;
    $rapidite_init = 0;
    $defense_init = 0;
    $endurance_init = 0;
    $attaque_init = 0;
    $gardien_init = 0;
    $maladie = 0;
    
    $query = "INSERT INTO hamster VALUES ($nouveauId,$joueur_id,$cage_id, 
        '$pseudo' ,$sexe,$type,$date_naissance, 
        $sante,$puissance,$coquet,
        $fort,$resistant,$sociable,
        $dragueur,$batisseur, 
        $beaute_poil,$niveau,$dernier_repas,
        $dernier_soin_beaute,$dernier_medicaments,
        $moral,$pause,$inscrit_academy,$poids,
        $coton,$paille,$brindilles,$construction_nid,
        $academy_popularite,$academy_danse,$academy_chant,$academy_musique,
        $academy_sociabilite,$academy_humour,$dernieres_caresses,$experience,
        $dernier_examen,$derniere_confession,$bois,$construction_cabane,$planCabane,
        $derniere_maj_popularite,$note,$specialite,$groupe_id,
        $compagnon_id,$fecondateur_id,$inscrit_repet,$inscrit_concert,
        $mere_id,$pere_id,$energie,$maxSante,$maxForce,$maxMoral,$maxBeaute,$maxEnergie,
        $majEnergie, $chezleveto , $derniere_activite,
        $agilite_init, $rapidite_init, $defense_init, $endurance_init, $attaque_init, $gardien_init,
        $maladie
        ) ";
    $result = $dbHT->sql_query($query);
        
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
        return -1 ;
    }
    
    return $nouveauId ;
}

function volumeCage($cage_id) {
    
    global $dbHT,$lst_cages;
    
    if (isset($lst_cages) && sizeof($lst_cages) > 0) { 
        $cageIndex = cageCorrespondante($cage_id);
        if ($cageIndex != -1) {
            $cage = $lst_cages[$cageIndex];
            return ($cage['etages'] * $cage['colonnes'] * $cage['profondeurs']);
        }
        else
            return 0;
    }
    else {
        $query = "SELECT etages, colonnes, profondeurs FROM cage WHERE cage_id=".$cage_id." LIMIT 1" ;
        $result = $dbHT->sql_query($query);
        if ( ! $result ) {
            message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
        }
        $cage = $dbHT->sql_fetchrow($result) ;
        return ($cage['etages'] * $cage['colonnes'] * $cage['profondeurs']);
    }    
}

function nbHamstersDansLaCage($cage_id, $joueur_id) {
    
    global $dbHT, $lst_hamsters, $nbHamsters;
    
    $nbHamstersDansLaCage = 0;
    
    if (isset($lst_hamsters) && sizeof($lst_hamsters) > 0) {
        for($ha = 0;$ha < $nbHamsters;$ha++) {
            if ($lst_hamsters[$ha]['cage_id'] == $cage_id && $lst_hamsters[$ha]['sante'] > 0)
                $nbHamstersDansLaCage ++ ;
        }        
    }
    else {
        $query = "SELECT hamster_id FROM hamster WHERE joueur_id=".$joueur_id." AND cage_id=".$cage_id." AND sante > 0";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbHamstersDansLaCage = $dbHT->sql_numrows($result) ;
        $dbHT->sql_freeresult($result);
    }
    return $nbHamstersDansLaCage ;
}

function yatilRoueDansLaCage($cage_id,$joueur_id) {

    global $dbHT,$nbAccessoires, $lst_accessoires;
    
    if (isset($lst_accessoires) && sizeof($lst_accessoires) > 0 ) {
    
        for($ac=0;$ac<$nbAccessoires;$ac++) {
            $rowAccessoires = $lst_accessoires[$ac];
            if ($rowAccessoires['cage_id'] == $cage_id){ 
                $type = $rowAccessoires['type'] ;
                if ($type == ACC_ROUE2 ||  $type == ACC_ROUE3 || $type == ACC_ROUE || $type == ACC_ROUE_AVEC_ENGRENAGE || $type == ACC_ROUE_AVEC_ENGRENAGE_PILE || $type == ACC_ROUE_AVEC_ENGRENAGE_PILE_FIL) 
                    return $ac;
            }
        }
        return -1 ;
    }
    else {
        // requete SQL
        $query = "SELECT accessoire_id FROM accessoires WHERE joueur_id=".$joueur_id." AND cage_id = ".$cage_id." AND type IN (".ACC_ROUE2.",".ACC_ROUE3.",".ACC_ROUE.",".ACC_ROUE_AVEC_ENGRENAGE.",".ACC_ROUE_AVEC_ENGRENAGE_PILE.",".ACC_ROUE_AVEC_ENGRENAGE_PILE_FIL.") LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbAcc = $dbHT->sql_numrows($result) ;
        $dbHT->sql_freeresult($result);
        
        if ($nbAcc == 1)
            return 0;
        else
            return -1;
    }
}

function yatilPierreRongerDansLaCage($cage_id,$joueur_id) {
    
    return yatilAccessoireDansLaCage($cage_id, ACC_PIERRE_RONGER,$joueur_id) ;
}

function yatilBiberonDansLaCage($cage_id,$joueur_id) {
    
    return yatilAccessoireDansLaCage($cage_id, 1,$joueur_id) ;
}

// indique s'il y a un accessoire de type accesssoireType dans la la cage cage_id
function yatilAccessoireDansLaCage($cage_id,$accessoireType,$joueur_id=-1) {
    
    global $dbHT,$nbAccessoires, $lst_accessoires;
    
    if (isset($lst_accessoires) && sizeof($lst_accessoires) > 0 ) {
    
        for($ac=0;$ac<$nbAccessoires;$ac++) {
            $rowAccessoires = $lst_accessoires[$ac];
            if ($rowAccessoires['cage_id'] == $cage_id && $rowAccessoires['type'] == $accessoireType) 
                return $ac;
        }
        return -1 ;
    }
    else {
        // requete SQL
        $query = "SELECT accessoire_id FROM accessoires WHERE joueur_id=".$joueur_id." AND cage_id = ".$cage_id." AND type = ".$accessoireType." LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbAcc = $dbHT->sql_numrows($result) ;
        $dbHT->sql_freeresult($result);
        if ($nbAcc == 1)
            return 0;
        else
            return -1;
    }
}

function affiche_age($dateNaissance) {
    
    global $dateActuelle;
    return afficherDuree($dateActuelle - $dateNaissance) ;
}

function cageCorrespondante($cage_id){

    global $lst_cages, $nbCages;
    
    if ($cage_id == -1)
        return -1;
        
    // on cherche la cage qui possederait cet accessoires pour le comptage
    for($cage=0;$cage<$nbCages;$cage++) {
        
        $cageRow = $lst_cages[$cage];
        
        if ($cageRow['cage_id'] == $cage_id)
            return $cage;
    }
    return -1; // erreur 
}

function hamsterCorrespondant($hamster_id){

    global $lst_hamsters, $nbHamsters;
    
    // on cherche l'hamster qui possede cet id
    for($ha=0;$ha<$nbHamsters;$ha++) {
        $hamsterRow = $lst_hamsters[$ha];
        if ($hamsterRow['hamster_id'] == $hamster_id)
            return $ha;
    }
    
    return -1; // erreur 
}

function accessoireCorrespondant($accessoire_id){

    global $lst_accessoires, $nbAccessoires;
    
    // on cherche l'accessoire qui possede cet id
    for($acc=0;$acc<$nbAccessoires;$acc++) {
        $accRow = $lst_accessoires[$acc];
        if ($accRow['accessoire_id'] == $accessoire_id)
            return $acc;
    }
    
    return -1; // erreur 
}
 
 // indique si le joueur possède l'accessoire en question, si oui, renvoie son indice dans lstAccessoires
function possedeAccessoire($type,$joueur_id=-1) {

    global $dbHT,$nbAccessoires, $lst_accessoires, $userdata;
    if (isset($lst_accessoires) && sizeof($lst_accessoires) > 0 ) {
        for($ac=0;$ac<$nbAccessoires;$ac++) {
            $rowAccessoires = $lst_accessoires[$ac];
            if ($rowAccessoires['type'] == $type) {
                return $ac;
            }
        }
        return -1 ;
    }
    else {
        if ($joueur_id == -1){
            if (isset($userdata))
                $joueur_id = $userdata['joueur_id'];
            else
                message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, "userdata non défini ou joueur_id non défini");
        }
        // requete SQL
        $query = "SELECT accessoire_id FROM accessoires WHERE joueur_id=".$joueur_id." AND type = ".$type." LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
        }
        $nbAcc = $dbHT->sql_numrows($result) ;
        $dbHT->sql_freeresult($result);
        if ($nbAcc == 1)
            return 1;
        else
            return -1;
    }
}

// si "indiquerQuantiteRestante" est false, ça renvoit la quantité réellement supprimée
function reduireQuantiteAccessoire($accessoire_id,$quantite, $indiquerQuantiteRestante = true) {
    
    global $dbHT, $lst_accessoires, $nbAccessoires;
    
    $accessoireIndex = accessoireCorrespondant($accessoire_id);
    if ($accessoireIndex != -1) {
        
        // on récupère la précédente quantité
        $query = "SELECT quantite FROM accessoires WHERE accessoire_id=".$accessoire_id. " LIMIT 1";
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $query);
        }
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result) ;
            
        $nouvelleQuantite = max(0,$row['quantite'] - $quantite);
        
        if ($nouvelleQuantite <= 0) // s'il n'y a plus de produit
            supprimerAccessoire($accessoire_id) ;
        else {
            $query = "UPDATE accessoires
                SET quantite = ".$nouvelleQuantite ."
                WHERE accessoire_id=".$accessoire_id. " LIMIT 1";
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $query);
            }
        }
        if ($indiquerQuantiteRestante)
            return $nouvelleQuantite;
        else
            return ($row['quantite']-$nouvelleQuantite);
    }
    else 
        return 0;
}


function getNbAccessoires($accessoire_type) {
    
    global $dbHT, $userdata;
    
    $query = "SELECT accessoire_id FROM accessoires WHERE type = ".$accessoire_type." AND joueur_id=".$userdata['joueur_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $query);
    }
    
    $result = $dbHT->sql_query($query);
    $nbAccessoires = $dbHT->sql_numrows($result) ;
    $dbHT->sql_freeresult($result) ;
    return $nbAccessoires;
}

function changerTypeAccessoire($accessoire_id,$type) {
    
    global $dbHT;
    
    $query = "UPDATE accessoires SET type = ".$type." WHERE accessoire_id=".$accessoire_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $query);
    }
    
    // il faut recharger les paramètres de cet accessoire
    $query = "SELECT a.*, c.* FROM accessoires a, config_accessoires c WHERE a.accessoire_id=".$accessoire_id." AND c.type=".$type;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error :', '', __LINE__, __FILE__, $query);
    }
    $result = $dbHT->sql_query($query);
    $rowAccessoire = $dbHT->sql_fetchrow($result) ;
    $dbHT->sql_freeresult($result) ;
    return $rowAccessoire;
}

function supprimerAccessoire($accessoire_id) {
    
    global $dbHT, $lst_accessoires, $nbAccessoires;
    
    $query = "DELETE FROM accessoires
    WHERE accessoire_id=".$accessoire_id;
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $query);
    }
    $accessoireIndex = accessoireCorrespondant($accessoire_id);
    $lst_accessoires[$accessoireIndex]['type']=-1; // equivalent a effacer l'accessoire
}

// indique sur 0 à 10 si un hamster a faim. 10 => vient de manger, 0 => meurt de faim
function faim($dateDernierRepas) {
    
    global $dateActuelle ;
    
    $nbHeures = ($dateActuelle-$dateDernierRepas)/3600 ;

    if ($nbHeures > 96)
        $nbHeures = 96;
        
    return (10*(96-$nbHeures)) / 96 ;
}

// nourrit l'hamster et retourne si c'est possible (0 => pas possible)
function nourrirHamster($hamster_id) {

    global $userdata, $dbHT, $dateActuelle,$lst_hamsters, $quantiteNourritureQuotidienneAvalee;
    
    // calcul de la bouffe restante
    $quantite_bouffe = $userdata['aliments'];

    $nouvelleQuantiteBouffe = $quantite_bouffe - $quantiteNourritureQuotidienneAvalee;
    if ($nouvelleQuantiteBouffe < 0)
        return 0;
    else { // on met à jour l'heure de son dernier repas
        $hamsterCorrespondant = hamsterCorrespondant($hamster_id) ;
        $lst_hamsters[$hamsterCorrespondant]['experience']++;
        
        $query = "UPDATE hamster 
        SET dernier_repas = ".$dateActuelle.",
        experience = ".$lst_hamsters[$hamsterCorrespondant]['experience']."        
        WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster_sante', '', __LINE__, __FILE__, $query);
        }
    
        if ( ($hamster_index = hamsterCorrespondant($hamster_id) ) == -1)
            message_die(GENERAL_ERROR, 'Error with hamster_index', '', __LINE__, __FILE__, "");
        else
            $lst_hamsters[$hamster_index]['dernier_repas']=$dateActuelle;
        $userdata['aliments'] = $nouvelleQuantiteBouffe;
        
        $lst_hamsters[$hamster_index]['poids'] += 2;
        
        $query = "UPDATE joueurs 
        SET aliments = ".$nouvelleQuantiteBouffe."
        WHERE joueur_id=".$userdata['joueur_id'];
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating joueur', '', __LINE__, __FILE__, $query);
        }
        
        $query = "UPDATE hamster 
        SET poids = ".$lst_hamsters[$hamster_index]['poids']."
        WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
        }
    
        return 1 ; // hamster nourri
    }
}

// met à jour la liste des objets trouvés (oeufs de paques par ex)
function metAJourObjetsTrouves($lstObjetsTrouves, $numeroObjet, $valeurObjet) {
    
    global $userdata;
    
    if ($userdata['lstObjetsTrouves'] == -1)
        return false;
        
    $masque = 1 ;
    $masque = $masque << $numeroObjet ;
    
    if ($valeurObjet == true) {
        $lstObjetsTrouves = $lstObjetsTrouves | $masque ;
    }
    else{
        $masque = ! $masque ;
        $lstObjetsTrouves = $lstObjetsTrouves & $masque ;
    }
    return $lstObjetsTrouves;
}

// récupère le statut d'un objet trouvé (oeuf de paques par ex)
function statutObjetTrouve($lstObjetsTrouves, $numeroObjet) {
    
    global $userdata;
    
    if ($userdata['lstObjetsTrouves'] == -1)
        return false;
    
    $masque = 1 ;
    $masque = $masque << $numeroObjet ;
    $val = $lstObjetsTrouves & $masque ;

    if ($val == 0)
        return false;
    else
        return true;
}

// récupère le statut d'un objet trouvé (oeuf de paques par ex)
function afficherObjet($nomObjet, $numeroObjet) {
    
    global $lstOeufs, $userdata, $hamster_id, $cage_id, $modeAffichage,$month ;
    
    if ($month != 4)
        return ; // uniquement mois d'avril
    
    if ($userdata['lstObjetsTrouves'] == -1)
        return false;
    
    if (statutObjetTrouve($userdata['lstObjetsTrouves'],$numeroObjet) == false) {
        $lien = "<a href=\"jeu.php?mode=m_accueil&oeufpaques=".$lstOeufs[$numeroObjet]."\" title=\"".T_("Clique sur l'oeuf pour le récupérer !")."\"><img src=\"images/".$nomObjet."/".$nomObjet."".$numeroObjet.".gif\" align=absmiddle></a>";
        return $lien; 
    }
    
}

// retourne true si tous les objets ont été trouvés
// sinon, retourne -1
function updateObjetsTrouvesJoueur($nbreObjets, $objet, $valeurObjet) { 

    global $userdata, $dbHT, $msg, $maskObjectifs ;
    
    if ($userdata['lstObjetsTrouves'] == -1)
        return false;
    
    $tousObjetsTrouves = false;
    $userdata['lstObjetsTrouves'] = metAJourObjetsTrouves($userdata['lstObjetsTrouves'],$objet,true) ;
    
    // on teste si tous les objectifs ont ete atteints
    if ($userdata['lstObjetsTrouves'] == $maskObjectifs[$nbreObjets]) {
        $userdata['lstObjetsTrouves'] = -1;
        $tousObjetsTrouves = true;
    }
    else {
        // combien d'objets sont encore à trouver ?
        $nbreOeufsTrouves = 0;
        for($obj=0;$obj < $nbreObjets;$obj++) {
            if (statutObjetTrouve($userdata['lstObjetsTrouves'],$obj) == true)
                $nbreOeufsTrouves++;
        }
        $msg .= str_replace("#1",($nbreObjets - $nbreOeufsTrouves),T_("<div>Il reste encore #1 oeuf(s) à trouver !</div>"));
    }
    
    $query = "UPDATE joueurs 
            SET lstObjetsTrouves = ".$userdata['lstObjetsTrouves']."
            WHERE joueur_id=".$userdata['joueur_id'];    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    if ($tousObjetsTrouves) {
        $msg .= "<div><table><tr><td><img src=\"images/oeufs/tout_trouve.gif\" align=absmiddle></td><td>".T_("Bravo ! Tu as trouvé tous les oeufs ! En récompense, tu reçois 3 bonbons et 3 chocolats pour le moral de tes hamsters ainsi que 15 pièces !")."</td></tr></table></div>" ;
        
        ajouterAccessoireDansBDD(ACC_BONBON, $userdata['joueur_id'], -1, -1, -1, -1, 3);
        ajouterAccessoireDansBDD(ACC_CALENDRIER_CHOCOLAT, $userdata['joueur_id'], -1, -1, -1, -1, 3);
        crediterPieces(15,false);
        
        $query = "UPDATE joueurs 
            SET lstObjetsTrouves = -1
            WHERE joueur_id=".$userdata['joueur_id'];    
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
        }
        
        return true;
    }
    else
        return false;
}


// modifie le statut d'un objectif pour un niveau donné
function setObjectifNiveau($objectifNiveau, $numeroObjectif, $valeurObjectif) {
    
    $masque = 1 ;
    $masque = $masque << $numeroObjectif ;
    
    if ($valeurObjectif == true) {
        $objectifNiveau = $objectifNiveau | $masque ;
    }
    else{
        $masque = ! $masque ;
        $objectifNiveau = $objectifNiveau & $masque ;
    }
    return $objectifNiveau;
}

// récupère le statut d'un objectif pour un niveau donné
function getObjectifNiveau($objectifNiveau, $numeroObjectif) {
    
    $masque = 1 ;
    $masque = $masque << $numeroObjectif ;
    $val = $objectifNiveau & $masque ;

    if ($val == 0)
        return false;
    else
        return true;
}

// retourne le nouveau niveau s'il y a eu un changement de niveau
// sinon, retourne -1
function updateNiveauJoueur($objectif,$valeurObjectif) { 

    //require_once "lstNiveaux.php" ;

    global $userdata, $dbHT,$msg, $lst_hamsters ;
    global $maskObjectifs, $infosNiveaux, $nbNiveaux ;
    
    if ($nbNiveaux < 10)
        echo "Pb avec les missions";
    
    $nouveauNiveau = false;
    $nbreObjectifs = sizeof ($infosNiveaux[$userdata['niveau']][NIVEAU_OBJECTIFS]);
    
    if ($objectif != -1)
        $userdata['niveau_objectif'] = setObjectifNiveau($userdata['niveau_objectif'],$objectif,true) ;

    // on teste si tous les objectifs ont ete atteints (test pour le négatif en raison d'un bug passé)
    // le test " & $maskObjectifs[$nbreObjectifs] " permet d'éviter les pbs en cas de rajout de missions ou d'objectifs, si le nombre d'objectifs a par exemple diminué.
    if ( ($userdata['niveau_objectif'] & $maskObjectifs[$nbreObjectifs]) == $maskObjectifs[$nbreObjectifs] || $userdata['niveau_objectif'] < 0) {
        $userdata['niveau_objectif'] = 0;
        $userdata['niveau'] = min($userdata['niveau']+1,$nbNiveaux-1);
        
        // pas de niveau 3
        if ($userdata['niveau'] == 3)
            $userdata['niveau'] = 4;
        $nouveauNiveau = true;
    }
    
    $query = "UPDATE joueurs 
            SET niveau_objectif = ".$userdata['niveau_objectif']."
            , niveau = ".$userdata['niveau']."
            WHERE joueur_id=".$userdata['joueur_id'];    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    if ($nouveauNiveau) {
        
        $msg = "<img src=\"images/hamster_inscription.gif\" style=\"vertical-align:middle; height:30px;\" alt=\"\" /> ".$infosNiveaux[($userdata['niveau']-1)][NIVEAU_DESCRIPTION_SI_REUSSI]."<br/>&nbsp;<br/>".$msg ;
        
        // selon le niveau, il faut changer les seuils pour les hamsters
        if (
        $userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12 || 
        $userdata['niveau'] == NIVEAU_DEFIS5_18 || 
        $userdata['niveau'] == NIVEAU_TENNIS) {
            
            $nouveauSeuil=0;
            if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12)
                $nouveauSeuil = 15;
            else if($userdata['niveau'] == NIVEAU_DEFIS5_18)
                $nouveauSeuil = 20;
            else if($userdata['niveau'] == NIVEAU_TENNIS)
                $nouveauSeuil = 25;
                
            $query = "UPDATE hamster SET maxForce = $nouveauSeuil, maxSante = $nouveauSeuil, maxMoral = $nouveauSeuil, maxBeaute = $nouveauSeuil, maxEnergie = $nouveauSeuil WHERE joueur_id=".$userdata['joueur_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
            }
        }
        
        // si c'est le niveau de la maladie, on rend le hamster malade
        if ($userdata['niveau'] == NIVEAU_MALADIE_CHORINTHE) {
            
            $query = "UPDATE hamster SET maladie = 1, sante = sante/2, moral = moral-2 WHERE hamster_id=".$lst_hamsters[0]['hamster_id'];
            $dbHT->sql_query($query);
        }
        else if ($userdata['niveau'] == NIVEAU_MALADIE_STALACTIC) {
            
            $query = "UPDATE hamster SET maladie = 2, sante = sante/3, moral = moral-2 WHERE hamster_id=".$lst_hamsters[0]['hamster_id'];
            $dbHT->sql_query($query);
        }
        else if ($userdata['niveau'] == NIVEAU_MALADIE_FIEVRE) {
            
            $query = "UPDATE hamster SET maladie = 3, moral = 4 WHERE joueur_id=".$userdata['joueur_id'];
            $dbHT->sql_query($query);
        }
        
        // on crédite de 10 pièces
        if (0 && $userdata['niveau'] < 10) {
            crediterPieces(5,false);
            $msg .= "<br/>&nbsp;<br/>".T_("Pour te féliciter, tu reçois aussi")." 5 ".IMG_PIECE.". ".T_("Bonne chance pour la prochaine mission !");
        }
        
        return $userdata['niveau'] ;
    }
    else
        return -1;
}

// deplace un accessoire d'une cage a l'autre. Met a jour les variables etage, colonne et profondeur.
// si succes, renvoie la cage
// sinon, renvoie -1
function mettreAccessoireDansLaCage($accessoire_id, $precedenteCage_id, $nouvelleCage_id) {
    
    global $dbHT, $lst_accessoires, $lst_cages, $userdata;
        
    $accessoire_index = accessoireCorrespondant($accessoire_id);
    $acc = $lst_accessoires[$accessoire_index];
    $accBaseY = $acc['decal_y'];
    $accWidth = $acc['img_width']* $acc['echelle'];
    $accHeight = $acc['img_height']* $acc['echelle'];
    $precedente_cage_index = cageCorrespondante($precedenteCage_id);
    $nouvelle_cage_index = cageCorrespondante($nouvelleCage_id);
    $etage=0;$posX=0;$posY=0;
    
    // on vérifie si l'accessoire est déplaçable
    if ($acc['danslacage'] == 0)
        $nouvelleCage_id = -1; // on laisse dans le bac
    else {
        // si on deplace vers une cage, on calcule correctement la position de l'objet
        if ($nouvelleCage_id != -1) {

            corrigePositionAcccessoire($posX,$posY,$etage,$lst_cages[$nouvelle_cage_index], $accWidth, $accHeight, $accBaseY);
            
            // si on a deplace une roue, on modifie eventuellement le niveau du joueur
            $type = $acc['type'];
            if ( ($type == ACC_ROUE || $type == ACC_ROUE2 || $type == ACC_ROUE3) && $userdata['niveau']==NIVEAU_GESTION_ETAGE) { 
                updateNiveauJoueur(2,true) ;
            }
            // si on a déplacé une écuelle, on modifie éventuellement le niveau du joueur
            else if ($type == ACC_ECUELLE && $userdata['niveau']==NIVEAU_GESTION_ETAGE) { 
                updateNiveauJoueur(0,true) ;
            }
        }
    }
    
    // mise à jour de la position de l'accessoire
    $lst_accessoires[$accessoire_index]['posX'] = $posX;
    $lst_accessoires[$accessoire_index]['etage'] = $etage;
    $lst_accessoires[$accessoire_index]['posY'] = $posY;
    $lst_accessoires[$accessoire_index]['cage_id'] = $nouvelleCage_id;
    
    // s'il y a vraiment eu mouvement d'une cage a l'autre...
    if ($nouvelle_cage_index != $precedente_cage_index) {
        
        // on met a jour le nbre d'accessoires dans les 2 cages (anciennes et nouvelles)
        if ($nouvelle_cage_index != -1)
            $lst_cages[$nouvelle_cage_index]['nbAccessoires'] ++ ;
        if ($precedente_cage_index != -1)
            $lst_cages[$precedente_cage_index]['nbAccessoires'] -- ;        
    }
    
    $query = "UPDATE accessoires SET posX = ".$posX.",etage = ".$etage.",posY = ".$posY.", cage_id = ".$nouvelleCage_id." WHERE accessoire_id=".$accessoire_id;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error moving accessory in accessoire ', '', __LINE__, __FILE__, $query);
    }
    $dbHT->sql_freeresult($result);

}

function majEtapeInscription($inscription) {
    
    global $userdata, $dbHT;
    
    $userdata['etape_inscription'] = max($userdata['etape_inscription'], $inscription) ;
    $query = "UPDATE joueurs 
        SET etape_inscription = ".$userdata['etape_inscription']."
        WHERE joueur_id=".$userdata['joueur_id'];
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating cage_data', '', __LINE__, __FILE__, $query);
    }
}

function afficherPasAssezArgent($message, $precedentePage) {
    
    global $lang, $univers;
    
    if (!isset($univers))
        $univers = 0;
    
    $val = "<table cellpadding=10 align=center><tr valign=top><td><img src=\"images/plusdesous.gif\" alt=\"".T_("plus de sous")."\"></td><td>";
    $val .= $message."<br>&nbsp;</br>";
    $val .= "<strong>".T_("Tu dois attendre ton prochain salaire !")."</strong>";
    $val .= "<br/>&nbsp;<br/>".T_("Pour gagner du temps, tu as plusieurs possibilités")." : <br/>&nbsp;<br/> ";
    if ($lang == "fr")
        $val .= "- <a href=\"quizz.php?precedentePage=".$precedentePage."\">".T_("jouer au quizz</a> (une fois par jour maximum)")."<br/>";
    $val .= "- <a href=\"jeu.php?mode=m_aide#plusdargent\">".T_("voir l'aide qui donne plein d'astuces")."</a><br/>";
    $val .= "- <a href=\"jeu.php?mode=m_banque&amp;univers=$univers&amp;precedentePage=$precedentePage\">".T_("ou acheter des pièces via la banque")."</a>";
    $val .= "</td></tr></table>";
    return $val;
}

// corrige la position de l'accessoire si celui-ci est hors cage
// renvoie 1 si une correction a été effectué, 0 sinon
function corrigePositionAcccessoire(&$posX,&$posY,&$etage, $cageRow, $accessoireWidth, $accessoireHeight, $accessoireBaseY) {
    
    global $infosCage ;
    
    $largeurBac = $infosCage[$cageRow['colonnes']][INFO_CAGE_LARGEUR_BAC] ;
    $largeurImageCage = $infosCage[$cageRow['colonnes']][INFO_CAGE_LARGEUR_IMAGE];
    $hauteurImageCage = $infosCage[$cageRow['colonnes']][INFO_CAGE_HAUTEUR_IMAGE];
    $hauteurBac = $infosCage[$cageRow['colonnes']][INFO_CAGE_HAUTEUR_BAC];
    $ratioInclinaison = $infosCage[$cageRow['colonnes']][INFO_CAGE_RATIO];
    
    $correction = 0;
    
    // si l'objet est trop monté par rapport à l'étage
    if ($posY < - $accessoireBaseY){
        
        // on regarde s'il n'y a pas eu intention volontaire de changement d'étage (marge de tolérance pour le saut  : 50 pix)
        //if ($posY < - (50 + $accessoireBaseY) && $etage < $cageRow['etages']-1)
        //    $etage++;
            
        $posY = - $accessoireBaseY;
        $correction = 1;
    }
    // si l'objet est trop descendu par rapport à l'étage
    else if ($posY > ($hauteurImageCage-$hauteurBac-$accessoireHeight)){
        // on regarde s'il n'y a pas eu intention volontaire de changement d'étage (marge de tolérance pour le saut  : 50 pix)
        //if ($posY > (50+$hauteurImageCage-$hauteurBac-$accessoireHeight) && $etage > 0)
        //    $etage--;
        $posY = $hauteurImageCage-$hauteurBac-$accessoireHeight;
        $correction = 1;
    }    
    
    // on vérifie que l'objet n'a pas dépassé le bord gauche de la cage, à calculer selon sa position verticale.
    
    // le coin en haut à gauche de l'objet est considéré pour ce calcul
    $bordGaucheDeLaCage = $ratioInclinaison*($hauteurImageCage-$posY-$hauteurBac-$accessoireBaseY) ;
    
    // pour le bord droit, on considère le coin inférieur droit de l'objet pour ce calcul
    $bordDroitDeLaCage = $largeurBac - $accessoireWidth + $ratioInclinaison*($hauteurImageCage-$posY-$hauteurBac-$accessoireHeight);
    
    if ($posX < $bordGaucheDeLaCage){
        $posX = round($bordGaucheDeLaCage);
        $correction = 1;
    }
    else if ($posX > $bordDroitDeLaCage){
        $posX = $bordDroitDeLaCage;
        $correction = 1;
    }
    if ($etage < 0){
        $etage = 0;
        $correction = 1;
    }
    else if ($etage > $cageRow['etages']-1){
        $etage = $cageRow['etages']-1 ;
        $correction = 1;
    }    
    
    return $correction;
}

function lienPagePrec($pagePrecedente="m_accueil", $hamster_id=-1, $cage_id=-1, $accessoire_id=-1) {
    
    return "&amp;pagePrecedente=".$pagePrecedente."&amp;hamster_id=".$hamster_id."&amp;cage_id=".$cage_id."&amp;accessoire_id=".$accessoire_id;
    
}

function ameliorerPoids($diffPoids, $poidsActuel) {

    global $poidsMoyen ;

    if ($poidsMoyen > $poidsActuel ) // si l'hamster est maigre, il prend du muscle
        $poidsActuel += $diffPoids;
    else // sinon, il maigrit !
        $poidsActuel -= $diffPoids;
    
    return $poidsActuel;
}

function verifierIngredientsNid($hamster_id) {

    global $dbHT,$lst_hamsters, $constructionNidCommencee;
    
    $dateActuelle = time();

    // on vérifie s'il peut commencer la construction du nid
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    $hamRow = $lst_hamsters[$hamsterIndex];
    if ($hamRow['possede_coton'] == 1 && $hamRow['possede_brindilles'] == 1 && $hamRow['possede_paille'] == 1) {
        // c'est parti pour le nid
        $lst_hamsters[$hamsterIndex]['possede_coton'] = 0;
        $lst_hamsters[$hamsterIndex]['possede_paille'] = 0;
        $lst_hamsters[$hamsterIndex]['possede_brindilles'] = 0;
        $lst_hamsters[$hamsterIndex]['construction_nid'] = $dateActuelle;
        
        $query = "UPDATE hamster 
            SET possede_coton = 0,
                    possede_paille = 0,
                    possede_brindilles = 0,
                    construction_nid = ".$dateActuelle."
                WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ nid', '', __LINE__, __FILE__, $query);
        }
        $constructionNidCommencee = true;
    }
}

function verifierIngredientsCabane($hamster_id) {

    global $dbHT,$lst_hamsters, $constructionCabaneCommencee;
    
    $dateActuelle = time();

    // on vérifie s'il peut commencer la construction du nid
    $hamsterIndex = hamsterCorrespondant($hamster_id);
    $hamRow = $lst_hamsters[$hamsterIndex];
    if ($hamRow['possede_plan_cabane'] == 1 && $hamRow['possede_bois'] == 1) {
        // c'est parti pour la cabane
        $lst_hamsters[$hamsterIndex]['possede_bois'] = 0;
        $lst_hamsters[$hamsterIndex]['construction_cabane'] = $dateActuelle;
        
        $query = "UPDATE hamster 
            SET possede_bois = 0,
                    construction_cabane = ".$dateActuelle."
                WHERE hamster_id=".$hamster_id;
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating hamster _ cabane', '', __LINE__, __FILE__, $query);
        }
        $constructionCabaneCommencee = true;
    }
}

function calculerDateFinConstruction($dateDebut, $initBatisseur) {
    global $dureeFabricationNidCabane;
    return $dateDebut + ($dureeFabricationNidCabane / ($initBatisseur+1)) ;
}
function isFabricationFinie($dateDebut, $initBatisseur) {
    
    global $dateActuelle;
    
    $dateFin = calculerDateFinConstruction($dateDebut, $initBatisseur);
    if ($dateFin < $dateActuelle)
        return true;
    else
        return false;
}
function calculerProgressionFabrication($dateDebut, $initBatisseur) {
    
    global $dateActuelle;
    
    $dateFin = calculerDateFinConstruction($dateDebut, $initBatisseur);
    $progression = 100. * ( 1. - (($dateFin - $dateActuelle) / ($dateFin - $dateDebut))) ;
    return min(100,round($progression));
}

function envoyerMail($destinataire,$sujet,$message,$fctAppelante) {
    
    // Inclusion de la bibliotheque
    require_once("phpmailer/class.phpmailer.php");
    
    // Création de l'objet PhpMailer
    $mail = new PHPMailer();
    
//    if ($fctAppelante == "inscription" || $fctAppelante == "activation") {
//        require_once("phpmailer/class.smtp.php");
//        $mail->IsSMTP();             // On se sert d'un SMTP
//        $mail->SMTPSecure = "ssl";  // La connexion est cryptée
//        $mail->Host = "smtp.gmail.com:465";  // Adresse et port du serveur de mail
//        $mail->SMTPAuth = true;     // Activation de l'authentification
//        $mail->Port = 465;
//        $mail->Username = "hamsteracademy@gmail.com";  // Nom d'utilisateur 
//        $mail->Password = "3q5acp3y"; // Mot de passe
//    }
//    else
    $mail->IsMail();

    $mail->From = "contact@hamsteracademy.fr"; // L'email de l'expediteur
    $mail->FromName = "Hamster Academy";  // Le nom de l'expediteur
    $mail->WordWrap = 80; // Nombre de caractère maximum par ligne
    $mail->CharSet = 'UTF-8';

//    $mail->AddAddress("dupont.romain@gmail.com"); // Adresse et nom du destinataire
//    $mail->Subject = "mail envoyé par ".$fctAppelante; // Sujet
//    $mail->Body = "Pour ".$destinataire. " : " .$message;
//    $mail->Send();
//    
    $mail->ClearAddresses();
    $mail->AddAddress($destinataire); // Adresse et nom du destinataire
    $mail->Subject = $sujet;
    $mail->Body = $message;
    $mail->Send();
    $mail->ClearAddresses();
    unset($mail);
    
    return 1;//@mail($destinataire, $sujet, $message, $headerMailFromHamsterAcademy);
}

function envoyerMessagePrive($joueur_dest_id, $message_texte, $joueur_exp_id = HAMSTER_ACADEMY_ID, $avecMail = true) {
    
    global $dbHT, $dateActuelle, $base_site_url;
    
    // on récupère le nouveau id du message
    $query = "SELECT MAX(message_id) as message_id FROM messages";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    // on ajoute le message
    $query = "INSERT INTO messages VALUES (".$nouveauId." , ".$joueur_exp_id.", ".$joueur_dest_id." , '".$message_texte."' , 
        ".$dateActuelle." , 1 ) ";
    if ( ! $dbHT->sql_query($query) ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    
    // on envoie un mail au destinataire (que si son email est valide)(et si c'est le premier message)(et s'il n'est pas en train de jouer)
    if ($avecMail) {
        
        $query = "SELECT pseudo, email FROM joueurs WHERE 
            joueur_id=".$joueur_dest_id. " AND 
            email_active = 1 AND
            prevenir_nouveau_message = 1 AND
            user_lastvisit < ".($dateActuelle-300)." 
            LIMIT 1";
        $result = $dbHT->sql_query($query);
        if ( ! $result ) {
            echo "Erreur SQL : ".$query."<br>" ;
        }
        $nbEmail = $dbHT->sql_numrows($result) ;
        if ($nbEmail == 1) {
            $rowDest = $dbHT->sql_fetchrow($result);

            $emailDest = $rowDest['email'];
            $nomDest = $rowDest['pseudo'];

            if ($joueur_dest_id == HAMSTER_ACADEMY_ID) {
                $TO = "dupont.romain@gmail.com";
                $subject = getPseudoFromId($joueur_exp_id). " a envoyé un message sur Hamster Academy..." ;
                $message = "Message reçu de la part de ".getPseudoFromId($joueur_exp_id)." :\n"."\"".stripslashes(html_entity_decode(strip_tags($message_texte)))."\"\n\n";
                envoyerMail($TO,$subject,$message,"envoyerMessagePrive_HA");
            }
            else {
                $TO = $emailDest;
                $subject = "";
                $message = "";
                if ($joueur_exp_id == HAMSTER_ACADEMY_ID) {
                    $subject = T_("Tu as reçu un message sur Hamster Academy...") ;
                    $message = T_("Bonjour")." ".$nomDest.", \n\n".T_("Tu viens de recevoir un message :")."\n\n";
                }
                else {
                    $subject = getPseudoFromId($joueur_exp_id). " ".T_("t'a envoyé un message sur Hamster Academy...") ;
                    $message = T_("Bonjour")." ".$nomDest.", \n\n".getPseudoFromId($joueur_exp_id)." ".T_("vient de t'envoyer un message :")."\n\n";
                }
                $message .= "\"".stripslashes(html_entity_decode(strip_tags($message_texte)))."\"\n\n";
                $message .= T_("Le lien pour répondre (Ville -> La Poste) :")." ".$base_site_url."\n\n";
                $message .= "--\nHamster Academy\n";
                envoyerMail($TO,$subject,$message,"envoyerMessagePrive");
            }
        }
        $dbHT->sql_freeresult($result);
    }
    
    // on prévient le destinataire dans le jeu qu'il a un mail
    $query = "UPDATE joueurs SET messageNonLu = 1 WHERE joueur_id=".$joueur_dest_id." LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in updating joueur', '', __LINE__, __FILE__, $query);
    }
    $dbHT->sql_freeresult($result);    
    
}

// affiche un lien javascript qui affiche le profil du joueur dans un pop-up
function afficherLienProfil($joueur_id, $texte) {
    echo returnLienProfil($joueur_id, $texte);
}

function afficherLienProfilHamster($hamster_id, $texte) {
    echo returnLienProfilHamster($hamster_id, $texte);
}
function returnLienProfilHamster($hamster_id, $texte) {
    return "<a href=\"#\" onclick=\"javascript: pop('afficheHamster.php?hamster_id=".$hamster_id."',null,600,800); return false;\" title=\"".T_("Voir le hamster")."\">".$texte."</a>";
}

function returnLienCage($cage_id, $texte) {
    return "<a href=\"afficheCage.php?cage_id=".$cage_id."\" onclick=\"javascript: pop('afficheCage.php?cage_id=".$cage_id."',null,600,800); return false;\" title=\"Voir la cage\">".$texte."</a>";
}

function returnLienLuiEcrire($joueur_id) {
    return "<a href=\"jeu.php?mode=m_messages&amp;envoyerMessage=1&amp;dest_joueur_id=".$joueur_id."\" target=\"_blank\"><img src=\"images/stylo.gif\" width=\"30\" alt=\"écrire\" style=\"vertical-align:middle;\" /> ".T_("Lui écrire")."</a>";
}

// affiche qu'il n'y a plus d'énergie 
function afficherPlusDEnergie($msgTxt) {
    
    return afficherNouvelle("energie.gif",T_("Ton hamster n'a pas assez d'énergie pour ").$msgTxt."... ".T_("Il faut attendre 20 minutes pour récupérer 1 point d'énergie. Sinon, tu peux en acheter en boutique !"));
}

function afficherPasLeBonJour($afficherNouvelle = true) {
    
    global $lang ;
    
    $msg = T_("Tu ne peux pas faire cette activité aujourd'hui... Regarde")." <a href=\"".($lang == "fr" ? "aide" : "help")."/doku.php?id=start#quel_est_l_agenda_de_la_semaine\">".T_("l'agenda dans l'aide")."</a>.";
    
    if ($afficherNouvelle)
        return afficherNouvelle("agenda.gif",$msg);
    else
        return $msg;
}

// calcul du code d'activation pour valider l'email: utilise l'id du joueur et son email
function calculCodeActivation($joueur_id, $email) {

    // le "5HA" pour rendre le calcul md5 plus complexe à retrouver 
    return md5($joueur_id.$email."5HA".$joueur_id);    
}

function ajouterInteraction($demandeur_id, $receveur_id, $type, $message, $status) {

    global $dbHT, $dateActuelle;
    
    // creation de l'interaction
    $query = "SELECT MAX(interaction_id) as interaction_id FROM interactions";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    $query = "INSERT INTO interactions VALUES (".$nouveauId." , ".$type.",".$status.",".$demandeur_id." , ".$receveur_id.", ".$dateActuelle.",'".$message."') ";
    if ( ! $dbHT->sql_query($query) ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    return $nouveauId;
}

// on dissout le groupe : pour tous les membres, on remet à -1 leur groupe_id et on leur envoie un message
function dissoudreGroupe($groupe_id_a_dissoudre, $leader_id = -1) {
    
    global $dbHT;
    
    $queryGroupe = "SELECT * FROM groupes WHERE groupe_id = ".$groupe_id_a_dissoudre. " LIMIT 1" ;
    if ( !($resultGroupe = $dbHT->sql_query($queryGroupe)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryGroupe);
    }
    $rowGroupe=$dbHT->sql_fetchrow($resultGroupe);
    $dbHT->sql_freeresult($resultGroupe);  
    
    $query = "SELECT hamster_id, joueur_id FROM hamster WHERE groupe_id = ".$groupe_id_a_dissoudre." GROUP BY joueur_id";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $messageAEnvoyer = "";
    if ($leader_id != -1)
        $messageAEnvoyer = str_replace("#1",$rowGroupe['nom'],T_("(Message automatique) Le groupe #1 a été dissous par son leader. Tu n'as plus de groupe."));
    else
        $messageAEnvoyer = str_replace("#1",$rowGroupe['nom'],T_("(Message automatique) Le groupe #1 a été dissous car il est resté trop longtemps inactif. Tu n'as plus de groupe : dépêche-toi d'en rejoindre un nouveau !"));
        
    $messageAEnvoyer = mysql_real_escape_string($messageAEnvoyer);
    
    while($rowMembre=$dbHT->sql_fetchrow($result)) {
        // envoi d'un message pour prévenir
        if ($leader_id != -1)
            envoyerMessagePrive($rowMembre['joueur_id'], $messageAEnvoyer, $leader_id) ;
        else
            envoyerMessagePrive($rowMembre['joueur_id'], $messageAEnvoyer) ;
    }
    
    // on supprime l'image si elle existe
    if ($rowGroupe['image'] > 0) {
        unlink("images/groupes/".$groupe_id_a_dissoudre.".jpg");
    }
    
    // on supprime le groupe pour chaque membre
    $query = "UPDATE hamster SET groupe_id = -1, inscrit_repet = 0, inscrit_concert = 0 WHERE groupe_id = ".$groupe_id_a_dissoudre;
    if ( !$dbHT->sql_query($query)){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    
    // on supprime le groupe de la base de donnée
    $query = "DELETE FROM groupes WHERE groupe_id = ".$groupe_id_a_dissoudre;
    if ( !$dbHT->sql_query($query)){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    
    // on supprime tous les messages du bulletin
    $query = "DELETE FROM groupe_bulletin WHERE groupe_id = ".$groupe_id_a_dissoudre;
    if ( !$dbHT->sql_query($query)){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
}

function getImageAccessoire($type) {
    
    global $dbHT;
    
    if ( ! isset($type) || ! is_numeric($type))
        return "";
    
    $queryImage = "SELECT image FROM config_accessoires WHERE type=".$type." LIMIT 1";
    if ( !($resultImage = $dbHT->sql_query($queryImage)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $queryImage);
    }
    $instrumentImage=$dbHT->sql_fetchrow($resultImage);
    $dbHT->sql_freeresult($resultImage);
    return $instrumentImage['image'];
}

function getImageSpecialiteFoot($specialite) {
    
    return "hamster_reduc_foot.gif";
}

function vendreAccessoire($accessoire_id, $touteslesquantites = false) {
    
    global $dbHT, $lst_accessoires, $nbAccessoires, $userdata;
    
    $accessoireIndex = accessoireCorrespondant($accessoire_id);
    if ($accessoireIndex != -1) {

        // on crédite le portefeuille du joueur
        $query = "SELECT prix, quantite_initiale FROM config_accessoires WHERE type=".$lst_accessoires[$accessoireIndex]['type']." LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        $objet=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        $prixDeVente = intval($objet['prix'] / (2 * $objet['quantite_initiale']) );
        
        if ($touteslesquantites) {
            
            // on récupère la quantité
            $queryQuantite = "SELECT quantite FROM accessoires WHERE accessoire_id=".$accessoire_id. " LIMIT 1";
            if ( !($dbHT->sql_query($queryQuantite)) ){
                message_die(GENERAL_ERROR, 'Error deleting accessory', '', __LINE__, __FILE__, $queryQuantite);
            }
            $result = $dbHT->sql_query($queryQuantite);
            $row = $dbHT->sql_fetchrow($result) ;
            $dbHT->sql_freeresult($result);
                
            $prixDeVente *= $row['quantite'] ;
            supprimerAccessoire($accessoire_id) ;
        }
        else 
            reduireQuantiteAccessoire($accessoire_id,1);
        
        crediterPieces($prixDeVente,false); 
        
        return $prixDeVente;        
    }
    return 0;
}

function ajouterStats($joueur_id, $action, $val, $date) {
    
    global $dbHT;
    
    $query = "INSERT INTO stats VALUES(".$joueur_id.",'".$action."','".$val."',".$date.")";
    if ( ! $dbHT->sql_query($query)) {
        message_die(GENERAL_ERROR, 'Error with stats', '', __LINE__, __FILE__, $query);
    }    
}

// retourne -1 s'il ne le trouve pas
function getJoueurIdFromPseudo($pseudo) {
    
    global $dbHT;
    
    $query = "SELECT joueur_id FROM joueurs WHERE pseudo = '".$pseudo."' LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    if ($dbHT->sql_numrows($result) == 1) {
        $row=$dbHT->sql_fetchrow($result);
        return $row['joueur_id']; 
    }
    else {
        return -1;
    }
    $dbHT->sql_freeresult($result);
}

function getPseudoFromId($id) {
    
    global $dbHT;
    
    $query = "SELECT pseudo FROM joueurs WHERE joueur_id = ".$id." LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    if ($dbHT->sql_numrows($result) == 1) {
        $row=$dbHT->sql_fetchrow($result);
        return $row['pseudo']; 
    }
    else {
        return "";
    }
    $dbHT->sql_freeresult($result);
}

function getNomHamsterFromId($id) {
    
    global $dbHT;
    
    $query = "SELECT nom FROM hamster WHERE hamster_id = ".$id." LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    if ($dbHT->sql_numrows($result) == 1) {
        $row=$dbHT->sql_fetchrow($result);
        return $row['nom']; 
    }
    else {
        return "";
    }
    $dbHT->sql_freeresult($result);
}


function ajouterAmi($joueur1_id, $joueur2_id) {
    
    global $dbHT, $userdata;
    
    // on vérifie que le couple n'existe pas déjà
    $query = "SELECT joueur_id FROM amis WHERE (joueur_id = ".$joueur1_id." AND ami_de = ".$joueur2_id.") OR (joueur_id = ".$joueur2_id." AND ami_de = ".$joueur1_id.") LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    if ($dbHT->sql_numrows($result) == 0) {
        
        // on ajoute les amis à la base de données
        $query = "INSERT INTO amis VALUES(".$joueur1_id.",".$joueur2_id.")";
        $dbHT->sql_query($query);
        $query = "INSERT INTO amis VALUES(".$joueur2_id.",".$joueur1_id.")";
        $dbHT->sql_query($query);
        
        // on prévient les joueurs que leur amitié est faite
        $pseudo_joueur_1 = getPseudoFromId($joueur1_id) ;
        $pseudo_joueur_2 = getPseudoFromId($joueur2_id) ;

        $message_texte = str_replace("#1",$pseudo_joueur_1,T_("(Message automatique) #1 vient de t ajouter à sa liste d amis !"));
        envoyerMessagePrive($joueur2_id, $message_texte);
        $message_texte = str_replace("#1",$pseudo_joueur_2,T_("(Message automatique) #1 vient de t ajouter à sa liste d amis !"));
        envoyerMessagePrive($joueur1_id, $message_texte);
    
        // on supprime toutes les demandes d'amitié en attente
        $query = "DELETE FROM amis_attente WHERE (demandeur_id=".$joueur1_id." AND sollicite_id=".$joueur2_id.") OR (demandeur_id=".$joueur2_id." AND sollicite_id=".$joueur1_id.")";
        $dbHT->sql_query($query);
    }
    // on supprime toutes les demandes d'amitié en attente
    $query = "DELETE FROM amis_attente WHERE (demandeur_id=".$joueur1_id." AND sollicite_id=".$joueur2_id.") OR (demandeur_id=".$joueur2_id." AND sollicite_id=".$joueur1_id.")";
    $dbHT->sql_query($query);
    
    $dbHT->sql_freeresult($result);
}

function getListeAmis() {
    
    global $dbHT, $userdata;
    
    $query = "SELECT a.ami_de, j.pseudo, j.joueur_id, j.image, j.liste_rouge FROM amis a, joueurs j WHERE a.joueur_id=".$userdata['joueur_id']." AND j.joueur_id = a.ami_de";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
    }
    $nbAmis = $dbHT->sql_numrows($result) ;
    
    while($rowAmi=$dbHT->sql_fetchrow($result)) {
        
        if ($rowAmi['image'] > 0)
            echo "<img src=\"images/joueurs/".$rowAmi['joueur_id']."jpg\" alt=\"\" style=\"vertical-align:middle; max-height:150px;\" />";
        
        afficherLienProfil($rowAmi['joueur_id'], $rowAmi['pseudo']);
        echo " : ".returnLienLuiEcrire($rowAmi['joueur_id'])." - <a href=\"options.php?mode=m_amis&amp;action=enleverAmi&amp;ami_id=".$rowAmi['joueur_id']."\">".T_("enlever de la liste")."</a><br/>";
                        
    }
        
    $dbHT->sql_freeresult($result);
    
}

function getNbResults($query){

    global $dbHT;
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    $nbresults = $dbHT->sql_numrows($result);
    $dbHT->sql_freeresult($result);
    return $nbresults;
}

function insererLienIngredient($index) {
    
    global $lstIngredients, $userdata;
    if ($userdata['niveau'] == NIVEAU_POTAGE && ! getObjectifNiveau($userdata['niveau_objectif'], $index)){
        return "<a href=\"jeu.php?mode=m_accueil&amp;univers=0&amp;action=ingredientTrouve&amp;ingredient=".$lstIngredients[$index]."\" title=\"".T_("Récupère cet ingrédient pour préparer le potage !")."\"><img src=\"images/ingredients/ingredient".$index.".gif\" alt=\"".T_("ingrédient du potage")."\" class=\"ingredient\"/></a>";
    }
}

function getProprete($cage_id) {

    global $dbHT, $lst_cages;
    
    if (isset($lst_cages) && sizeof($lst_cages) > 0) {
        
        $cageIndex = cageCorrespondante($cage_id) ;
        if ($cageIndex != -1)
            return $lst_cages[$cageIndex]['proprete'] ;
        else
            return -1;
    }
    else {
        $proprete = -1;
        $query = "SELECT proprete FROM cage WHERE cage_id=".$cage_id." LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
        }
        $nbresults = $dbHT->sql_numrows($result);
        if ($nbresults == 1)
            $proprete=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        return $proprete;    
    }
}

function calculerClassement($note,$table="joueurs", $optionsSelect = "") {
    
    global $dbHT;
    
    $queryClassement =  "SELECT COUNT( note ) FROM $table WHERE note > $note ".$optionsSelect;
    $resultClassement = $dbHT->sql_query($queryClassement);
    if ( ! $resultClassement ) {
        echo "Erreur SQL : ".$queryClassement."<br>" ;
    }
    $rowClassement = $dbHT->sql_fetchrow($resultClassement);
    $classement = $rowClassement[0] + 1;
    $dbHT->sql_freeresult($resultClassement);
    
    return $classement;
}


function crediterEnergie($quantite,$hamsterIndex) {

    global $dbHT, $lst_hamsters, $dateActuelle;
    
    $energie = $lst_hamsters[$hamsterIndex]['energie'];
    $energie += $quantite;
    if ($energie > $lst_hamsters[$hamsterIndex]['maxEnergie'])
        $energie = $lst_hamsters[$hamsterIndex]['maxEnergie'];
    
    $energie = round($energie,1);
    $lst_hamsters[$hamsterIndex]['energie'] = $energie;

    $query = "UPDATE hamster
        SET energie = '".$energie."',
        majEnergie = ".$dateActuelle." 
        WHERE hamster_id=".$lst_hamsters[$hamsterIndex]['hamster_id'];    
        
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    return $energie;       
}

// retourne -1 s'il n'y a pas assez d'énergie dispo (et ne fait rien dans ce cas)
function debiterEnergie($quantite,$hamsterIndex) {

    global $dbHT, $lst_hamsters, $dateActuelle, $lst_cages;
    
    // on checke si la cage du hamster contient une éolienne et si elle est activée
    $cageHamster = cageCorrespondante($lst_hamsters[$hamsterIndex]['cage_id']);
    if ($cageHamster != -1) {
        if ($lst_cages[$cageHamster]['eolienne_activee'] > 0){
            return $lst_hamsters[$hamsterIndex]['energie'];
        }
    }
    
    if ($lst_hamsters[$hamsterIndex]['energie'] < $quantite)
        return -1;
    else {
        $lst_hamsters[$hamsterIndex]['energie'] -= $quantite;
        $query = "UPDATE hamster
            SET energie = '".$lst_hamsters[$hamsterIndex]['energie']."',
            majEnergie = ".$dateActuelle." 
            WHERE hamster_id=".$lst_hamsters[$hamsterIndex]['hamster_id'];    
            
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
        }
        return $lst_hamsters[$hamsterIndex]['energie'];       
    }
}

function joueurVientdePayer($codePaiement) {

    global $dbHT,$userdata;    
        
    $query = "UPDATE joueurs SET paiement_recent = 1 WHERE joueur_id=".$userdata['joueur_id'];
        
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

function enregistrerPremierAchat($objetConcerne) {

    global $dbHT, $userdata,$dateActuelle;
    
    if ($userdata['paiement_recent'] == 0) // si pas de paiement recent, on ignore ces stats
       return ;
     
    ajouterStats($userdata['joueur_id'],"Achat suite à paiement : ".$objetConcerne." (niveau ".$userdata['niveau'].")",2,$dateActuelle);
    
    $query = "UPDATE joueurs SET paiement_recent = 0 WHERE joueur_id=".$userdata['joueur_id'];
    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
}

// attention : le compteur du nb d'accessoires pour chaque cage n'est pas réinitialisé
function updateListeAccessoires() {

    global $dbHT, $nbAccessoires, $lst_cages, $lst_accessoires, $userdata;
 
    $lstInfosDemandees = "c.type, c.image, c.nom_fr, c.prix, c.quantite_initiale, c.danslacage, c.decal_x, c.decal_y, c.img_width, c.img_height, c.rayon, c.echelle, c.nom_en";
 
    $query = "SELECT a.*, $lstInfosDemandees FROM accessoires a, config_accessoires c WHERE a.joueur_id=".$userdata['joueur_id']." AND a.type = c.type";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
    }
    $nbAccessoires = $dbHT->sql_numrows($result) ;
    $lst_accessoires=array();
    while($row=$dbHT->sql_fetchrow($result)) {
        array_push($lst_accessoires,$row);
        // on cherche la cage qui possederait cet accessoire pour le comptage
        $cage = cageCorrespondante($row['cage_id']);
        if ($cage != -1) 
            $lst_cages[$cage]['nbAccessoires'] ++ ;
    }
    $dbHT->sql_freeresult($result);   
}

function donnerObjet($accessoire_id, $ami_id, $quantite=1) {
    
    global $dbHT, $userdata, $lst_accessoires;
    
    // on vérifie que $ami_id est bien un ami
    $query = "SELECT COUNT(joueur_id) FROM amis WHERE joueur_id = ".$userdata['joueur_id']." AND ami_de = ".$ami_id. " LIMIT 1";
    $result = $dbHT->sql_query($query);
    $row = $dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    
    if ($row[0] == 1) {
        
        
        
        $query = "SELECT type FROM accessoires WHERE accessoire_id=".$accessoire_id." LIMIT 1";
        
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
            return 0;
        }
        else {
            $row = $dbHT->sql_fetchrow($result);
            $dbHT->sql_freeresult($result);
            
            $quantiteReelle = reduireQuantiteAccessoire($accessoire_id,$quantite,false);
            if ($quantiteReelle > 0)
                ajouterAccessoireDansBDD($row['type'],$ami_id,-1,-1,-1,-1,$quantiteReelle);            
            
            return 1;
        }        
    }
    else
        return 0;
}

// vérifie que tous les hamsters du joueurs ont le seuil requis pour passer au niveau suivant
function verifierSeuilsHamsters(){
    
    global $lst_hamsters, $nbHamsters, $userdata;
    
    $seuil = 0;
    if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12)
        $seuil = 11.9;
    else if($userdata['niveau'] == NIVEAU_DEFIS5_18)
        $seuil = 17.9;
    
    for($ham=0;$ham<$nbHamsters;$ham++){
        if ($lst_hamsters[$ham]['sante'] < $seuil || $lst_hamsters[$ham]['puissance'] < $seuil || $lst_hamsters[$ham]['beaute'] < $seuil || $lst_hamsters[$ham]['moral'] < $seuil) 
            return -1;
    }    
            
    if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12)
        return updateNiveauJoueur(1,true) ;
    else if($userdata['niveau'] == NIVEAU_DEFIS5_18)
        return updateNiveauJoueur(0,true) ;
}

function verifierMissions() {

    global $userdata, $lst_hamsters, $nbHamsters, $lst_groupes, $hamsterIndex;
    
    if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12 || $userdata['niveau'] == NIVEAU_DEFIS5_18 || $userdata['niveau'] == NIVEAU_MARIAGE){
        
        if ($userdata['niveau'] == NIVEAU_ROUE_MOTORISEE_12){
            if(getObjectifNiveau($userdata['niveau_objectif'], 1) == false)
                verifierSeuilsHamsters();
        }
        else if ($userdata['niveau'] == NIVEAU_DEFIS5_18){
            if(getObjectifNiveau($userdata['niveau_objectif'], 0) == false)
                verifierSeuilsHamsters();
        }
        else if ($userdata['niveau'] == NIVEAU_MARIAGE) {
            if  (getObjectifNiveau($userdata['niveau_objectif'], 1) == false) {
                for($ham=0;$ham<$nbHamsters;$ham++) {
                    if ($lst_hamsters[$ham]['compagnon_id'] > 0) {
                        updateNiveauJoueur(2,true);
                        break;
                    }
                }
            }
        }
    }
    else if ($userdata['niveau'] == NIVEAU_CABANE_BEBE_AVATAR) {
        //if ($userdata['image'] == 1 && getObjectifNiveau($userdata['niveau_objectif'], 2) == false)
        if (getObjectifNiveau($userdata['niveau_objectif'], 2) == false)
            updateNiveauJoueur(2,true);
    }
    else if ($userdata['niveau'] == NIVEAU_EVASION) {
        if (getObjectifNiveau($userdata['niveau_objectif'], 0) == false && $nbHamsters > 1)
            updateNiveauJoueur(0,true);
    }
    else if ($userdata['niveau'] == NIVEAU_GROUPE_MUSIQUE) {
        if (getObjectifNiveau($userdata['niveau_objectif'], 0) == false && $nbHamsters > 0 && sizeof($lst_groupes) > 0)
            updateNiveauJoueur(0,true);
    }
    
    if ($userdata['niveau'] == NIVEAU_DEFIS5_18 && $userdata['nb_defis_gagnes'] >= 5 ) {
        updateNiveauJoueur(1,true);
    }
    else if($userdata['niveau'] == NIVEAU_CHINE && $userdata['nb_defis_jour'] >= 10 ) {
        updateNiveauJoueur(2,true);
    }
    
    // on vérifie que le hamster courant n'aie pas évolué en niveau
    if ($hamsterIndex != -1)
        verifierSeuilFoot($lst_hamsters[$hamsterIndex]);
}

function verifierSeuilFoot($hamRow) {
    
    global $lst_hamsters,$hamsterIndex,$msg,$dbHT, $lstNiveauxFoot;
    
    if ($hamRow['inscrit_academy'] == 2 && $hamRow['niveau'] < (sizeof($lstNiveauxFoot)-1) && $hamRow['attaque'] > ($hamRow['niveau']+1)*10 && $hamRow['defense'] > ($hamRow['niveau']+1)*10 && $hamRow['agilite'] > ($hamRow['niveau']+1)*10 && $hamRow['endurance'] > ($hamRow['niveau']+1)*10 && $hamRow['rapidite'] > ($hamRow['niveau']+1)*10) {
        
        $lst_hamsters[$hamsterIndex]['niveau']++;
        $query = "UPDATE hamster SET niveau = ".($lst_hamsters[$hamsterIndex]['niveau'])." WHERE hamster_id = ".$lst_hamsters[$hamsterIndex]['hamster_id'];
        $dbHT->sql_query($query);
        $msg .= afficherNouvelle("ballon_foot.gif",T_("Ton hamster vient de passer au niveau ").$lstNiveauxFoot[$lst_hamsters[$hamsterIndex]['niveau']] );
    }
}

function ajouterInfosAccessoire($accRow,$type){

    global $dbHT;
    
    $query = "SELECT * FROM config_accessoires WHERE type=".$type." LIMIT 1";
        
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
        return $accRow;
    }
    else {
        $row = $dbHT->sql_fetchrow($result);
        $rowResult = array_merge($accRow,$row);
        $dbHT->sql_freeresult($result);
        return $rowResult;
    }
}

function supprimerHamster($hamster_id){
    
    global $dbHT;
    
    // 1) suppression du groupe s'il est leader
    // 2) suppression des interactions
    // 3) on prévient le compagnon s'il est marié
    // 4) on supprime les bulletins de mariage    

    $query = "SELECT groupe_id, compagnon_id FROM hamster WHERE hamster_id=".$hamster_id;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    
    $rowHamster=$dbHT->sql_fetchrow($result);
 
    // 1) pour chaque hamster, est-il leader ?
    if ($rowHamster['groupe_id'] >= 0) {
        $query2 = "SELECT groupe_id, leader_id FROM groupes WHERE leader_id = ".$hamster_id. " LIMIT 1" ;
        $result2 = $dbHT->sql_query($query2);
        if ($dbHT->sql_numrows($result2) == 1) {
            $row2 = $dbHT->sql_fetchrow($result2);
            
            //if ($verbose)
            //    echo "dissout le groupe ".$row2['groupe_id']."<br/>" ;
    
            dissoudreGroupe($row2['groupe_id'], $row2['leader_id']);
        }
        $dbHT->sql_freeresult($result2);
    }
    
    // 2) pour chaque hamster, y a t-il une interaction en cours
    $query2 = "DELETE FROM interactions 
        WHERE demandeur_id=".$hamster_id. " OR receveur_id=".$hamster_id;
    if ( !($dbHT->sql_query($query2)) ){
        message_die(GENERAL_ERROR, 'Error deleting interactions ', '', __LINE__, __FILE__, $query2);
    }
    
    // 3) pour chaque hamster, est-il marié à quelqu'un d'autre ?
    if ($rowHamster['compagnon_id'] > 0) {
        $query2 = "UPDATE hamster SET compagnon_id = 0 WHERE hamster_id=".$rowHamster['compagnon_id'];
        if ( !($dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, 'Error in updating compagnon_id', '', __LINE__, __FILE__, $query2);
        }
    }
    
    // 4) pour chaque hamster, enlever les bulletins de mariage
    $query2 = "DELETE FROM mariage_bulletin 
        WHERE hamster_id=".$hamster_id;
    if ( !($dbHT->sql_query($query2)) ){
        message_die(GENERAL_ERROR, 'Error deleting bulletins de mariage ', '', __LINE__, __FILE__, $query2);
    }

    $dbHT->sql_freeresult($result);

    // suppression du hamster
    $query = "DELETE FROM hamster WHERE hamster_id=".$hamster_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting hamster ', '', __LINE__, __FILE__, $query);
    }        
}

function envoyerCimetiere($hamster_id, $pseudoHamster, $joueur_id){
 
    // message au joueur
    envoyerMessagePrive($joueur_id,"Ton hamster $pseudoHamster est tombé trop longtemps malade et le vétérinaire a décidé de l envoyer à la SPH. Tu ne peux malheureusement pas le récupérer... <br/><img src=\"images/refuge.gif\" alt=\"SPH\" /><br/>Mais tu peux vite découvrir de nouveaux hamsters en boutique, acquérir un nouveau hamster et commencer une belle et nouvelle vie avec lui !",HAMSTER_ACADEMY_ID,true);
    
    supprimerHamster($hamster_id);
}

function setCageToBeRebuilt($cage_id) {
 
    global $dbHT;
    $query = "UPDATE cage SET rebuildCache = 1 WHERE cage_id=".$cage_id;
    $dbHT->sql_query($query);
}

function getJoueurIdFromFacebookId($facebook_id) {
 
    global $dbHT;
    
    $query = "SELECT * FROM facebook WHERE facebook_id = ".$facebook_id;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbComptes = $dbHT->sql_numrows($result) ;   
    
    if ($nbComptes == 0)
        return -1; 
    else{
        $row=$dbHT->sql_fetchrow($result) ;   
        $dbHT->sql_freeresult($result);
        return $row['joueur_id'];
    }
}

function ajouterCombat($defieur_id, $defie_id) {

    global $dbHT;
    
    // creation de l'interaction
    $query = "SELECT MAX(combat_id) as combat_id FROM combats";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    $query = "INSERT INTO combats VALUES (".$nouveauId." , ".$defieur_id.",".$defie_id.",0,'',0) ";
    if ( ! $dbHT->sql_query($query) ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    return $nouveauId;
}

?>