<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

require_once "lstMetiers.php";

?>


<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">
  
  <?php

echo "<h2 align=\"center\">".T_("Liste des métiers pour l'éleveur")."</h2><br/>";

if ($msg != "" ) {
	echo "<div class=\"hamBlocColonne\">";
	echo $msg ;
	echo "</div>";
}

if ($userdata['metier'] == 0) 
	echo T_("Tu as actuellement le <strong>salaire de base</strong>, qui s'élève à <strong>").$lstMetiers[$userdata['metier']][1]."</strong> ".IMG_PIECE." ".T_("tous les jours.") ;
else 
	echo $userdata['pseudo'].", ".T_("tu es actuellement")." <strong>".$lstMetiers[$userdata['metier']][METIER_NOM]."</strong>, ".T_("qui te rapporte tous les jours")." <strong>".$lstMetiers[$userdata['metier']][METIER_SALAIRE].IMG_PIECE."</strong>." ;

?>

<br/>&nbsp;<br/>

<?php echo T_("Tu peux changer de métier pour avoir un salaire plus élévé. Pour changer de métier, il faut suivre une formation.<br/> Choisis-en un parmi ceux ci-dessous.");?>

<br/>&nbsp;<br/>

<!-- <table align="center" cellpadding="10" border="1"> -->
		<?php 
		for($metierIndex=1; $metierIndex < $nbMetiers; $metierIndex++) {
			            
            // 3 metiers par ligne 
            echo "<div class=\"blocRounded\" style=\"width:230px;\">";
                  
            echo "<form action=\"jeu.php\" method=\"get\" name=\"form_$metierIndex\">";
            if ($lstMetiers[$metierIndex][METIER_NIVEAU_ACCESSIBLE] > $userdata['niveau'])
                echo "<div class=\"elementFlou\">";
			
			echo "<input type=\"hidden\" name=\"mode\" value=\"m_metier\" />";
			echo "<input type=\"hidden\" name=\"action\" value=\"changerMetier\" />";
			echo "<input type=\"hidden\" name=\"metierIndex\" value=\"".$metierIndex."\" />";
			echo "<img src=\"images/".$lstMetiers[$metierIndex][3]."\" alt=\"\" />
			<br/>\n<strong>".$lstMetiers[$metierIndex][0]."</strong>" ;
			echo "<br/>&nbsp;<br/>\n<div align=\"justify\">".$lstMetiers[$metierIndex][4]."</div>";
			if ($metierIndex == 15)
				echo insererLienIngredient(4);
			echo "<br/>&nbsp;<br/>\n".T_("Salaire : ").$lstMetiers[$metierIndex][1]." ".IMG_PIECE.T_(" / jour");
			echo "<div>\n".T_("Coût de la formation : ").$lstMetiers[$metierIndex][2]." ".IMG_PIECE."</div>" ;
            
            if ($lstMetiers[$metierIndex][METIER_NIVEAU_ACCESSIBLE] > $userdata['niveau'])
                echo "</div>";
            
            if ($lstMetiers[$metierIndex][METIER_NIVEAU_ACCESSIBLE] <= $userdata['niveau']) {
                echo "<br/><div class=\"centre\" style=\"width:140px;\">
                <a class=\"button_gris\" href=\"#\" onclick=\"javascript:document.forms.form_$metierIndex.submit(); this.blur(); return false;\"><span>".T_("Choisir ce métier")."</span></a></div>";
            }
            else
            {
                echo "<br/><div align=\"center\" style=\"font-weight:bold; color:#aaa;\">".T_("Accessible au niveau ").$lstMetiers[$metierIndex][METIER_NIVEAU_ACCESSIBLE]."</div>";
            }

            echo "</form>";
            echo "</div>";
            if ( ($metierIndex) % 3 == 0 )
                echo "<div class=\"clear\">&nbsp;</div>";
            
		}
        echo afficherObjet("oeufs",4); 	
		?>
	
<!-- </table> -->

  <div style="clear:both;">&nbsp;</div>

  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>