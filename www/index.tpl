<!-- <div align=right><a href="index.php?lang=fr" title="Jeu en fran�ais"><img src="images/lang/francais.gif" alt="Francais" width="32" height="21" border="1"></a> <a href="index.php?lang=en" title="Play the game in English !"><img src="images/lang/english.gif" alt="English" width="32" height="20" border="1"></a></div>
-->

<div align="center"> 
  <h1>Hamster Academy</h1>
  <div class="slogan_hamster_academy">Le meilleur jeu d'�levage de hamsters !</div>  
  <br/>
  {if $erreur == 3}
    <div class="txtErreur">Le compte a �t� supprim�.</div><br/>
  {elseif $erreur == 4}
       <div class="txtErreur">Ce compte a �t� temporairement suspendu
       {if $finSuspension > 1}
       &nbsp;(jusqu'au {$finSuspension|date_format:"%d/%m/%Y %H:%M"})
       {/if}
       
       .<br/>Tu peux consulter Hamster Academy pour savoir pourquoi.</div><br/>
   {/if}
</div>

<div class="cadreArrondiFloat">
    <div class="hautdroit"></div><div class="hautgauche"></div>
    <div class="contenu">            
      <table>
      <tr valign="top">
              <td align="center">
                  <p><img src="images/hamster_russe.jpg" width="250" alt="hamster" />
                    <br/>{$slogan_txt}</p>
          </td>
      <td width="50">&nbsp;
                     
          </td>
                 <td align="left">
                      <p>&nbsp;</p>
                      <p><strong>D�j� inscrit ? Saisis ton pseudo et ton mot de passe pour jouer !</strong>
                     </p>
          <form action="login.php" method="post" name="ha_login">
                          <input type="hidden" name="page" value="jeu" />
                        {if $erreur > 5}
                            <br/><div class=txtErreur>Erreur inconnue</div>
                        {/if}
                        <table style="border:0 ;" cellspacing="0" cellpadding="5">
                          <tr>
                            <td>
                                <table border="0" cellpadding="2" align="center">
                                <tr> 
                                  <td>Pseudo :</td>
                                  <td><input type="text" name="htPseudo" />
                                  {if $erreur == 2} 
                                    <br/><div class="txtErreur">Ce pseudo n'existe pas !</div>
                                  {/if}
                                  </td>
                                </tr>
                                <tr valign="top"> 
                                  <td>Mot de passe :</td>
                                  <td><input type="password" name="htPasswd" onkeydown="
                                          {literal}
                                          javascript: 
                                          if (event.which == 13 || event.keyCode == 13) { 
                                            document.forms.ha_login.submit();
                                            return false;
                                           } 
                                          {/literal}
                                         "/>
                                        {if $erreur == 1} 
                                            <br/><div class="txtErreur">Mot de passe incorrect !<br/>&nbsp;<br/>Si tu l'as oubli�, clique sur le lien :<br/> => <u><a href=\"motdepasseperdu.php\" class=txtErreur>retrouver mon mot de passe !<a/></u><br/>&nbsp;<br/></div>
                                        {else}
                                            <br/><div style="font-size:8pt;margin-bottom:5px;"><a href="motdepasseperdu.php" title="Retrouver son mot de passe !">Mot de passe perdu?</a></div>
                                        {/if}
                                        </td>
                                </tr>
                              </table>
                              <div align="center">
                                  <script type="text/javascript">
                                      {literal}
                                      //<![CDATA[
                                                if ( ! navigator.cookieEnabled ) {
                                                    document.write( "<span class='txtErreur'>Attention ! Ton navigateur n'accepte pas les cookies.<br/>Les cookies sont n�cessaires pour jouer.<\/span><br\/>" );
                                                }
                                                //]]>
                                      {/literal}
                                            </script> 
                                <a class="button_gris" href="#" onclick="javascript:document.forms.ha_login.submit(); this.blur(); return false;"><span>Entrer dans l'Hamster Academy !</span></a>
                              </div>
                              </td>
                          </tr>
                        </table>
                  </form>
          <br/>&nbsp;<br/>
            <table><tr><td><strong>Sinon, inscris-toi, c'est gratuit ! </strong> :</td>
          <td><a class="button_gris" href="inscription.php" onclick="this.blur(); return true;"><span>S'inscrire !</span></a></td></tr></table>
              <br/>
              Et rejoins une communaut� de plus de 40 000 joueurs actifs <br/>dont {$nbConnectes}
              connect�s en ce moment m�me ! (<a href="demo.html">voir la d&eacute;mo</a>)
          </td>
          </tr>
      </table>
    </div>
    <div class="basdroit"></div><div class="basgauche"></div>
</div>
<div class="cadreArrondiFloat" style="width:220px; margin-right:20px;">
    <div class="hautdroit"></div><div class="hautgauche"></div>
    <div class="contenu" style="width:200px;"><strong>D�couvre d'autres jeux&nbsp;:</strong>        
    <ul>
            <li><a href="http://www.minouvallee.com" target="_blank" title="MinouVallee"><img src="http://www.hamsteracademy.fr/images/logo_minouvallee.gif" align="middle" alt="MinouVallee" /></a></li>
            <li><a href= "http://www.elevezundragon.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=5&amp;sp=98'" title="Elevez votre dragon !" target="_blank">Elevez un Dragon</a></li>
            <li><a href= "http://www.poneyvallee.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=1&amp;sp=98'" title="Poney Vallee - Elevez votre poney" target="_blank">Poney Vallee</a></li>
            <li><a href= "http://www.ohmydollz.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=2&amp;sp=98'" title="Oh My Dollz - jeu de mode" target="_blank">OhMyDollz</a></li>
            <li><a href="http://www.jeu-virtuel.fr" target="_blank" title="Jeux virtuels : jeux, tests...">Jeux virtuels</a></li>
            <li><a href="http://www.moutonking.com?ref=2294" target="_blank" title="Mouton-King"><img src="http://fx1.moutonking.com/g/banners/mk88x31.gif" width="88" height="31" align="middle" alt="MoutonKing" /> Moutonking</a></li>
        </ul>
        <strong>D�couvre d'autres sites&nbsp;:</strong>        
        <ul>
            <li><a href="http://www.jeu-gratuit.net" title="Jeux en ligne gratuits" target="_blank">Jeux gratuits</a></li>
            <li><a href="http://www.todoojeux.com" target="_blank">Todoo Jeux</a></li>
            <li><a href="http://www.jeuxvideo-flash.com" title="Jeux Flash et Jeux sur internet" target="_blank">JeuxVideo-Flash</a></li>
            <li><a href="http://www.top-astuce.com" target="_blank" title="Jeux gratuit">Jeu Gratuit</a></li>
            <li><a href="http://www.desjeuxgratuits.fr" target="_blank" title="Jeux gratuits">Jeux Gratuits</a></li>
            <li><a href="http://www.sitacados.com"  title="Jeux gratuits" target="_blank">Jeux</a></li>
            <li><a target="_blank" href="http://www.jeux-gratuits-enfants.com/animaux/jeux-hamster/jeu-hamster.php">Jeux gratuits pour les enfants</a></li>
        </ul>
        <ul>
            <li><a href="http://www.jeux-en-ligne-gratuits.net" title="jeux en ligne gratuits">Jeux en ligne gratuits</a></li>
            <li>Espace <a href="http://forum.bestofchat.com/informatique-ntic/jeux-video/liste_sujet-1.htm">jeux vid�o</a></li>
            <li><a href="http://www.cadomax.com/" target="_blank">Jeux et cadeaux</a></li>
            <li><a href="http://www.portaildesjeux.com" target="_blank" title="Portail des Jeux Gratuits">Portail des Jeux Gratuits</a></li>
            <li><a href="http://www.lesjeuxvirtuels.com" target="_blank">Jeux Virtuels</a></li>
            <li><a href="http://www.jeuxvirtuels.com" target="_blank"><img src="http://www.jeuxvirtuels.com/bouton.gif" border="0" width="88" height="31" alt="Annuaire de jeux virtuels !" align="middle" /></a></li>
            <li><a href="http://www.bizgratuit.com" target="_blank" title="jeux gratuits">Jeux gratuits</a></li>
            <li><a href="http://www.indiana-jeux.com" target="_blank" title="jeux">jeux</a></li>
            <li><a href="http://www.jeux-gratuits.biz" title="Jeux gratuits" target="_blank">Jeux Gratuit</a></li>
            <li><a href="http://www.meilleursjeux.net" target="_blank" title="Jeux gratuits"><img src="http://www.meilleursjeux.net/img/bans/88.gif" alt="Jeu gratuit" /></a></li>
            <li><a href="http://mitsou.oldiblog.com/" target="_blank"><img src="http://nsm01.casimages.com/img/2008/02/01//080201104028217981668091.gif" border="0"></a></li>
            <!-- 
            <li><a href="http://www.mon-cobaye.net/in.php?idp=308" target="_blank" title="Mon-Cobaye, Elevage Virtuel de Cobayes">Mon-Cobaye</a></li> 
            -->
        </ul>
    </div>
    <div class="basdroit"></div><div class="basgauche"></div>
</div>    
<div class="cadreArrondiFloat" style="width:650px; margin-right:20px;">
    <div class="hautdroit"></div><div class="hautgauche"></div>
    <div class="contenu" style="width:620px;">
        
        <h3>Les nouveaut&eacute;s !</h3>

          <table cellpadding="10">
            <tr valign="top">
              <td><img src="images/nouveautes.gif" alt="nouveautes" width="49" /></td>
              <td width="500" align="left">{$nouveautes}
               <div style=\"font-size:small; font-weight:bold;\"><a href=\"index.php?toutesnouveautes=1\" title=\"Clique-ici pour voir toutes les derni&egrave;res nouveaut&eacute;s du jeu !\">Clique-ici pour voir toutes les autres nouveaut&eacute;s...</a></div>
               </td>
            </tr>
          </table>
    </div>
    <div class="basdroit"></div><div class="basgauche"></div>
</div>        
        
<div class="cadreArrondiFloat" style="width:650px;">
    <div class="hautdroit"></div><div class="hautgauche"></div>
    <div class="contenu" style="width:620px;">
    <h3>Autour du jeu :</h3>                
    <table cellpadding="0">
          <tr>
            <td width="50"><a href="idee.html" title="Clique sur la bo&icirc;te pour y mettre une remarque ou une id&eacute;e !"><img src="images/urne.gif" alt="urne :" height="50" /></a></td>
            <td width="500" align="left"><span style="font-size:9pt;"><strong>Bo&icirc;te &agrave; id&eacute;e</strong> :</span> <span style="font-size:9pt;">Clique sur la bo&icirc;te &agrave; id&eacute;e pour faire part de tes remarques et id&eacute;es. </span></td>
          </tr>
        </table>
         <table cellpadding="0">
          <tr>
            <td align="center" width="50"><a href="aide/start" title="Les r&egrave;gles du jeu"><img src="images/regles.gif" alt="r&egrave;gles du jeu" height="40" /></a></td>
            <td width="500" align="left"><span style="font-size:9pt;"><strong>Les r&egrave;gles du jeu</strong> : </span><span style="font-size:9pt;">astuces et de bonnes id&eacute;es pour bien avancer dans le jeu.</span></td>
          </tr>
        </table>
         <table cellpadding="0">
          <tr>
            <td align="center" width="50"><a href="forum/index.php" title="Forum du jeu"><img src="images/forum.gif" alt="r&egrave;gles du jeu" height="40" /></a></td>
            <td width="500" align="left"><span style="font-size:9pt;"><strong>Le Forum du jeu</strong> :</span> <span style="font-size:9pt;">Strat&eacute;gies, amis, groupes,  solutions, discussions...</span></td>
          </tr>
        </table>
         <table cellpadding="0">
          <tr>
            <td align="center" width="50"><a href="blog/index.php" title="Le Blog du jeu"><img src="images/nouveautes.gif" alt="Blog de Hamster Academy" height="40" /></a></td>
            <td width="500" align="left"><span style="font-size:9pt;"><strong>Le Journal du jeu</strong> (blog) : derni&egrave;res nouveaut&eacute;s sur le jeu, plein de bonus sur les hamsters, des photos, des vid&eacute;os... </span></td>
          </tr>
        </table>
         <br/>
    </div>
    <div class="basdroit"></div><div class="basgauche"></div>
</div>        
<br/>        

<br/>
