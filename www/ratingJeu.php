<?php
define('IN_HT', true);

include "common.php";

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);

if (isset($_POST['id'])) {
    
    $jeu_id = intval($_POST['id']);
    $note = intval($_POST['rating']);
    
    $query = "INSERT INTO jeux_notes VALUES ($jeu_id,$note,$dateActuelle)";
    $dbForum->sql_query($query);
    
    // mise à jour du score du jeu
    $query = "SELECT note FROM jeux_notes WHERE jeu_id = $jeu_id";
    $noteTotal = 0;
    
    if ( !($result = $dbForum->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    $nbNotes = $dbForum->sql_numrows($result) ;
    while($row=$dbForum->sql_fetchrow($result)) {
        $noteTotal += $row['note'] ;
    }
    $dbForum->sql_freeresult($result);
    
    $noteTotal /= $nbNotes ;
    
    $query = "UPDATE jeux_description SET note = ".$noteTotal." WHERE jeu_id = $jeu_id";
    $dbForum->sql_query($query);
    
}


?>