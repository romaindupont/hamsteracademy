<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";
include "lstAccessoires.php";
include "lstInstruments.php";
include "lstHamsters.php";

// pour affichage public du groupe
$groupe_id = -1 ;
if (isset($_GET['groupe_id'])) {
    $groupe_id = intval($_GET['groupe_id']);
}


// on recupere l'hamster
$query = "SELECT * FROM groupes WHERE groupe_id='".$groupe_id."' LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining groupe_data', '', __LINE__, __FILE__, $query);
}
$nbGroupes = $dbHT->sql_numrows($result) ;
if ($nbGroupes != 1) {
    echo "Il n'y a aucun groupe avec cet id. Verifier l'id et recommencer." ;
    return ;
}

$groupe=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

// on recupere qq infos sur les hamsters qui y appartiennent
$query = "SELECT * FROM hamster WHERE groupe_id=".$groupe_id." LIMIT 21";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbMembres = $dbHT->sql_numrows($result) ;
if ($nbMembres == 0) {
    echo T_("Il n'y a aucun hamster associé au groupe...") ;
}

$lstMembres = array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($lstMembres,$row);
}
$dbHT->sql_freeresult($result);

$pagetitle = "Hamster Academy - ".T_("Groupe")." : ".$groupe['nom'] ;
$description = "Hamster Academy - ".T_("voir le groupe de musique")." : ".$groupe['nom'] . ". ".T_("Inscris-toi sur Hamster Academy et crée ton propre groupe de musique!");
$liste_styles = "style.css,styleAcademy.css,style_groupe.css";
$pageWidth = 800;
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', UNIVERS_ACADEMY);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('pageWidth', $pageWidth);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');
?>

<div style="text-align:center;width:70Opx; margin-left:auto; margin-right:auto; margin-top:50px;">
    
<div style="width:700px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
  
  <div class="hamBlocColonne_full" style="width:640px; text-align:center;">

    <div class="logoMain" style="left:150px;top:-100px;margin-bottom:-150px;z-index:0; position:relative; background:transparent;">
        <div class="logoImage">
            <img src="images/logoHamsterAcademy_simple.gif" alt="Hamster Academy" />
        </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
        
    <h1><?php echo $groupe['nom'] ?></h1>
    
    <?php

    // informations sur le groupe
    // -------------------------
    echo "<div class=\"groupe_description\">";
    
    // description
    echo "<div>";
    echo "<div class=\"groupe_champs_description\">".nl2br($groupe['description'])."</div><br/>";

    // logo
    if ($groupe['image'] > 0){
        echo "<div>";
        $extensionImage = "jpg";
        if ($groupe['image'] == 2) // extension gif
            $extensionImage = "gif";
        echo "<img src=\"images/groupes/".$groupe['groupe_id'].".".$extensionImage."\" alt=\"".$groupe['nom']."\"><br>";
        echo "</div>";
    }    
    
    // nb de points
    echo "<div class=groupe_description_titre>".T_("Nb de points du groupe")." : </div>";
    echo "<div class=groupe_description_bloc>".$groupe['note']." points</div>";
    
    echo "<div class=groupe_liste_membres>";
                    
    // liste des membres
    echo "<div class=groupe_description_titre>".T_("Membres du groupe")." (".$nbMembres. ") : </div>";
    echo "<div class=groupe_description_bloc>";
    for($membre=0;$membre < $nbMembres;$membre++) {
        $rowMembre = $lstMembres[$membre] ;
        echo "<span class=groupe_description_membre>";
        if ($rowMembre['hamster_id'] == $groupe['leader_id'])
            echo "<strong><a href=\"afficheHamster.php?hamster_id=".$rowMembre['hamster_id']."\">".$rowMembre['nom']."</a></strong> (leader)";
        else
            echo "<a href=\"afficheHamster.php?hamster_id=".$rowMembre['hamster_id']."\">".$rowMembre['nom']."</a>";
        echo "<div ><img src=\"images/".$infosHamsters[$rowMembre['type']][HAMSTER_IMAGE]."\" style=\"width:100px;\"></div>";
        echo "<div class=groupe_specialite_membre>".T_("Spécialité")." : <br>";
        $instrument = $rowMembre['specialite'];
        if ($instrument != -1){
            echo "<img src=\"images/".getImageAccessoire($lstInstruments[$instrument][INSTRUMENT_REF_OBJET])."\" align=absmiddle height=30> ".$lstInstruments[$instrument][INSTRUMENT_SPECIALITE];
        }
        else
            echo "aucune";
        echo "</div>";
        echo "</span>";
    }
    echo "</div></div>";
    
    echo "</div>"; // fin description du groupe
    echo "</div>"; 

?>
        
    
  <div style="clear:both;">&nbsp;</div>
  
  </div>
  
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 
  
<br/><a href="#" onclick="javascript:window.close();">Fermer la fenêtre</a><br/>&nbsp;<br/>

<?php 

require "footer.php";

?>