<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require_once ("gestionAcademy.php");
require_once ("functions_jeu_hamster.php");

$afficherGroupe = false;
$hamRow = array();

if ($hamsterIndex != -1)
    $hamRow = $lst_hamsters[$hamsterIndex];
    
$typeGroupe = $hamRow['inscrit_academy'];

if ($hamsterIndex != -1 && $hamRow['inscrit_academy'] == 0) {
    $msg .= T_("Ton hamster n'est pas inscrit à l'Academy.");
}
else if ($userdata['niveau'] < $niveauMinimalAccesAcademy) {
    $msg .= T_("L'academy n'est accessible qu'à partir du niveau ").$niveauMinimalAccesAcademy.".";
}
else if ($nbHamsters == 0) {
    $msg .= T_("Tu n'as pas de hamster pour former ou rejoindre un groupe ... ");
}
else if ($lst_hamsters[0]['pause'] > 0) { // si les hamsters sont en mode pause
    $msg .= "<div class=hamNouvellesImg><img src=\"images/refuge.gif\" alt=\"\"></div> ".T_("Les hamsters sont au Refuge de l'Academy. Pour les récupérer, va dans la page d'accueil et clique sur Récuperer les hamsters");
}
else if ($hamRow['sante'] == 0) {
    $msg .= T_("Ton hamster est chez le véto... Reviens vite dans l'Univers Eleveur pour s'occuper de lui")."...<br/>&nbsp;<br/><a href=\"jeu.php?mode=m_hamster&hamster_id=".$hamRow['hamster_id']."&chezleveto=1\">".T_("Aller chez le véto")."</a>";
}
else if ($action == "" && isset($_GET['voirtouslesgroupes'])) {
    // -------------------------------------------------------------
    // on affiche la liste des groupes
    // -------------------------------------------------------------
    $fromGrp = 0;
    if (isset($_GET['fromGrp']))
        $fromGrp = intval($_GET['fromGrp']);
        
    ob_start(); 
    afficherTousLesGroupes(false,0,-1,$fromGrp);
    echo "<div style=\"clear:both;\">&nbsp;</div>";
    $msg .= ob_get_contents();
    ob_end_clean();
}
else {
    if ($action != "") {
        if($action == "quitterGroupe"){
            if ($erreurAction == 0)
                $msg .= T_("Ton hamster a quitté le groupe.");
        }
        else if($action == "rejoindreGroupe"){
            if ($erreurAction == 0)
                $msg .= T_("Ton hamster a rejoint le groupe !");
            else if ($erreurAction == 1)
                $msg .= T_("Ton hamster a déjà un groupe. Pour changer de groupe, il faut d'abord quitter l'ancien.");
            else if ($erreurAction == 2)
                $msg .= T_("Pour appartenir à un groupe, il faut que ton hamster aie une spécialité : il faut lui acheter un instrument de musique.");
            else if ($erreurAction == 3)
                $msg .= sprintf(T_("Il n'est pas possible de rejoindre ce groupe car il possède déjà %d membres. C'est le maximum. Il faut choisir un autre groupe ou créer le sien"),$groupeNbMaxMembres);
            else if ($erreurAction == 4)
                $msg .= T_("Chaque joueur ne peut avoir que 2 hamsters maximum dans un même groupe.");
        }
        else if($action == "dissoudreGroupe"){
            if ($erreurAction == 0)
                $msg .= T_("Le groupe a été dissous");
        }
        else if($action == "virerMembre"){
            if ($erreurAction == 0)
                $msg .= T_("Le joueur a été viré.");
        }
        else if($action == "creerGroupe"){
            if ($erreurAction == 0)
                $msg .= $typeGroupes[$typeGroupe]['LE_GROUPE'].T_(" a été créé avec succès !");
            else if ($erreurAction == 2) {
                $msg .= T_("Pour créer un groupe, il faut que le hamster aie un instrument de musique.");
            }
            else    {
                $msgTmp = T_("Tu n'as pas assez d'argent pour créer un groupe car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
                $msg .= afficherPasAssezArgent($msgTmp,0);
            }
        }
        else if($action == "inscrireRepet"){
            if ($hamRow['inscrit_academy'] == 1) {
                if ($erreurAction == 0)
                    $msg .= T_("Ton hamster s'est bien inscrit à la répétition !");
                else if ($erreurAction == 2) {
                    $msgTmp = T_("Tu n'as pas assez d'argent pour t'inscrire à la répétition car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
                    $msg .= afficherPasAssezArgent($msgTmp,0);
                }
                else if ($erreurAction == 1) {
                    $msg .= T_("Ton hamster est déjà inscrit à la répétition !");
                }
            }
            else if ($hamRow['inscrit_academy'] == 2) {
               if ($erreurAction == 0)
                    $msg .= T_("Ton hamster s'est bien inscrit à l'entrainement !");
                else if ($erreurAction == 2) {
                    $msgTmp = T_("Tu n'as pas assez d'argent pour t'inscrire à l'entrainement car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
                    $msg .= afficherPasAssezArgent($msgTmp,0);
                }
                else if ($erreurAction == 1) {
                    $msg .= T_("Ton hamster est déjà inscrit à l'entrainement !");
                } 
            }
        }
        else if($action == "repeterEnGroupe"){
            
            if ($erreurAction == 0)
                $msg .= afficherNouvelle("studiomusique.gif",T_("La répétition s'est bien passée !"));
            else if ($erreurAction == 1) {
                $msg .= T_("Il faut que tous les membres soient inscrits pour pouvoir faire une répétition !");
            }
            else if ($erreurAction == 2)
                $msg .= afficherNouvelle("studiomusique.gif",T_("La répétition s'est bien passée, super bien passée grâce à ta possession de la partition de <i>RockRollAndHamsters in the Trees</i> !"));
        }
        else if($action == "inscrireConcert"){
            if ($erreurAction == 0)
                $msg .= T_("Ton hamster s'est bien inscrit au prochain concert !");
            else if ($erreurAction == 2) {
                $msgTmp = T_("Tu n'as pas assez d'argent pour t'inscrire au prochain concert car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
                $msg .= afficherPasAssezArgent($msgTmp,0);
            }
            else if ($erreurAction == 1) {
                $msg .= T_("Ton hamster est déjà inscrit au concert !");
            }
        }
        else if($action == "concertEnGroupe"){
            if ($erreurAction == 0) {
                // déjà effectué par actionAcademy.php
            }
            else if ($erreurAction == 1) {
                $msg .= T_("Il faut que tous les membres soient inscrits pour pouvoir faire un concert !");
            }
            else if ($erreurAction == 3) {
                $msg .= T_("On ne peut faire qu'un concert par jour !");
            }
        }                    
    }        
                                           
    $peutCreerGroupe = true;

    if ($hamRow['groupe_id'] == -1) {
        
        if ($typeGroupe == 1 && $hamRow['niveau'] < $typeGroupes[$typeGroupe]['NIVEAU_MIN']){
            $msg .=  T_("Pour créer ou rejoindre un groupe, ton hamster doit avoir atteint le niveau <i>Professionel</i>. Reprends-vite son entrainement et réussit les examens !");
            $peutCreerGroupe = false;
        }
    }
    else
        $afficherGroupe = true;
        
    if ($afficherGroupe && $msg != "") {
        echo "<div class=\"hamBlocColonne\">";
        echo $msg ;
        echo "</div>";
    }

    if ($hamRow['groupe_id'] == -1) {
        
        if ($peutCreerGroupe) {
            
            ob_start(); 
            

    ?>

    <div class="hamBlocColonneTransparent">
    <strong><?php echo $hamRow['nom'] ." ".T_("n'a pas ").$typeGroupes[$typeGroupe]['DE_GROUPE']." ! ".T_("Deux possibilités");?></strong> :<br/>&nbsp;<br/>
    <strong>1) <?php echo T_("Créer ").$typeGroupes[$typeGroupe]['SON_GROUPE']."</strong> : ".T_("tu deviens le leader du groupe et tu choisis ses membres (coût de la création :")." ".$coutCreationGroupe." " .IMG_PIECE ;?>) <br/>
    <form method=get action="jeu.php">
    <input type=hidden value="m_groupe" name="mode" />
    <input type=hidden value="<?php echo $hamRow['hamster_id']; ?>" name="hamster_id" />
    <input type=hidden value="creerGroupe" name="actionAcademy" />
    <input type=hidden value="" name="description" />
    <input type=text value="<?php echo T_("nom")." ".$typeGroupes[$typeGroupe]['DU_GROUPE'];?>" name="nomDuGroupe" size="25" maxlength="25" />
    <input type="submit" value="<?php 
    if ($typeGroupe == 1)
        echo T_("Créer ce nouveau groupe");
    else
        echo T_("Créer cette nouvelle équipe");
    ?>" /></form>
    <br/>&nbsp;<br/>

    <strong>2) <?php echo T_("Ou rejoindre ").$typeGroupes[$typeGroupe]['UN_GROUPE'];?></strong> <?php echo T_("(gratuit !). Voici la liste des ").$typeGroupes[$typeGroupe]['GROUPES'].T_(" disponibles");?> : <br/>&nbsp;<br/> 
        <?php 
            // -------------------------------------------------------------
            // le joueur n'a pas de groupe : on affiche la liste des groupes
            // -------------------------------------------------------------
            $fromGrp = 0;
            if (isset($_GET['fromGrp']))
                $fromGrp = intval($_GET['fromGrp']);
            afficherTousLesGroupes(true,$typeGroupe,$hamRow['hamster_id'],$fromGrp);
            echo "</div>";
            
            $msg .= ob_get_contents();
            ob_end_clean();
        }
    }
    else {

    // ------------------------------
    // on affiche le groupe du joueur
    // ------------------------------
    $query = "SELECT * FROM groupes WHERE groupe_id = ".$hamRow['groupe_id']. " LIMIT 1" ;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;

    if ($nbGroupes == 0) {
        echo T_("Problème : le groupe de")." ".$hamRow['nom']." ".T_("a été supprimé (sûrement par son leader)...") ;
        
        // pb : on remet à zéro son groupe
        $queryGroupeReset = "UPDATE hamster SET groupe_id = -1 WHERE hamster_id = ".$hamRow['hamster_id'];
        $dbHT->sql_query($queryGroupeReset);
    }
    else {
        $rowGroupe=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
                
        // mise à jour du score du groupe
        $rowGroupe['note'] = miseAJourPointsGroupe($rowGroupe['groupe_id']);
        $classementGroupe = calculerClassement($rowGroupe['note'],"groupes"," AND type = ".$rowGroupe['type'] );
        
        $leader = ($rowGroupe['leader_id'] == $hamRow['hamster_id']) ;
        $nouvelleExperience = $rowGroupe['experience'];
        $nomLeader = "";
        if ($rowGroupe['leader_id'] != -1)
            $nomLeader = getNomHamsterFromId($rowGroupe['leader_id']);
        else
            $nomLeader = T_("sans leader");
                    
        // bonus
        // -----
        $bonusGroupeTxt = "";
        if ($typeGroupe == GROUPE_MUSIQUE) {
            
            if ($rowGroupe['possede_manager']) {
                $bonusGroupeTxt .= "<div><img src=\"images/manager_musique.png\" width=40 >".T_("Le groupe possède un manager : + 20% de points")."</div>";    
            }
            else {
                
                $managerDesactive = false;
                $txtManagerDesactive = "";
                
                if (possedeAccessoire(ACC_MANAGER_MUSIC) != -1 && $rowGroupe['niveau'] < 3 ) {
                    $managerDesactive = true;
                    $txtManagerDesactive = T_("Le groupe doit être de niveau 4 pour bénéficier d'un manager.");
                }
                else if (possedeAccessoire(ACC_MANAGER_MUSIC) == -1) {
                    $managerDesactive = true;
                    $txtManagerDesactive = T_("Achète un manager en boutique pour ton groupe !");
                }
                
                ob_start(); 
                afficherActionAcademy("attribuerManager",$hamRow['hamster_id'],T_("Attribuer le manager à ce groupe"),"manager_musique.png","",40,47, $managerDesactive , $txtManagerDesactive );
                $bonusGroupeTxt .= ob_get_contents();
                ob_clean();
            }
        }
                
        // tournois
        // --------
        $tournoiTxt = "";
        if ($rowGroupe['inscrit_tournoi'] != -1) {
            $tournoiTxt .= T_("Le groupe est inscrit au tournoi")." : <a href=\"jeu.php?mode=m_concours&amp;univers=1\">".getTournoiNom($rowGroupe['inscrit_tournoi'])."</a>" ;
        }
        else {
            $tournoiTxt .= getListeTournois();
        }
        
        $smarty->assign('tournoiTxt', $tournoiTxt);
        $smarty->assign('leader', $leader);
        $smarty->assign('pseudoHamster', $hamRow['nom']);
        $smarty->assign('hamsterId', $hamRow['hamster_id']);
        $smarty->assign('groupeId', $rowGroupe['groupe_id']);
        $smarty->assign('nomGroupe', $rowGroupe['nom']);
        $smarty->assign('typeGroupe', $rowGroupe['type']);
        $smarty->assign('edition_membre', $rowGroupe['edition_membre']);
        $smarty->assign('classement', $classementGroupe);
        $smarty->assign('nomLeader', $nomLeader); 
        $smarty->assign('niveau', $rowGroupe['niveau']+1);
                    
        if ($typeGroupe == GROUPE_MUSIQUE) {
            $smarty->assign('niveauDescription', $lstNiveauxGroupe[$rowGroupe['niveau']][0]);
            $smarty->assign('niveauDescriptionPlus', $lstNiveauxGroupe[$rowGroupe['niveau']][2]);
        }
        else{
            $smarty->assign('niveauDescription', $lstNiveauxEquipe[$rowGroupe['niveau']][0]);
            $smarty->assign('niveauDescriptionPlus', $lstNiveauxEquipe[$rowGroupe['niveau']][2]);
        }
        $smarty->assign('classement', $classementGroupe);
            
        // on cherche la liste des membres du groupe
        $query = "SELECT hamster_id, joueur_id, nom, note, specialite, type, inscrit_repet, inscrit_concert, experience, derniere_activite, round(attaque,1) as attaque, round(defense,1) as defense, round(agilite,1) as agilite, round(rapidite,1) as rapidite, round(endurance,1) as endurance, round(gardien,1) as gardien FROM hamster WHERE groupe_id = ".$hamRow['groupe_id']." LIMIT ".$typeGroupes[$typeGroupe]['NB_MAX_JOUEURS'];
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbMembres = $dbHT->sql_numrows($result) ;
        $lstMembres = array();
               
        // pour le calcul du combo
        $nbBatterie=$nbGuitares=$nbBasses=$nbPercu=$nbChants=$nbSaxo=$nbTrombone=0;
        
        // pour les répèts et concerts
        $nbInscritsRepet = 0;
        $nbInscritsConcert = 0;
        $listeInscritsRepet = "";
        $listeInscritsConcert = "";
        
        $leaderValide = false;
        $changementLeaderNonDemande = false;
        $meilleurMembreActif = array();
        $meilleurMembreNote = 0;
                        
        // passage en revue de tous les membres : on récupère les instruments, les points etc.
        while($rowMembre=$dbHT->sql_fetchrow($result)) {
            
            $rowMembre['hamster_image'] = $infosHamsters[$rowMembre['type']][HAMSTER_IMAGE];
            
            if ($typeGroupe == GROUPE_MUSIQUE){
                if ($rowMembre['specialite'] != -1){
                    $rowMembre['instrument_image'] = getImageAccessoire($lstInstruments[$rowMembre['specialite']][INSTRUMENT_REF_OBJET]);
                    $rowMembre['specialite_nom'] = $lstInstruments[$rowMembre['specialite']][INSTRUMENT_SPECIALITE];
                }
            }
            else if ($typeGroupe == EQUIPE_FOOT){
               if ($rowMembre['specialite'] != -1)
                   $rowMembre['specialite_nom'] = $lstSpecialitesFoot[$rowMembre['specialite']];
               else
                $rowMembre['specialite_nom'] = $lstSpecialitesFoot[0];
            }
            
            $nbJours = round(($dateActuelle - $rowMembre['derniere_activite'])/86400);
            if ($nbJours > 0)
                $rowMembre['derniere_activite_jour'] = $nbJours.T_(" j");
            else {
                $nbHeures = round(($dateActuelle - $rowMembre['derniere_activite'])/3600);
                if ($nbHeures > 0)
                    $rowMembre['derniere_activite_jour'] = $nbHeures." h";
                else {
                    $nbMinutes = round(($dateActuelle - $rowMembre['derniere_activite'])/60);
                    $rowMembre['derniere_activite_jour'] = $nbMinutes." m";
                }
            }
            
            array_push($lstMembres, $rowMembre) ;
            
            if (sizeof($meilleurMembreActif) == 0) {
                $meilleurMembreActif = $rowMembre;
            }
            
            // instruments, combos
            if ($typeGroupe == GROUPE_MUSIQUE) {
                if ($rowMembre['specialite'] == INST_BATTERIE)
                    $nbBatterie++;
                else if ($rowMembre['specialite'] == INST_GUITARE || $rowMembre['specialite'] == INST_GUITARE2 || $rowMembre['specialite'] == INST_GUITARE_FOLK)
                    $nbGuitares++;
                else if ($rowMembre['specialite'] == INST_CONTREBASSE || $rowMembre['specialite'] == INST_BASSE)
                    $nbBasses++;
                else if ($rowMembre['specialite'] == INST_DJEMBE)
                    $nbPercu++;
                else if ($rowMembre['specialite'] == INST_DJEMBE)
                    $nbPercu++;
                else if ($rowMembre['specialite'] == INST_CHANT)
                    $nbChants++;
                else if ($rowMembre['specialite'] == INST_SAXO)
                    $nbSaxo++;
                else if ($rowMembre['specialite'] == INST_TROMBONE)
                    $nbTrombone++;
            }
            
            // inscrit repets
            if ($rowMembre['inscrit_repet']) {
                $nbInscritsRepet ++;
                $listeInscritsRepet .= $rowMembre['nom']." &middot; ";
            }
            if ($rowMembre['inscrit_concert']) {
                $nbInscritsConcert ++;
                $listeInscritsConcert .= $rowMembre['nom']." &middot; " ;
            }
            
            // on vérifie qu'il y a bien un leader valide et connecté depuis 2 semaines...
            if ($rowGroupe['leader_id'] == $rowMembre['hamster_id']) {
                if ($rowMembre['derniere_activite'] > $dateActuelle - 1209600) {
                    $leaderValide = true;
                }
                if ($rowMembre['derniere_activite'] > $dateActuelle - 1814400) { // 3 semaines
                    $changementLeaderNonDemande = true;
                }
            }
            
            // meilleur membre le plus actif (utilisé si on veut changer de leader)
            if ($rowMembre['derniere_activite'] > $dateActuelle - 1209600 && $rowMembre['note'] > $meilleurMembreNote) {
                $meilleurMembreActif = $rowMembre ;
                $meilleurMembreNote = $rowMembre['note'] ;
            }
        }
        
        if ($typeGroupe == GROUPE_MUSIQUE){
            $combo = 0;
            
            // combo rock :
            if ($nbBatterie > 0 && $nbGuitares > 1 && $nbBasses > 0 && $nbChants > 0){
                // combo groupe rock
                $combo = 1;
            }
            // combo jazz :
            if ($nbBatterie > 0 && $nbGuitares > 1 && $nbBasses > 0 && $nbSaxo > 0){
                // combo groupe jazz
                $combo = 2;
            }
            // combo chorale :
            if ($nbChants > 7){
                // combo groupe chorale
                $combo = 3;
            }
        }
        
        if ($leaderValide == false) {
            // chgt de leader 
            if ( ! $changementLeaderNonDemande) {
                $rowGroupe['leader_id'] = $meilleurMembreActif['hamster_id'];
                ajouterBulletin($hamRow['groupe_id'], $meilleurMembreActif['hamster_id'], $meilleurMembreActif['nom'], T_("Message Automatique : le leader actuel n est plus actif depuis 3 semaines. Il a automatiquement été remplacé par le musicien actif qui a le plus de points : ").$meilleurMembreActif['nom']);
            }
                
            $rowGroupe['edition_membre'] = true; // tous les joueurs deviennent leader en l'absence du leader principal
            //($rowGroupe['leader_id'] == $hamRow['hamster_id']) ;
        }
        
        // mise à jour dans la base de données
        // et on en profite pour mettre à jour la date de dernière activité....
        $query = "UPDATE groupes SET experience = ".$nouvelleExperience.",
                    derniere_activite = ".$dateActuelle." ,
                    leader_id = ".$rowGroupe['leader_id']."
                    WHERE groupe_id = ".$hamRow['groupe_id'];
                    
        $dbHT->sql_query($query);

        // informations sur le groupe
        // -------------------------
        if ($rowGroupe['image'] > 0){
            $extensionImage = "jpg";
            if ($rowGroupe['image'] == 2) // extension gif
                $extensionImage = "gif";
            $lienImage = "<img src=\"images/groupes/".$rowGroupe['groupe_id'].".".$extensionImage."\" alt=\"".$rowGroupe['nom']."\" />";
            $smarty->assign('imageGroupe',$lienImage);
        }

        // description
        $smarty->assign('descriptionGroupe',$rowGroupe['description']);
        if (isset($_GET['modifierDescription'])){
            $smarty->assign('editerDescription',"1");
        }
        else{
            $smarty->assign('descriptionEditable',$leader || $rowGroupe['edition_membre']);
        }
        
        // rôles des joueurs
        if ($rowGroupe['type'] == EQUIPE_FOOT && isset($_GET['modifierRolesJoueurs'])) {
            if ($leader || $rowGroupe['edition_membre']){
                $smarty->assign('rolesJoueursEditable',1);
            }
        }
        
        // répèt'
        $smarty->assign('IMG_PIECE',IMG_PIECE);
        $smarty->assign('nbInscritsRepet',$nbInscritsRepet);
        $smarty->assign('listeInscritsRepet',$listeInscritsRepet);
        $smarty->assign('inscrit_repet',$hamRow['inscrit_repet']);
        $smarty->assign('inscrit_concert',$hamRow['inscrit_concert']);
        $smarty->assign('nbInscritsConcert',$nbInscritsConcert);
        $smarty->assign('listeInscritsConcert',$listeInscritsConcert);
        
        // 1 concert n'est possible dès qu'il a atteint 20 nouveaux points d'expérience
        $smarty->assign('concertPossible',$rowGroupe['experience'] >= 20);
        
        // si on change la formule du cout du concert, il faut aussi la changer dans actionAcademy.php
        //$coutInscriptionConcert = 5 * (1+ ($rowGroupe['experience']/5) ) ;
        $coutInscriptionConcert = $lstNiveauxGroupe[$rowGroupe['niveau']][3];
        $smarty->assign('coutInscriptionConcert',$coutInscriptionConcert);
        
        // bulletin    
        // on récupère les 10 derniers bulletins
        $query = "SELECT * FROM groupe_bulletin WHERE groupe_id=".$hamRow['groupe_id']. " AND date > ".($dateActuelle-86400*15)." ORDER BY date DESC "; // message depuis 2 semaines
        if (isset($_GET['plusdebulletins']))
            $query .= "LIMIT 100";
        else
            $query .= "LIMIT 15";
        
        if ( !($resultBulletins = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $lstMsgs = array();
        while($row=$dbHT->sql_fetchrow($resultBulletins)) {
            array_push($lstMsgs,$row);
        }
        $lstMsgs = array_reverse($lstMsgs,true);    
        $dbHT->sql_freeresult($resultBulletins);
        $smarty->assign('lstBulletins',$lstMsgs);

        // nb de points
        if ($typeGroupe == GROUPE_MUSIQUE && $combo > 0) {
            $bonusGroupeTxt .= "<br/><strong>Combo !</strong> ";
            if ($combo == 1)
                $bonusGroupeTxt .= T_("Groupe de rock");
            else if ($combo == 2)
                $bonusGroupeTxt .= T_("Groupe de jazz");
            else if ($combo == 3){
                $bonusGroupeTxt .= T_("Chorale");
            }
            $bonusGroupeTxt .= " !!";
        }
        $smarty->assign('bonusGroupeTxt', $bonusGroupeTxt);
        $smarty->assign('noteGroupe',$rowGroupe['note']);
        $smarty->assign('nouvelleExperience',$nouvelleExperience);        

        // nombre de joueur inscrits au concours
    //                $query20 = "SELECT inscritConcours FROM joueurs j, hamster h WHERE groupe_id = ".$hamRow['groupe_id']. " AND h.joueur_id = j.joueur_id AND j.inscritConcours > 0";
    //                if ( !($result20 = $dbHT->sql_query($query20)) ){
    //                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    //                }
    //                $nbJoueursInscritsConcours = $dbHT->sql_numrows($result20) ;
    //                $dbHT->sql_freeresult($result20);
    //
    //                // participation au concours
    //                echo "<div class=groupe_description_titre>Concours : </div>";
    //                echo "<div class=groupe_description_bloc>";
    //                if ($nbJoueursInscritsConcours < 3)
    //                    echo "Il y a ".$nbJoueursInscritsConcours." membre(s) inscrit(s) au concours... il faut encore ".(3-$nbJoueursInscritsConcours)." membres inscrit(s) pour que le groupe soit \"inscrit\" au concours.";
    //                else
    //                    echo "Il y a ".$nbJoueursInscritsConcours." membre(s) inscrit(s) au concours... Le groupe est donc bien \"inscrit\" pour le concours ! Bonne chance au groupe !";
    //                
    //                echo "</div>";
        
    $smarty->assign('nbMembres',$nbMembres);
    $smarty->assign('lstMembres',$lstMembres);
    $smarty->assign('leaderId',$rowGroupe['leader_id']);
        
        // actions générales possibles

    }
    $dbHT->sql_freeresult($result);

    if ($typeGroupe == 1)
        $smarty->display('groupe.tpl');
    else if ($typeGroupe == 2)
        $smarty->display('equipeFoot.tpl');
    }
}

if (! $afficherGroupe) {
    
?>
    <div style="width:940px;">

    <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
    </div>


    <div class="hamBlocColonne_full">

    <?php echo $msg ;?>
    
    <div style="clear:both;">&nbsp;</div>

  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 
  
</div>

<?php 
}
?>
