<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

// on débite le joueur
$query = "SELECT prix, quantite_initiale, danslacage, nom_fr FROM config_accessoires WHERE type=".$objet_a_acheter." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
}
$objet=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

// quantité sélectionnée par le joueur (si l'objet n'est pas à mettre dans la cage)
$quantiteSelectionnee = 1;
if ($objet['danslacage'] == 0) {
	$quantiteSelectionnee = intval($_GET['quantite']) ;
	if ($quantiteSelectionnee < 1)
		$quantiteSelectionnee = 1;
}
	
$prix = $objet['prix']*$quantiteSelectionnee;
if ($userdata['vip']) // réduction vip
    $prix = round($vip_reducAchat * $prix);
        
$resultatAchat = debiterPieces($prix,$objet['nom_fr']);
$plusdeplace = false;
$pourlacage_id = -1;
$nouvelAccessoireId = -1;

if ($resultatAchat == 1) { // achat ok
	
	// cas particuliers : 
	if ($objet_a_acheter == 3) { // copeaux ?
		$precedenteQuantite = $userdata['copeaux'] ;
		$nouvelleQuantite = $userdata['copeaux'] + $objet['quantite_initiale']*$quantiteSelectionnee ;
		$query = "UPDATE joueurs 
			SET copeaux = ".$nouvelleQuantite."
			WHERE joueur_id=".$userdata['joueur_id'];
			if ( !($dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
			}
			$userdata['copeaux'] = $nouvelleQuantite ;
	}
	else if ($objet_a_acheter == 5){ // aliments
		$precedenteQuantite = $userdata['aliments'] ;
		$nouvelleQuantite = $userdata['aliments'] + $objet['quantite_initiale']*$quantiteSelectionnee;
		$query = "UPDATE joueurs 
			SET aliments = ".$nouvelleQuantite."
			WHERE joueur_id=".$userdata['joueur_id'];
			if ( !($dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
			}
		$userdata['aliments'] = $nouvelleQuantite ;
	}
	else {
		
		// 2 cas : objet à mettre dans la cage ou non
		if ($objet['danslacage']) {
		
			// on ajoute l'accessoire dans la bdd
			$new_acc_id = ajouterAccessoireDansBDD($objet_a_acheter, $userdata['joueur_id'], -1, -1, -1, -1, $objet['quantite_initiale']);			
			// puis dans la liste courante
			$accessoireRow = array(
				'accessoire_id' => $new_acc_id,
				'type' => $objet_a_acheter,
				'joueur_id' => $userdata['joueur_id'],
				'cage_id' => -1,
				'etage' => -1,
				'colonne' => -1,
				'profondeur' => -1,
				'quantite' => $objet['quantite_initiale']) ;
                
            // on ajoute les infos sur l'accessoire
            $accessoireRow = ajouterInfosAccessoire($accessoireRow,$objet_a_acheter);
            
			array_push($lst_accessoires,$accessoireRow);
			$nbAccessoires++;
			
			// puis on le place dans la bonne cage
			mettreAccessoireDansLaCage($new_acc_id, -1, $pourlacage_id);
			$cage = cageCorrespondante($pourlacage_id);		
			$nouvelAccessoireId = $new_acc_id ;
		}
		else {
			// on ajoute l'accessoire dans la bdd
			$new_acc_id = ajouterAccessoireDansBDD($objet_a_acheter, $userdata['joueur_id'], -1, -1, -1, -1, $objet['quantite_initiale']*$quantiteSelectionnee);		
				
		    // puis dans la liste courante
			$accessoireRow = array(
				'accessoire_id' => $new_acc_id,
				'type' => $objet_a_acheter,
				'joueur_id' => $userdata['joueur_id'],
				'cage_id' => -1,
				'etage' => -1,
				'colonne' => -1,
				'profondeur' => -1,
				'quantite' => $objet['quantite_initiale']*$quantiteSelectionnee) ;
			array_push($lst_accessoires,$accessoireRow);
			$nbAccessoires++;
		}
	}
}
?>
