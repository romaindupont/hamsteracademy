<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

function microtime_float() {
    return array_sum(explode(' ', microtime()));
}

// pour analyse benchmark des requetes SQL
//define("HA_SHOW_QUERIES",true);

$board_config = array();
$userdata = array();
$dateActuelle = time();
$temps_debut = microtime_float(); 

// calcul de dateActuelleMinuit
$dateDuJour = getDate(time());
$heure = $dateDuJour['hours'];
$month = $dateDuJour['mon'];
$day = $dateDuJour['mday'];
$year = $dateDuJour['year'];
$dateActuelleMinuit =  mktime (0,0,10,$month,$day,$year);

$jourDansLaSemaine = date("w");

include "constants.php";
include "configServeur.php";
require_once "lang.php";
include "configJeu.php";
include "utils.php";
include "mysql.php" ;
include "sessions.php";

$board_config['allow_autologin']=true;
$board_config['session_length']=18000;
$board_config['max_autologin_time']=0;
$board_config['server_port'] = "80" ;
$board_config['script_path'] = "" ;

function encode_ip($dotquad_ip)
{
    $ip_sep = explode('.', $dotquad_ip);
    return sprintf('%02x%02x%02x%02x', $ip_sep[0], $ip_sep[1], $ip_sep[2], $ip_sep[3]);
}

function decode_ip($int_ip)
{
    $hexipbang = explode('.', chunk_split($int_ip, 2, '.'));
    return hexdec($hexipbang[0]). '.' . hexdec($hexipbang[1]) . '.' . hexdec($hexipbang[2]) . '.' . hexdec($hexipbang[3]);
}

function initDBHT() {
    
    global $serveur, $user, $password, $base, $dbHT;
    
    $dbHT = new sql_db($serveur, $user, $password, $base, false);
    if(!$dbHT->db_connect_id)
    {
       echo "Oups ! Petit probleme avec le jeu... Appuie sur la touche F5 ou le bouton <i>Actualiser</i> pour le relancer. Si le probleme persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
       echo "&nbsp;<br/>Server tired! Please, press F5 or Refresh.<br/>&nbsp;<br/>" ;
       echo "Pour revenir au jeu : <a href=\"#\" onClick=\"window.location.reload();return false;\">Revenir au jeu !</a><br/>&nbsp;<br/>";
       echo "Back to the game: <a href=\"#\" onClick=\"window.location.reload();return false;\">Back to the game !</a><br/>&nbsp;<br/>";
       echo "<br/>&nbsp;<br/>".mysql_error();
       return ;
    }
}

// ouverture de la base de données principale
if ( ! defined('NO_NEED_DB') ) {
    
    initDBHT();
}

// récupération de l'IP du joueur
if ( ! isset($REMOTE_ADDR) )
    $REMOTE_ADDR = "0.0.0.0";
    
$client_ip = ( !empty($HTTP_SERVER_VARS['REMOTE_ADDR']) ) ? $HTTP_SERVER_VARS['REMOTE_ADDR'] : ( ( !empty($HTTP_ENV_VARS['REMOTE_ADDR']) ) ? $HTTP_ENV_VARS['REMOTE_ADDR'] : $REMOTE_ADDR );
$user_ip = encode_ip($client_ip);

// PHP5 with register_long_arrays off?
if (!isset($HTTP_POST_VARS) && isset($_POST))
{
    $HTTP_POST_VARS = $_POST;
    $HTTP_GET_VARS = $_GET;
    $HTTP_SERVER_VARS = $_SERVER;
    $HTTP_COOKIE_VARS = $_COOKIE;
    $HTTP_ENV_VARS = $_ENV;
    $HTTP_POST_FILES = $_FILES;

    // _SESSION is the only superglobal which is conditionally set
    if (isset($_SESSION))
    {
        $HTTP_SESSION_VARS = $_SESSION;
    }
}

if (@phpversion() < '4.0.0')
{
    // PHP3 path; in PHP3, globals are _always_ registered
    
    // We 'flip' the array of variables to test like this so that
    // we can validate later with isset($test[$var]) (no in_array())
    $test = array('HTTP_GET_VARS' => NULL, 'HTTP_POST_VARS' => NULL, 'HTTP_COOKIE_VARS' => NULL, 'HTTP_SERVER_VARS' => NULL, 'HTTP_ENV_VARS' => NULL, 'HTTP_POST_FILES' => NULL);

    // Loop through each input array
    @reset($test);
    while (list($input,) = @each($test))
    {
        while (list($var,) = @each($$input))
        {
            // Validate the variable to be unset
            if (!isset($test[$var]) && $var != 'test' && $var != 'input')
            {
                unset($$var);
            }
        }
    }
}
else if (@ini_get('register_globals') == '1' || strtolower(@ini_get('register_globals')) == 'on')
{
    // PHP4+ path
    
    // Not only will array_merge give a warning if a parameter
    // is not an array, it will actually fail. So we check if
    // HTTP_SESSION_VARS has been initialised.
    if (!isset($HTTP_SESSION_VARS))
    {
        $HTTP_SESSION_VARS = array();
    }

    // Merge all into one extremely huge array; unset
    // this later
    $input = array_merge($HTTP_GET_VARS, $HTTP_POST_VARS, $HTTP_COOKIE_VARS, $HTTP_SERVER_VARS, $HTTP_SESSION_VARS, $HTTP_ENV_VARS, $HTTP_POST_FILES);

    unset($input['input']);
    
    while (list($var,) = @each($input))
    {
        unset($$var);
    }
   
    unset($input);
}

?>