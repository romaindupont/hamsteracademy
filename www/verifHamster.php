<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

if (isset($_POST['typeHamster'])) 
	$typeHamster=intval($_POST['typeHamster']);	
if (isset($_POST['sexeHamster'])) 
	$sexeHamster=intval($_POST['sexeHamster']);	
if (isset($_POST['prenomHamster'])) 
	$prenomHamster=mysql_real_escape_string($_POST['prenomHamster']);
	
// verif pseudo
if ($prenomHamster == "") {
	$erreur = $erreur + 1;
	$erreurPrenomHamster = 1;
	$prenomHamster = "Prenom";
}

// si on est a l'etape 2 du formulaire du choix de l'hamster (choix de caracteres)
// on verifie aussi les caracteres choisis pour l'hamster
if ($etapeVerifHamster == 2) {
	if (isset($_POST['prefHamster_1'])) 
		$prefHamster_1=mysql_real_escape_string($_POST['prefHamster_1']);
	if (isset($_POST['prefHamster_2'])) 
		$prefHamster_2=mysql_real_escape_string($_POST['prefHamster_2']);
	
	if (isset($_POST['caractHamster'])) {
		$options = $_POST['caractHamster'];
		
		// vérifie les caractères choisis
		$nbOpt = 0;
		foreach ($options as $choix) {
			$nbOpt = $nbOpt + 1;
			if ($nbOpt == 1)
				$prefHamster_1 = $choix;
			else if ($nbOpt == 2)
				$prefHamster_2 = $choix;
			else {
				$erreur = $erreur + 1;
				$erreurMsg = $erreurMsg.$erreur.") tu dois choisir deux caractéristiques maximum<br/>\n";
			}
		}
	}
	else {
		if ($prefHamster_1 != "" && $prefHamster_1 != $prefHamster_2)
			$nbOpt = 2;
	}
	
	if ($nbOpt != 2){
		$erreur = $erreur + 1;
		$erreurCaracteres = 1;
	}
}
	
?>
