<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}
// s'il revient du veto
switch($action) {
    case "recup_veto" :
        if ($resultatAchat == 1) {
            if ($hamRow['sexe']!=0)
                $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est bien rentrée de chez le veto, elle est encore toute faible. Il faut qu'elle se repose quelques jours. Donne-lui ses medicaments et vitamines. Veille sur elle attentivement.")) ;
            else
                $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est bien rentré de chez le veto, il est encore toute faible. Il faut qu'il se repose quelques jours. Donne-lui ses medicaments et vitamines. Veille sur lui attentivement.")) ;
            $msg .= afficherNouvelle("trousse_reduc.gif",$msg_modif);
        }
        break;
    case "nourrir":
        if ($hamster_nourri == 1) {
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 a bien mangé ! Burrrrps' ! Il te reste #2 portions d'aliments.")) ;
            $msg_modif = str_replace("#2",$userdata['aliments'],$msg_modif) ;
            $msg .= afficherNouvelle("ecuelle_pleine_reduc.gif",$msg_modif); // écuelle remplie
        }
        else {
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("Il n'y a pas assez d'aliments pour nourrir #1... Il faut vite en acheter !")) ;
            $msg .= afficherNouvelle("ecuelle_reduc_sb.gif",$msg_modif,"",50); // si le hamster n'a pas été nourri
        }
        break;
    case "halteres":
        if ($erreurAction == 1) 
            $msg .= afficherPlusDEnergie(T_("faire de la muscu"));
        else if ($erreurAction == 2)
            $msg .= afficherNouvelle("halteres_reduc.jpg",T_("Oh la ! Tu n'as pas d'haltères !") );
        else if ($erreurAction == 4) // pas le bon jour
            $msg .= afficherPasLeBonJour();
        else if ($erreurAction == 3) {
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est enceinte ! Le sport est déconseillé ! Donc pas de musculation pour aujourd'hui ...")) ;
            $msg .= afficherNouvelle("halteres_reduc.jpg",$msg_modif);
        }
        else
            $msg .= afficherNouvelle("halteres_reduc.jpg",T_("Ton hamster a pris du muscle ! <i>Toujours plus fort</i> semble être sa devise !"),"",60);
            break;
    case "partieCatch":
        if ($erreurAction == 2) 
            $msg .= afficherPlusDEnergie(T_("faire du catch"));
        else if ($erreurAction == 1)
            $msg .= afficherNouvelle("masque_catch.png",T_("Tu as besoin d'une tenue de catch !") );
        else if ($erreurAction == 3) // pas le bon jour
            $msg .= afficherPasLeBonJour();
        else
            $msg .= afficherNouvelle("masque_catch.png",T_("Ton hamster est allé à l'entrainement de catch, il a bien progressé !"),"");
            break;    
    case "brosser":
        $msgTmp = "";
        if ($erreurAction == 1) 
            $msgTmp = T_("Attends un peu avant de rebrosser l'hamster !") ;
        else if ($erreurAction == 2) 
            $msgTmp = T_("Tu n'as pas de brosse spéciale pour ton hamster. Il faut en acheter une.");
        else
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("Ca y est, le pelage de l'hamster est enfin brossé ! #1 adore avoir un poil lisse comme du velours !")) ;
        $msg .= afficherNouvelle("brosse_a_manche.gif",$msgTmp,"",50);
        break;
    case "donnerFioleEnergie":
        $msg .= afficherNouvelle("fiole.gif",T_("Tu as donné la fiole d'énergie à ton hamster ! Le voilà plein d'énergie. A toi d'en faire bon usage !"),"",30);
        break;
    case "mettrecreme":
        $msgTmp = "";
        if ($erreurAction == 1) 
            $msgTmp = T_("Attends un peu avant de remettre de la crème sur ton hamster !");
        else if ($erreurAction == 2) 
            $msgTmp = T_("Tu n'as pas de crème pour l'hamster. Il faut en acheter.");
        else
            $msgTmp = T_("Rien ne vaut la crème pour un beau pelage. Ton hamster a retrouvé un poil luisant et impeccable !");
        $msg .= afficherNouvelle("creme_reduc.gif",$msgTmp);
        break;
    case "mettrehamsteropoil":
        $msg_modif = "";
        if ($erreurAction == 1) 
            $msg_modif = T_("Attends un peu avant de remettre de la crème, il faut qu\'elle soit absorbée par le poil pendant quelques heures...") ;
        else if ($erreurAction == 2) 
            $msg_modif = T_("Tu n'as pas de crème HamsterOPoil pour le hamster... Il faut en acheter !");
        else
            $msg_modif = T_("Superbe ! Ton hamster a repris du <i>poil de la bête</i> comme on dit !");
        $msg .= afficherNouvelle("flacon_hamsteropoil.gif",$msg_modif);
        break;
    case "vitamines" :
        if ($erreurAction == 1) 
            $msgTmp = T_("Ton hamster vient de prendre ses vitamines... Attends quelques heures avant de lui en re-donner. Il risque l'overdose sinon...");
        else if ($erreurAction == 2) 
            $msgTmp = T_("Tu n'as pas de vitamines pour ton hamster. Il faut en acheter.");
        else if ($hamRow['sante'] > 0) 
            $msgTmp = str_replace("#1",$infoAction,T_("Ton hamster a pris ses vitamines. Il va mieux&nbsp;!<br/>Il te reste #1 pilule(s) de vitamines.")) ;
        $msg .= afficherNouvelle("vitamines_reduc2.gif",$msgTmp);
        break;
    case "stagepleinair":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("turimo_pleinair.gif",T_("Ton hamster est parti en stage de plein-air à la mer ! Son moral est reparti à la hausse !"));
        else
            $msg .= afficherNouvelle("turimo_pleinair.gif",T_("Il faut que tu achètes un stage de plein-air avant de pouvoir l'utiliser."));
            break;
    case "donnerBonbon":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("bonbons.jpg",T_("Mmmmmmh ... ton hamster adoooore les bonbons !! Il t'en remercie, son moral est à la hausse ! Surveille bien son poids par contre..."),"",70);
        else
            $msg .= afficherNouvelle("bonbons.jpg",T_("Il faut que tu achètes d'abord des bonbons dans la boutique pour pouvoir lui en donner."),"",70);
            break;
    case "donnerLivreBeaute":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("livre_beaute.gif",T_("Ton hamster a lu tout le livre et a appris tous les trucs ! (ouf) Regarde son niveau de coquetterie, il atteint 5 sur 5 !"));
        else
            $msg .= afficherNouvelle("livre_beaute.gif",T_("Tu n'as pas de Traité de beauté. Il faut en acheter un pour chaque hamster !"));
            break;
    case "donnerPartGalette":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("part_galette.gif",T_("Mmmmmmh ... ton hamster adoooore la galette de rois !! Il en redemande car c'est super bon et ... parce qu'il n'a pas eu la fève ! Mais il est quand super content, son moral est à la hausse. Surveille quand même son poids !"));
        else if ($erreurAction == 1)
            $msg .= afficherNouvelle("feve1.gif",T_("Mmmmmmh ... ton hamster adoooore la galette de rois !! Et il a la fève !!! Tu gagnes 10 ".IMG_PIECE." ! Son moral est à la hausse mais surveille quand même son poids !"));
        else
            $msg .= afficherNouvelle("part_galette.gif",T_("Il faut que tu achètes d'abord une part de galette de rois dans la boutique pour pouvoir lui en donner."));
            break;
    case "donnerCoton":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("coton.gif",T_("Ton hamster a bien reçu le coton pour la fabrication de son nid."));
        else
            $msg .= afficherNouvelle("coton.gif",T_("Il faut que tu achètes d'abord du coton dans la boutique pour pouvoir lui en donner."));
            break;
    case "donnerPaille":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("paille.gif",T_("Ton hamster a bien reçu la paille pour la fabrication de son nid."),"",40);
        else
            $msg .= afficherNouvelle("paille.gif",T_("Il faut que tu achètes d'abord de la paille dans la boutique pour pouvoir lui en donner."),"",40);
            break;
    case "donnerBrindilles":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("brindilles.gif",T_("Ton hamster a bien reçu les brindilles pour la fabrication de son nid."),"",40);
        else
            $msg .= afficherNouvelle("brindilles.gif",T_("Il faut que tu achètes d'abord des brindilles dans la boutique pour pouvoir lui en donner."),"",40);
            break;
    case "donnerBois":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("buches.gif",T_("Ton hamster a bien reçu les buches de bois, nécessaires pour la fabrication de la cabane."),"",40);
        else
            $msg .= afficherNouvelle("buches.gif",T_("Il faut que tu achètes d'abord du bois dans la boutique pour pouvoir lui en donner."),"",40);
            break;
    case "donnerPlanCabane":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("plan_cabane.png",T_("Ton hamster a bien reçu les plans de la cabane, nécessaires à sa construction."),"",40);
        else
            $msg .= afficherNouvelle("plan_cabane.png",T_("Il faut que tu achètes les plans de la cabane dans la boutique pour pouvoir lui en donner."),"",40);
            break;
    case "donnerFormationBatisseur":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("diplome.gif",T_("Ton hamster a bien suivi sa formation de bâtisseur. Le voilà apte à bâtir des nids très vite ! D'ailleurs, si tu construis un nid, regarde sa progression..."),"",40);
        else
            $msg .= afficherNouvelle("diplome.gif",T_("Il faut que tu achètes d'abord la formation dans la boutique pour pouvoir lui en donner."),"",40);
            break;
    case "caresser" :
        if ($erreurAction == 1) // caresses trop récentes
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a déjà eu plein de câlins !")) ;
        else 
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 adore tes câlins ! Ton hamster est tout joyeux !")) ;
        $msg .= afficherNouvelle("mains2.gif",$msgTmp);
        break;
    case "donnerSalade":
        if ($erreurAction == 1) // pas de salade
            $msgTmp = T_("Tu n'as plus de salade !");
        else 
            $msgTmp = str_replace("#1",$infoAction,T_("Ton hamster a beaucoup apprécié sa feuille de salade ! C'est plein de vitamines, de fibres.<br/>Il te reste #1 feuille(s) de salade.")) ;
        $msg .= afficherNouvelle("salade.gif",$msgTmp);
        break;
    case "sortieRugby":
        if ($erreurAction == 4) {
            $msg .= afficherPlusDEnergie(T_("le rugby"));
        }
        else {
            if ($erreurAction == 1) // pas de ballon
                $msgTmp = T_("Tu n'as pas de ballon de rugby pour ton hamster. Il en faut absolument un pour qu'il puisse faire sa sortie Rugby. Cours en acheter un !") ;
            else if ($erreurAction == 2) // pas de sortie rugby
                $msgTmp = T_("Tu n'as plus de sortie rugby. Il faut que tu lui en achètes une en boutique...");
            else if ($erreurAction == 3) // pas le bon jour
                $msgTmp = afficherPasLeBonJour();
            else 
                $msgTmp = T_("Génial ! Ton hamster a adoré sa sortie au rugby ! Tous les autres hamsters qui sont aussi venus ont beaucoup aimé. Le rugby, c'est vraiment sympa !");
            $msg .= afficherNouvelle("ballon_rugby.gif",$msgTmp,"",-1,-1);
        }
    break;
    case "sortieFoot":
        if ($erreurAction == 4) // pas d'énergie
            $msg .= afficherPlusDEnergie(T_("le foot"));
        else {
             if ($erreurAction == 1) // pas de ballon
                $msgTmp = T_("Tu n'as pas de ballon de foot pour ton hamster. Il en faut absolument un pour qu'il puisse faire sa sortie au club de foot. Cours en acheter un&nbsp;!");
            else if ($erreurAction == 2) // pas de sortie foot
                $msgTmp = T_("Tu n'as plus de sortie au club de foot. Il faut que tu lui en achètes une en boutique...") ;
            else if ($erreurAction == 3) // pas le bon jour
                $msgTmp = afficherPasLeBonJour(false);
            else 
                $msgTmp = T_("Génial ! Ton hamster a adoré sa sortie au club de foot ! Un bon petit match de foot entre amis, c'est cool !");
            $msg .= afficherNouvelle("ballon_foot.gif",$msgTmp,"Foot",-1,-1);
        }
    break;
    case "sortieTennis" :
        if ($erreurAction == 4) // pas d'énergie
            $msg .= afficherPlusDEnergie(T_("le tennis"));
        else {
            if ($erreurAction == 1) // pas de raquette ou de balles
               $msgTmp = T_("Il faut une raquette et des balles de tennis pour jouer !");
            else if ($erreurAction == 2) // pas de sortie tennis
                $msgTmp = T_("Il faut que tu achètes une sortie au tennis pour jouer !");
            else 
                $msgTmp = T_("Le match de tennis s'est super bien passé ! Même Yannick Noah est venu voir ton hamster ! Il faut continuer à progresser, être toujours plus fort, et surtout, s'amuser !");
            $msg .= afficherNouvelle("tennis.jpg",$msgTmp,"",-1,-1);
        }
    break;
    case "TennisContreHammer":
        if ($erreurAction == 4) // pas d'énergie
            $msg .= afficherPlusDEnergie(T_("le match tennis (chaque partie de tennis contre Hammer est trèèès fatiguante et consomme 15 points d'énergie)<br/>"));
        else {
            if ($erreurAction == 1) // pas de raquette ou de balles
               $msgTmp = T_("Il faut une raquette et des balles de tennis pour jouer !");
            else if ($erreurAction == 2) // pas assez fort
                $msgTmp = T_("Aïe aïe, ton hamster n'a pas réussi à battre Hammer...<br/>Il est très très fort, il faut que ton hamster aie :<ul><li>une santé de fer (supérieure à 23),</li><li>un moral d'acier (supérieur à 23)</li><li>et une force maximale (supérieure à 25).</li></ul>Ton hamster doit reprendre des forces, se muscler et être en pleine forme ! Et reviens vite battre Hammer !!");
            else
                $msgTmp = T_("Bravo ! Tu as battu Hammer Le Pirate !!");
            $msg .= afficherNouvelle("tennis.jpg",$msgTmp,"",-1,-1);
        }
    break;;
    case "bouleRose" :
    case "bouleNoire" :
        if ($erreurAction == 1) {
            $msg .= afficherPlusDEnergie(T_("se promener dans sa boule"));
        }
        else {
            $imageBoule = "boule_rose";
            if ($action == "bouleNoire")
                $imageBoule = "boule_noire";
                
            if ($erreurAction == 2) // pas de roue
                $msgTmp = T_("Tu n'as pas de boule.") ;
            else if ($erreurAction == 3) // pas le bon jour
                $msgTmp = afficherPasLeBonJour(false) ;
            else 
                $msgTmp = T_("Ca y est, sa sortie en boule est terminée. Ton hamster a adoré ! Il attend toujours avec impatience sa sortie en boule ! Parcourir ainsi la maison hors de la cage ! Nouvelles odeurs, nouveaux paysages ... En plus, il fait travailler ses muscles !");
            $msg .= afficherNouvelle($imageBoule.".gif",$msgTmp);
        }
    break;
    case "consolejeu":
    
        if ($erreurAction == 1) {
            $msg .= afficherPlusDEnergie(T_("jouer à la console"));
        }
        else {
            if ($erreurAction == 2) // pas de console
                $msgTmp = T_("Tu n'as pas de console de jeu.");
            else if ($erreurAction == 3) // pas le bon jour
                $msgTmp = afficherPasLeBonJour(false);
            else 
                $msgTmp = T_("C'est toujours cool de jouer à la console avec ses amis le soir ! Ton hamster attend toujours ce moment avec impatience ! Son moral est reparti à la hausse !");
            $msg .= afficherNouvelle("console.gif",$msgTmp,"");
        }
    break;
    case "promenerHamster":
        
        if ($erreurAction == 1) {
            $msg .= afficherPlusDEnergie(T_("se promener en forêt"));
        }
        else {
            $imageSrc = "foret.gif";
            if ($erreurAction == 2) // pas de laisse
                $msgTmp = T_("Tu n'as pas de laisse pour promener ton hamster !");
            else  {
                if ($infoAction == 1){
                    $msgTmp = T_("Sa promenade en forêt s'est bien passé, et mieux ! Il a trouvé des glands ! Il n'a plus faim maintenant !"); // trouvé un gland
                    $imageSrc = "gland.gif";
                }
                else if ($infoAction == 2){
                    $msgTmp = T_("Aïe aïe, ça arrive parfois (heureusement, rarement) ton hamster a malheureusement ramené une maladie, il a dû manger un champignon vénémeux ou un truc du genre... il est tombé malade. Soigne-le bien."); // chopé une maladie
                    $imageSrc = "virus.gif";
                }
                else if ($infoAction == 4){ // a récupéré du bois
                    $msgTmp = T_("Ton hamster a trouvé du bois en forêt !"); 
                    $imageSrc = "buches.gif";
                }
                else if ($infoAction == 5){ // a récupéré du bois mais trop lourd pour lui (niveau insuffisant)
                    $msgTmp = T_("Ton hamster a trouvé du bon bois en forêt mais il est encore trop peu musclé pour en ramener... peut-être un peu plus tard dans le jeu..."); 
                    $imageSrc = "buches.gif";
                }
                else if ($infoAction == 6){ // fin de la mission de l'évasion => on ne fait rien
                    $msgTmp = T_("Tu as retrouvé ton hamster, il était en forêt !");
                }
                else if ($infoAction == 7){ // mission sur Hammer le Pirate
                    $msgTmp = T_("Bravo ! En faisant la promenade, tu as trouvé Hammer Le Pirate ! Il était bien caché dans les herbes !<br/>Il est très joueur : il a proposé à ton hamster le défier au tennis ! Attention, il est très fort !<br/>Pour accepter ce défi et jouer contre lui").", <a href='jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;action=TennisContreHammer'>".T_("clique-ici")." !</a>";
                    $imageSrc = "HammerLePirate.jpg";
                }
                else
                    $msgTmp = T_("Sa promenade en forêt s'est bien passé, son moral est remonté !"); // promenade simple
            }
            $msg .= afficherNouvelle($imageSrc,$msgTmp);
        }
    break;
    case "donnerChocolatAvent":
        if ($erreurAction == 1) 
            $msg .= afficherNouvelle("calendrier_chocolat.gif",T_("Ton hamster a déjà eu son chocolat. Il faut attendre demain pour récupérer un nouveau bonbon dans le calendrier de l'Avent !"));
        else
            $msg .= afficherNouvelle("calendrier_chocolat.gif",T_("Mmmmmh ! Ton hamster adore le chocolat !!"));
    break;
    case "mettreDansRoueMotorisee":
        if ($erreurAction == 1) 
            $msg .= afficherNouvelle("roue3_engrenage.gif",T_("Tu n'as plus de roue motorisée avec une pile dans la cage de ton hamster."));
        else
            $msg .= afficherNouvelle("roue3_epf_anim.png",T_("Ouaaah, ça a marché ! La pile a donné toute son énergie au moteur de l'engrenage. Ton hamster a été obligé de courir comme un malade dans sa roue qui a tourné très vite ! Tous ses kilos superflus sont partis !! Et il a pris un peu de muscle ! Par contre, la pile est bonne à être changée..."));
    break;
    case "coursChant":
        if ($erreurAction == 0){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a pris 1 heure de cours de chant ! Sa voix devient plus riche, plus suaaaâve ! <br/>Ce qui est bien aussi, c'est que ton hamster, très intelligent, en a profité pour apprendre plein de petits trucs en musique&nbsp;: solfège, rythme, gammes, etc."))." <img src=\"images/notes2.gif\" align=absmiddle alt=\"\">";
            $msg .= afficherNouvelle("micro.gif",$msgTmp);
        }
        else if ($erreurAction == $erreurActionEnergie) 
            $msg .= afficherPlusDEnergie(T_("prendre des cours de chant"));
        else{
            $msgErr = T_("Tu n'as pas assez d'argent pour payer le cours de chant car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY_ENTRAINEMENT);
        }
    break;
    case "coursInstrument":
        if ($erreurAction == 0){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a pris 1 heure de cours pour son instrument ! Tout a été passé en revue : les notes, les gammes, le rythme ! <br/>Il faut continuer à s'exercer pour assouplir les doigts (les hamsters sont réputés assez souples pour ça) et commencer à composer de nouvelles chansons."))." <img src=\"images/notes2.gif\" align=absmiddle alt=\"\">";
            $msg .= "<div class=ham1Nouvelle><div class=hamNouvellesImg><img src=\"images/".getImageAccessoire($lstInstruments[$hamRow['specialite']][INSTRUMENT_REF_OBJET])."\" alt=\"\"></div><div class=\"hamBlocTxt\">".$msgTmp."</div></div>" ; 
        }
        else if ($erreurAction == $erreurActionEnergie) 
            $msg .= afficherPlusDEnergie(T_("prendre des cours"));
        else{
            $msgErr = T_("Tu n'as pas assez d'argent pour payer le cours de guitare car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY_ENTRAINEMENT);
        }
    break;
    case "apprendreBlague":
        if ($erreurAction == 0){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a assisté à un cours de blagues ! Ton hamster a eu beaucoup de mal à rester sérieux tout au long mais il a appris beaucoup de blagues !"));
            $msg .= afficherNouvelle("rire.gif",$msgTmp); 
        }
        else if ($erreurAction == $erreurActionEnergie) 
            $msg .= afficherPlusDEnergie(T_("participer aux cours de blagues"));
        else if ($erreurAction == 2){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("Le cours de blague est toujours intéressant mais le moral de ton hamster est si bas que finalement, il n'a fait rire personne tellement il est triste...")) ;
            $msg .= afficherNouvelle("rire.gif",$msgTmp); 
        }
        else{
            $msgErr = "Tu n'as pas assez d'argent pour assister au cours de blague car tu ne possèdes que ".$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY_ENTRAINEMENT);
        }
    break;
    case "techniqueImpro":
        if ($erreurAction == 0){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a participé à un match d'impro ! Le match d'impro est un match entre 2 groupes d'hamsters qui doivent faire les meilleures blagues. Grâce à ce match, ton hamster a appris à raconter ses blagues, a amélioré ses relations avec ses copains et a appris plein de nouvelles blagues. C'est du tout bon !")) ;
            $msg .= afficherNouvelle("rire.gif",$msgTmp); 
        }
        else if ($erreurAction == $erreurActionEnergie) 
            $msg .= afficherPlusDEnergie(T_("prendre des cours d'impro"));
        else{
            $msgErr = T_("Tu n'as pas assez d'argent pour assister au match d'impro car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY_ENTRAINEMENT);
        }
    break;
    case "coursDanse" :
    case "coursDanseTektonic":
        if ($erreurAction == 0){
            $msgTmp = "";
            if ($action == "coursDanse")
                $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a pris un cours de danse ! Ce qui est bien avec le cours de danse, c'est qu'on apprends aussi plein de choses en musique et on se fait plein de copains."))." <img src=\"images/notes2.gif\" align=\"absmiddle\" alt=\"\">" ;
            else
                $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a pris un cours de danse Tecktonik ! C'est une danse à la mode par ces temps qui courent, qu'un certain public adore voir de temps en temps ! Cette danse rapporte plus de points.")) ;
            $msg .= afficherNouvelle("danse.gif",$msgTmp);
        }
        else if ($erreurAction == $erreurActionEnergie) 
                $msg .= afficherPlusDEnergie(T_("prendre des cours de danse"));
        else{
            $msgErr = T_("Tu n'as pas assez d'argent pour prendre un cours de danse car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY_ENTRAINEMENT);
        }
    break;
    case "examen":
        if ($erreurAction == 0){
            // rien ... ?
        }
        else if($erreurAction == 1){
            $msgTmp = str_replace("#1",round((259200 - ($dateActuelle - $hamRow['dernier_examen']))/86400),T_("Il faut attendre 3 jours entre deux passages d'examens. Tu pourras ré-essayer dans #1 jour(s).")) ;
            $msg .= afficherNouvelle("micro.gif",$msgTmp);
        }
    break;
    case "confesser":
        $msgTmp="";
        if ($erreurAction == 0)
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("#1 a parlé. Dans le confessional. Face aux caméras. Il a tout dit sur son humeur, sur ses copains, sur les profs de l'Academy. Le public adooore tout savoir et la popularité de #1 est montée !")) ;
        else if($erreurAction == 1)
            $msgTmp = T_("Ton hamster ne peut se confier qu'une seule fois par jour (toutes les 24 heures). Attends demain pour recommencer.");

        $msg .= afficherNouvelle("cameras.gif",$msgTmp);
    break;
    case "raconterBlagueCouloir":
        $msgTmp="";
        if ($erreurAction == 0){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("Sans se faire prendre, #1 a raconté l'une de ses meilleures blagues dans le couloir. Succès garanti !")) ;
        }
        else if($erreurAction == 1){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("Aïe aïe, pas de chance, le directeur de l'Academy t'a surpris en train de distraire les autres hamsters dans le couloir, ce qui est interdit ! Le moral et la popularité en prennent un coup...")) ;
        }
        else if($erreurAction == 2){
            $msgTmp = str_replace("#1",$hamRow['nom'],T_("En racontant sa blague dans le couloir, #1 fut la victime d'un autre hamster qui a raconté une blague plus drôle que la tienne... c'est l'humiliation... Reprends vite les cours d'humour histoire de te venger !")) ;
        }

        $msg .= afficherNouvelle("rire.gif",$msgTmp);
    break;
    case "choisirInstrument":
        $msgTmp = "";
        if ($erreurAction == 0)
            $msgTmp = T_("Ton hamster a bien reçu son instrument. Commence vite l'entraînement !");
        else if ($erreurAction == 1)
            $msgTmp = T_("Attention, tu ne possèdes pas cet instrument en stock, il faut que tu en achètes un.");
        else if ($erreurAction == 2)
            $msgTmp = T_("Ton hamster a déjà choisi sa spécialité. Ce choix est définitif et tu ne peux pas changer.");
        
        $msg .= afficherNouvelle("notes3.gif",$msgTmp);
    break;
    case "inscrireAcademy":
        if ($erreurAction == 2){
            $msg .= afficherNouvelle("etoile.gif",T_("Ton hamster est déjà inscrit !"));
        }
        else if ($erreurAction == 1){
            $msgErr = T_("Tu n'as pas assez d'argent pour payer l'inscription à l'Academy car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgErr,PAGE_JEU_ACADEMY);
        }
        else {
            $msg.= afficherNouvelle("etoile.gif",T_("Ton hamster est maintenant inscrit à l'Hamster Academy ! A toi d'en faire le meilleur ! "));
            $msg .= "<div class=ham1Nouvelle><div class=hamBlocTxt>";
            require "jeu_academy.php";
            $msg .= "</div></div>" ;
        }
    break;
    case "donnerMedicChorinthe":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("medicament_pierre_ponce.png",T_("Ton hamster a bien reçu son médicament. Il va aller beaucoup mieux. Requinque-le bien avec des vitamines, des câlins et des soins quotidiens !"));
    break;
    case "donnerMedicBraiseNoisette":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("pastille_chaleur.png",T_("Ton hamster a bien reçu son médicamenent. Il va aller beaucoup mieux. Requinque-le bien avec des vitamines, des câlins et des soins quotidiens !"));            
    break;
    case "changerAcademy":
        if ($erreurAction == 0)
            $msg .= afficherNouvelle("etoile.gif",T_("Ton hamster peut dorénavant changer d'Academy"));
    break;
    case "prendreTemperature":
        if ($erreurAction == 0){
        
            $msgTemperature = "";
            if ($temperature != 37)
                $msgTemperature = T_("Il a de la fièvre !");
            $msg .= afficherNouvelle("thermometre.png",T_("La température de ton hamster est ").$temperature."°C (".($temperature*9/5+32)."°F). ".$msgTemperature,"",25);
        }
        else if ($erreurAction == 1)
            $msg .= afficherNouvelle("thermometre.png",T_("Ton hamster déprime un peu et ne veut pas se laisser prendre sa température. Essaye de l'amadouer avec des bonbons."),"",20,25);
    break;
}
?>