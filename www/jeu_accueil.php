<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

//require_once "lstNiveaux.php";

$numeroGazette = getConfig("numero_gazette");
$forumGazette = getConfig("forum_gazette");

$niveauJoueur = $userdata['niveau'];

if ($action == "deposerRefuge"){
    if ($resultatAchat == 1) 
        $msg .= afficherNouvelle("refuge.gif",T_("Tous tes hamsters ont été déposés au Refuge de l'Academy. Tu vas pouvoir partir tranquille en vacances !"));
}
else if ($action == "recupererRefuge"){
    $msg .= afficherNouvelle("refuge.gif",T_("Tous tes hamsters ont été récupérés du Refuge de l'Academy. Occupe-t-en bien !"));
}
else if ($action == "vendreHamster"){
    if ($erreurAction == 0)
        $msg .= afficherNouvelle("refuge.gif",T_("Ton hamster a été vendu."));
}
else if ($univers == UNIVERS_ACADEMY && $userdata['niveau'] < $niveauMinimalAccesAcademy) {
    echo T_("L'academy n'est accessible qu'à partir du niveau ").$niveauMinimalAccesAcademy.".";
    return;
}
else if ($action == "abandonHamster") {
    if ($erreurAction == 1) 
        $msg .= afficherNouvelle("refuge.gif",T_("Tu n'as pas assez d'argent pour confier définitivement ton hamster au refuge."));
    else
        $msg .= afficherNouvelle("refuge.gif",T_("Ca y est ! Ton hamster est au refuge, il sera très heureux avec d'autres amis hamsters là-bas !"));
}
else if ($action == "nourrirtousleshamsters") {
    if ($erreurAction == 1) 
        $msg .= afficherNouvelle("ecuelle_vide_pleine_reduc.gif",T_("Tu n'as pas assez d'argent pour nourrir tous tes hamsters."));
    else
        $msg .= afficherNouvelle("ecuelle_vide_pleine_reduc.gif",T_("Tous tes hamsters ont été nourris !"));
}
else if ($action == "changertouslescopeaux") {
    if ($erreurAction == 1) 
        $msg .= afficherNouvelle("copeaux_sales_propres2.gif",T_("Tu n'as pas assez de copeaux pour nettoyer toutes les cages."));
    else
        $msg .= afficherNouvelle("copeaux_sales_propres2.gif",T_("Les copeaux de toutes les cages ont été changés !"));
}
// on récupère la date de dernière connexion
if ($login == 1) {
    
    // maximum 1 mois en arrière et 0 seconde en avant
    $derniereConnexion = min($dateActuelle,$derniereConnexion);
    $derniereConnexion = max($dateActuelle-2592000,$derniereConnexion); // 2592000 = 1 mois
}

// bug inscription academy lors d'un chgt de missions : on met a jour l'objectif a atteindre du niveau 
if ($userdata['niveau'] == NIVEAU_DEBUT_ACADEMY && $lst_hamsters[0]['inscrit_academy'] == 1) {
    updateNiveauJoueur(0,true) ;
}

if ($login == 1 && ! $langue_choisie && $navigateur_lang != $lang) {
    if ( $lang== "fr" ) 
        $msg .= afficherNouvelle("lang/english.gif","Play at Hamster Academy in english! For that, just go to <a href=\"http://www.hamsteracademy.com\">http://www.hamsteracademy.com</a> with the same login/password!<br/>&nbsp;<br/><span style=\"color:grey; font-size:8pt;\">(<a href=\"jeu.php?langue_choisie=fr\">hide</a>)<br/>&nbsp;<br/>");
    else
        $msg .= afficherNouvelle("lang/francais.gif","Joue à Hamster Academy en français ! Pour cela, utilise l'adresse <a href=\"http://www.hamsteracademy.fr\">http://www.hamsteracademy.fr</a> en utilisant le même pseudo et mot de passe !<br/>&nbsp;<br/><span style=\"color:grey; font-size:8pt;\">(<a href=\"jeu.php?langue_choisie=en\">ne plus demander</a>)</span><br/>&nbsp;<br/>");
}

?>

<div class="accueilColonneGauche">
        <div class="hamBlocColonneTransparent">
             <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros"><?php echo $userdata['pseudo'] ; ?></span></div>        
            </div>
            
            <div class="cadreArrondi-contenu">
            <u><?php echo T_("Tu possèdes :");?></u> <br/>
            <div><?php echo T_("Pièces :")." ".$userdata['nb_pieces']." ".IMG_PIECE ; ?> </div>
            
            <?php 
            if ($univers == UNIVERS_ELEVEUR){
                $portionsAliments = $userdata['aliments'];
                if ($portionsAliments == 1)
                    echo "<div>".sprintf(T_("Nourriture : %d portion of"),$portionsAliments);
                else
                    echo "<div>".sprintf(T_("Nourriture : %d portions of"),$portionsAliments);
                echo " <img src=\"images/ecuelle";
                if ($userdata['aliments'] > 0) echo "_pleine" ; 
                echo "_reduc.gif\" alt=\"\" style=\"vertical-align:middle; height:20px;\" /></div>";
                $copeaux = $userdata['copeaux'];
                if ($copeaux == 1)
                    echo "<div>".sprintf(T_("Copeaux : %d paquet"),$copeaux)."</div>";
                else
                    echo "<div>".sprintf(T_("Copeaux : %d paquets"),$copeaux)."</div>";
            }
            ?>
            <br/><div><?php echo sprintf(T_("Score : %d points"),$userdata['note']); ?>
            <br/><?php 
                echo T_("Classement :");
                if ($classement > 1)
                    echo " ".$classement." <span style=\"font-size:8pt;vertical-align:super;\">".T_("ème")."</span>";
                else 
                    echo T_("1er");
            if ($userdata['niveau'] >= 8) {
                echo "<br/>".T_("Défis gagnés : ").$userdata['nb_defis_gagnes'];
                echo "<br/>".T_("Défis perdus : ").$userdata['nb_defis_perdus'];
                if($lang=="fr" && ($userdata['niveau'] == NIVEAU_DEFIS5_18 || $userdata['niveau'] == NIVEAU_CHINE))
                    echo "<br/>".T_("Défis gagnés aujourd'hui :")." ".$userdata['nb_defis_jour'];
            }
            if ($userdata['vip'])
                echo "<br/>".T_("Tu es")." <a href=\"options.php?mode=m_vip\" title=\"".T_("Voir mon status VIP")."\">VIP</a> !";
            ?></div>
            
         </div>
            
            <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
            </div>
            
        </div>    

        <?php 
            if ($userdata['etape_inscription'] > 5) {
                
                echo "<div class=\"hamBlocColonneTransparent\"";
                
                if (isset($_GET['aff_niveau']) || $aideNiveau1 == 1)
                    echo " style=\"border:2px solid red\" " ;
                    
                echo ">";
                echo "<div class=\"cadreArrondi-top\">";
                echo "<div class=\"cadreArrondi-top-left\"></div>";
                echo "<div class=\"cadreArrondi-top-right\"></div>";
                if ($niveauJoueur == 2)
                    echo "<div class=\"cadreArrondi-top-center\">".T_("Niveau")." 2 - 3</div>" ;
                else
                    echo "<div class=\"cadreArrondi-top-center\">".T_("Niveau")." $niveauJoueur</div>" ;
                echo "</div>";
                echo "<div class=\"cadreArrondi-contenu\">";
                echo T_("Missions à accomplir :")."<br/>";
                $nbObjectifs = sizeof($infosNiveaux[$niveauJoueur][NIVEAU_OBJECTIFS]) ;
                for($objectif=0;$objectif<$nbObjectifs;$objectif++) {
                    $infosObjectif = $infosNiveaux[$niveauJoueur][NIVEAU_OBJECTIFS][$objectif] ;
                    if (getObjectifNiveau($userdata['niveau_objectif'],$objectif) == false)
                        echo "<img src=\"images/case_cocher4.gif\" alt=\"".T_("à faire")."\" /> " ;
                    else
                        echo "<img src=\"images/case_cocher2.gif\" alt=\"".T_("fait")." !\" /> " ;
                    echo "<span ".tooltip("&lt;div align=left>&lt;u>Mission&lt;/u> : ".$infosObjectif[OBJECTIF_DESCRIPTION]."&lt;/div>").">".$infosObjectif[OBJECTIF_NOM]."</span><br/>";
                }
                echo "<br/>".afficherObjet("oeufs",0);
                if ($userdata['niveau'] < $nbNiveaux-1)
                    echo "<br/><span style=\"font-size:9pt;\"><a href=\"jeu.php?mode=m_pass&amp;univers=$univers&amp;option=niv\">".T_("Tu veux passer au niveau suivant sans attendre ? Clique ici")."</a></span>";
                echo "</div>";
        
                echo "<div class=\"cadreArrondi-bottom\">";
                echo "<div class=\"cadreArrondi-bottom-left\"></div>";
                echo "<div class=\"cadreArrondi-bottom-right\"></div>";
                echo "<div class=\"cadreArrondi-bottom-center\"></div>";
                echo "</div>";
                echo "</div>";

            }
            if ($userdata['niveau'] >= NIVEAU_METIER_NID && $univers == UNIVERS_ELEVEUR) {
                echo "<div class=\"hamBlocColonneTransparent\">";
                echo "<div class=\"cadreArrondi-top\">";
                echo "<div class=\"cadreArrondi-top-left\"></div>";
                echo "<div class=\"cadreArrondi-top-right\"></div>";
                echo "<div class=\"cadreArrondi-top-center\">".T_("Aux dernières nouvelles")."...</div>" ;
                echo "</div>";
                echo "<div class=\"cadreArrondi-contenu\">";
                if (isset($_GET['toutesnouveautes'])){
                    define ('TOUTES_LES_NOUVEAUTES',1);
                    define ('PLUS_DE_NOUVEAUTES',1);
                }
                require "nouveautes_".$lang.".php";
                echo $dernieresNouveautes;
                if ( ! isset($_GET['toutesnouveautes']))
                echo "<br/><a href=\"jeu.php?mode=m_accueil&amp;toutesnouveautes=1 \" style=\"font-size:9pt;\">(".T_("Voir toutes les autres nouveautés").")</a><br/>";
                
                echo insererLienIngredient(0);
                echo "</div>";
        
                echo "<div class=\"cadreArrondi-bottom\">";
                echo "<div class=\"cadreArrondi-bottom-left\"></div>";
                echo "<div class=\"cadreArrondi-bottom-right\"></div>";
                echo "<div class=\"cadreArrondi-bottom-center\"></div>";
                echo "</div>";
                echo "</div>";
            }
            ?>
            
        
</div>

<div class="accueilColonneCentrale">
    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>

    <?php 
    if ($userdata['etape_inscription'] == 5)  // correction d'un bug d'affichage liée à l'image de la cage
        echo "<div class=\"hamBlocColonneTransparent\">";
    else
        echo "<div class=\"hamBlocColonneTransparent\">";
    ?>
        <?php 
            if ($userdata['etape_inscription'] == 4) {
                $msgAccueil = T_("Bienvenue dans l'Univers des Eleveurs")." ! </br>&nbsp;</br>";
                $msgAccueil .= T_("Tu viens tout juste de récupérer ton hamster ! Mets-le vite dans sa cage en cliquant sur la boite")." !<br/>&nbsp;<br/>";
                $msgAccueil .= "<div align=\"center\" ".tooltip(T_("Clique sur la boîte pour mettre l'hamster dans sa cage !"))."><a href=\"jeu.php?mode=m_accueil&inscription=5\" title=\"".T_("Mettre l'hamster dans sa cage")."\"><img src=\"images/hamster_boite.gif\" alt=\"".T_("Hamster dans sa boite")."\" /></a></div>" ;
                $msg = $msgAccueil.$msg;
            }
            else if ($userdata['etape_inscription'] == 5) {
                $msgAccueil =  "<div align=\"center\">".T_("Ca y est ! Ton hamster est dans sa cage")." !</div><br/>";
                $msgAccueil .= "<div align=\"center\"><img src=\"images/hamsterEtCage.png\" alt=\"".T_("L'hamster dans sa cage")."\" width=\"380\" height=\"235\" /></div><br/>";
                $msgAccueil .= "<div align=\"center\">".T_("Il faut s'en occuper maintenant").".</div><br/>&nbsp;<br/>";
                $msgAccueil .= "<div align=\"center\"><a href=\"jeu.php?mode=m_accueil&inscription=6\" title=\"".T_("Continuer")."\"><img src=\"images/fleche_droite_bleue.gif\" align=\"middle\" /> ".T_("Continuer")."</a></div>" ;
                $msg = $msgAccueil.$msg;
            }
            else if ($userdata['etape_inscription'] == 6) {
                majEtapeInscription(8); // fin de l'inscription
                $msgAccueil = T_("Ton hamster est bien installé dans sa cage mais il a faim")." !<br/>&nbsp;<br/>" ; 
                $msgAccueil .= T_("Clique sur ton hamster pour lui donner à manger.") ;
                $msg = $msgAccueil.$msg;
            }
            else {
                if ($login == 1) {
                    $msg = "<div align=\"center\">".T_("Bienvenue ").$userdata['pseudo']. " ! </div><br/>".$msg ;
                    
                    // y-a-t-il des nouveautés ?
                    
                    // dans les groupes :
                    if (sizeof($lst_groupes) > 0) {
                        $query = "SELECT groupe_id FROM groupe_bulletin WHERE date > ".($derniereConnexion+600)." AND groupe_id IN (".implode(",",$lst_groupes).")";
                        if ( !($result = $dbHT->sql_query($query)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                        }
                        $nbMsgs = $dbHT->sql_numrows($result) ;
                        if ($nbMsgs > 0) {
                            $msg .= "<div align=\"left\"><img src=\"images/studiomusique.gif\" align=\"absmiddle\" alt=\"Les groupes\" height:40px;/>";
                            if ($nbMsgs == 1) {
                                $msg .= T_("Il y a 1 nouveau message dans l'un des groupes de tes hamsters !");
                            }
                            else {
                                $msg .= sprintf(T_("Il y a %d nouveaux messages dans les groupes de tes hamsters !"),$nbMsgs);
                            }
                            $msg .= " <a href=\"jeu.php?mode=m_groupe\" title=\"".T_("Voir mes groupes")."\">".T_("Voir")." ".( (sizeof($lst_groupes) == 1) ? T_("mon groupe") : T_("mes groupes") )."</a></div><br/>";
                        }
                        $dbHT->sql_freeresult($result);
                    }
                    
                    // bulletin de mariage
                    if (sizeof($lst_hamsters) > 0) {
                        $query = "SELECT hamster_id FROM mariage_bulletin WHERE date > ".($derniereConnexion+600)." AND hamster_id IN (";
                        for($ha=0;$ha<$nbHamsters;$ha++)
                            $query .= $lst_hamsters[$ha]['hamster_id']."," ;
                        $query .= "0)";
                        if ( !($result = $dbHT->sql_query($query)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                        }
                        $nbMsgs = $dbHT->sql_numrows($result) ;
                        if ($nbMsgs > 0) {
                            $msg .= "<div align=\"left\">".T_("Nouveau(x) message(s) de mariage")." ! <a href=\"jeu.php?mode=m_mariages&amp;univers=0\">".T_("Clique ici pour y aller")." !</a></div>";
                        }
                        $dbHT->sql_freeresult($result);
                    }
                    
                    // anniversaire ! (en cache)
                    $cache = 'cache/anniversaire_accueil_'.$lang.'.html' ;
                    $expire = $dateActuelle - 1800; // maj toutes les demi-heures
                    if (file_exists($cache) && filemtime($cache) > $expire){
                        ob_start();
                        readfile($cache);
                        $msg .= ob_get_contents();
                        ob_end_clean();
                    }
                    else {
                        $query = "SELECT pseudo, joueur_id, naissance_annee FROM joueurs WHERE (naissance_jour != 1 OR naissance_mois != 1 OR naissance_annee != 1940) AND naissance_jour = ".(date('j'))." AND naissance_mois = ".(date('n'))." AND langue = '".$lang."' ORDER BY pseudo ASC";
                        if ( !($result = $dbHT->sql_query($query)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
                        }
                        $txtAnniversaires = "";
                        if ($dbHT->sql_numrows($result) > 0) {
                            $txtAnniversaires = "<span style=\"float:left; height:60px; margin-right:5px;\"><img src=\"images/gateau.gif\" alt=\"\" /></span> <span>".T_("Aujourd'hui, c'est l'anniversaire de")." : ";
                            while($row=$dbHT->sql_fetchrow($result)) {
                                $txt = $row['pseudo'].( ($row['naissance_annee'] == 1940)? "" : "&nbsp;(".(date('Y') - $row['naissance_annee'])."&nbsp;".T_("ans").")");
                                $txtAnniversaires .= returnLienProfil($row['joueur_id'],$txt)." &middot; ";
                            }
                            $txtAnniversaires .= "</span>";
                        }
                        $dbHT->sql_freeresult($result);
                        file_put_contents($cache, $txtAnniversaires) ;
                        $msg .= $txtAnniversaires;
                    }
                }
                
                // message pour prévenir le joueur s'il a reçu des chocolats !
                //if (possedeAccessoire(ACC_CALENDRIER_CHOCOLAT) != -1){
                //    $msg .= afficherNouvelle("calendrier_chocolat.gif",$lstMessages[70],"Chocolat");
                //}
                                
                // quelques évènements aléatoires
                $chanceEvenement = rand(1,10);
                if ($chanceEvenement == 1) {
                    if ( (possedeAccessoire(ACC_ROUE, $userdata['joueur_id']) != -1 || possedeAccessoire(ACC_ROUE2, $userdata['joueur_id']) != -1 || possedeAccessoire(ACC_ROUE3, $userdata['joueur_id']) != -1) && possedeAccessoire(ACC_HUILE, $userdata['joueur_id']) == -1)  {
                        $msg .= "<div class=\"ham1Nouvelle\"><div class=\"hamNouvellesImg\"><img src=\"images/roue3.gif\" alt=\"\" /></div><div class=\"hamBlocTxt\">";
                        $msg .= T_("Ton voisin se plaint du bruit de la roue la nuit... Une solution possible : mettre de l'huile que tu peux acheter en boutique.");
                        $msg .= "</div></div>";
                    }
                }
                else if ($chanceEvenement == 2) {
                    if ($userdata['email_active'] == 0 && $userdata['niveau'] > 2) {
                        $msg .= "<div class=\"ham1Nouvelle\"><div class=\"hamNouvellesImg\"><img src=\"images/arobase2.gif\" alt=\"\" /></div><div class=\"hamBlocTxt\">";
                        $msg .= T_("Rappel : ton email n'est pas encore activé ! Ce qui ne te permet pas de récupérer ton mot de passe ou d'aller sur le forum ou de discuter avec d'autres joueurs.");
                        $msg .= "</div></div>";
                        $msg .= "<div class=\"ham1Nouvelle\"><div class=\"hamNouvellesImg\"><img src=\"images/arobase2.gif\" alt=\"\" /></div><div class=\"hamBlocTxt\">";
                        $msg .= T_("Pour activer ton email, dois aller dans")." <a href=\"options.php\">".T_("Mon Compte")."</a> ".T_("et cliquer sur le bouton")." <strong>".T_("Activer maintenant")." !</strong>";
                        $msg .= "</div></div>";
                    }
                }
            }
          if ($msg != "") {
               echo $msg;
          }
            else{
                $messageAccueilAleatoire = rand(0,10);
                $messageAccueil = "";
                if (getConfig("concours_cage_auto_active") == 1 && $day >= 8 && $day <= 14 && $userdata['a_vote'] == 0 && $messageAccueilAleatoire == 0 && $lang == "fr")
                    $messageAccueil = "<img src=\"images/coupe.gif\" alt=\"\" style=\"vertical-align:middle; height:40px;\" /> ".T_("Rappel : tu peux voter pour le concours de la plus belle cage, rendez-vous en")." <a href=\"jeu.php?mode=m_concours\">".T_("Ville")."</a> !";
                else if ($messageAccueilAleatoire == 0) {
                //else if (1 || $messageAccueilAleatoire == 0) {
                    //$messageAccueil = "<span style=\"float:left; margin-right:10px;\"><img src=\"images/coupe.gif\" alt=\"\" style=\"vertical-align:middle; height:40px;\" /></span><span> ".T_("Nouveau concours ! Concours du joueur qui progresse le plus vite ! Rendez-vous en")." <a href=\"jeu.php?mode=m_concours\">".T_("Ville")."</a> !</span>";
                    if (0)
                        $messageAccueil .= "<br/>&nbsp;<br/><span style=\"float:left;\"><img src=\"images/amulette.png\" alt=\"\" style=\"vertical-align:middle; height:50px;\" /></span> ".T_("Nouvel objet : l'Amulette de Sport, à découvrir en Boutique, rayon Sport. Indispensable pour maximiser ses chances de gagner au concours")." !";
                    if (0)
                        $messageAccueil .= "<div align=\"center\">".T_("Les résultats du concours sont arrivés !")."</div><br/>&nbsp;<br/><div align=center><img src=\"images/concours/diplome_dec_2010.png\" alt=\"\"/></div>";
                    if (0)
                        $messageAccueil .= "<div align=\"center\"><img src=\"images/joyeuxNoel_".$lang.".jpg\" style=\"vertical-align:middle; width:80%;\"/><br/>".T_("Joyeux Noël à tous !")."</div>";
                }
                else if ($messageAccueilAleatoire == 1){
                    if ($userdata['niveau'] < 5)
                        $messageAccueil = T_("Ici, tu trouveras ton tableau de bord où tu peux avoir un oeil sur tout&nbsp;! Tes hamsters, tes cages, tes pièces, ton métier et surtout ton niveau avec les missions à accomplir.");
                    else if ($lang == "fr") {
                        $texteAccueil = getConfig("texteAccueil");
                        if ($texteAccueil != "" && $texteAccueil != "0") 
                            $messageAccueil .= "<div>".$texteAccueil."</div>";
                    }
                }
                else if ($messageAccueilAleatoire == 2 && $lang=="fr")
                    $messageAccueil = "Nouveau numéro de La Gazette (n°".$numeroGazette."), préparée par les joueurs de Hamster Academy. A découvrir dans le forum (<a href=\"forum/viewtopic.php?id=".$forumGazette."\" title=\"la Gazette des Modérateurs n°".$numeroGazette."\" >Voir la Gazette</a>)";
                else if ($messageAccueilAleatoire == 3)
                    $messageAccueil = T_("D'autres jeux hammster'ouf sur les hamsters sont aussi disponibles sur Hamster Academy : hamtaro, Harry The Hamster, Course de hamster")." ... <a href=\"autresjeux.php\">".T_("Disponibles en cliquant ici")." !</a>";
                else if ($messageAccueilAleatoire == 4 && $lang=="fr")
                    $messageAccueil = "Si tu veux participer à la <a href=\"forum/viewtopic.php?id=".$forumGazette."\" title=\"la Gazette des Modérateurs\" >Gazette de Hamster Academy</a>, préparée par les joueurs de Hamster Academy, contacte les modérateurs ou Hamster Academy !";
                else if ($messageAccueilAleatoire == 5) {
                    if (getConfig("concours_cage_auto_active") == 1 && $day >= 2 && $day <= 7 && $lang=="fr")
                        $messageAccueil = T_("Rappel : fin du concours de la plus belle cage le 7 du mois, dépêche-toi de participer !");
                    else
                        $messageAccueil = T_("Si tu as besoin d'aide, n'oublie pas le forum ! Il y a plein de joueurs qui pourront t'aider")." ! <a href=\"forum/index.php\" title=\"".T_("Forum de Hamster Academy")."\" >".T_("Aller au forum")." ! </a>";
                }
                else if ($messageAccueilAleatoire == 6){
                    if ($userdata['niveau'] == 7) {
                        $aideNiveau = rand(1,2);
                        if ($aideNiveau == 1)
                            $messageAccueil = T_("Le fameux hamster <i>Hammer le Pirate</i> a dit un jour : si tu cherches la noisette, dans l'inventaire tu la trouveras.");
                        else
                            $messageAccueil = T_("Un bruit court au marché <i>Hamster &amp; Legumes</i> : on dit que la spécialité du jour du vendeur d'hamburger est la chataigne et qu'il n'y a rien de meilleur !");
                    }
                    else
                        $messageAccueil = T_("Que penses-tu de Hamster Academy ? Tu peux laisser un message dans le")." <a href=\"forum/viewtopic.php?id=".( ($lang=="fr") ? "3017" : "6326")."\">".T_("livre d'or du forum")."</a> !";
                }
                else if ($messageAccueilAleatoire == 7) {
                    if ( ! $userdata['vip'] && $userdata['niveau'] >=  NIVEAU_POTAGE)
                        $messageAccueil = T_("Sais-tu que tu peux devenir VIP sur Hamster Academy")." ? <a href=\"options.php?mode=m_vip\">".T_("Toutes les infos sur la page VIP")."</a> !";
                    else if ($userdata['naissance_jour'] == 0 && $userdata['naissance_mois'] == 0 && $userdata['naissance_annee'] == 0){
                        $messageAccueil = "<img src=\"images/gateau.gif\" alt=\"\" />".T_("As-tu indiqué ta date de naissance ? (via Mon Compte)");
                    }
                    else {
                        // découverte d'un profil au hasard
                        $messageAccueil = T_("Découvre le profil d'un joueur choisi au hasard sur Hamster Academy");
                        $messageAccueil .= ":<br/>".afficherProfilAleatoire();
                    }
                }
                else if ($messageAccueilAleatoire == 8 || ($messageAccueilAleatoire == 9 && $eolienneEnRoute == 0) ) {
                    // découverte d'un profil au hasard
                    $messageAccueil = T_("Découvre le profil d'un joueur choisi au hasard sur Hamster Academy");
                    $messageAccueil .= ":<br/>".afficherProfilAleatoire();
                }
                else if ($messageAccueilAleatoire == 9 && $eolienneEnRoute > 0 && $lang=="fr") {
                    $messageAccueil = "<img src=\"images/eolienne.gif\" alt=\"\" height=\"60\" /> ".T_("Attention, tu as une éolienne d'énergie en route ! N'oublie pas de l'éteindre !");
                    //$messageAccueil = T_("<img src=\"images/cafe.gif\" alt=\"\" height=\"40\" /> <a href=\"forum/index.php\">Le Café des Hamsters</a> a rouvert ses portes ! Tu peux de nouveau discuter avec les autres joueurs de Hamster Academy !");
                }
                else if ($messageAccueilAleatoire == 10 && $lang=="fr") {
                    //$messageAccueil = "Concours de la <strong></strong> jusqu'au 13 novembre ! Pour tout savoir, va en <a href=\"jeu.php?mode=m_concours\">Ville->Concours</a>";
                    //$messageAccueil = T_("Grand Tournoi des Groupes de Musique ! Les 1/8ème de Finale se jouent mercredi à 12h ! Pour tout savoir, va en")." <a href=\"jeu.php?mode=m_concours\">".T_("Ville->Concours")."</a>";
                    $texteAccueil = getConfig("texteAccueil");
                    if ($texteAccueil != "" && $texteAccueil != "0") 
                        $messageAccueil .= "<div>".$texteAccueil."</div>";
                }
                echo $messageAccueil ;
                ///echo "<br/>&nbsp;<br/>Les résultats du concours du hamster le plus musclé sont arrivés ! (voir en Ville puis à la Mairie)...";
                //echo "<br/>&nbsp;<br/><img src=\"images/coupe.gif\" alt=\"Coupe\" width=\"50\" align=\"middle\" /> N'oublie pas le concours de slogans, jusqu'au 10 juillet ! (voir en Ville puis à la Mairie)...";
                //echo "<br/>&nbsp;<br/>Nouveauté : l'énergie. Elle se recharge avec le temps : il faut 20 minutes pour gagner 1 point d'énergie. A utiliser avec modération pour les activité sportives !";
            }
        ?>
    </div>

    <?php if ($userdata['etape_inscription'] > 6 && !isset($_GET['vote']) )  { // on affiche les hamsters (sauf si on vote) ?>
        <div class="hamBlocColonneTransparent">
            <?php 
                if ($nbHamsters > 0) {
                    echo "<div class=\"separateur-accueil\">";
                    echo "</div>";

                    echo "<div class=\"hamTitreBloc\" style=\"text-align:center;\">";
                    if ($nbHamsters == 1)
                        echo T_("Ton hamster :");
                    else 
                        echo T_("Tes hamsters :") ;
                    echo "</div>" ;
        
                    for($hamsterIndex=0;$hamsterIndex<$nbHamsters;$hamsterIndex++) {
                        echo "<div><table border=\"0\" align=\"center\" cellpadding=\"5\"> ";
                        echo "<tr valign=\"top\"> " ;    
                        
                        $hamRow = $lst_hamsters[$hamsterIndex] ;
                        $faim = faim($hamRow['dernier_repas']) ;
                        echo "<td align=\"center\"><a href=\"jeu.php?mode=m_hamster&amp;univers=$univers&amp;hamster_id=".$lst_hamsters[$hamsterIndex]['hamster_id']."\" title=\"".T_("S'occuper du hamster")."\">";
                        if ($hamRow['date_naissance'] > ($dateActuelle - 172800)) {
                            // c'est un tout jeune bébé
                            echo "<img src=\"images/bebe_hamster.jpg\" alt=\"bebe hamster\" style=\"align:center;width:150px;\" />";
                        }
                        else {
                            echo "<img src=\"images/".$infosHamsters[$hamRow['type']][HAMSTER_IMAGE]."\" alt=\"hamster\" style=\"width:150px;\" />";
                        }
                
                        echo "<br/>".tronquerTxt($hamRow['nom'],15)."</a><br/></td>" ;
                        echo "<td>&nbsp;</td>";
                        echo "<td>";
                        ?>
                        <table border="0">
                            <?php if ($faim < 5) { ?>
                            <tr>
                                <td><?php echo T_("Ecuelle"); ?> </td><td><?php dessinerTableauNote($faim,10) ; ?></td>
                                <td><?php echo round($faim)."/10 " ; ?></td>
                            </tr>
                            <?php }
                            if ($hamRow['maxMoral']-$hamRow['moral'] > 5) { ?>
                            <tr>
                                <td><?php echo T_("Moral"); ?></td><td><?php dessinerTableauNote($hamRow['moral'],$hamRow['maxMoral']) ; ?></td>
                                <td><?php echo round($hamRow['moral'],1)."/".$hamRow['maxMoral']; ?></td>
                            </tr>
                            <?php } ?>
                            <tr>
                                <td><?php echo T_("Santé"); ?></td><td><?php dessinerTableauNote($hamRow['sante'],$hamRow['maxSante']) ; ?></td>
                                <td><?php echo round($hamRow['sante'],1)."/".$hamRow['maxSante'] ; ?></td>
                            </tr>
                            <?php if ($hamRow['maxForce']-$hamRow['puissance'] > 5) { ?>
                            <tr>
                                <td><?php echo T_("Force"); ?> </td><td><?php dessinerTableauNote($hamRow['puissance'],$hamRow['maxForce']) ; ?></td>
                                <td><?php echo round($hamRow['puissance'],1)."/".$hamRow['maxForce']; ?></td>
                            </tr> 
                            <?php }
                            if ($hamRow['maxBeaute']-$hamRow['beaute'] > 5) {
                            ?>
                            <tr>
                                <td><?php echo T_("Beauté"); ?> </td><td><?php dessinerTableauNote($hamRow['beaute'],$hamRow['maxBeaute']) ; ?></td><td><?php echo round($hamRow['beaute'],1)."/".$hamRow['maxBeaute']; ?></td>
                            </tr>
                            <?php }
                            if ($userdata['niveau'] > 2) { ?>
                            <tr>
                                <td><?php echo T_("Energie"); ?> </td><td><?php dessinerTableauNote($hamRow['energie'],$hamRow['maxEnergie']) ; ?></td><td><?php echo $hamRow['energie']."/".$hamRow['maxEnergie'] ; ?></td>
                            </tr>
                            <?php } 
                            if (120 - $hamRow['poids'] < -10 || 120-$hamRow['poids'] > 10) {
                            ?>                       
                            <tr>
                                <td><?php echo T_("Poids"); ?></td><td><?php dessinerTableauPoids($hamRow['poids']) ; ?></td><td><?php echo $hamRow['poids']." gr." ; ?></td>
                            </tr>
                            <?php } ?>  
                        </table>
                        <?php 
                        echo "</td></tr></table></div>";
                        echo "<div class=\"centre\" style=\"width:200px;\">
                        <a class=\"button_gris\" href=\"jeu.php?mode=m_hamster&amp;univers=$univers&amp;hamster_id=".$hamRow['hamster_id']."\" onclick=\"this.blur(); return true;\"><span>".T_("S'en occuper")." !</span></a></div>";
                        echo "<div class=\"clear\">&nbsp;</div>";
                    }
                    
                    if ($nbHamsters > 1){
                        echo "<div class=\"centre\" style=\"width:300px;\">
                        <a class=\"button_gris\" href=\"jeu.php?mode=m_accueil&amp;univers=$univers&amp;action=nourrirtousleshamsters\" onclick=\"this.blur(); return true;\"><span>".T_("Nourrir tous les hamsters")." !</span></a></div>";
                        echo "<div class=\"clear\">&nbsp;</div>";
                    }
            }
        ?>
        
        </div>
        <?php if ($univers == UNIVERS_ELEVEUR) { ?>
        <div class="hamBlocColonneTransparent">
            <?php 
                if ($nbCages > 0) {
                    echo "<div class=\"separateur-accueil\">";
                    echo "</div>";

                    echo "<div class=\"hamTitreBloc\" style=\"text-align:center;\">";
                    if ($nbCages == 1)
                        echo T_("Ta cage :");
                    else
                        echo T_("Tes cages :");
                    echo "</div>";
                            
                    for($cage=0;$cage<$nbCages;$cage++) {
                        $cageRow = $lst_cages[$cage] ;
                        echo "<div><table border=\"0\" align=\"center\" cellpadding=\"10\"><tr>";                        
                        echo "<td align=\"center\"><img src=\"images/cage_reduc.gif\" alt=\"Cage\" /></td>" ;
                        echo "<td><a href=\"jeu.php?mode=m_cage&amp;cage_class=".$cageRow['cage_id']."\">".tronquerTxt($cageRow['nom'],15)."</a><br/>&nbsp;<br/>".T_("Propreté")." : ";
                        dessinerTableauNote($cageRow['proprete'],10) ; 
                        echo "</td></tr></table></div> ";
                        
                        echo "<div class=\"centre\" style=\"width:200px;\">
                        <a class=\"button_gris\" href=\"jeu.php?mode=m_cage&amp;cage_class=".$cageRow['cage_id']."\" onclick=\"this.blur(); return true;\"><span>".T_("Voir la cage")." !</span></a></div>";
                        echo "<div class=\"clear\">&nbsp;</div>";
                    }
                    if ($nbCages > 1) {
                        echo "<div class=\"centre\" style=\"width:300px;\">
                            <a class=\"button_gris\" href=\"jeu.php?mode=m_accueil&amp;action=changertouslescopeaux\" onclick=\"this.blur(); return true;\"><span>".T_("Changer les copeaux de toutes les cages")." !</span></a></div>";
                        echo "<div class=\"clear\">&nbsp;</div>";
                    }
                }
            echo afficherObjet("oeufs",6);
            ?>
        </div>
        <?php } ?>
    <?php } ?>
    <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>        
    </div>    
    
</div>

<div class="accueilColonneDroite">

<?php if ($userdata['niveau'] >= NIVEAU_METIER_NID && $univers == UNIVERS_ELEVEUR) { ?>
    <div class="hamBlocColonneTransparent">
            <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2"><?php echo T_("Mon métier"); ?></span> <a href="jeu.php?mode=m_metier" title="<?php echo T_("Voir les autres métiers"); ?>"><img src="images/icone_outils.gif" alt="" style="width:20px; height:20px;" /></a></div>
                </div>
            
            <div class="cadreArrondi-contenu">
            <?php 
                $metier = $lstMetiers[$userdata['metier']] ;
                if ($userdata['metier'] == 0)
                    echo T_("Tu n'as pas encore de métier. Tu gagnes")." ".$metier[1]." ".IMG_PIECE." ".T_("tous les jours").".<br/>" ;
                else {
                    echo "<a href=\"jeu.php?mode=m_metier\"><img src=\"images/".$metier[METIER_IMAGE_SRC]."\" alt=\"".T_("Metier")."\" /></a><br/>";
                    echo $userdata['pseudo'].", ".T_("tu es actuellement")." ".$metier[0]." ".T_("et tu gagnes")."  ".$metier[1]." ".IMG_PIECE." ".T_("tous les jours.") ;
                }
                if ($userdata['niveau'] == NIVEAU_METIER_NID) 
                        echo T_("<br/> <u>Astuce</u> : tu peux changer de métier pour gagner plus d'argent")." : <a href=\"jeu.php?mode=m_metier\" title=\"".T_("Fait une formation et change de metier : tu gagneras plus d'argent chaque jour")." !\">".T_("clique ici")."</a>";
                ?>
                </div>
                
            <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
            </div>
    </div>

<?php } ?>
   
        <div class="hamBlocColonneTransparent">
            <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2"><?php echo T_("Mes amis"); ?></span> <a href="options.php?mode=m_amis" title="<?php echo T_("Modifier la liste d'amis"); ?>"><img src="images/icone_outils.gif" alt="<?php echo T_("Modifier la liste");?>" style="width:20px; height:20px;" /></a></div>
                </div>
            
            <div class="cadreArrondi-contenu">
                <?php
                
                if ($userdata['niveau'] < NIVEAU_DEBUT_ACADEMY) {
                    echo T_("accessible au niveau ").NIVEAU_DEBUT_ACADEMY;
                }
                else {
                    $nbAmis = 0;
                    $tousmesamis = "";
                    if (! isset($_GET['tousmesamis'])){
                        $tousmesamis = " ORDER BY  j.user_lastvisit DESC LIMIT 15 ";
                    }
                    $query = "SELECT a.ami_de, j.pseudo, j.joueur_id, j.image, j.liste_rouge, j.user_lastvisit FROM amis a, joueurs j WHERE a.joueur_id=".$userdata['joueur_id']." AND j.joueur_id = a.ami_de".$tousmesamis;
                    if ( !($result = $dbHT->sql_query($query)) ){
                        message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
                    }
                    while($rowAmi=$dbHT->sql_fetchrow($result)) {
                                    
                        $image_src = "silhouette.gif" ;
                        if ($rowAmi['image'] > 0)
                            $image_src = "joueurs/".$rowAmi['joueur_id'].".jpg";
                            
                        echo "<div class=\"avatarSilhouette\"><img src=\"images/".$image_src."\" alt=\"\" /> ";
                        
                        afficherLienProfil($rowAmi['joueur_id'], $rowAmi['pseudo']);

                        if ($rowAmi['joueur_id'] != HAMSTER_ACADEMY_ID && $rowAmi['user_lastvisit'] > ($dateActuelle - 600) )
                           echo " <img src=\"images/joueurConnecte.gif\" alt=\"Joueur connecté !\" style=\"vertical-align:middle; height:12px; width:12px;\" />";

                        $nbAmis++;
                        echo "</div>";
                    }
                    
                    if (! isset($_GET['tousmesamis'])) {
                        echo "<br/><div style=\"font-size:9pt;\"><a href=\"jeu.php?mode=m_accueil&amp;univers=$univers&amp;tousmesamis=1\">".T_("Voir tous mes autres amis")."...</a></div>";
                    }
                    
                    $dbHT->sql_freeresult($result);
                }
                ?>
                </div>
                <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
            </div>
    </div>

    
    <?php 
    if ($lang == "fr") {
    ?>
    <script type="text/javascript"><!--
    google_ad_client = "pub-4942968069828673";
    /* 250x250, date de création 03/06/10 */
    google_ad_slot = "1526771153";
    google_ad_width = 220;
    google_ad_height = 250;
    //-->
    </script>
    <script type="text/javascript"
    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
    <?php
    }
    else{
        ?>
        <script type="text/javascript"><!--
    google_ad_client = "pub-4942968069828673";
    /* 250x250, date de création 03/06/10 */
    google_ad_slot = "9535777769";
    google_ad_width = 220;
    google_ad_height = 250;
    //-->
    </script>
    <script type="text/javascript"
    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
    </script>
    <?php } ?>
    <br/>&nbsp;<br/>
    
    
    <?php if ($univers == UNIVERS_ELEVEUR) { ?>
    <div class="hamBlocColonneTransparent">
        <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2"><?php echo T_("Le Refuge"); ?></span></div>
         </div>
            
            <div class="cadreArrondi-contenu">
        <?php 
            if ($userdata['etape_inscription'] > 6 && $nbHamsters > 0) {
                if ($lst_hamsters[0]['pause'] == 0) { // le premier hamster indique si tous les hamsters sont au refuge ou non
                    // refuge classique (pour les vacances)
                    echo "<div><a href=\"jeu.php?mode=m_accueil&amp;action=deposerRefuge\" title=\"".T_("Pendant les vacances, les hamsters sont nourris et logés (attention : le salaire n'est plus versé pendant cette période)")."\"><img src=\"images/refuge.gif\" alt=\"\" align=\"middle\" />".T_("Déposer tous les hamsters au refuge de l'Academy pour les vacances (coût: ").$coutRefuge." ".IMG_PIECE.")</a></div>\n";
                }
                else {
                    echo "<a href=\"jeu.php?mode=m_accueil&amp;action=recupererRefuge\"><img src=\"images/refuge.gif\" alt=\"\" align=\"middle\" />".T_("Récuperer tous les hamsters du refuge")."</a>";
                }
            }
        ?>
        </div>
          <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
            </div>
    </div>


    <?php if ($userdata['niveau'] == NIVEAU_POTAGE && $userdata['niveau_objectif'] == $maskObjectifs[7] ) { ?>
        <div class="hamBlocColonneTransparent">
            <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2"><?php echo T_("Potage du Rongeur"); ?></span></div>        
            </div>
            
            <div class="cadreArrondi-contenu">

            <?php 
            for($index=0;$index<7;$index++)
                echo "<img src=\"images/ingredients/ingredient".$index.".gif\" class=\"ingredient\" style=\"height:30px;\" /> ";
            ?><br/>
            <?php echo T_("Félicitations, tu as réuni tous les ingrédients nécessaires à la préparation du Potage du Rongeur."); ?><br/>                 <a href="jeu.php?mode=m_accueil&amp;univers=0&amp;action=faireRecettePotage" title="<?php echo T_("Faire la recette");?> !"><img src="images/ingredients/marmite.gif" style="height:70px;" /> <?php echo T_("Clique-ici pour faire le potage");?> !</a>
            </div>

            <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
            </div>
        </div>

    <?php } ?>
    
    <?php 
    // SPH : refuge définitif
    if ($userdata['niveau'] >= NIVEAU_DEBUT_ACADEMY && $nbHamsters > 1 && $lst_hamsters[0]['pause'] == 0 && $userdata['niveau'] != NIVEAU_EVASION){
        
        echo "<div class=\"hamBlocColonneTransparent\">";
        echo "<div class=\"cadreArrondi-top\">";
        echo "<div class=\"cadreArrondi-top-left\"></div>";
        echo "<div class=\"cadreArrondi-top-right\"></div>";
        echo "<div class=\"cadreArrondi-top-center\">".T_("La SPH")."</div>";
        echo "</div>";
        echo "<div class=\"cadreArrondi-contenu\">";
        
        echo T_("Confier <u>définitivement</u> ses hamsters à la SPH<br/>(coût par hamster")."&nbsp;:&nbsp;".$coutAbandonHamster."&nbsp;".IMG_PIECE.") :<br/>&nbsp;<br/>";
        for($ha=0;$ha<$nbHamsters;$ha++) {
             if($lst_hamsters[$ha]['maladie'] == 0)            
                 echo "<div><a href=\"jeu.php?mode=m_accueil\" onclick=\"javascript: if (confirm('".sprintf(T_("Tu es sûr de donner %s définitivement à la SPH ? Tu n\'auras aucun moyen de le récupérer"),$lst_hamsters[$ha]['nom'])." !')) document.location.href='jeu.php?mode=m_accueil&amp;hamster_id=".$lst_hamsters[$ha]['hamster_id']."&amp;action=abandonHamster' ; else return false ; return false;\" title=\"".T_("Pour que la SPH s'occupe définitivement de ton hamster. Attention, tu ne peux plus le récupérer")." !\"><img src=\"images/".$infosHamsters[$lst_hamsters[$ha]['type']][HAMSTER_IMAGE]."\" alt=\"\" align=\"middle\" width=\"50\" /> ".T_("Confier ").$lst_hamsters[$ha]['nom']."</a></div>\n";
        }
        echo "</div>";
        echo "<div class=\"cadreArrondi-bottom\">
            <div class=\"cadreArrondi-bottom-left\"></div>
            <div class=\"cadreArrondi-bottom-right\"></div>
            <div class=\"cadreArrondi-bottom-center\"></div>        
        </div>
        </div>";
        
    }
    
    // vente de hamsters
    if ($userdata['niveau'] >= NIVEAU_DEBUT_ACADEMY && $nbHamsters > 1 && $lst_hamsters[0]['pause'] == 0 && $userdata['niveau'] != NIVEAU_EVASION){
        
        echo "<div class=\"hamBlocColonneTransparent\">";
        echo "<div class=\"cadreArrondi-top\">";
        echo "<div class=\"cadreArrondi-top-left\"></div>";
        echo "<div class=\"cadreArrondi-top-right\"></div>";
        echo "<div class=\"cadreArrondi-top-center\">".T_("Vente de hamsters")."</div>";
        echo "</div>";
        echo "<div class=\"cadreArrondi-contenu\">";
        
        echo T_("Vendre ses hamsters<br/>(prix par hamster")."&nbsp;:&nbsp;".$prixVenteHamster."&nbsp;".IMG_PIECE.") :<br/>&nbsp;<br/>";
        for($ha=0;$ha<$nbHamsters;$ha++) {
            $scriptVente = "";
            if (($dateActuelle - $lst_hamsters[$ha]['date_naissance']) > 1814400 && $lst_hamsters[$ha]['sante'] > 0 && $lst_hamsters[$ha]['maladie'] == 0) { // 3 semaines d'age et non malade
                $scriptVente = "if (confirm('".T_("Tu es sûr de vendre ton hamster ? Tu n\'auras aucun moyen de le récupérer")." !')) document.location.href='jeu.php?mode=m_accueil&amp;hamster_id=".$lst_hamsters[$ha]['hamster_id']."&amp;action=vendreHamster' ; else return false ; return false;";
                
            }
            else {
                $scriptVente = "alert('".T_("Tu ne peux pas vendre ton hamster, il soit trop jeune, soit malade, soit au refuge")."...') ; ";
            }
            echo "<div><a href=\"jeu.php?mode=m_accueil\" onclick=\"javascript: $scriptVente\" title=\"Vendre cet hamster\"><img src=\"images/".$infosHamsters[$lst_hamsters[$ha]['type']][HAMSTER_IMAGE]."\" alt=\"\" align=\"middle\" width=\"50\" /> ".T_("Vendre ").$lst_hamsters[$ha]['nom']."</a></div>\n";
        }
        echo "</div>";
        echo "<div class=\"cadreArrondi-bottom\">
            <div class=\"cadreArrondi-bottom-left\"></div>
            <div class=\"cadreArrondi-bottom-right\"></div>
            <div class=\"cadreArrondi-bottom-center\"></div>        
        </div>
        </div>";
    }
    
    } // fin du if (univers == eleveur)
        ?>
</div>

<div style="clear:both;">&nbsp;</div>