<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

function afficherAction($labelAction,$hamster_id,$txtAction,$imageSrc,$imageAlt="",$imageWidth=-1,$imageHeight=-1,$desactivee=0, $texteDesactive="",$inAcademy = false) {
	
	if ($desactivee == 0) 
		echo "<div class=\"elementAction\">";
	else
		echo "<div class=\"elementActionDesactive\" ".tooltip($texteDesactive)." >";
	
	$mode = "m_hamster";
    if ($labelAction == "attribuerManager")
        $mode = "m_groupe";
        
    echo "<a href=\"jeu.php?mode=$mode&amp;univers=".($inAcademy? UNIVERS_ACADEMY : UNIVERS_ELEVEUR)."&amp;action".($inAcademy ? "Academy" : "")."=".$labelAction."&amp;hamster_id=".$hamster_id."\"><img src=\"images/".$imageSrc."\" alt=\"".$imageAlt."\" style=\"vertical-align:middle;";
	if ($imageWidth != -1)
		echo " width:".$imageWidth."px;";
	if ($imageHeight != -1)
		echo " height:".$imageHeight."px;";
	echo "\" /> ".$txtAction."</a></div>\n";
}

function afficherActionDesactivee($labelAction,$texteDesactive, $hamster_id,$txtAction,$imageSrc,$imageAlt="",$imageWidth=-1,$imageHeight=-1) {
	
	afficherAction($labelAction,$hamster_id,$txtAction,$imageSrc,$imageAlt,$imageWidth,$imageHeight,1,$texteDesactive) ;
}

function afficherActionAcademy($labelAction,$hamster_id,$txtAction,$imageSrc,$imageAlt="",$imageWidth=-1,$imageHeight=-1,$desactivee=0, $texteDesactive="") {
    
    afficherAction($labelAction,$hamster_id,$txtAction,$imageSrc,$imageAlt,$imageWidth,$imageHeight,$desactivee,$texteDesactive,true) ;
}

function afficherActionDesactiveeAcademy($labelAction,$texteDesactive,$hamster_id,$txtAction,$imageSrc,$imageAlt="",$imageWidth=-1,$imageHeight=-1) {
    
    afficherAction($labelAction,$hamster_id,$txtAction,$imageSrc,$imageAlt,$imageWidth,$imageHeight,1,$texteDesactive,true) ;
}
?>