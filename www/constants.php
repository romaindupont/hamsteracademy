<?php
/***************************************************************************
 *                               constants.php
 *                            -------------------
 *   begin                : Saturday', Feb 13', 2001
 *   copyright            : ('C) 2001 The phpBB Group
 *   email                : support@phpbb.com
 *
 *   $Id: constants.php,v 1.47.2.6 2005/10/30 15:17:14 acydburn Exp $
 *
 *
 ***************************************************************************/

/***************************************************************************
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License', or
 *   ('at your option) any later version.
 *
 ***************************************************************************/

if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

define ('IMG_PIECE','<img src="images/piece.gif" class="img_piece" alt="" />'); // tag html de l'image de la pièce de monnaie
define ('IMG_SUPPRIMER','<img src="images/supprimer.gif" class="img_supprimer" alt="" />'); // tag html de l'image supprimer
define ('IMG_ENERGIE','<img src="images/energie.gif" class="img_piece" alt="" />'); // tag html de l'image de la pièce de monnaie
define ('IMG_HAMSTER','<img src="images/hamster_t2_1.gif" class="img_hamster" alt="" />'); 
define ('IMG_CAGE','<img src="images/cage1.gif" class="img_hamster" alt="" />'); 

define ('IMG_COCHER_OK','<img src="images/case_cocher2.gif" alt="" />'); // tag html pour la case ok
define ('IMG_COCHER_NON','<img src="images/case_cocher4.gif" alt="" />'); // tag html pour la case ok

define('NIVEAU_DEBUT',0);
define('NIVEAU_NOURRIR_COPEAUX',1);
define('NIVEAU_GESTION_ETAGE',2);
define('NIVEAU_METIER_NID',4);
define('NIVEAU_MALADIE_FIEVRE',3);
define('NIVEAU_DEBUT_ACADEMY',5);
define('NIVEAU_POPULARITE',6);
define('NIVEAU_ROUE_MOTORISEE_12',7);
define('NIVEAU_MALADIE_CHORINTHE',8);
define('NIVEAU_POTAGE',9);
define('NIVEAU_DEFIS5_18',10);
define('NIVEAU_MARIAGE',11);
define('NIVEAU_CABANE_BEBE_AVATAR',12);
define('NIVEAU_EVASION',13);
define('NIVEAU_GROUPE_MUSIQUE',14);
define('NIVEAU_TENNIS',15);
define('NIVEAU_CHINE',16);
define('NIVEAU_MALADIE_STALACTIC',17);
define('NIVEAU_FIN',18);

$mois_txt = array("","janvier","février","mars","avril","mai","juin","juillet","août","septembre","octobre","novembre","décembre") ;
$mois_txt_en = array("","january","february","march","april","may","june","july","august","september","october","november","december") ;

// Debug Level
define('DEBUG', 1); // Debugging on

// Error codes
define('GENERAL_MESSAGE', 200);
define('GENERAL_ERROR', 202);
define('CRITICAL_MESSAGE', 203);
define('CRITICAL_ERROR', 204);

// Session parameters
define('SESSION_METHOD_COOKIE', 100);
define('END_TRANSACTION', 2);

define ('DROITS_JOUEUR',0);
define ('DROITS_MOD',1);
define ('DROITS_ADMIN',2);
define ('DROITS_SUPER_ADMIN',3);

$droits_txt = array("joueur","moderateur","admin","super admin") ;

define ('COLONNE_TOP',"<div class=\"hamBlocColonne-top\">
             <div class=\"hamBlocColonne-top-left\"></div>
             <div class=\"hamBlocColonne-top-right\"></div>
             <div class=\"hamBlocColonne-top-center\"></div>        
          </div>
          <div class=\"hamBlocColonne_full\">");
          
define ('COLONNE_BOTTOM',"<div style=\"clear:both;\">&nbsp;</div>

          </div>
            
          <div class=\"hamBlocColonne-bottom\">
             <div class=\"hamBlocColonne-bottom-left\"></div>
             <div class=\"hamBlocColonne-bottom-right\"></div>
             <div class=\"hamBlocColonne-bottom-center\"></div>        
          </div>");      

define ('ALLOPASS',0);
define ('PAYPAL',1);

define ('PAIEMENT_NUL',0);
define ('PAIEMENT_300_PIECES',1);
define ('PAIEMENT_NID',2);
define ('PAIEMENT_CABANE',3);
define ('PAIEMENT_GROSSESSE',4);
define ('PAIEMENT_VIP',5);
define ('PAIEMENT_1000_PIECES',6);
define ('PAIEMENT_3000_PIECES',7);
define ('PAIEMENT_10000_PIECES',8);
define ('PAIEMENT_NIVEAU',9);

define('MODE_AUCUN',0);
define('MODE_EN_COURS',1);
define('MODE_RESULTAT',2);
define('MODE_INSCRIPTION',3);
define('MODE_INSCRIPTION_PARTICIPATION',4);
define('MODE_VOTE',5);
?>