<?php

define ('IN_HT', true);

include "common.php";
include "gestion.php";

$userdata = session_pagestart($user_ip);
$lang = "fr";

require "lang/".$lang."/common_lang.php";

if( ! $userdata['session_logged_in'] && ($userdata['pseudo'] == "César" || $userdata['pseudo'] == "goody" || $userdata['pseudo'] == "admin" || $userdata['pseudo'] == "HamsterAcademy") ) { 
    return;
}

$nbEmailParEnvoi = 30;
$nbEmailParEnvoiMessagerieInterne = 1000;

require("phpmailer/class.phpmailer.php");  
$mail = new PHPMailer();

//fonction ajax pour envoyer une mailing list par paquets
if (isset($_POST['mail_html'])) {
    //$mail_texte = utf8_decode(stripslashes($_POST['mail_texte']));
    $mail_html = utf8_decode(stripslashes($_POST['mail_html']));
    $mail_subject = utf8_decode(stripslashes($_POST['mail_subject']));
    $destinataire_mailing = mysql_real_escape_string($_POST['destinataire_mailing']);
    $emails_choisis = mysql_real_escape_string($_POST['emails_choisis']);
    $indiceEnvoi = mysql_real_escape_string($_POST['indiceEnvoi']);
    
    $mail->From     = "contact@hamsteracademy.fr";  
    $mail->FromName = "Hamster Academy";
    $mail->Subject  = $mail_subject;  
    $mail->IsHTML(true);
    $mail->CharSet = 'UTF-8';
    
    // insertion des images dans le mail sous forme embedded
//    $toutesimagestrouvees = false;
//    $prec = 0;
//    $indiceImage = 0;
//    while (!$toutesimagestrouvees) {
//        $prec = strpos($mail_html,"images/",$prec) ;
//        if ($prec === false) {
//            break;
//        }
//        else{
//            $extension_pos = strpos($mail_html,".",$prec);
//            $finChaine = $extension_pos+4;
//            $extension = substr($mail_html,$extension_pos+1,3);
//            if ($extension == "jpg")
//                $extension = "jpeg";
//            $urlImage = substr($mail_html,$prec,$finChaine-$prec) ;
//            $mail->AddEmbeddedImage($urlImage,"image".$indiceImage,substr($urlImage,7),"base64","image/".$extension);
//            $mail_html = str_replace($urlImage,"cid:image".$indiceImage,$mail_html);
//            $indiceImage++;
//        }
//    }

    //$mail->Body     = $mail_html;
    //$mail->AltBody  = $mail_texte;  
    //$mail->WordWrap = 50;      
    
    // envoi de test à qq mails
    if ($destinataire_mailing == "mailing_emails_choisis"){ 
        
        // on incruste les identifiants du joueur
        $mail_html_personnalise = str_replace("#pseudo","test pseudo",$mail_html);
        $mail_html_personnalise = str_replace("#passwd","test passwd",$mail_html_personnalise);
        $mail->MsgHTML($mail_html_personnalise);
        $mail->AddAddress($emails_choisis); 

        if(!$mail->Send()) {  
        //if(0) {  
            echo "0,0,".$mail->ErrorInfo; // fin d'envoi et 0 mail envoyé
        } else {  
            echo "0,1,".$mail->ErrorInfo; // fin d'envoi et 1 mail envoyé
        }          
    }
    else if( $destinataire_mailing == "mailing_emails_valides"){
        
        $lst_erreurs="erreur : ";
       
        $query = "SELECT j.pseudo, j.email, j.passwd, j.joueur_id FROM joueurs j, mailing m WHERE j.email_active = 1 AND j.newsletter = 1 AND j.joueur_id = m.joueur_id AND m.statut = 1 LIMIT $nbEmailParEnvoi";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
        }
        $nbMailsEnvoyes = 0 ;
        $nbMailsEnvoyesTheoriques =  $dbHT->sql_numrows($result) ;
       
        while($row=$dbHT->sql_fetchrow($result)) {
            
            // on incruste les identifiants du joueur
            $mail_html_personnalise = str_replace("#pseudo",$row['pseudo'],$mail_html);
            $mail_html_personnalise = str_replace("#passwd",$row['passwd'],$mail_html_personnalise);
            
            $mail->MsgHTML($mail_html_personnalise);
            $mail->AddAddress($row['email']);  
            
            if($mail->Send()) {
            //if (1) {
                $nbMailsEnvoyes++;
                
                $queryInsert = "UPDATE mailing SET statut = 2 WHERE joueur_id = ".$row['joueur_id'];
                $dbHT->sql_query($queryInsert);
            }
            else {
                $lst_erreurs .= $mail->ErrorInfo.",";
                
                $queryInsert = "UPDATE mailing SET statut = 3 WHERE joueur_id = ".$row['joueur_id'];
                $dbHT->sql_query($queryInsert);
            }
            
            $mail->ClearAddresses();
            $mail->ClearAttachments();
            
        }
        $dbHT->sql_freeresult($result);
        
        // on envoie un mail de verif pour s'assurer que chaque groupe de mails ait bien été envoyé
//        $mail->ClearAddresses();
//        $mail->AddAddress("contact@hamsteracademy.fr");
//        $mail->IsHTML(false);
//        $mail->Subject     = "[HamsterAcademy] - Envoi groupe $indiceEnvoi, nb mails envoyes $nbMailsEnvoyes / $nbMailsEnvoyesTheoriques";  
//        $mail->Body = "--fin";
//        $mail->Send();
        
        // envoi du résultat
        if ($nbMailsEnvoyesTheoriques < $nbEmailParEnvoi)
            echo "0,$nbMailsEnvoyes,$lst_erreurs"; // fin d'envoi et n mail envoyés
        else
            echo "1,$nbMailsEnvoyes,$lst_erreurs"; // envoi à poursuivre et n mail envoyés
                 
    }
    else if( $destinataire_mailing == "mailing_interne"){
       
        $query = "SELECT j.pseudo, j.email, j.passwd, j.joueur_id FROM joueurs j, mailing m WHERE j.email_active = 1 AND j.joueur_id = m.joueur_id AND m.statut = 1 LIMIT ".($nbEmailParEnvoiMessagerieInterne*$indiceEnvoi).",".$nbEmailParEnvoiMessagerieInterne ;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
        }
        $nbMailsEnvoyes = 0 ;
        $nbMailsEnvoyesTheoriques =  $dbHT->sql_numrows($result) ;
       
        while($row=$dbHT->sql_fetchrow($result)) {
            
            envoyerMessagePrive($row['joueur_id'],str_replace("'","\'",$mail_html),HAMSTER_ACADEMY_ID,false);
            $nbMailsEnvoyes++;
            $queryInsert = "UPDATE mailing SET statut = 2 WHERE joueur_id = ".$row['joueur_id'];
            $dbHT->sql_query($queryInsert);
        }
        $dbHT->sql_freeresult($result);
        
        // envoi du résultat
        if ($nbMailsEnvoyesTheoriques < $nbEmailParEnvoiMessagerieInterne)
            echo "0,$nbMailsEnvoyes"; // fin d'envoi et n mail envoyés
        else
            echo "1,$nbMailsEnvoyes"; // envoi à poursuivre et n mail envoyés
    }
    else {
        echo "0,-1"; // fin d'envoi et 0 mail envoyé   
    }    
}
else {
    echo "-1,0"; // fin d'envoi et 0 mail envoyé   
}
?>