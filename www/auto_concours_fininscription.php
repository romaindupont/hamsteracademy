<?php 

define('IN_HT', true);

include "common.php";

error_reporting( E_ALL );

// s'il n'y pas a le concours mensuel de la cage
if (getConfig("concours_cage_auto_active") == 0)
    return;

// on vide la table
// ----------------
$queryViderTable = "TRUNCATE TABLE lst_cages_concours";

if ( ! $dbHT->sql_query($queryViderTable) ){
    message_die(GENERAL_ERROR, 'Error :', '', __LINE__, __FILE__, $queryViderTable);
}

// liste des joueurs inscrits
// --------------------------
$query = "SELECT joueur_id, inscritConcours FROM joueurs WHERE inscritConcours > 0";

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error :', '', __LINE__, __FILE__, $query);
}
$nbJoueursInscrits = $dbHT->sql_numrows($result) ;
$indiceCage = 0;

while($row=$dbHT->sql_fetchrow($result)) {
    
    $cage_id = -1; // indice de la cage qui sera insérée dans la table "lst_cages_concours"
    
    // on regarde si le joueur a choisi une cage
    if ($row['inscritConcours'] == 1) {
        // on prend sa première cage
        $query2 = "SELECT cage_id FROM cage WHERE joueur_id=".$row['joueur_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
        }
        $rowCage=$dbHT->sql_fetchrow($result2);
        $cage_id = $rowCage['cage_id'];
        $dbHT->sql_freeresult($result2);    
    }
    else {
        // le joueur a choisi lui-même sa cage
        $cage_id = $row['inscritConcours'];
    }
    
    $joueur_proprietaire_id = $row['joueur_id'] ;
    
    // on insère la cage
    $query3 = "INSERT INTO lst_cages_concours VALUES(".$indiceCage.",".$cage_id.",".$joueur_proprietaire_id.",0,0)";
    if ( !($result3 = $dbHT->sql_query($query3)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query3);
    }
    $indiceCage++;
    $dbHT->sql_freeresult($result3);    
}
$dbHT->sql_freeresult($result);

// réinit du statut a_vote des joueurs
// -----------------------------------
$queryVote = "UPDATE joueurs SET a_vote = 0";
if ( ! $dbHT->sql_query($queryVote) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $queryVote);
}

// on doit calculer le nombre d'accessoires de la cage
// ---------------------------------------------------
$query = "SELECT cage_id FROM lst_cages_concours";

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
}

$lstAccessoiresTxt = "";

while($row=$dbHT->sql_fetchrow($result)) {
    
    // tout sauf la cabane (34) et le nid (28)
    $query2 = "SELECT accessoire_id FROM accessoires WHERE cage_id = ".$row['cage_id']." AND type != 34 AND type != 28";

    if ( !($result2 = $dbHT->sql_query($query2)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query2);
    }
    $nbAccessoires = $dbHT->sql_numrows($result2) ;
    
    $query3 = "UPDATE lst_cages_concours SET nb_accessoires = ".$nbAccessoires." WHERE cage_id=".$row['cage_id'];

    if ( !($dbHT->sql_query($query3))){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query2);
    }
    $lstAccessoiresTxt .= "cage ".$row['cage_id']." mise à jour : nb accessoires = ".$nbAccessoires."\n";
}


// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$h  = "From: contact@hamsteracademy.fr";
$subject = "Concours: fin des inscriptions et début des votes" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= "1) Nb de cages ajoutées : ".$nbJoueursInscrits."\n2) Votes remis à zéro\n\n";
$message .= "3) Nb accessoires pour chaque cage :\n".$lstAccessoiresTxt."\n\n";
$message .= "--\n";
$mail_sent = @mail($TO, $subject, $message, $h);

?>
