<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$rayon = RAYON_ENTRETIEN;
if (isset($_GET['rayon']))
    $rayon = intval($_GET['rayon']) ;
    
if ($univers == UNIVERS_ACADEMY && $userdata['niveau'] < $niveauMinimalAccesAcademy) {
    echo T_("L'academy n'est accessible qu'à partir du niveau ").$niveauMinimalAccesAcademy.".";
    return;
}

// debut de la page => on commence par afficher les resultats de la vente si elle a lieue
if (isset($_GET['achat'])) {
    $objet_a_acheter = intval($_GET['achat']) ;
    $query = "SELECT * FROM config_accessoires WHERE type=".$objet_a_acheter." LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
    }
    $objet=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    
    if (isset($objet['image_boutique']) && $objet['image_boutique'] != "")
        $objet['image'] = $objet['image_boutique'];

    // quantité sélectionnée par le joueur
    $quantiteSelectionnee = 1;
    if (isset($_GET['quantite'])) {
        $quantiteSelectionnee = intval($_GET['quantite']) ;
        if ($quantiteSelectionnee < 0)
            $quantiteSelectionnee = 1;    
    }
    $prix = $objet['prix']*$quantiteSelectionnee;
    if ($userdata['vip']) // réduction vip
        $prix = round($vip_reducAchat * $prix);
        
    if ($resultatAchat == 0) { // le joueur n'a pas assez d'argent
        
        echo COLONNE_TOP;
        $nomObjet = $objet['nom_fr'];
        if ($lang == "en")
            $nomObjet = $objet['nom_en'];
        
        $msgTmp = "<strong>".T_("Tu n'as pas assez d'argent pour acheter cet objet ").($quantiteSelectionnee>1?"(".T_("quantité")." : ".$quantiteSelectionnee.")":"")." :</strong>";
        $msgTmp .= "<div align=\"center\"><img src=\"images/".$objet['image']."\" alt=\"".$nomObjet."\" align=\"middle\" /><br/>".$nomObjet." (".T_("coût")." : ".$prix." ".IMG_PIECE.")</div><br/>" ;
        $msgTmp .= "<br/>".T_("Il te manque ").($prix - $userdata['nb_pieces']).IMG_PIECE. " ...";
        
        echo afficherPasAssezArgent($msgTmp,PAGE_JEU_ACHAT);
        echo "<br/><a href=\"jeu.php?mode=".$pagePrecedente."&amp;hamster_id=".$hamster_id."\">".T_("Retour")."</a>";
        
        echo COLONNE_BOTTOM;
    }
    else if ($resultatAchat == 1 && ! isset($_GET['pourlacage_id'])) { // le joueur a achete mais doit choisir sa cage
        
        if ($msg != "")
            echo $msg;
        
        ?>
        <div style="width:940px;">

          <div class="hamBlocColonne-top">
             <div class="hamBlocColonne-top-left"></div>
             <div class="hamBlocColonne-top-right"></div>
             <div class="hamBlocColonne-top-center"></div>        
          </div>

            
          <div class="hamBlocColonne_full">
          <div align="center">
        <strong>
        <?php
        $nomObjet = $objet['nom_fr'];
        if ($lang == "en")
            $nomObjet = $objet['nom_en'];
            
        echo T_("Félicitations ! Tu viens d'acheter cet objet ").($quantiteSelectionnee>1?"(".T_("quantité")." : ".$quantiteSelectionnee.")":"")." :</strong><br/>&nbsp;<br/>";
        echo "<img src=\"images/".$objet['image']."\" alt=\"$nomObjet\" align=\"middle\" /> $nomObjet (".T_("coût")." : ".$prix." ".IMG_PIECE.")<br/>" ;
        echo "<br/>".T_("Il te reste maintenant ").$userdata['nb_pieces']." ".IMG_PIECE.".<br/>";
        if ($objet['danslacage'] == 1) {
            echo "<br/><div><strong>".T_("Choisis maintenant où tu veux mettre l'objet")." :</strong></div><br/>&nbsp;<br/>" ;
            echo "<table align=\"center\">\n" ;
            for($cageIndex=0; $cageIndex < $nbCages+1; $cageIndex++) {
                // 3 cages par ligne 
                if ( ($cageIndex) % 3 == 0 )
                    echo "<tr valign=\"top\">";
                    
                echo "<td align=\"center\" width=\"33%\">";
                echo "<form action=\"jeu.php\" method=\"get\">\n";
                echo "<input type=\"hidden\" name=mode value=\"m_achat\" />";
                echo "<input type=\"hidden\" name=action value=\"mettreObjetDans\" />";

                if ($cageIndex == 0){  // premiere cage = bac fourre-tout
                    echo "<img src=\"images/bac_reduc.gif\"><br/>&nbsp;<br/>";
                    echo "<input type=\"hidden\" name=\"deplacer_dans_cage_id\" value=\"-1\" />";
                    echo "<input type=\"hidden\" name=\"accessoire_id\" value=\"".$nouvelAccessoireId."\" />";
                    echo "<input type=\"submit\" value=\"Dans le bac fourre-tout\" />";
                }
                else {
                    echo "<img src=\"images/cage_reduc.gif\" alt=\"Cage\" />";
                    $cageId = $lst_cages[$cageIndex-1]['cage_id'] ;
                    echo "<br/>&nbsp;<br/>";
                    echo "<input type=\"hidden\" name=\"deplacer_dans_cage_id\" value=\"".$cageId."\" />";
                    echo "<input type=\"hidden\" name=\"accessoire_id\" value=\"".$nouvelAccessoireId."\" />";
                    echo "<input type=\"submit\" value=\"".T_("Dans ").$lst_cages[$cageIndex-1]['nom']."\" />";
                }
                echo "</form></td>\n";
                if ( $cageIndex > 2 && ($cageIndex+1) % 3 == 0 )
                    echo "</tr>" ;
            }
            echo "</table>";
        }
        else {
            ?>
            <br/>&nbsp;<br/>
            <br/>&nbsp;<br/>
            <br/>&nbsp;<br/>
            <br/>&nbsp;<br/>
            <br/>&nbsp;<br/>
            
            <div class="centre" style="width:170px; margin-top:10px; "><a href="jeu.php?mode=m_achat&amp;univers=<?php echo $univers; ?>"><span><?php echo T_("Revenir à la boutique");?></span></a></div>
            <?php
        }
        ?>
        
        </div>
          <div style="clear:both;">&nbsp;</div>

          </div>
            
          <div class="hamBlocColonne-bottom">
             <div class="hamBlocColonne-bottom-left"></div>
             <div class="hamBlocColonne-bottom-right"></div>
             <div class="hamBlocColonne-bottom-center"></div>        
          </div> 
          
        </div>

        <?php
    }
}

else {
    
    // affichage de la boutique
    
    if ($msg != "") // au cas où, pour les niveaux/missions
        echo $msg;
        
    // liste des rayons
    $lstRayons = "<div class=\"boutiqueRayon ".(($rayon == RAYON_ENTRETIEN) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_ENTRETIEN.lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\"><img src=\"images/rayon_entretien.gif\" alt=\"\" /><br/>".T_("Tout pour le hamster")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_ACC_CAGE) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_ACC_CAGE.lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\"><img src=\"images/rayon_acc_cage.gif\" alt=\"\" /><br/>".T_("Tout pour la cage")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_SPORT) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_SPORT.lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\"><img src=\"images/rayon_sport.gif\" alt=\"\" /><br/>".T_("Articles de sport")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_BEAUTE) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_BEAUTE.lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\"><img src=\"images/rayon_beaute.gif\" alt=\"\" /><br/>".T_("Beauté / Pharmacie")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_INSTRUMENT) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_INSTRUMENT.lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\"><img src=\"images/guitare.gif\" alt=\"\" /><br/>".T_("Instruments de musique")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_HAMSTER) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_HAMSTER."\"><img src=\"images/nouvelHamster_reduc.gif\" alt=\"\" /><br/>".T_("Nouvel hamster")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_CAGE) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_CAGE."\"><img src=\"images/cage_reduc2.gif\" alt=\"\" /><br/>".T_("Nouvelle cage")."</a></div>";
    $lstRayons .= "<div class=\"boutiqueRayon ".(($rayon == RAYON_BANQUE) ? "boutiqueRayonSelected" : "")."\"><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_BANQUE."&amp;pagePrecedente=".PAGE_JEU_ACHAT_PIECE."\"><img src=\"images/pieces_bonus_reduc.gif\" alt=\"\" /><br/>".T_("Achat de pièces")."</a></div>";
                
    $smarty->assign('msg',$msg) ;
    
    $smarty->assign('lstRayons',$lstRayons) ;
    $smarty->assign('ingredient3',insererLienIngredient(3)) ;
    
    $rayonTxt = "";
    ob_start();
    
    if ($rayon == RAYON_HAMSTER) {
        
        $mode = "jeu";
        $prenomHamster = "" ;
        $erreurPrenomHamster=0;
        $erreurCaracteres=0;
        $prefHamster_1="coquet";
        $prefHamster_2="fort";
        $sexeHamster=0;
        $typeHamster = 0;
        
        if (isset($_GET['achatHamster'])) {
            $erreur = 0;
            $erreurPrenomHamster = 0;
            $erreurCaracteres = 0;
            $etapeVerifHamster = 2;
            require "verifHamster.php";
            if ($erreur == 0) { // si pas de pb, on achete l'hamster
                // dans quelle cage ? :
                $cageDest = intval($_POST['cageHamster']) ;
                
                // on ajoute l'hamster
                $hamster_id = ajouterHamsterDansBDD($prenomHamster, $sexeHamster, $prefHamster_1, $prefHamster_2, $typeHamster, $cageDest, $userdata['joueur_id'],604800,-1,-1,$userdata['niveau']);
    
                // on debite le joueur
                debiterPieces($prixHamster,"achat de hamster") ;
    
                if ($hamster_id == -1) {
                    echo "Erreur ajout d'hamster<br>" ;
                }
                
                // on se redirige vers la page avec le nouvel hamster
                redirectHT("jeu.php?mode=m_hamster&amp;hamster_id=".$hamster_id);
            }
            else {
                require "formulaireNouvelHamster.php" ;
            }
        }
        else {
            echo "<div align=\"center\">".str_replace("#1",$prixHamster,T_("Tu peux acheter ici un nouvel hamster. Il te coutera #1 pièces. Choisis-le bien !"))."</div><br/>&nbsp;<br/>";    
            require "formulaireNouvelHamster.php" ;
        }
                
    }
    else if ($rayon == RAYON_CAGE) {
        require "formulaireNouvelleCage.php";    
    }
    else if ($rayon == RAYON_BANQUE) {
        require "formulaireBanque.php";
    }
    else {
    
        // on récupère l'objet en cours d'affichage
        $objet = -1;
        if (isset($_GET['objet'])) {
            $objet = intval($_GET['objet']);
        }
        
        // on affiche tout le rayon
        echo "\n<div class=\"boutiqueEtagere\">";
        $query = "SELECT * FROM config_accessoires WHERE rayon=".$rayon." AND prix != -1 ORDER BY niveau ASC";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        $obj_index = -1 ;
        
        while($row=$dbHT->sql_fetchrow($result)) {
                $objectHeight = min($etagereHauteurMaxObjet,round($row['img_height']*$row['echelle']));
                $obj_index ++;
                
                // 1er objet valide du rayon => si $objet=-1, on lui donne cette valeur : ça permet d'afficher au moins le 1er article du rayon.
                if ($objet == -1)
                    $objet = $row['type'];
                    
               if (isset($row['image_boutique']) && $row['image_boutique'] != "")
                    $row['image'] = $row['image_boutique'];
                
                echo "\n<div class=\"boutiqueElementEtagere ".(($row['type'] == $objet)? "boutiqueElementEtagereSelected":"")." ";
                if ($row['niveau'] > $userdata['niveau'])
                    echo "elementFlou\"".tooltip(T_("Accessible au niveau ").$row['niveau']).">";
                else
                    echo "\" >";
                
                if ($row['niveau'] <= $userdata['niveau'])
                    echo "<a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".$rayon."&amp;objet=".$row['type'].lienPagePrec($pagePrecedente,$hamster_id,$cage_id)."\" title=\"Voir cet objet\">";
                    
                $nomObjet = $row['nom_fr'];
                if ($lang == "en")
                    $nomObjet = $row['nom_en'];
                echo "<img src=\"images/".$row['image']."\" alt=\"\" height=\"".$objectHeight."\" /><br/>".$nomObjet;
                
                if ($row['niveau'] <= $userdata['niveau'])
                    echo "</a>\n";
                else
                    echo "</span>";
                echo "</div>";
        }
        
        $dbHT->sql_freeresult($result);

        echo "\n</div><div class=\"clear\">&nbsp;</div>";
        
        // on affiche la fiche d'achat de l'objet sélectionné
        if ($objet != -1) {
            $query = "SELECT * FROM config_accessoires WHERE type=".$objet." LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            }
            $objet_infos=$dbHT->sql_fetchrow($result);
            $dbHT->sql_freeresult($result);

            if (isset($objet['image_boutique']) && $objet['image_boutique'] != "")
                $objet['image'] = $objet['image_boutique'];
                
            $nomObjet = $objet_infos['nom_fr'];
            $descObjet = $objet_infos['desc_fr'];
            if ($lang == "en") {
                $nomObjet = $objet_infos['nom_en'];
                $descObjet  = $objet_infos['desc_en'];
            }
                        
            echo "\n<div class=\"boutiqueObjet\"><form action=\"jeu.php\" method=\"get\" name=\"form_achat\">\n";
            echo "<div><strong>".$nomObjet."</strong></div>";
            echo "<div style=\"padding:20px 0 0 0;\">".$descObjet."</div>";
            echo "<div style=\"padding:20px 0 0 0;\">".T_("Prix")." : ".$objet_infos['prix'].IMG_PIECE;
            if ($userdata['vip']) // réduction vip
                echo " (".T_("prix VIP")." : ".round($vip_reducAchat * $objet_infos['prix']).IMG_PIECE.")";
            echo "</div>";
            if ($objet_infos['danslacage']==0) {
                echo "<div style=\"padding:20px 0 0 0;\">".T_("Quantité")." : <input type=\"text\" value=\"1\" name=\"quantite\" size=\"1\" /></div>";
            }
            echo "<div style=\"padding:20px 0 0 0;\">";
            echo "<input type=\"hidden\" name=\"mode\" value=\"m_achat\" /><input type=\"hidden\" name=\"rayon\" value=\"".$rayon."\" /><input type=\"hidden\" name=\"achat\" value=\"".$objet."\" /><input type=\"hidden\" name=\"pagePrecedente\" value=\"".$pagePrecedente."\" /><input type=\"hidden\" name=\"cage_id\" value=\"".$cage_id."\" /><input type=\"hidden\" name=\"hamster_id\" value=\"".$hamster_id."\" /><input type=\"hidden\" name=\"univers\" value=\"".$univers."\" />";
            //echo "<input type=\"submit\" value=\"Acheter !\" /></div>";
            echo "<a href=\"#\" onclick=\"javascript:document.forms.form_achat.submit(); this.blur(); return false;\"><span>".T_("Acheter !")."</span></a></div>";
            //class= \"button_gris\" 
            echo "\n</form>";
            if ($rayon == 2)
                echo afficherObjet("oeufs",5);
            echo "</div>";
        }
    }
    
    $rayonTxt .= ob_get_contents();
    ob_end_clean();
    
    $smarty->assign('rayon',$rayonTxt) ;
    $smarty->display('boutique.tpl');   
    

} // fin du else

?>
