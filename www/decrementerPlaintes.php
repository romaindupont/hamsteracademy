<?php 

define('IN_HT', true);

include "common.php";

// on récupère la liste des joueurs qui possèdent des plaintes
$query = "SELECT joueur_id, pseudo, nb_plaintes FROM joueurs WHERE nb_plaintes > 0";

if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining nb_plaintes', '', __LINE__, __FILE__, $query);
}

$nbJoueurs = $dbHT->sql_numrows($result) ;

$lstModifsTxt = "";

while($row=$dbHT->sql_fetchrow($result)) {

	$joueur_id = $row['joueur_id'] ;
	
	$nouveau_nb_plaintes = $row['nb_plaintes'] - 1 ;
	if ($nouveau_nb_plaintes < 0)
		$nouveau_nb_plaintes = 0; 
	
	$query2 = "UPDATE joueurs SET nb_plaintes = ".$nouveau_nb_plaintes." WHERE joueur_id = ".$joueur_id ;
	$dbHT->sql_query($query2) ;
	
	$lstModifsTxt .= $row['pseudo'] . " : " .$nouveau_nb_plaintes. "\n" ;
}

$dbHT->sql_freeresult($result);

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$h  = "From: contact@hamsteracademy.fr";
$subject = "Hamster Academy : décrémentation des plaintes, bilan " ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= "Nb de joueurs concernés : ".$nbJoueurs."\n";
$message .= $lstModifsTxt."\n\n--\nFin\n";

$mail_sent = @mail($TO, $subject, $message, $h);

$dbHT->sql_close();

?>
