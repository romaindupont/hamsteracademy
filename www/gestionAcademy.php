<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

$typeGroupes = array (
    1 => array(
        'LE_GROUPE' => T_("le groupe"),
        'UN_GROUPE' => T_("un groupe"),
        'DE_GROUPE' => T_("de groupe"),
        'DU_GROUPE' => T_("du groupe"),
        'SON_GROUPE' => T_("son groupe"),
        'GROUPE' => T_("groupe"),
        'GROUPES' => T_("groupes"),
        'NB_MAX_JOUEURS' => 8,
        'NIVEAU_MIN' => 3
        ),
    2 => array(
        'LE_GROUPE' => T_("l'équipe"),
        'UN_GROUPE' => T_("une équipe"),
        'DE_GROUPE' => T_("d'équipe"),
        'DU_GROUPE' => T_("de l'équipe"),
        'SON_GROUPE' => T_("son équipe"),
        'GROUPE' => T_("équipe"),
        'GROUPES' => T_("équipes"),
        'NB_MAX_JOUEURS' => 14,
        'NIVEAU_MIN' => 0
        )
    );

function checkNiveauGroupe($groupe) {

    global $lstNiveauxGroupe, $dbHT;
    
    if ($groupe['type'] == 1 && $groupe['niveau'] < sizeof($lstNiveauxGroupe) -1 ){
        
        if ($groupe['experience'] >= $lstNiveauxGroupe[ ($groupe['niveau']+1) ][1]) {
                        
            $groupe['niveau']++;
            $query = "UPDATE groupes SET niveau = ".$groupe['niveau']." WHERE groupe_id=".$groupe['groupe_id'];
            $dbHT->sql_query($query);
            
            ajouterBulletin($groupe['groupe_id'],$groupe['leader_id'],"Leader",T_("Le groupe ").$groupe['nom'].T_(" vient de monter au niveau ").($groupe['niveau']+1). " !",true);
        }    
    }
}

function ajouterBulletin($groupe_id, $hamster_id, $nomHamster, $message, $alaposte = false) {
    
    global $dbHT, $dateActuelle; 
    
    $query = "SELECT MAX(bulletin_id) as bulletin_id FROM groupe_bulletin";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        message_die(GENERAL_ERROR, 'Error in obtaining max bulletin id', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);

    if ($message != "") {
        $query = "INSERT INTO groupe_bulletin VALUES ( ".$nouveauId." , ".$groupe_id." , ".$hamster_id." , 
            '".$message."', ".$dateActuelle.", '".$nomHamster."' ) ";
        if (! $dbHT->sql_query($query) )
            echo "Pb sql : ".$query."<br/>" ;
        
        $query2 = "UPDATE groupes SET derniere_activite = ".$dateActuelle." WHERE groupe_id = ".$groupe_id;
        $dbHT->sql_query($query2);
        
        if ($alaposte) {
            $query = "SELECT joueur_id FROM hamster WHERE groupe_id = ".$groupe_id. " LIMIT 8";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $message = T_("(Message automatique) Un nouveau message dans l\'un de tes groupes de la part de")." ".$nomHamster." : ".$message;
            while($rowMembre=$dbHT->sql_fetchrow($result)) {
                envoyerMessagePrive($rowMembre['joueur_id'],$message);
            }
        }
    }    
}

function afficherOptionsTri($commande, $commandeTxt, $sensTri, $triencours,$hamster_id,$nouveau_sens,$voirtouslesgroupes) {
    
    $encours = ($triencours == $commande);
    
    $txt = "<a href=\"jeu.php?mode=m_groupe&amp;".($voirtouslesgroupes? "voirtouslesgroupes=1" : "")."&amp;hamster_id=".$hamster_id."&amp;tri=$commande&amp;".$nouveau_sens."=1\">".($encours ? "<span class=Selected>" : "")."$commandeTxt".($encours ? "</span>" : "")."</a>";
    if ($encours)
        $txt .= " <img src=\"images/sort_$sensTri.gif\" alt=\"\" />";
    $txt .= " | ";
    echo $txt;
}

function afficherTousLesGroupes($boutonRejoindre, $typeGroupe = 1, $hamster_id = -1, $fromGrp = 0) {

    global $dbHT, $lstInstruments, $lstNiveauxGroupe, $lstNiveauxEquipe, $lang;
    define ("NbGroupesAffiches", 21);
    
    $orderby = "derniere_activite";
    $tri = "activite";
    if (isset($_GET['tri'])) {
        $tri = mysql_real_escape_string($_GET['tri']); 
        if ($tri == "points")
            $orderby = "note";
        else if ($tri == "nom")
            $orderby = "nom";
        else if ($tri == "nb_membres")
            $orderby = "nb_membres";
        else if ($tri == "experience")
            $orderby = "experience";
        else if ($tri == "activite")
            $orderby = "derniere_activite";
    }
    $sens = "desc";
    if (isset($_GET['asc'])) {
        $sens = "asc";
    }
    
    $query = "";
    if ($typeGroupe > 0)
        $query = "SELECT * FROM groupes WHERE lang = '".$lang."' AND type = $typeGroupe";
    else
        $query = "SELECT * FROM groupes WHERE lang = '".$lang."' AND type >= 0"; // tous types de groupe
    $searchTxt = "";
    if (isset($_GET['searchGroup'])) 
        $searchTxt = mysql_real_escape_string($_GET['searchGroup']);
    if ($searchTxt != "")
       $query .= " AND nom LIKE '%".$searchTxt."%'" ;

    //$query .= " ORDER BY ".$orderby." ASC LIMIT $fromGrp,30";
    $query .= " ORDER BY ".$orderby." ".$sens." LIMIT ".$fromGrp.",".NbGroupesAffiches."";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    if ($nbGroupes == 0) {
        echo T_("Aucun groupe trouvé...");   
    }
    else {
        if (isset($_GET['desc']))
            $nouveau_sens = "asc";
        else
            $nouveau_sens = "desc";
        
        echo "<div class=\"boutonTriGroupes\">";
        echo "<form action=\"jeu.php\">";
        echo T_("Trier par")." : ";
        
        afficherOptionsTri("nom",T_("Nom"),$sens, $tri,$hamster_id,$nouveau_sens,! $boutonRejoindre);
        afficherOptionsTri("nb_membres",T_("Nb de joueurs"),$sens, $tri,$hamster_id,$nouveau_sens,! $boutonRejoindre);
        afficherOptionsTri("points",T_("Points"),$sens, $tri,$hamster_id,$nouveau_sens,! $boutonRejoindre);
        afficherOptionsTri("activite",T_("Activité récente"),$sens, $tri,$hamster_id,$nouveau_sens,! $boutonRejoindre);
        
        // formulaire de recherche de groupe
        echo " &nbsp; &nbsp; &nbsp; <input type=\"hidden\" name=\"mode\" value=\"m_groupe\" />";
        if (!$boutonRejoindre)
            echo "<input type=\"hidden\" name=\"voirtouslesgroupes\" value=\"1\" />";
        echo "<input type=\"hidden\" name=\"hamster_id\" value=\"".$hamster_id."\" />";
        echo "<input type=\"text\" name=\"searchGroup\" value=\"".( ($searchTxt != "") ? $searchTxt : T_("nom du groupe à chercher") )."\" size=\"30\" />";
        echo "<input type=\"submit\" name=\"submitSearch\" value=\"".T_("Chercher")."\" />";
        echo "</form><br/>";
        echo "</div>\n";
        
        echo "<div class=\"listeGroupes\">\n";
        $nbGroupesAffiches = 0;
        
        while($row=$dbHT->sql_fetchrow($result)) {
            
            echo "<div class=\"blocRounded\" style=\"width:230px;\">";
            
            echo "<div".tooltip(str_replace("\"","'",$row['description'])).">";
            if ($row['image'] > 0){
                $extensionImage = "jpg";
                if ($row['image'] == 2) // extension gif
                    $extensionImage = "gif";
                echo "<img src=\"images/groupes/".$row['groupe_id'].".".$extensionImage."\" width=\"50\"> ";
            }             
            afficherLienProfilGroupe($row['groupe_id'],tronquerTxt(htmlspecialchars($row['nom']),25));
            echo "</div>".$row['nb_membres'];
            if ($row['type'] == 1)
                echo T_(" musicien");
            else
                echo T_(" joueur");
            echo ($row['nb_membres']>1 ? "s":"");
            if ($row['type'] == 1 && $row['possede_manager'])
                echo " + 1 manager";
            echo " : ";
            // liste des instruments: 
            if ($row['type'] == 1) {
                if ($row['possede_manager'])
                    echo "<img src=\"images/manager2.gif\" class=\"miniInstrument\" alt=\"\" title=\"Manager de groupe\">";
                $queryInstruments = "SELECT specialite FROM hamster WHERE groupe_id = ".$row['groupe_id']." LIMIT 8";
                $resultInstruments = $dbHT->sql_query($queryInstruments);
                while($rowInstrument=$dbHT->sql_fetchrow($resultInstruments)) {
                    if ($rowInstrument['specialite'] >= 0)
                        echo "<img title=\"".$lstInstruments[$rowInstrument['specialite']][INSTRUMENT_SPECIALITE]."\" src=\"images/".getImageAccessoire($lstInstruments[$rowInstrument['specialite']][INSTRUMENT_REF_OBJET])."\" class=\"miniInstrument\">";
                }
            }
            else if ($row['type'] == 2){
                $queryInstruments = "SELECT specialite FROM hamster WHERE groupe_id = ".$row['groupe_id']." LIMIT 20";
                $resultInstruments = $dbHT->sql_query($queryInstruments);
                while($rowInstrument=$dbHT->sql_fetchrow($resultInstruments)) {
                    if ($rowInstrument['specialite'] == -1)
                        $rowInstrument['specialite'] = 0;
                    echo "<img src=\"images/".getImageSpecialiteFoot($rowInstrument['specialite'])."\" class=\"miniInstrument\">";
                }   
            }
            echo "<br/>".T_("Points")." : ".$row['note']." pts<br/>";
            echo T_("Niveau")." : ".($row['niveau']+1)." (";
            if ($row['type'] == 1)
                echo $lstNiveauxGroupe[$row['niveau']][0].")<br/>";   
            else if ($row['type'] == 2)
                echo $lstNiveauxEquipe[$row['niveau']][0].")<br/>";   
            echo T_("Expérience")." : ".$row['experience']." pts<br/>";   
            echo T_("Dernière activité")." : ".date("d/m/Y G:i ",$row['derniere_activite'])."<br/>";
            if ($hamster_id >= 0) {
                echo "<br/><a href=\"jeu.php?mode=m_groupe&amp;hamster_id=".$hamster_id."&amp;actionAcademy=rejoindreGroupe&amp;groupe_id=".$row['groupe_id']."\">".T_("Rejoindre")." !</a>";   
            }
            echo "</div>";            
            
            if ($nbGroupesAffiches % 3 == 2)
                echo "<div style=\"clear:both\">&nbsp;</div>";
                
            $nbGroupesAffiches++;
        }
        echo "</div><div style=\"clear:both\">&nbsp;</div>";
        
        // navigation dans les groupes
        echo "<div class=\"navigationGroupe\">";
        
        $query = "SELECT COUNT(groupe_id) FROM groupes";
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        $nbGroupesTotal = $row[0];
        $dbHT->sql_freeresult($result);
        
        $grpCourant = $fromGrp / NbGroupesAffiches ;
        $debutGrp = max(1,$grpCourant-5) ;
        $finGrp = min(max(10,$grpCourant+5),$nbGroupesTotal / NbGroupesAffiches + 1) ;

        if ($fromGrp > 0)
            echo "<a href=\"jeu.php?mode=m_groupe".(isset($_GET['voirtouslesgroupes'])?"&amp;voirtouslesgroupes=1":"")."&amp;hamster_id=".$hamster_id."&amp;fromGrp=".(($grpCourant-1)*NbGroupesAffiches)."&amp;tri=".$orderby."&amp;".$sens."=1\"><img src=\"images/groupes_precedents.png\" alt=\"".T_("Groupes précédents")."\" /></a> ";
        else
            echo "<img src=\"images/image_vide.gif\" style=\"width:177px;height:32px;\" />";
        
        if ($debutGrp > 1)
            echo " ... ";
        
        for($grp=$debutGrp;$grp < $finGrp;$grp++) {
            $isGrpCourant = ($fromGrp == ($grp-1)*NbGroupesAffiches)? 1 : 0;
            echo "<a href=\"jeu.php?mode=m_groupe".(isset($_GET['voirtouslesgroupes'])?"&amp;voirtouslesgroupes=1":"")."&amp;hamster_id=".$hamster_id."&amp;fromGrp=".(($grp-1)*NbGroupesAffiches)."&amp;tri=".$orderby."&amp;".$sens."=1\">".($isGrpCourant?"<b>":"").($grp).($isGrpCourant?"</b>":"")."</a> ";
        }
        if (($finGrp-1)*NbGroupesAffiches < $nbGroupesTotal)
            echo " ... ";
        if (($grpCourant+1)*NbGroupesAffiches < $nbGroupesTotal)
            echo " <a href=\"jeu.php?mode=m_groupe".(isset($_GET['voirtouslesgroupes'])?"&amp;voirtouslesgroupes=1":"")."&amp;hamster_id=".$hamster_id."&amp;fromGrp=".(($grpCourant+1)*NbGroupesAffiches)."&amp;tri=".$orderby."&amp;".$sens."=1\"><img src=\"images/groupes_suivants.png\" alt=\"".T_("Groupes suivants")."\"></a>";   
        else
            echo "<img src=\"images/image_vide.gif\" style=\"width:147px;height:32px;\" />";
            
        echo "</div>";        
        echo "<div align=\"right\">".insererLienIngredient(2)."</div>";
    }
    
    $dbHT->sql_freeresult($result);
}

function isLeader($groupeId,$hamsterId) {
    
    global $dbHT;
    
    $query = "SELECT COUNT(groupe_id) FROM groupes WHERE groupe_id = $groupeId AND leader_id = $hamsterId LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result) ;
    
    if ($row[0] != 1)
        return false;
    else 
        return true;
}

function getTournoiNom($tournoi_id) {
    global $dbHT;
    
    $query = "SELECT nom FROM tournois WHERE tournoi_id = $tournoi_id LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbTournois = $dbHT->sql_numrows($result) ;
    
    if ($nbTournois == 0)
        return T_("aucun tournoi");
    else {
        $row = $dbHT->sql_fetchrow($result) ;
        return $row['nom'];
    }
}

function getListeTournois(){
    
    global $dbHT, $hamster_id, $dateActuelle;
    
    $query = "SELECT * FROM tournois";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbTournois = $dbHT->sql_numrows($result) ;
    
    $txt = "";
    
    while ($row = $dbHT->sql_fetchrow($result) ) {

        $txt .= $row['nom']." : ";
        if ($row['inscriptions_ouvertes'] && $dateActuelle < $row['date_cloture_inscription'])
            $txt .= "<a href=\"jeu.php?mode=m_groupe&amp;actionAcademy=inscrireTournoi&amp;hamster_id=".$hamster_id."&amp;tournoi_id=".$row['tournoi_id']."\">".T_("Inscrire le groupe à ce tournoi")."</a>";
        else
            $txt .= T_("Inscriptions closes");
        $txt .= " (".T_("fin des inscriptions le ").format_date($row['date_cloture_inscription'],true).")<br/>";
        
    }
    return $txt;
}

function updateClassementPoules($tournoi_id) {
    
    global $dbHT;
    
    for($pool = 0;$pool<8;$pool++) {
        
        $query = "SELECT * FROM groupes WHERE tournoi_pool = ".$pool. " AND inscrit_tournoi = ".$tournoi_id." ORDER BY score_pool DESC, groupe_id ASC";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        $classementGroupe = 1;
        
        while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
            
            $query2 = "UPDATE groupes SET classement_pool = $classementGroupe WHERE groupe_id = ".$rowGroupe['groupe_id'];
            if (! $dbHT->sql_query($query2)){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
            }
            $classementGroupe++;
        }
    }
}

function afficherPoules($tournoi) {
    
    $nomPool = array("A","B","C","D","E","F","G","H") ;
    global $dbHT,$dateActuelle, $userdata;
    
    if ($tournoi['niveau_actuel'] != -1) {
        echo "<div id=\"voirPoules\"><a href=\"#\" onclick='javascript:$(\"#affichagePoules\").show();$(\"#voirPoules\").hide();return false;'>(".T_("Voir les poules").")</a></div>";
        echo "<div id=\"affichagePoules\" style=\"display:none;\">";    
    }
    else 
        echo "<div>";
    
    echo "<strong>".T_("Principe des poules")."</strong> : ".T_("tous les groupes sont regroupés aléatoirement dans les poules.<br/>Puis on fait un classement des groupes dans chaque poule. Les 8 meilleurs groupes de chaque poule iront en 1/32ème de Finale")." !<br/>";
        
    for($pool = 0;$pool<8;$pool++) {
        echo "<div class=\"blocRounded\" style=\"width:160px;\">";
        
        echo "<h2 align=\"center\">".T_("Poule")." ".$nomPool[$pool]."</h2> ";
        
        // affichage des groupes de ce pool
        $query = "SELECT * FROM groupes WHERE tournoi_pool = ".$pool. " AND inscrit_tournoi = ".$tournoi['tournoi_id']." ORDER BY score_pool DESC, groupe_id ASC";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbGroupes = $dbHT->sql_numrows($result) ;
        $classementGroupe = 1;
        
        while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
            
            $nom_groupe = substr(htmlspecialchars($rowGroupe['nom']),0,25);
            if ($rowGroupe['score_pool'] == -1 || $userdata['pseudo'] != "César")
                echo "&middot; ";
            else
                echo $classementGroupe.". ";//."(".$rowGroupe['score_pool'].")";
            echo returnLienProfilGroupe($rowGroupe['groupe_id'],$nom_groupe);   
            echo "<br/>";
            $classementGroupe++;
        }
        
        echo "</div>";
        if ($pool % 4 == 3)
            echo "<div style=\"clear:both;\">&nbsp;</div>";
    }
    echo "</div>";
}

function repartirGroupesDansPoules($tournoi_id) {
        
    global $dbHT;
    
    // liste des groupes inscrits
    $query = "SELECT groupe_id, nom FROM groupes WHERE inscrit_tournoi = ".$tournoi_id." ORDER BY date_inscription_tournoi ASC";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }

    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    // on commence toujours le tournoi par les poules
    $modePool = true;
    $niveau = -1;    
    $poolCourante = 0;
   
    while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
        
        $query2 = "UPDATE groupes SET tournoi_pool = $poolCourante, score_pool = -1 WHERE groupe_id=".$rowGroupe['groupe_id']. " LIMIT 1";
        if ( ! $dbHT->sql_query($query2) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
        }
        
        // 8 poules au total
        $poolCourante = ($poolCourante+1)%8;
    }
    
    $queryTournoi = "UPDATE tournois SET niveau_actuel = $niveau, niveau_initial = $niveau WHERE tournoi_id = $tournoi_id";
    if ( ! $dbHT->sql_query($queryTournoi) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryTournoi);
    }
}

function afficherTableauTournoi($tournoi_id) {
        
    global $dbHT,$dateActuelle, $userdata,$lstNiveauxTournois;
        
    $query = "SELECT * FROM tournois WHERE tournoi_id = $tournoi_id";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbTournois = $dbHT->sql_numrows($result) ;
    
    if ($nbTournois == 0)
        return ;
    
    $rowTournoi = $dbHT->sql_fetchrow($result) ;
            
    echo "<div>".T_("Prochains matchs")." : vendredi 25 décembre à 18h</div>" ;
    
    // si les inscriptions sont encore ouvertes, on génère un nouveau tableau
    $tableauDefinitif = true;
    
    if ($dateActuelle < $rowTournoi['date_cloture_inscription'] && $rowTournoi['inscriptions_ouvertes']) 
        $tableauDefinitif = false;
        
    if (! $tableauDefinitif) {
        echo "<br/>&nbsp;<br/>";
        repartirGroupesDansPoules($tournoi_id);
    }
    
    if ($tableauDefinitif) {
        
        // le tableau est finalisé, on effectue le classement pour chaque poule (si le tableau n'est pas commencé, ie. on est en mode Poule)
        if (0 && $userdata['pseudo'] == "César" && $rowTournoi['niveau_actuel'] == -1) {
            
            if (0) {
                for($pool = 0;$pool<8;$pool++) {
                    matchsPool($pool);
                }
                updateClassementPoules($tournoi_id);
            }
            
            // on crée le tableau initial
            genererTableauInitial($tournoi_id);
        }
    }
    
    if (0 &&  $userdata['pseudo'] == "César")
        matchsTableau($rowTournoi);
    
    // affichage du tableau
    if ($rowTournoi['niveau_actuel'] != -1) {
        for($niveau=$rowTournoi['niveau_actuel'];$niveau <= $rowTournoi['niveau_initial'];$niveau++) {
                    
            if ($niveau > $rowTournoi['niveau_actuel']+1) {
                echo "<div id=\"voirTableau$niveau\"><a href=\"#\" onclick='javascript:$(\"#tableau$niveau\").show();$(\"#voirTableau$niveau\").hide();return false;'>(".T_("Voir le tableau des")." ".$lstNiveauxTournois[$niveau].")</a></div>";
                echo "<div style=\"display:none;\" id=\"tableau$niveau\">";
            }
            else {
                echo "<div>";
            }
            echo "<div class=\"blocRounded\" style=\"width:380px;\">";
            
            echo "<h2 align=\"center\">".$lstNiveauxTournois[$niveau]."</h2> ";
            echo ( ($niveau != $rowTournoi['niveau_initial'] || $tableauDefinitif) ? "":"<div align=\"center\">(".T_("liste non définitive").")<br/>&nbsp;<br/></div>");
            
            // liste des matchs
            $query = "SELECT * FROM tournoi_match WHERE niveau_dans_tournoi = ".$niveau. " ORDER BY match_id ASC";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbMatchs = $dbHT->sql_numrows($result) ;
            
            while ($rowMatch = $dbHT->sql_fetchrow($result) ){
                
                echo "<div style=\"width:300px;clear:both; margin-left:auto;margin-right:auto;\">";
                
                echo "<div style=\"text-align:center; float:left; border:1px solid grey; width:120px; margin-bottom:5px; padding: 5px 5px 5px 5px ;\">";
                if ($rowMatch['equipe1'] != -1)
                    echo returnLienProfilGroupe($rowMatch['equipe1'],$rowMatch['nom_groupe1']);   
                echo "<br/>vs<br/>";
                if ($rowMatch['equipe2'] != -1)
                    echo returnLienProfilGroupe($rowMatch['equipe2'],$rowMatch['nom_groupe2']);   
                else 
                    echo "...";
                echo "</div>";
                echo "<div style=\"margin-top:15px;width:20px;float:left; border-bottom:1px solid grey;\">&nbsp;</div>";
                
                if ($rowMatch['vainqueur'] == -1)
                    $txtVainqueur = "&nbsp;";
                else {
                    if ($rowMatch['vainqueur'] == $rowMatch['equipe1'])
                        $txtVainqueur = returnLienProfilGroupe($rowMatch['equipe1'],$rowMatch['nom_groupe1']);   
                    else
                        $txtVainqueur = returnLienProfilGroupe($rowMatch['equipe2'],$rowMatch['nom_groupe2']);   
                    
                }
                
                echo "<div style=\"margin-top:15px;width:120px;float:left; border:1px solid grey; margin-bottom:5px; padding: 5px 5px 5px 5px ;\">$txtVainqueur</div>";
                echo "</div>";
            }
            echo "</div>";
            echo "</div>";
            //echo "<div style=\"clear:both;\">&nbsp;</div>";
        }
        echo "<div style=\"clear:both;\">&nbsp;</div>";
    }
    
    // affichage des pools ou du tableau
    afficherPoules($rowTournoi);
            
    $dbHT->sql_freeresult($result) ;
    echo "<div style=\"clear:both;\">&nbsp;</div>";
}

function genererTableauInitial($tournoi_id) {
 
    global $dbHT,$lstNiveauxTournois,$userdata;
    
    if ($userdata['pseudo'] != "César")
        return;
    
    // on supprime le tableau initial en cours
    $query = "DELETE FROM tournoi_match WHERE tournoi_id = $tournoi_id";
     if ( ! $dbHT->sql_query($query) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }

    // on est au 32ème de finale
    $nbMatchs = 32;
    $niveau = round(log($nbMatchs) / log(2));
    if (pow(2,$niveau) < $nbMatchs)
        $niveau++;
        
    // liste des groupes inscrits
    $query = "SELECT * FROM groupes WHERE inscrit_tournoi = $tournoi_id AND classement_pool <= 8 AND classement_pool > 0 ORDER BY tournoi_pool ASC, classement_pool ASC";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }

    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    $lstGroupes = array();
    
    while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
        
        array_push($lstGroupes,$rowGroupe);
    }
    
    $match_id = 0+$tournoi_id*10000;
    
    for($pool=0;$pool<4;$pool++){
        
        for($match=0;$match<8;$match++){
    
            $rowGroupe1 = $lstGroupes[$match+$pool*8] ;
            $rowGroupe2 = $lstGroupes[(7-$match)+(7-$pool)*8] ;
            
            $nom_groupe1 = tronquerTxt(addslashes(htmlspecialchars($rowGroupe1['nom'])),25);
            $nom_groupe2 = tronquerTxt(addslashes(htmlspecialchars($rowGroupe2['nom'])),25);
            
            $match_id++;
            
            $query2 = "INSERT INTO tournoi_match VALUES($match_id,".$rowGroupe1['groupe_id'].",".$rowGroupe2['groupe_id'].",".$niveau.",-1,-1,0,'',$tournoi_id,'".$nom_groupe1."','".$nom_groupe2."')";
            if ( ! $dbHT->sql_query($query2) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
            }
            $match_id++;
        }
    }
    
    $queryTournoi = "UPDATE tournois SET niveau_actuel = $niveau, niveau_initial = $niveau WHERE tournoi_id = $tournoi_id";
    if ( ! $dbHT->sql_query($queryTournoi) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryTournoi);
    }
}

function matchsPool($pool) {

    global $dbHT;
    
    // on remet les scores à zéro
    $queryPool = "UPDATE groupes SET score_pool = 0 WHERE tournoi_pool = $pool AND inscrit_tournoi != -1";
    if ( ! $dbHT->sql_query($queryPool) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryPool);
    }
    
    // on fait tous les matchs possibles
    $lstGroupes = array();
    
    $query = "SELECT * FROM groupes WHERE tournoi_pool = ".$pool;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbGroupes = $dbHT->sql_numrows($result) ;
    
    while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
        array_push($lstGroupes,$rowGroupe);
    }
    
    // on lance tous les matchs
    $commentaire = "";
    for($g=0;$g<$nbGroupes-1;$g++) {
        
        for($g2=$g+1;$g2<$nbGroupes;$g2++) {
            
            $resultat = matchGroupesMusiques($lstGroupes[$g],$lstGroupes[$g2],$commentaire);
            if ($resultat)
                $lstGroupes[$g]['score_pool'] ++;
            else
                $lstGroupes[$g2]['score_pool'] ++;
        }
    }
    
    // update des scores
    for($g=0;$g<$nbGroupes-1;$g++) {
        $query = "UPDATE groupes SET score_pool = ".$lstGroupes[$g]['score_pool']." WHERE groupe_id = ".$lstGroupes[$g]['groupe_id'];
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
    }
}

function matchsTableau($tournoi) {

    global $dbHT, $dateActuelle,$lstNiveauxTournois;
    
    $query = "SELECT * FROM tournoi_match WHERE tournoi_id = ".$tournoi['tournoi_id']." AND niveau_dans_tournoi = ".$tournoi['niveau_actuel']." ORDER BY match_id ASC" ;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbMatchs = $dbHT->sql_numrows($result) ;
    
    while ($rowMatchs = $dbHT->sql_fetchrow($result) ){
        
        // pour chaque match, on récupère les infos sur les deux groupes
        $queryEquipe1 = "SELECT * FROM groupes WHERE groupe_id = ".$rowMatchs['equipe1']." LIMIT 1";
        if ( !($result1 = $dbHT->sql_query($queryEquipe1)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryEquipe1);
        }
        $rowEquipe1 = $dbHT->sql_fetchrow($result1);
        $dbHT->sql_freeresult($result1);
        
        $queryEquipe2 = "SELECT * FROM groupes WHERE groupe_id = ".$rowMatchs['equipe2']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($queryEquipe2)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryEquipe2);
        }
        $rowEquipe2 = $dbHT->sql_fetchrow($result2);
        $dbHT->sql_freeresult($result2);
        
        $commentaire = "";
        $resultat = matchGroupesMusiques($rowEquipe1,$rowEquipe2,$commentaire);
        $groupe_vainqueur = $rowEquipe1;
        $groupe_vaincu = $rowEquipe2;
        if (!$resultat){
            $groupe_vainqueur = $rowEquipe2;
            $groupe_vaincu = $rowEquipe1;
        }
        
        $queryResultat = "UPDATE tournoi_match SET vainqueur = ".$groupe_vainqueur['groupe_id'].", date = $dateActuelle WHERE match_id = ".$rowMatchs['match_id']." LIMIT 1";
        if ( ! $dbHT->sql_query($queryResultat)){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryResultat);
        }
        
        $messageVainqueur = T_("Bravo à vous tous ! Le groupe a gagné le match du ").$lstNiveauxTournois[$tournoi['niveau_actuel']]. " ! ".T_("Bonne continuation !");
        $messageVaincu = T_("Le groupe a perdu en ").$lstNiveauxTournois[$tournoi['niveau_actuel']]. "... ".T_("Il faut retenter la chance au prochain concours !");
        ajouterBulletin($groupe_vainqueur['groupe_id'],$groupe_vainqueur['leader_id'],"Leader",$messageVainqueur,true);
        ajouterBulletin($groupe_vaincu['groupe_id'],$groupe_vaincu['leader_id'],"Leader",$messageVaincu,true);
    }

    // on récupère l'indice max pour les matchs
    $queryMax = "SELECT MAX(match_id) as match_id FROM tournoi_match";
    $resultMax = $dbHT->sql_query($queryMax);
    if ( ! $resultMax ) {
        echo "Erreur SQL : ".$queryMax."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($resultMax);
    $match_id = $row[0] + 1;
    $dbHT->sql_freeresult($resultMax);
    
    // on rajoute les matchs pour le niveau suivant
    $query = "SELECT * FROM tournoi_match WHERE tournoi_id = ".$tournoi['tournoi_id']." AND niveau_dans_tournoi = ".$tournoi['niveau_actuel'];
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbMatchs = $dbHT->sql_numrows($result) ;
    
    while ($rowMatchs1 = $dbHT->sql_fetchrow($result) ){

        $niveau = $rowMatchs1['niveau_dans_tournoi']-1;
        
        // on récupère l'équipe vainqueur du 1er match
        $vainqueur1 = $rowMatchs1['nom_groupe1'];
        if ($rowMatchs1['equipe2'] == $rowMatchs1['vainqueur'])
            $vainqueur1 = $rowMatchs1['nom_groupe2'];
                
        // on récupère l'équipe vainqueur du 2ème match
        $rowMatchs2 = $dbHT->sql_fetchrow($result);
        $vainqueur2 = $rowMatchs2['nom_groupe1'];
        if ($rowMatchs2['equipe2'] == $rowMatchs2['vainqueur'])
            $vainqueur2 = $rowMatchs2['nom_groupe2'];
        
        $match_id++;
            
        $query2 = "INSERT INTO tournoi_match VALUES($match_id,".$rowMatchs1['vainqueur'].",".$rowMatchs2['vainqueur'].",".$niveau.",-1,-1,0,'',".$tournoi['tournoi_id'].",'".mysql_real_escape_string($vainqueur1)."','".mysql_real_escape_string($vainqueur2)."')";
        if ( ! $dbHT->sql_query($query2) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
        }
    }
    
    // on avance d'un niveau dans le tournoi
    $queryTournoi = "UPDATE tournois SET niveau_actuel = ".($tournoi['niveau_actuel']-1)." WHERE tournoi_id=".$tournoi['tournoi_id']." LIMIT 1";
    if ( ! $dbHT->sql_query($queryTournoi) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryTournoi);
    }
}


function matchGroupesMusiques($groupe1,$groupe2,$commentaire,$avecCommentaire = false){
    
    global $dbHT;
    
    // liste des musiciens de chaque groupe
    
    // groupe 1
    $query = "SELECT hamster_id, nom, note, sante FROM hamster WHERE groupe_id = ".$groupe1['groupe_id'];
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbMembres1 = $dbHT->sql_numrows($result) ;
    $lstMembres1 = array();
    
    while($rowMembre=$dbHT->sql_fetchrow($result)) {
        array_push($lstMembres1,$rowMembre);
    }
    
    $dbHT->sql_freeresult($result);
    
    // groupe 2
    $query = "SELECT hamster_id, nom, note, sante FROM hamster WHERE groupe_id = ".$groupe2['groupe_id']. " ORDER BY note DESC";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbMembres2 = $dbHT->sql_numrows($result) ;
    $lstMembres2 = array();
    
    while($rowMembre=$dbHT->sql_fetchrow($result)) {
        array_push($lstMembres2,$rowMembre);
    }
    $dbHT->sql_freeresult($result);
    
    $pointEquipe1 = 0;
    $pointEquipe2 = 0;
    $nbMembres1 --;
    $nbMembres2 --;
    
    for($m=0;$m<=max($nbMembres1,$nbMembres2);$m++) {
        
        if ($lstMembres1[min($m,$nbMembres1)]['note'] * $lstMembres1[min($m,$nbMembres1)]['sante'] < $lstMembres2[min($m,$nbMembres2)]['note'] * $lstMembres2[min($m,$nbMembres2)]['sante']){
            $pointEquipe2 ++;
            
            if ($avecCommentaire){
                $commentaire .= $lstMembres1[min($m,$nbMembres1)]['nom']." a moins bien joué que ".$lstMembres2[min($m,$nbMembres2)]['nom'].";";
            }
        }
        else{
            $pointEquipe1 ++;
            if ($avecCommentaire){
                $commentaire .= $lstMembres1[min($m,$nbMembres1)]['nom']." a mieux joué que ".$lstMembres2[min($m,$nbMembres2)]['nom'].";";
            }
        }
    }
    
    if ($groupe1['note'] > $groupe2['note'])
        $pointEquipe1++;
    else
        $pointEquipe2++;
        
    if ($groupe1['dernier_concert'] > $groupe2['dernier_concert'])
        $pointEquipe1++;
    else
        $pointEquipe2++;
        
    if ($groupe1['nb_membres'] > $groupe2['nb_membres'])
        $pointEquipe1++;
    else if ($groupe1['nb_membres'] < $groupe2['nb_membres'])
        $pointEquipe2++;
        
    if ($groupe1['image'] > $groupe2['image'])
        $pointEquipe1++;
    else if ($groupe1['image'] < $groupe2['image'])
        $pointEquipe2++;
    
    if ($pointEquipe1 > $pointEquipe2){
        if ($avecCommentaire){
            $commentaire .= "Le groupe ".$groupe1['nom']." bat le groupe ".$groupe2['nom']." avec $pointEquipe1 points contre $pointEquipe2";
        }
        return true;
    }
    else{
         if ($avecCommentaire){
            $commentaire .= "Le groupe ".$groupe2['nom']." bat le groupe ".$groupe1['nom']." avec $pointEquipe2 points contre $pointEquipe1";
        }
        return false;
    }
}

?>
