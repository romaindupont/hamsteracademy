<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo "Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

$query = "SELECT joueur_id, passwd FROM joueurs";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;

$lstJoueurs = array();
$lstMdP = array();

while($row=$dbHT->sql_fetchrow($result)) {
    array_push($lstJoueurs, $row);
}

$dbHT->sql_freeresult($result);

// on doit faire une MAJ de la base du forum => on ferme la base principale
$dbForum->sql_activate();

define('PUN_ROOT', './forum/');
require PUN_ROOT.'include/common.php';

//$query = "UPDATE pun_users SET password = ''";
//if (! $dbForum->sql_query($query)) {
//    echo "Connexion impossible avec le forum" ;
//}

//echo "mot de passe à zéro";

for($j=0;$j<$nbJoueurs;$j++) {
    $query = "UPDATE pun_users SET password = '".pun_hash($lstJoueurs[$j]['passwd'])."' WHERE id=".$lstJoueurs[$j]['joueur_id'];
    if (! $dbForum->sql_query($query)) {
        echo "Connexion impossible avec le forum" ;
    }
}

// on reouvre la base principale
$dbHT->sql_activate();

echo "nb de joueurs mis a jour : " . $nbJoueurs. "<br/>";

?>
