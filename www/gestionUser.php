<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

function ajouterJoueurDansBDD($pseudo, $passwd, $email, $sexeJoueur, $age, $langue, $pays, $etape_inscription) {
    
    global $dbHT, $dbForum, $dateActuelle, $dateActuelleMinuit, $nbPiecesInscription, $alimentsInscription, $quantiteCopeauxInscription;
    global $metierInscription, $niveauInscription, $objectifNiveauInscription;
    
    // on a besoin de pun_hash : on inclut forum/include/functions.php
    include "forum/include/functions.php";
    
    $dateDernierQuizz = 0;
    $note=0;
    $presentation = T_("Me présenter ici...");
    $liste_rouge = 0;
    $montrer_profil = 1;
    $messageNonLu = 0; // indique si le joueur a un message non lu
    $image = 0; // indique si le joueur a une image (0 => pas d'image)
    $inscritConcours = 0; // indique si le joueur est inscrit au concours actuel
    $a_vote = 0; // indique si le joueur a voté ou non
    $nb_parrainages = 0; // indique si le nombre de fois un joueur a été parrain (pour controle les abus)
    $email_active = false; // indique si l'adresse email du joueur est valide/actif
    $suspendu = 0; // indique si le joueur est suspendu
    $ville = "";
    $prevenir_nouveau_message = 1;
    $nbPass = 0;
        
    $dbHT->sql_activate();
    
    $query = "SELECT MAX(joueur_id) as joueur_id FROM joueurs";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $id = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    //$code_activation = md5(calculCodeActivation($id,$email)); // indique le code d'activation pour l'email du joueur 
    $image_bonus = 0;
    $legende_bonus = "";
    $lstObjetsTrouves = 0;
    $cheat_code = $nb_plaintes = 0;
    $last_ip = "";
    $blog = "";
    $newsletter = 1;
    $date_maj_note = 0;
    $paiement_recent = 0;
    $nbConnectionsDansLeMois = 0;
    $vip = 0;$noteProfil = 3;
    $naissance_jour = 1;$naissance_mois = 1;$naissance_annee=1940;
    $droits = 0;
    $nb_defis_gagnes = 0;
    $nb_defis_perdus = 0;
    $nb_defis_jour = 0;
        
    $query = "INSERT INTO joueurs VALUES ( ".$id." , '".$pseudo."' , '".$passwd."' , 
    '".$email."' , ".$sexeJoueur ." , ".$age." , '".$langue."', 
    ".$pays."  , '".$ville."' , ".$dateActuelle.",".$dateActuelle.",".$dateActuelle." , ".$etape_inscription.",
    ".$dateActuelleMinuit."  , ".$nbPiecesInscription.", ".$dateActuelle.", ".$alimentsInscription.",
    ".$quantiteCopeauxInscription.",".$metierInscription."  , ".$niveauInscription.",
    ".$objectifNiveauInscription.",".$dateDernierQuizz.",".$note.",'".$presentation."',
    ".$liste_rouge.",".$montrer_profil.",
    ".$messageNonLu.",".$image.",".$inscritConcours.",".$a_vote.",
    ".$nb_parrainages.",'".$email_active."','".$suspendu."',".$image_bonus.",
    '".$legende_bonus."',".$lstObjetsTrouves.", ".$cheat_code.", '".$last_ip."', '".$blog."',
    ".$nb_plaintes.",".$newsletter.",".$date_maj_note.",".$paiement_recent.",".$nbConnectionsDansLeMois.",".$vip.",
    ".$noteProfil.",".$naissance_jour.",".$naissance_mois.",".$naissance_annee.",".$droits.",".$prevenir_nouveau_message.",
    ".$nb_defis_gagnes.",".$nb_defis_perdus.",".$nb_defis_jour.",".$nbPass."
    ) ";
    $result = $dbHT->sql_query($query);
    
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
        return -1;
    }
    else {
        
        // on ajoute le joueur aussi dans punBB
        $dbForum->sql_activate();        

        $password_hash = pun_hash($passwd) ;
        $email_setting = 1;
        $save_pass = 1;
        $timezone = 1;
        $language = "French";
        if ($langue != "fr")
            $language = "English";
        $query = 'INSERT INTO pun_users (id, username, group_id, password, email, email_setting, save_pass, timezone, language, style, registered, registration_ip, last_visit) VALUES('.$id.',\''.$pseudo.'\', 4, \''.$password_hash.'\', \''.$email.'\', '.$email_setting.', '.$save_pass.', '.$timezone.' , \''.$language.'\', \'Oxygen\', '.$dateActuelle.', \''.get_remote_address().'\', '.$dateActuelle.')';
        if ( ! $dbForum->sql_query($query)) {
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
        
        $dbHT->sql_activate();
            
        return $id;
    }
}

function supprimerCompte($joueur_id, $verbose = false, $messageExplication = "") {
    
    global $dbHT, $dbForum ;
    
    $dbHT->sql_activate();
    
    // on récupère tout son profil avant de supprimer le joueur
    $sql = 'SELECT *
        FROM joueurs
        WHERE joueur_id = ' .$joueur_id;
    if (!($result = $dbHT->sql_query($sql)))
    {
        message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
    }
    $userToDelete = $dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);    
    
    if ($verbose)
        echo "<br>supprime le joueur id=".$userToDelete['joueur_id'].", pseudo=".$userToDelete['pseudo']."<br/>";
    
    // on supprime les images du joueurs si elles existent
    if ($userToDelete['image'] > 0) {
        if ($verbose)
            echo "supprime images/joueurs/".$userToDelete['joueur_id'].".jpg<br>/" ;
            
        unlink("images/joueurs/".$userToDelete['joueur_id'].".jpg");
    }
    if ($userToDelete['image_bonus'] > 0) {
        if ($verbose)
            echo "supprime images/joueurs/".$userToDelete['joueur_id']."_bonus.jpg" ;
            
        unlink("images/joueurs/".$userToDelete['joueur_id']."_bonus.jpg");
    }
    
    if ($verbose)
        echo "supprime accessoires, cages...<br/>" ;
    
    // suppression des accessoires
    $query = "DELETE FROM accessoires
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }    
    
    // suppression des cages
    $query = "DELETE FROM cage
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting cage ', '', __LINE__, __FILE__, $query);
    }
    
    // suppression des amis et des amis en attente
    $query = "DELETE FROM amis WHERE joueur_id=".$joueur_id. " OR ami_de = ".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }        
    $query = "DELETE FROM amis_attente WHERE demandeur_id=".$joueur_id. " OR sollicite_id = ".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }     
    
    // pour chaque hamster, 
    // 1) suppression du groupe s'il est leader
    // 2) suppression des interactions
    // 3) on prévient le compagnon s'il est marié
    // 4) on supprime les bulletins de mariage
    $query = "SELECT hamster_id FROM hamster WHERE joueur_id=".$userToDelete['joueur_id'];
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    while($rowHamster=$dbHT->sql_fetchrow($result)) {
        
        supprimerHamster($rowHamster['hamster_id']);
    }
    $dbHT->sql_freeresult($result);
    
    if ($verbose)
        echo "supprime les messages, parrainages, interactions, joueur, id dans le forum...<br/>" ;

    // suppression des messages (la poste)
    $query = "DELETE FROM messages
        WHERE exp_joueur_id=".$joueur_id." OR dest_joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        echo 'Error deleting messages : '.$query;
    }

    // suppression des parrainages
    $query = "DELETE FROM parrainage
        WHERE parrain_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        echo 'Error deleting messages : '.$query;
    }
    
    // suppression des comptes facebook
    $query = "DELETE FROM facebook
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        echo 'Error deleting facebook : '.$query;
    }
    
    // suppression des interactions
    $query = "DELETE FROM interactions
        WHERE demandeur_id=".$joueur_id. " OR receveur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        echo 'Error deleting interactions : '.$query;
    }    
    
    // suppression des combats
    $query = "DELETE FROM combats
        WHERE defieur_id=".$joueur_id. " OR defie_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        echo 'Error deleting combats : '.$query;
    }       
    
    // un mail à hamster academy pour indiquer qq infos sur le joueur qui se désinscrit
    if ($messageExplication != "") {
        $TO = "contact@hamsteracademy.fr";
        $subject = "Suppression de compte, user : ".$userToDelete['pseudo'].", niveau : ".$userToDelete['niveau'];
        if ($userToDelete['age'] > 3)
            $subject .= ", age : ".$userToDelete['age'];
        $subject = "(E) ".$subject;
        envoyerMail($TO,$subject,$messageExplication,"suppression");
    }
    
    // suppression du joueur
    $query = "DELETE FROM joueurs
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }    

    // suppression du joueur dans punBB (plus remise à 0 des posts dans le forum à "Guest")
    $dbForum->sql_activate();

    $query = "UPDATE pun_posts SET poster_id=1 WHERE poster_id=".$joueur_id;    
    $dbForum->sql_query($query);
        
    $query = "DELETE FROM pun_users WHERE id=".$joueur_id;    
    $dbForum->sql_query($query);
        
    // on reouvre la base principale
    $dbHT->sql_activate();
}

function reinitialiserCompte($joueur_id) {
    
    global $dbHT, $userdata, $nbPiecesInscription ;
    
    // suppression des accessoires
    $query = "DELETE FROM accessoires
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting accessoire ', '', __LINE__, __FILE__, $query);
    }    
    
    // suppression des cages
    $query = "DELETE FROM cage
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting cage ', '', __LINE__, __FILE__, $query);
    }
    
    // on récupère les infos de son premier hamster
    $query = "SELECT * FROM hamster WHERE joueur_id=".$userdata['joueur_id']. " LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $rowHamster = $dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    
    // suppression des hamsters
    $query = "DELETE FROM hamster
        WHERE joueur_id=".$joueur_id;    
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error deleting hamster ', '', __LINE__, __FILE__, $query);
    }    
    
    // on récupère tout son profil avant de reinitialiser le joueur
    $sql = 'UPDATE joueurs SET
        niveau = 1,
        nb_pieces = '.$nbPiecesInscription.',
        note = 0,
        metier = 0,
        niveau_objectif = 0,
        copeaux = 3,
        aliments = 5
        WHERE joueur_id = ' .$joueur_id;
    if (!($result = $dbHT->sql_query($sql)))
    {
        message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $sql);
    }

//    $TO = "contact@hamsteracademy.fr";
//    $h  = "From: contact@hamsteracademy.fr";
//    $subject = "Reinit du compte : ".$userdata['pseudo'] ;
//    $message = "";
//    $mail_sent = @mail($TO, $subject, $message, $h);
    
    $nouvelle_cage_id = ajouterCageDansBDD($joueur_id, 0,1, 0, 5, 0, 1,1, "Nouvelle cage") ;
    ajouterAccessoireDansBDD(1,$joueur_id, $nouvelle_cage_id,230,0,0,1);
    ajouterAccessoireDansBDD(16,$joueur_id, $nouvelle_cage_id,157,0,0,1);
    ajouterHamsterDansBDD($rowHamster['nom'], $rowHamster['sexe'], 0, 1, $rowHamster['type'], $nouvelle_cage_id, $joueur_id);
}

function preparerMailing() {
    
    global $dbHT;
    
    $queryErase = "TRUNCATE TABLE mailing";
    if ( ! $dbHT->sql_query($queryErase))
        echo "Pb de requete : $queryErase<br/>";
    
    $query = "SELECT joueur_id FROM joueurs WHERE email_active=1 AND newsletter=1";
    
    if ( ! $result = $dbHT->sql_query($query))
        echo "Pb de requete : $query<br/>";
        
    while ($row = $dbHT->sql_fetchrow($result)) {
        
        $queryInsert = "INSERT INTO mailing VALUES (".$row['joueur_id'].",1)";
        if ( ! $dbHT->sql_query($queryInsert))
            echo "Pb de requete : $queryInsert<br/>";
    }
    $dbHT->sql_freeresult($result);
}

// indique si une personne répond aux critères pour devenir VIP
function isVIP($joueur_id){

    global $dbHT, $dateActuelle, $vip_nbConnexionsMin, $vip_niveauMin, $vip_noteProfilMin, $vip_nbMessagesMin,$vip_nbParrainages;
    
    // crtières de vip  
    $query = "SELECT nbConnectionsDansLeMois, niveau, noteProfil, nb_parrainages FROM joueurs WHERE joueur_id = ".$joueur_id. " LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueur profil', '', __LINE__, __FILE__, $query);
    }
    $rowJoueur=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    
    // on compte le nbre de messages envoyés
    $query = "SELECT COUNT(message_id) as message_id FROM messages WHERE exp_joueur_id = ".$joueur_id." AND dest_joueur_id != ".$joueur_id." AND date > ".($dateActuelle-2678400)." LIMIT 1";
    $result = $dbHT->sql_query($query);
    $row = $dbHT->sql_fetchrow($result);
    $nbMessagesEnvoyes = $row[0];
    $dbHT->sql_freeresult($result);

    // tous les critères ici :
    if ($rowJoueur['nbConnectionsDansLeMois'] < $vip_nbConnexionsMin || $rowJoueur['niveau'] < $vip_niveauMin || $rowJoueur['noteProfil'] < $vip_noteProfilMin || $nbMessagesEnvoyes < $vip_nbMessagesMin || $rowJoueur['nb_parrainages'] < $vip_nbParrainages) {
        return false;
    }
    else
        return true;
}

function supprimerPass($joueur_id) {
    
    global $userdata, $dbHT;
    
    $query = "SELECT nb_pass FROM joueurs WHERE joueur_id = ".$joueur_id. " LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueur profil', '', __LINE__, __FILE__, $query);
    }
    $rowJoueur=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    $nouveauNbPass = max(0,$rowJoueur['nb_pass'] -1) ;
    $query = "UPDATE joueurs SET nb_pass = $nouveauNbPass WHERE joueur_id = ".$joueur_id;
    $dbHT->sql_query($query) ;
    
    if (isset($userdata) && sizeof($userdata) > 0)
        $userdata['nb_pass'] = $nouveauNbPass;
}

function ajouterPass($joueur_id, $nbPass = 1) {
    
    global $userdata, $dbHT;
    
    $query = "SELECT nb_pass FROM joueurs WHERE joueur_id = ".$joueur_id. " LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining joueur profil', '', __LINE__, __FILE__, $query);
    }
    $rowJoueur=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    $nouveauNbPass = $rowJoueur['nb_pass'] + $nbPass ;
    $query = "UPDATE joueurs SET nb_pass = $nouveauNbPass WHERE joueur_id = ".$joueur_id;
    $dbHT->sql_query($query) ;
    
    if (isset($userdata) && sizeof($userdata) > 0)
        $userdata['nb_pass'] = $nouveauNbPass;
}
?>