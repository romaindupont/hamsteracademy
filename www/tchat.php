<?php 

define('IN_HT', true);

require "common.php";

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hamster Academy - Café des Hamsters</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Hamster Academy, Eleve un hamster, Fabrique sa cage, Et vit à sa place, Jeu de role, jeu gratuit ">
<meta name="keywords" content="Hamster, Academy, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="icon" type="image/ico" href="/favicon.ico" />
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/styleEleveur.css" type="text/css">
<link rel="stylesheet" href="css/styleIndex.css" type="text/css">
<link rel="stylesheet" href="forum/chat/css/shoutbox.css" type="text/css">
</head>

<body>

<div class="mainPage">
<div class="logoMain">
    <div class="logoImage">    
        <img src="images/logoHamsterAcademy.gif" alt="Hamster Academy" />
    </div>
</div>

<div style="width:850px; margin-left: auto; margin-right: auto;">
    <div align=center>
        <h2>Au Café des Hamsters</h2>
        <br>
        
<?php

$avertissement = 0;
if (isset($_GET['avertissement']))
    $avertissement = $_GET['avertissement'];

if ($avertissement == 0) {

?>
    
    <img src="images/cafe.gif" alt="Café des Hamsters"><br/>&nbsp;<br/>

    </div>
    <div style="width:650px; margin-left: auto; margin-right: auto;">
    Attention, pour discuter, il faut <strong>respecter</strong> ces règles : 
    <ul>
        <li>ne <strong>JAMAIS</strong> donner son vrai nom, ni son adresse, ni son école</li>
        <li>ne jamais insulter ou tenir des propos grossiers : une suspension de compte est immédiate</li>
    </ul>
    Rappels :
    <ul>
        <li>sur Internet, <strong>n'importe QUI peut MENTIR</strong>, notamment sur son age ou ses intentions : il faut donc toujours se <strong>méfier</strong>...</li>
        <li>il est déconseillé de donner son adresse MSN ou son adresse mail aux autres joueurs</li>
        <li>ne jamais donner son mot de passe (et Hamster Academy le connaît déjà)</li>
        <li>en cas de problème, d'insultes ou de questions anormales, contacte immédiatement Hamster Academy (un lien sous le tchat permet de le faire)</li>
    </ul>
    </div>
    <br/>&nbsp;<br/>
    <div class=cadreArrondi style="width:450px;">
        <div class="hautdroit"></div><div class="hautgauche"></div>
        <div class="contenu">        
            <a href="forum/chat/index.php"><strong>Ok, j'ai bien lu les avertissements ! Go pour le tchat ! :-)</strong></a>
        </div>
        <div class="basdroit"></div><div class="basgauche"></div>
    </div>
    <br/>&nbsp;<br/>
    </div>

<?php
}
else {    

?>
    </div>
    
    <ul>    
        <li>Rappel : tout propos grossier ou insultant entraînera la suspension immédiate du joueur.</li>
    </ul>
    
    <div align=center>

       <br/>

    Un joueur insulte ou dit des choses choquantes : <a href="alerter.php?alerte_lieu=tchat" target=hblank >Prévenir Hamster Academy !</a>
    
    <br/>&nbsp;<br/>
    
    </div> <!-- fin du mainPageUnderMenu -->

<?php
}

?>

<?php require "footer.php" ; ?>

</body>
</html>

