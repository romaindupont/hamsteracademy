<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

?>

<form method="post" onsubmit="javascript: return (verifPseudo());" action="<?php 

    echo "jeu.php?mode=m_achat&amp;rayon=".RAYON_HAMSTER."&amp;achatHamster=1\" > \n";

    echo "<div align=\"center\"><strong>".T_("Choisis parmi ces hamsters celui que tu veux élever :")."</strong><br/>&nbsp;<br/>\n</div>";
          
    // liste des hamsters achetables
    $lstHamsters = array();
    $niveauMin = 0;
    for($hamsterIndex=0; $hamsterIndex < $nbInfosHamsters; $hamsterIndex++) {

        if ($hamsterIndex == 7)
            continue; // on enlève le hamster grunge
        
        if ($hamsterIndex < 7  || $userdata['niveau'] > NIVEAU_MARIAGE) {
            
            if ($hamsterIndex < 12  || $userdata['niveau'] > NIVEAU_EVASION)
                $accessible = true;
            else
                $accessible = false;
        }
        else
            $accessible = false;
        
        if ($hamsterIndex >= 7 && $hamsterIndex < 12)
            $niveauMin = NIVEAU_MARIAGE+1;
        else if ($hamsterIndex >= 12)
            $niveauMin = NIVEAU_EVASION+1;
            
        $hamster = array(
            "accessible" => $accessible,
            "image" => $infosHamsters[$hamsterIndex][1],
            "nom" => $infosHamsters[$hamsterIndex][0],
            "yeux" => $infosHamsters[$hamsterIndex][2],
            "pelage" => $infosHamsters[$hamsterIndex][3],
            "description" => $infosHamsters[$hamsterIndex][4],
            "already_checked" => ( $typeHamster == $hamsterIndex ),
            "type" => $hamsterIndex,
            "niveau_min" => $niveauMin
        );
        array_push($lstHamsters,$hamster);
    }

    $smarty->assign('lstHamsters', $lstHamsters);

    $msgErreur = "";
    if ($erreurCaracteres > 0) {
        $msgErreur = "<font color=\"#AA0000\">".T_("Attention, choisis <strong>deux</strong> caractéristiques maximum pour l'hamster (ex : puissant et dragueur)")."</font><br/>&nbsp;<br/>\n";
    }
    $smarty->assign('msgErreur', $msgErreur);
    $smarty->assign('prefHamster_1', $prefHamster_1);
    $smarty->assign('prefHamster_2', $prefHamster_2);
    $smarty->assign('prenomHamster', $prenomHamster);
    $smarty->assign('sexeHamster', $sexeHamster);
    $smarty->display('formulaireNouvelHamster.tpl');
     
    echo "<div align=\"center\"><br/>".T_("Le mettre dans la cage :");
    echo "<select name=\"cageHamster\">";

        for($c=0;$c<$nbCages;$c++) {
             $cage = $lst_cages[$c];
            $cage_ind = $c+1;
            echo "<option value=\"".$cage['cage_id']."\" >cage n°".$cage_ind." : ".$cage['nom']."</option>\n";
        }
    echo "</select></div>";
    echo "<br/>&nbsp;<br/>";
    echo "<div align=\"center\"><input type=\"submit\" name=\"acquerirHamster\" value=\"".T_("Acquérir maintenant le hamster !")."\" /></div>";

?>
</form>
