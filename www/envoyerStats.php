<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";

error_reporting( E_ALL );

$depuis24H = $dateActuelle-3600*24;

// nb d'inscrits fr
$query = "SELECT joueur_id FROM joueurs WHERE langue='fr' AND date_inscription > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbInscritsFr = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb d'inscrits en
$query = "SELECT joueur_id FROM joueurs WHERE langue='en' AND date_inscription > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbInscritsEn = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de joueurs venus
$query = "SELECT joueur_id FROM joueurs WHERE user_lastvisit > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbVenus = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements tel/sms
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND (action = 'paiement_nid' OR action = 'paiement_cabane' OR action = 'paiement_grossesse' OR action = 'paiement_tel')";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsTelSms = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements CB5
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND action = 'paiement_CB5'";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsCB5 = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements CB10
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND action = 'paiement_CB10'";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsCB10 = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements CB30
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND action = 'paiement_CB30'";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsCB30 = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements missions
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND action = 'paiement_mission'";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsMissions = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// nb de paiements vip
$query = "SELECT joueur_id FROM stats WHERE date > ".$depuis24H." AND action = 'paiement_vip'";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPaiementsVIP = $dbHT->sql_numrows($result) ;
$dbHT->sql_freeresult($result);

// messages reçus par hamster academy
$query = "SELECT * FROM messages WHERE dest_joueur_id=".HAMSTER_ACADEMY_ID." AND date > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbMessages = $dbHT->sql_numrows($result) ;
$lst_messages="";
while($row=$dbHT->sql_fetchrow($result)) {
    $lst_messages .= "    - De ".$row['exp_joueur_id']." : ".stripslashes(html_entity_decode(strip_tags($row['message'])))."\n";
}
$dbHT->sql_freeresult($result);

// idées reçues par hamster academy
$query = "SELECT * FROM idees WHERE date > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbIdees = $dbHT->sql_numrows($result) ;
$lst_idees="";
while($row=$dbHT->sql_fetchrow($result)) {
    $lst_idees .= "    - De ".$row['pseudo']." : ".stripslashes(html_entity_decode(strip_tags($row['idee'])))."\n";
}
$dbHT->sql_freeresult($result);

// alertes reçues par hamster academy
$query = "SELECT * FROM alertes WHERE date > ".$depuis24H;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbAlertes = $dbHT->sql_numrows($result) ;
$lst_alertes="";
while($row=$dbHT->sql_fetchrow($result)) {
    $lst_alertes .= $row['denonciateur']."=>".$row['denonce']." ";
}
$dbHT->sql_freeresult($result);

// liste d'exclusion
$query = "SELECT pseudo, suspendu FROM joueurs WHERE suspendu > 0 ORDER BY user_lastvisit DESC";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbSuspendus = $dbHT->sql_numrows($result) ;
$lst_suspendusTmp="";
$lst_suspendusInd="";
while($row=$dbHT->sql_fetchrow($result)) {
    if ($row['suspendu'] == 1)
        $lst_suspendusInd .= $row['pseudo'].",";
    else if ($row['suspendu'] > $dateActuelle)
        $lst_suspendusTmp .= $row['pseudo']."(".round(($row['suspendu'] - $dateActuelle)/(3600*24))." j),";
    
}
$dbHT->sql_freeresult($result);

// liste de joueurs avec trop de plaintes
$query = "SELECT pseudo, nb_plaintes, suspendu FROM joueurs WHERE nb_plaintes >= 3 ORDER BY user_lastvisit DESC";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbPlaintes = $dbHT->sql_numrows($result) ;
$lst_plaintes="";
while($row=$dbHT->sql_fetchrow($result)) {
    $lst_plaintes .= $row['pseudo']." (nb plaintes=".$row['nb_plaintes'].")";
    if ($row['suspendu'] == 1)
        $lst_plaintes .= "(suspendu à durée indeterminée)";
    else if ($row['suspendu'] > $dateActuelle)
        $lst_plaintes .= "(suspendu pour ".round(($row['suspendu'] - $dateActuelle)/(3600*24))." j)";
    $lst_plaintes .= "\n";
}
$dbHT->sql_freeresult($result);

// todo list
$query = "SELECT * FROM todo ORDER BY status";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires', '', __LINE__, __FILE__, $query);
        }
$lst_todo="";
while($row=$dbHT->sql_fetchrow($result)) {
    $lst_todo .= "- ".$row['texte'];
    if ($row['status'] == 0)
        $lst_todo .= " => à faire";
    else
        $lst_todo .= " => effectué";
    if ($row['date_limite'] > 0) {
        $dateStr = date("d/m/Y",$row['date_limite']) ;
        $lst_todo .= " (avant le ".$dateStr.")";
    }
    $lst_todo .= "\n";
}
$dbHT->sql_freeresult($result);


// mail bilan pour les admins
$TO = "dupont.romain@gmail.com, greg_dt@hotmail.com";
$subject = "Hamster Academy : Stats du jour" ;
$message = "Stats du ".date('l dS \of F Y h:i:s A')."\n\n";
$message .= "Depuis 24 heures : \n";
$message .= "Nb de joueurs inscrits (fr) : ".$nbInscritsFr."\n";
$message .= "Nb de joueurs inscrits (en) : ".$nbInscritsEn."\n";
$message .= "Nb de joueurs connectés : ".$nbVenus."\n\n";
$message .= "Nb de paiements tel/sms : ".$nbPaiementsTelSms."\n";
$message .= "Nb de paiements missions : ".$nbPaiementsMissions."\n";
$message .= "Nb de paiements vip : ".$nbPaiementsVIP."\n";
$message .= "Nb de paiements CB5 : ".$nbPaiementsCB5."\n";
$message .= "Nb de paiements CB10 : ".$nbPaiementsCB10."\n";
$message .= "Nb de paiements CB30 : ".$nbPaiementsCB30."\n\n";
$message .= "Nb de messages reçus : ".$nbMessages."\n".$lst_messages."\n";
$message .= "Nb d'idées reçues : ".$nbIdees."\n".$lst_idees."\n";
$message .= "Nb d'alertes reçues : ".$nbAlertes."\n".$lst_alertes."\n\n";
$message .= "Suspendus durée limitée : ".$lst_suspendusTmp."\n";
$message .= "Suspendus durée indéterminée : ".$lst_suspendusInd."\n\n";
$message .= "Joueurs avec au moins 5 plaintes : ".$nbPlaintes."\n".$lst_plaintes."\n\n";
$message .= "Rappel todo list : \n-----------------\n".$lst_todo."\n";
$message .= "\n--Fin";

$mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);

//echo $message;

?>
