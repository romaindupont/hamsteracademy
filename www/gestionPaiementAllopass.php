<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$message_erreur = "<font color=\"#AA0000\">".T_("Le code saisi est invalide.</font> Vérifie-le et réessaye. Sinon, contacte ")."<a href=\"mailto:".$email_contact."\">Hamster Academy</a>.";

if (isset($_GET['err'])) {
    
    $erreur = intval($_GET['err']) ;
}
else if (isset($_GET['paiement']) || isset($_GET['RECALL']) || isset($_GET['paiement_cb']) || isset($_GET['paiement_cb_3000']) || isset($_GET['paiement_cb_10000']) ) {
    
    // le joueur a payé, on vérifie son code
    $RECALL = mysql_real_escape_string($_GET['RECALL']);
    
    if( trim($RECALL) == "" )
  {
    // La variable RECALL est vide, renvoi de l'internaute
    // vers une page d'erreur
    $erreur = 2; 
  }
  else {
      // $RECALL contient le code d'accès
      $RECALL = urlencode( $RECALL );
    
      // $AUTH doit contenir l'identifiant de VOTRE document
      $AUTH = "";
      if (isset($_GET['paiement']))
          $AUTH = urlencode( $site_id."/".$doc_id."/".$adv_id );
        else if (isset($_GET['paiement_cb']))
            $AUTH = urlencode( "xxx" );
        else if (isset($_GET['paiement_cb_3000']))
            $AUTH = urlencode( "xxx" );
        else if (isset($_GET['paiement_cb_10000']))
            $AUTH = urlencode( "xxx" );
            
      /**
       * envoi de la requête vers le serveur AlloPAss
       * dans la variable $r[0] on aura la réponse du serveur
       * dans la variable $r[1] on aura le code du pays d'appel de l'internaute
       * (FR,BE,UK,DE,CH,CA,LU,IT,ES,AT,...)
       * Dans le cas du multicode, on aura également $r[2],$r[3] etc...
       * contenant à chaque fois le résultat et le code pays.
       */
    
      $r = @file( "http://www.allopass.com/check/vf.php4?CODE=$RECALL&AUTH=$AUTH" );
    
      // on teste la réponse du serveur
    
      if( substr( $r[0],0,2 ) != "OK" ) 
      {
        // Le serveur a répondu ERR ou NOK : l'accès est donc refusé
        $erreur = 3 ;
      }
    }
    
    if ($erreur == -1) {
        
        // le joueur a payé, on agit selon le contexte : 
        
        // paiement par téléphone ou sms
        if (isset($_GET['paiement'])) {
            
            if ($pageTxt == "nid")
                $paiement = PAIEMENT_NID;
            else if ($pageTxt == "cab")
                $paiement = PAIEMENT_CABANE;
            else if ($pageTxt == "gro")
                $paiement = PAIEMENT_GROSSESSE;
            else if ($pageTxt == "niv")                
                $paiement = PAIEMENT_NIVEAU;
            else if ($pageTxt == "vip")
                $paiement = PAIEMENT_VIP;
            else
                $paiement = PAIEMENT_300_PIECES; 
        }
        
        // paiement par CB 5 euros
        else if (isset($_GET['paiement_cb']))
            $paiement = PAIEMENT_1000_PIECES;

        // paiement par CB 10 euros
        else if (isset($_GET['paiement_cb_3000']))
            $paiement = PAIEMENT_3000_PIECES;

        // paiement par CB 30 euros
        else if (isset($_GET['paiement_cb_10000']))
            $paiement = PAIEMENT_10000_PIECES;      
    }
}
?>
