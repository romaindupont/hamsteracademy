<?php
define('IN_HT', true);

    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="refresh" content="3; url=jeu.php" /><title>Patientez svp ...</title></head><body>';
    echo "<div align=center>Le jeu ne peut afficher aujourd'hui les options de compte, d'amis et de VIP. Ce sera rétablit dans la soirée.</div>" ;
    echo '</body></html>';
        
    exit;


include "common.php";
include "gestion.php";
include "lstMessages.php";

$userdata = session_pagestart($user_ip);
	
if( ! $userdata['session_logged_in'] ) { 
	echo "Erreur de connection";
	redirectHT("index.php?err=1",3);
}

$pagePrecUrl = "jeu.php"; // par défaut

$erreur = 0;
$nomFilleul = "";
$emailFilleul = "";
$motFilleulInit = "Salut [nom_du_filleul_a_changer] !\n
Viens me rejoindre sur Hamster Academy ! Fais comme moi, achète un hamster, une cage, des accessoires sympas puis participe à l'Academy des Hamsters où tes hamsters jouent le rôle de star !\n
Inscris-toi vite au jeu, c'est gratuit ! \n(il faut cliquer sur le lien suivant pour que je devienne ton parrain : http://www.hamsteracademy.fr/inscription.php?parrain=".$userdata['joueur_id'].") \n
Si tu veux voir, le lien est http://www.hamsteracademy.fr \n
Je t'attends sur Hamster Academy ! \n".$userdata['pseudo'];

$motFilleul = $motFilleulInit;

// si une demande a été envoyée 
if (isset($_POST['nomFilleul'])) {
	
	$nomFilleul = stripslashes($_POST['nomFilleul']);
	$emailFilleul = stripslashes($_POST['emailFilleul']);
	$motFilleul = stripslashes($_POST['motFilleul']);

	$motFilleul = str_replace("[nom_du_filleul_a_changer]",$nomFilleul,$motFilleul);
	
	if ($emailFilleul == $userdata['email'])
		$erreur = 1;
	else if ($emailFilleul == ""){
		$erreur = 3;
	}
	else if ( ! $userdata['email_active']){
		$erreur = 4;
	}
	else if ( ! validate_mail_address ($emailFilleul) ){
		$erreur = 5;
	}
	else {
		$subject = "Invitation à jouer au jeu Hamster Academy" ;
		$motFilleul = str_replace("[email_du_filleul]",$emailFilleul,$motFilleul);
		
		$erreur = -1;
		$query = "INSERT INTO parrainage VALUES (".$userdata['joueur_id'].",'".$userdata['pseudo']."','".mysql_real_escape_string($emailFilleul)."' , 
			'".mysql_real_escape_string($nomFilleul)."' , ".$dateActuelle.", 0	) ";
		if ( ! ($dbHT->sql_query($query)) ) {
			echo "Erreur SQL : ".$query."<br>" ;
		}
		redirectMailHT($emailFilleul,$subject,$motFilleul);
	}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hamster Academy - Parrainage</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Hamster Academy, Eleve un hamster, Fabrique sa cage, Et vit à sa place, Jeu de role, jeu gratuit ">
<meta name="keywords" content="Hamster, Academy, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/styleEleveur.css" type="text/css">
</head>

<script type="text/javascript" src="tooltip.js"></script>

<body>

<div id="tooltip"></div>

<div align=center>
	
	<h1>Hamster Academy - Parrainage</h1>
	
	<div style="width:600px ; text-align:left;">
		
	<div align=center><img src="images/parrainage.gif" alt="Parrainage"></div>
	
	<?php 
	if ($erreur == -1) {
		echo "<div><br/>Le mail de parrainage a été envoyé avec succès à ".$nomFilleul." (email : ".$emailFilleul.")&nbsp;!</div>";
		$nomFilleul = "";
		$emailFilleul = "";
		$motFilleul = $motFilleulInit;
	}
	else {
		if ($erreur > 0) {
			echo "<br><div class=txtErreur>";
			if ($erreur == 2)
				echo "Il y a eu un problème avec l'envoi du mail de parrainage. Es-tu sûr d'avoir saisi une adresse mail correcte ? Sinon, contacte l'Hamster Academy.";
			else if ($erreur == 4)
				echo "Il faut d'abord que tu actives ton adresse email pour que ça marche ! Pour cela, va dans <a href=\"options.php\">Options</a> (à côté de l'aide) puis fait une demande d'activation.";
			else if ($erreur == 5 || $erreur == 3)
				echo "L'adresse email de ton filleul ne semble pas valide : peux-tu la vérifier ?";
			echo "</div><br>&nbsp;<br/>";
		}
		else {
		?>
	<h2>Principe</h2>
	Tu peux <span <?php echo tooltip("<u>Parrainer</u> = <u>Inviter un ami à jouer au jeu</u>. Cet ami devient ton filleul et tu deviens son parrain."); ?> >parrainer <span style="font-size:small;">(?)</span></span> un ou plusieurs amis : il suffit de les inviter à jouer à Hamster Academy. 
	<br/>&nbsp;<br/>
	En échange, l'Hamster Academy t'offre 20 <?php echo IMG_PIECE ; ?> par nouveau joueur inscrit grâce à toi.	<br><u>Attention, pour
que ça marche, il faut respecter les règles suivantes</u> : 
	<ul>
		<li>le nouveau joueur (ton filleul) devra utiliser le lien d'inscription donné dans le mail qu'il recevra</li>
		<li>il est interdit de se parrainer soi-même ! Tout compte bidon est détecté. Si un abus est détecté, la suspension de compte est immédiate.</li>
		<li>il faut que ton adresse email soit valide et surtout <u>activée</u> ! Pour "activer" ton adresse email, il faut aller dans le menu <a href="options.php">Options</a>.
	</ul>
	
	Tu peux le faire autant de fois que tu le souhaites, c'est gratuit !
	
	<br/>&nbsp;<br/>
	
	<?php 
		}
	}
	?>
	
	<h2>Qui veux-tu parrainer <?php if ($erreur == -1) echo " d'autres "; ?> ? </h2>
	<br/>
	<?php 
	if ($userdata['email'] == "") {
		echo "Il faut d'abord que tu aies une adresse mail valide pour parrainer. Pour cela, il faut aller dans \"<a href=\"options.php\">Options</a>\" pour en saisir une puis reviens ici !";
	}
	else {
	?>
		<form method=post action="parrainage.php">
			Nom de ton ami (futur filleul) : <input type=text size=25 maxlength=100 name=nomFilleul value="<?php echo $nomFilleul ; ?>"> <br/>
			Son email : <input type=text size=70 maxlength=100 name=emailFilleul value="<?php echo $emailFilleul ; ?>"><br/>
			<?php if ($erreur == 1) {
				echo "<div class=txtErreur>Il ne faut pas donner son propre email, mais celui de ton ami.</div>";
			}
			else if  ($erreur == 3) {
				echo "<div class=txtErreur>Il faut saisir l'adresse email de ton ami (futur filleul).</div>";
			}
			?><br/>
			Voici le mot qui sera envoyé à ton futur filleul <br/>(que tu peux modifier, sauf le lien d'inscription) : 
			<textarea name=motFilleul cols=70 rows=8><?php echo $motFilleul ; ?></textarea>
			
			 <br/>&nbsp;<br/>
			<div align=center><input type=submit value="Envoyer l'invitation !"></div>
			
		</form>
	<?php } ?>
	</div>
	
<br/>&nbsp;<br/>
<a href="<?php echo $pagePrecUrl ; ?>">Revenir au jeu</a>

<br/>&nbsp;<br/>

<div align=center>&copy; Hamster Academy</div>

</div> 
</div> 

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2189291-1";
urchinTracker();
</script>

</body>
</html>

<?php $dbHT->sql_close(); ?>
