<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

define('INSTRUMENT_REF_OBJET',0) ;
define('INSTRUMENT_POINTS',1) ;
define('INSTRUMENT_SPECIALITE',2) ;

// Rappel : chaque instrument doit être défini ici ET dans la liste des accessoires (en base de données)

$lstInstruments = array(
0 => array(45,5,'Guitariste'),
1 => array(46,10,'Violoniste'),
2 => array(47,3,'Percussionniste'),
3 => array(48,7,'Batterie'),
4 => array(52,12,'Pianiste'),
5 => array(53,8,'Contrebassiste'),
6 => array(58,8,'Saxophoniste'),
7 => array(67,5,'Chanteur/chanteuse'),
8 => array(68,8,'Tromboniste'),
9 => array(69,8,'Flutiste'),
10 => array(70,8,'Bassiste'),
11 => array(71,8,'Guitariste'),
12 => array(72,8,'Guitariste Folk'),
13 => array(73,8,'Cornemusiste')
);

$nbInstruments = sizeof($lstInstruments);


define ('INST_BATTERIE',3) ;
define ('INST_DJEMBE',2) ;
define ('INST_GUITARE',0) ;
define ('INST_GUITARE2',11) ;
define ('INST_GUITARE_FOLK',12) ;
define ('INST_CONTREBASSE',5) ;
define ('INST_BASSE',10) ;
define ('INST_CHANT',7) ;
define ('INST_SAXO',6) ;
define ('INST_TROMBONE',8) ;
?>
