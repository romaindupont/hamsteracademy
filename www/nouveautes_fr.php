<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$dernieresNouveautes = '
<div class="newsBloc"><div class="newsDate">Lundi 27 décembre</div>
<div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=10517" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°21 ! Spécial neige!</a></div>
<div class="newsTxt">- Nouvelle mission 3 : guéris ton hamster</div>
</div>
<div class="newsBloc">
<div class="newsDate">Vendredi 24 décembre : </div>
<div class="newsTxt">- <img src="images/FaceBook_48x48.png" width="20" alt="Facebook" align="middle" /> Publie ta cage sur Facebook !</div>
</div>
<div class="newsBloc">
<div class="newsDate">Lundi 20 décembre : </div>
<div class="newsTxt">- Spécial hiver enneigé : nouvelle peinture pour la cage !</div>
</div>
<div class="newsBloc"><div class="newsDate">Mercredi 15 décembre : </div>
<div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de Noël sont arrivés ! Va vite les découvrir dans la boutique !</div>
<div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de Noël à mettre au pied du sapin sont arrivés !</div>
</div>
<div class="newsBloc"><div class="newsDate">Mardi 7 décembre</div>
<div class="newsTxt">- <img src="images/halteres_reduc.png" width="40" alt="Concours" align="middle" /> Les résultats du concours sont arrivés ! Toutes les infos en Ville->Concours !</div>
</div>
<div class="newsBloc"><div class="newsDate">Lundi 6 décembre : </div>
<div class="newsTxt">- <img src="images/eolienne_arret.gif" style="vertical-align:middle;height:70px;" alt="Eolienne d\'énergie" /> Nouvel objet pour la cage : une éolienne qui capte l\'énergie de l\'air... Au rayon Sport !</div>
</div>    
<div class="newsBloc"><div class="newsDate">Vendredi 3 décembre : </div>
<div class="newsTxt">- <img src="images/calendrier_avent_red.jpg" style="vertical-align:middle;width:50px;" alt="Calendrier de l\'Avent" /> Le calendrier de l\'Avent est là ! A découvrir en boutique !</div>
</div>    
<div class="newsBloc"><div class="newsDate">Jeudi 2 décembre</div>
<div class="newsTxt">- <img src="images/flocon.gif" width="40" alt="Neige" align="middle" /> La neige débarque sur Hamster Academy ! Nouvel objet spécial hiver pour la cage !</div>
</div>
';

if (defined ('PLUS_DE_NOUVEAUTES')) {

    $dernieresNouveautes .= '
    <div class="newsBloc"><div class="newsDate">Dimanche 28 novembre</div>
    <div class="newsTxt">- <img src="images/chomik18.jpg" width="40" alt="Hamster Craintif" align="middle" /> Nouvel hamster disponible en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 27 novembre</div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="20" alt="Concours" align="middle" /> Concours du joueur qui progresse le plus vite ! Toutes les infos en Ville->Concours !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mardi 17 novembre</div>
    <div class="newsTxt">- <img src="images/halteres_reduc.png" width="40" alt="Concours" align="middle" /> Les résultats du concours sont arrivés ! Toutes les infos en Ville->Concours !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 15 novembre</div>
    <div class="newsTxt">- <img src="images/amulette.png" width="40" alt="Concours" align="middle" /> Nouvel objet : l\'Amulette de Force ! Au rayon Sport.</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 31 octobre</div>
    <div class="newsTxt">- <img src="images/halteres_reduc.png" width="40" alt="Concours" align="middle" /> Concours du hamster le plus fort ! Toutes les infos en Ville->Concours !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 8 octobre</div>
    <div class="newsTxt">- Nouveaux objets pour l\'automne : porte-bûches <img src="images/porte_buches2.png" width="30" alt="porte buches" /> <img src="images/porte_buches3.png" width="30" alt="porte buches" /> </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 27 septembre</div>
    <div class="newsTxt">- Nouvelle mission 7 et 16 : guéris ton hamster</div>
    <div class="newsTxt">- Nouveaux objets pour l\'automne : cheminée et accessoires <img src="images/cheminee_newage.png" width="15" alt="cheminée" /> <img src="images/ustensiles_cheminee.png" width="10" alt="ustensiles de cheminée" /> </div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=10207" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°18 ! Spécial rentrée !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 24 septembre</div>
    <div class="newsTxt">- Mission 8 divisée en deux missions, mission 8 et 9 (pour que ce soit plus facile)</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 2 juin</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=9919" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°15 ! A découvrir d\'urgence !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 16 mai</div>
    <div class="newsTxt">- Défis entre joueurs ! Es-tu meilleur que les autres joueurs de HA ? :-) (accessible au niveau 8)</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 15 mai</div>
    <div class="newsTxt">- <img src="images/chinoix_lampe_rouge_big.png" width="40" alt="" align="middle" /> Plein de nouvels objets accessibles au niveau 13 !</div>
    <div class="newsTxt">- Nouvelle mission n°13 à découvrir !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mardi 4 mai</div>
    <div class="newsTxt">- <img src="images/muguet.png" width="40" alt="Muguet" align="middle" /> Muguet de mai, à mettre dans la cage ! Accessible au niveau 6 !</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=9787" title="la Gazette des Modérateurs" > Découvrez la Gazette de Hamster Academy, n°14</a> ! En mai fait ce qu\'il te plait ! Préparée par les modérateurs et joueurs de HA !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 30 avril</div>
    <div class="newsTxt">- <img src="images/oeufs/oeufs1.gif" width="35" alt="Oeufs de Pâques" align="middle" /> Dernier jour pour trouver les oeufs de Pâques !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 29 avril</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=9601" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°13, un an déjà ! A découvrir d\'urgence !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 21 avril</div>
    <div class="newsTxt">- <img src="images/FaceBook_48x48.png" width="30" alt="Facebook" align="middle" /> - <a href="http://www.facebook.com/hamsteracademy" title="Deviens Fan !" >Deviens fan de HA sur Facebook !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mardi 20 avril</div>
    <div class="newsTxt">- <img src="images/masque_catch" width="30" alt="catch américain" align="middle" /> - Nouveau sport, le catch américain!</div>
    <div class="newsTxt">- <img src="images/oeufs/oeufs1.gif" width="35" alt="Oeufs de Pâques" align="middle" /> Trouve les oeufs de Pâques !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 10 avril</div>
    <div class="newsTxt">- <img src="images/caisse_bois.png" width="40" alt="vendre noix de coco" align="middle" /> - Possibilité de vendre ses noix de cocos au vétérinaire. Il faut aller dans l\'inventaire une fois que la caisse a été achetée en boutique.</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 4 avril</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Groupe" align="middle" /> - Manager disponible pour les groupes de musique</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 2 avril</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Groupe" align="middle" /> - Nouvelle présentation pour les groupes de musique</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 28 mars</div>
    <div class="newsTxt">- <img src="images/ballon_foot.gif" width="40" alt="Foot" align="middle" /> - Le foot débarque sur HA ! Forme une équipe jusqu\'à 11 joueurs (+ 3 remplaçants) !</div>
    <div class="newsTxt">- Nouvelle régle pour les groupes de musique : si un leader est absent trop longtemps, il le reste mais en attendant son retour, tous les membres du groupe ont les mêmes pouvoirs que le leader.</div>
    <div class="newsTxt">- Les groupes ont désormais un niveau : l\'expérience a dû être remise à zéro (pas les points, ouf!)</div>
    <div class="newsTxt">- Pour les modos : possibilité de bannir une IP pour éviter le multi-compte</div>
    <div class="newsTxt">- Le catch américain débarque sur HA ! (actif dans la soirée)</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Le fichier  <a href="changelog.txt">changelog.txt (nouveautés et bugs corrigés)</a> </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 1er février</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=9190" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°11 ! A découvrir !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 9 Janvier</div>
    <div class="newsTxt">- <img src="images/part_galette.gif" alt="galette des rois" width="30" /> La galette des rois est arrivée ! Cours vite l\'acheter en Boutique (en Ville) pour tes hamsters ! Attention, c\'est jusqu\'au 31 janvier !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 6 janvier</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=8970" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°10, spéciale nouvelle année ! A découvrir d\'urgence !</a></div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 20 décembre : </div>
    <div class="newsTxt">- Grand Tournoi de Hamster Academy : inscrit vite ton groupe !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 12 décembre : </div>
    <div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de Noël sont arrivés ! Va vite les découvrir dans la boutique !</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de Noël à mettre au pied du sapin sont arrivés !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 1er décembre : </div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=8700" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°9, spéciale hiver ! A découvrir d\'urgence !</a></div>
    <div class="newsTxt">- <img src="images/calendrier_avent_red.jpg" style="vertical-align:middle;width:50px;" alt="Calendrier de l\'Avent" /> Le calendrier de l\'Avent est là ! A découvrir en boutique !</div>
    </div>    
    <div class="newsBloc"><div class="newsDate">Samedi 14 novembre</div>
    <div class="newsTxt">- Fin du concours du meilleur dessin : résultats dimanche !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 2 novembre</div>
    <div class="newsTxt">- Concours du meilleur dessin : dessine le village ou la ville idéal pour les hamsters !</div>
    <div class="newsTxt">- Nouveau design du jeu !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 9 octobre</div>
    <div class="newsTxt">- Les règles pour les groupes changent petit à petit : plus dures, la guerre entre les groupes sera plus forte !</div>
    <div class="newsTxt">- 1) Un leader absent depuis 1 semaine est automatiquement remplacé. 2) 3 musiciens sur 4 doivent être inscrits à une répèt ou un concert pour la déclencher. 3) les musiciens doivent avoir un niveau similaire. 4) 2 hamsters maximum d\'un même joueur dans chaque groupe</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 4 octobre</div>
    <div class="newsTxt">- Rajout d\'un bouton pour nourrir tous les hamsters d\'un seul click, idem pour changer les copeaux des cages.</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 3 octobre</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=8185" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°7, spéciale automne ! A découvrir d\'urgence !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 5 septembre</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=7888" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°6 !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 7 août</div>
    <div class="newsTxt">- <a class="boxl" href="#" onClick="window.open(\'http://hamsteracademy.spreadshirt.net\',\'shopfenster\',\'scrollbars=yes,width=650,height=450\') ; return false;"><img src="http://image.spreadshirt.net/image-server/image/product/15025448/view/1/type/png/width/190/height/190" border="0" alt="" title="10253592-15025448" width="40" /> Le 1er t-shirt officiel ! (attention, ce ne sont pas pour les hamsters !)</a> </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 2 août</div>
    <div class="newsTxt">- <img src="images/hamac.gif" alt="hamac" width="40" align="middle" /> Nouvel objet pour la cage : le hamac !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 1er aout</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=7429" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°5 !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 26 juillet</div>
    <div class="newsTxt">- <img src="images/piscine_red.gif" alt="piscine" align="middle" /> Spécial été : la piscine est disponible sur Hamster Academy !</div>
    <div class="newsTxt">- <img src="images/texture_mer_red.gif" alt="Décoration cage plage / mer" align="middle" /> Nouvelle décoration possible pour la cage pour l\'été !</div>
    <div class="newsTxt">- <img src="images/cocotier4.png" height="30" alt="Cocotier Plage" align="middle" /> <img src="images/coquillage.png" height="20" alt="Coquillage Plage" align="middle" />C\'est l\'été ! De nouveaux objets à découvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 23 juillet</div>
    <div class="newsTxt">- Nouvelles catégories dans le <a href="forum/index.php">Forum</a>&nbsp;! Parlez de vos vrais hamsters, cages, petites annonces, etc.</div>
    <div class="newsTxt">- <img src="images/cafe.gif" height="30" alt="Tchat / chatter" align="middle" /> Le Café des Hamsters est de retour ! A vous le tchat entre joueurs ! A découvrir dans le <a href="forum/index.php">forum</a>&nbsp;!</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 11 juillet</div>
    <div class="newsTxt">- <img src="images/trombone.gif" width="40" alt="Changer d instrument" align="middle" /> Désormais, les hamsters peuvent changer d\'instrument !</div>
    <div class="newsTxt">- Indique ta date de naissance ! (Dans Options)</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 5 juillet</div>
    <div class="newsTxt">- <img src="images/HammerLePirate.jpg" width="40" alt="Battre Hammer le Pirate" align="middle" /> Une nouvelle mission très sportive !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 4 juillet</div>
    <div class="newsTxt">- <img src="images/partition_rockrollhamsters.jpg" width="40" alt="Partition Rock Roll Hamsters" align="middle" /> Une nouvelle mission musicale !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 2 juillet</div>
    <div class="newsTxt">- <img src="images/hamster_chantilly_red.jpg" width="40" alt="Hamster Chantilly" align="middle" /> Un nouvel hamster a fait son apparition ! A découvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 1er juillet</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=6973" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°4 !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 25 juin</div>
    <div class="newsTxt">- Découvre la liste des joueurs VIP <a href="jeu.php?mode=m_eleveurs&amp;univers=0" title="Liste des joueurs VIP" >à la mairie&nbsp;!</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 1er juin</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=6663" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°3 !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 11 mai</div>
    <div class="newsTxt">- <img src="images/oeufs/oeufs1.gif" width="40" alt="Oeufs de Pâques" align="middle" /> Attention, plus que 2 jours pour trouver les oeufs de Pâques (fin le 13 mai)...</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 4 mai</div>
    <div class="newsTxt">- Attention, le véto ne garde les hamsters que 2 semaines...</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 2 mai</div>
    <div class="newsTxt">- <a href="options.php?mode=m_vip">Deviens VIP sur Hamster Academy</a> ! </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 1er mai</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=6168" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°2 !</a></div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 12 avril</div>
    <div class="newsTxt">- <img src="images/oeufs/oeufs1.gif" width="40" alt="Oeufs de Pâques" align="middle" /> Essaye de trouver les 10 oeufs de Pâques cachés dans le jeu ! Il y a une récompense pour ceux qui les trouvent !</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=5769" title="la Gazette des Modérateurs" > Découvrez la Gazette des Modérateurs, n°1 !</a></div>
    <div class="newsTxt">- <img src="images/tonneau.gif" width="40" alt="Tonneau Sésame" align="middle" /> Un nouvel objet à découvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 8 avril</div>
    <div class="newsTxt">- Une nouvelle mission !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 2 avril</div>
    <div class="newsTxt">- <img src="images/tennis_red.jpg" width="40" alt="Tennis" align="middle" /> Un nouveau sport pour dégourdir les jambes et les bras des hamsters ! A découvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 29 mars</div>
    <div class="newsTxt">- <img src="images/hamster_choupette_red.jpg" width="40" alt="Hamster Choupette" align="middle" /> <img src="images/hamster_tarzan_red.jpg" width="40" alt="Hamster Touffou" align="middle" /> 2 nouvelles races de hamsters à découvrir !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 28 mars</div>
    <div class="newsTxt">-<img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Concours de la plus belle cage, jusqu\'au 15 avril ! Inscription à partir du 1er avril.</div>
    </div>    
    <div class="newsBloc"><div class="newsDate">Dimanche 1er Février</div>
    <div class="newsTxt">- <img src="images/dessin_concours_jan_2009.gif" alt="Dessin Hamster Academy" style="vertical-align:middle; width:40px;" /> Les plus beaux dessins sont sur Hamster Academy !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 30 Janvier</div>
    <div class="newsTxt">- <img src="images/hamster_gourmand_red.gif" alt="Hamster Gourmand" style="vertical-align:middle; width:40px;" /> Nouvel hamster : le Hamster Gourmand</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 26 Janvier</div>
    <div class="newsTxt">- Nouvelle mission</div>
    <div class="newsTxt">- Fin du jeu de dessin, résultats bientôt !</div>
    </div>    
    <div class="newsBloc"><div class="newsDate">Jeudi 22 Janvier</div>
    <div class="newsTxt">- <img src="images/HamsterGrungeRed.jpg" alt="Hamster Grunge" style="vertical-align:middle; width:40px;" /> Nouvelle race de hamster : le Hamster Grunge ! A découvrir d\'urgence en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 14 Janvier</div>
    <div class="newsTxt">- Nouvelles missions !</div>
    <div class="newsTxt">- Le moral, la force, l\'énergie... peuvent dépasser 10 selon le niveau du joueur</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 7 Janvier</div>
    <div class="newsTxt">- Jeu du mois ! Objectif : le plus beau dessin ou histoire avec dessins. Tu as jusqu\'au 25 janvier pour participer !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 4 Janvier</div>
    <div class="newsTxt">- <img src="images/part_galette.gif" alt="galette des rois" width="30" /> La galette des rois est arrivée ! Cours vite l\'acheter en Boutique (en Ville) pour tes hamsters ! Attention, c\'est jusqu\'au 20 janvier !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 3 janvier</div>
    <div class="newsTxt">- <img src="images/bac_reduc2.gif" alt="bac fourre-tout" width="20" /> Un inventaire plus pratique est enfin disponible !</div>
    <div class="newsTxt">- <img src="images/bac_reduc2.gif" alt="bac fourre-tout" width="20" /> Via l\'inventaire, on peut donner les objets de son choix à ses amis !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 2 janvier</div>
    <div class="newsTxt">- <img src="images/energie.gif" alt="énergie" width="20" /> Barre d\'énergie pour les activités sportives : chaque activité coute de l\'énergie qui se recharge avec le temps.</div>
    <div class="newsTxt">- <img src="images/fiole.gif" alt="fiole d énergie" width="20" /> Permet de donner de l\'énergie au hamster ! A découvrir en boutique !</div>
    <div class="newsTxt">- Tous les membres d\'un groupe peuvent éditer la description du groupe (sauf si le leader refuse)</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 25 décembre</div>
    <div class="newsTxt">- <img src="images/plante.gif" alt="plante verte" width="20" /> Nouvel objet à découvrir en boutique pour la cage : une plante d interieur !</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Pour ceux qui avaient acheté des cadeaux en boutique, ils sont arrivés !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 22 décembre</div>
    <div class="newsTxt">- Affichage du classement !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 21 décembre</div>
    <div class="newsTxt">- <b>Nouveau calcul des scores</b> pour les groupes. Attention, les scores des hamsters et des joueurs peuvent baisser!</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 15 décembre</div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> <span '.tooltip("Cage du vainqueur superleo : &lt;br/&gt; &lt;img src=images/cage_superleo_red2.jpg style=&quot;max-width:400px;&quot;&gt;").' >Résultats du championnat de la plus belle cage !</span> <a href="#" onclick="javascript: pop(\'afficheCage.php?cage_id=61457\',null,600,800); return false;" title="Voir la cage">Voir la cage du vainqueur!</a></div>
    </div>    
    <div class="newsBloc">
    <div class="newsDate">Jeudi 9 décembre</div>
    <div class="newsTxt">- début des votes de la plus belle cage !</div>
    <div class="newsTxt">- nouveau calcul des scores</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 04 décembre</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de Noël à mettre au pied du sapin sont arrivés !</div>
    <div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de Noël sont arrivés ! Va vite les découvrir dans la boutique !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 1er décembre : </div>
    <div class="newsTxt">- <img src="images/calendrier_avent_red.jpg" style="vertical-align:middle;width:50px;" alt="Calendrier de l\'Avent" /> Le calendrier de l\'Avent est là ! A découvrir en boutique !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 29 novembre : </div>
    <div class="newsTxt">- Nouvelle mission de l\'hiver...</div>
    </div>    
    <div class="newsBloc">
    <div class="newsDate">Samedi 22 novembre : </div>
    <div class="newsTxt">- Lancement du jeu de la plus belle cage !</div>
    <div class="newsTxt">- Groupes : nouvelle présentation, possibilité de mettre des images plus grandes</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 13 novembre : </div>
    <div class="newsTxt">- <img src="images/texture_feu_red.gif" width="30" alt="feu" align="middle" /> <img src="images/texture_halloween_red.gif" width="30" alt="halloween" align="middle" /> Nouvelle peinture pours les cages&nbsp;! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 27 octobre : </div>
    <div class="newsTxt">- <img src="images/texture_coeur_red.gif" width="30" alt="coeur" align="middle" /> Nouvelle peinture pours les cages&nbsp;! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 21 septembre: </div>
    <div class="newsTxt">- Jeu du hamster le plus heureux ! Inscription en Ville !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 17 septembre: </div>
    <div class="newsTxt">- Décrit ton groupe de musique avec plus d\'options !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 9 septembre: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> <span '.tooltip("Cage du vainqueur louliou : &lt;br/&gt; &lt;img src=images/cage_louliou_red.png style=&quot;max-width:400px;&quot;&gt;").' >Résultats du jeu de la plus belle cage !</span> <a href="#" onclick="javascript: pop(\'afficheCage.php?cage_id=4438\',null,600,800); return false;" title="Voir la cage">Voir la cage du vainqueur!</a></div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 19 août : </div>
    <div class="newsTxt">- Indique ton blog !</div>
    <div class="newsTxt">- Ecris des messages plus sympas à la Poste !</div>
    <div class="newsTxt">- Indique tes amis !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 16 juillet : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Jeu de la plus belle cage jusqu\'au 18 aout ! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 5 juillet : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Jeu de slogans !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 25 juin: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Les résultats du jeu de beauté sont arrivés ! Va vite voir en Ville, à la Mairie !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 6 juin: </div>
    <div class="newsTxt">- <img src="images/livre_beaute.gif" alt="Livre de beauté" align="middle" width="40" /> Nouveau : un Traité de beauté ... à vite donner à son hamster !</div>
    </div>
    ';
    
    if (defined ('TOUTES_LES_NOUVEAUTES')) {
    
    $dernieresNouveautes .= '    
    
    <div class="newsBloc">
    <div class="newsDate">Jeudi 5 juin: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Le jeu de beauté a commencé !</div>
    <div class="newsTxt">- <img src="images/flacon_hamsteropoil.gif" alt="flacon" align="middle" /> Un nouveau produit de beauté a fait son apparition !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 20 mai : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" align="middle" /> Les résultats du jeu du hamster le plus sportif sont arrivés !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 18 mai : </div>
    <div class="newsTxt">- <img src="images/notes3.gif" alt="musique" align="middle" /> Concerts et répètitions : à vous d\'améliorer votre groupe encore davantage : de l\'expérience, des points et des pièces en récompense !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 9 mai : </div>
    <div class="newsTxt">- <img src="images/poubelle.gif" alt="cage vente" /> Bon à savoir, il est désormais possible de vendre sa cage.</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 8 mai : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" /> Jeu du hamster le plus sportif ! Pour s\'inscrire, il faut aller en Ville.</div>
    </div>        
    <div class="newsBloc">
    <div class="newsDate">Lundi 14 Avril : </div>
    <div class="newsTxt">- <img src="images/forum.gif" alt="Café des Hamsters" width="30" align="middle" /> Le Café des Hamsters a ouvert ses portes : on peut y discuter entre éleveurs en direct ! Il est au Forum !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 2 Avril : </div>
    <div class="newsTxt">- Les résultats du jeu du plus meilleur groupe à l\'Academy ! A découvrir en Ville puis en Mairie...</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 23 Mars : </div>
    <div class="newsTxt">- Essaye de trouver les 10 oeufs de Pâques cachés dans le jeu ! Il y a une récompense pour ceux qui les trouvent !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 17 Mars : </div>
    <div class="newsTxt">- Jeu du mois de Mars : meilleur groupe à l\'Academy ! Les détails sont en Ville puis à la Mairie...</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 9 Mars : </div>
    <div class="newsTxt">- <img src="images/interro_reduc2.png" alt="enigme" /> Une nouvelle façon de faire maigrir son hamster a fait son apparition. A toi de déviner comment...</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 7 Mars : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" /> Les résultats du jeu du plus beau dessin/photo sont arrivés ! A découvrir en Ville puis en Mairie...</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 10 Février : </div>
    <div class="newsTxt">- Ouverture du jeu du plus beau dessin/photo ! Jusqu\'au 29 février.</div>
    </div>

    <div class="newsBloc"><div class="newsDate">Lundi 21 Janvier</div>
    <div class="newsTxt">- <img src="images/coeur.gif" alt="mariage" width="30" /> Les hamsters peuvent se marier ! Pour cela, rendez-vous dans la Mairie en ville !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 9 Janvier</div>
    <div class="newsTxt">- <img src="images/part_galette.gif" alt="galette des rois" width="30" /> La galette des rois est arrivée ! Cours vite l\'acheter en Boutique (en Ville) pour tes hamsters ! Attention, c\'est jusqu\'au 20 janvier !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 5 Janvier</div>
    <div class="newsTxt">- <img src="images/agenda.gif" alt="coupe" width="20" /> Le jeu du meilleur poème et de la meilleure chanson est ouvert ! Explications dans le <a href="http://www.hamsteracademy.fr/forum/index.php">Forum</a>, à la rubrique "Annonces".</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 2 Janvier</div>
    <div class="newsTxt">- <img src="images/danseuse.gif" alt="danseuse étoile" width="20" /> <img src="images/crocodile.gif" alt="crocodile" width="25" /> Nouveaux métiers : aventurier, mannequin, vendeur de hamburgers, éleveur de crocodiles, etc.</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mardi 1er Janvier</div>
    <div class="newsTxt">- Toute l\'équipe de l\'Hamster Academy souhaite la bonne année à tous les joueurs ! Accrochez-vous, plein de nouveautés sont prévues pour ce début d\'année !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 24 décembre</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau" width="20" /> Les cadeaux de Noël sont arrivés !</div>
    <div class="newsTxt">- <img src="images/cactus.gif" alt="cactus" width="20" /> Cactus et le bonzai disponibles ! A découvrir en Boutique !</div>
    <div class="newsTxt">- <img src="images/console.gif" alt="console" width="30" /> Console de jeu ! A vous les soirées jeu entre amis (le mercredi et le dimanche) ! </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 14 décembre</div>
    <div class="newsTxt">- <img src="images/ballon_foot.gif" alt="foot" width="20" /> Sortie football possible ! A découvrir en Boutique !</div>
    <div class="newsTxt">- <img src="images/ballon_foot.gif" alt="foot" width="20" /> <img src="images/ballon_rugby.gif" alt="rugby" width="20" /> <img src="images/halteres_reduc.gif" alt="haltères" width="20" /> <img src="images/boule_noire_reduc.gif" alt="boule" width="20" /> Et face aux multiples possibilités d\'activités, ces dernières sont dorénavant reparties tout au long de la semaine. </div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 10 décembre</div>
    <div class="newsTxt">- <img src="images/micro2.gif" alt="micro" width="20" /> Il y a plein de nouveaux instruments&nbsp;! A découvrir en boutique&nbsp;!</div>
    <div class="newsTxt">- <img src="images/detective.gif" alt="micro" width="40" /> Nouveaux métiers ! Détective, policier, skater...</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Vendredi 07 décembre</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de Noël à mettre au pied du sapin sont arrivés !</div>
    <div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de Noël sont arrivés ! Va vite les découvrir dans la boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 05 décembre</div>
    <div class="newsTxt">- Le jeu de la plus belle cage est ouvert ! Inscription jusqu\'au mercredi 12 décembre.</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 28 novembre</div>
    <div class="newsTxt">- Les chocolats de Noël sont arrivés ! A découvrir d\'urgence dans la boutique !</div>
    <div class="newsTxt">- <img src="images/saxophone.gif" alt="Saxophone" width="32" /> Le saxophone est arrivé !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 26 novembre</div>
    <div class="newsTxt">- On peut choisir son avatar dans le jeu : il faut aller dans Options.</div>
    <div class="newsTxt">- On peut choisir l\'image du groupe : le leader du groupe peut mettre une image pour représenter le groupe. Motivez-le à le faire !</div>
    </div>
    ';
    }
}