<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

$nbPiecesParAchatBanque = 300 ; // nb de pièces données au joueur lorsqu'il paye par téléphone
$nbPiecesParAchatBanqueCB = 1000; // nb de pièces données au joueur lorsqu'il paye par CB (ticket de 5 euros)
$nbPiecesParAchatBanqueCB2 = 3000; // nb de pièces données au joueur lorsqu'il paye par CB (ticket de 10 euros)
$nbPiecesParAchatBanqueCB3 = 10000;// nb de pièces données au joueur lorsqu'il paye par CB (ticket de 30 euros)

$intervalleMajEnergie = 1200 ; // 20 minutes
$energieFiole = 20; // énergie contenue dans la fiole
$energieHalteres = $energieBoule = $energiePromenade = 3;
$energieFoot = $energieRugby = $energieConsole = $energieTennis = 5;
$energieTennisHammer = 15;
$energieCatch = 20;
$energieMusique = 1;
$erreurActionEnergie = 145;

$coutCube = 20 ; // cout cage pour chaque "cube" de la cage (bloc indivisible de la cage, 1 objet par bloc)
$coutFabrication = 10 ; // cout de l'agrandissement de la cage
$nbPiecesInscription = 150 ; // en pièces
$alimentsInscription = 10; // en portions
$quantiteCopeauxInscription = 2 ; // en litres
$metierInscription = 0 ;
$niveauInscription = 1 ;
$objectifNiveauInscription = 0;
$typeCageInscription = 0 ;
$etagesInscription = 1 ;
$fondInscription = 0 ;
$propreteInscription = 4 ;
$toitInscription = 0 ;
$colonnesInscription = 1 ;
$profondeursInscription = 1;
$poidsMoyen = 120; // poids moyen de l'hamster en gr
$prixHamster = 50; // prix d'achat de l'hamster
$coutVeto = 100 ; // frais de veto lorsque l'hamster a ete place en reanimation
$coutInscriptionAcademy = 5 ; // cout d'inscription a l'academy pour chaque hamster
$coutCoursChant = 4;
$coutCoursGuitare = 4;
$coutCoursDanse = 6;
$coutCoursDanseTektonic = 10;
$coutCoursMusique = 3;
$coutCoursMatchImpro = 7;
$coutCoursBlague = 5;
$coutRefuge = 10;
$coutAbandonHamster = 15;
$coutCreationGroupe = 30;
$dureeGrossesse = 249600; // 4 jours
$coutSuppressionHamster = 15 ; // cout pour se débarrasser d'un hamster
$coutDivorce = 10;
$prixVenteHamster = 5; // prix de vente pour 1 hamster
$coutDemandeMariage = 10 ; // cout de la demande en mariage
$coutPeinture = 5; // cout de la peinture en couleur basique pour la cage
$coutPeintureCoeur = 20; // cout de la peinture en motif coeur pour la cage
$coutPeintureFeu = 15; // cout de la peinture en motif feu pour la cage
$coutPeintureHalloween = 25; // cout de la peinture en motif halloween pour la cage
$coutPeintureMer = 50; // cout de la peinture en motif mer pour la cage
$coutPeintureNeige = 10;
$coutCoursFoot = 10; 

$puissanceAmuletteForce = 1.; // facteur multiplicatif de l'Amulette de Force

$groupeNbMaxMembres = 8; // nombre de membres maximum dans un groupe

$dureeFabricationNidCabane = 172800 ; // 2 jours

$niveauMinimalAccesAcademy = NIVEAU_DEBUT_ACADEMY;
$niveauMinimalPourLeMariage = NIVEAU_MARIAGE;

// dimensions des blocs unitaires qui composent chaque cage
$tailleBordureCageGauche =  0 ;
$tailleBordureCageBas =  0 ;

$etagereHauteurMaxObjet = 70; // hauteur maximale des objets sur l'étagère de la boutique

// variables générales
$quantiteNourritureQuotidienneAvalee = 1; // en portions
$intervalleEntreDeuxSports = 4 ; // en heures
$intervalleEntreDeuxVitamines = 4 ; // en heures
$intervalleEntreDeuxBeaute = 4 ; // en heures
$intervalleEntreDeuxCaresses = 4 ; // en heures
$intervalleEntreDeuxExamen = 3 ; // en jours
$intervalleEntreDeuxConfessions  = 1 ; // en jours

$nbJoursSalairesSansConnexion = 3; // le salaire n'est versé que pour 3 jours max sans connexion du joueur

// pour le vip
$vip_nbConnexionsMin = 15;
$vip_nbMessagesMin = 10;
$vip_nbParainagesMin = 2;
$vip_niveauMin = NIVEAU_POTAGE;
$vip_noteProfilMin = 2.5;
$vip_reducAchat = 0.85;
$vip_nbParrainages = 2;

define('RAYON_AUCUN',-1);
define('RAYON_ENTRETIEN',1);
define('RAYON_ACC_CAGE',2);
define('RAYON_JOUETS',3);
define('RAYON_SPORT',5);
define('RAYON_BEAUTE',6);
define('RAYON_HAMSTER',7);
define('RAYON_CAGE',8);
define('RAYON_BANQUE',9);
define('RAYON_INSTRUMENT',10);
define('RAYON_PHARMACIE',11);

define ('PAGE_JEU_CAGE',1);
define ('PAGE_JEU_ACHAT',2);
define ('PAGE_JEU_ACHAT_PIECE',3);
define ('PAGE_JEU_METIER',4);
define ('PAGE_JEU_HAMSTER',5);
define ('PAGE_JEU_ACADEMY_ENTRAINEMENT',6);
define ('PAGE_CONSTRUCTION_NID',7);
define ('PAGE_CONSTRUCTION_CABANE',8);
define ('PAGE_JEU_ACADEMY',9);
define ('PAGE_JEU_ACCUEIL',10);

define ('ERREUR_ACTION_CAGE_ETAGE',1);
define ('ERREUR_ACTION_CAGE_PROFONDEUR',2);
define ('ERREUR_ACTION_CAGE_COLONNE',3);

define ('UNIVERS_ELEVEUR',0);
define ('UNIVERS_ACADEMY',1);
define ('UNIVERS_ADMIN',2);
define ('UNIVERS_INDEX',3);
define ('UNIVERS_FOOT',4);

$lstUnivers = array(
'fr'=> array('Univers Eleveur','Univers Academy','Univers Hamster','','Univers Foot'),
'en'=>array('Greeder World','Academy World','Hamster World','Foot World')
);

define ('INFO_CAGE_LARGEUR_IMAGE',0);
define ('INFO_CAGE_HAUTEUR_IMAGE',1);
define ('INFO_CAGE_LARGEUR_BAC',2);
define ('INFO_CAGE_HAUTEUR_BAC',3);
define ('INFO_CAGE_RATIO',4); // ratio largeur/hauteur (quand on monte d'1 pixel, on va 1.37 pixel à droite)
define ('INFO_CAGE_HAUTEUR_GRILLE',5);

// constantes sur la couleur du fond de la cage
define ('FOND_CAGE_BLEUVERT',0);
define ('FOND_CAGE_ROSE',1);
define ('FOND_CAGE_NOIR',2);
define ('FOND_CAGE_BLANC',3);
define ('FOND_CAGE_COEUR',4);
define ('FOND_CAGE_FEU',5);
define ('FOND_CAGE_HALLOWEEN',6);
define ('FOND_CAGE_MER',7);
define ('FOND_CAGE_NUIT',8);
define ('FOND_CAGE_NEIGE',9);

// pour les messages et status
define ('MESSAGE_STATUS_INCONNU',0);
define ('MESSAGE_ENVOYE',1);
define ('MESSAGE_LU',2);
define ('MESSAGE_SUPPRIME',3);

define ('DEMANDE_MARIAGE',3);
define ('TYPE_INTERACTION_DEFI',4);

define ('HISTORIQUE_MARIAGE',0);
define ('HISTORIQUE_NAISSANCE',1);
define ('HISTORIQUE_MARIAGE_EN',2);
define ('HISTORIQUE_NAISSANCE_EN',3);

define ('HAMSTER_ACADEMY_ID',1371) ; // id de HamsterAcademy (pour l'envoi des emails officiels)

$infosCage = array(
0 => array(1.,1.,1.,1.),
1 => array(410.,138.,264.,38.,1.37,106.),
2 => array(492.,170.,330.,45.,1.37,126.),
3 => array(574.,193.,390.,52.,1.37,148.)
) ;

$lstNiveauxAcademy = array (
T_("Académicien débutant"),
T_("Académicien confirmé"),
T_("Académicien expérimenté"),
T_("Académicien professionel"),
T_("Académicien de génie"),
T_("Maître Académicien"),
""
) ;

$lstNiveauxFoot = array (
 T_("Footballeur débutant"),
 T_("Footballeur amateur"),
 T_("Footballeur confirmé"),
 T_("Footballeur professionel"),
 T_("Footballeur de génie"),
 T_("Maître Footballeur"),
""
) ;

define ('NIVEAU_ACADEMY_PRO',3) ;
define ('NIVEAU_ACADEMY_FIN',5) ;

$lstOeufs= array (
10,
500,
4879,
13208470,
121,
1280,
45648,
1218,
211048,
784231
) ;

$lstIngredients= array (
101,
5001,
48791,
1320470,
1211,
1280,
456148,
12118,
2110148,
78411231
) ;

$lstNiveauxTournois = array (
    "Finale",
    "1/2 finale",
    "1/4 finale",
    "1/8 finale",
    "1/16 finale",
    "1/32 finale",
    "1/64 finale",
    "1/128 finale",
    "1/256 finale",
    "1/512 finale",
    "1/1024 finale"
);

$lstSpecialitesFoot = array(
     T_("Remplaçant"),
     T_("Attaquant"),
     T_("Défenseur"),
     T_("Gardien"),
     T_("Milieu")    
    );

define('GROUPE_MUSIQUE',1);
define('EQUIPE_FOOT',2);

// description du niveau + nb de points d'expérience pour l'atteindre + description supp
$lstNiveauxGroupe = array (
    0 => array( T_("Premières répétitions"),0, T_("Le groupe vient tout juste d'être créé ! Les premières répétitions commencent !"),10),
    1 => array( T_("Premiers concerts devant les amis"),20, T_("Au fond de la cave, devant quelques amis des membres du groupe"),20),
    2 => array( T_("Concerts au Ham'Bar du coin"),50, T_("La salle du bar contient une trentaine de places assises et propose plein de cocktails à base de noisettes, glands et parfums forestiers"),75),
    3 => array( T_("Concerts sur scène pour les fêtes du village"),100, T_("Grâce aux scènes en hauteur, le groupe peut jouer devant de plus en plus de monde ! Il y a plusieurs centaines de personnes qui viennent écouter ! Pendant ce temps-là, le groupe prépare son premier disque !"),125),
    4 => array( T_("Sortie du premier disque et concerts"),150, T_("Ca y est, c'est le début du succès, le groupe a sorti son premier album !"),200),
    5 => array( T_("Concert au Zenith de l'Arbre Sans Feuille"),300, T_("L'Arbre Sans Feuille est un magnifique endroit pour écouter le concert, tout le public est assis sur les branches. Il peut y avoir plusieurs milliers de personnes qui viennent écouter !"),300),
    6 => array( T_("Passage à l'émission TV Hamster Academy"),350, T_("Le groupe commence à être connu un peu partout, surtout après son passage à la télé ! Le manager très connu HamZik aide maintenant le groupe à développer son succès !"),300),
    7 => array( T_("Concerts au Hamster Palace"),400, T_("Ce palace brille grâce aux milles feux de bambous. Sa salle des fêtes contient très exactement 6770 places !"),400),
    8 => array( T_("Sortie du second disque, il est disque d'or"),800, T_("Disque d'or!! Le deuxième album du groupe s'est vendu à plus de 50 000 exemplaires dans toute la communauté des hamsters !"),800)
);

$lstNiveauxEquipe = array (
    0 => array( T_("Premiers matchs"),0,T_("Premiers matchs entre amis, histoire de souder un peu l'équipe."),10),
    1 => array( T_("Entrainement au stade municipal"),30,T_("Ca y est, l'équipe de foot a progressé et la ville de Hamster Academy ouvre l'accès au stade pour les entrainements ! C'est mieux pour progresser !"),30)
);

define ('MALADIE_NOM',0) ;
define ('MALADIE_DESC',1) ;
define ('MALADIE_TRANSMISSION',2) ; // mode de transmission
define ('MALADIE_MEDICAMENT',3) ;
define ('MALADIE_GRAVITE',4) ; // nb de santé en moins chaque jour
define ('MALADIE_IMAGE',5) ;
define ('MALADIE_NIVEAU_MIN',6) ;
define ('MALADIE_SYMPTOMES',7) ;

$lstMaladies = array (
    0 => array( "non malade", "","",0,0,"",0,""),
    1 => array( T_("la straphyloctoque, dite maladie de la Salive Bleue"), T_("Cette maladie n'est pas très grave mais inquiète le hamster qui ne reconnaît plus sa salive habituelle. Il devient nerveux, fatigué et sa santé baisse."),T_("l'air et les poussières"),118,1,"maladie_salive_bleue.jpg",7,T_("salive bleue, fatigue, moral en berne")),
    2 => array( T_("la stalactaque, dite le Rhume des Corbeaux"), T_("Cette maladie est à prendre au sérieux. Le hamster a du mal à se chauffer, il attrape froid et se fatigue très vite ! Il ne faut pas tarder à le guérir."),T_("l'air frais et les déjections d'oiseaux"),120,5,"cristaux_glace.jpg",16,T_("nez qui coule, grande fatigue, impression de froid")),
    3 => array( T_("la fièvre des Fougères"), T_("Certaines variétés de fougères utilisent le pelage du rongeur pour se développer. Hélas, le hamster en souffre particulièrement, son pelage verdit et surtout, il a de la fièvre. Il se défend tout seul mais que si sa température redescend à la température normale. Il a besoin d'un coup de pouce."),T_("les courants d'air, promenade en forêt"),130,0.5,"fougeres.jpg",4,T_("fièvre, tâches vertes sur le pelage"))
);

define ('MALADIE_CHORINTHE',1) ;
define ('MALADIE_STALACTIC',2) ;
define ('MALADIE_FIEVRE',3) ;

$email_contact_fr = "contact@hamsteracademy.fr";
$email_contact_en = "contact@hamsteracademy.com";

$email_contact = $email_contact_fr;
if ($lang == "en")
    $email_contact = $email_contact_en;

$headerMailFromHamsterAcademy  = 'From: Hamster Academy <'.$email_contact.'>'."\n";
$headerMailFromHamsterAcademy  .=  'Content-type: text/plain; charset=UTF-8'."\n";
