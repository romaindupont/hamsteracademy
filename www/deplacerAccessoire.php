<?php

define ('IN_HT', true);

include "common.php";
include "lstAccessoires.php";
include "lstMessages.php";
include "lstNiveaux.php" ;
include "gestion.php";

$userdata = session_pagestart($user_ip);
$user_lang = "fr";

require "lang/".$user_lang."/common_lang.php";

if( ! $userdata['session_logged_in'] ) { 
    return;
}

$accessoire_id = -1;
$accessoireIndex = -1;
if (isset($_GET['accessoire_id'])){
    $accessoire_id_s = mysql_real_escape_string($_GET['accessoire_id']) ;
    $accessoire_id = intval(substr($accessoire_id_s,4));
    $accessoireIndex = accessoireCorrespondant($accessoire_id);
}
else
    return;

if (isset($_GET['etage_id'])){
    $etage_id_s = mysql_real_escape_string($_GET['etage_id']) ;
    $nouvel_etage = intval(substr($etage_id_s,6));
}
else
    return;
        
if ($accessoire_id == -1)
    return ;
    
// on récupère l'accessoire
$query = "SELECT a.*, c.* FROM accessoires a, config_accessoires c WHERE a.joueur_id=".$userdata['joueur_id']." AND a.type = c.type AND a.accessoire_id=".$accessoire_id;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
}

if ($dbHT->sql_numrows($result) != 1)
    return ;

$accessoire_a_deplacer=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);
    
// on récupère la cage associée
$query = "SELECT * FROM cage WHERE joueur_id=".$userdata['joueur_id']. " AND cage_id=".$accessoire_a_deplacer['cage_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
if ($dbHT->sql_numrows($result) != 1)
    return ;

$cageRow=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);
    
$posX = $accessoire_a_deplacer['posX'] ; 
$etage_courant = $accessoire_a_deplacer['etage'] ;
$posY = $accessoire_a_deplacer['posY'] ; 

$accWidth = $accessoire_a_deplacer['img_width'] * $accessoire_a_deplacer['echelle'];
$accHeight = $accessoire_a_deplacer['img_height'] * $accessoire_a_deplacer['echelle'];
$accBaseY = $accessoire_a_deplacer['decal_y'] * $accessoire_a_deplacer['echelle'];
        
$positionActuelleStatut = corrigePositionAcccessoire($posX,$posY,$etage_courant, $cageRow, $accWidth, $accHeight,$accBaseY);
        
$deplacement = false ;

if (isset($_GET['posX'])) {// déplacement absolu avec la souris
    
    $posX = intval($_GET['posX']);
    if (isset($_GET['posY']))
        $posY = intval($_GET['posY']);
    $deplacement = true;
}

$positionNouvelleStatut = corrigePositionAcccessoire($posX,$posY,$nouvel_etage, $cageRow, $accWidth, $accHeight,$accBaseY);

// mise à jour de la colonne + profondeur + étage
$query = "UPDATE accessoires SET posX = ".$posX.",
        etage = ".$nouvel_etage.",     
        posY=".$posY." 
        WHERE accessoire_id=".$accessoire_id;
if ( !($result = $dbHT->sql_query($query)) )
    message_die(GENERAL_ERROR, 'Errorin obtaining accessoires_data', '', __LINE__, __FILE__, $query);
    
setCageToBeRebuilt($cageRow['cage_id']);
                                                                   
// mise à jour du niveau du joueur (en cas d'un changement d'étage)
$missionReussie = 0;
if ($userdata['niveau']==NIVEAU_GESTION_ETAGE) { 
    if ($nouvel_etage != $etage_courant){
        updateNiveauJoueur(1,true) ;
        $missionReussie = 1;
    }
}        

// update aussi du tableau lstAccessoires
$accessoire_a_deplacer['posX'] = $posX ;
$accessoire_a_deplacer['etage'] = $nouvel_etage ;
$accessoire_a_deplacer['posY'] = $posY ;

echo $posX.",".$nouvel_etage.",".$posY.",".$missionReussie ;
                                                                      
?>