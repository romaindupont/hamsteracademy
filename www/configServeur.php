<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

error_reporting(0);

$serveur='xxx'; 
$user='xxx'; 
$password='xxx'; 
$base='xxx';  

$serveurForum_fr='xxx'; 
$userForum_fr='xxx'; 
$passwordForum_fr='SKdMyPMk'; 
$baseForum_fr='xxx';  
$prefixForum_fr = 'xxx_';

$serveurForum = $serveurForum_fr; 
$userForum = $userForum_fr; 
$passwordForum = $passwordForum_fr; 
$baseForum = $baseForum_fr; 
$prefixForum = $prefixForum_fr;

$serveurForum_en='xxx'; 
$userForum_en='xxx'; 
$passwordForum_en='xxx'; 
$baseForum_en='xxx';  
$prefixForum_en = 'xxxs_';

$serveur_url_fr = "www.hamsteracademy.fr/";
$serveur_url_en = "www.hamsteracademy.com/";

if (! isset($template_dir))
    $template_dir = 'templates';

if (! isset($smarty_dir))
    $smarty_dir = '../cgi-bin/smarty';
    
if ( ! defined('NO_SMARTY') )
{
    require($smarty_dir.'/libs/Smarty.class.php');
    
    $smarty = new Smarty();
    $smarty->template_dir = $template_dir;
    $smarty->compile_dir = $template_dir.'/templates_c';
    $smarty->cache_dir = $template_dir.'/cache';
    $smarty->config_dir = $template_dir.'/configs';
    $smarty->caching = 0;
}

?>
