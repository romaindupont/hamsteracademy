<?php 

define('IN_HT', true);
include "common.php"; 

$pseudo = "";
$idee = "";

if (isset($_POST['pseudo'])) 
    $pseudo = mysql_real_escape_string($_POST['pseudo']) ;

if (isset($_POST['idee'])) 
    $idee = mysql_real_escape_string($_POST['idee']) ;
    
if($idee != "ton idée/remarque...." && strlen($idee) > 10 ) {

    // ajout de l'idée dans la BDD
    $query = "SELECT MAX(idee_id) as idee_id FROM idees";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);
    
    $query = "INSERT INTO idees VALUES (".$nouveauId." , '".$pseudo."','".$pseudo."','".$idee."', 0 , 0 ,".$dateActuelle.")" ;
    if ( ! $dbHT->sql_query($query) ) {
        echo "Erreur SQL : ".$query."<br>" ;
    }
}
?>

<html>
<head>
<title>Hamster Academy - <?php echo T_("Boîte à idées");?></title>
<meta name="description" content="Hamster Academy, <?php echo T_("Boîte à idées");?>">
<link rel="shortcut icon" href="favicon.ico" /> 
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<link href="css/style.css" rel="stylesheet" type="text/css">
<link href="css/styleEleveur.css" rel="stylesheet" type="text/css">
</head>

<body>

<div align="center"> 
  <h2>Hamster Academy</h2>
  <h1><?php echo T_("Boîte à idées");?></h1>
  <br/>
  <table align="cente"r cellpadding="20">
      <tr valign=top>
          <td><img src="images/ht-11.jpg" width=200></td>
          <td width=350>
              <strong><?php echo T_("Merci");?> !</strong>
              <br/>&nbsp;<br/>
              <?php echo T_("Si ton idée est retenue, on te l'informera par mail.");?>
          </td>
      </tr>
  </table>
  <p>&nbsp;</p>
  <a href="index.php"><?php echo T_("retour à la page d'accueil");?></a><br/>&nbsp;<br/>
  <?php echo T_("Et pour tout autre contact");?> : <a href="mailto:contact@hamsteracademy.fr">contact@hamsteracademy.fr</a>
  <br/>&nbsp;<br/>
  <div align=center>&copy; Hamster Academy</div>
</div>

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2189291-1";
urchinTracker();
</script>

</body>
</html>

