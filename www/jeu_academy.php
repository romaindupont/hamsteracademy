<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

if ($lang == "fr") {

    $msg .= "
    <div style=\"text-align:left;\">
    Il doit maintenant respecter le réglement intérieur de l'Academy.
    <br/>&nbsp;<br/>
    <strong>Rappel du règlement intérieur :</strong>
    
    <ul>
        <li><u>Article 1</u>. L'Academy est un lieu sérieux où il s'agit avant tout de travailler sa voix, sa danse, son humour etc. En aucun cas, l'Academy ne tolerera les comportements suivants&nbsp;: 
            <ul>
                <li>discuter, blaguer ou chahuter dans les couloirs de l'Academy et en cours : sinon, direction le bureau du Directeur</li>
                <li>de draguer ses camarades</li>
                <li>d'empêcher ses camarades de travailler sous quelque forme que ce soit</li>
            </ul>
            Si un désordre est constaté, le hamster sera puni dans le bureau du Directeur qui lui fera la morale atrocement pendant plusieurs longues heures, jusqu'à ce que baillements s'ensuivent.
            </li>                
        <li><u>Article 2</u>. Les examens ont lieu quand le hamster inscrit le souhaite mais un intervalle de 3 jours doit être respecté entre 2 passages. Attention donc à bien travailler avant !</li>
        <li><u>Article 3</u>. Une fois par jour, le joueur peut aller se confier tout seul face au public extérieur via une caméra. Une tâche pénible mais le public adore et la popularité monte en général.</li>
        <li><u>Article 4</u>. Chaque hamster a une seule spécialité en musique (guitariste, violoniste...). Une fois qu'elle est choisie, elle est définitive.</li>
    </ul>
    </div>";
}
?>