/*
 * jQuery CornFLEX plugin
 *
 * @name	cornflex
 * @type	jQuery
 * @param	boxname		options		
 */
(function($) {
	$.fn.cornflex = function(boxname, options) {
		
		var settings = {
			omega		: 0,
			boxname		: boxname,
			image_t		: this.boxname + '_t.png',
			image_r		: this.boxname + '_r.png',
			image_b		: this.boxname + '_b.png',
			image_l		: this.boxname + '_l.png',
			alpha		: 0,
			beta		: 0,
			gamma		: 0,
			delta		: 0,
            width       : this.width
		};
		
		if (options) {
			$.extend(settings, options);
		}
		
		//$(this).each(function(){
			$(this).wrap('<div class="cornflex ' + settings.boxname + '"><div class="t"><div class="c"></div></div></div>');
			$('.cornflex.' + settings.boxname).append('<div class="r"></div><div class="b"></div><div class="l"></div>');
		//});
		
		/*
		 * Defining specific styles for that box (according to the css: http://dev.pnumb.com/cornflex/wiki/WikiStart#Specificclass)
		 */
		
        if (settings.width > 0) {
            $('.cornflex').css('width', (settings.width) + 'px');
        }
//		
//        $('.cornflex.' + settings.boxname).css('padding', '0 ' + (settings.omega + settings.beta) + 'px ' + (settings.omega + settings.gamma) + 'px 0');
//		
//		$('.cornflex.' + settings.boxname + ' .t').css('background-image', 'url(' + settings.image_t + ')');
//		
//		$('.cornflex.' + settings.boxname + ' .t .c').css('padding-top', settings.alpha + 'px');
//		$('.cornflex.' + settings.boxname + ' .t .c').css('padding-left', settings.delta + 'px');
//		$('.cornflex.' + settings.boxname + ' .t .c').css('position', 'relative');
//		$('.cornflex.' + settings.boxname + ' .t .c').css('top', (settings.omega / 2) + 'px');
//		$('.cornflex.' + settings.boxname + ' .t .c').css('left', (settings.omega / 2) + 'px');

//      $('.cornflex.' + settings.boxname + ' .r, .cornflex.' + settings.boxname + ' .b').css('width', (settings.omega + settings.beta) + 'px');
//		
//		$('.cornflex.' + settings.boxname + ' .b, .cornflex.' + settings.boxname + ' .l').css('height', (settings.omega + settings.gamma) + 'px');
//		
//		$('.cornflex.' + settings.boxname + ' .r').css('bottom', (settings.omega + settings.gamma) + 'px');
//		$('.cornflex.' + settings.boxname + ' .r').css('background-image', 'url(' + settings.image_r + ')');
//		
//		$('.cornflex.' + settings.boxname + ' .b').css('background-image', 'url(' + settings.image_b + ')');
//		
//		$('.cornflex.' + settings.boxname + ' .l').css('right', (settings.omega + settings.beta) + 'px');
//		$('.cornflex.' + settings.boxname + ' .l').css('background-image', 'url(' + settings.image_l + ')');
//		
//		$('* html .cornflex.' + settings.boxname + ' .l').css('width', "expression(this.parentNode.offsetWidth - " + (settings.omega + settings.beta) + " + 'px')");
//		
//		$('* html .cornflex.' + settings.boxname + ' .r').css('height', "expression(this.parentNode.offsetHeight - " + (settings.omega + settings.gamma) + " + 'px')");
//	
		return $(this);
	
	};
})(jQuery);
