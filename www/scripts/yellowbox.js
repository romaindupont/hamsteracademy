$(document).ready(function(){
        $("div.blocRounded").cornflex('yellowbox', {
                omega: ((20)),                   // rounded corner's radius
                image_t: 'images/cadres/yellowbox_t.png',      // path to top image (relative OR url)
                image_r: 'images/cadres/yellowbox_r.png',      // right image
                image_b: 'images/cadres/yellowbox_b.png',      // bottom image
                image_l: 'images/cadres/yellowbox_l.png',      // left image
                alpha: ((12)),                   // alpha value (cf. schema)
                beta: ((18)),                    // beta value
                gamma: ((24)),                   // gamma value
                delta: ((18))                    // delta value
                //width: ((270))
        });
});