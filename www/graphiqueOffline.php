<?php 

if ( ! defined('NO_OUTPUT_CAGE') ) {
    
    if ( ! defined('IN_HT') ) {
        define('IN_HT', true);

        include "common.php";
        include "gestion.php";
        include "lstAccessoires.php";
    }
}
else
    require_once "lstAccessoires.php";

function addImage(&$arrayDessins, &$arrayZindex,$zIndex,$src,$width=-1,$height=-1,$posX=0,$posY=0,$echelle=1) {
 
    array_push($arrayZindex,$zIndex);   
    array_push($arrayDessins, array($src,$width,$height,$posX, $posY,$echelle));
}

function imagecopymerge_alpha($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h, $pct){
    if(!isset($pct)){
        return false;
    }
    $pct /= 100;
    // Get image width and height
    $w = imagesx( $src_im );
    $h = imagesy( $src_im );
    // Turn alpha blending off
    imagealphablending( $src_im, false );
    // Find the most opaque pixel in the image (the one with the smallest alpha value)
    $minalpha = 127;
    for( $x = 0; $x < $w; $x++ )
    for( $y = 0; $y < $h; $y++ ){
        $alpha = ( imagecolorat( $src_im, $x, $y ) >> 24 ) & 0xFF;
        if( $alpha < $minalpha ){
            $minalpha = $alpha;
        }
    }
    //loop through image pixels and modify alpha for each
    for( $x = 0; $x < $w; $x++ ){
        for( $y = 0; $y < $h; $y++ ){
            //get current alpha value (represents the TANSPARENCY!)
            $colorxy = imagecolorat( $src_im, $x, $y );
            $alpha = ( $colorxy >> 24 ) & 0xFF;
            //calculate new alpha
            if( $minalpha !== 127 ){
                $alpha = 127 + 127 * $pct * ( $alpha - 127 ) / ( 127 - $minalpha );
            } else {
                $alpha += 127 * $pct;
            }
            //get the color index with new alpha
            $alphacolorxy = imagecolorallocatealpha( $src_im, ( $colorxy >> 16 ) & 0xFF, ( $colorxy >> 8 ) & 0xFF, $colorxy & 0xFF, $alpha );
            //set pixel with the new color + opacity
            if( !imagesetpixel( $src_im, $x, $y, $alphacolorxy ) ){
                return false;
            }
        }
    }
    // The image copy
    imagecopy($dst_im, $src_im, $dst_x, $dst_y, $src_x, $src_y, $src_w, $src_h);
} 

function colordecode($hex){

   $code[0] = hexdec(substr($hex, 0 ,2));
   $code[1] = hexdec(substr($hex, 2 ,2));
   $code[2] = hexdec(substr($hex, 4 ,2));

   return $code;

} // end func colordecode

function drawImagesCage($imageFinale,&$lstDessins,&$zIndexDessins){
    
    $nbDessins = sizeof($zIndexDessins);
    array_multisort($zIndexDessins,$lstDessins);
    
    for($d=0;$d<$nbDessins;$d++) {
        
        if (substr($lstDessins[$d][0],-3) == "png" || $lstDessins[$d][5] != 1){
            // on récupère la version 8-bits
            $cheminfichier = $lstDessins[$d][0];
            $cheminfichier = str_replace("images/cage/","images/cage/version_gif/",$cheminfichier,$count) ;
            if ($count == 0)
                $cheminfichier = str_replace("images/","images/cage/version_gif/",$cheminfichier) ;
            $cheminfichier = str_replace(".png",".gif",$cheminfichier) ;
            $imageTmp = imagecreatefromgif($cheminfichier);
        }
        else
            $imageTmp = imagecreatefromgif($lstDessins[$d][0]);
        
        imagealphablending($imageTmp, false); // setting alpha blending on
        imagesavealpha($imageTmp, true); // save alphablending setting (important)

        if ($lstDessins[$d][1] == -1)
            $lstDessins[$d][1] = imagesx($imageTmp);
        if ($lstDessins[$d][2] == -1)
            $lstDessins[$d][2] = imagesy($imageTmp);
        
        if (imageistruecolor($imageTmp)) {

            //imagecopy($imageFinale,$imageTmp,$lstDessins[$d][3],$lstDessins[$d][4],0,0,$lstDessins[$d][1],$lstDessins[$d][2]);
            //imagecopymerge_alpha($imageFinale,$imageTmp,$lstDessins[$d][3],$lstDessins[$d][4],0,0,$lstDessins[$d][1],$lstDessins[$d][2],100);
            imagecopymerge($imageFinale,$imageTmp,$lstDessins[$d][3],$lstDessins[$d][4],0,0,$lstDessins[$d][1],$lstDessins[$d][2],100);
        }
        else
            imagecopymerge($imageFinale,$imageTmp,$lstDessins[$d][3],$lstDessins[$d][4],0,0,$lstDessins[$d][1],$lstDessins[$d][2],100);
        //($imageFinale,$imageTmp,$lstDessins[$d][3],$lstDessins[$d][4],0,0,$lstDessins[$d][1],$lstDessins[$d][2]);
        imagedestroy($imageTmp);
    }
}

function rebuildCache($cageRow, $lstAccessoiresDeLaCage, $nbAccessoires) {

	global $lstTypesCages, $dbHT, $taillePixelColonne, $taillePixelProfondeur,  $offsetDecalageDuLeftPourChaqueProfondeur;
	global $tailleBordureCageGauche, $tailleBordureCageBas, $ratioInclinaison3D, $infosCage ;
    
    $lstDessins = array();
    $zIndexDessins = array();

	$tailleCage = $cageRow['colonnes'];
	
	$largeurImageCage = $infosCage[$tailleCage][INFO_CAGE_LARGEUR_IMAGE];
	$hauteurImageCage = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_IMAGE];
	$hauteurImageGrille = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_GRILLE];
	$largeurBac = $infosCage[$tailleCage][INFO_CAGE_LARGEUR_BAC];
	$hauteurBac = $infosCage[$tailleCage][INFO_CAGE_HAUTEUR_BAC];
	$ratioInclinaison = $infosCage[$tailleCage][INFO_CAGE_RATIO];
	
	$extensionImages = ".png";
    
    $imageFinale = imagecreatetruecolor($largeurImageCage, ($cageRow['etages']+1)*$hauteurImageGrille+$hauteurBac);
    //$imageFinale = imagecreatefrompng("images/cage/grille_etage_taille1_bleu.png");
    $couleur = imagecolorallocatealpha($imageFinale,255,255,255,50);
    imagealphablending($imageFinale, false); // setting alpha blending on
    imagesavealpha($imageFinale, true); // save alphablending setting (important)
    imagefill($imageFinale,0,0,$couleur);
        
	$couleurImage = "_bleu";
	if ($cageRow['fond'] > 0) {
		if ($cageRow['fond'] == FOND_CAGE_ROSE)
			$couleurImage = "_rose";
		else if ($cageRow['fond'] == FOND_CAGE_BLANC)
			$couleurImage = "_blanc";
		else if ($cageRow['fond'] == FOND_CAGE_NOIR)
			$couleurImage = "_noir";
        else if ($cageRow['fond'] == FOND_CAGE_COEUR)
            $couleurImage = "_coeur";
        else if ($cageRow['fond'] == FOND_CAGE_FEU)
            $couleurImage = "_feu";            
        else if ($cageRow['fond'] == FOND_CAGE_HALLOWEEN)
            $couleurImage = "_halloween";            
        else if ($cageRow['fond'] == FOND_CAGE_MER)
            $couleurImage = "_mer";            
        else if ($cageRow['fond'] == FOND_CAGE_NUIT)
            $couleurImage = "_nuit";
        else if ($cageRow['fond'] == FOND_CAGE_NEIGE)
            $couleurImage = "_neige";
	}    
	
	//$dessinCage .= "<div class=\"blocCage\" style=\"width:".$largeurImageCage."px;\">\n";
	
	// dessine le toit
	// ---------------
	//$dessinCage .= "<div class=\"blocEtageCageToit\" style=\"z-index:1000000;\">" ;
	//$dessinCage .= "<img src=\"images/cage/grille_top_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"".$hauteurImageGrille."\" alt=\"\" />" ;
	//$dessinCage .= "</div>";
    
    addImage($lstDessins,$zIndexDessins,1000000,"images/cage/grille_top_taille".$tailleCage.$couleurImage.$extensionImages,$largeurImageCage,$hauteurImageGrille);
	
	// dessine chaque etage
	// --------------------
	for($etage=$cageRow['etages']-1;$etage>=0;$etage--)  {
	
		//$dessinCage .= "\n<div class=\"blocEtageCage\" id=\"etage_$etage\" " ;
		// on calcule le z-index, de sorte que les etages superieurs soient devant
		$zIndexEtage = 2+($etage*$largeurImageCage*($hauteurImageCage+100)) ;
		
        $posYInitiale = ($cageRow['etages']-1-$etage)*$hauteurImageGrille+$hauteurImageGrille;
        
		// etage du bac
		if ($etage == 0) {
			//$dessinCage .= "style=\"border:0px blue solid; height:".($hauteurImageCage)."px ; z-index:".$zIndexEtage ." ; top: 10px;\" >\n" ;
			//$dessinCage .= "<div class=\"textureFond\" ";
			//$dessinCage .= "style=\"border:0px red solid; height:".($hauteurImageCage)."px ; \" " ;
			//$dessinCage .= "><img src=\"images/cage/fond_cage_copeaux_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"100%\" alt=\"\" /></div>";
            addImage($lstDessins,$zIndexDessins,$zIndexEtage,"images/cage/fond_cage_copeaux_taille".$tailleCage.$couleurImage.$extensionImages,$largeurImageCage,-1,0,0+$posYInitiale);
		}
		else { // etage normal
			//$dessinCage .= "style=\"border:0px blue solid; height:".$hauteurImageGrille."px ; z-index:".$zIndexEtage ." ; top: 10px;margin-bottom:10px;\">\n" ;
			//$dessinCage .= "<div class=\"textureFond\" ";
			//$dessinCage .= "style=\"border:0px red solid; height:".$hauteurImageGrille."px ; \" " ;
			//$dessinCage .= "><img src=\"images/cage/grille_etage_taille".$tailleCage.$couleurImage.$extensionImages."\" width=\"".$largeurImageCage."\" height=\"100%\"alt=\"\" /></div>";	
            addImage($lstDessins,$zIndexDessins,$zIndexEtage,"images/cage/grille_etage_taille".$tailleCage.$couleurImage.$extensionImages,$largeurImageCage,-1,0,0+$posYInitiale);
		}
		
		for($ac=0;$ac<$nbAccessoires;$ac++) {
			$rowAcc = $lstAccessoiresDeLaCage[$ac];
			
			if ($rowAcc['cage_id'] == $cageRow['cage_id'] && $rowAcc['etage'] == $etage) {
			
				// cas special pour l'ecuelle : si elle est pleine, on met l'image pleine.
				if ($rowAcc['type'] == ACC_ECUELLE){
					$rowAcc['image'] = "ecuelle_pleine_reduc.gif";
				}
                else if ($rowAcc['type'] == ACC_PISCINE && $cageRow['fond'] == FOND_CAGE_NEIGE) {
                    $rowAcc['image'] = "piscine2_neige.gif";
                }
                
                if ($rowAcc['type'] == ACC_CRISTAL_NEIGE)
                    continue;
							
				// calcul des coordonnées de chaque objet dans la cage
				$posTop = $rowAcc['posY'];
				$posLeft = $rowAcc['posX'];
				
				$imgWidth = intval($rowAcc['img_width'] * $rowAcc['echelle']) ;
				$imgHeight = intval($rowAcc['img_height'] * $rowAcc['echelle']) ;
				
				// en fonction du size ratio, deplacer l'objet
					
				// on calcule le z-index, de sorte que les objets plus bas soient devant et que les etages superieurs soient devant
				$zIndexAccessoire = 2+($etage*$largeurImageCage*($hauteurImageCage+100))+ $largeurImageCage*(100+($posTop+$imgHeight)) ;	
				//$dessinCage .= "\n<div class=\"cageAccessoire\" id=\"acc_".$rowAcc['accessoire_id']."\"";
				//$dessinCage .= " style=\"width:".($imgWidth)."px;height:".($imgHeight)."px;left:".($posLeft)."px; top:".($posTop)."px; z-index:".$zIndexAccessoire." ;  ";
				//$dessinCage .= "\"";

				addImage($lstDessins,$zIndexDessins,$zIndexAccessoire,"images/".$rowAcc['image'],$imgWidth,$imgHeight,$posLeft,$posTop+$posYInitiale,$rowAcc['echelle']);
			}
		}
	}
    
    drawImagesCage($imageFinale,$lstDessins,$zIndexDessins);
    
    $width = $largeurImageCage;
    $height = ($cageRow['etages']+1)*$hauteurImageGrille+$hauteurBac;
    $newwidth = $width /2;
    $newheight = $height /2;

    // Load
    $thumb = imagecreatetruecolor($newwidth, $newheight);
    imagefill($thumb,0,0,$couleur);
    imagecopyresampled ($thumb, $imageFinale, 0, 0, 0, 0, $newwidth, $newheight, $width, $height);
    
    imagejpeg($thumb,"images/cage/joueurs/".$cageRow['cage_id'].".jpg",100);
    imagepng($thumb,"images/cage/joueurs/".$cageRow['cage_id']."_full.png");
    imagepng($imageFinale,"images/cage/joueurs/".$cageRow['cage_id']."_full_full.png");
    imagedestroy($imageFinale);
    imagedestroy($thumb);
    
    $query = "UPDATE cage SET rebuildCache = 0 WHERE cage_id=".$cageRow['cage_id'];
    $dbHT->sql_query($query);
}

$cage_id = -1;

if (isset($_GET['cage_id']))
    $cage_id = intval($_GET['cage_id']);
else if (isset($_POST['action_id']))
    $cage_id = intval($_POST['action_id']);
else if (isset($_GET['action_id']))
    $cage_id = intval($_GET['action_id']);
    
// on recupere la cage
$query = "SELECT * FROM cage WHERE cage_id='".$cage_id."' LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;

if ($nbCages != 1) {
    
    if ( defined('NO_OUTPUT_CAGE') )
        return ;
        
    $imageCage = imagecreatefrompng("images/cage_reduc2.png");
    header('Content-type: image/png');
    imagepng($imageCage);
    imagedestroy($imageCage);
}
else {
    $cage=$dbHT->sql_fetchrow($result);
    $cage['nbAccessoires'] = 0;
    $dbHT->sql_freeresult($result);
    error_reporting(E_ALL);
    
    // doit-on refaire le build cache ?
    if ($cage['rebuildCache'] == 1) {
                
        // liste des accessoires pour les cages
        // ------------------------------------
        $query = "SELECT a.*, c.* FROM accessoires a, config_accessoires c WHERE a.cage_id=".$cage_id." AND a.type = c.type";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        $nbAccessoires = $dbHT->sql_numrows($result) ;
        $lst_accessoires=array();
        $cage['nbAccessoires'] = 0;
        while($row=$dbHT->sql_fetchrow($result)) {
            array_push($lst_accessoires,$row);
            $cage['nbAccessoires'] ++ ;
        }
        $dbHT->sql_freeresult($result);
        
        rebuildCache($cage, $lst_accessoires, $nbAccessoires);
    }

    if ( defined('NO_OUTPUT_CAGE') )
        return ;
    
    if (! isset($_GET['full'])){
        $imageCage = imagecreatefromjpeg("images/cage/joueurs/".$cage['cage_id'].".jpg");
        header('Content-type: image/jpeg');
        imagejpeg($imageCage);
    }
    else{
        $imageCage = imagecreatefrompng("images/cage/joueurs/".$cage['cage_id']."_full.png");
        header('Content-type: image/png');
        imagepng($imageCage);
    }

    imagedestroy($imageCage);
}
?>