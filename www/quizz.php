<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";
include "lstMessages.php";
include "lstQuizz.php";

$userdata = session_pagestart($user_ip);
	
if( ! $userdata['session_logged_in'] ) { 
	echo "Erreur de connection";
	redirectHT("index.php?err=1",3);
}

// on récupère l'adresse d'où on vient
$pagePrec = 0;
if (isset($_GET['precedentePage'])) 
	$pagePrec = intval($_GET['precedentePage']);
else if (isset($_GET['DATAS'])) 
	$pagePrec = intval($_GET['DATAS']);

$pagePrecUrl = "jeu.php?m_accueil"; // par défaut
if ($pagePrec != 0) {
	if ($pagePrec < -1) 
		$pagePrecUrl = "jeu.php?mode=m_achat&objet=".(-1*$pagePrec); // si le joueur voulait acheter un objet, le type de cet objet est la valeur absolue de pagePrec
	else if ($pagePrec == PAGE_JEU_CAGE)
		$pagePrecUrl = "jeu.php?mode=m_cage";
	else if ($pagePrec == PAGE_JEU_ACHAT)
		$pagePrecUrl = "jeu.php?mode=m_achat";
	else if ($pagePrec == PAGE_JEU_ACHAT_PIECE)
		$pagePrecUrl = "jeu.php?mode=m_achat&rayon=9";		
}

$erreur = -1;
$msg = "";

// a-t-il déjà joué ?
$dejaJouer = false;
$dateDuJour = getDate(time());
$codeJour = $dateDuJour['mday'] + 100*$dateDuJour['mon'] ;

$codeJourDernierQuizz = $userdata['dernier_quizz'] ;

if ($codeJour == $codeJourDernierQuizz) {
	$dejaJouer = true;	
	$msg = "<strong>Désolé, tu as déjà joué aujourd'hui, il faut attendre demain pour retenter.</strong>";
	$msg .= "<br/>&nbsp;<br/><form method=get action=\"banque.php\"><table><tr><td>Si tu veux gagner des pièces ".IMG_PIECE." sans attendre, tu peux passer à la banque </td>";
	$msg .= "<td><input type=submit value=\"Aller à la banque\"></td></tr></table></form>";
}

// le joueur a-t-il répondu ?
if (isset($_POST['reponse']) && isset($_POST['question'])) {
	$reponseJoueur = intval($_POST['reponse']);
	$question = intval($_POST['question']);
	
	// a-t-il droit de jouer ?
	if (! $dejaJouer) {
		$msg = "A la question : ".$lstQuizz[$question][QUIZZ_QUESTION].", tu as répondu : ".$lstQuizz[$question][$reponseJoueur].".<br/>&nbsp;<br/>";
	
		// bonne réponse ?
		if ($reponseJoueur == $lstQuizz[$question][QUIZZ_BONNE_REPONSE]) {
			// bonne réponse !
			$msg .= "<strong>Bravo, tu as bien répondu. ".$lstQuizz[$question][QUIZZ_COMMENTAIRE]."</strong><br/>&nbsp;<br/>" ;
			$msg .= "<strong>Tu gagnes 5 ".IMG_PIECE." !</strong>";
			crediterPieces(5,false);
		}
		else { // mauvaise réponse
			$msg .= "<strong>Désolé, ce n'est pas la bonne réponse... Tu peux réessayer demain !</strong><br/>" ;
		}
		
		// MAJ du jour du dernier quizz
		$query = "UPDATE joueurs 
			SET dernier_quizz = '".$codeJour."'
			WHERE joueur_id='".$userdata['joueur_id']."'";
		if ( !($dbHT->sql_query($query)) ){
			message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
		}
		$userdata['dernier_quizz'] = $codeJour ;
		
	}
}

?>

<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
<title>Hamster Academy - Quizz</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta name="description" content="Hamster Academy, Eleve un hamster, Fabrique sa cage, Et vit à sa place, Jeu de role, jeu gratuit ">
<meta name="keywords" content="Hamster, Academy, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer">
<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
<link rel="stylesheet" href="css/style.css" type="text/css">
<link rel="stylesheet" href="css/styleEleveur.css" type="text/css">
</head>

<body>

	<h1>Hamster Academy - Quizz</h1>
	
	<img src="images/quizz.gif" alt="">
	
<div class=Quizz>
	<h3>Principe du Quizz : </h3>
	
	<u>Une fois par jour</u>, tu peux répondre à une question du quizz.<br>Si la réponse est correcte, tu récupères 5 <?php echo IMG_PIECE ;?>.<br/>Sinon, tu peux recommencer le lendemain.<br/>&nbsp;<br/>
	
	<?php 
	
	if (isset($_POST['reponse'])) {
		echo $msg ;
	}
	else if ($dejaJouer){
		echo $msg;
	}
	else {
		echo "Pour gagner les 5 ".IMG_PIECE.", réponds à la question : <br/>&nbsp;<br/>";
		
		// choix aléatoire de la question (toujours la même chaque jour, et propre à chaque joueur) : 
		srand($codeJour + $userdata['joueur_id']);
		$question = rand(0,$nbQuizz-1);
		echo "<form method=post action=\"quizz.php\">";
		
		echo $lstQuizz[$question][QUIZZ_QUESTION]." ? <br/>";
		
		echo "<div class=quizzReponse>";
		
		echo "<input type=radio name=reponse value=1 CHECKED>".$lstQuizz[$question][QUIZZ_REPONSE1]."<br/>";
		echo "<input type=radio name=reponse value=2>".$lstQuizz[$question][QUIZZ_REPONSE2]."<br/>";
		echo "<input type=radio name=reponse value=3>".$lstQuizz[$question][QUIZZ_REPONSE3]."<br/>";
		echo "<input type=radio name=reponse value=4>".$lstQuizz[$question][QUIZZ_REPONSE4]."<br/>&nbsp;<br/>";
		
		echo "<input type=hidden name=question value=\"".$question."\">";
		echo "<input type=submit value=\"Valider la réponse\">";
		
		echo "</div>";
		
		echo "</form>";
	}
	
	?>
	
</div>
	
<br/>&nbsp;<br/>
<a href="<?php echo $pagePrecUrl ; ?>">Revenir au jeu</a>

<br/>&nbsp;<br/>

<div align=center style="font-size :small  ;">&copy; Hamster Academy - Tous droits réservés</div>

</div> 
</div> 

<script src="http://www.google-analytics.com/urchin.js" type="text/javascript">
</script>
<script type="text/javascript">
_uacct = "UA-2189291-1";
urchinTracker();
</script>

</body>
</html>

<?php $dbHT->sql_close(); ?>
