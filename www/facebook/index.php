<?php

define('IN_HT', true);
define('NO_SMARTY',true);
include "../common.php";
include "../facebook.php";
include "../gestion.php";

// on r�cup�re les informations du visiteur
 try {
    $compteFacebook = $facebook->api('/me');
} catch (FacebookApiException $e) {
    error_log($e);
}

?>
<!DOCTYPE html>
<!--  on va utilisser du fbml ( le html de facebook) -->
<html xmlns='http://www.w3.org/1999/xhtml'
      xmlns:fb='http://www.facebook.com/2008/fbml'>
  <body>

  <?php echo "Bienvenue dans l'application Facebook officielle de Hamster Academy !<br/>&nbsp;<br/>" ;?>
  
  <!--  si le client a accept� l'application -->
    <?php if ($sessionFacebook) {
        $joueur_id = getJoueurIdFromFacebookId($compteFacebook['id']) ;
        if ($joueur_id == -1){
            echo "Tu n'as pas de compte dans Hamster Academy associ� avec ton compte Facebook (".$compteFacebook['name'].")";
            echo "<br/>Pour cela, il faut :";
            echo "<li><ul>avoir un compte dans Hamster Academy</ul>";
            echo "<ul>et associer ce compte avec le compte Facebook : on peut le faire dans \"Mon Compte\" dans le jeu</ul></li>";
        }
        else {
            
            $query = "SELECT * FROM joueurs WHERE joueur_id = $joueur_id";
            if ( !($resultUserdata = $dbHT->sql_query($query)) )
            {
                message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $query);
            }
            
            $userdata = $dbHT->sql_fetchrow($resultUserdata);
            $dbHT->sql_freeresult($resultUserdata);
            
            echo "Bienvenue ".utf8_decode($userdata['pseudo'])." !";
            echo "<br/>&nbsp;<br/>";
            
            // liste des cages
            // ----------------------
            $query = "SELECT * FROM cage WHERE joueur_id=".$userdata['joueur_id'];
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
            }
            $nbCages = $dbHT->sql_numrows($result) ;
            $lst_cages=array();
            while($row=$dbHT->sql_fetchrow($result)) {
                array_push($row,0);
                $row['nbAccessoires'] = 0;
                array_push($lst_cages,$row);
            }
            $dbHT->sql_freeresult($result);
            
            if ($nbCages == 0)
                echo "Ta cage :" ;
            else
                echo "Tes cages :" ;
            echo "<br/>&nbsp;<br/>";

            echo "<div align=\"center\">";
            for($cageIndex=0;$cageIndex<$nbCages;$cageIndex++) {
                
                $lienImage = "http://www.hamsteracademy.fr/graphiqueOffline.php?cage_id=".$lst_cages[$cageIndex]['cage_id']."&amp;full=1";
                $nomCage = utf8_decode($lst_cages[$cageIndex]['nom']);
                
                echo "<img src=\"".$lienImage."\" /><br/>";
                echo "<strong>".$nomCage."</strong><br/>";
                
                if (isset($_GET['publishCage']) && is_numeric($_GET['publishCage'])) {

                    $cageId_demandee = $_GET['publishCage'];
                    
                    
                    if ($cageId_demandee == $lst_cages[$cageIndex]['cage_id']) {

                        $message = "Cage : ".$nomCage;

//                        $attachment = array( 
//                            'access_token' => $access_token,
//                            'name' => $nomCage,
//                            'href' => $nomCage,
//                             'caption' => $nomCage, 'description'
//                             => $nomCage, 
//                             'media' => array(array('type' => 'image', 'src' =>
//                             $lienImage,
//                             'href' => $lienImage)));
                        
                        try
                        {
                            //$retour = $facebook->api('/me/feed', 'post', array( 'message' => $message, 'source' => $lienImage ) ) ;
                            $args = array(
                              'message' => $message,
                            );
                            //$args['pic.png'] = '@' . $lienImage;
                            $nomArg = $nomCage.'.png';
                            $args[] = '@'.realpath('../images/cage/joueurs/'.$cageId_demandee.'_full_full.png');
                            
                            //print_r($args);

                            $tok = $sessionFacebook['access_token'];
                            $url = 'https://graph.facebook.com/me/photos?access_token='.$tok;
                            $ch = curl_init();
                            curl_setopt($ch, CURLOPT_URL, $url);
                            curl_setopt($ch, CURLOPT_HEADER, false);
                            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                            curl_setopt($ch, CURLOPT_POST, true);
                            curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
                            //print_r (curl_getinfo($ch));
                            $data = curl_exec($ch);
                            //print_r (curl_getinfo($ch));
                            curl_close($ch);
                            //print_r(json_decode($data,true));

                            echo "Cage publi�e ! ";
                        }
                        catch (Exception $e)
                        {
                            //echo $e;
                            $client_id    = CLE_PUBLIQUE;
                            $display      = 'page';
                            $scope         = 'publish_stream';
                            $redirect_url = "http://apps.facebook.com/hamsteracademy/";
                            $oauth_url    = 'https://graph.facebook.com/oauth/authorize?client_id=' . $client_id . '&redirect_uri=' . $redirect_url . '&type=web_server&display=' . $display . '&scope=' . $scope;
                            $text = "<script type=\"text/javascript\">\ntop.location.href = \"$oauth_url\";\n</script>";
                            echo $text;
//                            
//                            $facebook->api('/me/feed', 'post', array( 'message' => $message )) ;
                        }
                        
                    }
                }
                
                echo "<a href=\"index.php?publishCage=".$lst_cages[$cageIndex]['cage_id']."\">";
                echo "Publier sur mon mur"; 
                echo "</a>";
                echo "<br/>&nbsp;<br/>"; 
            }
            echo "</div>";
        }
     
     
    } else { ?> <!--  sinon on propose � l'utilisateur de se connecter � l'application -->
     Pour se connecter c'est ici :
      <fb:login-button></fb:login-button>
    <?php } ?>

    <div id='fb-root'></div>
    <!--  on charge la librairie javascript de facebook pour la connexion -->
    <script src='http://connect.facebook.net/en_US/all.js'></script>
     <!--  fonction qui envoie les informations � facebook connect -->
    <script>
      FB.init({appId: '<?= CLE_PUBLIQUE ?>', status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
        window.location.reload();
      });
    </script>
  </body
</html>
