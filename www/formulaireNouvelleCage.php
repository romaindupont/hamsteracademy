<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$erreur = 0;
$erreurMsg = "";
    
$nomCage = T_("Ma nouvelle cage");
$nbColonnes = 1;
$nbProfondeurs = 1;
$nbEtages = 1;

$coutCage = round($coutCube*$nbEtages*$nbColonnes*$nbColonnes);

if (isset($_GET['validerAchatCage'])) {

    if (isset($_GET['nomCage'])) 
        $nomCage=mysql_real_escape_string($_GET['nomCage']);
            
    // verif pseudo
    if ($nomCage == "" || strlen($nomCage) < 3)
        $erreur = 1;
    else {    
        $resultatAchat = debiterPieces($coutCage,"achat de cage") ;
    
        if ($resultatAchat == 0) {
            $erreur = 2;
        }
        else {
            $cage_id = ajouterCageDansBDD($userdata['joueur_id'],0, $nbEtages, 0, $propreteInscription, 0, $nbColonnes, $nbProfondeurs, $nomCage);
            redirectHT("jeu.php?mode=m_cage&cage_id=".$cage_id."&nouvelleCage=1");
        }
    }
}

if ($erreur == 1) { // mauvais nom de cage
    echo "<p>".T_("Attention, il faut choisir un nom de cage un petit peu plus long")."...</p>\n";
}
else if ($erreur == 2) { // pas assez d'argent
    $msgTmp = "<div align=\"center\">".T_("Tu n'as pas assez d'argent pour acheter la cage :");
    echo afficherPasAssezArgent($msgTmp,PAGE_JEU_ACHAT);
    echo "<br><a href=\"jeu.php?mode=m_achat&rayon=".RAYON_CAGE."\">".T_("Retour à la boutique")."</a></div>";
}
else {
?>
<div align="center">
<?php echo T_("Achat d'une nouvelle cage");?><br/><img src="images/cage_reduc.gif" alt="" />

<form method="get" action="jeu.php">
    <input type="hidden" name="validerAchatCage" value="1" />
    <input type="hidden" name="mode" value="m_achat" />
    <input type="hidden" name="rayon" value="<?php echo RAYON_CAGE; ?>" />
    <input type="hidden" name="pagePrecedente" value="<?php echo $pagePrecedente; ?>" />
  <?php echo T_("Nom de la cage :");?> <input type="text" name="nomCage" size="30" value="<?php echo $nomCage ; ?>" />

    <br/>&nbsp;<br/>
    <?php echo T_("Coût de la cage :");?> <span id="coutCage"><?php echo $coutCage ; ?></span><img src="images/piece.gif" alt="pieces" width="25" height="25" style="vertical-align:middle;" />
    <br/>&nbsp;<br/>
    (<?php echo T_("attention, aucun accessoire n'est fourni avec la cage");?>)
    <br/>&nbsp;<br/>
    <input type="submit" value="<?php echo T_("Acheter la cage ! ");?>" />
</form>
</div>
<?php }?>
