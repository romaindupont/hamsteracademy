<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

$maleId = intval($_GET['bebe']) ;

$query = "SELECT sexe,hamster_id FROM hamster WHERE hamster_id=".$maleId." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamstersQuery = $dbHT->sql_numrows($result) ;

if ($nbHamstersQuery == 0){
	$erreurAction = 4;
	return;
}

$maleRow=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

$hamRow = $lst_hamsters[$hamsterIndex];

if ($maleRow['sexe'] != 0) { // le joueur a triché, c'est une femelle
	$erreurAction = 1;
	return ;
}
else if ($hamRow['sexe'] != 1) {
	// on vérifie que la femelle n'est pas déjà enceinte
	$erreurAction = 2;
	return ;
}
// on vérifie qu'il y a bien un nid pour accueillir les bébés
else if (yatilAccessoireDansLaCage($hamRow['cage_id'],ACC_NID,$userdata['joueur_id']) == -1) {
	$erreurAction = 3;
	return ;
}
else {
	// on lance la grossesse
	$hamRow['sexe'] = $dateActuelle;
	$hamRow['experience']++;
	$lst_hamsters[$hamsterIndex]['sexe'] = $dateActuelle;
	$lst_hamsters[$hamsterIndex]['experience']=$hamRow['experience'];
	
	$query = "UPDATE hamster 
		SET sexe = ".$dateActuelle.",
		fecondateur_id = ".$maleRow['hamster_id'].",
		experience = ".$hamRow['experience']."
		WHERE hamster_id=".$hamRow['hamster_id'];
	if ( !($dbHT->sql_query($query)) ){
		message_die(GENERAL_ERROR, 'Error updating hamster grossesse', '', __LINE__, __FILE__, $query);
	}
}
?>
