<?php

if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

// langue du navigateur
$navigateur_lang = substr($_SERVER['HTTP_ACCEPT_LANGUAGE'],0,2);

$langue_choisie = false;
if(isset($_GET['langue_choisie']) || isset($_COOKIE['ha_lang_choisie']))
    $langue_choisie = true;

// on regarde s'il y a un choix de langue explicite
// ------------------------------------------------
$user_lang = 'fr';
if (isset($_GET['lang'])) {
    if ($_GET['lang'] == 'fr') 
        $user_lang = 'fr';
    else if ($_GET['lang'] == 'en') 
        $user_lang = 'en';
    else
        $user_lang = 'en';
}
else {
    if(isset($_COOKIE['lang'])) {
        $user_lang = $_COOKIE['lang'];
    }
    else {
        // si aucune langue n'est déclarée on tente de reconnaitre la langue par défaut du navigateur
        $user_lang = $navigateur_lang; 
    }
}

$server_langue = "fr";
if (substr($_SERVER['SERVER_NAME'],-3) == "com")
    $server_langue = "en";

// si le serveur est .fr, le visiteur aura forcément la page en francais
$lang = "fr";
if ($server_langue != "fr")
    $lang = "en";

    
//définition de la durée du cookie (1 an)
$expire = 31536000 ; // = 365*24*3600; 

//enregistrement du cookie au nom de lang
setcookie("lang", $user_lang, time() + $expire);

if (isset($_GET['langue_choisie']))
    setcookie("ha_lang_choisie", $lang, time() + $expire);
    
// config du serveur
if ($lang == "fr") {
    $board_config['server_name'] = $serveur_url_fr ;
    $base_site_url = "http://".$serveur_url_fr ;
    $serveurForum = $serveurForum_fr; 
    $userForum = $userForum_fr; 
    $passwordForum = $passwordForum_fr; 
    $baseForum = $baseForum_fr; 
    $prefixForum = $prefixForum_fr;
}
else{
    $board_config['server_name'] = $serveur_url_en ;
    $base_site_url = "http://".$serveur_url_en ;
    $serveurForum = $serveurForum_en; 
    $userForum = $userForum_en; 
    $passwordForum = $passwordForum_en; 
    $baseForum = $baseForum_en; 
    $prefixForum = $prefixForum_en;
}

putenv("LANG=$lang" );
define('DEFAULT_LOCALE', 'fr');

// si langue française, on ne fait rien
if ($lang == "fr") {
    
    function T_($text) {
        return $text;
    }
    function _gettext($text) {
        return $text;
    }
}
else {
    
    $textdomain = 'messages';
    
    // utilisation de l'extension gettext de php
    if (1) {
        putenv("LANG=en_US" );
        
        define('PROJECT_DIR', realpath('./'));
        define('LOCALE_DIR', PROJECT_DIR .'/locale');

        $supported_locales = array('"en_US"', 'fr');
        $encoding = 'UTF-8';
        $locale = "en_US";

        setlocale(LC_ALL, "en_US");
        putenv("LANG=en_US" );

        $domain = 'messages';
        bindtextdomain($domain, LOCALE_DIR);
        bind_textdomain_codeset($domain, $encoding);
        textdomain($domain);
        
        function T_($text) {
            return _($text);
        }
        function _gettext($text) {
            return _($text);
        }
    }
    else {
        require_once('php-gettext/gettext.inc');
        
        $supported_locales = array('en', 'fr');
        $encoding = 'UTF-8';
        $locale = $lang;

        // gettext setup
        T_setlocale(LC_ALL, $locale);

        // Set the text domain as 'messages'
        $domain = 'messages';
        T_bindtextdomain($domain, LOCALE_DIR);
        T_bind_textdomain_codeset($domain, $encoding);
        T_textdomain($domain);
    }
}

if ($lang != "fr")
    $mois_txt = $mois_txt_en;
     
?>