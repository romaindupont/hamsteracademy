<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

$dernieresNouveautes = '
<div class="newsBloc"><div class="newsDate">Lundi 4 mai</div>
<div class="newsTxt">- Attention, le v�to ne garde les hamsters que 2 semaines...</div>
</div>
<div class="newsBloc"><div class="newsDate">Samedi 2 mai</div>
<div class="newsTxt">- <a href="options.php?mode=m_vip">Deviens VIP sur Hamster Academy</a> ! </div>
</div>
<div class="newsBloc"><div class="newsDate">Vendredi 1er mai</div>
<div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=6168" title="la Gazette des Mod�rateurs" > D�couvrez la Gazette des Mod�rateurs, n�2 !</a></div>
</div>
';

if (defined ('PLUS_DE_NOUVEAUTES')) {

    $dernieresNouveautes .= '
    <div class="newsBloc"><div class="newsDate">Dimanche 12 avril</div>
    <div class="newsTxt">- <img src="images/oeufs/oeufs1.gif" width="40" alt="Oeufs de P�ques" align="middle" /> Essaye de trouver les 10 oeufs de P�ques cach�s dans le jeu ! Il y a une r�compense pour ceux qui les trouvent !</div>
    <div class="newsTxt">- <img src="images/nouveautes.gif" width="40" alt="Gazette" align="middle" /> <a href="forum/viewtopic.php?id=5769" title="la Gazette des Mod�rateurs" > D�couvrez la Gazette des Mod�rateurs, n�1 !</a></div>
    <div class="newsTxt">- <img src="images/tonneau.gif" width="40" alt="Tonneau S�same" align="middle" /> Un nouvel objet � d�couvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 8 avril</div>
    <div class="newsTxt">- Une nouvelle mission !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Jeudi 2 avril</div>
    <div class="newsTxt">- <img src="images/tennis_red.jpg" width="40" alt="Tennis" align="middle" /> Un nouveau sport pour d�gourdir les jambes et les bras des hamsters ! A d�couvrir en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 29 mars</div>
    <div class="newsTxt">- <img src="images/hamster_choupette_red.jpg" width="40" alt="Hamster Choupette" align="middle" /> <img src="images/hamster_tarzan_red.jpg" width="40" alt="Hamster Touffou" align="middle" /> 2 nouvelles races de hamsters � d�couvrir !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Samedi 28 mars</div>
    <div class="newsTxt">-<img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Concours de la plus belle cage, jusqu\'au 15 avril ! Inscription � partir du 1er avril.</div>
    </div>    
    <div class="newsBloc"><div class="newsDate">Dimanche 1er F�vrier</div>
    <div class="newsTxt">- <img src="images/dessin_concours_jan_2009.gif" alt="Dessin Hamster Academy" style="vertical-align:middle; width:40px;" /> Les plus beaux dessins sont sur Hamster Academy !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 30 Janvier</div>
    <div class="newsTxt">- <img src="images/hamster_gourmand_red.gif" alt="Hamster Gourmand" style="vertical-align:middle; width:40px;" /> Nouvel hamster : le Hamster Gourmand</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Lundi 26 Janvier</div>
    <div class="newsTxt">- Nouvelle mission</div>
    <div class="newsTxt">- Fin du jeu de dessin, r�sultats bient�t !</div>
    </div>    
    <div class="newsBloc"><div class="newsDate">Jeudi 22 Janvier</div>
    <div class="newsTxt">- <img src="images/HamsterGrungeRed.jpg" alt="Hamster Grunge" style="vertical-align:middle; width:40px;" /> Nouvelle race de hamster : le Hamster Grunge ! A d�couvrir d\'urgence en boutique !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 14 Janvier</div>
    <div class="newsTxt">- Nouvelles missions !</div>
    <div class="newsTxt">- Le moral, la force, l\'�nergie... peuvent d�passer 10 selon le niveau du joueur</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Mercredi 7 Janvier</div>
    <div class="newsTxt">- Jeu du mois ! Objectif : le plus beau dessin ou histoire avec dessins. Tu as jusqu\'au 25 janvier pour participer !</div>
    </div>
    <div class="newsBloc"><div class="newsDate">Dimanche 4 Janvier</div>
    <div class="newsTxt">- <img src="images/part_galette.gif" alt="galette des rois" width="30" /> La galette des rois est arriv�e ! Cours vite l\'acheter en Boutique (en Ville) pour tes hamsters ! Attention, c\'est jusqu\'au 20 janvier !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 3 janvier</div>
    <div class="newsTxt">- <img src="images/bac_reduc2.gif" alt="bac fourre-tout" width="20" /> Un inventaire plus pratique est enfin disponible !</div>
    <div class="newsTxt">- <img src="images/bac_reduc2.gif" alt="bac fourre-tout" width="20" /> Via l\'inventaire, on peut donner les objets de son choix � ses amis !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 2 janvier</div>
    <div class="newsTxt">- <img src="images/energie.gif" alt="�nergie" width="20" /> Barre d\'�nergie pour les activit�s sportives : chaque activit� coute de l\'�nergie qui se recharge avec le temps.</div>
    <div class="newsTxt">- <img src="images/fiole.gif" alt="fiole d �nergie" width="20" /> Permet de donner de l\'�nergie au hamster ! A d�couvrir en boutique !</div>
    <div class="newsTxt">- Tous les membres d\'un groupe peuvent �diter la description du groupe (sauf si le leader refuse)</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 25 d�cembre</div>
    <div class="newsTxt">- <img src="images/plante.gif" alt="plante verte" width="20" /> Nouvel objet � d�couvrir en boutique pour la cage : une plante d interieur !</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Pour ceux qui avaient achet� des cadeaux en boutique, ils sont arriv�s !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 22 d�cembre</div>
    <div class="newsTxt">- Affichage du classement !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 21 d�cembre</div>
    <div class="newsTxt">- <b>Nouveau calcul des scores</b> pour les groupes. Attention, les scores des hamsters et des joueurs peuvent baisser!</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 15 d�cembre</div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> <span '.tooltip("Cage du vainqueur superleo : &lt;br/&gt; &lt;img src=images/cage_superleo_red2.jpg style=&quot;max-width:400px;&quot;&gt;").' >R�sultats du championnat de la plus belle cage !</span> <a href="#" onclick="javascript: pop(\'afficheCage.php?cage_id=61457\',null,600,800); return false;" title="Voir la cage">Voir la cage du vainqueur!</a></div>
    </div>    
    <div class="newsBloc">
    <div class="newsDate">Jeudi 9 d�cembre</div>
    <div class="newsTxt">- d�but des votes de la plus belle cage !</div>
    <div class="newsTxt">- nouveau calcul des scores</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 04 d�cembre</div>
    <div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de No�l � mettre au pied du sapin sont arriv�s !</div>
    <div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de No�l sont arriv�s ! Va vite les d�couvrir dans la boutique !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 1er d�cembre : </div>
    <div class="newsTxt">- <img src="images/calendrier_avent_red.jpg" style="vertical-align:middle;width:50px;" alt="Calendrier de l\'Avent" /> Le calendrier de l\'Avent est l� ! A d�couvrir en boutique !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 29 novembre : </div>
    <div class="newsTxt">- Nouvelle mission de l\'hiver...</div>
    </div>    
    <div class="newsBloc">
    <div class="newsDate">Samedi 22 novembre : </div>
    <div class="newsTxt">- Lancement du jeu de la plus belle cage !</div>
    <div class="newsTxt">- Groupes : nouvelle pr�sentation, possibilit� de mettre des images plus grandes</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 13 novembre : </div>
    <div class="newsTxt">- <img src="images/texture_feu_red.gif" width="30" alt="feu" align="middle" /> <img src="images/texture_halloween_red.gif" width="30" alt="halloween" align="middle" /> Nouvelle peinture pours les cages&nbsp;! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Lundi 27 octobre : </div>
    <div class="newsTxt">- <img src="images/texture_coeur_red.gif" width="30" alt="coeur" align="middle" /> Nouvelle peinture pours les cages&nbsp;! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 21 septembre: </div>
    <div class="newsTxt">- Jeu du hamster le plus heureux ! Inscription en Ville !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 17 septembre: </div>
    <div class="newsTxt">- D�crit ton groupe de musique avec plus d\'options !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 9 septembre: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> <span '.tooltip("Cage du vainqueur louliou : &lt;br/&gt; &lt;img src=images/cage_louliou_red.png style=&quot;max-width:400px;&quot;&gt;").' >R�sultats du jeu de la plus belle cage !</span> <a href="#" onclick="javascript: pop(\'afficheCage.php?cage_id=4438\',null,600,800); return false;" title="Voir la cage">Voir la cage du vainqueur!</a></div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 19 ao�t : </div>
    <div class="newsTxt">- Indique ton blog !</div>
    <div class="newsTxt">- Ecris des messages plus sympas � la Poste !</div>
    <div class="newsTxt">- Indique tes amis !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 16 juillet : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Jeu de la plus belle cage jusqu\'au 18 aout ! </div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Samedi 5 juillet : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Jeu de slogans !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mercredi 25 juin: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Les r�sultats du jeu de beaut� sont arriv�s ! Va vite voir en Ville, � la Mairie !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 6 juin: </div>
    <div class="newsTxt">- <img src="images/livre_beaute.gif" alt="Livre de beaut�" align="middle" width="40" /> Nouveau : un Trait� de beaut� ... � vite donner � son hamster !</div>
    </div>
    ';
	
    if (defined ('TOUTES_LES_NOUVEAUTES')) {
	
    $dernieresNouveautes .= '	
	
    <div class="newsBloc">
    <div class="newsDate">Jeudi 5 juin: </div>
    <div class="newsTxt">- <img src="images/coupe.gif" width="30" alt="coupe" align="middle" /> Le jeu de beaut� a commenc� !</div>
    <div class="newsTxt">- <img src="images/flacon_hamsteropoil.gif" alt="flacon" align="middle" /> Un nouveau produit de beaut� a fait son apparition !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Mardi 20 mai : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" align="middle" /> Les r�sultats du jeu du hamster le plus sportif sont arriv�s !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Dimanche 18 mai : </div>
    <div class="newsTxt">- <img src="images/notes3.gif" alt="musique" align="middle" /> Concerts et r�p�titions : � vous d\'am�liorer votre groupe encore davantage : de l\'exp�rience, des points et des pi�ces en r�compense !</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Vendredi 9 mai : </div>
    <div class="newsTxt">- <img src="images/poubelle.gif" alt="cage vente" /> Bon � savoir, il est d�sormais possible de vendre sa cage.</div>
    </div>
    <div class="newsBloc">
    <div class="newsDate">Jeudi 8 mai : </div>
    <div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" /> Jeu du hamster le plus sportif ! Pour s\'inscrire, il faut aller en Ville.</div>
    </div>        
	<div class="newsBloc">
	<div class="newsDate">Lundi 14 Avril : </div>
	<div class="newsTxt">- <img src="images/forum.gif" alt="Caf� des Hamsters" width="30" align="middle" /> Le Caf� des Hamsters a ouvert ses portes : on peut y discuter entre �leveurs en direct ! Il est au Forum !</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Mercredi 2 Avril : </div>
	<div class="newsTxt">- Les r�sultats du jeu du plus meilleur groupe � l\'Academy ! A d�couvrir en Ville puis en Mairie...</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Dimanche 23 Mars : </div>
	<div class="newsTxt">- Essaye de trouver les 10 oeufs de P�ques cach�s dans le jeu ! Il y a une r�compense pour ceux qui les trouvent !</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Lundi 17 Mars : </div>
	<div class="newsTxt">- Jeu du mois de Mars : meilleur groupe � l\'Academy ! Les d�tails sont en Ville puis � la Mairie...</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Dimanche 9 Mars : </div>
	<div class="newsTxt">- <img src="images/interro_reduc2.png" alt="enigme" /> Une nouvelle fa�on de faire maigrir son hamster a fait son apparition. A toi de d�viner comment...</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Vendredi 7 Mars : </div>
	<div class="newsTxt">- <img src="images/coupe.gif" alt="coupe" width="30" /> Les r�sultats du jeu du plus beau dessin/photo sont arriv�s ! A d�couvrir en Ville puis en Mairie...</div>
	</div>
	<div class="newsBloc">
	<div class="newsDate">Dimanche 10 F�vrier : </div>
	<div class="newsTxt">- Ouverture du jeu du plus beau dessin/photo ! Jusqu\'au 29 f�vrier.</div>
	</div>

	<div class="newsBloc"><div class="newsDate">Lundi 21 Janvier</div>
	<div class="newsTxt">- <img src="images/coeur.gif" alt="mariage" width="30" /> Les hamsters peuvent se marier ! Pour cela, rendez-vous dans la Mairie en ville !</div>
    </div>
	<div class="newsBloc"><div class="newsDate">Mercredi 9 Janvier</div>
	<div class="newsTxt">- <img src="images/part_galette.gif" alt="galette des rois" width="30" /> La galette des rois est arriv�e ! Cours vite l\'acheter en Boutique (en Ville) pour tes hamsters ! Attention, c\'est jusqu\'au 20 janvier !</div>
    </div>
	<div class="newsBloc"><div class="newsDate">Samedi 5 Janvier</div>
	<div class="newsTxt">- <img src="images/agenda.gif" alt="coupe" width="20" /> Le jeu du meilleur po�me et de la meilleure chanson est ouvert ! Explications dans le <a href="http://www.hamsteracademy.fr/forum/index.php">Forum</a>, � la rubrique "Annonces".</div>
	</div>
    <div class="newsBloc"><div class="newsDate">Mercredi 2 Janvier</div>
	<div class="newsTxt">- <img src="images/danseuse.gif" alt="danseuse �toile" width="20" /> <img src="images/crocodile.gif" alt="crocodile" width="25" /> Nouveaux m�tiers : aventurier, mannequin, vendeur de hamburgers, �leveur de crocodiles, etc.</div>
    </div>
	<div class="newsBloc"><div class="newsDate">Mardi 1er Janvier</div>
	<div class="newsTxt">- Toute l\'�quipe de l\'Hamster Academy souhaite la bonne ann�e � tous les joueurs ! Accrochez-vous, plein de nouveaut�s sont pr�vues pour ce d�but d\'ann�e !</div>
    </div>
	<div class="newsBloc"><div class="newsDate">Vendredi 24 d�cembre</div>
	<div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau" width="20" /> Les cadeaux de No�l sont arriv�s !</div>
	<div class="newsTxt">- <img src="images/cactus.gif" alt="cactus" width="20" /> Cactus et le bonzai disponibles ! A d�couvrir en Boutique !</div>
	<div class="newsTxt">- <img src="images/console.gif" alt="console" width="30" /> Console de jeu ! A vous les soir�es jeu entre amis (le mercredi et le dimanche) ! </div>
	</div>
    <div class="newsBloc"><div class="newsDate">Vendredi 14 d�cembre</div>
	<div class="newsTxt">- <img src="images/ballon_foot.gif" alt="foot" width="20" /> Sortie football possible ! A d�couvrir en Boutique !</div>
	<div class="newsTxt">- <img src="images/ballon_foot.gif" alt="foot" width="20" /> <img src="images/ballon_rugby.gif" alt="rugby" width="20" /> <img src="images/halteres_reduc.gif" alt="halt�res" width="20" /> <img src="images/boule_noire_reduc.gif" alt="boule" width="20" /> Et face aux multiples possibilit�s d\'activit�s, ces derni�res sont dor�navant reparties tout au long de la semaine. </div>
	</div>
    <div class="newsBloc"><div class="newsDate">Lundi 10 d�cembre</div>
	<div class="newsTxt">- <img src="images/micro2.gif" alt="micro" width="20" /> Il y a plein de nouveaux instruments&nbsp;! A d�couvrir en boutique&nbsp;!</div>
	<div class="newsTxt">- <img src="images/detective.gif" alt="micro" width="40" /> Nouveaux m�tiers ! D�tective, policier, skater...</div>
	</div>
    <div class="newsBloc"><div class="newsDate">Vendredi 07 d�cembre</div>
	<div class="newsTxt">- <img src="images/cadeauNoel.gif" alt="cadeau de noel" width="20" /> Les cadeaux de No�l � mettre au pied du sapin sont arriv�s !</div>
	<div class="newsTxt">- <img src="images/sapindenoel.gif" alt="Sapin de noel" width="32" /> Les sapins de No�l sont arriv�s ! Va vite les d�couvrir dans la boutique !</div>
	</div>
    <div class="newsBloc"><div class="newsDate">Mercredi 05 d�cembre</div>
	<div class="newsTxt">- Le jeu de la plus belle cage est ouvert ! Inscription jusqu\'au mercredi 12 d�cembre.</div>
	</div>
    <div class="newsBloc"><div class="newsDate">Mercredi 28 novembre</div>
	<div class="newsTxt">- Les chocolats de No�l sont arriv�s ! A d�couvrir d\'urgence dans la boutique !</div>
    <div class="newsTxt">- <img src="images/saxophone.gif" alt="Saxophone" width="32" /> Le saxophone est arriv� !</div>
	</div>
    <div class="newsBloc"><div class="newsDate">Lundi 26 novembre</div>
	<div class="newsTxt">- On peut choisir son avatar dans le jeu : il faut aller dans Options.</div>
	<div class="newsTxt">- On peut choisir l\'image du groupe : le leader du groupe peut mettre une image pour repr�senter le groupe. Motivez-le � le faire !</div>
    </div>
    ';
    }
}