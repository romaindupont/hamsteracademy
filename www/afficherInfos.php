<?php 
define('IN_HT', true);

include "common.php";
include "lstNiveaux.php";

error_reporting(E_ALL);

$modeAffichage="m_liste_accessoires";
if (isset($_POST['mode'])) 
    $modeAffichage=$_POST['mode'];
else if (isset($_GET['mode'])) 
    $modeAffichage=$_GET['mode'];    

$pagetitle = "Hamster Academy - ".T_("Infos sur le jeu");
$univers = UNIVERS_ELEVEUR ;
$description = T_("Tout savoir sur le jeu HA");

$liste_styles = "style.css,styleEleveur.css,cornflex-common.css";
$liste_scripts = "tooltip.js,gestionHamster.js,jquery.js,jquery.cornflex.js,yellowbox.js";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

// changement du macaron
if ($lang == "en"){
    $imageMacaron = "macaron-en.png";
    echo "<style content=\"text/css\">
        .macaron {
            background: transparent url('../images/$imageMacaron');
        }
        </style>";
}

?>

<div class="logoMain">
    <div class="logoImage">     
        <img src="images/logoHamsterAcademy.gif" alt="Hamster Academy" />
    </div>
    <div class="macaron"></div>
</div>

<div class="header">
  <ul>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_liste_accessoires") echo "id=\"current\"" ; ?> ><a href="afficherInfos.php?mode=m_liste_accessoires"><?php echo T_("Liste des Objets");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_liste_metiers") echo "id=\"current\"" ;?> ><a href="afficherInfos.php?mode=m_liste_metiers"><?php echo T_("Métiers");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_liste_missions") echo "id=\"current\"" ; ?> ><a href="afficherInfos.php?mode=m_liste_missions"><?php echo T_("Missions");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_liste_instruments") echo "id=\"current\"" ; ?> ><a href="afficherInfos.php?mode=m_liste_instruments"><?php echo T_("Instruments");?></a></li>
    <li class="menuPrincipal" <?php if ($modeAffichage == "m_liste_hamsters") echo "id=\"current\"" ; ?> ><a href="afficherInfos.php?mode=m_liste_hamsters"><?php echo T_("Hamsters");?></a></li>
    <li class="menuPrincipalDroite"><a href="jeu.php?mode=m_accueil&amp;univers=0"><img src="images/hamster_reduc.gif" style="height:16px; vertical-align:middle;" alt="" /> <?php echo T_("Revenir au jeu") ;?></a></li>
  </ul>
</div> 

<div class="mainPageUnderMenu">

<br/>&nbsp;<br/>

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">

<?php 

if ($modeAffichage == "m_liste_accessoires") {
    
   $query = "SELECT * FROM config_accessoires WHERE type NOT IN (0) AND prix >= 0 ORDER BY niveau ASC";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining ', '', __LINE__, __FILE__, $query);
    }
    
    $lstObjets = array();
    while($rowAccessoire=$dbHT->sql_fetchrow($result)) {
        
        array_push($lstObjets,$rowAccessoire);
    }
    $dbHT->sql_freeresult($result);
    $smarty->assign('listeAccessoires', $lstObjets);
    $smarty->display('afficherInfosListeAccessoires.tpl');
}
else if ($modeAffichage == "m_liste_missions") {
    
    $lstMissions = array();
    
    for($m=1;$m < $nbNiveaux ; $m++) {
    
        array_push($lstMissions,$infosNiveaux[$m]);
    }
    $smarty->assign('listeMissions', $lstMissions);
    $smarty->display('afficherInfosListeMissions.tpl');
}
else {
    echo "..."; 
}  
?>

  <div style="clear:both;">&nbsp;</div>

  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>

</div> <!-- fin du mainPageUnderMenu -->

<?php require "footer.php"; ?>