<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

if ($action == "vendreCaisseCocos") {
 
    if ($erreurAction == 0) {
        $msg .= afficherNouvelle("caisse_bois.png",T_("Tu as bien vendu la caisse pleine de cocos au vétérinaire ! Il en est très content car il peut faire des crèmes avec ! Il t'a donné 30 pièces."));
    }   
}

// si le joueur a sélectionné un ou plusieurs objets (cases à cocher)
if (isset($_POST['selectionObjets'])) {
    
    $listeObj = $_POST['selectionObjets'];
    $nbObjetsTraites = 0;
    $prixVenteTotal = 0;
    $ok = 0;
    
    // on récupère la liste des objets à vendre
    while (list ($key,$val) = @each($listeObj) ) { 
    
        $accessoire_id = intval($val);
        
        // quelle action a été effectuée ?
        if (isset($_POST['vendreObjets'])) {
            $prixVenteTotal += vendreAccessoire($accessoire_id);
            $nbObjetsTraites++;
        }
        elseif (isset($_POST['vendreTousObjets'])) {
            $prixVenteTotal += vendreAccessoire($accessoire_id,true);
            $nbObjetsTraites++;
        }
        elseif (isset($_POST['deplacerObjets'])) {
            
            $pourlacage_id = -1;
            if (isset($_POST['deplacer_dans_cage_id'])) 
                $pourlacage_id = intval($_POST['deplacer_dans_cage_id']) ;
                
            $precedenteCage_id = -1;
            if (isset($_POST['precedenteCage_id']))
                $precedenteCage_id = intval($_POST['precedenteCage_id']) ;
            
            mettreAccessoireDansLaCage($accessoire_id, $precedenteCage_id, $pourlacage_id) ;
        }
        elseif (isset($_POST['donnerAAmis'])) {
            
            $ami_id = -1;
            if (isset($_POST['ami_id'])) 
                $ami_id = intval($_POST['ami_id']) ;
            $quantite = 1;
            if (isset($_POST['quantiteDonObjet']))
                $quantite = intval($_POST['quantiteDonObjet']);
            $ok += donnerObjet($accessoire_id, $ami_id,$quantite) ;
        }
    }
    
    if (isset($_POST['vendreObjets']) || isset($_POST['vendreTousObjets'])) {
        
        echo T_("En vendant ces accessoires, tu as récupéré")." ".$prixVenteTotal." ".IMG_PIECE." !";
        updateListeAccessoires();
    }
    else if (isset($_POST['donnerAAmis']) && $ok > 0) {
        
        $msgAEnvoyer = T_("[Message automatique] ").$userdata['pseudo']." ".T_("vient te donner")." ";
        if ($ok > 1){
            echo T_("Ton ami a bien reçu tes objets");
            $msgAEnvoyer .= T_("plusieurs objets");
        }
        else{
            echo T_("Ton ami a bien reçu ton objet");
            $msgAEnvoyer .= T_("un objet");
        }
        $msgAEnvoyer .= ". ".T_("Regarde dans ton inventaire !");
        envoyerMessagePrive($ami_id,$msgAEnvoyer);
        updateListeAccessoires();
    }    
}

function regrouperAccessoiresDeType($type) {
    
    global $dbHT, $userdata;
    $modif = false;
    
    // pour chaque type d'accessoire, on compte ceux du joueur    
    $queryType = "SELECT quantite FROM accessoires WHERE joueur_id=".$userdata['joueur_id']." AND type = ".$type;
    if ( !($resultType = $dbHT->sql_query($queryType)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryType);
    }
    $nbAccessoiresType = $dbHT->sql_numrows($resultType);
    if ($nbAccessoiresType > 1) {
        $quantiteTotale = 0;
        while($rowType=$dbHT->sql_fetchrow($resultType)) {
            $quantiteTotale += $rowType['quantite'];
        }
        if ($quantiteTotale > 1) {

            // s'il y a au moins 2 accessoires de même type
            $querySupp = "DELETE FROM accessoires WHERE joueur_id=".$userdata['joueur_id']." AND type = ".$type;
            if ( ! $dbHT->sql_query($querySupp)){
                message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $querySupp);
            }
            $modif = true;            
            $new_acc_id = ajouterAccessoireDansBDD($type,$userdata['joueur_id'],-1,-1,-1,-1,$quantiteTotale);
            // attention : remise à zéro des dates d'acquisition des objets. à gérer dans les cas particuliers !
        }
        $dbHT->sql_freeresult($resultType);
    }
    
    return $modif;
}

// on regroupe les objets équivalents
$modif = false;
$compteurAccessoires = array_fill(0,1000,0);

for($accIndex=0;$accIndex<$nbAccessoires;$accIndex++) {

    if ($lst_accessoires[$accIndex]['danslacage'] == 0){
        
         $type = $lst_accessoires[$accIndex]['type'] ;
         $compteurAccessoires[ $type ] ++ ;
         $modif = true;
    }
}
for($accIndex=0;$accIndex<1000;$accIndex++) {
    if ($compteurAccessoires[ $accIndex ] > 1)
        regrouperAccessoiresDeType( $accIndex );
}

if ($modif) 
    updateListeAccessoires();

$lstObjetsFourreTout = array();
$lstObjetsCage = array();
$lstAmis = array();

// liste des cages
for($i=0;$i<$nbCages;$i++){
    array_push($lstObjetsCage,array('cage_id' => $lst_cages[$i]['cage_id'],'nom' => $lst_cages[$i]['nom'], 'listeAccessoires' => array(), 'lstAutresCages' => array() ));
    for($j=0;$j<$nbCages;$j++){
        if ($i != $j) {
            array_push($lstObjetsCage[$i]['lstAutresCages'],array('cage_id' => $lst_cages[$j]['cage_id'], 'nom' => $lst_cages[$j]['nom']));
        }
    }
}

// liste des amis
$query = "SELECT a.ami_de, j.pseudo, j.joueur_id FROM amis a, joueurs j WHERE a.joueur_id=".$userdata['joueur_id']." AND j.joueur_id = a.ami_de";
if ( !($result = $dbHT->sql_query($query)) ){
message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
}
while($rowAmi=$dbHT->sql_fetchrow($result)) {
    array_push($lstAmis,array('joueur_id' => $rowAmi['ami_de'], 'pseudo' => $rowAmi['pseudo']));
}
$dbHT->sql_freeresult($result);

// on remplit la liste des accessoires, fourre-tout + cages pour l'affichage
if ($nbAccessoires > 0) {
    for($ac=0;$ac<min(1500,$nbAccessoires);$ac++) {
        $rowAccessoire = &$lst_accessoires[$ac];
        if ($rowAccessoire['type'] != -1) {
            if ($rowAccessoire['cage_id'] == -1 || $rowAccessoire['danslacage'] == 0) {
                array_push($lstObjetsFourreTout,$rowAccessoire);
            }
            else {
                $cageIndex = cageCorrespondante($rowAccessoire['cage_id']);
                array_push($lstObjetsCage[$cageIndex]['listeAccessoires'],$rowAccessoire);
            }
        }
    }
}

// actions d'inventaire
$txtActionsInventaire = "";
if (possedeAccessoire(ACC_CAISSE_COCO) != -1) {

    $nbCocos = getNbAccessoires(ACC_NOIX_COCO);
    if ($nbCocos >= 20) {
        $txtActionsInventaire .= T_("Tu peux remplir une caisse de 20 noix de cocos. Tu veux la vendre au vétérinaire et récupérer ainsi 30 pièces ?");
        $txtActionsInventaire .= "<br/>&nbsp;<br/><a href=\"jeu.php?univers=$univers&amp;mode=m_objet&amp;action=vendreCaisseCocos\">";
        $txtActionsInventaire .= T_("Vendre une caisse de noix de cocos au vétérinaire !");
        $txtActionsInventaire .= "</a>";
    }
    else {
        $txtActionsInventaire .= "<img src=\"images/noixcoco.png\" alt=\"\" title=\"".T_("Noix de coco")."\"> ".sprintf(T_("Il te manque %d noix de cocos (fermées) pour remplir la caisse avant de pouvoir la vendre au vétérinaire."),(20-$nbCocos));
    }
}

$smarty->assign('txtActionsInventaire',$txtActionsInventaire);
$smarty->assign('msg',$msg) ;
$smarty->assign('lang',$lang);
$smarty->assign('lstObjetsFourreTout',$lstObjetsFourreTout);
$smarty->assign('lstObjetsCage',$lstObjetsCage);
$smarty->assign('lstAmis',$lstAmis);
$smarty->assign('ingredientNoisette',insererLienIngredient(5));
$smarty->display('inventaire.tpl');
?>