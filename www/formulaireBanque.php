<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

//if ($userdata['pseudo'] != "HamsterAcademy")  {
//    echo "La Banque est fermée quelques minutes : elle rouvre dans 5 minutes.";
//    return;
//}

if (!isset($systeme_bancaire))
    $systeme_bancaire = ALLOPASS;

// on récupère l'adresse d'où on vient
$pagePrec = "ach_";
if (isset($_GET['codePagePrec'])) 
    $pagePrec = mysql_real_escape_string($_GET['codePagePrec']);

$pagePrecUrl = "jeu.php?mode=m_achat"; // par défaut
$actionBanque = 0;

// information allopass
//$site_id = "xxx";
$site_id = "xxx";
//$doc_id = "xxx";
$doc_id = "xxx";
//$adv_id = "xxx";
$adv_id = "xxx";

if ($lang == "en") {
    //$site_id = "xxx";
    $site_id = "xxx";
    //$doc_id = "xxx";
    $doc_id = "xxx";
    //$adv_id = "xxx";
    $adv_id = "xxx";
}

require "gestionPaiement.php";

if ($paiement != PAIEMENT_NUL) {
    require "gestionPaiementReussi.php";
}
else {

    if ($pageTxt == "nid" ||$pageTxt == "cab") {
        echo "<div style=\"padding-top:30px; width:600px; text-align:left;\">";
        echo "<img src=\"images/montre.gif\" alt=\"\" height=\"80\"><br/>&nbsp;<br/>";
        
        if ($erreur != -1) 
            echo $message_erreur;
        else {
            if ($pageTxt == "nid")
                echo "<strong>".T_("Tu veux accélérer la construction du nid ?")."</strong>";
            else 
                echo "<strong>".T_("Tu veux accélérer la construction de la cabane ?")."</strong>";
            
            echo "<br/>&nbsp;<br/>".T_("Tu as 2 possibilités : ");        
            echo "<ol><li><img src=\"images/diplome.gif\" style=\"vertical-align:middle;\" width=\"50\" />".T_("Acheter une formation de bâtisseur pour ton hamster (qui réduit à 24 heures la durée de fabrication) : elle est disponible dans la Boutique.")."<br/>&nbsp;<br/></li>";
            echo T_("<li>Ou, si tu es plus pressé, tu peux acheter un code qui réduit la durée de fabrication à 1 minute ! Pour cela, compose le numéro suivant (selon ton pays)")."</li></ol></div>";        
        }
    }
    else if ($pageTxt == "gro") {
        echo "<div style=\"padding-top:30px; width:600px; text-align:left;\">";
        echo "<div align=\"center\"><img src=\"images/reproduction.gif\" alt=\"\" /></div><br/>&nbsp;<br/>";
        
        if ($erreur != -1) 
            echo $message_erreur;
        else {
            echo "<strong>".T_("Tu veux accélérer la grossesse de ton hamster ?")."</strong>";
            
            echo "<br/>&nbsp;<br/>".T_("Tu peux acheter un code pour mettre les bébés dans une couveuse ultra rapide ! Ca permet de réduire le temps de la grossesse à 1 minute ! Pour cela, compose le numéro suivant (selon ton pays)")." :</div>";        
        }
    }
    else if ($pageTxt == "niv") {
        echo "<div style=\"padding-top:30px; width:600px; text-align:left;\">";
        
        if ($erreur != -1) 
            echo $message_erreur;
        else {
            echo "<strong>".T_("Tu veux passer au niveau suivant ?")."</strong>";
            
            echo "<br/>&nbsp;<br/>".str_replace("#1",$userdata['niveau']+1,T_("Tu peux acheter un code pour passer au niveau #1 tout de suite ! Pour cela, compose le numéro suivant (selon ton pays) :</div>"));        
        }
    }
    else if ($pageTxt == "vip") {
        echo "<div style=\"padding-top:30px; width:600px; text-align:left;\">";
        
        if ($erreur != -1) 
            echo $message_erreur;
        else {
            echo "<strong>".T_("Tu veux devenir VIP pour le mois de ").$mois_txt[$month]." ? </strong>";
            
            echo "<br/>&nbsp;<br/>".T_("Tu peux acheter un code pour devenir VIP tout de suite ! Pour cela, compose le numéro suivant (selon ton pays) :");        
        }
        echo "</div>";
    }
    else {
        echo "<div align=\"center\">";
        echo "<img src=\"images/plusdesous.gif\" alt=\"\" /><br/>&nbsp;<br/>";
        if ($erreur != -1) 
            echo $message_erreur;
        else {
            echo str_replace("#1",$nbPiecesParAchatBanque,T_("Tu n'as plus ou pas assez de pièces ? <br/>&nbsp;<br/>Sans attendre ton prochain salaire, Tu peux gagner <strong>#1</strong> pièces<br/> en composant le numéro suivant (selon ton pays) "));
            //echo "<br>&nbsp;<br>(<strong>C'est une promotion de rentrée valable jusqu'au 20 novembre ! </strong>)";
        }
        echo "</div>";
    }
    
    if ($lang == "fr")
        require "afficherMenuAllopass.php";
    else {
        if ($pageTxt != "nid" && $pageTxt != "cab" && $pageTxt != "gro" && $pageTxt != "vip" && $pageTxt != "niv")
            require "afficherMenuPaypal.php";
        else
            require "afficherMenuAllopass.php";
    }

}
?>
