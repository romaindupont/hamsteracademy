    <?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require "functions_jeu_hamster.php";
require_once "lstInstruments.php";

// hamster tres malade et chez le veto
$revientDuVeto = -1 ;

$afficherColonnes = true;
$blocCentralTxt = "";
$blocActionTxt = "";
$blocActionAcademyTxt = "";

// pas de hamster
$hamRow = array();
if ($nbHamsters > 0)
    $hamRow = $lst_hamsters[$hamsterIndex];

if ($nbHamsters == 0) {
    echo afficherNouvelleDansCadre("Veterinaire_reduc.png", T_("Tu n'as plus de hamster... Il est peut-être resté malade trop longtemps et le véto l'a donné à la SPH...<br/>&nbsp;<br/>Mais dépêche-toi d'aller en")." <a href=\"jeu.php?mode=m_achat&amp;rayon=".RAYON_HAMSTER."\" title=\"".T_("Aller en boutique")."\">".T_("boutique")."</a> ".T_("pour en acheter un nouveau et commencer une nouvelle vie avec lui !") );
    $afficherColonnes = false;
}
else if (isset($_GET['voirbebes']) || isset($_GET['reprod']) ){
    // si une grossesse est terminée ou que le joueur veut une grossesse
    require "jeu_reprod.php";
    $afficherColonnes = false;
}
else if ($action == "chezleveto"){
    require "jeu_veto.php" ;
    $afficherColonnes = false;
}
else if ($lst_hamsters[$hamsterIndex]['pause'] > 0) { // si les hamsters sont en mode pause
    echo afficherNouvelleDansCadre("refuge.gif",T_("Les hamsters sont au Refuge de l'Academy. Pour les récupérer, va dans la page d'accueil et clique sur \"Récuperer les hamsters\""));
    $afficherColonnes = false;
}
else if ($userdata['niveau'] == NIVEAU_EVASION && $hamsterIndex == 0 && getObjectifNiveau($userdata['niveau_objectif'],1) == false ) { // mission 10 : évasion du hamster
    if ($hamRow['sante'] == 0)
        echo afficherNouvelle("foret.gif",T_("Ton hamster s'est évadé du véto ! Retrouve-le vite avant qu'il disparaisse pour de bon dans la nature !"));
    else
        echo afficherNouvelle("foret.gif",T_("Ton hamster s'est évadé ! Retrouve-le vite avant qu'il disparaisse pour de bon dans la nature !"));
}
else if ($univers == UNIVERS_ACADEMY && $userdata['niveau'] < $niveauMinimalAccesAcademy) {
    echo T_("L'academy n'est accessible qu'à partir du niveau")." ".$niveauMinimalAccesAcademy.".";
}
else if ($nbHamsters > 0 ) {
    $faim = faim($hamRow['dernier_repas']) ;
    
    // on gère la construction de son nid/cabane (uniquement en mode éleveur pour que l'affichage soit cohérent)
    if ($univers == UNIVERS_ELEVEUR) {
        if ($hamRow['construction_nid'] > 0) {
            
            // si le nid est prêt, on l'ajoute à la cage de l'hamster
            if (isFabricationFinie($hamRow['construction_nid'], $hamRow['init_batisseur'])) {
                $cageHamsterIndex = cageCorrespondante($hamRow['cage_id']);
                $nouveauNidId = ajouterAccessoireDansBDD(ACC_NID, $userdata['joueur_id'],$hamRow['cage_id'], 100, 0, 20, 1);
                $lst_cages[$cageHamsterIndex]['nbAccessoires']++;
                $query = "SELECT * FROM accessoires WHERE accessoire_id=".$nouveauNidId;
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
                }
                $nouveauNidAcc=$dbHT->sql_fetchrow($result);
                $dbHT->sql_freeresult($result);
                array_push($lst_accessoires,$nouveauNidAcc);
                
                // on met à jour le niveau du joueur
                if ($userdata['niveau'] == NIVEAU_METIER_NID)
                    updateNiveauJoueur(1,true) ;
            }
        }
        if ($hamRow['construction_cabane'] > 0) {
            
            // si la cabane est prête, on l'ajoute à la cage de l'hamster
            if (isFabricationFinie($hamRow['construction_cabane'], $hamRow['init_batisseur'])) {
                $cageHamsterIndex = cageCorrespondante($hamRow['cage_id']);
                $nouvelleCabaneId = ajouterAccessoireDansBDD(ACC_MAISON_BOIS, $userdata['joueur_id'],$hamRow['cage_id'], 100, 0, 20, 1);
                $lst_cages[$cageHamsterIndex]['nbAccessoires']++;
                $query = "SELECT * FROM accessoires WHERE accessoire_id=".$nouvelleCabaneId;
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
                }
                $nouvelleCabaneAcc=$dbHT->sql_fetchrow($result);
                $dbHT->sql_freeresult($result);
                array_push($lst_accessoires,$nouvelleCabaneAcc);
                
                if ($userdata['niveau'] == NIVEAU_CABANE_BEBE_AVATAR)
                    updateNiveauJoueur(0,true);
            }
        }
    }
    
    if ($action != "")
        require "hamster_msg_actions.php";

    // si le hamster a été abandonné, on affiche un message
    if (isset($_GET['abandonHamster']))
        $msg .= afficherNouvelle("refuge.gif",$lstMessages[72],"Refuge");
        
//    if (isset($_GET['bebe'])) {
//        $msgTmp="";
//        if ($erreurAction == 0) 
//            $msgTmp=T_("La grossesse est partie. Il faut attendre environ 1 semaine pour voir les bébés")."&nbsp;!";
//        else if ($erreurAction == 2) 
//            $msgTmp = T_("Hum ! Ta femelle est déjà enceinte !");
//        else if ($erreurAction == 3) 
//            $msgTmp = T_("Il faut un nid dans la cage pour accueillir les futurs bébés !");
//        else if ($erreurAction == 1) 
//            $msgTmp = T_("Hum ! Tu as choisi une femelle pour avoir des bébés avec ta femelle ! Ce n'est pas possible !");
//        $msg .= afficherNouvelle("couffin.gif",$msgTmp);
//    }
    if ($constructionNidCommencee)
        $msg .= "<div class=\"ham1Nouvelle\">".T_("La construction du nid a commencé ! Elle dure plusieurs jours (parfois une semaine). Si tu veux accélérer la construction, achète une formation de bâtisseur pour ton hamster").".</div>";
    else if ($constructionCabaneCommencee)
        $msg .= "<div class=\"ham1Nouvelle\">".T_("La construction de la cabane a commencé ! Elle dure plusieurs jours... (moins longtemps si ton hamster a reçu une formation de bâtisseur)")."</div>";
        
    if ($hamRow['sante'] == 0) {
        if ($hamRow['sexe']!=0)
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est très malade, il a été envoyé chez le vétérinaire. Dépêche-toi de le récupérer !")) ;
        else 
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est très malade, elle a été envoyée chez le vétérinaire. Dépêche-toi de la récupérer ! <br/>Car le véto ne garde les hamsters que 2 semaines... Il te reste <strong>#2</strong> pour le récupérer.")) ;
        $nbJoursRestantsVeto = round(($hamRow['chezleveto'] + 1209600 - $dateActuelle ) / 86400 ) ;
        $msg_modif = str_replace("#2",$nbJoursRestantsVeto . " ".T_("jour").($nbJoursRestantsVeto > 1 ? "s":""),$msg_modif) ;
        $msg .= afficherNouvelle("Veterinaire_reduc.gif",$msg_modif);
    }
    else if ($hamRow['sante'] < 3) {
        if ($hamRow['sexe']!=0)
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("Attention, #1 est malade, elle manque de vitalité, de vitamines. L'hygiène de la cage peut être insuffisante. Est-elle suffisamment nourrie&nbsp;? <br/>Il faut absolument la soigner et améliorer la propreté de la cage !")) ;
        else 
            $msg_modif = str_replace("#1",$hamRow['nom'],T_("Attention, #1 est malade, il manque de vitalité, de vitamines. L'hygiène de la cage peut être insuffisante. Est-il suffisamment nourri&nbsp;? <br/>Il faut absolument le soigner et améliorer la propreté de la cage !")) ;
        $msg .=    afficherNouvelle("trousse_reduc.gif",$msg_modif);
    }
    if ($hamRow['puissance'] < 3) {
        $msg .= afficherNouvelle("halteres_reduc.jpg",T_("Attention, ton hamster est faible, il manque de force et de muscles. A trop glander dans sa cage, le voilà tout mou ! Il faut absolument qu'il fasse de la roue ou des pompes ou des haltères par exemple !"),"",70); // peu de force physique
    }
    $diffPoids = $poidsMoyen - $hamRow['poids'] ;
    if ($diffPoids > 15) {
        $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 est en train de devenir un vrai squelette&nbsp;! Vérifie que #1 mange suffisamment !")) ;
        $msg .= afficherNouvelle("pese_personne.gif",$msg_modif,"",50); // trop gros
    }
    else if($diffPoids < -15) {
        $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 devient un vrai hamsterObélix ! Il faut à tout prix qu'il perde du poids sinon gare à la mauvaise santé ! Donc sport et pas trop de gateaux au programme ! Si ton niveau est suffisant, tu peux fabriquer une roue motorisée. Regarde l'aide comment faire.")) ;
        $msg .= afficherNouvelle("pese_personne.gif",$msg_modif,"",50); // trop maigre
    }
    if ($faim < 3) {
        $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 a faim !")) ;
        $msg .= afficherNouvelle("ecuelle_reduc_sb.gif",$msg_modif,""); // faim
    }
    if (yatilBiberonDansLaCage($hamRow['cage_id'], $userdata['joueur_id']) == -1) {
        $msg .= afficherNouvelle("biberon.gif",T_("Ton hamster a soif ! Mets-vite un biberon dans sa cage !")); // soif
    }
    
    // le hamster est tombé malade
    if ($hamRow['maladie'] > 0) {
        
        // le joueur possède t-il le livre de médecine ?
        if ($userdata['niveau'] > 4) { // pour la mission 4 où le joueur ne doit pas savoir ce qu'a le hamster
            if ( possedeAccessoire(ACC_MEDIC_BOOK) != -1 ) {
                $msgTmp = $hamRow['nom'] ." ".T_("est malade, il a")." ";
                $msgTmp .= $lstMaladies[$hamRow['maladie']][MALADIE_NOM]."...<br/>".$lstMaladies[$hamRow['maladie']][MALADIE_DESC];
                $msgTmp .= "<br/>".T_("Elle se transmet par")." ".$lstMaladies[$hamRow['maladie']][MALADIE_TRANSMISSION];
                $msgTmp .= "<br/>".T_("Les symptomes sont :")." ".$lstMaladies[$hamRow['maladie']][MALADIE_SYMPTOMES];
                $msg .= afficherNouvelle($lstMaladies[$hamRow['maladie']][MALADIE_IMAGE],$msgTmp,"");
            }
            else {
                $msgTmp = $hamRow['nom'] ." ".T_("est malade mais on ne sait pas ce qu'il a...");
                $msgTmp .= T_("Achète vite le livre de médecine en boutique pour savoir quel médicament lui donner.");
                $msg .= afficherNouvelle("vieux-livre.gif",$msgTmp,"",100);
            }
        }
        else {
            $msgTmp = $hamRow['nom'] ." ".T_("est malade mais on ignore ce qu'il a. Il a l'air très fatigué. Guéris-le vite !");
            $msg .= afficherNouvelle("",$msgTmp,"");
        }
    }
    
    // message aleatoire selon le contexte si rien de particulier
    if ($univers == UNIVERS_ELEVEUR && $action == "") {
        // évènements si rien ne se passe
        $chanceEvenement = rand(1,100);
        
        // l'hamster a chopé un virus
        if ($chanceEvenement == 1 && $hamRow['sante'] > 0 && $hamRow['sante'] < 7 && $userdata['niveau'] >= NIVEAU_DEBUT_ACADEMY) {
            $msgTmp = afficherNouvelle("virus.gif",T_("Ca arrive, ça arrive ! Ton hamster vient de choper un virus... Le voilà tout faible, il a un peu maigri... Si tu peux lui donner des vitamines, ce sera mieux. Bon rétablissement à ").$hamRow['nom']."...","");
            $msg = $msgTmp.$msg;
            $nouveauPoids = max(80,$hamRow['poids']-10);
            $nouvelleSante = min($hamRow['sante'],max(1,$hamRow['sante']-5));
            if ($userdata['inscritConcours']==0)
                $nouveauPhysique = max(0,$hamRow['puissance']-2);
            else
                $nouveauPhysique = $hamRow['puissance'];
            $nouveauMoral = max(0,$hamRow['moral']-2);
            $nouvelleBeaute = max(0,$hamRow['beaute']-2);
            
            $lst_hamsters[$hamsterIndex]['sante'] = $nouvelleSante;
            $lst_hamsters[$hamsterIndex]['puissance'] = $nouveauPhysique;
            $lst_hamsters[$hamsterIndex]['beaute'] = $nouvelleBeaute;
            $lst_hamsters[$hamsterIndex]['moral'] = $nouveauMoral;
            $lst_hamsters[$hamsterIndex]['poids'] = $nouveauPoids;

            $hamRow['sante'] = $nouvelleSante;
            $hamRow['puissance'] = $nouveauPhysique;
            $hamRow['beaute'] = $nouvelleBeaute;
            $hamRow['moral'] = $nouveauMoral;
            $hamRow['poids'] = $nouveauPoids;                
            
            $query = "UPDATE hamster 
                SET sante = ".$nouvelleSante.",
                puissance = ".$nouveauPhysique.",
                beaute = ".$nouvelleBeaute.",
                moral = ".$nouveauMoral.",
                poids = ".$nouveauPoids."
                WHERE hamster_id=".$hamRow['hamster_id'];
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
            }    
        }
        else if ($chanceEvenement < 10) {
            // pour rassurer certains joueurs, on indique explicitement, parfois, que le hamster fait bien de la roue
            // et on augmente d'un petit chouia sa forme physique
            if (yatilRoueDansLaCage($hamRow['cage_id'],$userdata['joueur_id']) == true && $hamRow['sante'] > 0) {
                $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 vient de faire de la roue !")) ;
                $msg .= afficherNouvelle("roue3.gif",$msg_modif,"",37);
                
                $hamRow['puissance'] = seuillerForce($hamRow['puissance'] + 0.1, $hamRow['maxForce']);
                $query = "UPDATE hamster 
                    SET puissance = '".$hamRow['puissance']."'
                    WHERE hamster_id=".$hamRow['hamster_id'];
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                }    
            }
        }
        else if ($chanceEvenement < 15) { // idem avec le vélo d'appart
            if (yatilAccessoireDansLaCage($hamRow['cage_id'], ACC_VELO,$userdata['joueur_id']) != -1 && $hamRow['sante'] > 0) {
                $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 a fait du vélo aujourd'hui ! Quel sportif !")) ;
                $msg .= afficherNouvelle("velo.gif",$msg_modif,"",37);
            }
        }
        else if ($userdata['niveau'] >= 12 && $chanceEvenement >= 15 && $chanceEvenement < 20) {
            if (yatilAccessoireDansLaCage($hamRow['cage_id'], ACC_COCOTIER1,$userdata['joueur_id']) != -1 || yatilAccessoireDansLaCage($hamRow['cage_id'], ACC_COCOTIER2,$userdata['joueur_id']) != -1) {
                if ($chanceEvenement < 16){
                    $msg .= afficherNouvelle("noixcoco_ouverte.png",T_("Ouch !! Ton hamster a reçu une noix de coco sur la tête !"));
                    $hamRow['sante'] = seuillerSante($hamRow['puissance'] - rand(1,10), $hamRow['maxSante']);
                    $query = "UPDATE hamster 
                        SET sante = '".$hamRow['sante']."'
                        WHERE hamster_id=".$hamRow['hamster_id'];
                    if ( !($dbHT->sql_query($query)) ){
                        message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                    }    
                    ajouterAccessoireDansBDD(ACC_NOIX_COCO_OUVERTE,$userdata['joueur_id'],-1,-1,-1,-1,1);
                }
                else{
                    $msg .= afficherNouvelle("noixcoco.png",T_("Une noix de coco est tombée de l'arbre ! Ella a été mise dans ton inventaire. Tu peux la remettre dans la cage si tu veux."));
                    ajouterAccessoireDansBDD(ACC_NOIX_COCO,$userdata['joueur_id'],-1,-1,-1,-1,1);
                }
            }
        }
        else if ($chanceEvenement < 25) { // idem avec le muguet
            if (yatilAccessoireDansLaCage($hamRow['cage_id'], ACC_MUGUET,$userdata['joueur_id']) != -1 && $hamRow['sante'] > 0) {
                $msg_modif = str_replace("#1",$hamRow['nom'],T_("#1 adore le muguet !")) ;
                $msg .= afficherNouvelle("muguet.png",$msg_modif,"",37);
                $hamRow['moral'] = seuillerMoral($hamRow['moral']+0.5,$hamRow['maxMoral']);
                $query = "UPDATE hamster SET moral = '".$hamRow['moral']."' WHERE hamster_id=".$hamRow['hamster_id'];
                $dbHT->sql_query($query);
            }
        }
        else if ($hamRow['energie'] < 5 && $chanceEvenement < 50) {
            $msg .= afficherNouvelle("energie.gif",T_("Attention, ton hamster n'a plus beaucoup d'énergie. Economise-la car il devra bientôt se reposer s'il continue toutes ses activités !"),"",30);
        }
        else if (0 && possedeAccessoire(ACC_AMULETTE_FORCE) != -1 && $chanceEvenement < 75 && $lang == "fr") {
            $msg .= afficherNouvelle("amulette.png",T_("Grâce à l'Amulette de Sport, ton hamster est deux fois plus fort !"),"",50);
        }
    }
    else if ($univers == UNIVERS_ACADEMY && $action == "" && $hamRow['inscrit_academy'] != 2) {
        // évènements aléatoires
        $chanceEvenement = rand(1,20);
        if ($chanceEvenement < 3 ) {
            
            $msgTmp = "";
            if ($chanceEvenement == 1)
                $msgTmp = T_("L'Academy est très appréciée du public ! Ce dernier suit avec beaucoup d'intérêt les élèves de l'Academy. Tous les hamsters récupèrent 2 points de popularité !");
            else
                $msgTmp = T_("Les élèves de l'Academy se rebellent contre la sévérité des professeurs et du jury d'examen. Le public adore tout ce mouvement ! Tous les hamsters récupèrent 2 points de popularité !");
                
            $msg .= "<div class=\"ham1Nouvelle\"><div class=\"hamNouvellesImg\"><img src=\"images/cameras.gif\" alt=\"\"></div><div class=\"hamBlocTxt\">".$msgTmp."</div></div>" ; 
            
            // on rajoute 2 points de popularité à chaque hamster
            for($hamster=0;$hamster<$nbHamsters;$hamster++) {
                $lst_hamsters[$hamster]['popularite'] += 2 ;
                $query = "UPDATE hamster 
                    SET popularite = ".$lst_hamsters[$hamster]['popularite']."
                    WHERE hamster_id=".$lst_hamsters[$hamster]['hamster_id'];
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                }
            }
            $hamRow['popularite'] += 2;
        }
    }
 
if ( ($univers == UNIVERS_ACADEMY || $univers == UNIVERS_FOOT)  && $msg == "" && $hamRow['inscrit_academy'] != 2) {
    $chanceEvenement = rand(1,2);
    if ($chanceEvenement == 1) 
        $msg .= T_("Bienvenue dans l'Academy ! C'est ici que ton hamster va améliorer ses talents. Note que sa popularité est notée tous les 3 jours seulement. Il faut bien s'entraîner !");
    else {
        if(($hamRow['niveau']+1) < NIVEAU_ACADEMY_FIN) {
            $msg .= T_("Bienvenue dans la Salle d'Examen ! C'est ici que ton hamster va montrer tous ses talents.")." <br/>&nbsp;<br/>".T_("Entraîne-toi bien avant car il faut attendre 3 jours entre 2 passages d'examen.");
            $msg .= "<br/>&nbsp;<br/>";
            $msg .= T_("L'examen permet de passer au niveau supérieur")." (".$lstNiveauxAcademy[($hamRow['niveau']+1)+1].").";
        }
    }
}        

if ($univers == UNIVERS_ACADEMY || $univers == UNIVERS_FOOT){
    
    $smarty->assign('experience', round($hamRow['experience']));
    $smarty->assign('popularite', round($hamRow['popularite']));
    $smarty->assign('academy', $hamRow['inscrit_academy']);
    
    if ($hamRow['inscrit_academy'] == 1){
    
        $smarty->assign('danse', round($hamRow['danse']));
        $smarty->assign('chant', round($hamRow['chant']));
        $smarty->assign('musique', round($hamRow['musique']));
        $smarty->assign('sociabilite', round($hamRow['sociabilite']));
        $smarty->assign('humour', round($hamRow['humour']));
        $smarty->assign('nomNiveau', $lstNiveauxAcademy[$hamRow['niveau']]);

        $instrument = $hamRow['specialite'];
        $txtInstrument = "";
        if ($instrument != -1)
            $txtInstrument .= T_("Instrument")." : <img src=\"images/".getImageAccessoire($lstInstruments[$instrument][INSTRUMENT_REF_OBJET])."\" style=\"vertical-align:middle; height:30px;\" alt=\"\" /> ".$lstInstruments[$instrument][INSTRUMENT_SPECIALITE];
        else
             $txtInstrument .= T_("Instrument")." : ".T_("aucun");
            
        // possibilité de choisir une spécialité
        if ($instrument == -1){
             $txtInstrument .= "<br/>&nbsp;<br/>".T_("Choisir son instrument")." : <br/>" ;
            $possedeInstrument = 0;
            for($acc=0;$acc<$nbAccessoires;$acc++) {
                if ($lst_accessoires[$acc]['rayon'] == RAYON_INSTRUMENT && $lst_accessoires[$acc]['type'] != ACC_PARTITION_ROCKROLLHAMSTERS) {
                    $possedeInstrument ++ ;
                     $txtInstrument .= "<a href=\"jeu.php?mode=m_hamster&amp;univers=$univers&amp;hamster_id=".$hamRow['hamster_id']."&amp;actionAcademy=choisirInstrument&amp;instrument=".$lst_accessoires[$acc]['accessoire_id']."\" title=\"".T_("Son futur instrument")." : ".$lst_accessoires[$acc]['nom_fr']."\">";
                     $txtInstrument .= "<img src=\"images/".$lst_accessoires[$acc]['image']."\" height=\"40\" alt=\"\" />";
                     $txtInstrument .= "</a> ";
                }
            }
            // si le joueur ne possède pas d'instruments
            if ($possedeInstrument == 0)
                 $txtInstrument .= "<br/><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_INSTRUMENT."\">".T_("Tu n'as pas d'instrument dans ton inventaire : en acheter un ?")."</a>";
            else
                 $txtInstrument .= "<br/><a href=\"jeu.php?mode=m_achat&amp;univers=$univers&amp;rayon=".RAYON_INSTRUMENT."\">".T_("Acheter d'autres instruments ?")."</a>";
        }else { // changer d'instrument?
             $txtInstrument .= "<br/>&nbsp;<br/><div style=\"font-size:9pt;\">";
             $txtInstrument .= "<a href=\"jeu.php?mode=m_hamster&amp;univers=1\" onclick=\"javascript: if (confirm('".T_("Tu es sûr de changer l instrument de ton hamster ? Rappel : tu perds l instrument et l expérience de ton hamster sera divisée par deux")."...')) document.location.href='jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;actionAcademy=changerInstrument&amp;univers=1' ; else return false ; return false;\" title=\"".T_("Pour changer d'instrument... Mais attention, tu perds l'instrument et l'expérience de ton hamster sera divisée par deux")." !\">".T_("Changer d'instrument ?")."</a>";
             $txtInstrument .= "</div>";
        }
        
        $smarty->assign('txtInstrument', $txtInstrument);

    }
    else if($hamRow['inscrit_academy'] == 2) {
        $smarty->assign('agilite', round($hamRow['agilite']));
        $smarty->assign('vitesse', round($hamRow['rapidite']));
        $smarty->assign('defense', round($hamRow['defense']));
        $smarty->assign('endurance', round($hamRow['endurance']));
        $smarty->assign('attaque', round($hamRow['attaque']));
        $smarty->assign('gardien', round($hamRow['gardien']));
        $smarty->assign('nomNiveau', $lstNiveauxFoot[$hamRow['niveau']]);
        $smarty->assign('specialite', $hamRow['specialite']);
        $smarty->assign('noteMaxFoot', 10*($hamRow['niveau']+1));

//        $specialite = $hamRow['specialite'];
//        $txtSpecialite = "";
//        if ($specialite != -1)
//            $txtSpecialite .= T_("Spécialité")." : ".$lstSpecialitesFoot[$specialite];
//        else {
             //$txtSpecialite.= T_("Spécialité")." : ".T_("aucune");
//            
            // possibilité de choisir une spécialité
//            if ($specialite == -1){
//                $txtSpecialite.= "<br/>&nbsp;<br/>".T_("Choisir sa spécialité")." : <br/>" ;
//                
//                $txtSpecialite .= "<br/><a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;univers=$univers&amp;actionAcademy=choisirJoueurFoot\">".T_("En défense")."</a>";
//                $txtSpecialite .= "<br/><a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;univers=$univers&amp;actionAcademy=choisirJoueurFoot\">".T_("Milieu de terrain")."</a>";
//                $txtSpecialite .= "<br/><a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;univers=$univers&amp;actionAcademy=choisirJoueurFoot\">".T_("En défense")."</a>";
//                $txtSpecialite .= "<br/>ou <a href=\"jeu.php?mode=m_hamster&amp;".$hamRow['hamster_id']."&amp;univers=$univers&amp;actionAcademy=choisirGardienFoot\">".T_("Gardien de but")."</a>";
//            }
//        }
//        $smarty->assign('txtSpecialite', $txtSpecialite);

    }
}
else if ($univers == UNIVERS_ELEVEUR) { 

    if (possedeAccessoire(ACC_AMULETTE_FORCE) != -1 )
        $hamRow['puissance'] *= $puissanceAmuletteForce;
    
    $smarty->assign('moral', round($hamRow['moral'],1));
    $smarty->assign('poids', $hamRow['poids']);
    $smarty->assign('sante', round($hamRow['sante'],1));
    $smarty->assign('force', round($hamRow['puissance'],1));
    $smarty->assign('beaute', round($hamRow['beaute'],1));
    $smarty->assign('energie', round($hamRow['energie'],1));
    $smarty->assign('maxMoral', $hamRow['maxMoral']);
    $smarty->assign('maxSante', $hamRow['maxSante']);
    $smarty->assign('maxBeaute', $hamRow['maxBeaute']);
    $smarty->assign('maxEnergie', $hamRow['maxEnergie']);
    $smarty->assign('maxForce', $hamRow['maxForce']);
    $smarty->assign('faim', $faim);
    $smarty->assign('age', affiche_age($hamRow['date_naissance']));
    $smarty->assign('sexe', $hamRow['sexe']);
    $smarty->assign('oeuf9', afficherObjet("oeufs",9));
    $smarty->assign('yeux', $infosHamsters[$hamRow['type']][HAMSTER_YEUX]);
    $smarty->assign('pelage', $infosHamsters[$hamRow['type']][HAMSTER_PELAGE]);
    $smarty->assign('init_coquet',$hamRow['init_coquet']);
    $smarty->assign('init_fort', $hamRow['init_fort']);
    $smarty->assign('init_resistant', $hamRow['init_resistant']);
    $smarty->assign('init_sociable',$hamRow['init_sociable'] );
    $smarty->assign('init_dragueur', $hamRow['init_dragueur']);
    $smarty->assign('init_batisseur', $hamRow['init_batisseur']);
    $smarty->assign('pere_id', $hamRow['pere_id']);
    $smarty->assign('mere_id', $hamRow['mere_id']);
    $smarty->assign('compagnon_id', $hamRow['compagnon_id']);
    $smarty->assign('pere_id', $hamRow['pere_id']);
    $smarty->assign('mere_id', $hamRow['mere_id']);
    $smarty->assign('compagnon_id', $hamRow['compagnon_id']);
    
    if ($hamRow['pere_id'] > 0) {

        $query = "SELECT nom,hamster_id,type FROM hamster WHERE hamster_id=".$hamRow['pere_id']. " LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        if ($dbHT->sql_numrows($result)>0) {
            $pere_row=$dbHT->sql_fetchrow($result);
            $smarty->assign('pere_nom', tronquerTxt($pere_row['nom'],15));
        }
        $dbHT->sql_freeresult($result);
    }
    if ($hamRow['mere_id'] > 0) {

        $query = "SELECT nom,hamster_id,type FROM hamster WHERE hamster_id=".$hamRow['mere_id']. " LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        if ($dbHT->sql_numrows($result)>0) {
            $mere_row=$dbHT->sql_fetchrow($result);
            $smarty->assign('mere_nom', tronquerTxt($mere_row['nom'],15));
        }
        $dbHT->sql_freeresult($result);
    }
    if ($hamRow['compagnon_id'] > 0) {

        $query = "SELECT nom,hamster_id,type FROM hamster WHERE hamster_id=".$hamRow['compagnon_id']. " LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        if ($dbHT->sql_numrows($result)>0) {
            $compagnon_row=$dbHT->sql_fetchrow($result);
            $smarty->assign('compagnon_nom', tronquerTxt($compagnon_row['nom'],15));
        }
        $dbHT->sql_freeresult($result);
    }
    $txtReproductivite = "";
    if ( ($dateActuelle - $hamRow['date_naissance']) < 3600*24*10) { 
        $txtReproductivite = T_("Trop jeune pour avoir des bébés");
    }
    else {
        if ($hamRow['sexe'] == 0)
            $txtReproductivite = T_("Assez agé pour avoir des bébés, mais seule la femelle peut en avoir !");
        else if ($hamRow['sexe'] == 1)
            if ($userdata['niveau'] < NIVEAU_CABANE_BEBE_AVATAR)
                $txtReproductivite = T_("Assez agée pour avoir des bébés mais il faut être au niveau")." ".NIVEAU_CABANE_BEBE_AVATAR." ".T_("pour en avoir")."...";
            else
                $txtReproductivite = T_("Assez agée pour avoir des bébés").", <a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;reprod=1\">".T_("clique ici pour en avoir")." !</a>";
        else
            $txtReproductivite = T_("Grossesse en cours...");    
    }
    $smarty->assign('txtReproductivite', $txtReproductivite);
}

$smarty->assign('msg', $msg);
$smarty->assign('nomHamster', $hamRow['nom']);
$smarty->assign('hamster_id', $hamRow['hamster_id']);

$isBebe = ($hamRow['date_naissance'] > ($dateActuelle - 86400));
$smarty->assign('isBebe', $isBebe);
$smarty->assign('imgHamster', "<img src=\"images/".$infosHamsters[$hamRow['type']][1]."\" style=\"width:200px; align:center;\" alt=\"".$infosHamsters[$hamRow['type']][0]."\" />");

if ($univers == UNIVERS_ELEVEUR) {

    // bloc central (construction nid, cabane et grossesse)
    
    $possedeCoton = $hamRow['possede_coton'];
    $possedePaille = $hamRow['possede_paille'];
    $possedeBrindilles = $hamRow['possede_brindilles'];
    if ($possedeCoton == 1 || $possedePaille == 1 || $possedeBrindilles == 1 || $hamRow['construction_nid'] > 0) {
        $blocCentralTxt .= "<div class=\"hamBlocColonne\">";
        $blocCentralTxt .= "<div class=\"hamTitreBloc\">".T_("Construction du nid")."</div>";
        // en attente d'ingrédient ?
        if ($hamRow['construction_nid'] > 0)
            $possedeCoton = $possedePaille = $possedeBrindilles = 1; // si l'hamster construit son nid, il possède forcément tous ces ingrédients

        $blocCentralTxt .= "<u>".T_("Matériaux nécessaires")."</u> : <br/>";
        $blocCentralTxt .= "<img src=\"images/coton.gif\" style=\"vertical-align:middle; height:30px;\" alt=\"\" /> ".T_("Coton")." : ".(($possedeCoton == 1)?"ok ! ":"---")."<br/>";
        $blocCentralTxt .= "<img src=\"images/paille.gif\" style=\"vertical-align:middle; height:30px;\" alt=\"\" /> ".T_("Paille")." : ".(($possedePaille == 1)?"ok ! ":"---")."<br/>";
        $blocCentralTxt .= "<img src=\"images/brindilles.gif\" style=\"vertical-align:middle; height:40px;\" alt=\"\" /> ".T_("Brindilles")." : ".(($possedeBrindilles == 1)?"ok ! ":"---");

        $blocCentralTxt .= "<br/>&nbsp;<br/>";
        $blocCentralTxt .= "<u>".T_("Evolution de la fabrication du nid")."</u> : <br/>";
        if ($hamRow['construction_nid'] == 0)
            $blocCentralTxt .= "&nbsp;&nbsp;&nbsp;&nbsp;".T_("en attente des matériaux nécessaires...");
        else if ($hamRow['construction_nid'] > 0 && ! isFabricationFinie($hamRow['construction_nid'],$hamRow['init_batisseur'])) {
            $blocCentralTxt .= "&nbsp;&nbsp;&nbsp;&nbsp;".T_("en cours : construit à ").calculerProgressionFabrication($hamRow['construction_nid'],$hamRow['init_batisseur'])." %";
            $blocCentralTxt .= " &nbsp;&nbsp;&nbsp;&nbsp; (<a href=\"banque.php?codePagePrec=nid_".$hamRow['hamster_id']."\">".T_("accélérer")." ?</a>)";
        }
        else {
            // on a attendu l'affichage du résltat du nid pour arrêter la construction du nid
            // on remet à zéro le paramètre de construction de nid
            $lst_hamsters[$hamsterIndex]['construction_nid'] = 0;
            $query = "UPDATE hamster 
                SET construction_nid = 0 WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ construction_nid', '', __LINE__, __FILE__, $query);
            }
            $blocCentralTxt .= "<img src=\"images/nid.gif\" alt=\"\" style=\"vertical-align:middle; height:50px;\" alt=\"\"> ".T_("Fabrication terminée ! Va vite voir dans la cage de ton hamster le résultat !");
        }
        $blocCentralTxt .= "</div>";
    }
    
    // pour la construction de la cabane
    $possedeBois = $hamRow['possede_bois'];
    $possedePlanCabane = $hamRow['possede_plan_cabane'];
    if ($possedeBois == 1 || $hamRow['construction_cabane'] > 0) {
        $blocCentralTxt .= "<div class=hamBlocColonne>";
        $blocCentralTxt .= "<div class=hamTitreBloc>".T_("Construction de la cabane")."</div>";
        // en attente d'ingrédient ?
        if ($hamRow['construction_cabane'] > 0)
            $possedeBois = $possedePlanCabane = 1; // si l'hamster construit sa cabane, il possède forcément tous ces ingrédients

        $blocCentralTxt .= "<u>".T_("Matériaux nécessaires")."</u> : <br/>";
        $blocCentralTxt .= "<img src=\"images/buches.gif\" style=\"vertical-align:middle; height:30px;\" alt=\"Bois\" /> ".T_("Bois")." : ".(($possedeBois == 1)?"ok ! ":"---")."<br/>";
        $blocCentralTxt .= "<img src=\"images/plan_cabane.png\" style=\"vertical-align:middle; height:40px;\" alt=\"Plans cabane\" /> ".T_("Plans de la cabane")." : ".(($possedePlanCabane == 1)?"ok ! ":"---");

        $blocCentralTxt .= "<br/>&nbsp;<br/>";
        $blocCentralTxt .= "<u>".T_("Evolution de la fabrication de la cabane")."</u> : <br/>";
        if ($hamRow['construction_cabane'] == 0)
            $blocCentralTxt .= "&nbsp;&nbsp;&nbsp;&nbsp;".T_("en attente des matériaux nécessaires...");
        else if ($hamRow['construction_cabane'] > 0 && ! isFabricationFinie($hamRow['construction_cabane'],$hamRow['init_batisseur']) ) {
            $blocCentralTxt .= "&nbsp;&nbsp;&nbsp;&nbsp;".T_("en cours : construite à ").calculerProgressionFabrication($hamRow['construction_cabane'],$hamRow['init_batisseur'])." %";
            $blocCentralTxt .= " &nbsp;&nbsp;&nbsp;&nbsp; (<a href=\"banque.php?codePagePrec=cab_".$hamRow['hamster_id']."\">".T_("accélérer")." ?</a>)";
        }
        else {
            // on a attendu l'affichage du résltat de la cabane pour arrêter la construction de la cabane
            // on remet à zéro le paramètre de construction de la cabane
            $lst_hamsters[$hamsterIndex]['construction_cabane'] = 0;
            $query = "UPDATE hamster 
                SET construction_cabane = 0 WHERE hamster_id=".$hamster_id;
            if ( !($dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error updating hamster _ construction_cabane', '', __LINE__, __FILE__, $query);
            }
            $blocCentralTxt .= "<img src=\"images/maison2.gif\" style=\"vertical-align:middle; height:50px;\" alt=\"Nid\" /> ".T_("Fabrication terminée ! Va vite voir dans la cage de ton hamster le résultat !");
        }
        $blocCentralTxt .= "</div>";
    }    

    // suivi de la grossesse
    if ($hamRow['sexe'] > 1) {
        $blocCentralTxt .= "<div class=\"hamBlocColonne\">";
        $blocCentralTxt .= "<div class=\"hamTitreBloc\">".T_("Grossesse en cours")."</div>";
        
        if ( ($hamRow['sexe']+$dureeGrossesse) > $dateActuelle) { // grossesse en progression
            $blocCentralTxt .= "<img src=\"images/couffin.gif\" alt=\"Bébés : \" style=\"vertical-align:middle;\" /> ".T_("grossesse à ").(100-round((($hamRow['sexe']+$dureeGrossesse-$dateActuelle)*100.) / $dureeGrossesse))." %";
            $blocCentralTxt .= " : ".T_("il reste")." ".round(($hamRow['sexe']+$dureeGrossesse-$dateActuelle)/(3600*24))." ".T_("jour(s)");
            $blocCentralTxt .= " &nbsp;&nbsp;&nbsp;&nbsp; (<a href=\"banque.php?codePagePrec=gro_".$hamRow['hamster_id']."\">".T_("accélérer")." ?</a>)";
        }
        else { // fin de la grossesse
            $blocCentralTxt .= "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;voirbebes=1\"><img src=\"images/couffin.gif\" style=\"vertical-align:middle;\" alt=\"\"> ".T_("Les bébés sont arrivés, prends-en soin ! Clique sur le couffin pour les voir !")."</a>";
        }
        $blocCentralTxt .= "</div>";
    } 
    
   // bloc d'actions
   // --------------
    if ($hamRow['sante']==0) {
        $blocActionTxt .= "<div class=\"elementAction\">";
        $blocActionTxt .= "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;action=chezleveto\" title=\"".T_("Cout pour le récupérer")." : 150\"><img src=\"images/Veterinaire_reduc.gif\" alt=\"\" style=\"vertical-align:middle;\" /> ".T_("Le récupérer du véto")."</a>"; 
        $blocActionTxt .= "</div>";
    }
    else {
        
        ob_start();
        
        if ($userdata['aliments'] > 0) {
            if ($faim < 5 || $hamRow['poids'] < 110)
                afficherAction("nourrir",$hamRow['hamster_id'],T_("Remplir l'écuelle"),"ecuelle_vide_pleine_reduc.gif");
            else
                afficherActionDesactivee("nourrir","L'écuelle est déjà pleine",$hamRow['hamster_id'],T_("Remplir l'écuelle"),"ecuelle_vide_pleine_reduc.gif");
        }
        afficherAction("caresser",$hamRow['hamster_id'],T_("Caresser le hamster"),"mains2.gif");

        if ($userdata['niveau'] >= NIVEAU_GESTION_ETAGE) {
            if (possedeAccessoire(ACC_STAGE_PLEIN_AIR) != -1) {
                afficherAction("stagepleinair",$hamRow['hamster_id'],T_("Inscrire le hamster au stage de plein-air"),"turimo_pleinair.gif","");
            }
            
            // le joueur possede des vitamines ?
            if (possedeAccessoire(ACC_VITAMINES) != -1 || possedeAccessoire(ACC_VITAMINES_PACK5) != -1) {
                afficherAction("vitamines",$hamRow['hamster_id'],T_("Donner ses vitamines"),"vitamines_reduc3.gif","");
            }
            
            // a-t-il des haltères ? 
            if (possedeAccessoire(4) != -1) {
                afficherAction("halteres",$hamRow['hamster_id'],T_("Faire de la muscu"),"halteres_reduc.gif","");
            }
            // a-t-il de la crème ? 
            if (possedeAccessoire(ACC_CREME) != -1) {
                afficherAction("mettrecreme",$hamRow['hamster_id'],T_("Appliquer de la crème sur le pelage"),"creme_reduc.gif","");
            }
            
            // a-t-il de la crème hamsteropoil ? 
            if (possedeAccessoire(ACC_CREME_HAMSTEROPOIL) != -1) {
                afficherAction("mettrehamsteropoil",$hamRow['hamster_id'],T_("Appliquer la crème HamsterOPoil"),"flacon_hamsteropoil.gif","");
            }
            // a-t-il le médicament la Chorinthe et son hamster est-il malade ? 
            if (possedeAccessoire(ACC_MEDIC_CHORINTHE) != -1 && $hamRow['maladie'] == MALADIE_CHORINTHE) {
                afficherAction("donnerMedicChorinthe",$hamRow['hamster_id'],T_("Donner la chorynthe"),"medicament_pierre_ponce.png","");
            }
            // a-t-il le médicament la Chorinthe et son hamster est-il malade ? 
            if (possedeAccessoire(ACC_MEDIC_BRAISE_NOISETTE) != -1 && $hamRow['maladie'] == MALADIE_STALACTIC) {
                afficherAction("donnerMedicBraiseNoisette",$hamRow['hamster_id'],T_("Donner la braise de noisette"),"pastille_chaleur.png","");
            }

            // a-t-il une brosse ? 
            if (possedeAccessoire(ACC_BROSSE) != -1){
                afficherAction("brosser",$hamRow['hamster_id'],T_("Brosser le hamster"),"brosse_a_manche.gif","",50);
            }
            
            // a-t-il des bonbons ? 
            if (possedeAccessoire(ACC_BONBON) != -1){
                afficherAction("donnerBonbon",$hamRow['hamster_id'],T_("Donner des bonbons"),"bonbons.jpg","",70);
            }
            
            // a-t-il du coton ? 
            if (possedeAccessoire(ACC_COTON) != -1){
                afficherAction("donnerCoton",$hamRow['hamster_id'],T_("Donner le coton"),"coton.gif","",50);
            }
            
            // a-t-il de la paille ? 
            if (possedeAccessoire(ACC_PAILLE) != -1){
                afficherAction("donnerPaille",$hamRow['hamster_id'],T_("Donner la paille"),"paille.gif","",50);
            }
            
            // a-t-il des brindilles ? 
            if (possedeAccessoire(ACC_BRINDILLES) != -1){
                afficherAction("donnerBrindilles",$hamRow['hamster_id'],T_("Donner les brindilles"),"brindilles.gif","",50);
            }
            
            // a-t-il la formation batisseur ? 
            if (possedeAccessoire(ACC_FORMATION_BATISSEUR) != -1){
                afficherAction("donnerFormationBatisseur",$hamRow['hamster_id'],T_("Inscrire le hamster à la formation de Bâtisseur"),"diplome.gif","",50);
            }
            
            // a-t-il le bois ? 
            if (possedeAccessoire(ACC_BOIS) != -1){
                afficherAction("donnerBois",$hamRow['hamster_id'],T_("Donner le bois"),"buches.gif","Bois",50);
            }
            
            // a-t-il le plan de la cabane ? 
            if (possedeAccessoire(ACC_PLAN_CABANE) != -1){
                afficherAction("donnerPlanCabane",$hamRow['hamster_id'],T_("Donner les plans de la cabane"),"plan_cabane.png","",50);
            }                                        
            
            // a-t-il de la salade ? 
            if (possedeAccessoire(ACC_SALADE) != -1){
                afficherAction("donnerSalade",$hamRow['hamster_id'],T_("Donner une feuille de salade"),"salade.gif","");
            }                            
            // a-t-il une boule rose ? (uniquement le lundi et jeudi)
            if (possedeAccessoire(ACC_BOULE_ROSE) != -1){
                if ($jourDansLaSemaine == 1 || $jourDansLaSemaine == 4)
                    afficherAction("bouleRose",$hamRow['hamster_id'],T_("Faire de la boule"),"boule_rose.gif","");
                else
                    afficherActionDesactivee("bouleRose",T_("Seulement le lundi et jeudi"),$hamRow['hamster_id'],T_("Faire de la boule"),"boule_rose.gif","");
            }                        
            // a-t-il une boule rose ? 
            if (possedeAccessoire(ACC_BOULE_NOIRE) != -1){
                if ($jourDansLaSemaine == 1 || $jourDansLaSemaine == 4)
                    afficherAction("bouleNoire",$hamRow['hamster_id'],T_("Faire de la boule"),"boule_noire.gif","");
                else
                    afficherActionDesactivee("bouleNoire",T_("Seulement le lundi et jeudi"),$hamRow['hamster_id'],T_("Faire de la boule"),"boule_noire.gif","");
            }                        
            // a-t-il une sortie rugby ? 
            if (possedeAccessoire(ACC_RUGBY_JEU) != -1){
                if ($jourDansLaSemaine == 6)
                    afficherAction("sortieRugby",$hamRow['hamster_id'],T_("Jouer au rugby"),"rugby.gif","",-1,60);
                else
                    afficherActionDesactivee("sortieRugby",T_("Seulement le samedi"),$hamRow['hamster_id'],T_("Jouer au rugby"),"rugby.gif","",-1,60);
            }                                
            // a-t-il une sortie foot ? 
            if (possedeAccessoire(ACC_FOOT_JEU) != -1){
                if ($jourDansLaSemaine == 3 || $jourDansLaSemaine == 0)
                    afficherAction("sortieFoot",$hamRow['hamster_id'],T_("Jouer au football"),"football.gif","");
                else
                    afficherActionDesactivee("sortieFoot",T_("Seulement le mercredi et dimanche"),$hamRow['hamster_id'],T_("Jouer au football"),"football.gif","");
            }
            // sortie tennis ?
            if (possedeAccessoire(ACC_TENNIS_JEU) != -1){
                afficherAction("sortieTennis",$hamRow['hamster_id'],T_("Jouer au tennis"),"tennis.jpg","");
            }                                
            // a-t-il une laisse ? 
            if (possedeAccessoire(ACC_LAISSE) != -1)
                afficherAction("promenerHamster",$hamRow['hamster_id'],T_("Promener le hamster"),"laisse.gif","");    
                
            // a-t-il une laisse (rose) ? 
            if (possedeAccessoire(ACC_LAISSE_ROSE) != -1)
                afficherAction("promenerHamster",$hamRow['hamster_id'],T_("Promener le hamster"),"laisse2.gif","");

            // a-t-il un calendrier de l'Avent ? 
            if ( ($month == 11 || $month == 12) && possedeAccessoire(ACC_CALENDRIER_AVENT) != -1){
                echo "<div class=\"elementAction\">";
                echo "<table cellpadding=\"5\"><tr><td><a href=\"images/calendrier_avent.jpg\" target=\"hblank\"><img src=\"images/calendrier_avent_red.jpg\" alt=\"\" /></a></td><td>".T_("Tu as le calendrier : chaque jour, tu auras un chocolat par boîte achetée !")."</td></tr></table>" ;
                echo "</div>";
            }                        
        
            // a-t-il reçu un chocolat r de l'Avent ? 
            if ( ($month == 12 || $month == 1) && possedeAccessoire(ACC_CALENDRIER_CHOCOLAT) != -1){
                echo "<div class=\"elementAction\">";
                echo "<a href=\"jeu.php?mode=m_hamster&amp;action=donnerChocolatAvent&amp;hamster_id=".$hamRow['hamster_id']."\"><img src=\"images/calendrier_chocolat.gif\" alt=\"\" /> ".T_("Donner le chocolat au hamster")."</a>" ;
                echo "</div>";
            }    
            // a-t-il une console de jeu ? (uniquement ... le mercredi et le dimanche)
            if (possedeAccessoire(ACC_CONSOLE_JEU) != -1){
                if ($jourDansLaSemaine == 3 || $jourDansLaSemaine == 0)
                    afficherAction("consolejeu",$hamRow['hamster_id'],T_("Jouer à la console"),"console.gif","",50);
                else
                    afficherActionDesactivee("consolejeu",T_("Seulement le mercredi et le dimanche"),$hamRow['hamster_id'],T_("Jouer à la console"),"console.gif","",50);
            }            
            // a-t-il une galette des rois ?
            if (possedeAccessoire(ACC_GALETTE_ROIS) != -1 || possedeAccessoire(ACC_PART_GALETTE) != -1){
                afficherAction("donnerPartGalette",$hamRow['hamster_id'],T_("Donner une part de Galette"),"part_galette.gif","");
            }
            // a-t-il un masque de catch ?
            if (possedeAccessoire(ACC_TENUE_CATCH) != -1){
                if ($jourDansLaSemaine == 1 || $jourDansLaSemaine == 4)
                    afficherAction("partieCatch",$hamRow['hamster_id'],T_("Faire du catch"),"masque_catch.png","");
                else
                    afficherActionDesactivee("partieCatch",T_("Seulement le lundi et jeudi"),$hamRow['hamster_id'],T_("Faire du catch"),"masque_catch.png","");
            }
            // a-t-il une fiole d'énergie ?
            if (possedeAccessoire(ACC_FIOLE) != -1){
                afficherAction("donnerFioleEnergie",$hamRow['hamster_id'],T_("Donner la fiole d'énergie"),"fiole.gif","");
            }                    
            
            // livre de beauté ?
            if ($hamRow['init_coquet'] < 5 && possedeAccessoire(ACC_LIVRE_BEAUTE) != -1){
                afficherAction("donnerLivreBeaute",$hamRow['hamster_id'],T_("Donner le Traité de beauté"),"livre_beaute.gif","");
            }                    
            
            // thermomètre
            if (possedeAccessoire(ACC_THERMOMETRE) != -1){
                afficherAction("prendreTemperature",$hamRow['hamster_id'],T_("Prendre la température"),"thermometre.png","");
            }
            
            // menthe des glaces dans le biberon
            if (possedeAccessoire(ACC_MEDIC_MENTHE_GLACE) != -1 && $hamRow['maladie'] == MALADIE_FIEVRE) {
                afficherAction("donnerMedicMentheGlace",$hamRow['hamster_id'],T_("Mettre la Menthe des Glaces dans le biberon"),"glacon_plante.jpg","");
            }
            
            // possède une roue motorisée
            if (yatilAccessoireDansLaCage($hamRow['cage_id'], ACC_ROUE_AVEC_ENGRENAGE_PILE_FIL,$userdata['joueur_id']) != -1){
                afficherAction("mettreDansRoueMotorisee",$hamRow['hamster_id'],T_("Mettre le hamster dans sa roue et allumer la pile"),"roue3_engrenage_pile_fil.gif","",70);
            }
            // potage du rongeur (mission 7)
            if ($userdata['niveau'] >= NIVEAU_POTAGE && getObjectifNiveau($userdata['niveau_objectif'],7) ) {
                if (possedeAccessoire(ACC_LOUCHE) != -1)
                    afficherAction("donnerPotageRongeur",$hamRow['hamster_id'],T_("Donner le Potage du Rongeur"),"ingredients/louche.gif","");
                else
                 afficherAction("donnerPotageRongeur",$hamRow['hamster_id'],T_("Donner le Potage du Rongeur"),"ingredients/marmite.gif","",40);
            }            
            echo "<br/>&nbsp;<br/><div class=\"elementAction\" style=\"font-size:10pt;\">".T_("Rappel : tu trouveras dans la ")."<a href=\"jeu.php?mode=m_achat&amp;pagePrecedente=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."\">".T_("Boutique")."</a> ".T_("de quoi t'occuper de ton hamster")."...</div>";
        }
        
        $blocActionTxt .= ob_get_contents();
        ob_end_clean();
    }
    
    // infos sur la cage
    $cageIndex = cageCorrespondante($hamRow['cage_id']) ; 
    $cageRow = $lst_cages[$cageIndex];
    $smarty->assign('cage_id', $cage_id);
    $cage_nom = tronquerTxt($cageRow['nom'],15) ;
    $smarty->assign('cage_nom', $cage_nom);
    $cage_proprete = $cageRow['proprete'];
    $smarty->assign('cage_proprete', $cage_proprete);
    $smarty->assign('nb_cages', $nbCages);
           
    if ($nbCages > 1) { // on propose un changement de cage si le joueur  en possede plus de 2
        ob_start(); 
        for($c=0;$c<$nbCages;$c++) {
             $cage = $lst_cages[$c];
             // on ne considere pas la cage dans laquelle l'hamster se trouve deja
             if ($cage['cage_id'] != $hamRow['cage_id']) {
                $cage_ind = $c+1;
                echo "<option value=\"".$cage['cage_id']."\" >".T_("cage")." n°".$cage_ind." : ".$cage['nom']."</option>\n";
            }
        }
        
        $lstCagesPourTransfert = ob_get_contents();
        ob_end_clean();
        $smarty->assign('lstCagesPourTransfert', $lstCagesPourTransfert);
    }
    
}
else if($univers == UNIVERS_ACADEMY || $univers == UNIVERS_FOOT) { 
    
    ob_start();
    
    if ($hamRow['inscrit_academy'] == 1 && $hamRow['sante'] > 0) { 
     
        afficherActionAcademy("coursChant",$hamRow['hamster_id'],T_("Cours de chant")." : ".$coutCoursChant." ".IMG_PIECE,"micro.gif");
        
        if ($hamRow['specialite'] != -1) {
            
            $queryImage = "SELECT image, nom_fr FROM config_accessoires WHERE type=".$lstInstruments[$hamRow['specialite']][INSTRUMENT_REF_OBJET]." LIMIT 1";
            if ( !($resultImage = $dbHT->sql_query($queryImage)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            }
            $instrumentImage=$dbHT->sql_fetchrow($resultImage);
            $dbHT->sql_freeresult($resultImage);
            
            afficherActionAcademy("coursInstrument",$hamRow['hamster_id'],$instrumentImage['nom_fr']." : ".$coutCoursGuitare." ".IMG_PIECE,$instrumentImage['image']);
        }
        
        afficherActionAcademy("apprendreBlague",$hamRow['hamster_id'],T_("Apprendre des blagues")." : ".$coutCoursBlague,"rire.gif");
                    
        if (($hamRow['niveau']+1) >= NIVEAU_NOURRIR_COPEAUX) {
            
            afficherActionAcademy("techniqueImpro",$hamRow['hamster_id'],T_("Matchs d'impro")." : ".$coutCoursMatchImpro." ".IMG_PIECE,"rire.gif");
            afficherActionAcademy("coursDanse",$hamRow['hamster_id'],T_("Cours de danse")." : ".$coutCoursDanse." ".IMG_PIECE,"danse.gif");
            afficherActionAcademy("coursDanseTektonic",$hamRow['hamster_id'],T_("Cours de Tektonic")." : ".$coutCoursDanseTektonic." ".IMG_PIECE,"danse.gif","",40);
        }
        else {
            echo "<br/>&nbsp;<br/><em>".T_("Seulement accessible aux Académiciens confirmés")." :</em><br/>";
            afficherActionDesactiveeAcademy("techniqueImpro",T_("Seulement accessible aux Académiciens confirmés"),$hamRow['hamster_id'],T_("Matchs d'impro")." : ".$coutCoursMatchImpro." ".IMG_PIECE,"rire.gif");
            afficherActionDesactiveeAcademy("coursDanse",T_("Seulement accessible aux Académiciens confirmés"),$hamRow['hamster_id'],T_("Cours de danse")." : ".$coutCoursDanse." ".IMG_PIECE,"danse.gif");
            afficherActionDesactiveeAcademy("coursDanseTektonic",T_("Seulement accessible aux Académiciens confirmés"),$hamRow['hamster_id'],T_("Cours de Tektonic")." : ".$coutCoursDanseTektonic." ".IMG_PIECE,"danse.gif");
        }

         if (($hamRow['niveau']+1) == NIVEAU_NOURRIR_COPEAUX) {
            echo "<br/>".T_("D'autres possibilités apparaîtront au niveau suivant.");
        }          
    }
    else if ($hamRow['inscrit_academy'] == 2 && $hamRow['sante'] > 0) {

        afficherActionAcademy("coursDribble",$hamRow['hamster_id'],T_("Travailler le dribble")." : ".(($hamRow['niveau']+1)*$coutCoursFoot*2).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursPasse",$hamRow['hamster_id'],T_("Améliorer les passes")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursDefense",$hamRow['hamster_id'],T_("Travailler le marquage")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursTackle",$hamRow['hamster_id'],T_("Travailler le tackle")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursEndurance",$hamRow['hamster_id'],T_("Endurance")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursCoupsFrancs",$hamRow['hamster_id'],T_("Coups francs")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursTirsButs",$hamRow['hamster_id'],T_("Tirs au but")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursTactiques",$hamRow['hamster_id'],T_("Cours de tactiques")." : ".(($hamRow['niveau']+1)*$coutCoursFoot*2).IMG_PIECE,"ballon_foot.gif","",20,20);
        afficherActionAcademy("coursGardien",$hamRow['hamster_id'],T_("Arrêts de buts")." : ".(($hamRow['niveau']+1)*$coutCoursFoot).IMG_PIECE,"ballon_foot.gif","",20,20);

    }
    $blocActionTxt = ob_get_contents();
    ob_end_clean();
    ob_start();
    
       if ($hamRow['sante']==0) {
           echo "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$hamRow['hamster_id']."&amp;action=chezleveto\" title=\"".T_("Cout pour le récupérer")." : 150\"><img src=\"images/Veterinaire_reduc.gif\" alt=\"\" align=absmiddle> ".T_("Revenir dans l'univers Eleveur et aller chez le véto")."</a>"; 
           //echo "</div>";
       }
       else {
           if ($hamRow['inscrit_academy'] == 0) {
                afficherActionAcademy("inscrireAcademy",$hamRow['hamster_id'],T_("Inscrire ").tronquerTxt($hamRow['nom'],15).T_(" à l'Academy"),"etoile.gif","",20,20);
                if ($userdata['niveau'] >= NIVEAU_GROUPE_MUSIQUE)
                    afficherActionAcademy("inscrireAcademyFoot",$hamRow['hamster_id'],T_("Inscrire ").tronquerTxt($hamRow['nom'],15).T_(" à la Foot Academy"),"ballon_foot.gif","",20,20);
                else
                    afficherActionDesactiveeAcademy("inscrireAcademyFoot",T_("accessible au niveau ")." ".NIVEAU_GROUPE_MUSIQUE,$hamRow['hamster_id'],T_("Inscrire ").tronquerTxt($hamRow['nom'],15).T_(" à la Foot Academy"),"ballon_foot.gif","",20,20);
           }
           else {
               if ($hamRow['inscrit_academy'] == 1) {
                   if($hamRow['niveau'] < NIVEAU_ACADEMY_FIN)
                       afficherActionAcademy("examen",$hamRow['hamster_id'],T_("Passer l'examen"),"micro.gif","");
                       
                   afficherActionAcademy("confesser",$hamRow['hamster_id'],T_("Se confier devant le public"),"cameras.gif","");
              
                    $chance = rand(1,5) ;
                    if ($chance == 1)
                        afficherActionAcademy("raconterBlagueCouloir",$hamRow['hamster_id'],T_("Raconter une blague dans le couloir"),"rire.gif","");
               }
               else {
                   
               }
               afficherActionAcademy("changerAcademy",$hamRow['hamster_id'],T_("Changer d'Academy"),"etoile.gif","",20,20);
           }
       }
       
    $blocActionAcademyTxt = ob_get_contents();
    ob_end_clean();
    }
}

$smarty->assign('blocCentralTxt', $blocCentralTxt);
$smarty->assign('blocActionTxt', $blocActionTxt);
$smarty->assign('blocActionAcademyTxt', $blocActionAcademyTxt);


if ($afficherColonnes){
    if ($univers == UNIVERS_ELEVEUR)
        $smarty->display('hamsterEleveur.tpl');  
    else if ($univers == UNIVERS_ACADEMY || $univers == UNIVERS_FOOT)
        $smarty->display('hamsterAcademy.tpl');  
    else
        $smarty->display('hamsterEleveur.tpl');  // au cas où 
}
