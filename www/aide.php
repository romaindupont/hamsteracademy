<?php 
define('IN_HT', true);
define('IN_HT_AIDE_PHP', true);

include "common.php";

$univers=0;
if (isset($_GET['univers'])) {
    $univers = intval($_GET['univers']);
    if ($univers > 3)
        $univers = 0;
    else if ($univers < 0)
        $univers = 0;
}

$pagetitle = "Hamster Academy - ".T_("Règles du jeu")." - ";
if ($univers == 0) $pagetitle .= T_("Univers Eleveur");
else if($univers == 1 ) $pagetitle .= T_("Univers Academy"); 
else $pagetitle .= T_("Questions générales") ;

$description = T_("Aide de Hamster Academy");
if ($univers == 0) $description .= T_(" pour l'Univers Eleveur : retrouve ici plein de conseils pour mieux élever ton hamster ! Comment nettoyer sa cage, lui faire du sport, le rendre beau...");
else if($univers == 1 ) $description .= T_(" pour l'Univers Academy : comment rendre son hamster musicien, qu'est-ce qu'un groupe, quels sont les instruments de musique, ..."); 
else $description .= T_(" : questions générales : le jeu est-il gratuit ? (oui!) comment se faire des amis ? ...");

$liste_styles = "style.css,styleEleveur.css";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>
    
<h1 align="center">Hamster Academy</h1>
<h2 align="center"><?php echo T_("Règles du jeu"); ?></h2>

<?php 
require "jeu_aide.php";
?>

<div align=center><a href="index.php"><?php echo T_("Retour à l'accueil"); ?></a></div>

<br/>

<?php require "footer.php" ;  ?>

