<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require "graphique.php" ;    

$cageRow = $lst_cages[$cageIndex];

// évènements si rien ne se passe
if ($action == "") {
    $chanceEvenement = rand(1,40);
    
    // l'hamster a rongé les barreaux, faut repeindre (sauf si la cage possède une pierre à ronger...)
    if ($chanceEvenement < 3) {
        if (yatilPierreRongerDansLaCage($cageRow['cage_id'],$userdata['joueur_id']) == false && nbHamstersDansLaCage($cageRow['cage_id']) > 0) {
            $msg .= afficherNouvelle("pinceau.gif",T_("Les barreaux ont été complètement rongés... Il a fallu repeindre toute la cage (moins cher que de racheter une cage neuve !). Ca arrive de temps en temps... Frais de peinture : 2 pièces"));
            debiterPieces(2);
        }
    }
    else if ($chanceEvenement == 3) {
        if (nbHamstersDansLaCage($cageRow['cage_id'],$userdata['joueur_id']) > 1) {
            $msg .= afficherNouvelle("bagarre.jpg",T_("Aïe aïe, il y a eu une bagarre entre les hamsters ! Ca arrive souvent, les hamsters sont des animaux solitaires ! A cause de leurs blessures, leur santé a diminué... S'ils pouvaient avoir chacun leur propre cage, ce serait mieux !"));

            for($ha=0;$ha<$nbHamsters;$ha++) {
                if ($lst_hamsters[$ha]['cage_id'] == $cage_id && $lst_hamsters[$ha]['sante'] > 0) {
                    
                    $nouvelleSante = max(0.5,$lst_hamsters[$hamsterIndex]['sante']-2);                    
                    $lst_hamsters[$hamsterIndex]['sante'] = $nouvelleSante;
    
                    $query = "UPDATE hamster 
                        SET sante = ".$nouvelleSante."
                        WHERE hamster_id=".$lst_hamsters[$ha]['hamster_id'];
                    if ( !($dbHT->sql_query($query)) ){
                        message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                    }    
                }
            }
        }
    }
}

$coutAgrandissementSurface = intval($coutFabrication + $coutCube * $cageRow['etages']  * $cageRow['profondeurs'] * $cageRow['colonnes']);
$coutAgrandissementHauteur = intval($coutFabrication + $coutCube * $cageRow['colonnes'] * $cageRow['profondeurs']);
$coutVente = intval($coutCube * $cageRow['colonnes'] * $cageRow['profondeurs'] / 2);

if ($action == "remplacerCopeaux"){
    $msgTmp = "";
    if ($remplacement_copeaux_ok == false)
        $msgTmp = T_("Tu n'as pas assez de copeaux pour les remplacer !") ;
    else
        $msgTmp = T_("La cage possède des copeaux tout neufs. Ton hamster te remercie!");
    $msg .= afficherNouvelle("copeaux2.gif",$msgTmp);
}
else if ($action == "peindre"){
    if ($erreurAction == 0)
        $msg .= afficherNouvelle("pinceau.gif",T_("La cage a été peinte avec succès ! La nouvelle couleur te plaît?"));
}
else if ($action == "assembler"){
    if ($erreurAction == 1)
        $msg .= afficherNouvelle("roue3_engrenage.gif",T_("L'engrenage et son moteur intégré ont été fixés à la roue !"));
    else if ($erreurAction == 2)
        $msg .= afficherNouvelle("roue3_engrenage_pile.gif",T_("Il faudrait relier la pile au moteur de l'engrenage..."));
    else if ($erreurAction == 3)
        $msg .= afficherNouvelle("roue3_engrenage_pile_fil.gif",T_("Ca y est ! La roue est motorisée ! Il ne reste plus qu'à mettre le hamster dans la roue et à allumer la pile ! Il devrait courir beaucoup plus vite !"));
}
else if ($action == "agrandirSurface"){
 if ($erreurAction == ERREUR_ACTION_CAGE_PROFONDEUR || $erreurAction == ERREUR_ACTION_CAGE_COLONNE) {
        $messageTmp = T_("Tu n'as pas assez d'argent pour agrandir la cage.");
        $msg .= "<div>".afficherPasAssezArgent($messageTmp,PAGE_JEU_CAGE);
        $msg .= "</div>";
    }
}
else if ($action == "ajouterEtage"){
    if ($erreurAction == ERREUR_ACTION_CAGE_ETAGE) {
        $messageTmp = T_("Tu n'as pas assez d'argent pour ajouter un étage.");
        $msg .= "<div>".afficherPasAssezArgent($messageTmp,PAGE_JEU_CAGE);
        $msg .= "</div>";
    }
}
else if ($action == "vendreCage") {
    if ($erreurAction == 0) {
        $msg .= "<div>".afficherNouvelle("poubelle.gif",T_("La cage a été vendue !"),"",-1,-1);
        $msg .= "</div>";
    }
    else if ($erreurAction == 1) {
        $msg .= "<div>".afficherNouvelle("poubelle.gif",T_("Pour vendre la cage, il ne faut pas qu'il y ait un seul hamster dedans."),"",-1,-1);
        $msg .= "</div>";
    }
    else if ($erreurAction == 2) {
        $msg .= "<div>".afficherNouvelle("poubelle.gif",T_("Pour vendre la cage, il ne faut pas qu'il y ait un seul objet dedans."),"",-1,-1);
        $msg .= "</div>";
    }    
}
if ($cageRow['proprete'] < 3){
    $msg .= afficherNouvelle("odeurs.gif",T_("Ouh la la ! La cage commence à sentir mauvais ! C'est peut-être le bon moment pour changer les copeaux..."));
}

// s'il n'y a plus de copeaux en stock, inciter à en acheter :
if ($userdata['copeaux'] <= 0) 
    $msg .= "<div>".T_("Attention ! Tu n'as plus de copeaux.")." <a href=\"jeu.php?mode=m_achat&objet_a_acheter=3&pagePrecedente=m_cage&cage_id=".$cageRow['cage_id']."\">".T_("En acheter ?")."</a></div><br/>" ;

$lstHabitants = "";
$nbHamstersDansLaCage = 0;
$lstHamstersDansLaCage = array();
for($ha=0;$ha<$nbHamsters;$ha++) {
    $rowHamster = $lst_hamsters[$ha];
    if ($rowHamster['cage_id'] == $cageRow['cage_id']){
        $nbHamstersDansLaCage++;
        array_push($lstHamstersDansLaCage,$rowHamster['hamster_id']);
        if ($rowHamster['type']==2 || $rowHamster['type']==3)
            $imageHamster = "hamster_reduc2.gif";
        else
            $imageHamster = "hamster_reduc.gif";
        $lstHabitants .= "<tr><td align=\"center\"><img src=\"images/".$imageHamster."\"  alt=\"hamster\" style=\"vertical-align:middle\" /></td><td align=\"left\"> <a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".$rowHamster['hamster_id']."\">".tronquerTxt($rowHamster['nom'],15)."</a></td></tr>";
    }
}

$dessinCage = afficherCage($cageRow,true, $lst_accessoires, $nbAccessoires) ;

$txtInscriptionConcours = "";
if (getConfig("concours_cage_auto_active") == 1 && $userdata['inscritConcours'] == 1 && $nbCages > 1 && $day >= 2 && $day <= 15) {
   
    $txtInscriptionConcours = "<br/>&nbsp;<br/>".T_("Tu es inscrit(e) au concours : pour sélectionner la cage qui sera notée pour le concours, tu dois aller en Ville->Mairie->Concours. Bonne chance !");
} 

$commandeCopeaux = "";
if ($userdata['copeaux'] <= 0) 
    $commandeCopeaux = "<img src=\"images/copeaux2.png\" alt=\"\" title=\"".T_("Acheter des copeaux")."\"> <a href=\"jeu.php?mode=m_achat&amp;objet_a_acheter=3&amp;pagePrecedente=m_cage&amp;cage_id=".$cageRow['cage_id']."\">".T_("Acheter des copeaux")."</a>";
else
    $commandeCopeaux = "<a href=\"jeu.php?mode=m_cage&amp;action=remplacerCopeaux&amp;cage_id=".$cageRow['cage_id']."\" title=\"".T_("consomme 1 litre de copeaux")."\"><img src=\"images/copeaux_sales_propres2.gif\" alt=\"\" style=\"vertical-align:middle;\" /> ".T_("Changer les copeaux")."</a><br/>";

$peutModifierCage = ($userdata['niveau'] >= NIVEAU_GESTION_ETAGE) ;
$peutRajouterColonne = ($cageRow['colonnes'] < 2);
    
$smarty->assign('proprete',$cageRow['proprete']) ;
$smarty->assign('lstHabitants',$lstHabitants) ;
$smarty->assign('nomCage',$cageRow['nom']) ;
$smarty->assign('cageId',$cageRow['cage_id']) ;
$smarty->assign('dessinCage',$dessinCage) ;
$smarty->assign('msg',$msg) ;
$smarty->assign('niveauJoueur',$userdata['niveau']) ;
$smarty->assign('txtInscriptionConcours',$txtInscriptionConcours) ;
$smarty->assign('commandeCopeaux',$commandeCopeaux) ;
$smarty->assign('peutModifierCage',$peutModifierCage) ;
$smarty->assign('peutRajouterColonne',$peutRajouterColonne) ;
$smarty->assign('coutAgrandissementSurface',$coutAgrandissementSurface) ;
$smarty->assign('coutAgrandissementHauteur',$coutAgrandissementHauteur) ;

$classSupplCage = "";
if ($userdata['pseudo'] == "César")
    $classSupplCage = " cageNeige";

$actionFacebook = "";
if ($sessionFacebook) {
    $query = "SELECT * FROM facebook WHERE joueur_id=".$userdata['joueur_id'];
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbComptes = $dbHT->sql_numrows($result) ;
    
    if ($nbComptes != 0)
        $actionFacebook = "<a href=\"#\" onclick='javascript:publierFacebook(\"publishCage\",".$cageRow['cage_id'].")' > <img src=\"images/FaceBook_48x48.png\" height=\"20\" /> ".T_("Publier la cage sur Facebook")."</a>";
    $dbHT->sql_freeresult($result);
}   
$smarty->assign('actionFacebook',$actionFacebook) ; 
    
$smarty->assign('classSupplCage',$classSupplCage) ;

$smarty->assign('coutPeinture',$coutPeinture) ;

$smarty->assign('FOND_CAGE_BLEUVERT',FOND_CAGE_BLEUVERT) ;
$smarty->assign('FOND_CAGE_ROSE',FOND_CAGE_ROSE) ;
$smarty->assign('FOND_CAGE_NOIR',FOND_CAGE_NOIR) ;
$smarty->assign('FOND_CAGE_BLANC',FOND_CAGE_BLANC) ;

$smarty->assign('FOND_CAGE_COEUR',FOND_CAGE_COEUR) ;
$smarty->assign('FOND_CAGE_FEU',FOND_CAGE_FEU) ;
$smarty->assign('FOND_CAGE_HALLOWEEN',FOND_CAGE_HALLOWEEN) ;
$smarty->assign('FOND_CAGE_MER',FOND_CAGE_MER) ;
$smarty->assign('FOND_CAGE_NEIGE',FOND_CAGE_NEIGE) ;

$smarty->assign('coutPeinture',$coutPeinture) ;
$smarty->assign('coutPeintureCoeur',$coutPeintureCoeur) ;
$smarty->assign('coutPeintureFeu',$coutPeintureFeu) ;
$smarty->assign('coutPeintureHalloween',$coutPeintureHalloween) ;
$smarty->assign('coutPeintureMer',$coutPeintureMer) ;
$smarty->assign('coutPeintureNeige',$coutPeintureNeige) ;

$actionEolienne = "";
if (yatilAccessoireDansLaCage($cageRow['cage_id'],ACC_EOLIENNE_ENERGIE) != -1){
    if ($cageRow['eolienne_activee'] == 0)
        $actionEolienne = "<a href=\"jeu.php?univers=$univers&amp;cage_id=".$cageRow['cage_id']."&amp;action=allumerEolienne&amp;mode=m_cage\"><img src=\"images/eolienne_arret.gif\" height=\"70\"/>".T_("Allumer l'éolienne d'énergie")."</a>";
    else
        $actionEolienne = "<a href=\"jeu.php?univers=$univers&amp;cage_id=".$cageRow['cage_id']."&amp;action=eteindreEolienne&amp;mode=m_cage\"><img src=\"images/eolienne_arret.gif\" height=\"70\" />".T_("Eteindre l'éolienne")."</a>";
}
$smarty->assign('actionEolienne',$actionEolienne) ;

// fabrication de la roue avec engrenage
$actionRoueMotorisee = "";
if ($userdata['niveau'] >= NIVEAU_ROUE_MOTORISEE_12) {
    
    ob_start();
    
    $indiceEngrenage = possedeAccessoire(ACC_ENGRENAGE);
    if ($indiceEngrenage != -1) {
        // possede t-il une roue simple
        $indiceRoue = yatilAccessoireDansLaCage($cageRow['cage_id'], ACC_ROUE3, $userdata['joueur_id']);
        if ($indiceRoue != -1) {
            // proposer un montage roue + engrenage
            echo "<div class=\"actionCage\"><a href=\"jeu.php?mode=m_cage&amp;cage_id=".$cageRow['cage_id']."&amp;action=assembler&amp;objet1=".$indiceEngrenage."&amp;objet2=".$indiceRoue."\">".T_("Assembler la")." <img src=\"images/roue3.gif\" class=\"assemblage\" /> ".T_("avec")." <img src=\"images/engrenage.gif\"  class=\"assemblage\" /> ?</a></div>";
        }
    }
    $indicePile = possedeAccessoire(ACC_PILE);
    if ($indicePile != -1) {
        // possede t-il une roue simple
        $indiceRoue = yatilAccessoireDansLaCage($cageRow['cage_id'], ACC_ROUE_AVEC_ENGRENAGE, $userdata['joueur_id']);
        if ($indiceRoue != -1) {
            // proposer un montage
            echo "<div class=\"actionCage\"><a href=\"jeu.php?mode=m_cage&amp;cage_id=".$cageRow['cage_id']."&amp;action=assembler&amp;objet1=".$indicePile."&amp;objet2=".$indiceRoue."\">".T_("Assembler la")." <img src=\"images/roue3_engrenage.gif\" class=\"assemblage\" /> ".T_("avec la")." <img src=\"images/pile.gif\"  class=\"assemblage\" /> ?</a></div>";
        }
    }    
    // petit bug à l'affichage liée à la différence des indices : le fait de supprimer un objet (ici pile ou engrenage) ne décale pas les index des autres accessoires alors que cela devrait être le cas. Correction à appliquer : transmettre l'id et non l'index.
    $indiceFil = possedeAccessoire(ACC_FIL);
    if ($indiceFil != -1) {
        // possede t-il une roue simple
        $indiceRoue = yatilAccessoireDansLaCage($cageRow['cage_id'], ACC_ROUE_AVEC_ENGRENAGE_PILE, $userdata['joueur_id']);
        if ($indiceRoue != -1) {
            // proposer un montage 
            echo "<div class=\"actionCage\"><a href=\"jeu.php?mode=m_cage&amp;cage_id=".$cageRow['cage_id']."&amp;action=assembler&amp;objet1=".$indiceFil."&amp;objet2=".$indiceRoue."\">".T_("Assembler la")." <img src=\"images/roue3_engrenage_pile.gif\" class=\"assemblage\" /> ".T_("avec du")." <img src=\"images/fil.gif\" class=\"assemblage\" title=\"Fil électrique\" /> ?</a></div>";
        }
    }
    $actionRoueMotorisee = ob_get_contents();
    ob_end_clean(); 
}

ob_start();
?>            
    <div class="actionCage"><div class="icone"><img src="images/icone_outils.gif" alt="" style="vertical-align:middle; height:20px;" /></div>
    <a href="jeu.php?mode=m_cage&amp;cage_id=<?php echo $cageRow['cage_id']; ?>&amp;action=corrigerPlacements" title="<?php echo T_("Corriger les placements de tous les objets");?>"><?php echo T_("Replacer correctement tous les objets");?></a></div>
    </div>
<?php
$blocActionTxt.= ob_get_contents();
ob_end_clean();

$smarty->assign('actionRoueMotorisee',$actionRoueMotorisee) ;
$smarty->assign('blocActionTxt',$blocActionTxt) ;

$smarty->assign('coutVenteCage',$coutVente) ;
$smarty->assign('ingredient1',insererLienIngredient(1)) ;
$smarty->assign('oeuf8',afficherObjet("oeufs",8)) ;

$smarty->display('cage.tpl');  

?>