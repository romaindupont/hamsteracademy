<?php 
define('IN_HT', true);
include "common.php"; 
include "gestion.php";

$errMsg = "" ;
$msg = "" ;

if (isset($_POST['page'])) {

	// le joueur effectue une tentative
	if ($_POST['page'] == "motdepasse") {
		
		$pseudo = mysql_real_escape_string($_POST['htPseudo']) ;
		$mail = mysql_real_escape_string($_POST['adressemail']) ;
		$trouve = false;
		
		if ($pseudo != "") { // le joueur indique son pseudo, on effectue une recherche
			$query = "SELECT email, passwd FROM joueurs WHERE pseudo='".$pseudo."' AND email_active = 1 LIMIT 1";
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
			}

			$nbResultat = $dbHT->sql_numrows($result) ;
			if ($nbResultat == 1) { // pseudo valide, on envoie un mail au joueur
				$joueurData=$dbHT->sql_fetchrow($result) ;
				$mail = $joueurData['email'] ;
				$passwd = $joueurData['passwd'] ;
				$trouve = true;
			}
			else {
				$trouve = false;
				$errMsg = "<font color=red><strong>".T_("Le pseudo")." \"".$pseudo."\" ".T_("est soit introuvable (vérifie son orthographe), soit son email n'a pas été validé lors de l'inscription").".</strong></font><br/>&nbsp;<br/>\n ";
			}
			$dbHT->sql_freeresult($result);
		}
		else if ($mail != "") { // le joueur indique son mail, on effectue une recherche
			$query = "SELECT pseudo, passwd FROM joueurs WHERE email='".$mail."' AND email_active = 1 LIMIT 1";
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
			}

			$nbResultat = $dbHT->sql_numrows($result) ;
			if ($nbResultat == 1) { // mail valide, on envoie un mail au joueur
				$joueurData=$dbHT->sql_fetchrow($result) ;
				$pseudo = $joueurData['pseudo'] ;
				$passwd = $joueurData['passwd'] ;
				$trouve = true;
			}
			else {
				$trouve = false;
                if ($nbResultat == 0)
				    $errMsg = "<font color=red><strong>".T_("L'adresse mail")." \"".$mail."\" ".T_("est introuvable ou non validée lors de l'inscription. Vérifie son orthographe").".</strong></font><br/>&nbsp;<br/>\n\n ";
                else
                    $errMsg = "<font color=red><strong>".T_("L'adresse mail")." \"".$mail."\" ".T_("est la même pour plusieurs joueurs. On a besoin de ton pseudo pour retrouver ton mot de passe").".</strong></font><br/>&nbsp;<br/>\n\n ";
				$errMsg = $errMsg.T_("Essaye de saisir ton pseudo").".<br/>&nbsp;<br/>\n";
			}
			$dbHT->sql_freeresult($result);
		}
		else { // ni mail, ni pseudo
			$errMsg = T_("Saisis ton pseudo ou ton mail utilisé pour l'inscription.") ;
		}
		
		// on envoie un mail si pas d'erreur : 
		if ($errMsg == "") {
			$TO = $mail;
			$subject = T_("Mot de passe perdu") ;
			$message = T_("Bonjour").",\n\n".T_("Voici le mot de passe associé à ton pseudo")." ".$pseudo." ".T_("et l'adresse mail")." ".$mail." :\n\n";
			$message .= "=> ".$passwd." <= \n\n";
			$message .= T_("Reconnecte-toi vite au jeu")." : ".$base_site_url." ! \n\n";
			$message .= T_("L'équipe Hamster Academy")."\n";
			$message .= $base_site_url."\n";
			//$mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);
            $mail_sent = envoyerMail($TO,$subject,$message,"motdepasseperdu");
            //$mail_sent = 0;
			if ($mail_sent == 0) {
				$errMsg = "<font color=red><strong>".T_("Erreur dans l'envoi du mot de passe. Re-essaye").".</strong></font><br/>\n";
				$errMsg = $errMsg.T_("Si l'erreur persiste, contacte Hamster Academy via ").$email_contact;
			}
			else {
				$msg = "<strong>".T_("C'est bon ! Ton mot de passe a été envoyé à ton adresse mail d'inscription")." !</strong><br/>&nbsp;<br/>\n";
				$msg .= T_("Reviens vite jouer avec nous !")."<br/>&nbsp;<br/>\n";
				$msg .= T_("En cas de problème, contacte Hamster Academy par mail.") ;
			}
		}		
	}
}

$liste_styles = "style.css,styleEleveur.css";

// paramètres d'entete
$smarty->assign('pagetitle', "Hamster Academy - ".T_("Mot de passe perdu / oublié"));
$smarty->assign('description', "Hamster Academy, ".T_("Tu as oublié ton mot de passe ? Retrouve-le ici !"));
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>
	
<div align="center"> 
  <h1>Hamster Academy</h1>
  <p>&nbsp;</p>
  <p><img src="images/hamster_russe.jpg" width="250" ></p>
  <p>&nbsp;</p>
  <?php 
  if ($msg != "") {
  	echo $msg ;
	echo "<br/>&nbsp;<br/><a href=\"".$base_site_url."index.php\">".T_("Revenir au jeu maintenant")."</a>." ;
  }
  else {
  ?>
	  <p><?php echo T_("Tu as oublié ton mot de passe ?");?> </p>
	  <p><?php echo T_("Indique ton pseudo ou ton email utilisé pour l'inscription");?> 
		... <br>
	  </p>
	  
	  <?php 
	  if($errMsg != "") {
		echo $errMsg;
	  }
	  ?>
	  
		<form action="motdepasseperdu.php" method="post" >
		<input type="hidden" name="page" value="motdepasse">
		<br/>
		<table border="2"  bordercolor="#FFFFFF"cellspacing="0" cellpadding="5">
		  <tr>
			<td><table border="0" cellpadding="5" align="center">
				<tr> 
				  <td><?php echo T_("Pseudo :");?></td>
				  <td><input type="text" name="htPseudo" size=50 ></td>
				</tr>
                <tr> 
                  <td><?php echo T_("Ou");?></td>
                  <td>&nbsp;</td>
                </tr>
                <tr> 
                  <td>Email :</td>
                  <td><input type="text" name="adressemail" size=50 ></td>
                </tr>
			  </table>
			  <div align="right"><br>
				<input type="submit" name="Submit" value="<?php echo T_("Récupérer mon mot de passe");?>">
			  </div>
	  </td>
		  </tr>
		</table>
		<div align="center"></div>
	  </form>
  <?php 
  } // fin du else pour if ($msg!="")
  ?>
  <p>&nbsp;</p>
  <p><?php echo T_("Ca ne marche pas ? Ecris à");?> <a href="mailto:<?php echo $email_contact;?>"><?php echo $email_contact; ?></a>.<br/>&nbsp;<br/><?php echo T_("Evidemment, si tu as tout tout oublié (pseudo et email), on ne pourra pas retrouver ton mot de passe");?>...</p>
</div>
<br/>&nbsp;<br/>

<?php
require "footer.php" ; 
?>
