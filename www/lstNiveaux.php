<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

define('NIVEAU_OBJECTIFS',0);
define('NIVEAU_DESCRIPTION_SI_REUSSI',1);

define('OBJECTIF_NOM',0);
define('OBJECTIF_DESCRIPTION',1);

global $maskObjectifs, $infosNiveaux, $nbNiveaux ;

// colonnes : 0) nom du niveau <-> 1) nbre d'objectifs <-> 2) tableaux d'objectifs
$infosNiveaux = array(

0 => array(
    array( 0 => array('','') ),
    ''),
    
1 => array(
    array(
        0 => array(T_('Nourrir le hamster'),T_("Il faut nourrir le hamster : achète une écuelle et des aliments.")), 
        1 => array(T_('Changer les copeaux'),T_("Pour améliorer l'hygiène de la cage, tu dois changer les copeaux."))
    ),
    T_('Bravo !! Tu as réussi toutes les missions ! Tu as vu comment on nourrit son hamster et comment rendre une cage toute propre. C\'est très important pour le hamster, il faut le faire régulièrement').'.<br/>'.T_('Maintenant, tu peux acheter de nouveaux objets pour ton hamster et ta cage').'. <br/>&nbsp;<br/>'.T_('Regarde aussi les').' <a href="jeu.php?mode=m_accueil">'.T_('nouvelles missions').'</a> '.T_('pour atteindre le niveau suivant. Tu vas pouvoir aménager un peu mieux la cage du hamster !')),
    
2 => array(
    array(
        0 => array(T_('Ajouter un étage'),T_("Agrandit la cage de l'hamster en ajoutant un étage.")),
        1 => array(T_('Déplacer un objet en haut'),T_("Déplace n'importe quel objet dans l'étage du haut.")),
        2 => array(T_('Mettre une roue'),T_("Pour que l'hamster puisse faire du sport, tu dois acheter une roue et la mettre dans sa cage."))
    ),
    T_('Bravo !! Tu as réussi toutes les missions du niveau ! La cage de ton hamster prend forme ! Pour acheter des nouveaux objets, il va falloir gagner plus de pièces. Une façon d\'y arriver, c\'est d\'économiser pour avoir un nouveau métier qui rapporte plus !').'<br/>&nbsp;<br/>'.T_('Regarde les').' <a href="jeu.php?mode=m_accueil">'.T_('nouvelles missions').'</a> '.T_('pour atteindre le niveau suivant. Bonne chance !')),
    
3 => array(
    array(
        0 => array(T_('Deviner la maladie du hamster'),T_("Peut-être trouveras-tu en boutique de quoi en savoir plus sur sa maladie...")), 
        1 => array(T_('Guérir ton hamster'),T_("Il n'existe pas toujours de médicament mais parfois des petites recettes de Grand-mère améliorent les choses !"))
    ),
    T_('Bien joué, ton hamster va mieux ! Il est d\'attaque pour la prochaine mission !')),

4 => array(
    array(
        0 => array(T_('Changer de métier'),T_("Il faut changer de métier afin de gagner plus d'argent chaque semaine !")), 
        1 => array(T_('Fabriquer un nid'),T_("Avec du coton, de la paille et des brindilles, demande à ton hamster de fabriquer son nid.<br/>Il faut d'abord acheter les trois matériaux à la boutique : coton, paille et brindilles.<br/>Si ton hamster n'est pas très bâtisseur, tu peux lui acheter une formation de <i>Fabrication de Nid</i> via la boutique."))
    ),
    T_('Bravo !! Tu as réussi toutes les missions ! Tu sais comment fabriquer un nid. Ce nid est indispensable pour avoir des bébés').'.<br/>&nbsp;<br/>'.T_('Regarde maintenant les').' <a href="jeu.php?mode=m_accueil">'.T_('nouvelles missions').'</a> '.T_('pour atteindre le niveau suivant. Ca se passe maintenant à l\'Academy ! Bonne chance !')),
    
5 => array(
    array(
    0 => array(T_('Inscrire un hamster à l\'Academy'),T_('Il faut entrer dans l\'Academy, payer les frais d\'inscription pour son hamster.')), 
    1 => array(T_('Prendre un cours de danse'),T_('Le cours de danse n\'est accessible qu\'aux Académiciens confirmés. Pour cela, il faut passer l\'examen avec succès.'))
    ),
    T_('Bravo !! Tu as réussi toutes les missions du niveau 4 ! Pas trop dur l\'entrée à l\'Academy ? <br/>&nbsp;<br/>Des nouvelles missions t\'y attendent ! Elles sont <a href="jeu.php?mode=m_accueil">ici</a>.')),
    
6 => array(
    array(
        0 => array(T_('Se confier devant le public'),''),
        1 => array(T_('Dépasser 10 points de popularité'),'')
    ),
    T_('Bravo !! Tu as réussi toutes les missions ! Tu commences à connaître un peu mieux l\'Academy ! Nouveauté : la force, le moral, la beauté, etc. de tes hamsters peuvent monter à 15 points.<br/>&nbsp;<br/>Regarde maintenant les <a href="jeu.php?mode=m_accueil">nouvelles missions</a> pour atteindre le niveau suivant. Bonne chance !')),
    
7 => array(
    array(
        0 => array(T_('Fabriquer une roue motorisée qui marche'),T_('A toi de déviner comment fabriquer une roue motorisée...')),
        1 => array(T_('Atteindre 12 sur 15 partout'),T_('Tous tes hamsters doivent avoir au moins 12 sur 15 en santé, moral, beauté et force.'))
    ),
    T_('Bravo !! Tu as réussi la mission ! Tes hamsters vont bien et grâce à la fabrication de la roue motorisée, tu vas pouvoir muscler ton hamster de façon beaucoup plus efficace. C\'est aussi un moyen très efficace de le faire maigrir')
    .'<br/>&nbsp;<br/>'.T_('Voici les').' <a href="jeu.php?mode=m_accueil">'.T_('nouvelles missions').'</a> '.T_('pour atteindre le niveau suivant.')),

8 => array(
    array(
        0 => array(T_('Guérir ton hamster'),T_('Ton hamster est tombé malade, guéris-le vite !'))
    ),
    T_('Mission réussie ! Ton hamster va aller beaucoup mieux ! Prochaine mission : préparer le potage qui va requinquer tout le monde !')),
    
9 => array(
    array(
        0 => array(T_('Trouver 1 oignon'),T_('Regarde partout dans le jeu pour trouver cet ingrédient !')),
        1 => array(T_('Trouver 1 pomme de terre'),T_('Regarde partout dans le jeu pour trouver cet ingrédient !')),
        2 => array(T_('Trouver 1 navet'),T_('Un indice pour trouver le navet : il se trouve à l\'Academy, au niveau des groupes... cherche bien  !')),
        3 => array(T_('Trouver 1 poireau'),T_('Regarde partout dans le jeu pour trouver cet ingrédient !')),
        4 => array(T_('Trouver 1 chataigne'),T_('Un indice pour trouver la chataigne : regarde dans les métiers...')),
        5 => array(T_('Trouver 1 noisette'),T_('Regarde partout dans le jeu pour trouver cet ingrédient !')),
        6 => array(T_('Trouver 1 carotte'),T_('Regarde partout dans le jeu pour trouver cet ingrédient !')),
        7 => array(T_('Faire le Potage du Rongeur'),T_('Trouve dans le jeu tous les ingrédients nécessaires à la préparation du potage (1 pomme de terre, 1 navet, 1 carotte, 1 poireau, 1 chataigne, 1 noisette et 1 oignon) et prépare-le.')),
        8 => array(T_('Donner le potage au hamster'),T_('Une fois le potage préparé, donne-le à ton hamster de ton choix si tu en as plusieurs.'))
    ),
    T_('<strong>Bravo !!</strong> Tu as réussi toutes les missions ! Nouveauté : la force, le moral, la beauté, etc. de tes hamsters peuvent monter à 20 points.')),

10 => array(
    array(
        0 => array(T_('Atteindre au moins 18 sur 20 partout'),T_('Tous tes hamsters doivent atteindre au moins 18 sur 20 partout (santé,moral,force et beauté).')),
        1 => array(T_('Gagner 5 défis aujourd\'hui !'),T_('Tu dois gagner 5 défis aujourd\'hui (en Ville->Concours). Le compteur de défis est remis à zéro à minuit : si tu n\'y arrives pas aujourd\'hui, réessaye demain.'))
    ),
    '<strong>'.T_('Mission réussie ! Tes hamsters sont en pleine forme, ils veulent se marier maintenant !').'</strong>'),

    
11 => array(
    array(
        0 => array(T_('Faire une demande en mariage pour l\'un de tes hamsters'),T_('Il faut que tu fasses une demande en mariage pour l\'un de tes hamsters avec un hamster d\'un autre joueur si possible.')),
        1 => array(T_('Accepter une demande en mariage'),T_('Il faut que tu maries un de tes hamsters avec le hamster d\'un autre joueur.'))
    ),
    T_('<strong>Bravo !!</strong> Tes hamsters sont mariés ! Ils vont pouvoir avoir des bébés ! Ce sera l\'objectif de la mission suivante, mais avant cela, il faudra demander aux hamsters de construire une cabane pour les accueillir... Mais où trouver du bois ?')),
    
12 => array(
    array(
        0 => array(T_('Construire une cabane pour accueillir les bébés'),T_('Il faut du bois et un plan. Où peut-on trouver du bois ailleurs qu\'en boutique ... ? ')),
        1 => array(T_('Avoir un bébé'),T_('Il faut un hamster femelle et faire des bébés... ')),
        2 => array(T_('Avoir un avatar'),T_('Si ce n\'est pas encore fait, va dans ton Profil et choisis ton image (avatar).'))
    ),
    T_('<strong>Bravo !!</strong> Tu as réussi la mission ! Mais ton hamster en a profité pour s\'échapper ! Retrouve-le vite ! Tu peux te faire aider d\'un deuxième hamster...')),

13 => array(
    array(
        0 => array(T_('Avoir au moins deux hamsters'),T_('Peut-être que tu pourras te faire aider d\'un deuxième hamster pour retrouver celui qui s\'est échappé...')),
        1 => array(T_('L\'un de tes hamsters s\'est échappé !'),T_('Retrouve-le vite ! A toi de trouver où il a bien pu aller...'))
    ),
    T_('<strong>Bravo !!</strong> Tu as réussi la mission ! <br/>&nbsp;<br/><img src="images/foret.gif" alt="" /> Tu as retrouvé ton hamster ! Il était en forêt !')),

14 => array(
    array(
        0 => array(T_('S\'inscrire à une répétition'),T_('Inscris l\'un de tes hamsters à une répétition.')),
        1 => array(T_('Donner au groupe la partition musicale des Rock\'Roll\'Hamsters et répéter avec !'),T_('A acheter en boutique...')),
        2 => array(T_('Faire un concert'),T_('C\'est à toi de demander à faire un concert...'))
    ),
    T_('<strong>Bravo !!</strong> Grâce à toi, ton groupe de musique a bien progressé ! Pour la prochaine mission, place au sport ! Pour t aider, les niveaux de sante, de moral, energie, etc. ont été montés !')),

15 => array(
    array(
        0 => array(T_('Trouver Hammer le Pirate'),T_('Pas évident à trouver ce fameux Hammer Le Pirate... Il aime bien se cacher... Utilise l\'un de tes hamsters pour le trouver, entre hamsters, ils connaissent les mêmes cachettes non ?... Et un autre indice : un pirate a souvent besoin de matières premières pour construire ses armes, ses bateaux ... où ?')),
        1 => array(T_('Le battre au tennis !'),T_('Achète une raquette et des balles si ce n est déjà fait, et bats-le au tennis !'))
    ),
    T_('<strong>Bravo !!</strong>Ton hamster est devenu un champion au tennis ! Bravo ! Pour la prochaine mission, un vent d\'orient pour les cages !')),

16 => array(
    array(
        0 => array(T_('Décorer un étage sur le thème chinois'),T_('Il faut au moins 10 objets chinois sur le même étage !')),
        1 => array(T_('Une fois la déco faite, cliquer sur <i>Replacer correctement tous les objets</i>'),T_('Cliquer sur <i>Replacer correctement tous les objets</i> (accessible dans le sous-menu <i>Atelier</i> dans <i>Mes Cages</i>')),
        2 => array(T_('Gagner 15 défis aujourd\'hui !'),T_('Tu dois gagner 15 défis aujourd\'hui (en Ville->Concours). Le compteur de défis est remis à zéro à minuit : si tu n\'y arrives pas aujourd\hui, réessaye demain.'))
    ),
    '<strong>Bravo !!</strong>'.T_('Mission terminée ! Malheureusement, le repis est de courte durée, un grand courant d\'air a balayé toute l\'Academy... Brrrrrrrrrrr....')),

17 => array(
    array(
        0 => array(T_('Guérir ton hamster'),T_('Ton hamster est tombé malade, guéris-le vite !'))
    ),
    '<strong>Bravo !!</strong>'.T_('Mission terminée ! Toutes les missions du jeu sont terminées. Il ne reste plus qu\'à devenir le meilleur de l\'Academy&nbsp;! Bonne chance&nbsp;!').'<br/> &nbsp; <br/><strong>'.T_('Bon à savoir').'</strong> : '.T_('nous préparons régulièrement de nouvelles missions. Reviens régulièrement, les nouveautés sont nombreuses').'.<br/>&nbsp;<br/>'.T_('Tu peux aussi visiter les').' <strong><a href="autresjeux.php">'.T_('jeux de nos partenaires').'</a></strong>, '.T_('bonne continuation sur Hamster Academy !')),
    
18 => array(
    array(
        0 => array(T_('Fin des missions. Il te reste à devenir le meilleur de l\'Academy&nbsp;!'),T_('Devenir le meilleur joueur de l\'Hamster Academy'))
    ),
    '')  
);

$nbNiveaux = sizeof($infosNiveaux);

$maskObjectifs = array(0,1,3,7,15,31,63,127,255,511,1023) ;

?>