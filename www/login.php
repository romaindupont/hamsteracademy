<?php 

if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$err_type = 0;
$joueur_id = -1;

if (isset($_POST['htPseudo'])) {
	
	$pseudo = mysql_real_escape_string($_POST['htPseudo']);

	if (isset($_POST['htPasswd'])) {
	
		$passwd = mysql_real_escape_string($_POST['htPasswd']);
		
		$sql = "SELECT joueur_id, pseudo, passwd, user_lastvisit
			FROM joueurs
			WHERE pseudo = '" . str_replace("\\'", "''", $pseudo) . "'";
			
		if ( !($result = $dbHT->sql_query($sql)) )
		{
			$err_type = 3;
			message_die(GENERAL_ERROR, 'Error in obtaining userdata', '', __LINE__, __FILE__, $sql);
		}

		if($row = $dbHT->sql_fetchrow($result) )
		{
			if($passwd == $row['passwd'])
			{
				$derniereConnexion = $row['user_lastvisit'];
                $joueur_id = $row['joueur_id'];
                $session_id = session_begin($joueur_id, $user_ip);

				if( $session_id ){
					
					// arrivé ici = joueur correctement loggé, on récupère le userdata
                    $query = "SELECT u.*, s.*
                        FROM sessions s, joueurs u
                        WHERE s.session_id = '$session_id'
                            AND u.joueur_id = s.session_user_id";
                    if ( !($resultUserdata = $dbHT->sql_query($query)) )
                    {
                        message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $query);
                    }
                    
                    $userdata = $dbHT->sql_fetchrow($resultUserdata);
                    $dbHT->sql_freeresult($resultUserdata);
					
					// connection au forum punBB
					define('PUN_ROOT', './forum/');
					require PUN_ROOT.'include/common.php';

					$form_password_hash = pun_hash($row['passwd']);
					$expire = time() + 31536000 ;
					pun_setcookie($joueur_id, $form_password_hash, $expire);

                    // on sauve un cookie pour remémorrer le pseudo du joueur en page de login
                    setcookie ("HamsterAcademyPseudo", $pseudo, time()+2592000, '/' );                    
                    
                    // pour la suite du programme
                    $dbHT->sql_activate();
					
					if ($pseudo == "admin")
						redirectHT("tableaudebord.php");
				}
				else
				{
					$err_type = 3;
					message_die(CRITICAL_ERROR, "Couldn't start session : login", "", __LINE__, __FILE__);
				}
			}
			else
				$err_type = 1;
		}
		else
			$err_type = 2;
        
        $dbHT->sql_freeresult($result);
	}
}

// s'il y a eu une erreur
if ($err_type > 0)
    redirectHT("index.php?err=".intval($err_type));

?>
