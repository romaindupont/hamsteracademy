<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";
include "lstAccessoires.php";

$depuis24H = $dateActuelle-3600*24;

// on sélectionne tous les joueurs qui n'ont pas eu de mise à jour de scores depuis 1 jour
$query = "SELECT joueur_id, date_maj_note FROM joueurs WHERE date_maj_note < ".$depuis24H." LIMIT 10000";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;

while($row=$dbHT->sql_fetchrow($result)) {
    miseAJourPointsJoueur($row['joueur_id']);
}

$dbHT->sql_freeresult($result);

echo "nb de joueurs avec le score mis à jour : " . $nbJoueurs. "<br/>";

// on met à jour qq stats pour la page d'accueil
// pour le top 5 des éleveurs
$lstTop5Joueurs = "";
$query = "SELECT pseudo, joueur_id FROM joueurs WHERE joueur_id NOT IN (1,7,2,".HAMSTER_ACADEMY_ID.") ORDER BY note DESC LIMIT 5";
if ($result = $dbHT->sql_query($query) ){
    while($row=$dbHT->sql_fetchrow($result)) {
        $lstTop5Joueurs .= $row['joueur_id'].";".$row['pseudo'].";";
    }
}
$dbHT->sql_freeresult($result);

// pour le top 5 des groupes de musiques
$lstTop5groupesMusiques = "";
$query = "SELECT groupe_id,image, nom FROM groupes WHERE type = 1 ORDER BY note DESC LIMIT 5";
if ($result = $dbHT->sql_query($query) ){
    while($row=$dbHT->sql_fetchrow($result)) {
        $lstTop5groupesMusiques .= $row['groupe_id'].";".$row['nom'].";";
    }
}
$dbHT->sql_freeresult($result);

// pour le top 5 des équipes de foot
$lstTop5equipesFoot = "";
$query = "SELECT groupe_id,image, nom FROM groupes WHERE type = 2 ORDER BY note DESC LIMIT 5";
if ($result = $dbHT->sql_query($query) ){
    while($row=$dbHT->sql_fetchrow($result)) {
        $lstTop5equipesFoot .= str_replace("'","\'",$row['groupe_id'].";".$row['nom'].";");
    }
}
$dbHT->sql_freeresult($result);

$query = "UPDATE config SET ValeurTxt= '".$lstTop5Joueurs."' WHERE parametre  = 'lstTop5Joueurs'";
$dbHT->sql_query($query);
echo $query."<br/>";

$query = "UPDATE config SET ValeurTxt= '".$lstTop5groupesMusiques."' WHERE parametre  = 'lstTop5groupesMusiques'";
$dbHT->sql_query($query);
echo $query."<br/>";

$query = "UPDATE config SET ValeurTxt= '".$lstTop5equipesFoot."' WHERE parametre  = 'lstTop5equipesFoot'";
$dbHT->sql_query($query);
echo $query."<br/>";

// nb de joueurs connectés
// -----------------------
$rs = $dbHT->sql_query("select count(*) from joueurs where user_lastvisit > ".($dateActuelle - 1200)); 
$r = $dbHT->sql_fetchrow($rs);
$dbHT->sql_freeresult($rs);

$query = "UPDATE config SET ValeurTxt= '".$r[0]."' WHERE parametre  = 'nbJoueursConnectes'";
$dbHT->sql_query($query);
echo $query."<br/>";

// profil aléatoire (france)
// -------------------------
$vip_query_addon = "";
//    $vip_priorite = rand(1,3) ;
//    if ($vip_priorite == 1)
//        $vip_query_addon = " AND vip = 1";

$queryProfilAleatoir = "SELECT joueur_id, pseudo FROM joueurs 
    WHERE user_lastvisit > ".($dateActuelle-259200)." 
    AND image > 0 
    AND montrer_profil > 0
    AND liste_rouge = 0
    AND niveau > 6 
    AND langue = 'fr' "
    .$vip_query_addon ; // un joueur au hasard mais connecté depuis moins de 2 jours (et vip) et niveau > 8

if ( !($resultProfilAleatoir = $dbHT->sql_query($queryProfilAleatoir)) ){
    return "";
}

$lstProfilsAleatoires = "";
while($rowJoueurChoisi=$dbHT->sql_fetchrow($resultProfilAleatoir)) {

    $lstProfilsAleatoires .= $rowJoueurChoisi['joueur_id'].";".$rowJoueurChoisi['pseudo'].";" ;
}

$query = "UPDATE config SET ValeurTxt= '".$lstProfilsAleatoires."' WHERE parametre  = 'profilsAleatoires_fr'";
$dbHT->sql_query($query);
echo $query."<br/>";
$dbHT->sql_freeresult($resultProfilAleatoir);

// profil aléatoire (france)
// -------------------------
$vip_query_addon = "";
//    $vip_priorite = rand(1,3) ;
//    if ($vip_priorite == 1)
//        $vip_query_addon = " AND vip = 1";

$queryProfilAleatoir = "SELECT joueur_id, pseudo FROM joueurs 
    WHERE user_lastvisit > ".($dateActuelle-259200)." 
    AND image > 0 
    AND montrer_profil > 0
    AND liste_rouge = 0
    AND niveau > 6 
    AND langue = 'en' "
    .$vip_query_addon ; // un joueur au hasard mais connecté depuis moins de 2 jours (et vip) et niveau > 8

if ( !($resultProfilAleatoir = $dbHT->sql_query($queryProfilAleatoir)) ){
    return "";
}

$lstProfilsAleatoires = "";
while($rowJoueurChoisi=$dbHT->sql_fetchrow($resultProfilAleatoir)) {

    $lstProfilsAleatoires .= $rowJoueurChoisi['joueur_id'].";".$rowJoueurChoisi['pseudo'].";" ;
}

$query = "UPDATE config SET ValeurTxt= '".$lstProfilsAleatoires."' WHERE parametre  = 'profilsAleatoires_en'";
$dbHT->sql_query($query);
echo $query."<br/>";
$dbHT->sql_freeresult($resultProfilAleatoir);

?>
