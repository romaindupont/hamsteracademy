<?php 

define('IN_HT', true);
define('NO_NEED_DB', true);

if (0 && $server_langue == "en") { // maintenance
    $server_langue = "fr";
    if (substr($_SERVER['SERVER_NAME'],-3) == "com")
        $server_langue = "en";
        
    if ($server_langue == "fr")
        $Fnm = "http://www.hamsteracademy.fr/site_maintenance_fr.html";
    else
        $Fnm = "http://www.hamsteracademy.com/site_maintenance_en.htm";
        
    $tableau = file($Fnm);

    while(list($cle,$val) = each($tableau)) {
       echo $val."\n";
    }
    return ;
}

include "common.php";
include "facebook.php";

//si le client a accepté l'application
$connexion_facebook_txt = T_("ou via Facebook")." <fb:login-button perms=\"publish_stream,email\"></fb:login-button>";
if ($sessionFacebook) {
    $connexion_facebook_txt = "<a href=\"jeu.php?loginFacebook=1\"> <img src=\"images/FaceBook_48x48.png\" alt=\"\" style=\"height:20px; vertical-align:middle;\"/> ".T_("Se connecter avec Facebook")."</a>" ;
}
$smarty->assign('connexion_facebook', $connexion_facebook_txt);

// erreur ?
$erreur = 0;
if (isset($_GET['err']))
    $erreur = intval($_GET['err']);
    
if (isset($_GET['compteSupprime']))
    $erreur = 3 ;

$pagetitle = T_("Hamster Academy - Jeu d'élevage virtuel gratuit de hamsters");
$description = T_("Hamster Academy, Jeu virtuel d'élevage de hamsters gratuit ! Adopte un hamster, personnalise sa cage, trouve-lui des amis, apprends-lui à jouer de la musique et à danser puis fonde ton groupe !");
$keywords = T_("Hamster, Academy, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer");
if (isset($_GET['toutesnouveautes'])) {
    $pagetitle .= T_(" - Toutes les nouveautés");
    $description = T_("Toutes les nouveautés de Hamster Academy, Jeu virtuel d'élevage de hamsters gratuit ! Concours, nouveaux dessins, missions, objets, toutes les nouveautés sont présentées ici !");
}

$univers = UNIVERS_INDEX;

// pseudo déjà saisi ?
if (isset($_COOKIE['HamsterAcademyPseudo'])) {
    $prevPseudo = htmlspecialchars($_COOKIE['HamsterAcademyPseudo']);
    $smarty->assign('prevPseudo', $prevPseudo);
}

$liste_styles = "style.css,styleIndex.css,cornflex-common.css";    
$liste_scripts = "tooltip.js,gestionHamster.js";

// pour ie 5 et 6, proposition d'installer Chrome
$chrome_instal = "
<!-- Redirection pour IE 5.0, IE 5.5 et IE 6.0 -->
<!--[if lte IE 6]>
    <script type=\"text/javascript\" src=\"http://ajax.googleapis.com/ajax/libs/chrome-frame/1/CFInstall.min.js\"></script>
    <script type=\"text/javascript\">
        CFInstall.check({
            onmissing: function(){
                window.location=\"/mise-a-jour-navigateur";
if ($lang != "fr")
    $chrome_instal .= "-us";
$chrome_instal .= ".html\"
            }
        });
    </script>
<![endif]-->";

// barre top
$topbar_texte = "";
$topbar_texte = "<a href=\"http://".$serveur_url_fr."index.php\" title=\"Jeu en francais\"><img src=\"images/lang/francais.gif\" alt=\"Francais\" width=\"20\" height=\"14\" border=\"1\" style=\"vertical-align:text-top;\" /> Francais</a> &nbsp; <a href=\"http://".$serveur_url_en."index.php\" title=\"Play the game in English !\"><img src=\"images/lang/english.gif\" alt=\"English\" width=\"20\" height=\"14\" border=\"1\" style=\"vertical-align:text-top;\" /> English</a>";

// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('keywords', $keywords);
$smarty->assign('univers', UNIVERS_INDEX);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('topbar_texte', $topbar_texte);
$smarty->assign('lang', $lang);
$smarty->assign('chrome_instal', $chrome_instal);
$smarty->display('header.tpl');

// changement de langue
if (! $langue_choisie && $navigateur_lang != $lang && strpos($_SERVER['HTTP_USER_AGENT'],"Googlebot") === FALSE ) {
    $msgLangue = "";
    if ( $lang== "fr" ) 
        $msgLangue = afficherNouvelle("lang/english.gif","Hamster Academy in english? Just go to <a href=\"http://www.hamsteracademy.com\">http://www.hamsteracademy.com</a> ! If you are already registered, use the same login/password! Enjoy!<br/><span style=\"color:grey; font-size:8pt;\">(<a href=\"index.php?langue_choisie=fr\">hide</a>)(<a href=\"index.php?langue_choisie=en\">ne plus demander</a>)</span>");
    else
        $msgLangue = afficherNouvelle("lang/francais.gif","Astuce ! Joue à Hamster Academy en français en utilisant l'adresse <a href=\"http://www.hamsteracademy.fr\">http://www.hamsteracademy.fr</a> ! Si tu es déjà inscrit, tu pourras utiliser le même pseudo et mot de passe.<br/><span style=\"color:grey; font-size:8pt;\">(<a href=\"index.php?langue_choisie=en\">ne plus demander</a>)(<a href=\"index.php?langue_choisie=en\">hide</a>)</span>","",-1,15);
    //$smarty->assign('msgLangue', $msgLangue);
}

// date de fin de suspension si le joueur est suspendu
$finSuspension = 1;
if (isset($_GET['finSuspension']))
    $finSuspension = intval($_GET['finSuspension']);

// affichage des slogans aléatoires
//$slogan_index = rand(0,10+$nbSlogans-1);
//if ($slogan_index<10)
//    $smarty->assign('slogan_txt', $slogans[0]);
//else
//    $smarty->assign('slogan_txt', $slogans[$slogan_index-10]);

// affichage des nouveautés
if (isset($_GET['toutesnouveautes'])) {
    define ('TOUTES_LES_NOUVEAUTES',1);
    define ('PLUS_DE_NOUVEAUTES',1);
}

$topbar_texte = "";
//Bienvenue sur Hamster Academy <span style=\"margin-left:30px;\"><a href=\"idee.html\" title=\"Clique sur la bo&icirc;te pour y mettre une remarque ou une id&eacute;e !\">Boîte à idées</a> | <a href=\"aide/start\" title=\"Les r&egrave;gles du jeu\">Règles du jeu</a>| <a href=\"forum/index.php\" title=\"Forum du jeu\">Forum</a>| <a href=\"autresjeux.php\" title=\"Jeux virtuels gratuits recommandés par Hamster Academy\">Jeux virtuels gratuits</a> | <a href=\"demo.html\" title=\"Quelques extraits du jeu\">Demo</a> | <a href=\"bonus.html\" title=\"Bonus, photos, vidéos sympas\">Bonus</a> | </span>";

include "nouveautes_".$lang.".php";
$smarty->assign('lang', $lang);
$smarty->assign('nouveautes', $dernieresNouveautes);
$smarty->assign('erreur', $erreur);
$smarty->assign('finSuspension', $finSuspension);
$smarty->assign('topbar_texte', $topbar_texte);

initDBHT();

if ($dbHT->db_connect_id) {

    // affichage du nbre de connectés
    $rs = $dbHT->sql_query("SELECT ValeurTxt FROM config WHERE parametre = 'nbJoueursConnectes'"); 
    $r = $dbHT->sql_fetchrow($rs);
    $nbJoueursConnectes = max(10,intval($r[0]) + rand(0,10)-5);
    $smarty->assign('nbConnectes', $nbJoueursConnectes);
    $dbHT->sql_freeresult($rs);

    // pour le top 5 des éleveurs
    $lstTop5Joueurs = array();
    $query = "SELECT ValeurTxt FROM config WHERE parametre = 'lstTop5Joueurs'";
    if ($result = $dbHT->sql_query($query) ){
        $row=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        $val = explode(";",$row['ValeurTxt']);
        for($i=0;$i<5;$i++)
            array_push($lstTop5Joueurs,"<div>".returnLienProfil($val[2*$i],$val[2*$i+1],true,false)."</div>");
    }

    // pour le top 5 des groupes de musiques
    $lstTop5groupesMusiques = array();
    $query = "SELECT ValeurTxt FROM config WHERE parametre = 'lstTop5groupesMusiques'";
    if ($result = $dbHT->sql_query($query) ){
        $row=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        $val = explode(";",$row['ValeurTxt']);
        for($i=0;$i<5;$i++) 
            array_push($lstTop5groupesMusiques,"<div>".returnLienProfilGroupe($val[2*$i],$val[2*$i+1],true,false)."</div>");
    }

    // pour le top 5 des équipes de foot
    $lstTop5equipesFoot = array();
    $query = "SELECT ValeurTxt FROM config WHERE parametre = 'lstTop5equipesFoot'";
    if ($result = $dbHT->sql_query($query) ){
        $row=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        $val = explode(";",$row['ValeurTxt']);
        for($i=0;$i<5;$i++) 
            array_push($lstTop5equipesFoot,"<div>".returnLienProfilGroupe($val[2*$i],$val[2*$i+1],true,false)."</div>");
    }

    // 1 joueur aléatoire
    $joueurAleatoire = afficherProfilAleatoire();

    $dbHT->sql_close();
}

$smarty->assign('joueurAleatoire', $joueurAleatoire);
$smarty->assign('lstTop5Joueurs', $lstTop5Joueurs);
$smarty->assign('lstTop5groupesMusiques', $lstTop5groupesMusiques);
$smarty->assign('lstTop5equipesFoot', $lstTop5equipesFoot);
$smarty->assign('cle_application_publique', CLE_PUBLIQUE);


$smarty->display('index.tpl');

require "footer.php"
?>
