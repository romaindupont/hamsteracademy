<?php

define('IN_HT', true);



include "common.php";



$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);

if(!$dbForum->db_connect_id)

{

   echo T_("Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email")." : contact@hamsteracademy.fr .<br/>" ;

   exit(1);

}



// informations sur la page

$pagetitle = T_("Jeux virtuels gratuits - Jeux en Flash");

$univers = UNIVERS_ELEVEUR;

$description = T_("Tous les jeux virtuels gratuit autour des hamsters en flash : harry the hamster, elevage virtuel d'animaux, concours, missions, simulation, aventure, etc.");
$keywords = T_("Hamster, Jeux, Academy, Flash, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission");
$liste_styles = "style.css,styleEleveur.css,cornflex-common.css,styleRate.css";



// information sur le jeu sélectionné

if (isset($_GET['jeu']) || isset($_POST['jeu'])) {

    if (isset($_GET['jeu']))

        $jeu = mysql_real_escape_string($_GET['jeu']);

    else

        $jeu = mysql_real_escape_string($_POST['jeu']);

    

    $query = "SELECT * FROM jeux_description WHERE jeu_tag = '".$jeu."' LIMIT 1";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);

    }

    $nbJeux = $dbForum->sql_numrows($result) ;

    

    if ($nbJeux != 1)

        redirectHT("autresjeux.php?jeu_inconnu=1") ;

    

    $rowJeu = $dbForum->sql_fetchrow($result);

    $dbForum->sql_freeresult($result);

    

    $queryVisites = "UPDATE jeux_description SET visites = ".($rowJeu['visites'] + 1)." WHERE jeu_tag = '".$jeu."' LIMIT 1";

    if ( ! $dbForum->sql_query($queryVisites) ){

        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $queryVisites);

    }

    

    $pagetitle .= " - ".$rowJeu['titre_long']." - ".T_("Recommandé par Hamster Academy");

    $description = $rowJeu['titre_long']." : ".$rowJeu['description']." ".T_("Jeu virtuel gratuit recommandé par Hamster Academy !");

    

    // liste des scores

    $lstScores = "";

    $lstScoresSemaine = "";

    $lstScoresMois = "";

    

    $query = "SELECT * FROM jeux_scores WHERE jeu_id = ".$rowJeu['jeu_id']." AND score < 100000 ORDER BY score DESC LIMIT 10";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);

    }

    $classement = 1;

    while ($rowScore = $dbForum->sql_fetchrow($result)) {

        $lstScores .= $classement.". ".$rowScore['pseudo']." : ".$rowScore['score']." pts<br/>";

        $classement++;

    }

    $dbForum->sql_freeresult($result);

    $query = "SELECT * FROM jeux_scores WHERE jeu_id = ".$rowJeu['jeu_id']." AND score < 100000 AND date > ".($dateActuelle-604800)." ORDER BY score DESC LIMIT 10";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);

    }

    $classement = 1;

    while ($rowScore = $dbForum->sql_fetchrow($result)) {

        $lstScoresSemaine .= $classement.". ".$rowScore['pseudo']." : ".$rowScore['score']." pts<br/>";

        $classement++;

    }

    $dbForum->sql_freeresult($result);

    $query = "SELECT * FROM jeux_scores WHERE jeu_id = ".$rowJeu['jeu_id']." AND score < 100000 AND date > ".($dateActuelle-2592000)." ORDER BY score DESC LIMIT 10";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);

    }

    $classement = 1;

    while ($rowScore = $dbForum->sql_fetchrow($result)) {

        $lstScoresMois .= $classement.". ".$rowScore['pseudo']." : ".$rowScore['score']." pts<br/>";

        $classement++;

    }

    $dbForum->sql_freeresult($result);

    

    // liste des commentaires

    $lstCommentaires = "";

    $query = "SELECT * FROM jeux_commentaires WHERE jeu_id = ".$rowJeu['jeu_id']." ORDER BY date DESC LIMIT 10";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);

    }

    while ($rowCommentaire = $dbForum->sql_fetchrow($result)) {

        $lstCommentaires .= "<b>".$rowCommentaire['pseudo']."</b> : ".$rowCommentaire['commentaire']."<br/>";

    }

    $dbForum->sql_freeresult($result);

    

    $smarty->assign('jeu_tag',$rowJeu['jeu_tag']);

    $smarty->assign('jeu_id',$rowJeu['jeu_id']);

    $smarty->assign('jeu_titre',$rowJeu['titre_long']);

    $smarty->assign('jeu_flash',$rowJeu['flash']);

    $smarty->assign('jeu_description',$rowJeu['description']);

    $smarty->assign('jeu_forum_id',$rowJeu['forum_id']);

    $smarty->assign('scores_joueurs_semaine',$lstScoresSemaine);

    $smarty->assign('scores_joueurs_mois',$lstScoresMois);

    $smarty->assign('scores_joueurs',$lstScores);

    $smarty->assign('commentaires_joueurs',$lstCommentaires);

}

     

// paramètres d'entete

$smarty->assign('pagetitle', $pagetitle);

$smarty->assign('description', $description);

$smarty->assign('univers', $univers);

$smarty->assign('liste_styles', $liste_styles);

$liste_scripts = "jquery.js,jquery.rater.js";

$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('keywords', $keywords);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');



// on se connecte à la base de données du forum

$dbForum->sql_activate();



if (isset($_POST['action'])) {

    

    $action = $_POST['action'] ;

    if ($action == "ajouterScore") {

     

        $pseudo =  mysql_real_escape_string($_POST['scorePseudo']);  

        $score =  intval($_POST['scoreScore']);  

        $jeu_id =  intval($_POST['jeu_id']);

        

        $query = "INSERT INTO jeux_scores VALUES (-1,'$pseudo',$score,$dateActuelle,$jeu_id)" ;

        if ( !($result = $dbForum->sql_query($query)) ){

            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);

        }

    }

}



?>



<div align="center"> 

  <h1><img src="images/logoHamsterAcademy.gif" alt="Hamster Academy" /></h1>

  

  <?php 

    if (isset($_GET['jeu']) || isset($_POST['jeu'])) {

        

        $smarty->display('autresjeux_fiche.tpl');

                         

    }    

    // affichage de tous les jeux

    else {

  ?>

  

  <h2><?php echo T_("Jeux Virtuels Gratuits");?></h2>

  <br/>

  

  <div style="float:right;">

    <script type="text/javascript"><!--

    google_ad_client = "pub-4942968069828673";

    /* 160x600, date de création 23/04/09 */

    google_ad_slot = "6654976584";

    google_ad_width = 160;

    google_ad_height = 600;

    //-->

    </script>

    <script type="text/javascript"

    src="http://pagead2.googlesyndication.com/pagead/show_ads.js">

    </script>

</div>

  

  <div style="text-align:left; width:700px;">

   

<?php 



    $query = "SELECT * FROM jeux_description";

    if ( !($result = $dbForum->sql_query($query)) ){

        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);

    }

    $nbJeux = $dbForum->sql_numrows($result) ;



    while ( $rowJeu = $dbForum->sql_fetchrow($result) ) {

?>

      <div class="cornflex yellowbox" style="width:180px;">

        <div class="t">

        <div class="c" style="text-align:center;">

        <strong><?php echo $rowJeu['titre']; ?> </strong><br/>&nbsp;<br/>

        <a href="autresjeux.php?jeu=<?php echo $rowJeu['jeu_tag']."\" title=\"".T_("Jouer à")." ".$rowJeu['titre_long']; ?> !">

        <img src="jeux/<?php echo $rowJeu['image']; ?>" alt="<?php echo $rowJeu['titre_long']; ?>" /></a><br/>&nbsp;<br/>

        <?php echo T_("Joué")." ".$rowJeu['visites'] ." ".T_("fois") ;?>

        <div id="starJeu_<?php echo $rowJeu['jeu_tag']; ?>" class="stat">

            <div class="statVal"  style="text-align:left; width;:90px;">

                <span class="ui-rater">

                    <span class="ui-rater-starsOff" style="width:90px;"><span class="ui-rater-starsOn" style="width:<?php echo round($rowJeu['note']*18,0); ?>px;"></span></span>

                </span>

            </div>

        </div>

        <script language="javascript">

        $(document).ready(function() {

                $('#starJeu_<?php echo $rowJeu['jeu_tag'];?>').rater({postHref:'ratingJeu.php', id:'<?php echo $rowJeu['jeu_id']?>'});

        });

        </script>

    </div>

    <div class="r"></div>

    <div class="b"></div>

    <div class="l"></div>

    </div>

</div>



<?php

    }

    $dbForum->sql_freeresult($result);

    



?> 

    

<div style="clear:both;">&nbsp;</div>

<div align="center">

    <script language='JavaScript' src='http://www.clicjeux.net/banniere.php?id=1045'></script>

    </br>&nbsp;<br/>

    

  <?php 

    } // fin du if (isset jeu)

    

    ?>

    <br/>&nbsp;<br/>

    <a href="jeu.php?mode=m_accueil"><?php echo T_("Retour à l'accueil");?></a>

    <br/>&nbsp;<br/>

 </div>


 <?php

    require "footer.php";

?>