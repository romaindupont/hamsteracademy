<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

if ($userdata['niveau'] < $niveauMinimalPourLeMariage){
    echo T_("Désolé, il faut au moins être de niveau ").$niveauMinimalPourLeMariage.T_(" pour pouvoir te marier.") ;
    return;
}

if ($action != "") {
    if ($action == "accepterMariage") {
        if ($erreurAction == 0)
            $msg .= T_("Félicitations ! Ton mariage s'est bien passé !");
        else if ($erreurAction == 1 || $erreurAction == 2) 
            $msg .= T_("Ton mariage n'a pas eu lieu, peut-être que le hamster qui a fait sa demande a disparu ou que sa demande est annulée...");
        else if ($erreurAction == 3) {
            $msgTmp = T_("Tu n'as pas assez d'argent pour accepter la demande en mariage car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_HAMSTER);
        }
    }
    if ($action == "refuserMariage") {
        if ($erreurAction == 0)
            $msg .= T_("La demande en mariage a bien été annulée et l'autre joueur a été prevenu.");
    }
    else if ($action == "demandeMariage") {
        if ($erreurAction == 0)
            $msg .= T_("Ta demande a été envoyé à l'autre hamster !");
        else if ($erreurAction == 1) 
            $msg .= T_("Il n'y a pas de joueur à ce nom-là...");
        else if ($erreurAction == 2)
            $msg .= T_("Il n'y a pas de hamster à ce nom-là...");
        else if ($erreurAction == 3) 
            $msg .= T_("Tu as déjà fait une demande. Tu ne peux en faire qu'une seule à la fois !");
        else if ($erreurAction == 4) 
            $msg .= T_("Tu ne peux pas marier ton hamster avec lui-même !");
        else if ($erreurAction == 5) 
            $msg .= T_("Tu ne peux pas marier ton hamster avec un hamster de même sexe !");
        else if ($erreurAction == 6) 
            $msg .= T_("Le hamster à qui tu as fait la demande est ... déjà marié !");
        else if ($erreurAction == 7) 
            $msg .= T_("Il faut donner le pseudo du joueur qui possède le hamster.");            
        else if ($erreurAction == 8) {
            $msgTmp = T_("Tu n'as pas assez d'argent pour payer la demande en mariage car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_HAMSTER);
        }
    }
    else if ($action == "rompreMariage") {
        if ($erreurAction == 0)
            $msg .= "<div>".T_("Tu viens de rompre l'union des deux hamsters. Retrouve-vite un nouveau compagnon pour ton hamster !")."</div>";
        else if ($erreurAction == 1 ) {
            $msgTmp = T_("Tu n'as pas assez d'argent pour payer la rupture du mariage car tu ne possèdes que ").$userdata['nb_pieces'].IMG_PIECE;
            $msg .= afficherPasAssezArgent($msgTmp,PAGE_JEU_HAMSTER);
        }
    }
    else if ($action == "annulerDemandeMariage") {
        $msg .= "<div>".T_("Tu viens d'annuler ta demande en mariage. Tu as récupéré les ").$coutDemandeMariage." ".IMG_PIECE.".</div>";
    }    
}

?>

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">
  
  

<h2 align=center><?php echo T_("Mariages");?></h2>

<div align=center>
    <?php echo T_("Ici, tu peux marier tes hamsters !");?>
    <br/>&nbsp;<br/>

<?php 
if ($msg != "" ) {
    echo "<div class=hamBlocColonne>".$msg."</div>" ;
}

$nbHamstersMaries = 0;
for($ham = 0;$ham<$nbHamsters;$ham++){
    if ($lst_hamsters[$ham]['compagnon_id']>0)
        $nbHamstersMaries ++;        
}

// sollicitation à une demande en mariage (pour chaque hamster)
for($ham = 0;$ham<$nbHamsters;$ham++) {
    
    $hamRow = $lst_hamsters[$ham] ;
    $query = "SELECT demandeur_id, receveur_id FROM interactions WHERE receveur_id = ".$hamRow['hamster_id']. " AND type = ".DEMANDE_MARIAGE;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbDemandes = $dbHT->sql_numrows($result) ;

    if ($nbDemandes > 0) {
        echo "<div class=\"mariageBlocPrincipal\"><table><h2 align=\"left\">".T_("Demande(s) en mariage de la part d'autres hamsters")."</h2>";
    
        while($rowDemande = $dbHT->sql_fetchrow($result)) { // pour chaque demande en mariage
        
            // on récupère qq infos sur le hamster solliciteur
            $query2 = "SELECT nom, type FROM hamster WHERE hamster_id = ".$rowDemande['demandeur_id']. " LIMIT 1";
            if ( !($result2 = $dbHT->sql_query($query2)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
            }
            $nbDemandes2 = $dbHT->sql_numrows($result2) ;
            if ($nbDemandes2 > 0) { // si le joueur solliciteur existe toujours
                $joueurSolliciteurRow = $dbHT->sql_fetchrow($result2);
                echo "<tr><td align=center><img src=\"images/".$infosHamsters[$joueurSolliciteurRow['type']][HAMSTER_IMAGE]."\" style=\"width:150px;\"><br/>".$joueurSolliciteurRow['nom']."</td>";
                echo "<td align=center>".T_("a fait une demande en mariage à ton hamster")."</td>";
                echo "<td align=center width=230><img src=\"images/".$infosHamsters[$hamRow['type']][HAMSTER_IMAGE]."\" style=\"width:150px;\">";
                echo "<br/>".$hamRow['nom']."<br>&nbsp;<br/><a href=\"jeu.php?mode=m_mariages&actionInteraction=accepterMariage&hamster_sollicite_id=".$hamRow['hamster_id']."&hamster_solliciteur_id=".$rowDemande['demandeur_id']."\">Accepter la demande<br/>(".T_("dépenses pour la fête :")." ".$coutDemandeMariage." ".IMG_PIECE.")</a><br/>ou<br/><a href=\"jeu.php?mode=m_mariages&actionInteraction=refuserMariage&hamster_sollicite_id=".$hamRow['hamster_id']."&hamster_solliciteur_id=".$rowDemande['demandeur_id']."\">".T_("Refuser la demande")."</a></td></tr>";
            }
        }
        echo "</table></div>";
    }
}


if ($nbHamstersMaries > 0) {
    echo "<div class=\"mariageBlocPrincipal\"><h2 align=\"left\">";
    if ($nbHamstersMaries > 1)
        echo T_("Tes hamsters mariés");
    else 
        echo T_("Ton hamster marié") ;
    echo "</h2>";

    for($ham = 0;$ham<$nbHamsters;$ham++) {
        
        $hamRow = $lst_hamsters[$ham] ;
    
        // on affiche son lien avec un éventuel compagnon
        if ($hamRow['compagnon_id'] > 0){
            
            echo "<div class=mariageBlocUnMariage>";
            
            // on récupère qq infos sur le compagnon
            $query = "SELECT type, nom, joueur_id, sexe FROM hamster WHERE hamster_id = ".$hamRow['compagnon_id']." LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbCompagnon = $dbHT->sql_numrows($result) ;
            if ($nbCompagnon == 0) {
                // bizarre : peut-être un joueur sauvagement supprimé => on remet à zéro son compagnon
                $query2 = "UPDATE hamster SET compagnon_id = 0 WHERE hamster_id = ".$hamRow['hamster_id'];
                $dbHT->sql_query($query2);
                echo T_("Ton compagnon a disparu ... essaye de le retrouver mais il a peut-être quitter l'Hamster Academy... ") ;
                echo "<span class=mariageColonneUnMariage><img src=\"images/hamster_masque.gif\" style=\"width:100px;\"></span>";
            }
            else {
                //$nbHamstersMaries ++;
                $rowCompagnon=$dbHT->sql_fetchrow($result);
                
                echo $hamRow['nom']. T_(" est ");
                if ($hamRow['sexe'] == 0)
                    echo T_("marié");
                else
                    echo T_("mariée");
                echo T_(" avec ").returnLienProfilHamster($hamRow['compagnon_id'],tronquerTxt($rowCompagnon['nom'],15))."<br/>";
                echo "<table cellpadding=5><tr valign=center><td><img src=\"images/".$infosHamsters[$hamRow['type']][HAMSTER_IMAGE]."\" style=\"width:100px;\"></td>";
                echo "<td><img src=\"images/coeur.gif\"></td>";
                echo "<td align=left><img src=\"images/".$infosHamsters[$rowCompagnon['type']][HAMSTER_IMAGE]."\" style=\"width:100px;\"></td></tr></table>";
                $dbHT->sql_freeresult($result);        
            }
            echo "<div>";
                echo "<div class=groupe_description_titre>".T_("Carnet des amants :")."</div>" ;
                echo "<table class=mariageBulletin>";
                
                // on récupère les 10 derniers bulletins
                $query = "SELECT * FROM mariage_bulletin WHERE (hamster_id=".$hamRow['hamster_id']. " OR hamster_id=".$hamRow['compagnon_id']. ") ORDER BY date DESC LIMIT 10";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                }
                $nbBulletin = $dbHT->sql_numrows($result) ;
                if ($nbBulletin == 0) // on affiche une ligne vide si aucun message
                    echo "<tr><td style=\"font-size:small;\">".T_("Aucun message")."</td></tr>";
                else {
                    $lstMsgs = array();
                    while($rowMessages=$dbHT->sql_fetchrow($result)) {
                        array_push($lstMsgs,$rowMessages);
                    }
                    for($msgIndex=$nbBulletin-1;$msgIndex>=0;$msgIndex--) {
                        $dateMessage = date('d/m',$lstMsgs[$msgIndex]['date']);
                        echo "<tr><td style=\"font-size:small;\">[".$dateMessage."] <strong>".$lstMsgs[$msgIndex]['nom_hamster']."</strong> : ".$lstMsgs[$msgIndex]['texte']."</td></tr>";
                    }
                }
                $dbHT->sql_freeresult($result);
                echo "</table>";
                echo "<form method=get action=\"jeu.php\">";
                echo "<input type=hidden name=mode value=\"m_mariages\">";
                echo "<input type=hidden name=\"actionInteraction\" value=\"ajouterBulletinMariage\">";
                echo "<input type=hidden name=hamster_id value=\"".$hamRow['hamster_id']."\">";
                echo "<table cellpadding=0 cellspacing=0><tr><td style=\"font-size:small;\">".T_("Ecrire dans le carnet :")." &nbsp;</td><td><input type=textbox name=bulletinTexte size=70 maxlength=150></td><td><input type=submit value=\"".T_("envoyer")."\"></td></tr></table>";
                echo "</form>";
                echo "</div>";
                    
                echo T_("Actions :")." <a href=\"\" onClick=\"javascript: if (confirm('".T_("Tu es sûr de rompre leur relation ? Une rupture coute ").$coutDivorce." ".T_("pièces")."...')) document.location.href='jeu.php?mode=m_mariages&hamster_id=".$hamRow['hamster_id']."&actionInteraction=rompreMariage' ; else return false ; return false;\" title=\"".T_("Veux-tu rompre leur relation ?")." (".T_("coût")." : 10 ".T_("pièces").")\">".T_("Rompre leur relation")."</a>";
                echo " - <a href=\"jeu.php?mode=m_messages&dest_joueur_id=".$rowCompagnon['joueur_id']."\">Ecrire à l'éleveur de ".tronquerTxt($rowCompagnon['nom'],15)."</a>";
                if ($hamRow['sexe'] == 1 && $rowCompagnon['sexe'] == 0 && $hamRow['sante'] > 0) { // si le hamster est une femelle non enceinte et si le compagnon est bien un mâle
                    echo "- <a href=\"jeu.php?mode=m_mariages&amp;hamster_id=".$hamRow['hamster_id']."&amp;bebe=".$hamRow['compagnon_id']."\" title=\"".T_("Avoir un bébé avec ").$rowCompagnon['nom']." !\">".T_("Avoir un bébé avec ").$rowCompagnon['nom']."</a>";
                }
                echo "</div>";
                echo "<div style=\"float:left; margin-bottom:30px;\">&nbsp;</div>";
        }
    }
    echo "</div>";
}

echo "<div class=mariageBlocPrincipal>";

if ($nbHamstersMaries != $nbHamsters) {
    echo "<h2 align=left>".T_("Demande en mariage")."</h2>";
    
    echo "<div><table cellpadding=10>";
    // demande en mariage (à faire ou en cours)
    for($ham = 0;$ham<$nbHamsters;$ham++) {
        
        $hamRow = $lst_hamsters[$ham] ;
        
        if ($hamRow['compagnon_id'] == 0) {
            
            // a-t-il une demande en cours
            $query = "SELECT demandeur_id, receveur_id FROM interactions WHERE demandeur_id = ".$hamRow['hamster_id']. " AND type = ".DEMANDE_MARIAGE. " LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbDemandes = $dbHT->sql_numrows($result) ;
            
            if ($nbDemandes > 0) {
                $rowDemande = $dbHT->sql_fetchrow($result);
                $query2 = "SELECT nom, type FROM hamster WHERE hamster_id = ".$rowDemande['receveur_id'];
                if ( !($result2 = $dbHT->sql_query($query2)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query2);
                }
                $nbDemandes2 = $dbHT->sql_numrows($result2) ;
                if ($nbDemandes2 > 0) { // si le joueur sollicité existe toujours
                    $joueurSolliciteRow = $dbHT->sql_fetchrow($result2);
                    echo "<tr valign=top><td align=center><img src=\"images/".$infosHamsters[$hamRow['type']][HAMSTER_IMAGE]."\" style=\"width:150px;\"></td>";
                    echo "<td align=center>".$hamRow['nom']." ".T_("n'est pas marié(e) !")." <br>&nbsp;<br/>";
                    echo "Il a fait une demande en mariage à </td><td><img src=\"images/".$infosHamsters[$joueurSolliciteRow['type']][HAMSTER_IMAGE]."\" style=\"width:150px;\"><br/>".$joueurSolliciteRow['nom']."<br>(elle est prévenue : on attend sa réponse...)";
                    echo "<br/><a href=\"\" onClick=\"javascript: if (confirm('".T_("Tu es sûr(e) de vouloir annuler ta demande en mariage ?")."')) document.location.href='jeu.php?mode=m_mariages&hamster_id=".$hamRow['hamster_id']."&actionInteraction=annulerDemandeMariage' ; else return false ; return false;\" title=\"Veux-tu annuler la demande en mariage ?\">(".T_("Annuler la demande et récupérer les ").$coutDemandeMariage."&nbsp;".T_("pièces ?").")</a></td></tr>";
                }
                else {
                    // le joueur sollicité n'existe plus, on supprime la demande
                    $query2 = "DELETE FROM interactions WHERE demandeur_id = ".$hamRow['hamster_id'];
                    $dbHT->sql_query($query2) ;
                    echo T_("Ton hamster a fait une demande à un hamster mais ce dernier a malheureusement quitté le jeu...")."<br/>&nbsp;<br/>".T_("Re-clique sur ")."<a href=\"jeu.php?mode=m_mariages&univers=0\">".T_("Mariages")."</a> ".T_("pour faire une nouvelle demande.") ;
                }
            }
            else{ // proposer une demande
                echo "<tr valign=top><td align=center><img src=\"images/".$infosHamsters[$hamRow['type']][HAMSTER_IMAGE]."\" style=\"width:150px;\"></td>";
                echo "<td colspan=2>".$hamRow['nom']." ".T_("n'est pas marié(e)").". <br>&nbsp;<br/>";
                echo T_("Veux-tu faire une demande au hamster")."<br> <form method=get action=\"jeu.php\"><input type=hidden name=mode value=m_mariages><input type=hidden name=\"actionInteraction\" value=\"demandeMariage\"><input type=hidden name=hamster_id value=".$hamRow['hamster_id']."><input type=textbox value=\"\" name=nomHamster>";
                echo "<br> ".T_("qui appartient au joueur")." <br><input type=textbox value=\"\" name=nomJoueur><br><input type=submit value=\"".T_("Faire une demande à cet hamster")."\"></form><div style=\"font-size:small;\">".T_("La demande en mariage coûte ").$coutDemandeMariage." ".IMG_PIECE." ".T_("les dépenses liées à la fête. Cette somme est remboursée si la demande est refusée par l'autre joueur").".</div><br/></td></tr>";
            }
        }
    }
    
    echo "</table></div>";
}
echo "</div>";

echo "<div class=mariageBlocPrincipal>";
if ( ! isset($_GET['voirleshamsters'])) {
    echo T_("Tu ne sais pas qui choisir ? Consulte")." <a href=\"jeu.php?mode=m_mariages&voirleshamsters=1\">".T_("l'annuaire des hamsters pour choisir un mari ou une femme !")."</a>";
}
else {

    echo "<h2 align=left>".T_("Annuaire des hamsters (les plus actifs)")."</h2>";
    
    // liste des hamsters (et de leurs joueurs) qui peuvent se marier (joueurs connectés depuis moins de 2 semaines = 60*3600*24 = 5184000)
    $query = "SELECT h.nom, h.hamster_id, h.sexe, h.type, j.pseudo, j.joueur_id FROM joueurs j, hamster h WHERE j.niveau >= ".$niveauMinimalPourLeMariage." AND h.joueur_id = j.joueur_id AND j.liste_rouge = 0 AND h.compagnon_id = 0 AND h.sante > 0 AND j.user_lastvisit > ".($dateActuelle - 259200)." ORDER BY j.joueur_id DESC LIMIT 200"; // 3 jours
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    echo "<table cellpadding=5><tr><td><strong>".T_("Nom du hamster")."</strong></td><td><strong>".T_("Sexe")."</strong></td><td><strong>".T_("Image")."</strong></td><td><strong>".T_("Nom du joueur")."</strong></td></tr>";
    while($rowListeHamster = $dbHT->sql_fetchrow($result)) {
        echo "<tr><td>".returnLienProfilHamster($rowListeHamster['hamster_id'],tronquerTxt($rowListeHamster['nom'],15))."</td><td>";
        if ($rowListeHamster['sexe']==0)
            echo T_("Mâle");
        else
            echo T_("Femelle");
        echo "</td><td><img src=\"images/".$infosHamsters[$rowListeHamster['type']][HAMSTER_IMAGE]."\" height=50></td>";
        echo "<td>".returnLienProfil($rowListeHamster['joueur_id'],tronquerTxt($rowListeHamster['pseudo'],15))."</td></tr>";
    }
    echo "</table>";
}
echo "</div>";

?>
</div>


  <div style="clear:both;">&nbsp;</div>


  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 
  
  

</div>

