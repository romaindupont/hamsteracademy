<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$searchPseudo = "";
$searchHamster = "";

if (isset($_GET['searchPseudo']) && $_GET['searchPseudo'] != "")
    $searchPseudo = mysql_real_escape_string($_GET['searchPseudo']) ;
if (isset($_GET['searchHamster']) && $_GET['searchHamster'] != "")
    $searchHamster = mysql_real_escape_string($_GET['searchHamster']) ;
    
$action = "";
$hamster_id_concerne = -1;
$nomHamsterConcerne = "";

if (isset($_GET['action']))
    $action = mysql_real_escape_string($_GET['action']) ;
if (isset($_GET['hamster_id_concerne'])){
    $hamster_id_concerne = intval($_GET['hamster_id_concerne']) ;
    $nomHamsterConcerne = getNomHamsterFromId($hamster_id_concerne);
}
    
$msg = "";
$contrainteSexe = "";
if ($action == "avoirunbebe")
    $contrainteSexe = "AND h.sexe = 0";
$query = "SELECT j.joueur_id, j.pseudo, j.image, h.hamster_id, h.nom, h.sexe, h.type FROM joueurs j, hamster h WHERE "; 
$query_ok = false;

if ($searchPseudo != "") {
    $pseudoSoumis = str_replace("*","%",$searchPseudo);
    $query .= "j.pseudo LIKE '".$pseudoSoumis."' AND h.joueur_id = j.joueur_id $contrainteSexe ORDER BY pseudo";
    $query_ok = true;
}
else if ($searchHamster != "" ) {
    $hamsterSoumis = str_replace("*","%",$searchHamster);
    $query .= "h.nom LIKE '".$hamsterSoumis."'  AND h.joueur_id = j.joueur_id $contrainteSexe ORDER BY nom";
    $query_ok = true;
}
else if( isset($_GET['searchPseudo']) || isset($_GET['searchHamster']))
    $msg .= T_("Il faut saisir un pseudo de joueur ou un nom de hamster");

$query .= " LIMIT 100";

$lstResultats = array();

if($query_ok) {

    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error  ', '', __LINE__, __FILE__, $query);
    }
    $nbJoueursTrouves = $dbHT->sql_numrows($result);
    if ($nbJoueursTrouves == 0) {
        $msg .= T_("Aucun joueur ou hamster trouvé");
    }
    else {
        if ($nbJoueursTrouves == 1)
            $msg .= $nbJoueursTrouves.T_(" joueur ou hamster trouvé")." :<br/>";
        else if ($nbJoueursTrouves == 100)
            $msg .= T_("Il y a trop de réponses possibles. Les 100 premiers joueurs ou hamsters trouvés sont ci-dessous :")." :<br/>";
        else
            $msg .= $nbJoueursTrouves.T_(" joueurs ou hamsters trouvés")." :<br/>";
            
        while($row=$dbHT->sql_fetchrow($result)) {
            array_push($lstResultats,array(
                'pseudo' => $row['pseudo'],
                'lienprofil' => returnLienProfil($row['joueur_id'],$row['pseudo']),
                'joueur_id' => $row['joueur_id'],
                'nom' => $row['nom'],
                'hamster_id' => $row['hamster_id'],
                'type' => $row['type'],
                'image' => $row['image'],
                'sexe' => $row['sexe'],
            ));
        }
    }
}

$smarty->assign('action', $action);
$smarty->assign('contrainteSexe', $contrainteSexe);
$smarty->assign('hamster_id_concerne', $hamster_id_concerne);
$smarty->assign('nomHamsterConcerne', $nomHamsterConcerne);
$smarty->assign('searchHamster', $searchHamster);
$smarty->assign('searchPseudo', $searchPseudo);
$smarty->assign('lstResultats', $lstResultats);
$smarty->assign('msg', $msg);
$smarty->display('annuaire.tpl');

?>