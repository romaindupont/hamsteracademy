<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";

// profil accessible à tous, depuis l'extérieur

//$temps_mid = microtime_float();

//$userdata = session_pagestart($user_ip);

//$temps_mid2 = microtime_float();

//    
//if( ! $userdata['session_logged_in'] ) { 
//    echo "Erreur de connection";
//    redirectHT("index.php?err=1");
//}

$joueur_id = -1;
if (isset($_GET['joueur_id']))
    $joueur_id = intval($_GET['joueur_id']);
else
    return;
    
if ($joueur_id == 1)
    return;
    
$query = "SELECT * FROM joueurs WHERE joueur_id=".$joueur_id." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining joueur', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;
if ($nbJoueurs != 1)
    return;
$rowJoueur=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

// liste des hamsters
// ----------------------
$query = "SELECT hamster_id, nom FROM hamster WHERE joueur_id=".$joueur_id;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamsters = $dbHT->sql_numrows($result) ;
$rowHamsters=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($rowHamsters,$row);
}
$dbHT->sql_freeresult($result);

// liste des cages
// ----------------------
$query = "SELECT cage_id, nom FROM cage WHERE joueur_id=".$joueur_id;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;
$rowCages=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($rowCages,$row);
}
$dbHT->sql_freeresult($result);

$pagetitle = "Hamster Academy - Profil de ".$rowJoueur['pseudo'];
$univers = UNIVERS_ELEVEUR;
$pageWidth = 600;
$description = T_("Profil de ").$rowJoueur['pseudo'].T_(" : retrouve toutes les infos sur ce joueur !");
$liste_styles = "style.css,styleIndex.css";    
$liste_scripts = "jquery-1.4.2.min.js";
$keywords = T_("Hamster, Academy, Profil, Amis, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer");
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('pageWidth', $pageWidth);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('keywords', $keywords);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>

<script type="text/javascript" >
// encoding: utf-8
// $.fn.linkify 1.0 - MIT/GPL Licensed - More info: http://github.com/maranomynet/linkify/
(function(c){var f=/(^|"|&lt;|\s)(www\..+?\..+?)(\s|&gt;|"|$)/g,g=/(^|"|&lt;|\s)(((https?|ftp):\/\/|mailto:).+?)(\s|&gt;|"|$)/g;c.fn.linkify=function(){return this.each(function(){var d=this.childNodes,e=d.length;while(e--){var a=d[e];if(a.nodeType==3){var b=a.nodeValue;if(/\S/.test(b)){b=b.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(f,'$1<a href="<``>://$2">$2</a>$3').replace(g,'$1<a href="$2">$2</a>$5').replace(/"<``>/g,'"http');c(a).after(b).remove()}}else if(a.nodeType==1&&!/^(a|button|textarea)$/i.test(a.tagName)){arguments.callee.call(a)}}})}})(jQuery);

$(document).ready(
function()
{
    $('#lien_blog').linkify();
});
</script>
<div id="tooltip"></div>
<div style="text-align:center;width:50Opx; margin-left:auto; margin-right:auto; margin-top:50px;">
    
<div style="width:500px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
  
  <div class="hamBlocColonne_full" style="width:440px; text-align:center;">

    <div class="logoMain" style="left:80px;top:-100px;margin-bottom:-150px;z-index:0; position:relative; background:transparent;">
        <div class="logoImage">
            <img src="images/logoHamsterAcademy_simple.gif" alt="Hamster Academy" />
        </div>
    </div>
    <div style="clear:both;">&nbsp;</div>
        
    <h1><?php echo $rowJoueur['pseudo']; ?></h1>
    
    <?php 
    if ($rowJoueur['montrer_profil'] == 0)
        echo T_("Le joueur a choisi ne pas montrer son profil."); 
    else if ($rowJoueur['suspendu'] != 0)
        echo T_("Le joueur a été suspendu."); 
    else {
    ?>
    
    <!-- <div style="text-align:left;width:30Opx; margin-left:auto; margin-right:auto;"> -->    
    
    <?php
        echo "<div align=\"center\">";
        if ($rowJoueur['image'] > 0){
            $extensionImage = "jpg";
            if ($rowJoueur['image'] == 2) // extension gif
                $extensionImage = "gif";
            echo "<img src=\"images/joueurs/".$rowJoueur['joueur_id'].".".$extensionImage."\" alt=\"".$rowJoueur['pseudo']."\" /><br/>";
        }
        else {
            echo "<img src=\"images/grande_silhouette.gif\" alt=\"".$rowJoueur['pseudo']."\" /><br/>";
        }
        echo "<i>".stripslashes($rowJoueur['presentation'])."</i>" ;
        echo "</div><br/>";
    ?>
    
    <div><?php echo T_("Inscrit le : ").format_date($rowJoueur['date_inscription']) ; ?></div>
    <div><?php echo T_("Dernière connexion le : ").format_date($rowJoueur['user_lastvisit']) ; ?></div>
    <br/>
    <?php
    if ($rowJoueur['vip'])
        echo "<strong>VIP !</strong><br/>&nbsp;<br/>";
    ?>
    <strong><?php echo T_("Evolution dans le jeu");?></strong> : <br/>
    <div><?php echo T_("Niveau : ").$rowJoueur['niveau'] ; ?></div>
    <div><?php echo T_("Nombre de points : ").$rowJoueur['note'] ; ?></div>
    <div><?php echo T_("Classement : ").calculerClassement($rowJoueur['note']) ; ?></div>
    <div><?php echo T_("Défis gagnés : ").$rowJoueur['nb_defis_gagnes'] ; ?></div>
    <div><?php echo T_("Défis perdus : ").$rowJoueur['nb_defis_perdus'] ; ?></div>
    <br/>
    <strong><?php echo T_("Carte d'identité");?></strong> : <br/>
    <div><?php echo T_("Age : ").$rowJoueur['age'] ; ?> (<?php if ($rowJoueur['sexe']==0) echo T_("garçon") ; else if ($rowJoueur['sexe'] > 0)  echo T_("fille") ;?>)</div>
    <div><?php echo T_("Ville : ").$rowJoueur['ville'] ; ?></div>
    <div><?php echo T_("Pays : ");
                $queryPays = "SELECT * FROM config_pays WHERE flag_id = ".$rowJoueur['pays']." LIMIT 1";
                if ( !($resultPays = $dbHT->sql_query($queryPays)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining pays', '', __LINE__, __FILE__, $queryPays);
                }
                $row=$dbHT->sql_fetchrow($resultPays);
                echo "<img src=\"images/flags/".$row['flag_image']."\" style=\"vertical-align:middle;\" alt=\"".t_("Pays")."\" /> ".$row['flag_name'];
                $dbHT->sql_freeresult($resultPays);
         ?></div>    
    <?php 
        $query = "SELECT a.ami_de, j.pseudo, j.joueur_id, j.image, j.liste_rouge FROM amis a, joueurs j WHERE a.joueur_id=".$rowJoueur['joueur_id']." AND j.joueur_id = a.ami_de";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining amis', '', __LINE__, __FILE__, $query);
        }
        $nbAmis = $dbHT->sql_numrows($result) ;
        
        if ($nbAmis > 0) {
            echo "<br/><strong>".T_("Ses amis :")."</strong><br/>";
            
            while($rowAmi=$dbHT->sql_fetchrow($result)) {
            
                if ($rowAmi['image'] > 0)
                    echo "<img src=\"images/joueurs/".$rowAmi['joueur_id'].".jpg\" alt=\"\" style=\"vertical-align:middle; width:30px;\" />";
                echo "<a href=\"profil.php?joueur_id=".$rowAmi['joueur_id']."\">".$rowAmi['pseudo']."</a><br/>";    
            }        
        }
        $dbHT->sql_freeresult($result);
        
        if ($rowJoueur['blog'] != "") {
            echo "<br/><strong>".T_("Son blog :")."</strong><br/>";
            //echo "<a href=\"".$rowJoueur['blog']."\" target=\"hblank\">".$rowJoueur['blog']."</a><br/>";            
            echo "<div id=\"lien_blog\">".$rowJoueur['blog']."</div><br/>";            
        }
    
    ?>    
    <br/>
    <strong><?php if ($nbHamsters == 1) echo T_("Son hamster") ; else echo T_("Ses hamsters"); ?></strong> : <br/>
    <?php 
        for($ha=0;$ha<$nbHamsters;$ha++)
            echo "<div><a href=\"afficheHamster.php?hamster_id=".$rowHamsters[$ha]['hamster_id']."\">".$rowHamsters[$ha]['nom']."</a></div>";
    ?>
    <br/>
    <strong><?php if ($nbCages == 1) echo T_("Sa cage") ; else echo T_("Ses cages"); ?></strong> : <br/>
    <?php 
        for($ca=0;$ca<$nbCages;$ca++)
            echo "<div><a href=\"afficheCage.php?cage_id=".$rowCages[$ca]['cage_id']."\">".$rowCages[$ca]['nom']."</a></div>";
    ?>
    <br/>
    <?php 
        
        if ($rowJoueur['liste_rouge'] == 0) {
            echo "<br/><div>".returnLienLuiEcrire($rowJoueur['joueur_id'])."</div><br/>";
        }

        echo "<div><strong>".T_("Image bonus")."</strong> : <br/>";
        if ($rowJoueur['image_bonus'] > 0){
            echo "<a href=\"images/joueurs/".$rowJoueur['joueur_id']."_bonus.jpg\"><img src=\"images/joueurs/".$rowJoueur['joueur_id']."_bonus.jpg\" alt=\"Image de ".$rowJoueur['pseudo']."\" width=\"100\" /></a><br/>";
            
        }
        else {
            echo T_("pas d'image");
        }
        echo "</div>";
    ?>
<?php } ?>

    
  <div style="clear:both;">&nbsp;</div>
  
  </div>
  
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 
  
</div>
<br/><a href="#" onclick="javascript:window.close();"><?php echo T_("Fermer la fenêtre");?></a><br/>&nbsp;<br/>

<?php 

require "footer.php";

?>