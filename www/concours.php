<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

//if ($userdata['pseudo'] != "HamsterAcademy") {
//    $msg .= "<br/>&nbsp;<br/>Les résultats du concours du meilleur poème/chanson sont annoncés d'ici 1 heure.<br/>&nbsp;<br/>";
//    return;
//}

require_once "gestionAcademy.php"

?>
<link href="http://www.hamsteracademy.fr/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />


<div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>

     <div class="hamBlocColonne-top-center"></div>        
    
    <div class="hamBlocColonne_full">

 <?php
 if ($msg != "" ) {
    echo "<div class=\"hamBlocColonne\">".$msg."</div>" ;
}
?>
    
<?php if ($userdata['niveau'] >= NIVEAU_DEFIS5_18) { 

echo "<span class=\"titre1\">".T_("Tes défis")."</span><br/>&nbsp;<br/>";
?>
<script type="text/javascript">
$().ready(function() {
    $("#pseudoDefie").autocomplete("listePseudos.php?niveauMin=<?php echo $userdata['niveau'] ;?>&amp;connexionRecente=1&amp;lang=<?php echo $lang ;?>", {
        minChars: 0,
        width: 200,
        max: 100,
        selectFirst: false
    });

});
</script>
<?php

echo "<form action=\"jeu.php\" method=\"get\">";
echo "<input type=\"hidden\" name=\"mode\" value=\"m_concours\" />";
echo "<input type=\"hidden\" name=\"univers\" value=\"$univers\" />";
echo "<input type=\"hidden\" name=\"actionInteraction\" value=\"defierJoueur\" />";
echo T_("Proposer un nouveau défi à")." "."<input type=\"text\" id=\"pseudoDefie\" name=\"pseudoDefie\" value=\"\" size=\"20\"/> ".T_(" avec une mise");
//echo "<input type=\"checkbox\" id=\"avecmise\" name=\"avecmise\" onclick='
//        if (this.checked) {
//            $(\"#champMise\").show();
//            $(\"#avecmise\").hide();
//        }
//        return true;'
//        />";
echo "<span id=\"champMise\" style=\"/*display: none;*/\" > ".T_("de")." <input type=\"text\" name=\"mise\" value=\"1\" size=\"5\" />"." ".T_("pièces")."</span>";
echo "&nbsp;&nbsp;&nbsp;<input type=\"submit\" value=\"ok !\" />";
echo "</form>";
 
echo "<br/>";
$query = "SELECT * FROM interactions WHERE demandeur_id = ".$userdata['joueur_id']." AND type = ".TYPE_INTERACTION_DEFI;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
}
$nbDefis = $dbHT->sql_numrows($result) ;
if ($nbDefis > 0) {
    echo T_("Tu as défié :")."<br/>";
    while ($row=$dbHT->sql_fetchrow($result)) {
        
        echo "&nbsp;&nbsp;".returnLienProfil($row['receveur_id'],getPseudoFromId($row['receveur_id'])); 
        $mise = $row['status'];
        if ($mise > 0)
            echo ", ".T_("avec une mise de ").$mise. " ".IMG_PIECE;
        echo " (<a href=\"jeu.php?mode=m_concours&amp;univers=$univers;&amp;actionInteraction=annulerDefi&amp;defi_id=".$row['interaction_id']."\">".T_("annuler")."</a>)<br/>";
    }    
    $dbHT->sql_freeresult($result);
    echo "<br/>&nbsp;<br/>";
}
$query = "SELECT * FROM interactions WHERE receveur_id = ".$userdata['joueur_id']." AND type = ".TYPE_INTERACTION_DEFI;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
}
$nbDefis = $dbHT->sql_numrows($result) ;
if ($nbDefis > 0) {
    if ($userdata['sexe'] == 0)
        echo T_("Tu es défié par :")."<br/>";
    else
        echo T_("Tu es défiée par :")."<br/>";
    while ($row=$dbHT->sql_fetchrow($result)) {
        
        echo "&nbsp;&nbsp;".returnLienProfil($row['demandeur_id'],getPseudoFromId($row['demandeur_id'])); 
        $mise = $row['status'];
        if ($mise > 0)
            echo ", ".T_("avec une mise de ").$mise. " ".IMG_PIECE;
        echo " : <a href=\"jeu.php?mode=m_concours&amp;univers=$univers;&amp;actionInteraction=accepterDefi&amp;defi_id=".$row['interaction_id']."\">".T_("accepter !")."</a> ".T_("ou")." ";
        echo "<a href=\"jeu.php?mode=m_concours&amp;univers=$univers;&amp;actionInteraction=refuserDefi&amp;defi_id=".$row['interaction_id']."&amp;defieur_id=".$row['demandeur_id']."\">".T_("refuser")."</a>";
        echo "<br/>";
    }    
    $dbHT->sql_freeresult($result);
    echo "<br/>&nbsp;<br/>";
}
echo "<br/>&nbsp;<br/>";

}
?>

<?php

if (0 && $lang == "en") {

    echo "<br/>&nbsp;<br/>Competitions will start in december!<br/>&nbsp;<br/>";
}
else {

    if ($lang == "fr") {
        $texteConcours = getConfig("texteConcours");
        if ($texteConcours != "" && $texteConcours != "0") 
            echo "<div>".$texteConcours."</div>";
            
        // concours flash
        // --------------
        echo "<div>";
        require "concoursFlash.php";
        echo "</div><br/>&nbsp;<br/>";            
    }
    
    // concours automatique de la plus belle cage
    if (getConfig("concours_cage_auto_active") == 1) {
        
        echo "<span class=\"titre1\">".T_("Concours du mois de")." ".$mois_txt[$month]. " ".$year." : ".T_("la plus belle cage !")." </span>";

        echo "<br/>&nbsp;<br/>";

        echo "<strong>".T_("Le thème")."</strong> : ".T_("que la plus belle cage de hamster gagne !")."<br/>";
        echo "<strong>".T_("Date limite")."</strong> : 7 ".$mois_txt[$month]. " ".$year." ".T_("inclus pour l'inscription et la participation")."<br/>";
        echo "<strong>".T_("Coût d'inscription")."</strong> : 20 ".IMG_PIECE." <br/>";
        echo "<strong>".T_("Nombre d'inscrits")." </strong> : ".T_("il y a actuellement")." ";

        $query = "SELECT COUNT(joueur_id) FROM joueurs WHERE inscritConcours > 0";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $r = mysql_fetch_row($result);
        echo $r[0];
        echo " ".T_("joueurs inscrits")."<br/>";
        echo "<strong>".T_("Réglement")."</strong> : <ul>";
        
        echo "<li>".T_("L'heure limite pour l'inscription et la participation est le 7")." ".$mois_txt[$month]." ".T_("inclus, jusqu'à 23h59").".</li>";
        echo "<li>".T_("Une cage par joueur pourra être proposée pour le concours : un petit bouton qui sera installé dans quelques jours permettra de choisir laquelle").".</li>";
        echo "<li>".T_("Les prix vont de 400 pièces pour le vainqueur, 200 pour les dix meilleures cages et 50 pièces pour les autres 100 meilleurs cages. Chaque participant aura un petit cadeau").".</li>";
        echo "</ul>";        
        require "concours/concoursPlusBellCage.php";
    }
    else {
        //$typeConcours = "meilleureForce";
        $typeConcours = "meilleureProgression";
        
        if ($typeConcours == "meilleureForce") {
            echo "<span class=\"titre1\">".T_("Concours du mois de")." ".$mois_txt[$month]. " ".$year." : ".T_("le hamster le plus fort !")." </span>";
            echo "<br/>&nbsp;<br/>";
            echo "<strong>".T_("Le thème")."</strong> : ".T_("que celui qui a le plus fort hamster gagne !")."<br/>";
            echo "<strong>".T_("Date limite")."</strong> : 15 ".$mois_txt[$month]. " ".$year." ".T_("inclus pour l'inscription et la participation")."<br/>";
        }
        else if ($typeConcours == "meilleureProgression") {
            echo "<span class=\"titre1\">".T_("Concours du mois de")." ".$mois_txt[$month]. " ".$year." : ".T_("le joueur qui a le mieux progressé !")." </span>";
            echo "<br/>&nbsp;<br/>";
            echo "<strong>".T_("Le thème")."</strong> : ".T_("que celui qui a le plus augmenté ses points gagne !")."<br/>";
            echo "<strong>".T_("Date limite")."</strong> : 6 ".$mois_txt[12]. " ".$year." ".T_("inclus pour l'inscription et la participation")."<br/>";
        }
        
        echo "<strong>".T_("Coût d'inscription")."</strong> : 20 ".IMG_PIECE." <br/>";
        echo "<strong>".T_("Nombre d'inscrits")." </strong> : ".T_("il y a actuellement")." ";

        $query = "SELECT COUNT(joueur_id) FROM joueurs WHERE inscritConcours > 0";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $r = mysql_fetch_row($result);
        echo $r[0];
        echo " ".T_("joueurs inscrits")."<br/>";
        echo "<strong>".T_("Réglement")."</strong> : <ul>";
        
        if ($typeConcours == "meilleureForce") {
            echo "<li>".T_("L'heure limite pour l'inscription et la participation est le 15")." ".$mois_txt[$month]." ".T_("inclus, jusqu'à 23h59").".</li>";
            echo "<li>".T_("Si le joueur possède plusieurs hamsters, seul le hamster le plus fort d'entre eux sera pris en compte").".</li>";
            echo "<li>".T_("Les prix vont de 400 pièces pour le vainqueur, 200 pour les dix meilleurs joueurs et 50 pièces pour les autres 100 plus forts hamsters. Chaque participant aura un petit cadeau").".</li>";
        }
        else if ($typeConcours == "meilleureProgression") {
            echo "<li>".T_("L'heure limite pour l'inscription et la participation est le 6")." ".$mois_txt[12]." ".T_("inclus, jusqu'à 23h59").".</li>";
            echo "<li>".T_("A l'inscription, le nombre de points du joueur est mémorisé. L'objectif : avoir le plus de points ! Mais pour que ce soit équitable entre tous les joueurs, c'est la progression qui est prise en compte. Exemple : si un joueur a 100 points à l'inscription et qu'il a 300 points à la fin du concours, il a donc multiplié par 3 ses points. 3 est le nombre pris en compte pour le concours, pas 300.").".</li>";
            echo "<li>".T_("Les prix vont de 400 pièces pour le vainqueur, 200 pour les dix meilleurs joueurs et 50 pièces pour les 50 autres meilleurs.").".</li>";
        }
        echo "</ul>";    
        echo "<div>";
        if ($userdata['inscritConcours'] == 0){
            echo "<table><tr valign=baseline><td valign=baseline><strong>".T_("Je m'inscris")."</strong> (20 ".IMG_PIECE.") :&nbsp;&nbsp;</td><td valign=baseline><form action=\"jeu.php\" method=get><input type=hidden name=mode value=m_concours><input type=hidden name=action value=inscrireConcours><input type=submit name=inscriptionConcours value=\"".T_("S'inscire au concours !")."\"></form></td></tr></table></div>";
        }
        else {
            if ($typeConcours == "meilleureForce")
                echo "<div><strong>".T_("Tu es inscrit au concours !")."</strong> ".T_("Tu as jusqu'au 15")." ".$mois_txt[$month]." ".T_("inclus pour entraîner ton hamster !");
            else if ($typeConcours == "meilleureProgression"){
                echo "<div><strong>".T_("Tu es inscrit au concours !")."</strong> ".T_("Tu as jusqu'au 6")." ".$mois_txt[12]." ".T_("inclus pour augmenter tes points!");
                if (0) {
                    $score = round( ($userdata['note']/$userdata['inscritConcours']),1);
                    $queryClassement = "select count(note/inscritConcours) from joueurs where inscritConcours > 0 AND (note/inscritConcours) > ".$score." order by (note/inscritConcours) DESC";
                    $resultClassement = $dbHT->sql_query($queryClassement);
                    if ( ! $resultClassement ) {
                        echo "Erreur SQL : ".$queryClassement."<br>" ;
                    }
                    $rowClassement = $dbHT->sql_fetchrow($resultClassement);
                    $classementProvisoire = max(0,$rowClassement[0] );
                    $dbHT->sql_freeresult($resultClassement);
                    
                    echo "<br/>&nbsp;<br/>".T_("A ton inscription, tu avais")." ".$userdata['inscritConcours']." ".T_("points. Maintenant, tu as")." ".$userdata['note']." ".T_("points, soit une progression de")." ".$score." (".$userdata['inscritConcours']." * <strong>".$score."</strong> = ".$userdata['note'].").<br/>&nbsp;<br/>".T_("Ton score au concours est donc pour l'instant")." <strong>".$score."</strong> ! ";
                    if ($day < 3)
                        echo "(".T_("au classement général, tu es pour l'instant")." <strong>".$classementProvisoire.( $classementProvisoire==1 ? T_("er") : T_("ème"))."</strong>)";
                    else
                        echo T_("(le classement n'est plus indiqué lors des 3 derniers jours)");
                }
            }
        }
        
        // en fin de concours seulement
        if (1){
            if ($typeConcours == "meilleureForce")
                require "concoursMeilleureForce.php";
            else if ($typeConcours == "meilleureProgression")
                require "concoursMeilleureProgression.php";
        }
        echo "</div>";
    }
?>
    <!--    Concours de dessin
                
                <li>Chaque joueur peut envoyer au choix un dessin ou une histoire dessinée ayant un rapport avec les hamsters.</li>      
                <li>Tout dessin n'ayant aucun rapport avec les hamsters (ou les rongeurs) sera ignoré.</li>
                <li>Le joueur peut nous envoyer son dessin avec 3 façons différentes : 
                    <ol>
                        <li><a href="options.php?mode=m_profil" title="changer l'image bonus">Via l'image bonus dans le menu du profil</a> (2ème dessin)</li>
                        <li>En envoyant un mail avec le dessin ou l'histoire à <a href="mailto:concours@hamsteracademy.fr?subject=Concours">concours@hamsteracademy.fr</a></li>                
                        <li>En envoyant le dessin ou l'histoire à l'adresse suivante : <br/><strong>
                        &nbsp;&nbsp;Ludique Production<br/>
                        &nbsp;&nbsp;Concours de Hamster Academy<br/>
                        &nbsp;&nbsp;9 rue des Innocents<br/>
                        &nbsp;&nbsp;75001 Paris</strong><br/>
                        (merci de nous prévenir si vous envoyez un courrier par la poste)<br/>
                        Les dessins reçus seront ensuite affichés dans le jeu.
                        </li>
                    </ol>
                </li>        
        -->
        <!--    Concours de la plus belle cage
              --> 

        <!--
            Concours du meilleur moral
                <li>Durant le concours, pour les joueurs inscrits au concours, le moral de leur hamster n'est plus bloqué à 10 mais à l'infini !</li>
                <li>Tous les hamsters du participant sont inscrits d'office au concours.</li>
                <li>Attention à ne pas tomber malade pendant le concours, cela remet le moral à un faible niveau ! Quitte ou double lors de la promenade&nbsp;! Dites-vous que même le meilleur n'est pas à l'abri d'un virus le dernier jour ! :-)</li>
                <li>Les prix vont de 400 pièces pour le vainqueur, 200 pour les dix meilleurs hamsters et 50 pièces pour les autres 100 meilleurs hamsters. Chaque participant aura un petit cadeau.</li>
        -->

<br/>&nbsp;<br/>


<?php 

if ($lang == "fr") {
    
    if (0) {
        $query = "SELECT * FROM tournois";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbTournois = $dbHT->sql_numrows($result) ;
            
        $txt = "";

        while ($row = $dbHT->sql_fetchrow($result) ) {

            echo "<span class=\"titre1\">Tournoi : ".$row['nom']."</span>";
            echo "<br/>&nbsp;<br/>";
            
            if ($row['inscriptions_ouvertes'] && $dateActuelle < $row['date_cloture_inscription'])
                echo T_("Les inscriptions sont ouvertes : tu peux inscrire ton groupe dans l'Academy !");
            else
                echo T_("Inscriptions closes");
            echo " (".T_("fin des inscriptions le ").format_date($row['date_cloture_inscription'],true).")<br/>&nbsp;<br/>";
            
            $query = "SELECT groupe_id, nom FROM groupes WHERE inscrit_tournoi = ".$row['tournoi_id'];
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }

            $nbGroupes = $dbHT->sql_numrows($result) ;
            echo "<strong>".T_("Liste des groupes inscrits")." (".$nbGroupes.")</strong> : ";
            $nbGroupesAffiches = 1;
            echo "<span id=\"debutListe\">";
           
            while ($rowGroupe = $dbHT->sql_fetchrow($result) ){
                
                if ($nbGroupesAffiches == 10) {
                    echo "</span><span id=\"interListe\"><a href=\"#\" onclick='javascript:$(\"#finListe\").show();$(\"#interListe\").hide();return false;'>(".T_("voir la suite").")</a></span>";
                    echo "<span id=\"finListe\" style=\"display:none;\">";
                }
                echo tronquerTxt(htmlspecialchars($rowGroupe['nom']),25)." &middot; ";   
                $nbGroupesAffiches++;
            }
            echo "</span>";
            echo "<br/>&nbsp;<br/>";
            
            $tournoi_id = 1;
            if (isset($_GET['tournoi_id'])) 
                $tournoi_id = intval($_GET['tournoi_id']);
                
            // on affiche le tableau
            afficherTableauTournoi($tournoi_id) ;   
            
            echo "<br/>&nbsp;<br/>";

        }
    }
?>

    
<!--
<span class="titre1">2ème concours du mois de <?php echo $mois_txt[$month]. " ".$year ; ?> : le meilleur dessin ! </span>

<br/>&nbsp;<br/>

<strong>Le thème</strong> : imagine la ville ou le village idéal pour les hamsters !<br/>

<img src="images/exemple_village.jpg" /><br/>

<strong>Réglement</strong> :
<ul>
<li>Date limite d'envoi des dessins : 13 novembre 2009 à minuit</li>
<li>Chaque joueur peut envoyer plusieurs dessins.</li>      
<li>Tout dessin n'ayant aucun rapport avec les hamsters (ou les rongeurs) et le thème sera ignoré.</li>
<li>Le joueur peut nous envoyer son dessin avec 3 façons différentes : 
    <ol>
        <li><a href="options.php?mode=m_profil" title="changer l'image bonus">Via l'image bonus dans le menu du profil</a></li>
        <li>En envoyant un mail avec le dessin à <a href="mailto:concours@hamsteracademy.fr?subject=Concours">concours@hamsteracademy.fr</a></li>                
        <li>En envoyant le dessin à l'adresse suivante : <br/><strong>
        &nbsp;&nbsp;Ludique Production<br/>
        &nbsp;&nbsp;Concours de Hamster Academy<br/>
        &nbsp;&nbsp;9 rue des Innocents<br/>
        &nbsp;&nbsp;75001 Paris</strong><br/>
        (merci de nous prévenir si vous envoyez un courrier par la poste)<br/>
        Les dessins reçus seront ensuite affichés dans le jeu.
        </li>
    </ol>
</li>
<li>Pas besoin de s'inscrire</li>
<li>Les résultats seront donnés le 15 novembre</li>
<li>Récompenses : 1000 pièces pour le vainqueur avec son dessin en page d'accueil, 500 pour le second, 300 pour le 3ème. Et 100 pièces pour tous les autres participants.</li>
</ul>        
-->
<?php

//if ($userdata['inscritConcours'] == 0){
//    
    // pour le concours de la plus belle cage
//    if ($day >= 2 && $day <= 15 || ($day == 1 && $heure > 2)) // on attend 2 heures du mat' pour être que le script de reinit s'est bien déroulé
//        echo "<div><table><tr valign=baseline><td valign=baseline><strong>Je m'inscris</strong> (20 ".IMG_PIECE.") :&nbsp;&nbsp;</td><td valign=baseline><form action=\"jeu.php\" method=get><input type=hidden name=mode value=m_concours><input type=hidden name=action value=inscrireConcours><input type=submit name=inscriptionConcours value=\"S'inscire au concours !\"></form></td></tr></table></div>";
//    else if ($day >= 16 && $day <= 21)
//        echo "<div><table><tr valign=baseline><td valign=baseline><strong>Fin des inscriptions</strong>... en attendant le prochain concours, tu peux voter pour la plus belle cage (dès demain), chaque votant aura un petit cadeau.</td></tr></table></div>";
//    else
//        echo "<div>Fin du concours, rendez-vous le mois prochain pour un nouveau concours !</div>";
    // pour les autres concours
    //echo "<div>Fin des inscriptions</div>";
//}
//else {
//    if ($day >= 1 && $day <= 15) {
//        echo "<div><strong>Tu es inscrit au concours !</strong> Tu as jusqu'au 15 ".$mois_txt[$month]." pour décorer ta cage !";
//        if ($userdata['inscritConcours'] == 1 && $nbCages > 1) {
//            echo "<div><br/><b>Tu as plusieurs cages : </b><u>choisis ta cage pour le concours</u> (clique sur son nom) (attention, tu ne pourras pas changer) : ";
//            for($cage=0;$cage<$nbCages;$cage++) {
//                echo "<br/><a href=\"jeu.php?mode=m_concours&action=inscrireCageConcours&cage_id=".$lst_cages[$cage]['cage_id']."\" title=\"Choisir cette cage\">".$lst_cages[$cage]['nom']."</a>";
//            }
//        }
//        if ($userdata['inscritConcours'] > 1 && $nbCages > 1) {
//            echo "<br/>&nbsp;<br/>La cage </i> ".$lst_cages[cageCorrespondante($userdata['inscritConcours'])]['nom']."</i> a été sélectionnée pour le concours !";
//        }
//        echo "</div>";
//    }
//    else
//        echo "<div><strong>Tu es inscrit au concours !</strong> Les inscriptions sont closes et les votes vont commencer (demain)! Bonne chance !</div>";        
    //require "concours/concoursSlogan.php";
//}

    // on n'affiche pas la liste des précédents concours si le joueur vote
    if (!isset($_GET['vote'])) {
?>

<br/>&nbsp;<br/>
<span class="titre1">Les précédents concours </span>
<br/>&nbsp;<br/>

<strong>Décembre 2010</strong> : concours de la meilleure progression. Vainqueur : <b>cathlyna</b> ! <br/>&nbsp;<br/>
<img src="images/concours/diplome_dec_2010.png" alt="Diplome du concours de la meilleure progression" align=absmiddle>
<br/>&nbsp;<br/>

<strong>Novembre 2010</strong> : concours du hamster le plus fort. Vainqueur : <b>Squelettes</b> ! <br/>&nbsp;<br/>
<img src="images/concours/diplome_nov_2010.png" alt="Diplome du concours du hamster le plus fort" align=absmiddle>
<br/>&nbsp;<br/>

<strong>Janvier 2009</strong> : concours du plus beau dessin ou histoire dessinée. Vainqueur : <b>mylifeincartoonmotion</b> ! <br/>&nbsp;<br/>
<img src="images/diplome_jan_2009.gif" alt="Diplome du concours du plus beau dessin" align=absmiddle>
<?php 
    if (! isset($_GET['voirConcoursJan2009']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursJan2009=1\">Voir tous les résultats du concours</a>";
    else {
        echo "<br/>";
        require "concours/concours_dessin_janvier_2009.txt";
    }
?>
<br/>&nbsp;<br/>

<strong>Décembre 2008</strong> : concours de la plus belle cage. Vainqueur : <b>superleo</b> ! <br/>&nbsp;<br/>
<img src="images/diplome_dec_2008.gif" alt="Diplome du concours de la plus belle cage" align=absmiddle>
<?php 
    if (! isset($_GET['voirConcoursDec2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursDec2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_cage_dec_2008.txt";
?>
<br/>&nbsp;<br/>
<strong>Sept 2008</strong> : concours du hamster au meilleur moral. Vainqueur : florence578<br/>
<img src="images/diplome_sept_2008.gif" alt="Diplome du concours du hamster au meilleur moral" align=absmiddle>
<?php 
    if (! isset($_GET['voirConcoursSept2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursSept2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_moral_sept_2008.txt";
?>
<br/>&nbsp;<br/>
<strong>Aout 2008</strong> : concours de la plus belle cage. Vainqueur : louliou<br/>
<img src="images/diplome_aout_2008.gif" alt="Diplome du concours de la plus belle cage" align=absmiddle> 
<?php 
    if (! isset($_GET['voirConcoursAout2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursAout2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_cage_aout_2008.txt";
?>
<br/>&nbsp;<br/>

<strong>Juillet 2008</strong> : concours du meilleur slogan débile et/ou fun. Vainqueurs : cecileuh10, lulucoeur83, fashion collection, Samounet, kim2190, ely03, XD et minane<br/>

<?php 
    if (! isset($_GET['voirConcoursJuillet2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursJuillet2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_slogans_juillet_2008.txt";
?>
<br/>&nbsp;<br/>

<strong>Juin 2008</strong> : concours du plus bel hamster. Vainqueur : louliou<br/>
<img src="images/diplome_juin_2008.gif" alt="Diplome du concours du plus hamster le plus beau" align=absmiddle> 
<?php 
    if (! isset($_GET['voirConcoursJuin2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursJuin2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_beaute_juin_2008.txt";
?>
<br/>&nbsp;<br/>

<strong>Mai 2008</strong> : concours du hamster le plus musclé. Vainqueur : emilie94700<br/>
<img src="images/diplome_mai_2008.gif" alt="Diplome du concours du plus hamster le plus musclé" align=absmiddle> 
<?php 
    if (! isset($_GET['voirConcoursMai2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursMai2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_force_mai_2008.txt";
?>
<br/>&nbsp;<br/>

<strong>Mars 2008</strong> : meilleur groupe à l'Academy. Vainqueurs : Dude et Lis<br/>
<img src="images/diplome_mars_2008.gif" alt="Diplome du concours du plus meilleur groupe" align=absmiddle> 
<br/>&nbsp;<br/>

<strong>Février 2008</strong> : meilleur dessin de hamsters. Vainqueur : Coolmam !<br/>
<img src="images/diplome_fev_2008.gif" alt="Diplome du concours du plus beau dessin" align=absmiddle> 
<?php 
    if (! isset($_GET['voirConcoursFev2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursFev2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_dessin_fev_2008.txt";
?>
<br/>&nbsp;<br/>
<strong>Janvier 2008</strong> : meilleur poème ou meilleure chanson sur les hamsters. Vainqueurs : Lumina et Malou !<br/>
<img src="images/diplome_jan_2008_a.gif" alt="Diplome du concours du plus beau poème" align=absmiddle> <img src="images/diplome_jan_2008_b.gif" alt="Diplome du concours de la plus belle chanson" align=absmiddle>
<?php 
    if (! isset($_GET['voirConcoursJan2008']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursJan2008=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_poemes_jan_2008.txt";
?>
<br/>&nbsp;<br/>
<strong>Décembre 2007</strong> : la plus belle cage. Vainqueur : Loop1979 !<br/>
<img src="images/diplome_dec_2007.gif" alt="Diplome du concours de la plus belle cage" align=absmiddle>
<?php 
    if (! isset($_GET['voirConcoursDec2007']))
        echo "<a href=\"jeu.php?mode=m_concours&voirConcoursDec2007=1\">Voir tous les résultats du concours</a>";
    else 
        require "concours/concours_cage_dec_2007.txt";
?>

<?php
    } // affichage ou non de la liste des précédents concours si le joueur vote
?>

<?php

    }   // fin de la langue française
    echo afficherObjet("oeufs",2);
?>

<?php
    $dbHT->sql_close();
?>

<?php

} // fin de la version française

?>
   </div>
   </div>
    
    <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
    </div> 

  </div>
