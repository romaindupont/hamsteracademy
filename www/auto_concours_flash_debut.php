<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";

error_reporting( E_ALL );

// on donne un nouvel hamster � tous les inscrits

$query = "SELECT * FROM lst_joueurs_concours";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining info', '', __LINE__, __FILE__, $query);
}
while($row=$dbHT->sql_fetchrow($result)) {
    
    if ($row['hamster_id'] == 0) {
    
        // r�cup�ration d'une cage
        $query2 = "SELECT cage_id FROM cage WHERE joueur_id=".$row['joueur_id']." LIMIT 1";
        if ( !($result2 = $dbHT->sql_query($query2)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query2);
        }
        $nbCages = $dbHT->sql_numrows($result2) ;
        if ($nbCages > 0) {
            $row2=$dbHT->sql_fetchrow($result2);
            
            ajouterHamsterDansBDD("HamsterFlash de ".$row['pseudo_joueur'],rand(0,1),0,1,rand(0,10),$row2['cage_id'],$row['joueur_id'],3600*24*21);
        }
        $dbHT->sql_freeresult($result2);
    }
}
$dbHT->sql_freeresult($result);

?>