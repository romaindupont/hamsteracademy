<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

if ($paiement == PAIEMENT_NUL)
    return;

if ($paiement == PAIEMENT_1000_PIECES) {
    
    crediterPieces($nbPiecesParAchatBanqueCB,false); // on crédite (mais ce n'est pas un salaire => false)
    ajouterStats($userdata['joueur_id'],"paiement_CB5",1,$dateActuelle);
    
    echo "<div align=\"center\">";
    echo "<img src=\"images/pieces_bonus.gif\" alt=\"\" /><br/>&nbsp;<br/>";
    echo T_("Félicitations, tu viens de récupérer ").$nbPiecesParAchatBanqueCB." " .IMG_PIECE." !<br/>&nbsp;<br/>".T_("Tu possèdes maintenant ").$userdata['nb_pieces']." ".IMG_PIECE." !" ;
}
else if ($paiement == PAIEMENT_3000_PIECES) {
    
    crediterPieces($nbPiecesParAchatBanqueCB2,false); // on crédite (mais ce n'est pas un salaire => false)
    ajouterStats($userdata['joueur_id'],"paiement_CB10",1,$dateActuelle);
            
    echo "<div align=\"center\">";
    echo "<img src=\"images/pieces_bonus.gif\" alt=\"\" /><br/>&nbsp;<br/>";

    echo T_("Félicitations, tu viens de récupérer ").$nbPiecesParAchatBanqueCB2." " .IMG_PIECE." !<br/>&nbsp;<br/>".T_("Tu possèdes maintenant ").$userdata['nb_pieces']." ".IMG_PIECE." !" ;
}
else if ($paiement == PAIEMENT_10000_PIECES) {
    
    crediterPieces($nbPiecesParAchatBanqueCB3,false); // on crédite (mais ce n'est pas un salaire => false)
    ajouterStats($userdata['joueur_id'],"paiement_CB30",1,$dateActuelle);    
    
    echo "<div align=\"center\">";
    echo "<img src=\"images/pieces_bonus.gif\" alt=\"\" /><br/>&nbsp;<br/>";

    echo T_("Félicitations, tu viens de récupérer ").$nbPiecesParAchatBanqueCB3." " .IMG_PIECE." !<br/>&nbsp;<br/>".T_("Tu possèdes maintenant ").$userdata['nb_pieces']." ".IMG_PIECE." !" ;
}
else if ($paiement == PAIEMENT_NIVEAU) {
    
    ajouterStats($userdata['joueur_id'],"Paiement pr mission, niveau ".$userdata['niveau'],3,$dateActuelle);
    ajouterStats($userdata['joueur_id'],"paiement_mission",1,$dateActuelle);
    
    if ($userdata['niveau'] < $nbNiveaux-1) {
    
        $nbreObjectifs = sizeof ($infosNiveaux[$userdata['niveau']][NIVEAU_OBJECTIFS]);
        $tousobjectisfinis = $maskObjectifs[$nbreObjectifs] ;
        
        $query = "UPDATE joueurs
            SET niveau_objectif = ".$tousobjectisfinis." WHERE joueur_id=".$userdata['joueur_id']; 
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error updating joueur _ niveau', '', __LINE__, __FILE__, $query);
        }
    }
    
    echo "<div align=\"center\">";
    echo "<img src=\"images/hamster_inscription.gif\" alt=\"\" />".T_("Félicitations, tu viens de passer au niveau suivant !")."<br/>&nbsp;<br/>" ;
}
else if ($paiement == PAIEMENT_VIP) {
    
    ajouterStats($userdata['joueur_id'],"Paiement pr vip, niveau ".$userdata['niveau'],4,$dateActuelle);
    ajouterStats($userdata['joueur_id'],"paiement_vip",1,$dateActuelle);
    
    $query = "UPDATE joueurs
        SET vip = 1 WHERE joueur_id=".$userdata['joueur_id']; 
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating vip', '', __LINE__, __FILE__, $query);
    }
    
    echo "<div align=\"center\">";
    echo "<img src=\"images/hamster_inscription.gif\" alt=\"VIP\" />".str_replace("#1",$mois_txt[$month],T_("Félicitations, tu es maintenant VIP ! Tu vas pouvoir profiter pour tout le mois de #1 de plein d'avantages !"))."<br/>&nbsp;<br/>" ;
}
else if ($paiement == PAIEMENT_GROSSESSE) {
    
    $query = "UPDATE hamster 
        SET sexe = 2 WHERE hamster_id=".$pageParam; // on met "2" comme date de début de grossesse pour être certain que la grossesse est finie
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster _ grossesse', '', __LINE__, __FILE__, $query);
    }
    ajouterStats($userdata['joueur_id'],"paiement_grossesse",1,$dateActuelle);
    
    echo "<div align=\"center\"><img src=\"images/reproduction.gif\" alt=\"\" /><br/>&nbsp;<br/>";
    echo T_("Grâce à l'achat de la couveuse ultra-rapide, la grossesse est terminée. Les bébés sont prêts à naître !")."<br/>&nbsp;<br/>";
    echo "<a href=\"".$pagePrecUrl."\">".T_("Voir ton hamster et ses petits !")."</a></div>" ;
}
else if ($paiement == PAIEMENT_NID) {
    
    $query = "UPDATE hamster 
        SET construction_nid = 1 WHERE hamster_id=".$pageParam; // on met "1" comme date de début de construction pour être certain que le nid est fini
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster _ construction_nid', '', __LINE__, __FILE__, $query);
    }
    ajouterStats($userdata['joueur_id'],"paiement_nid",1,$dateActuelle);
    
    echo "<img src=\"images/nid.gif\" alt=\"\" /><br/>&nbsp;<br/>";
    echo "<div align=\"center\">";
    
    echo T_("Grâce à ton achat, la construction du nid est terminée").".<br/>&nbsp;<br/>";
    
    echo "<a href=\"".$pagePrecUrl."\">".T_("Voir le hamster et son nouveau nid !")."</a>" ;
}
else if ($paiement == PAIEMENT_CABANE) {
    
    $query = "UPDATE hamster 
        SET construction_cabane = 1 WHERE hamster_id=".$pageParam; // on met "1" comme date de début de construction pour être certain que la cabane est finie
    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating hamster _ construction_cabane', '', __LINE__, __FILE__, $query);
    }
    ajouterStats($userdata['joueur_id'],"paiement_cabane",1,$dateActuelle);
    
    echo "<img src=\"images/maison2.gif\" alt=\"\" /><br/>&nbsp;<br/>";
    echo "<div align=\"center\">";
    echo T_("Grâce à ton achat, la construction de la cabane est terminée").".<br/>&nbsp;<br/>";
    echo "<a href=\"".$pagePrecUrl."\">".T_("Voir le hamster et sa nouvelle cabane !")."</a>" ;
}
else if ($paiement == PAIEMENT_300_PIECES) {

    crediterPieces($nbPiecesParAchatBanque,false); // on crédite (mais ce n'est pas un salaire => false)    
    ajouterStats($userdata['joueur_id'],"paiement_tel",1,$dateActuelle);

    echo "<div align=\"center\">";
    echo "<img src=\"images/pieces_bonus.gif\" alt=\"\" /><br/>&nbsp;<br/>";
    echo T_("Félicitations, tu viens de récupérer ").$nbPiecesParAchatBanque." " .IMG_PIECE." !<br/>&nbsp;<br/>".T_("Tu possèdes maintenant ").$userdata['nb_pieces']." ".IMG_PIECE." !" ;
}
else
    return;
?>