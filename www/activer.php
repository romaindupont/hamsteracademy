<?php 
define('IN_HT', true);
include "common.php"; 
include "gestion.php"; 

$errMsg = "" ;
$msg = "" ;

// on récupère le joueur_id et l'email associé
if (isset($_GET['joueur_id'])) {
    $joueur_id = intval($_GET['joueur_id']);
    
    // on récupère l'adresse email associée
    $query = "SELECT joueur_id, email FROM joueurs WHERE joueur_id = ".$joueur_id;
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    $nbJoueur = $dbHT->sql_numrows($result) ;
    if ($nbJoueur == 1) {
        $row=$dbHT->sql_fetchrow($result);
        
        // on récupère la clé
        if (isset($_GET['cle'])){
            $cle = mysql_real_escape_string($_GET['cle']) ;
            
            if ($cle == calculCodeActivation($joueur_id, $row['email'])) {
                
                // la clé est valide, on active le compte
                $query2 = "UPDATE joueurs SET email_active = 1 WHERE joueur_id=".$joueur_id;
                if ( !($dbHT->sql_query($query2)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query2);
                }
                
                $msg = str_replace("#1",$row['email'],T_("<strong>C'est bon </strong> !<br>&nbsp;<br/> Ton adresse email #1 a correctement été validée ! Bon jeu !"))."<br>&nbsp;<br>&nbsp;<br>";
                $msg .= "=> <a href=\"".$base_site_url."jeu.php\">".T_("Revenir dans le jeu")."</a> <=";
            }
            else {
                $errMsg = T_("La clé d'activation est invalide. Vérifie le lien ou contacte l'Hamster Academy.");
            }
        }
    }
    else {
        $errMsg = T_("Il n'a pas de joueur avec cet id. Vérifie le lien ou contacte l'Hamster Academy.");
    }
}

$liste_styles = "style.css,styleEleveur.css";

// paramètres d'entete
$smarty->assign('pagetitle', "Hamster Academy - ".T_("Activation de l'adresse email"));
$smarty->assign('description', "Hamster Academy, ".T_("Activation du mail"));
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>
    
<div align="center"> 
  <h1>Hamster Academy</h1>
  <p>&nbsp;</p>
  <p><img src="images/hamster_russe.jpg" width="250" ></p>
  <p>&nbsp;</p>
  
  <?php 
  if ($errMsg != "") {
      echo "<span class=optionsValeurChampsErreur>".$errMsg."</span>";
  }
  else {
      echo "<span>".$msg."</span>";
  }
  
  ?>
  <p>&nbsp;</p>
  <p><?php echo T_("Et pour tout contact ou problème") ; ?> : <a href="mailto:<?php echo $email_contact;?>"><?php echo $email_contact;?></a></p>
</div>

<?php
require "footer.php" ; 
?>