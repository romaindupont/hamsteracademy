====== Help of the Breeder Universe ======


===== Overview ===== 

This is the World where everyone starts off: it is in this world that you have to raise your hamster and build its cage. To enable you to evolve in the game, you get a basic salary from the start(7 coins/day) which allows you to make everyday purchases. You are then free to sign up for training which affords you a career, allowing you to earn more money so that you can purchase everything you need for your hamster.

===== Getting Started ===== 

==== What currency is used? ==== 

The local currency is the gold coin. Each breeder earns at least 7 coins per day. The game allows you to sign up for training, enabling you to earn more coins. At any time, you can go to town and choose a new career by purchasing training. Be careful, take note that if a breeder is absent for more than 3 days, his salary is no longer paid out. You will therefore have to login at least once every 3 days to earn your salary!

{{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce gold}} {{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce gold}} 
{{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce gold}} 

==== How do I water the hamster? ====

To ensure your hamster gets enough water, you just need to put a feeding bottle in its cage. This is vital, otherwise your hamster can get sick very quickly, and its health can decline rapidly. While buying a cage, you need to buy a feeding bottle along to be put in the cage. After this, you only need to change the water regularly.

==== How do I feed the hamster? ====

To feed the hamster, you must have a bowl in its cage with sufficient food supply. Sunflower seeds are its staple food. You need to ensure that the hamster is never hungry and that its bowl is never completely empty, otherwise its health and mood start decreasing. Feeding and watering the hamster are the two most important tasks you must never forget! 

{{Http://www.hamsteracademy.fr/images/ecuelle_reduc_sb.gif | Bowl empty}} {{http://www.hamsteracademy.fr/images/ecuelle_pleine_reduc.gif | Bowl Full}}

===== The breeder =====

==== How to buy items? ====

You have the possibility to buy items for both the cage and the hamster. To do so, you have to go into town and then in the shop. Afterwards you can walk through the various shelves and choose your items for the cage, the hamster and the Academy. 
Only the hamsters who reached the Level 2 can access the shop!

Some items such as musical instruments are available from a certain level in the game only 


{{http://www.hamsteracademy.fr/images/banc_reduc.gif?50 * {{50 |Small bench}} 
{{http://www.hamsteracademy.fr/images/balance.gif?50 * {{50 |Balance}} 
{{http://www.hamsteracademy.fr/images/miroir_noir.gif?25 * {{25 |Black Mirror}} 
{{http://www.hamsteracademy.fr/images/balancoire.gif?50 * {{50 |Swing}} 
{{http://www.hamsteracademy.fr/images/fontaine2.gif?50 * {{50 |Fontaine}} 
{{http://www.hamsteracademy.fr/images/cachette.gif?25 * 25 | Cache timber}}
{{http://www.hamsteracademy.fr/images/roue3.gif?50 * 50 | Wheel Wood}}
{{http://www.hamsteracademy.fr/images/piano.gif?50 * {{50 |Piano}} 
{{http://www.hamsteracademy.fr/images/guitare_electrique.gif?50 * {{50 |Electric Guitar}} 
{{http://www.hamsteracademy.fr/images/cabane.gif?50 * {{50 |Shack}} 
{{http://www.hamsteracademy.fr/images/boule_rose_reduc.gif?50 * {{50 |Pink Ball}} 


==== How to make the hamster build its nest? ====

One of the missions proposed to the player is building its nest. The hamster will need 3 items to do so:
  * Straw;
  * Cotton;
  * Twigs.
All these items are available in the shop. As soon as you give them to your hamster, he starts building his nest! The time needed for the construction depends on his ability to Build (one of the specific choices to make when registering). The time needed may vary from 24 hours to 1 week. To accelerate the construction of the nest and to be able to move more quickly to the next level, you also have the possibility to purchase a construction training or to buy a code Allopass ** ** , in this last case the construction becomes instantaneous.

==== How to make the hamster build a cabin? ====

The construction of the cabin works like the construction of the nest. You need 2 things:

    * Logs of wood;
    * A building plan.

Once these items purchased and given to your hamster, the construction of the cabin begins immediately! The construction time also depends on the building capacity of the hamster.

==== How to take my hamster for a walk? ====

To take the hamster for a walk, you have to purchase a special leash for hamsters from the shop (only available for breeders at level 4). Once the purchase of the permit is done, you can take your hamster for a walk once a day at the most. The walk in the woods is especially appreciated by the hamster who loves this type of entertainment (he finds his natural environment). In addition, your hamster may find a tassel on the way (which increases the mood). However you must be careful as your hamster could catch a disease (poisonous mushrooms and microbes are common in the forest).

==== How to change job? ====

If you do not earn enough coins, you can change job. This allows you to increase your daily income. Only the breeders who reached the level 3 can do so. You have to follow training to learn the new profession. The more expensive the chosen training is, the more money will earn. It is interesting to pay more at the start to be sure to make lots of money afterwards. To change job, you must go to town then click on the tab [[http://www.hamsteracademy.fr/jeu.php?mode=m_metier|Mon trade]].

{{http://www.hamsteracademy.fr/images/pompier2.gif?50 * {{50 |Firefighter}} 
{{http://www.hamsteracademy.fr/images/hamburger.gif?50 * 50 | Seller hamburgers}}
{{http://www.hamsteracademy.fr/images/sauveteur.gif?50 * 50 | Rescue at Sea}}
{{http://www.hamsteracademy.fr/images/joueur_tennis.gif?50 * 50 | Tennis players}}
{{http://www.hamsteracademy.fr/images/Veterinaire_reduc.gif?50 * 50 | Vet Zoo}}
{{http://www.hamsteracademy.fr/images/musician3.gif?50 * {{50 |Musician}} 
{{http://www.hamsteracademy.fr/images/rallye3.gif?50 * 50 | Rally driver}}
{{http://www.hamsteracademy.fr/images/detective.gif?50 * {{50 |private detective}} 
{{http://www.hamsteracademy.fr/images/police.gif?50 * {{50 |Thriller}} 
{{http://www.hamsteracademy.fr/images/microscope.gif?50 * {{50 |Bonati}} 
{{http://www.hamsteracademy.fr/images/crocodile.gif?50 * 50 | breeder crocodiles}}

==== How are the points of the breeder calculated? ====

They are calculated every 5 minutes and for each act. Then the information is updated.

The rules to calculate the score are: 
   * The more points the hamsters have, the more points the breeder has
   * The more cages the player has, the more points he gets. They must be very clean and as large as possible (while avoiding empty space)
   * The more items a player owns, the more points he gets
   * If the player has got an avatar, he gets more points (+10%)

But if one or more of his hamsters are sick, he loses points (-25% for each sick hamster).

[[# comment_sont_calcules_les_points_du_hamster | The next section explains how the scores for the hamsters are calculated.]]

===== My hamster =====

==== How to improve the health of the hamster? ====

The health of the hamster evolves depending on many parameters. The three basic actions (feeding, watering and changing the litter) allow you to keep your hamster in a good health. If you would like your hamster in very good shape, there are many items in the shop to help you improve his health (for instance vitamins). You must know as well that a hamster who runs on the wheel or make other types of physical activities will be more easily in a better state of health. Some food items such as salads are very good for the hamster. And he loves it! Be careful to take good care of your hamster. If his health level drops close to 0, he will be sent to the veterinarian to receive intensive care. This prevents him from dying, but intensive care is very expensive if you want your hamster back.
If your hamster is in bad health, you can check the follwing things:

    * His bowl is not empty;
    * There is a feeding bottle in his cage;
    * Chips are clean;
    * Physical strength is sufficient.


==== How to improve the mood of the hamster? ====

If the mood of your hamster is low, this means he is not happy in his cage. The main elements affecting the mood of the hamster are :

    * An empty bowl;
    * Poor health;
    * Too many hamsters in the same cage (hamsters are solitary animals);
    * The lack of feeding bottle;
    * The lack of care (caresses, care creams ...).

To improve the mood of your hamster, you can think of different entertainments for him (outdoor trainings, rugby or football games, ...).

==== How to improve the beauty of the hamster? ====

One of the main parameters for the general condition of the hamster is its appearance. This is especially important in the world of the Academy. The more beautiful your hamster is, the more popular he is! It is therefore essential to carress the hamster, to brush him and to apply a care cream on his fur. The cleanliness of the cage affects his beauty, a hamster who lives in a dirty cage can not be clean. The items offered in Beauty City can help you improve the beauty of your hamster.

==== How to make your hamster a sporty type? ====

It is very important for the health and the mood of your hamster that he does some sport. His fitness depends on it! To become stronger and in better shape, the hamster should do some sport. Here are several options:

    * Run in the wheel in his cage;
    * A bodybuilding training;
    * To buy accessories (bike, swing, ball, ...). 

But be carefull, your hamster should not do too much sport  and several hours are required in between two musculation training!

NB: it takes several days for your hamster to get muscles with the wheel whereas the results are immediate with the dumbbells!

==== My hamster is too skiny or too fat, what should I do? ====

The ideal weight for hamsters is 120g. If his weight is too low (less than 105g) or too high (above 135g), his health, fitness and beauty decrease. If he is too skiny, you can feed him more often and let him play sports (to gain muscles) more regularly. If he is too fat, you can check if there is a wheel in his cage and make sure he gets enough physical exercise. Give him less food and buy a maximum of sport items.

==== My hamster is way too big, I can not bring his weight down ... ====

There is a slightely twisted way for the hamster to loose weight in once, without using slimming pills. But we'll let you guess how to do that! Just a clue: you must make him run in the wheel, but if possible, not with any wheelbut a fiddled wheel... (only possible for breeders at Level 3)

==== My hamster does not want to run in the wheel ... ====

As long as you bought a wheel for the hamster and you put it in his cage, he is bound. The effects on muscular strength and its overall shape are not instantaneous (unlike dumbbells) and it takes several days to see the results.

==== How to have babies? ====

Like any pet, the hamster may have babies ** as soon as she is 1 month old **. Only femal hamsters can have babies. We must then choose them a male hamster within the Hamster Academy. Pregnancy lasts about 1 week and the amount of babies vary between 1 and 3, both males and females. Their genetic characteristics depend on their parents ones. Warning! Once the babies are born, you must take care of them! It is out of the question to let them die of hunger, thirst ... It is strongly advised to buy them a new cage so they do not spend their time fighting.


==== How are points of the hamster calculated ? ====

The points are equal to the sum of health, beauty, strength, mood, experience, popularity, dance, song, humour and music.

They also increase:
    * If there is a music instrument
    * If married (+10%)
    * If he belongs to a band (he gets the points of the band)

They are reduced:
    * When ill (division by 2)

[[www.hamsteracademy.fr / aide_academy # comment_sont_calcules_les_points_de_chaque_groupe | The calculation of the points of each band is explained here.]]

===== My cage =====

==== How to move an intem inside the cage or from one cage to another? ====

The construction of the cage is an important step in the game. Each breeder is free to presonalize it as much as he wants. You can add floors, enlarge the surface, change the color and add all the items you want. To move an item inside the cage from one floor to another, simply click on the item to move (without releasing the mouse) and move it to the location of the cage where you want it to appear . You can also use the arrows that appear in the dial right into the workshop.
In case of a problem, you can always go to the forum or leave a message to [[mailto: contact@hamsteracademy.fr | contact@hamsteracademy.fr]].


If you want to remove an item from the cage, you must select it in the inventory and move it to the tote tray.

If you want to move an item from one cage to another, you must click on the item in the inventory and select the cage in which you want to move it.

==== My hamster has chewed the bars of the cage ... how to avoid it? ====

All hamsters have the habit of biting the bars of their cage. This allows them to use their teeth that grow constantly. To prevent your hamster from chewing the bars, you can buy from the shop a stone to gnaw. You must put one stone per cage to ensure that the bars are no longer biting (gnawing stones are available for the farmers at level 4).

==== I'm going on holidays and I can not take care of my hamsters ... ====

If you do not have time to take care of your hamster for a few days or weeks, you can leave them at The Refuge. The price is 10 coins for all hamsters. They will be fed and housed as long as you like and you will get them back in the same state of health and mood as you left them. While your hamster is at the Refuge, you do not earn any money.

==== How do I clean my cage? ====

Each day the hamsters slightly dirty the cage. Therefore it must be cleaned regularly (at least once a week for a hamster cage of one hamster)! You can find chips of wood in the shop. You just need a litre of chips to clean the cage once. 

Warning: a dirty cage conducts to diseases and lower health status for the hamster. The more hamsters you have in the same cage, the faster it gets dirty , and the more vigilant you must be! 


{{http://www.hamsteracademy.fr/images/odeurs.gif}}

==== The design of the cages is weird or does not work? ====

It may happen that it does not work. There are 2 solutions:

     - Refresh the page upside down by pressing the F5 key;
     - If still not working, the browser (Internet Explorer, Firefox, Safari or Opera) may be causing the problem. The ideal browser for the game is Firefox. If you still get the same problems, do not hesitate to contact Hamster Academy ([[mailto: contact@hamsteracademy.fr | contact@hamsteracademy.fr]])!

===== Other =====

==== Not enough coins in your pocket? What to do? ====

To make money there are several options in the game. First of all, you can wait for your coming salary (paid once a day). Here are some other possibilities : 
  - Play quiz (questionnaire about hamsters, only one test a day);
  - Sell items (you must go to the inventory and choose the item for sale);
  - Ask the hamster to build a nest (or a hut) you can sell afterwards;
  - Refer a friend, in other words, invite him to join the Hamster Academy : This gives you 20 coins per registered friend. To sponsor a friend, please click on [[http://www.hamsteracademy.fr/parrainage.php|ce link]] and follow the signs;
  - Purchase some coins at the BANK, you will receive between 300 and 1000 coins ([[http://www.hamsteracademy.fr/jeu.php?mode=m_banque|Go to the Bank !]]).

==== How to play rugby and football? ====

Playing rugby or football is an important physical activity that allows the breeder to improve the strength and mood of the hamster. Practicing a team sport is always good for the mood! To make your hamster play with another hamster, you need 2 things : 

    * A ball: you can find some in the shop;
    * An outdoor game: the outdoor game is not free(organization of the game, rental of the land, ...), it must be purchased at The Store. An outdoor game is valid only once. If you would like to improve the health, mood and strength of the hamster, you are strongly advise to bring your hamster for an outdoor game regularly ... To be renewed with no restriction, while there is money in the wallet!

** NB: The outdoor rugby games are available on Saturdays only, the outdoor football games are available on Wednesdays and Saturdays **.

==== What is the agenda of the week? ====

Available activities are spread out throughout the week. The possible outdoor games are :

    * Monday: Bodybuilding
    * Tuesday: Walk in the woods - bodybuilding
    * Wednesday: Football - bodybuilding - video game console
    * Thursday: Release the ball - bodybuilding
    * Friday: walk in the woods - bodybuilding
    * Saturday: Rugby - bodybuilding
    * Sunday: Football - video game console - bodybuilding

^ Monday ^ Tuesday ^ Wednesday ^ Thursday ^ Friday ^ Saturday ^  Sunday ^
| {{http://www.hamsteracademy.fr/images/boule_noire_reduc.gif|Sortie in the ball}}{{http://www.hamsteracademy.fr/images/halteres_reduc.gif}} | {{http://www.hamsteracademy.fr/images/laisse.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif }}|{{ http://www.hamsteracademy.fr/images/ballon_foot.gif}}  {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}  {{http://www.hamsteracademy.fr/images/console.gif }}|{{ http://www.hamsteracademy.fr/images/boule_noire_reduc.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif }}|{{ http://www.hamsteracademy.fr/images / laisse.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif }}|{{ http://www.hamsteracademy.fr/images/ballon_rugby.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif }}|{{ http://www.hamsteracademy.fr/images/ballon_foot.gif}} {{http://www.hamsteracademy.fr/images/console.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}} |

==== Is it possible to let one or more hamsters go? ====

It is now possible to let a hamster go if you have no more time or not enough money to take care of him. From the home section, you can go to SPH to bring them a hamster. The SPH (Society for the Protection of Hamsters) take care of the hamsters whose breeders can not deal with anymore. Once you left them your hamster, ** you can not have him back; ** ** This choice is definite!** Giving the hamster to the SPH is not free, the price allows them to take care of him at the beginning.
The SPH is open to breeders at the level 3 and breeders owning at least 2 hamsters.

==== How to make friends? ====

The Forum, Chat and the game allows you to make friends. From the Home page, you can create a friends list, by clicking on {{http://www.hamsteracademy.fr/images/icone_outils.gif?20 * 20|Tools}} then Add friends.

==== Trades ====

[[www.hamsteracademy.fr/métier|Description trades]] (coming soon)