<?php

/**

 * DokuWiki Incognitek Template

 *

 * @link   http://wwww.incognitek.com

 * @author Daniel Sperl <redge@incognitek.com>

 */



// must be run from within DokuWiki

if (!defined('DOKU_INC')) die(); 



define ('IN_HT',true);



?>



<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $conf['lang']?>"

 lang="<?php echo $conf['lang']?>" dir="<?php echo $lang['direction']?>">

<head>

  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

  <title>Help of Hamster Academy - 

    <?php echo strip_tags($conf['title'])." - ".htmlspecialchars($INFO['meta']['title']) ; ?>

  </title>

  <meta name="description" content="Aide de Hamster Academy : <?php echo htmlspecialchars(str_replace("\n",". ",str_replace("\n\n",". ",$INFO['meta']['description']['abstract'])));?>" />

  <link rel="shortcut icon" href="../images/favicon.ico" />

  <?php tpl_metaheaders()?>
  <link href="http://www.hamsteracademy.com/css/style.css,cornflex-common.css,styleDialog.css,styleEleveur.css" rel="stylesheet" type="text/css" />
  <script type="text/javascript" src="http://www.hamsteracademy.com/scripts/tooltip.js,gestionHamster.js,jquery.js,jquery-ui.min.js,jquery.cornflex.js,yellowbox.js,jquery-impromptu.js"></script>

  <?php //include('ie6_fixes.php'); ?>

</head>

<style content="text/css">
    .macaron {
        background: transparent url('http://www.hamsteracademy.com/images/macaron-en.png');
    }
</style>

<body>

  <?php html_msgarea()?>

  <!-- <div id="container">    -->

   <!-- <div id="header">            -->

      <div class="mainPage">

        <div class="topbar">
            <div class="topbartxt" style="background:transparent;" ><a href="http://www.hamsteracademy.com/aide/help" title="Help in french"><img src="http://www.hamsteracademy.fr/images/lang/francais.gif" alt="Francais" width="20" height="14" border="1" style="vertical-align:text-top;" /> Francais</a> &nbsp; <a href="http://www.hamsteracademy.com/help/help" title="Help of the game in English !"><img src="http://www.hamsteracademy.com/images/lang/english.gif" alt="English" width="20" height="14" border="1" style="vertical-align:text-top;" /> English</a></div>
        </div>
            <div id="tooltip"></div>

            <div class="logoMain">
                <div class="logoImage">     
                    <img src="http://www.hamsteracademy.com/images/logoHamsterAcademy.gif" alt="Hamster Academy" />
                </div>
                <div class="macaron"></div>
            </div>


        <div class="header">

          <ul>          
        <li class="menuPrincipal" ><a href="http://www.hamsteracademy.com/help/start">Help of Breeder Universe</a></li>
        <li class="menuPrincipal" ><a href="http://www.hamsteracademy.com/help/www.hamsteracademy.fr_aide_academy">Help of Academy Universe</a></li>
        <li class="menuPrincipal"  ><a href="http://www.hamsteracademy.com/help/www.hamsteracademy.fr_aide_generale">Miscellaneous</a></li>
        <li class="menuPrincipalDroite"><a href="http://www.hamsteracademy.com/jeu.php?mode=m_accueil&amp;univers=0"><img src="images/hamster_reduc.gif" style="height:16px; vertical-align:middle;" alt="" />Back to the game</a></li></div>       


<div class="mainPageUnderMenu"> 

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>

  <div class="hamBlocColonne_full">


    <div id="corpus">



      <?php if($conf['breadcrumbs']) {?>

      <div id="breadcrumbs"><?php tpl_breadcrumbs()?></div>

      <?php } elseif ($conf['youarehere']) {?>

      <div id="breadcrumbs"><?php 

          echo "&nbsp;";

          //tpl_youarehere() 

      ?></div>

      <?php }?> 

      <?php flush()?>



      <div class="dokuwiki">



        <!-- wikipage start -->

        <?php tpl_content(); ?>

        <!-- wikipage stop -->



        <div class="clearer">&nbsp;</div>



      </div>
      
      <div style="clear:both;">&nbsp;</div>

      </div>
      </div>

      <div class="hamBlocColonne-bottom">
         <div class="hamBlocColonne-bottom-left"></div>

         <div class="hamBlocColonne-bottom-right"></div>
         <div class="hamBlocColonne-bottom-center"></div>        
      </div> 
      
    </div>

      <div id="footer">

        <div id="footer_left">

          <?php tpl_button('edit')?>

        </div>

        <div id="footer_center">

          <?php tpl_button('subscription')?>

          <?php tpl_button('admin')?>

          <?php tpl_button('profile')?>

          <?php tpl_button('login')?>

        </div>        

   <?php

    $tgt = ($conf['target']['extern']) ? 'target="'.$conf['target']['extern'].'"' : '';

    ?>

      </div>
      
<!-- fin du mainPageUnderMenu -->
<div align="center" class="footer"  >

    <a href="http://www.hamsteracademy.com/partenaires.html">Partners</a> - <a href="http://www.hamsteracademy.com/charte.html" target="_blank">General user conditions</a> - <a href="http://www.hamsteracademy.com/charteForumTchat.html" target="_blank">Forum rules</a> - <a href="http://www.hamsteracademy.com/charteParents.html" target="_blank">Parental advisory</a> - <a href="http://www.hamsteracademy.com/help/doku.php?id=start">Help</a> - <a href="http://hamsteracademy.spreadshirt.net" target="_blank">The Official Shop</a> - <a href="mailto:contact@hamsteracademy.fr">Contact</a> - <a href="http://www.hamsteracademy.com/bonus.html">Bonus</a> - <a href="http://www.hamsteracademy.com/autresjeux.php">Play other games</a> 
    <br/>    &nbsp;<br/>

    &copy; Hamster Academy - All rights reserved</div>

</div> <!-- fin du mainPage -->



<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {

var pageTracker = _gat._getTracker("UA-10152005-1");

pageTracker._trackPageview();
} catch(err) {}</script>


</body></html>

