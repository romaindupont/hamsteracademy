<?php 

define('IN_HT', true);

include "common.php";

$base_site_url = "http://www.hamsteracademy.fr/";
$univers = UNIVERS_INDEX;
$pagetitle = "404 Not Found" ;
$description = "Erreur 404 ...";

$liste_styles = "style.css,styleEleveur.css";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', UNIVERS_ELEVEUR);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>

<div style="height:200px; border-top:1px; margin-top:1px">
<div align="center" style="margin-left:auto; margin-right:auto; height:165px; width:120px; float:top; position: relative; right: 160px; top:0px; " >	
	<img src="http://www.hamsteracademy.fr/images/hamster_logo.gif" alt="Logo Hamster Academy" />
</div>
<div style="margin-left:auto; margin-right:auto; border-bottom:0px ;width:300px; text-align:center; float:top; position: relative; top: -130px;">	
	<h1>Hamster Academy</h1>
	<h2>Univers <?php echo $lstUnivers['fr'][$univers] ; ?></h2>
</div>
</div>

<br/>&nbsp;<br/>

<h2 align="center"><?php echo T_("Ouh la la, je suis perdu ! Le lien n'est pas valide...");?></h2>

<br/>&nbsp;<br/>

<h4>Tu peux :</h4> 
<ul>
<li><a href="index.php"><?php echo T_("retourner dans le jeu et retrouver l'Univers des Hamsters !");?></a></li>
<li><a href="aide/"><?php echo T_("ou voir l'aide du jeu et tous ses bons conseils !");?></a></li>
</ul>

<br/>

<style type="text/css">
  #goog-wm { }
  #goog-wm h3.closest-match { }
  #goog-wm h3.closest-match a { }
  #goog-wm h3.other-things { }
  #goog-wm ul li { }
  #goog-wm li.search-goog { display: block; }
</style>
<script type="text/javascript">
  var GOOG_FIXURL_LANG = 'fr';
  var GOOG_FIXURL_SITE = 'http://www.hamsteracademy.fr/';
</script>
<script type="text/javascript" 
    src="http://linkhelp.clients.google.com/tbproxy/lh/wm/fixurl.js"></script>

<p>&nbsp;</p>

<br/>
<?php require "footer.php"; ?>