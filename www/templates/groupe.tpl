{if isset($editerDescription)}
{literal}
<!-- TinyMCE -->
    <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            plugins : "safari,advhr,advimage,advlink,emotions,iespell,media,contextmenu",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,blockquote,|,undo,redo,|,link,unlink,image,|,forecolor,backcolor,|,hr,emotions",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js"
        });
    </script>
<!-- /TinyMCE -->
{/literal}
{/if}
<div class="hamColonneGauche" style="width:200px;">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Infos{/t}</span></div>
        </div>       
      
        <div class="cadreArrondi-contenu">
        
        
        {if $leader}
            {$pseudoHamster} {t}est le leader{/t} ! 
        {else}
            {$pseudoHamster} {t}est membre{/t}
        {/if} 

        <table border="0">
            <tr>
                <td><span>{t}Points{/t}</span> : </td><td>{$noteGroupe} pts</td>
            </tr>
            <tr>
                <td><span>{t}Expérience{/t}</span> : </td><td>{$nouvelleExperience}</td>
            </tr>
            <tr>
                <td><span>{t}Classement{/t}</span> : </td><td>{if $classement > 1}
                        {$classement} <span style="font-size:8pt;vertical-align:super;">ème</span>
                    {else} 
                        1er
                    {/if}</td>
            </tr>
            <tr>
                <td><span>{t}Leader{/t}</span> : </td><td>{$nomLeader}</td>
            </tr>
        </table>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Niveau{/t} {$niveau}</span></div>
        </div>       
      
        <div class="cadreArrondi-contenu">
        ({$niveauDescription})
                <br/><i>{$niveauDescriptionPlus}</i>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Bonus{/t}</span></div>        
        </div>
          
        <div class="cadreArrondi-contenu">
            {$combo}<br/>
            {$bonusGroupeTxt}
            <div style="clear:both;">&nbsp;</div>    
        </div>
            
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
</div>

<div class="hamColonneCentrale" style="width:450px;">

    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>
    
    {if $msg != ""}
        <div class="hamBlocColonne">{$msg}</div>
    {/if}
    
    <div class="hamBlocColonne">
      <div class="hamTitreBloc">
         <b>{$nomGroupe}</b>
        </div> 
        
        <div class="hamImg">
            {$imageGroupe}<br/>
            {if isset($editerDescription) }
                <form method="POST" action="jeu.php" ENCTYPE="multipart/form-data">
                <br><table>
                <tr><td>{t}Changer le logo{/t}</td>
                <input type="hidden" value="m_groupe" name="mode" />
                <input type="hidden" value="{$hamsterId}" name="hamster_id" />
                <input type="hidden" value="modifImageGroupe" name="actionAcademy" />
                <input type="hidden" value="1" name="modifierDescription" />
                <td><input type="file" name="fichierImage" size="20" /></td>
                <td><input type="submit" value="{t}Envoyer{/t}" /></td></tr></table></form>
                <span style="font-size:8pt;">{t}L'image doit faire au plus 100 ko et sa taille ne doit pas dépasser 300*300 pixels.{/t}</span>
            {/if}
        </div>
        
        <div>
        {if isset($editerDescription)}
            <br/>{t}Description du groupe :{/t}
            <form method="post" action="jeu.php">
            <input type="hidden" value="m_groupe" name="mode" />
            <input type="hidden" value="{$hamsterId}" name="hamster_id" />
            <input type="hidden" value="modifGroupe" name="actionAcademy" />        
            <textarea name="description" cols="50" rows="10">{$descriptionGroupe}</textarea><br/>
            <input type="submit" value="{t}valider la nouvelle description{/t}"></form>
        {else}
            <div class="groupe_champs_description">{$descriptionGroupe}</div>
        {/if}
        </div>
      </div>
      <div class="hamBlocColonne">
    
        <div class="hamTitreBloc">{t}Bulletins du groupe{/t} :</div>        
        <table class="groupeBulletin">
            {foreach name=outer item=bulletin from=$lstBulletins}
                <tr><td style="font-size:small;">[{$bulletin.date|date_format:"%d/%m %H:%M"}] <strong>{$bulletin.nom_hamster}</strong> : {$bulletin.texte}</td></tr>
            {/foreach}        
            {if $smarty.foreach.outer.total eq 0 }
                <tr><td style="font-size:small;">{t}Aucun message{/t}</td></tr>
            {/if} 
        </table>    
        <form method="get" action="jeu.php">
            <input type="hidden" name="mode" value="m_groupe" />
            <input type="hidden" name="actionAcademy" value="ajouterBulletin" />
            <input type="hidden" name="hamster_id" value="{$hamsterId}" />
            <table cellpadding="0" cellspacing="0"><tr><td style="font-size:small;">{t}Ecrire{/t}&nbsp;</td>
            <td><input type="text" name="bulletinTexte" size="45" maxlength="150" /></td>
            <td><input type="submit" value="{t}envoyer{/t}" /></td></tr>
            <tr>
            <td style="font-size:9pt; margin-left:10px;" colspan="2"><input type="checkbox" name="alaposte" />{t}envoyer aussi à la Poste{/t}</td>
            </tr></table>
        </form>
    </div>
    <div class="hamBlocColonne" style="float:top;">
    
        <div class="hamTitreBloc">
        {if $nbMembres eq 1}
            {t}Le membre du groupe{/t} :
        {else}
            {t}Les{/t} {$nbMembres} {t}membres du groupe{/t} :
        {/if}
        </div>        
        
        {foreach name=outerMembre item=membre from=$lstMembres}
            <div style="float:left; width:115px; margin:0px 10px; text-align:center;">
                {if $membre.hamster_id eq $leaderId }
                    <strong>{lienprofilhamster hamster_id=$membre.hamster_id texte=$membre.nom }</strong> (leader)
                {else}
                    {lienprofilhamster hamster_id=$membre.hamster_id texte=$membre.nom }
                {/if}<br/>
                <img src="images/{$membre.hamster_image}" style="height:80px;" alt="hamster" /><br/>
                {t}Points{/t} : {$membre.note} <br/>
                {t}Dern. activité{/t}:{$membre.derniere_activite_jour}<br/>
                    
                {if $membre.specialite neq -1 }
                    <img src="images/{$membre.instrument_image}" style="vertical-align:middle; height:30px;" alt="" /> {$membre.specialite_nom}
                {else}
                    {t}aucune{/t}
                {/if}
                <br/>
                
                {if $leader && $membre.hamster_id neq $leaderId}
                    <a href="jeu.php?mode=m_groupe&amp;actionAcademy=virerMembre&amp;hamster_id={$hamsterId}&amp;groupe_id=$groupeId&amp;hamsterAVirer_id={$membre.hamster_id}">{t}Virer du groupe{/t}</a>
                {/if}        
            </div>
            {if $smarty.foreach.outerMembre.iteration % 3 == 0}
                <div style="clear:both;">&nbsp;</div>
            {/if}
        {/foreach}    
    <div style="clear:both;">&nbsp;</div>         
    </div>
    
    <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>        
    </div>
</div>

<div class="hamColonneDroite">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Actions{/t} : </span></div>        
        </div> 

        <div class="cadreArrondi-contenu">
        
          <div>
            <div class="groupe_description_titre">{t}Répétitions de musique{/t}</div>
            <div class="groupe_description_bloc">
            {t}Il y a{/t} {$nbInscritsRepet} {t}inscrit{/t}{if $nbInscritsRepet > 1}s{/if} {t}à la répétition :{/t} {$listeInscritsRepet}.
            {if ($nbInscritsRepet < round($nbMembres*0.75)) }
                {t}Il n'y a pas d'assez d'inscrits pour faire une répétition (il faut que{/t} {math equation="max(3,round($nbMembres*0.75))" } {t}musiciens soient inscrits à la répétition{/t}).
            {else}
                {t}Vous pouvez faire une répétition !{/t} <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=repeterEnGroupe" title="{t}Commencer la répétition{/t} !">{t}Répéter maintenant !{/t}</a><br/>
            {/if}
            <br/>&nbsp;<br/>
            {if $inscrit_repet}
                {$pseudoHamster} {t}est inscrit pour la prochaine répétition du groupe{/t}.
            {else}
                {$pseudoHamster} {t}n'est pas inscrit à cette répétition : fais vivre son groupe{/t}, <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=inscrireRepet" title="{t}Pour s'inscrire à la répèt', clique-ici ! Cout : 5 pieces.{/t}">{t}inscris-le !{/t}</a><br/>({t}coût :{/t} 5 {$IMG_PIECE})
            {/if}
            </div>
            <div class="groupe_description_titre">{t}Concerts{/t}</div>
            <div class="groupe_description_bloc">
            
            {if $concertPossible}                
                {t}Il y a{/t} {$nbInscritsConcert} {t}inscrit{/t}{if $nbInscritsConcert > 1}s{/if} {t}pour le prochain concert :{/t} {$listeInscritsConcert}.
                
                {if $nbInscritsConcert < round($nbMembres*0.75) } 
                    {t}Il n'y a pas d'assez d'inscrits pour faire un concert (il faut que{/t} {math equation="max(3,round($nbMembres * 0.75))" } {t}musiciens participent au concert{/t}).
                {else}
                    {t}Vous pouvez faire un concert !{/t} <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=concertEnGroupe" title="{t}Faire un concert !{/t}">{t}Jouer maintenant !{/t}</a><br/>
                {/if}
                <br/>&nbsp;<br/>
                {if $inscrit_concert }
                    {$pseudoHamster} {t}est inscrit pour le prochain concert du groupe{/t}.
                {else}
                    {$pseudoHamster} {t}n'est pas inscrit au prochain concert : fais vivre son groupe{/t}, <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=inscrireConcert" title="{t}Pour s'inscrire au concert, clique-ici ! Cout :{/t} {$coutInscriptionConcert} {t}pieces{/t}.">{t}inscris-le !{/t}</a> ({t}coût :{/t} {$coutInscriptionConcert} {$IMG_PIECE})
                {/if}
            {else}
                {t}Le groupe manque d'expérience pour faire un concert ! Il faut continuer les répétitions pour atteindre au moins 20 points d'expérience{/t}.
            {/if}
            </div>
          </div>
          
          <div class="groupe_description_titre">{t}Tournoi{/t}</div>
          <div class="groupe_description_bloc">
          {$tournoiTxt}
          </div>
          
          <div class="groupe_description_titre">{t}Gestion du groupe{/t}</div>
            <div class="groupe_description_bloc">
            
            {if $leader && isset($editerDescription) }
                <span style="font-size:8pt;"><a href="jeu.php?mode=m_groupe&amp;univers=1" onclick="javascript: var nouveauNom = prompt('{t}Nouveau nom du groupe{/t} : ', '{$nomGroupe}'); document.location.href='jeu.php?mode=m_groupe&hamster_id={$hamsterId}&groupe_id={$groupeId}&actionAcademy=nouveauNomGroupe&nom='+nouveauNom ;return false;">({t}changer le nom{/t})</a></span>
            {/if}
            <div class="groupe_description_bloc">
            {if $descriptionEditable}
                <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;modifierDescription=1" title="{t}Editer la description{/t}"><img src="images/icone_outils.gif" alt="{t}Modifier la description{/t}" style="width:20px; height:20px;" /> {t}Editer la description du groupe{/t}</a><br/>
            {/if}
            {if $leader}
                <br/>&nbsp;<br/>
                <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=dissoudreGroupe&amp;groupe_id={$groupeId}">{t}Dissoudre le groupe{/t}</a> ({t}attention, c'est irréversible!{/t})<br/>
                <br/>
                <form method="get" action="jeu.php">
                <input type="hidden" name="mode" value="m_groupe" />
                <input type="hidden" name="hamster_id" value="{$hamsterId}" />
                <input type="hidden" name="groupe_id" value="{$groupeId}" />
                <input type="hidden" name="actionAcademy" value="modifEditionMembre" />
                {t}Permettre aux membres de modifier la description du groupe :{/t} <input type="checkbox" name="checkEditionMembre" value="ok" {if $edition_membre} checked="checked" {/if} />
                <input type="submit" value="ok" />
                </form>
            {else}
                <br/>&nbsp;<br/><a href="jeu.php?mode=m_groupe&actionAcademy=quitterGroupe&hamster_id={$hamsterId}">{t}Quitter le groupe{/t}</a>
            {/if}
            </div>
    </div>
            
        </div>
    

        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
    </div>
</div>
<div style="clear:both;">&nbsp;</div>    
