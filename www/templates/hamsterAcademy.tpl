<div class="hamColonneGauche">
    {if $academy > 0}
    <div class="hamBlocColonneTransparent">
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Ses talents{/t} : </span></div> 
            </div>       
      
        <div class="cadreArrondi-contenu">
            <table border="0" width=240>
                {if $academy == 1}
                <tr>
                    <td>{t}Popularité{/t}</td><td>{dessinerTableauNote note=$popularite noteMax=10}</td>
                    <td>{$popularite} pts</td>
                </tr>
                <tr>
                    <td>{t}Expérience{/t}</td><td>{dessinerTableauNote note=$experience noteMax=10}</td>
                    <td>{$experience} pts</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td>{t}Danse{/t} :</td><td>{dessinerTableauNote note=$danse noteMax=10}</td>
                    <td>{$danse} pts</td>
                </tr>
                <tr>
                    <td>{t}Chant{/t} :</td><td>{dessinerTableauNote note=$chant noteMax=10}</td>
                    <td>{$chant} pts</td>
                </tr>
                <tr>
                    <td>{t}Musique{/t} :</td><td>{dessinerTableauNote note=$musique noteMax=10}</td>
                    <td>{$musique} pts</td>
                </tr>
                <tr>
                    <td>{t}Bon copain{/t}</td><td>{dessinerTableauNote note=$sociabilite noteMax=10}</td>
                    <td>{$sociabilite} pts</td>
                </tr>
                <tr>
                    <td>{t}Humour{/t} :</td><td>{dessinerTableauNote note=$humour noteMax=10}</td>
                    <td>{$humour} pts</td>
                </tr>
                <tr>
                    <td>&nbsp;</td>
                </tr>
                {elseif $academy == 2}
                <tr>
                    <td>{t}Expérience{/t}</td><td>{dessinerTableauNote note=$experience noteMax=$noteMaxFoot}</td>
                    <td>{$experience} pts</td>
                </tr>                
                <tr>
                    <td>{t}Agilité{/t}</td><td>{dessinerTableauNote note=$agilite noteMax=$noteMaxFoot}</td>
                    <td>{$agilite} pts</td>
                </tr>
                <tr>
                    <td>{t}Vitesse{/t} :</td><td>{dessinerTableauNote note=$vitesse noteMax=$noteMaxFoot}</td>
                    <td>{$vitesse} pts</td>
                </tr>
                <tr>
                    <td>{t}Endurance{/t} :</td><td>{dessinerTableauNote note=$endurance noteMax=$noteMaxFoot}</td>
                    <td>{$endurance} pts</td>
                </tr>
                <tr>
                    <td>{t}Défense{/t} :</td><td>{dessinerTableauNote note=$defense noteMax=$noteMaxFoot}</td>
                    <td>{$defense} pts</td>
                </tr>
                <tr>
                    <td>{t}Attaque{/t}</td><td>{dessinerTableauNote note=$attaque noteMax=$noteMaxFoot}</td>
                    <td>{$attaque} pts</td>
                </tr>
                <tr>
                    <td>{t}Arrêt de but{/t} :</td><td>{dessinerTableauNote note=$gardien noteMax=$noteMaxFoot}</td>
                    <td>{$gardien} pts</td>
                </tr>
                {/if}
                <tr>
                    <td>{t}Niveau{/t} : </td>
                    <td colspan="2">{$nomNiveau}</td>
                </tr>   
            </table>
    </div>
        
    <div class="cadreArrondi-bottom">
      <div class="cadreArrondi-bottom-left"></div>
      <div class="cadreArrondi-bottom-right"></div>
      <div class="cadreArrondi-bottom-center"></div>        
    </div>
    </div>
    {/if}
    {if $academy == 1}
        <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center">

          <span class="titre-bloc-gros">{t}Son instrument{/t} : </span></div>
           </div>
          
        <div class="cadreArrondi-contenu">
        {$txtInstrument}
        </div>
         
        <div class="cadreArrondi-bottom">
      <div class="cadreArrondi-bottom-left"></div>
      <div class="cadreArrondi-bottom-right"></div>
      <div class="cadreArrondi-bottom-center"></div>        
      </div>
        </div>
      {/if}
        </div>

<div class="hamColonneCentrale">

    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>

    
    {if $msg != ""}
        <div class="hamBlocColonne">{$msg}</div>
    {/if}
    
    <div class="hamBlocColonne">
      <div class="hamTitreBloc">
        {if $isBebe} 
            Bébé
        {else}
            {$nomHamster}
        {/if}
        </div> 
        
        <div class="hamImg">
            {if $isBebe}
                <img src="images/bebe_hamster.jpg" alt="bebe hamster" style="align:center;" title="Bébé hamster : dans 2 jours, il aura ses poils !" />
            {else}
                {$imgHamster}
            {/if}
        </div>    
    </div>
    
    {$blocCentralTxt}
    
    <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>        
    </div>
    
    <!-- <div style="clear:both;">&nbsp;</div> -->
</div>

<div class="hamColonneDroite">

    {if $academy > 0}
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Entraîner son hamster{/t} : </span></div>        
        </div> 

        <div class="cadreArrondi-contenu">
           <div>
           {$blocActionTxt}
          </div>
          <div style="clear:both;">&nbsp;</div>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
    {/if}

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Dans l'Academy{/t} :</span></div>        
        </div>
        
        <div class="cadreArrondi-contenu">
        <div>
        {$blocActionAcademyTxt}
        </div>
        <div style="clear:both;">&nbsp;</div>
        </div>
    

        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
    </div>
</div>
<div style="clear:both;">&nbsp;</div>    