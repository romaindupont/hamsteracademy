<div class="hamColonneGauche">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Son état :{/t}</span></div>        
        </div>       
      
        <div class="cadreArrondi-contenu">
        <table border="0">
            <tr>
                <td><span>{t}Ecuelle{/t}</span> </td><td>{dessinerTableauNote note=$faim noteMax=10}</td>
                <td>{if $faim == 0}{t}vide{/t}{elseif $faim > 9}{t}pleine{/t}{elseif $faim > 5}{t}bien remplie{/t}{else}{t}presque vide{/t}{/if}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>{t}Moral{/t}</td><td>{dessinerTableauNote note=$moral noteMax=$maxMoral}</td>
                <td>{$moral}/{$maxMoral}</td>
            </tr>
            <tr>
                <td>{t}Santé{/t}</td><td>{dessinerTableauNote note=$sante noteMax=$maxSante}</td>
                <td>{$sante}/{$maxSante}</td>
            </tr>
            <tr>
                <td>{t}Force{/t}</td><td>{dessinerTableauNote note=$force noteMax=$maxForce}</td>
                <td>{$force}/{$maxForce}</td>
            </tr>
            <tr>
                <td>{t}Beauté{/t}</td><td>{dessinerTableauNote note=$beaute noteMax=$maxBeaute}</td>
                <td>{$beaute}/{$maxBeaute}</td>
            </tr>
            <tr>
                <td>{t}Energie{/t}</td><td>{dessinerTableauNote note=$energie noteMax=$maxEnergie}</td>
                <td>{$energie}/{$maxEnergie} {$IMG_ENERGIE}</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td>{t}Poids{/t}</td><td>{dessinerTableauPoids poids=$poids}</td>
                <td>{$poids} gr.</td>
            </tr>
        </table>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Carnet de naissance{/t}</span></div>        
        </div>
          
        <div class="cadreArrondi-contenu">
                <script type="text/javascript">
                var txtChoixNomHamster = '{t}Choisis le nouveau prénom de ton hamster{/t} : <br/><input type="text" class="alertName" name="alertName" value="{$nomHamster}" /> <input type="hidden" class="hamsterId" name="hamsterId" value="{$hamster_id}" />';
                </script>
                    {t}Prénom{/t} : {$nomHamster}
                    {literal}
                    (<a href="#" onclick="javascript: $.prompt(txtChoixNomHamster,{
                      callback: callbackChangerNomHamster,
                      buttons: { Ok: true, Cancel: false }
                }); return false;" >
                {/literal}
                {t}changer{/t}</a>)<br/>
                    
                {t}Age{/t}: {$age}<br/>
                {t}Sexe{/t}: {if $sexe == 0}{t}mâle{/t}{else}{t}femelle{/t}{/if}
                <br/>&nbsp;<br/>
                {$oeuf9}
                {t}Yeux{/t} : {$yeux}<br/>
                {t}Pelage{/t} : {$pelage}
                <br/>&nbsp; <br/>
                <table cellpadding="0" cellspacing="0">
                    <tr>
                        <td>{t}Coquetterie{/t} : </td><td>{dessinerTableauNote note=$init_coquet noteMax=5}
                        </td><td>&nbsp;{$init_coquet}/5</td>
                    </tr>
                    <tr>
                        <td>{t}Force{/t} : </td><td>{dessinerTableauNote note=$init_fort noteMax=5}
                        </td><td>&nbsp;{$init_fort}/5</td>
                    </tr>
                    <tr>
                        <td>{t}Résistance{/t} : </td><td>{dessinerTableauNote note=$init_resistant noteMax=5}
                        </td><td>&nbsp;{$init_resistant}/5</td>
                    </tr>
                    <tr>
                        <td>{t}Sociable{/t} : </td><td>{dessinerTableauNote note=$init_sociable noteMax=5}
                        </td><td>&nbsp;{$init_sociable}/5</td>
                    </tr>
                    <tr>
                        <td>{t}Drague{/t} : </td><td>{dessinerTableauNote note=$init_dragueur noteMax=5}
                        </td><td>&nbsp;{$init_dragueur}/5</td>
                    </tr>
                    <tr>
                        <td>{t}Bâtisseur{/t} : </td><td>{dessinerTableauNote note=$init_batisseur noteMax=5}
                        </td><td>&nbsp;{$init_batisseur}/5</td>
                    </tr>                                        
                </table>

                {if ($pere_id > 0 || $mere_id > 0 || $compagnon_id > 0) }
                
                <br/>&nbsp; <br/><u>{t}Famille{/t}</u> : <br/>
                        
                    {if $pere_id > 0}
                        {t}Père{/t} : {lienprofilhamster hamster_id=$pere_id texte=$pere_nom } <br/>
                    {/if}
                    {if $mere_id > 0}
                        {t}Mère{/t} : {lienprofilhamster hamster_id=$mere_id texte=$mere_nom } <br/>
                    {/if}
                    {if $compagnon_id > 0}
                        {if $sexe==0}
                        {t}Marié avec{/t}
                        {else}
                        {t}Mariée avec{/t} 
                        {/if}
                        {lienprofilhamster hamster_id=$compagnon_id texte=$compagnon_nom } <br/>
                    {/if}
                {/if}
                    &nbsp;<br/>
                    {$txtReproductivite}
        </div>
            
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
</div>



<div class="hamColonneCentrale">

    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>

    
    {if $msg != ""}
        <div class="hamBlocColonne">{$msg}</div>
    {/if}
    
    <div class="hamBlocColonne">
      <div class="hamTitreBloc">
        {if $isBebe} 
            {t}Bébé{/t}
        {else}
            {$nomHamster}
        {/if}
        </div> 
        
        <div class="hamImg">
            {if $isBebe}
                <img src="images/bebe_hamster.jpg" alt="" style="align:center;" title="{t}Bébé hamster : dans 2 jours, il aura ses poils !{/t}" />
            {else}
                {$imgHamster}
            {/if}
        </div>    
    </div>
    
    {$blocCentralTxt}
    
    <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>        
    </div>
    
    <!-- <div style="clear:both;">&nbsp;</div> -->
</div>



<div class="hamColonneDroite">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}S'occuper du hamster{/t} : </span></div>        
        </div> 

        <div class="cadreArrondi-contenu">
          <!-- <div class="hamTitreBloc">{t}S'occuper du hamster{/t} : </div> -->
        
          <div>
          {$blocActionTxt}     
          </div>
          
          <div style="clear:both;">&nbsp;</div>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Sa cage actuelle{/t} : </span></div>        
        </div>
        
        <div class="cadreArrondi-contenu">
        
          <div align="center">
          <a href="jeu.php?mode=m_cage&amp;cage_id={$cage_id}" title="{t}Voir la cage{/t}">
            <img src="images/cage_reduc.gif" width="100" alt="Cage" /><br/>{$cage_nom}</a>
            
          </div>
          <br/>
          <div>
            <table cellpadding="0">
                <tr><td>{t}Propreté de sa cage{/t} : </td><td>
                {dessinerTableauNote note=$cage_proprete noteMax=10}
                </td></tr>
            </table>
          </div>
        
          {if $nb_cages > 1}
        
            <br/>
            <div>{t}Changer la cage du hamster vers :{/t} </div>
            <div><form action="jeu.php" method="get">
                <input type="hidden" name="mode" value="m_hamster" />
                <input type="hidden" name="hamster_id" value="{$hamster_id}" />
                <input type="hidden" name="action" value="deplacerHamster" />
                <select name="cageHamster">
                {$lstCagesPourTransfert}
                 </select> <input type="submit" value="ok" /></form>
            </div>
                
          {/if}
        </div>
    

        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
    </div>
</div>
<div style="clear:both;">&nbsp;</div>    