<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">

        <div align="center"><img src="images/transfusion_reduc.png" alt="" /></div><br/>&nbsp;<br/>
            
        {t}Ton hamster est gravement malade, il a été envoyé chez le vétérinaire{/t}. <br/>&nbsp;<br/>
        {t}Les soins intensifs coutent cher{/t} : <strong>{$coutVeto} {$IMG_PIECE} {t}pour le récupérer{/t}...</strong>
        
        <br/>&nbsp;<br/>
        
        {t}Deux possibilités{/t} : <br/>&nbsp;<br/>
        
        <img src="images/Veterinaire_reduc.png" alt="" style="vertical-align:middle;" /> {t}Récupérer l'hamster maintenant{/t} : <a href="jeu.php?mode=m_hamster&amp;action=recup_veto&amp;hamster_id={$hamster_id}" title="Cout : 150 pieces"> {t}Payer le vétérinaire{/t}</a>
        
        <br/>&nbsp;<br/>
            
        <img src="images/Veterinaire_reduc.png" alt="" style="vertical-align:middle;" />{t}Ou attendre encore quelques jours{/t} : <a href="jeu.php?mode=m_hamster&amp;hamster_id={$hamster_id}" title="Attendre"> {t}Attendre{/t} ...</a><br/>&nbsp;<br/>{t}Mais attention, le véto ne garde pas les hamsters éternellement. Au bout d'un certain temps, il les envoie à la SPH et tu ne pourras plus récupérer ton hamster...{/t}
        
    
  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>