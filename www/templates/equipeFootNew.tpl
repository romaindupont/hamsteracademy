{if isset($editerDescription)}
{literal}
<!-- TinyMCE -->
    <script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
    <script type="text/javascript">
        tinyMCE.init({
            mode : "textareas",
            theme : "advanced",
            plugins : "safari,advhr,advimage,advlink,emotions,iespell,media,contextmenu",

            // Theme options
            theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
            theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,blockquote,|,undo,redo,|,link,unlink,image,|,forecolor,backcolor,|,hr,emotions",
            theme_advanced_buttons3 : "",
            theme_advanced_toolbar_location : "top",
            theme_advanced_toolbar_align : "left",
            theme_advanced_statusbar_location : "bottom",
            theme_advanced_resizing : true,

            // Drop lists for link/image/media/template dialogs
            template_external_list_url : "lists/template_list.js",
            external_link_list_url : "lists/link_list.js",
            external_image_list_url : "lists/image_list.js"
        });
    </script>
<!-- /TinyMCE -->
{/literal}
{/if}

<div class="hamColonneGauche" style="width:200px;">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Infos{/t}</span></div>
        </div>       
      
        <div class="cadreArrondi-contenu">
        
        
        {if $leader}
            {$pseudoHamster} {t}est le leader de l'équipe{/t} : 
        {else}
            {$pseudoHamster} {t}est membre de l'équipe{/t} : 
        {/if}    

        <table border="0">
            <tr>
                <td><span>{t}Points{/t}</span> : </td><td>{$noteGroupe} pts</td>
            </tr>
            <tr>
                <td><span>{t}Expérience{/t}</span> : </td><td>{$nouvelleExperience}</td>
            </tr>
            <tr>
                <td><span>{t}Classement{/t}</span> : </td><td>{if $classement > 1}
                        {$classement} <span style="font-size:8pt;vertical-align:super;">ème</span>
                    {else} 
                        1er
                    {/if}</td>
            </tr>
        </table>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Niveau{/t} {$niveau}</span></div>
        </div>       
      
        <div class="cadreArrondi-contenu">
        ({$niveauDescription})
                <br/><i>{$niveauDescriptionPlus}</i>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros">{t}Bonus{/t}</span></div>        
        </div>
          
        <div class="cadreArrondi-contenu">
            {$bonusGroupeTxt}
            <div style="clear:both;">&nbsp;</div>    
        </div>
            
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
</div>

<div class="hamColonneCentrale" style="width:450px;">

    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>
    
    {if $msg != ""}
        <div class="hamBlocColonne">{$msg}</div>
    {/if}
    
    <div class="hamBlocColonne">
      <div class="hamTitreBloc">
         <b>{$nomGroupe}</b>
        </div> 
        
        <div class="hamImg">
            {$imageGroupe}<br/>
            {if isset($editerDescription) }
                <form method="POST" action="jeu.php" ENCTYPE="multipart/form-data">
                <br><table>
                <tr><td>{t}Changer le logo{/t}</td>
                <input type="hidden" value="m_groupe" name="mode" />
                <input type="hidden" value="{$hamsterId}" name="hamster_id" />
                <input type="hidden" value="modifImageGroupe" name="actionAcademy" />
                <input type="hidden" value="1" name="modifierDescription" />
                <td><input type="file" name="fichierImage" size="20" /></td>
                <td><input type="submit" value="Envoyer" /></td></tr></table></form>
                <span style="font-size:8pt;">L'image doit faire au plus 100 ko et sa taille ne doit pas dépasser 300*300 pixels.</span>
            {/if}
        </div>
        
        <div>
        {if isset($editerDescription)}
            <br/>Description de l'équipe :
            <form method="post" action="jeu.php">
            <input type="hidden" value="m_groupe" name="mode" />
            <input type="hidden" value="{$hamsterId}" name="hamster_id" />
            <input type="hidden" value="modifGroupe" name="actionAcademy" />        
            <textarea name="description" cols="50" rows="10">{$descriptionGroupe}</textarea><br/>
            <input type="submit" value="valider la nouvelle description"></form>
        {else}
            <div class="groupe_champs_description">{$descriptionGroupe}</div>
        {/if}
        </div>
      </div>
      <div class="hamBlocColonne">
    
        <div class="hamTitreBloc">{t}Messages de l'équipe{/t} :</div>        
        <table class="groupeBulletin">
            {foreach name=outer item=bulletin from=$lstBulletins}
                <tr><td style="font-size:small;">[{$bulletin.date|date_format:"%d/%m %H:%M"}] <strong>{$bulletin.nom_hamster}</strong> : {$bulletin.texte}</td></tr>
            {/foreach}        
            {if $smarty.foreach.outer.total eq 0 }
                <tr><td style="font-size:small;">Aucun message</td></tr>
            {/if} 
        </table>    
        <form method="get" action="jeu.php">
            <input type="hidden" name="mode" value="m_groupe" />
            <input type="hidden" name="actionAcademy" value="ajouterBulletin" />
            <input type="hidden" name="hamster_id" value="{$hamsterId}" />
            <table cellpadding="0" cellspacing="0"><tr><td style="font-size:small;">{t}Ecrire{/t}&nbsp;</td>
            <td><input type="text" name="bulletinTexte" size="45" maxlength="150" /></td>
            <td><input type="submit" value="envoyer" /></td></tr>
            <tr>
            <td style="font-size:9pt; margin-left:10px;" colspan="2"><input type="checkbox" name="alaposte" />envoyer aussi à la Poste</td>
            </tr></table>
        </form>
    </div>
    <div class="hamBlocColonne" style="float:top;">
    
        <div class="hamTitreBloc">
        {if $nbMembres eq 1}
            {t}Le membre de l'équipe{/t}: </div>
        {else}
            {t}Les{/t} {$nbMembres} {t}joueurs de l'équipe{/t} : </div>
        {/if}

        {foreach name=outerMembre item=membre from=$lstMembres}
            <div style="float:left; width:115px; margin:0px 10px; text-align:center;">
                {if $membre.hamster_id eq $leaderId }
                    <strong>{lienprofilhamster hamster_id=$membre.hamster_id texte=$membre.nom }</strong> (leader)
                {else}
                    {lienprofilhamster hamster_id=$membre.hamster_id texte=$membre.nom }
                {/if}
                
                <div><img src="images/{$membre.hamster_image}" style="height:100px;" alt="hamster" onmouseover='$("#infos{$membre.hamster_id}").show("fast");return true;' onmouseout='$("#infos{$membre.hamster_id}").hide("fast");return true;' /></div>
                
                <div class="groupe_specialite_membre">
                {if ! $rolesJoueursEditable }
                    <strong>{$membre.specialite_nom}</strong><br/>
                {else}
                <form action="jeu.php" method="get">
                <input type="hidden" name="mode" value="m_groupe" />
                <input type="hidden" name="hamster_id" value="{$hamsterId}" />
                <input type="hidden" name="univers" value="1" />
                <input type="hidden" name="modifierRolesJoueurs" value="1" />
                <input type="hidden" name="actionAcademy" value="changerStatusFoot" />
                <input type="hidden" name="changerStatusFoot" value="{$membre.hamster_id}" />
                <select name="changerStatusFoot_{$membre.hamster_id}">
                    <option value="0"
                    {if $membre.specialite == -1 || $membre.specialite == 0}
                          selected="selected"
                    {/if}
                    >{t}Remplaçant{/t}</option>
                    <option value="1" 
                    {if $membre.specialite == 1}
                          selected="selected"
                    {/if}
                    >{t}Attaquant{/t}</option>
                    <option value="2" 
                    {if $membre.specialite == 2}
                          selected="selected"
                    {/if}
                    >{t}Défenseur{/t}</option>
                    <option value="3" 
                    {if $membre.specialite == 3}
                          selected="selected"
                    {/if}
                    >{t}Gardien{/t}</option>
                </select>
                <input type="submit" value="ok" /></form>
                <br/>
                {/if}
                {t}Points{/t} : {$membre.note}<br/>
                {t}Dern. activité{/t} : {$membre.derniere_activite_jour}<br/>
                <span id="infos{$membre.hamster_id}" style="display:none;">
                {t}Agilité{/t} : {$membre.agilite}<br/>
                {t}Rapidité{/t} : {$membre.rapidite}<br/>
                {t}Attaque{/t} : {$membre.attaque}<br/>
                {t}Défense{/t} : {$membre.defense}<br/>
                {t}Expérience{/t} : {$membre.experience}<br/>
                {t}Endurance{/t} : {$membre.endurance}<br/>
                {t}Arrêt des buts{/t} : {$membre.gardien}
                </span>
                </div>
                    
                    {if $leader && $membre.hamster_id neq $leaderId && ! $rolesJoueursEditable }
                        <br/><a href="jeu.php?mode=m_groupe&amp;actionAcademy=virerMembre&amp;hamster_id={$hamsterId}&amp;groupe_id=$groupeId&amp;hamsterAVirer_id={$membre.hamster_id}">{t}Virer de l'équipe{/t}</a>
                    {/if}        
            </div>
            {if $smarty.foreach.outerMembre.iteration % 3 == 0}
                <div style="clear:both;">&nbsp;</div>
            {/if}
        {/foreach}
    <div style="clear:both;">&nbsp;</div>         
    </div>
    
    <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>        
    </div>
</div>

<div class="hamColonneDroite">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Actions{/t} : </span></div>        
        </div> 

        <div class="cadreArrondi-contenu">
        
          <div>
            <div class="groupe_description_titre">{t}Faire un entrainement{/t}</div>
            <div class="groupe_description_bloc">
            {t}Il y a{/t} {$nbInscritsRepet} {t}inscrit(s) à l'entrainement{/t} : {$listeInscritsRepet}.
            {if $nbInscritsRepet < 11 }
                Il n'y a pas d'assez d'inscrits pour l'entraînement (il faut que les 11 joueurs soient inscrits à l'entrainement).
            {else}
                Vous pouvez faire un entraînement ! <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=entrainementFoot" title="Faire un entrainement !"> Maintenant !</a><br/>
            {/if}
            <br/>&nbsp;<br/>
            {if $inscrit_repet}
                {$pseudoHamster} {t}est inscrit pour le prochain entrainement de l'équipe{/t}.
            {else}
                {$pseudoHamster} {t}n'est pas inscrit au prochain entrainement : c'est indispensable pour ton équipe{/t} ! <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=inscrireRepet" title="{t}Pour s'inscrire, clique-ici ! Cout : 5 pieces{/t}.">{t}inscris-le{/t} !</a> ({t}coût{/t} : 5 {$IMG_PIECE})
            {/if}
            </div>
            <br/>&nbsp;<br/>
            <div class="groupe_description_titre">{t}Matchs amicaux{/t}</div>
            <div class="groupe_description_bloc">
            
            {if $concertPossible}                
                Il y a {$nbInscritsConcert} inscrit{if $nbInscritsConcert > 1}s{/if} pour le prochain match amical : {$listeInscritsConcert}.
                
                {if $nbInscritsConcert < 11 } 
                    Il n'y a pas d'assez d'inscrits pour faire un match (il faut que 11 joueurs participent au match).
                {else}
                    Vous pouvez faire un match ! <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=footMatchAmical" title="Faire un match !">Jouer maintenant !</a><br/>
                {/if}
                <br/>&nbsp;<br/>
                {if $inscrit_concert }
                    {$pseudoHamster} est inscrit pour le prochain match amical.
                {else}
                    {$pseudoHamster} n'est pas inscrit au prochain match amical : fais vivre son équipe, <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=inscrireConcert" title="Pour s'inscrire au concert, clique-ici ! Cout : {$coutInscriptionConcert} pieces.">inscris-le !</a> (coût : {$coutInscriptionConcert} {$IMG_PIECE})
                {/if}
            {else}
                {t}L'équipe manque d'expérience pour faire un match ! Il faut continuer les entraînements pour atteindre au moins 20 points d'expérience.{/t}
            {/if}
            </div>
          </div>
          <div class="groupe_description_titre">{t}Tournoi{/t}</div>
          <div class="groupe_description_bloc">
          {$tournoiTxt}
          </div>
          
          <div class="groupe_description_titre">{t}Gestion de l'équipe{/t}</div>
            <div class="groupe_description_bloc">
            
            {if $leader && isset($editerDescription) }
                <span style="font-size:8pt;"><a href="jeu.php?mode=m_groupe&amp;univers=1" onclick="javascript: var nouveauNom = prompt('Nouveau nom de l équipe : ', '{$nomGroupe}'); document.location.href='jeu.php?mode=m_groupe&hamster_id={$hamsterId}&groupe_id={$groupeId}&actionAcademy=nouveauNomGroupe&nom='+nouveauNom ;return false;">(changer le nom)</a></span>
            {/if}
            <div class="groupe_description_bloc">
            {if $descriptionEditable}
                <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;modifierDescription=1" title="Editer la description"><img src="images/icone_outils.gif" alt="{t}Modifier la description{/t}" style="width:20px; height:20px;" /> {t}Editer la description du groupe{/t}</a><br/>
            {/if}
            {if $leader}
                <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;modifierRolesJoueurs=1" title="Editer la description"><img src="images/icone_outils.gif" alt="{t}Modifier les rôles des joueurs{/t}" style="width:20px; height:20px;" /> {t}Modifier les rôles des joueurs{/t}</a><br/>
            {/if}
            {if $leader && $membre.hamster_id == $leaderId}
                <a href="jeu.php?mode=m_groupe&amp;hamster_id={$hamsterId}&amp;actionAcademy=dissoudreGroupe&amp;groupe_id={$groupeId}">Dissoudre le groupe</a> (attention, c'est irréversible !)<br/>
                
                <form method="get" action="jeu.php">
                <input type="hidden" name="mode" value="m_groupe" />
                <input type="hidden" name="hamster_id" value="{$hamsterId}" />
                <input type="hidden" name="groupe_id" value="{$groupeId}" />
                <input type="hidden" name="actionAcademy" value="modifEditionMembre" />
                Permettre aux membres de modifier la description de l'équipe : <input type="checkbox" name="checkEditionMembre" value="ok" {if $edition_membre} checked="checked" {/if} />
                <input type="submit" value="ok" />
                </form>
            {else}
                <br/>&nbsp;<br/><a href="jeu.php?mode=m_groupe&actionAcademy=quitterGroupe&hamster_id={$hamsterId}">{t}Quitter l'équipe{/t}</a>
            {/if}
            </div>
            </div>
            
        </div>
    

        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
    </div>
</div>
<div style="clear:both;">&nbsp;</div>    