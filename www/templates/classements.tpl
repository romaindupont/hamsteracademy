<div align="center">
{t}Se trouvent ici les 50 meilleurs hamsters : les mieux notés, les plus expérimentés et les plus populaires !{/t}<br/>&nbsp;<br/>
{t}L'aide explique comment sont calculés les points{/t}.<br/>&nbsp;<br/>

        <img src="images/coupe.gif" alt=""><br/>
        <h2>{t}Hamsters les mieux notés :{/t}</h2><br/>
        {foreach from=$lstHamstersNotes item=hamster}
            <div>{$hamster}</div>
        {/foreach}
        <br/>

        <img src="images/coupe.gif" alt=""><br/>
        <h2>{t}Meilleurs groupes{/t} : </h2><br/>
        {foreach from=$lstGroupesNotes item=groupe}
            <div>{$groupe}</div>
        {/foreach}
        <br/>

        <img src="images/coupe.gif" alt=""><br/>
        <h2>{t}Hamsters les plus expérimentés{/t} : </h2><br/>
        {foreach from=$lstHamstersExp item=hamster}
            <div>{$hamster}</div>
        {/foreach}
        <br/>
        <img src="images/coupe.gif" alt=""><br/>
        <h2>{t}Hamsters les plus populaires{/t} : </h2><br/>
        {foreach from=$lstHamstersPop item=hamster}
            <div>{$hamster}</div>
        {/foreach}

</div>

<div style="clear:both;">&nbsp;</div>
