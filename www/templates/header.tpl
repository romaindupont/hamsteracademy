<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:fb="http://www.facebook.com/2008/fbml" xml:lang="{$lang}" lang="{$lang}">

<head>
<title>{$pagetitle}</title>
    <meta name="description" content="{$description}" />
    <meta name="keywords" content="{$keywords}" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="{$lang}" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <link rel="meta" href="http://www.hamsteracademy.com/labels.rdf" type="application/rdf+xml" title="ICRA labels" />
    <meta http-equiv="pics-Label" content='(pics-1.1 "http://www.icra.org/pics/vocabularyv03/" l gen true for "http://hamsteracademy.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 1) gen true for "http://www.hamsteracademy.com" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 1) gen true for "http://hamsteracademy.fr" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 1) gen true for "http://www.hamsteracademy.fr" r (n 0 s 0 v 0 l 0 oa 0 ob 0 oc 0 od 0 oe 0 of 0 og 0 oh 0 c 1))' />
    <link href="{$base_site_url}css/{$liste_styles}" rel="stylesheet" type="text/css" />
    {if isset($liste_scripts)}
    <script type="text/javascript" src="{$base_site_url}scripts/{$liste_scripts}"></script>
    {/if}
</head>

<body{if $univers == UNIVERS_INDEX } onload="javascript:document.forms.ha_login.htPseudo.focus();" {/if}>



<div class="mainPage"{if isset($pageWidth)}  style="width:{$pageWidth}px;" {/if}>

{if  isset($topbar_texte) &&  $topbar_texte != ""}
    <div class="topbar">
        {if $univers == UNIVERS_INDEX}
            <div class="topbartxt" >{$topbar_texte}</div>
        {else}
            <div class="topbar_sup">
            <table border="0" cellspacing="0" cellpadding="0" align="right">
              <tr>
                <td><img src="images/top_bar_left.gif" height="21" width="13" /></td>
                <td><div class="topbartxt" >
                    {$topbar_texte}
                    </div>
                </td>
                <td><img src="images/top_bar_right.gif" height="21" width="8" /></td>
              </tr>
            </table>
            </div>
            {if isset($topbar_texte_under)}
                <div style="clear:both;  line-height:1pt;">&nbsp;</div>
                <div class="topbar_under">
                    <div class="topbartxt topbartxt_under" >
                    {$topbar_texte_under}
                    </div>
                </div>
            {/if}
            
            
        {/if}
    </div>
{/if}
<div id="tooltip"></div>
