{literal}
<link href="http://www.hamsteracademy.fr/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$().ready(function() {
    $("#pseudoPassOffert").autocomplete("listePseudos.php", {
        minChars: 0,
        width: 200,
        max: 100,
        selectFirst: false
    });

});
</script>
{/literal}

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full" style="text-align:left;">
        
        <div align="center" style="font-weight:bold; font-size:18pt;">
            <img src="images/pieces_reduc.gif" alt="" style="vertical-align:middle;"/> {t}Banque de Hamster Academy{/t}
            <br/>&nbsp;<br/>
        </div>
        
        {if $msg != ""}
            <div id="divMsg">{$msg}</div><br/>&nbsp;<br/>
        {/if}
        <span id="lienAchatPass" {if $erreurPaiement != 0}style="display:none;"{/if}>
        {t}Tu possèdes{/t} {$nbPass} Pass{if $nbPass != 0}. <a href="jeu.php?mode=m_banque&amp;univers={$univers}"
            onclick='
                $("#divAchatPass").show();
                $("#lienAchatPass").hide();
                $("#divUtiliserPass").hide();
                $("#divMsg").hide();
                return false;'
            >{t}Clique ici pour acheter des Pass{/t}.</a>
        
        {else}
            !
        {/if}
        </span>
        <div {if $nbPass != 0}style="display:none;"{/if} id="divAchatPass" >
        {if $lang == "fr"}
            
            <div align="center" style="width:600px; margin-left:auto; margin-right:auto;">
            <span style="font-size:14pt ; font-weight:bold;">Achat de 1 Pass</span>
            <br/>&nbsp;<br/>
            Grâce à un Pass, tu peux avoir <strong>300 pièces d'or</strong> sans attendre ! Tu peux aussi passer au niveau suivant sans finir tous les objectifs, accélérer une grossesse ou la construction du nid, devenir VIP. Un Pass est aussi une idée de cadeau sympa pour un ami, pour son anniversaire par exemple !
            <br/>&nbsp;<br/>
            Pour acheter 1 pass, il suffit d'appeler le numéro ci-dessous ou d'envoyer un sms : 
            <br/>&nbsp;<br/>
            
            <!-- Begin Allopass Checkout-Button Code -->
			<script type="text/javascript" src="https://payment.allopass.com/buy/checkout.apu?ids=291826&idd=1226913&lang=fr"></script>
			<noscript>
			 <a href="https://payment.allopass.com/buy/buy.apu?ids=291826&idd=1226913" style="border:0">
			  <img src="https://payment.allopass.com/static/buy/button/fr/162x56.png" style="border:0" alt="Buy now!" />
			 </a>
			</noscript>
			<!-- End Allopass Checkout-Button Code -->
            
            <br/>&nbsp;<br/>&nbsp;<br/>
            <span style="font-size:14pt ; font-weight:bold;">Achat de plusieurs Pass</span>
            <br/>&nbsp;<br/>
            Ca coute moins cher d'en acheter plusieurs ! (via <img src="images/paypal.png" alt="Paypal" style="vertical-align:middle;" />)
            <br/>&nbsp;<br/>
            
            <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
            <input type="hidden" name="cmd" value="_s-xclick">
            <input type="hidden" name="custom" value="{$joueur_id}">
            <input type="hidden" name="hosted_button_id" value="PBXA7733PDY74">
            <table>
            <tr><td><input type="hidden" name="on0" value="Nombre de Pass">Nombre de Pass :</td></tr><tr><td><select name="os0">
                <option value="1 Pass">1 Pass €3,00</option>
                <option value="2 Pass">2 Pass €5,00</option>
                <option value="5 Pass">5 Pass €10,00</option>
                <option value="10 Pass">15 Pass €20,00</option>
            </select> </td></tr>
            </table>
            <input type="hidden" name="currency_code" value="EUR">
            <br/>
            <input type="image" src="https://www.paypal.com/fr_FR/FR/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - la solution de paiement en ligne la plus simple et la plus sécurisée !">
            <br/>
            <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
            </form>
            
            <br/>&nbsp;<br/>
            </div>
        {else}
            <div align="center" style="width:600px; margin-left:auto; margin-right:auto;">
            <span style="font-size:14pt ; font-weight:bold;">Buy 1 Pass</span>
            <br/>&nbsp;<br/>
            With a Pass, you can have <strong>300 gold coins</strong> without needing waiting for your next salary! Also, with 1 Pass, you can reach next level right now, reduce pregnancy duration or accelerate nest building, etc.<br/>A Pass is also a good idea of present for a friend, for its birthday for example! 
            <br/>&nbsp;<br/>
            <div style="vertical-aling:middle;">
            <img src="images/pieces_bonus_reduc.gif" alt="" > 
            <img src="images/maison2.gif" alt="" height="40"/>  
            <img src="images/nid.gif" alt="" height="40"/>
            <img src="images/cadeauNoel.gif" alt="" />
            </div>
            <br/>&nbsp;<br/>
            Buy 1 or several Pass (it is less expensive!) with <img src="images/paypal.png" alt="Paypal" style="vertical-align:middle;" /> : 
            <br/>&nbsp;<br/>
            Choose your currency: 
            <br/>&nbsp;<br/>
            <table border="0" cellpadding="0">
                <tr><td align="center"><img src="images/Dollar.jpg" alt="In Dollar" height="60" onclick='
                    $("#divBanqueDollar").show();
                    $("#divBanqueEuro").hide();
                    $("#divBanquePound").hide();
                    return false;' /><br/>Dollar</td>
                    <td>&nbsp;</td><td align="center"><img src="images/Euro.jpg" alt="In Euro" height="60" onclick='
                    $("#divBanqueDollar").hide();
                    $("#divBanqueEuro").show();
                    $("#divBanquePound").hide();
                    return false;'/><br/>Euro</td>
                    <td>&nbsp;</td><td align="center"><img src="images/Pound.jpg" alt="In Pound" height="60" onclick='
                    $("#divBanqueDollar").hide();
                    $("#divBanqueEuro").hide();
                    $("#divBanquePound").show();
                    return false;'/><br/>Pound</td></tr>
                <tr><td align="center">
                <div id="divBanqueDollar" style="display:none;">
                    <table>
                    <tr><td>
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                            <input type="hidden" name="cmd" value="_s-xclick">
                            <input type="hidden" name="hosted_button_id" value="B4AAG33W93QFU">
                            <input type="hidden" name="custom" value="{$joueur_id}">
                            <input type="hidden" name="on0" value="Number of Pass">Number of Pass</td></tr><tr><td><select name="os0">
                            <option value="1 Pass">1 Pass $3,00</option>
                            <option value="2 Pass">2 Pass $5,00</option>
                            <option value="5 Pass">5 Pass $10,00</option>
                            <option value="15 Pass">15 Pass $20,00</option>
                            </select> </td></tr>
                            </table>
                            <input type="hidden" name="currency_code" value="USD">
                            <br/>
                            <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                            <br/>
                            <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
                        </form>
                </div>
            </td><td width="30">&nbsp;</td><td align="center">
                <div id="divBanqueEuro" style="display:none;">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="K898T9WAFQBTS">
                        <input type="hidden" name="custom" value="{$joueur_id}">
                        <table>
                        <tr><td>
                        <input type="hidden" name="on0" value="Number of Pass">Number of Pass</td></tr><tr><td><select name="os0">
                        <option value="1 Pass">1 Pass €3,00</option>
                        <option value="2 Pass">2 Pass €5,00</option>
                        <option value="5 Pass">5 Pass €10,00</option>
                        <option value="15 Pass">15 Pass €20,00</option>
                        </select> </td></tr>
                        </table>
                        <input type="hidden" name="currency_code" value="EUR">
                        <br/>
                        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <br/>
                        <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
                    </form>
                </div>
            </td><td width="30">&nbsp;</td><td align="center">
                <div id="divBanquePound" style="display:none;">
                    <form action="https://www.paypal.com/cgi-bin/webscr" method="post">
                        <input type="hidden" name="cmd" value="_s-xclick">
                        <input type="hidden" name="hosted_button_id" value="NGZN9XKDDUWEQ">
                        <input type="hidden" name="custom" value="{$joueur_id}">
                        <table>
                        <tr><td><input type="hidden" name="on0" value="Number of Pass">Number of Pass</td></tr><tr><td><select name="os0">
                            <option value="1 Pass">1 Pass £3,00</option>
                            <option value="2 Pass">2 Pass £5,00</option>
                            <option value="5 Pass">5 Pass £10,00</option>
                            <option value="15 Pass">15 Pass £20,00</option>
                        </select> </td></tr>
                        </table>
                        <input type="hidden" name="currency_code" value="GBP">
                        <br/>
                        <input type="image" src="https://www.paypal.com/en_US/i/btn/btn_buynowCC_LG.gif" border="0" name="submit" alt="PayPal - The safer, easier way to pay online!">
                        <br/>
                        <img alt="" border="0" src="https://www.paypal.com/fr_FR/i/scr/pixel.gif" width="1" height="1">
                    </form>
                </div>
            </td>
            </tr>
            </table>
            
            <br/>&nbsp;<br/>
            </div>
        {/if}
        </div>
        <span id="divUtiliserPass" {if $erreurPaiement != 0}style="display:none;"{/if}>
        <br/>&nbsp;<br/>

        <strong>{t}Avec un Pass, tu peux faire l'une des actions ci-dessous :{/t}</strong><br/>&nbsp;<br/>
            
        <div>
          
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto; ">
                <div style="float:left; width:250px; text-align:center;"><br />
                    <span class="titre-bloc-gros2"><strong>{t}Obtenir 300 pièces{/t}</strong></span>
                    <br/>&nbsp;<br/>                    
                    <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassPieces" title="{t}Echanger 1 pass contre 300 pièces d'or{/t}"><img src="images/pieces_bonus_reduc.gif" alt=""><br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {foreach item=hamster from=$listeHamstersGrossesse}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto;">
                <div style="float:left; width:250px;text-align:center;"><br />
                    <span class="titre-bloc-gros2"><strong>{t}Réduire la durée de la grossesse de{/t} {$hamster.nom} {t}à 1 minute{/t}</strong></span>
                    <br/>&nbsp;<br/>
                    <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassGrossesse&amp;hamsterConcerne={$hamster.hamster_id}" title="{t}Echanger 1 pass contre 1 grossesse accélérée{/t}">
                    <img src="images/prev_grossesse.jpg" alt="" height="60" />
                    <br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {/foreach}
            {foreach item=hamster from=$listeHamstersCabane}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto;">
                <div style="float:left; width:250px;text-align:center;"><br />
                    <span class="titre-bloc-gros2"><strong>{t}Réduire la durée de la construction de la cabane de{/t} {$hamster.nom} {t}à 1 minute{/t}</strong></span>
                    <br/>&nbsp;<br/>
                    <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassCabane&amp;hamsterConcerne={$hamster.hamster_id}" title="{t}Echanger 1 pass contre 1 cabane terminée{/t}">
                    <img src="images/maison2.gif" alt="" height="40"/>
                    <br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {/foreach}
            {foreach item=hamster from=$listeHamstersNid}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto;">
                <div style="float:left; width:250px;text-align:center;"><br />
                    <span class="titre-bloc-gros2"><strong>{t}Réduire la durée de la construction du nid de{/t} {$hamster.nom} {t}à 1 minute{/t}</strong></span>
                    <br/>&nbsp;<br/>
                    <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassNid&amp;hamsterConcerne={$hamster.hamster_id}" title="{t}Echanger 1 pass contre 1 nid terminé{/t}">
                    <img src="images/nid.gif" alt="" height="40"/>
                    <br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {/foreach}
            {if $nouveauNiveau != -1}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto;">
                <div style="float:left; width:250px;text-align:center;"><br />
                <span class="titre-bloc-gros2"><strong>{t}Passer immédiatement au niveau{/t}&nbsp;{$nouveauNiveau}</strong></span>
                <br/>&nbsp;<br/>
                <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassNiveauSuperieur" title="{t}Utiliser 1 pass pour passer au niveau{/t} {$nouveauNiveau} "><img src="images/CaseACocher2.gif" alt="" /><br/><img src="images/CaseACocher2.gif" alt="" />
                <br/>&nbsp;<br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {/if}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto;">
                <div style="float:left; width:250px;text-align:center;"><br />
                <span class="titre-bloc-gros2"><strong>{t}Offrir un pass à un ami (pour son anniversaire par exemple){/t}</strong></span>
                <br/>&nbsp;<br/>
                {t}Indique le pseudo du joueur :{/t}<br/>&nbsp;<br/>
                <form action="jeu.php" method="get">
                <input type="hidden" name="univers" value="{$univers}" />
                <input type="hidden" name="mode" value="m_pass" />
                <input type="hidden" name="action" value="offrirPass" />
                <img src="images/cadeauNoel.gif" alt="" style="vertical-align:bottom;" />
                <input type="text" id="pseudoPassOffert" name="pseudoPassOffert" value="" size="20" />
                <input type="submit" value="ok" />
                </form>
                <br/>{t}Coût : 1 pass{/t}
                </div>
                
            </div>
            {if $vip == 0}
            <div class="blocRounded" style="width:300px;margin-left:auto;margin-right:auto; ">
                <div style="float:left; width:250px; text-align:center;"><br />
                    <span class="titre-bloc-gros2"><strong>{t}Devenir VIP pour le mois en cours{/t}</strong></span>
                    <br/>&nbsp;<br/>                    
                    <a href="jeu.php?mode=m_pass&amp;univers={$univers}&amp;action=echangerPassVIP" title="{t}Utiliser 1 pass pour devenir VIP{/t}"><img src="images/VIP.png" alt="VIP" /><br/>{t}Coût : 1 pass{/t}</a>
                </div>
            </div>
            {/if}
        </div>
        <div style="clear:both;">&nbsp;</div>
        </span>
      
  </div>
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>