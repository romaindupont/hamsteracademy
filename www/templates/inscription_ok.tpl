
<div align="center" style="float:left;margin-right:auto;margin-left:auto;width:600px; margin-bottom:30px;">
    <h3>{t}Félicitations{/t} !!</h3>
    <img src="images/hamster_inscription.gif" alt="{t}inscription terminée{/t}">
    <br/>&nbsp;<br/>
    
    <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center">
          {t}L'inscription est finie{/t} !   
          {t}Tu commences le jeu avec{/t} :
          </div>
        </div>       
    <div class="cadreArrondi-contenu">
        <ul>
        <li>{t}une cage{/t} <br/><span><img src="images/cageSansHamster.png" alt="{t}Cage d'inscription{/t}" style="vertical-align:middle;" /></span>
        </li> 
        <li>{t}un biberon{/t} <span><img src="images/biberon.gif" alt="" style="vertical-align:middle;" /></span>
        </li>
        <li>{t}une écuelle{/t} <span><img src="images/ecuelle_pleine_reduc.gif" alt="" style="vertical-align:middle;" /></span>
        </li>
        <li>{$nbPiecesInscription} {t}pièces d'or{/t} {$IMG_PIECE}</li>
        <li>{t}et{/t} {$alimentsInscription} {t}portions d'aliments{/t}</li>
        </ul>
    </div>
    
    <div class="cadreArrondi-bottom">
      <div class="cadreArrondi-bottom-left"></div>
      <div class="cadreArrondi-bottom-right"></div>
      <div class="cadreArrondi-bottom-center"></div>        
    </div>
    
    <div style="clear:both;">&nbsp;</div>
    
    <form action="jeu.php" method="post">
        <input type="hidden" name="htPseudo" value="{$pseudo}">
        <input type="hidden" name="htPasswd" value="{$passwd}">
        <input type="hidden" name="page" value="jeu">
        <input type="hidden" name="login" value="1">
        <input type="submit" value="{t}Entrer dans Hamster Academy !{/t}">
    </form>
</div>

<br/>&nbsp;<br/>