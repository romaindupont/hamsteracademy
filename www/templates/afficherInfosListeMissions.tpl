<div align="center"><strong>{t}Liste de toutes les missions de Hamster Academy{/t}</strong></div><br/>
<br />
{foreach item=mission from=$listeMissions name=lstAcc}

    <div class="cornflex yellowbox" style="width:700px;">
        <div class="t">
            <div class="c">
                <div style="margin-bottom:15px;font-size:9pt;">
                    
                    <span style="font-weight:bold; font-size:12pt;">{t}Mission{/t} {$smarty.foreach.lstAcc.iteration}</span>
                    
                    <br/>&nbsp;<br/> 
                    
                    {foreach item=objectif from=$mission[0]}
                    
                    - <strong>{$objectif[0]}</strong> : {$objectif[1]}<br/>
                    
                    {/foreach}
                    
                    
                </div>
            </div>
        </div>                
        <div class="r"></div>
        <div class="b"></div>
        <div class="l"></div>
    </div>
    <div style="clear:both;">&nbsp;</div>
{/foreach}
<div style="clear:both;">&nbsp;</div>