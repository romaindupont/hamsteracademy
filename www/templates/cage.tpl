{literal}
<!--[if lt IE 7]>
<script defer type="text/javascript" src="scripts/pngfix.js"></script>
<![endif]-->

<div class="info"></div>
<div class="accessoireCourantId"></div>


<script type="text/javascript" >

function addDraggable(el){
    el.draggable(
    {
        revert: 'invalid',
        zIndex: 1,
        opacity: 1
    }
    )
};


$(document).ready(

    function() {
        
        //Nos élements draggable sont définis ici. Ce sont tous les éléments avec la class CSS '.drag'.
        addDraggable($('.cageAccessoire'));

        $('.blocEtageCage').droppable( {

           //elle accept les élément ayant la class 'drag'
           accept : '.cageAccessoire',
            
            //ici cela affecte des classes css suivant que la zone est active ou hover. Pratique pour changer de style au survol.
           activeClass: 'droppable-active',
           hoverClass: 'droppable-hover',

            //plusieurs zone de tolerance existe. Allez faire un tour sur la doc.
            //Ici nous choisissons de pouvoir dropper un élément quand le pointeur de la souri est dans la zone.
            tolerance: 'pointer',
            
            //on définit ce qu’il va se passer lorsqu’on drop un élément dans la zone. On passe l’élément draggé en parametre.
            
            drop: function (e,drag) {

                var pos_x = drag.position.left;
                var pos_y = drag.position.top;
                var accessoire_id = drag.helper.attr('id');
                var etage_id = this.id;
                var etage_obj = this;
                
                $.get("deplacerAccessoire.php",{accessoire_id:accessoire_id,posX:pos_x,posY:pos_y,etage_id:etage_id},
                    function(data){
                        $tab = data.split(',');
                        
                        // changement d'étage
                        $nouvelEtage = $tab[1];
                        $precEtage = (drag.helper.parent().get(0).id).substr(6,8);
                        
                        if ($nouvelEtage != $precEtage) {
                            var el1 = $(drag.helper);
                            var el2 = $('#etage_'+$nouvelEtage);
                            var nouvelObj = $(el2).append($(el1));
                            $(el1).animate({
                                left : $tab[0],
                                top : $tab[2]
                            });
                            
                            if ($tab[3] == 1) { // mission réussie
                                $('#message_bloc').show();
{/literal}                                
{if $lang == "fr"}
                                $('#message_bloc').append("Bravo ! Tu as réussi la mission ! Va vite voir à l'accueil la nouvelle mission !");
{else}
                                $('#message_bloc').append("Congratulations! The mission is successful! Go at home to see your new mission!");
{/if}                                
{literal}
                            }
                        }
                        else {
                            drag.helper.animate({
                                left : $tab[0],
                                top : $tab[2]
                            });
                        }
                    }
                );
            },

            fit: true

        } );
    }
);

</script>

<script type="text/javascript">
                
function publierFacebook(facebookaction,facebookaction_id) {
    
    $('#message_bloc').html('<img src="images/wait.gif" style="vertical-align:middle;" /> Cage => Facebook...<br/>&nbsp;<br/>') ;
    $.ajax({
        type : 'POST', url : 'publierFacebook.php', dataType : 'text',
        data :{action:facebookaction,
            action_id:facebookaction_id,
        },
        success : function(retour){
            $('#message_bloc').html(retour + "<br/>&nbsp;<br/>") ;
        }
    });
    return false;
};

</script>
{/literal}

<div class="cageColonneGauche">

    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">{t}Son état{/t} :</span></div>        
        </div>
            
        <div class="cadreArrondi-contenu">   

        <div>{t}Propreté{/t} :{dessinerTableauNote note=$proprete noteMax=10}</div>
        <br/>        
        <div>{t}Les habitants{/t} :</div>        
        <table cellpadding="2" style="margin-left:2em;">
            {$lstHabitants}
        </table>
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
  <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">
                {t}Actions{/t}
                </span></div>        
         </div>
        
        <div class="cadreArrondi-contenu">
         <div>{$commandeCopeaux}</div>
        
        <div><script type="text/javascript">
            var txtChoixNomCage = '{t}Choisis le nouveau nom de ma cage{/t} : <br/><input type="text" class="alertName" name="alertName" value="{$nomCage}" /> <input type="hidden" class="cageId" name="cageId" value="{$cageId}" />';
            </script>
            {literal}
            <a href="#" onclick="javascript: $.prompt(txtChoixNomCage,{
                callback: callbackChangerNomCage,
                buttons: { Ok: true, Annuler: false }
                }); return false;" >
            {/literal}
            <img src="images/stylo2.gif" alt="" style="vertical-align:middle;" /> 
            {t}Changer son nom{/t}</a>
        </div>
        {if $actionFacebook neq ""}
        <div>{$actionFacebook}</div>
        {/if}
        <div>{$actionEolienne}</div>
        
        {if $actionRoueMotorisee neq ""}
        <div>{$actionRoueMotorisee}</div>
        {/if}
        </div>
        
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
    
    {if $peutModifierCage}
    <div class="hamBlocColonneTransparent">
         <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">
                {t}L'atelier{/t} <img class="imageTest" src="images/icone_outils.gif" alt="" style="vertical-align:middle; height:30px;" /></span>
                 </div>
         
         <div class="cadreArrondi-contenu">
       
        {if $peutRajouterColonne}
            <div class="actionCage"><a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=agrandirSurface" title="{t}prix{/t} : {$coutAgrandissementSurface} {t}pièces{/t}"><img src="images/grille.png" alt="" style="vertical-align:middle;" /> {t}Agrandir la surface{/t} ({$coutAgrandissementSurface} {t}pièces{/t})</a></div>
        {/if}

        <div class="actionCage"><div class="icone"><img src="images/grille.png" alt="" style="vertical-align:middle;" /></div><a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=ajouterEtage" title="{t}prix{/t} : {$coutAgrandissementHauteur} {t}pieces{/t}"> {t}Ajouter un étage{/t} ({$coutAgrandissementHauteur} {t}pièces{/t})</a></div>
        <div class="actionCage"><div class="icone"><img src="images/pinceau.gif" alt="" style="vertical-align:middle;" /></div> {t}Peindre la cage en couleur unie{/t}<br/>
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_BLEUVERT}" title="{t}en bleu/vert{/t}"><img src="images/bleu.gif" alt="" style="vertical-align:middle;" /></a> 
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_ROSE}" title="{t}en rose{/t}"><img src="images/rose.gif" alt="" style="vertical-align:middle;" /></a> 
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_NOIR}" title="{t}en noir{/t}"><img src="images/noir.gif" alt="" style="vertical-align:middle;" /></a> 
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_BLANC}" title="{t}en blanc{/t}"><img src="images/blanc.gif" alt="" style="vertical-align:middle;" /></a> 
            ({t}prix{/t}: {$coutPeinture} {t}pièces{/t})</div>
        {if $niveauJoueur > 4}
        <div class="actionCage"><div class="icone"><img src="images/pinceau.gif" alt="" style="vertical-align:middle;" /></div> {t}Peindre la cage en design{/t}<br/>
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_COEUR}" title="{t}avec des coeurs{/t}, {t}prix{/t} {$coutPeintureCoeur} {t}pièces{/t}" ><img src="images/texture_coeur_red.gif" alt="" style="vertical-align:middle;" /></a>
            {if $niveauJoueur > 5}
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_FEU}" title="{t}l'enfer{/t}, {t}prix{/t} {$coutPeintureFeu} {t}pièces{/t}" ><img src="images/texture_feu_red.gif" alt="" style="vertical-align:middle;" /></a>
            {/if}
            {if $niveauJoueur > 6}
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_HALLOWEEN}" title="{t}halloween{/t}, {t}prix{/t} {$coutPeintureHalloween} {t}pièces{/t}" ><img src="images/texture_halloween_red.gif" alt="haloween" style="vertical-align:middle;" /></a>
            {/if}
            {if $niveauJoueur > 7}
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_MER}" title="{t}mer{/t}, {t}prix{/t} {$coutPeintureMer} {t}pièces{/t}" ><img src="images/texture_mer_red.gif" alt="" style="vertical-align:middle;" /></a>
            {/if}
            {if $niveauJoueur > 8}
            <a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=peindre&amp;couleur={$FOND_CAGE_NEIGE}" title="{t}neige{/t}, {t}prix{/t} {$coutPeintureNeige} {t}pièces{/t}" ><img src="images/texture_neige.png" alt="" style="vertical-align:middle;" /></a>
            {/if}
        </div>
        {/if}
        
        {$blocActionTxt}
                 </div>
        
         <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
        </div>
        
    </div>
    
    <div class="hamBlocColonneTransparent">
    
        <div class="cadreArrondi-top">
                <div class="cadreArrondi-top-left"></div>
                <div class="cadreArrondi-top-right"></div>
                <div class="cadreArrondi-top-center"><span class="titre-bloc-gros2">
                {t}La boutique{/t}</span></div>        
        </div>         
        
        <div class="cadreArrondi-contenu">
            <br/><div><a href="jeu.php?mode=m_achat&amp;pagePrecedente=m_cage&amp;cage_id={$cageId}&amp;pourlacage_id={$cageId}&amp;rayon=2" title="{t}Acheter un nouvel accessoire{/t} : "><img src="images/vente.gif" alt="" style="vertical-align:middle;" /> {t}Acheter un accessoire{/t}</a></div>
            
            <br/><div><a href="jeu.php?mode=m_cage&amp;cage_id={$cageId}&amp;action=vendreCage" title="{t}Vendre la cage : elle doit d'abord être vide et sans hamster. Prix de vente{/t} : {$coutVenteCage} pièces."><img src="images/poubelle.gif" alt="" style="vertical-align:middle;" /> {t}Vendre la cage{/t}</a> {$ingredient1}</div>
            
            {$oeuf8}
        </div>
        
        <div class="cadreArrondi-bottom">
                <div class="cadreArrondi-bottom-left"></div>
                <div class="cadreArrondi-bottom-right"></div>
                <div class="cadreArrondi-bottom-center"></div>        
        </div>        
        
    </div>

    {/if}

</div>

<div class="cageColonneCentrale"  style="margin-left: auto; margin-right: auto;">

    <div class="hamBlocColonne-top">
          <div class="hamBlocColonne-top-left"></div>
          <div class="hamBlocColonne-top-right"></div>
          <div class="hamBlocColonne-top-center"></div>        
    </div>
    
    {if $msg neq ""}
    <div class="hamBlocColonne" class="message_bloc" id="message_bloc" >{$msg}</div>
    {else}
    <div id="message_bloc"></div>
    {/if}
    
    <div class="hamBlocColonneTransparent{$classSupplCage}">
        <div class="hamTitreBloc" style="text-align:center;">{$nomCage}</div>

        {$dessinCage}
            
        {$txtInscriptionConcours}

       
       {if $lang == "en"}
           <br/>&nbsp;<br/>
           <br/>&nbsp;<br/>
           <div align="center">
           
            {literal}
            <script type="text/javascript"><!--
                google_ad_client = "pub-4942968069828673";
                /* 468x60, date de création 31/07/10 */
                google_ad_slot = "9256143706";
                google_ad_width = 468;
                google_ad_height = 60;
                //-->
                </script>
                <script type="text/javascript"
                src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
            </script>
            {/literal}
            </div>
        {/if}
        
        
        
    </div>
    
  <div class="hamBlocColonne-bottom">
          <div class="hamBlocColonne-bottom-left"></div>
          <div class="hamBlocColonne-bottom-right"></div>
          <div class="hamBlocColonne-bottom-center"></div>
          {if $lang == "fr"}
               <br/>&nbsp;<br/>
               <div align="center">
               
                {literal}
                  <script type="text/javascript"><!--
                        google_ad_client = "ca-pub-4942968069828673";
                        /* Cage fr */
                        google_ad_slot = "8617250692";
                        google_ad_width = 468;
                        google_ad_height = 60;
                        //-->
                        </script>
                        <script type="text/javascript"
                        src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
                    </script>
                {/literal}
                </div>
        {/if}   
    </div> 
</div>

<div style="clear:both;">&nbsp;</div>
