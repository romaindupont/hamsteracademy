{literal}
<script type="text/javascript">

function checkPseudoAjax() {
    
    var pseudoSaisi = $('#pseudo').val();
    
    if ( $('#messageCheckPseudo').length > 0) {
        
        $('#messageCheckPseudo').text('');
        
        if (pseudoSaisi.length >= 3) {
        
            $.get("checkPseudoAjax.php",{pseudoSaisi:pseudoSaisi},
                function(data){
                    if (data == "1") {
                       $('#messageCheckPseudo').html(' (ce pseudo est déjà  pris !)');
                    }
                    else if (data == "2") {
                       $('#messageCheckPseudo').html(' (le pseudo doit comporter des caractères)');
                    }
                    else if (data == "3") {
                       $('#messageCheckPseudo').html(' (2 symboles spéciaux autorisés : _ et . )');
                    }
                    else {
                        $('#messageCheckPseudo').html(' <img src="images/icone_ok.png" alt="ok" />');
                    }
                }        
            );
        }
    }
    else if ($('#messageCheckPseudoEn').length > 0) {
        $('#messageCheckPseudoEn').text('');
        
        if (pseudoSaisi.length >= 3) {
        
            $.get("checkPseudoAjax.php",{pseudoSaisi:pseudoSaisi},
                function(data){
                    if (data == "1") {
                       $('#messageCheckPseudoEn').html(' (this username is already used!)');
                    }
                    else if (data == "2") {
                       $('#messageCheckPseudoEn').html(' (this username must only contain characters )');
                    }
                    else if (data == "3") {
                       $('#messageCheckPseudoEn').html(' (2 special symbols are allowed: _ and . )');
                    }
                    else {
                        $('#messageCheckPseudoEn').html(' <img src="images/icone_ok.png" alt="ok" />');
                    }
                }        
            );
        }
    }
}

</script>
{/literal}

{if $lang == "en"}
{literal}
<style content="text/css">
.logoMain_inscription {
    background: transparent url('../images/fond-logoMain_Inscription-en.png') top left no-repeat;
}
.macaron {
    background: transparent url('../images/macaron-en.png');
}    
</style>
{/literal}
{/if}

<div class="logoMain_inscription">
    <div class="logoImage">     
        <img src="images/logoHamsterAcademy.gif" alt="Hamster Academy" />
    </div>
    <div class="macaron"></div>
</div>





<!-- <div class="logoImageInscription">    
    <img src="images/inscription_{$lang}.gif" alt="{t}Inscription à  Hamster Academy{/t}" />
</div> -->


<div class="mainPageUnderMenu">

  <div class="inscriptionPage">
  
    <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
    </div>
    
    <div class="hamBlocColonne_full">


<form method="post" action="{t}inscription.php{/t}" >
<input type="hidden" name="parrain" value="{$parrain}" />
<input type="hidden" name="montrer_profil" value="1" />

{if $nbErreurs > 0}
    {t}L'inscription n'est pas encore finie, il faut corriger les problèmes ci-dessous{/t} :<br/>
    {if isset($erreurPseudoMsg) } {$erreurPseudoMsg} <br/> {/if}
    {if isset($erreurPasswdMsg) } {$erreurPasswdMsg} <br/> {/if}
    {if isset($erreurMailMsg) } {$erreurMailMsg} <br/> {/if}
    {if isset($erreurCharteMsg) } {$erreurCharteMsg} <br/>{/if}
    {if isset($erreurHamsterMsg) } {$erreurHamsterMsg}{/if}
    <div style="clear:both;">&nbsp;</div>
{/if}

<div align="center"><span class="titre1">{t}Inscription à Hamster Academy{/t} !</span><br />
&nbsp;<br/>
<span class="titre1">{t}C'est simple et gratuit !{/t}</span>
<br/>&nbsp;<br/>
{$facebookTxt}
<br/>&nbsp;<br/>
{include file='formulaireNouvelHamster.tpl'}</div>
<div style="clear:both;">&nbsp;</div>

<div class="blocRounded" style="width:800px; padding-left:10px;">
    {t}Pour ton compte{/t} : {t}choisis ton pseudo et mot de passe{/t} !
    <br/>&nbsp;<br/>
    <div style="clear:both;">
        <span class="optionsNomChamps">{t}Ton pseudo d'éleveur{/t}</span>
        <span class="optionsValeurChamps"><input name="pseudo" id="pseudo" type="text" maxlength="30" value="{$pseudo}" onkeyup="javascript:checkPseudoAjax(); return true;" /><span id="messageCheckPseudo{if $lang != "fr"}En{/if}">&nbsp;</span></span>
            {$erreurPseudoMsg}
    </div>
    <div style="clear:both;"> 
      <span class="optionsNomChamps">{t}Mot de passe{/t}</span>
      <span class="optionsValeurChamps"><input name="passwd" type="password" maxlength="20"  value="{$passwd}" /></span>
      {$erreurPasswdMsg} 
    </div>
    <div style="clear:both;">
      <span class="optionsNomChamps">{t}Retape ton mot de passe{/t}</span>
      <span class="optionsValeurChamps"><input name="passwd_check" type="password" maxlength="20"  value="{$passwd}" /></span>
      {$erreurPasswdCheckMsg} 
    </div>
<!--
    <br/>&nbsp;<br/>
    <div style="clear:both;">
        <span class="optionsNomChamps">{t}Tu es{/t}</span>
         <span class="optionsValeurChamps"><select name="sexe" style="width:100px;" ><option value="0" {if $sexe==0} selected="selected" {/if}>{t}un garçon{/t} </option><option value="1" {if $sexe>0} selected="selected" {/if}>{t}une fille{/t}</option></select></span>
    </div>
-->
    <br/>&nbsp;<br/>
    <div style="clear:both;"> 
        {t}Veux-tu recevoir les emails de Hamster Academy (permet l'accès au forum/tchat, bonus...){/t} : 
        <input type="checkbox" name="okPourEmail" value="1" 
        {if $okPourEmail == 1}
            checked="checked"
        {/if}
        onclick='
        if (this.checked)
            $("#champsEmail").show("slow");
        else
            $("#champsEmail").hide("slow");
        return true;'
        />
        <div style="clear:both; 
        {if $okPourEmail == 0}
        display:none;
        {/if}
        "
         id="champsEmail"> 
                
            <span class="optionsNomChamps">{t}Indique ton adresse mail{/t}</span>
            <span class="optionsValeurChamps"><input name="email" type="text" size="50" maxlength="150" value="{$email}" /></span>
            {if $erreurMailMsg neq ""}
                <br/>&nbsp;<br/>
                {$erreurMailMsg}
            {/if}
        </div>
    </div>
    <br/>&nbsp;<br/>
    <div style="clear:both;">
    {if $parrain_pseudo eq ""}
        <span class="optionsNomChamps" style="width:570px;font-size:9pt;">{t}Si tu as un parrain, indique son pseudo{/t} <input name="parrain_pseudo" type="text" size="25" maxlength="50"  value="{$parrain_pseudo}" style="font-size:9pt;" /></span>
    {else}
        <span class="optionsNomChamps" style="width:550px; ">{t}Ton parrain est{/t} {$parrain_pseudo}</span>
        <input type="hidden" name="parrain_pseudo" value="{$parrain_pseudo}" />
    {/if}
    </div>

    <br/>        
    <input type="hidden" name="presenceCharte" value="1" />
    <br/><table cellpadding="0" cellspacing="0"><tr valign="baseline"><td>{t}J'ai pris connaissance du{/t} <a href="charte.html" target="_blank">{t}règlement intérieur du jeu{/t}</a> {t}et je l'accepte sans réserve{/t} :&nbsp;&nbsp;</td><td><input type="checkbox" name="validCharte" value="1" 
    {if $charte == 1}
    checked="checked"
    {/if}
    /></td></tr></table>
    {$erreurCharteMsg}
    <div style="clear:both;">&nbsp;</div>
        
    <div style="margin:2em 0 0 15em;"><input type="submit" name="submitInscription" 
        value="{t}Finir l'inscription{/t} !"
        onclick="javascript: verif_carac_pseudo();" />
    </div>
</div>

</form>
    
<div style="clear:both;">&nbsp;</div>

<div align="center">
<a href="index.php" title="{t}Accueil{/t}">{t}Retour{/t}</a>
</div>
        
    
    </div>
    
    <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
    </div> 

  </div>

</div>

<div id='fb-root'></div>
    <!--  on charge la librairie javascript de facebook pour la connexion -->
{if $lang == "fr"}    
    <script src='http://connect.facebook.net/fr_FR/all.js'></script>
{else}
    <script src='http://connect.facebook.net/en_US/all.js'></script>
{/if}
     <!--  fonction qui envoie les informations à facebook connect -->
{literal}
    <script>
      FB.init({appId: 
{/literal}
      '{$cle_application_publique}'
{literal}
      , status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
        window.location.href="inscription.php?facebookPerms=1";
      });
    </script>
{/literal}