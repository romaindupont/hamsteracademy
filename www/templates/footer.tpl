<div align="center" class="footer" {if isset($pageWidth)}  style="width:{$pageWidth}px;" {/if} >
    <a href="{$base_site_url}{t}aide{/t}/doku.php?id=start">{t}Aide{/t}</a> 
    <!-- - <a href="{$base_site_url}partenaires.html">{t}Partenaires{/t}</a>--> 
    {if $lang=="fr"}- <a href="{$base_site_url}Apropos.html">{t}A propos{/t}</a> 
    - <a href="{$base_site_url}charteParents.html" target="_blank">{t}Contrôle parental{/t}</a>{/if} 
    
    <!-- - <a href="http://hamsteracademy.spreadshirt.net" target="_blank">{t}La Boutique Officielle{/t}</a>--> 
    - <a href="mailto:{$email_contact}">{t}Contact{/t}</a> 
    <!-- - <a href="{$base_site_url}bonus.html">Bonus</a>-->
    <!-- - <a href="{$base_site_url}autresjeux.php">{t}Jouer à d'autres jeux{/t}</a> -->
    <br/>    &nbsp;<br/>
    &copy; Hamster Academy - {t}Tous droits réservés{/t}
</div>

{if !defined('DOKU_INC') }
    </div> <!-- fin du mainPage -->
{/if}


{literal}
<script type="text/javascript">
var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
</script>
<script type="text/javascript">
try {
{/literal}
var pageTracker = _gat._getTracker("{$codeanalytics}");
{literal}
pageTracker._trackPageview();
} catch(err) {}</script>
{/literal}

{if !defined('DOKU_INC') }
    </body></html>
{/if}