<form method="post" action="options.php?mode=m_profil" enctype="multipart/form-data">

<div class="blocOptions">
<strong>{t}Mon profil :{/t}</strong><br/>&nbsp;<br/>

{t}Coche si tu veux changer de mot de passe :{/t} 
<input type="checkbox" name="okPourChangerMotDePasse" value="1" 
onclick='
if (this.checked)
    $("#chgtMotPasse").show("slow");
else
    $("#chgtMotPasse").hide("slow");
return true;'
/><br/>
<div id="chgtMotPasse" style="display:none;">
    <div style="clear:both;"> 
      <span class="optionsNomChamps">{t}Mot de passe :{/t} </span>
      <span class="optionsValeurChamps"><input name="passwd" type="text" maxlength="20" value="{$passwd}" /></span>
    </div>
      <div style="clear:both;"> 
      <span class="optionsNomChamps">{t}Retape ton mot de passe{/t}</span>
      <span class="optionsValeurChamps"><input name="passwd_check" type="text" maxlength="20"  value="{$passwd}" /></span>
      
      {if isset($msgPasswdErreur) }
        <span class="optionsValeurChampsErreur">{$msgPasswdErreur}</span>
      {/if}
      <br/>    
    </div>
</div>
<br/>
<div style="clear:both;"> 
        <span class="optionsNomChamps">{t}Adresse mail :{/t} </span>
        <span class="optionsValeurChamps"><input name="email" type="text" size="50" maxlength="150" value="{$email}" /></span>
</div>
<div style="clear:both;"> 
   <!--<span class="optionsNomChamps">{t}Retape ton adresse mail{/t}</span>
    <span class="optionsValeurChamps"><input name="email_check" type="text" size="50" maxlength="150" value="{$email}" /></span>
   -->
    {if $email_active == 0}
        <div style="clear:both;">({t}Mets une adresse valide, elle sera vérifiée{/t})</div>
    {/if}
    {if isset($msgMailErreur) }
        <span class="optionsValeurChampsErreur">{$msgMailErreur}</span>
    {/if}
</div>
<div style="clear:both;">
{if isset($msgmailActivation) }
    {$msgmailActivation}
{/if}
</div>
</div>
<br/>
<div class="blocOptions">
<div><strong>{t}Ma carte d'identité :{/t}</strong></div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel">{t}Date de naissance :{/t} </span>
    <span class="optionsValeurChampsOptionnel">
    {$txtDateNaissance}
    </span>
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Tu es un(e) :{/t} </span>
    <span class="optionsValeurChampsOptionnel"><select name="sexe" >
    <option value="0" {if ($sexe==0)} selected="selected"{/if} >{t}garçon{/t}</option>
    <option value="1" {if ($sexe>0) } selected="selected"{/if} >{t}fille{/t}</option></select>
    </span>
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Ton pays :{/t} </span>
    <span class="optionsValeurChampsOptionnel">
    <img src="images/flags/{$flag_image}" style="vertical-align:middle;" alt="" />
    <select name="pays">
       {$lst_pays}
    </select>
    </span>
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Ta langue :{/t}</span>
    {if $lang == "fr"}
    <span class="optionsValeurChampsOptionnel"><input name="langue" type="hidden" value="fr" />Francais</span>
    {else}
    <span class="optionsValeurChampsOptionnel"><input name="langue" type="hidden" value="en" />English</span>
    {/if}
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Ta ville :{/t} </span><span class="optionsValeurChampsOptionnel"><input name="ville" type="text" size="25" maxlength="25"  value="{$ville}" /></span>
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Ton blog :{/t} </span><span class="optionsValeurChampsOptionnel"><input name="blog" type="text" size="50" maxlength="50"  value="{$blog}" /></span>
</div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel">{t}Qui suis-je :{/t} </span><br/><span class="optionsNomChampsOptionnel"><textarea name="presentation" rows="5" cols="60">{$presentation}</textarea></span>
</div>
</div>
<br/>
<div class="blocOptions">
<div><strong>{t}Avatars :{/t}</strong></div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel">{t}Mon avatar (ma photo) (indispensable pour la mission 9) :{/t} </span><br/>
    {$lien_avatar}
    
    <table class="optionsNomChampsOptionnel">
    <tr><td style="font-size:small;">{t}Choisir un nouvel avatar :{/t} </td>
    <td><input type="file" name="fichierImage" size="50" /></td>
    <td><input type="submit" name="submitImage" value="{t}Envoyer la nouvelle image{/t}" /></td></tr></table>
    <div class="optionsNomChampsOptionnel"><span style="font-size:small;">{t}(nb : l'image doit faire au plus 100 ko et sa taille ne doit pas dépasser 100*100 pixels{/t}</span>)</div>
    <div class="optionsNomChampsOptionnel"><span style="font-size:small;">{t}(bon à savoir : une image dans son profil rapporte 10% de points en plus !{/t}</span>)</div>
</div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel">{t}La photo de mon hamster ou de mon animal (ou de mon dessin pour le concours) :{/t} </span>
    {$oeuf7}
    <br/>
    {if isset($image_bonus)}
        {$image_bonus}
    {/if}

    <table class="optionsNomChampsOptionnel">
    <tr><td style="font-size:small;">{t}Choisir une nouvelle photo :{/t} </td>
    <td><input type="file" name="fichierImageBonus" size="50" /></td>
    <td><input type="submit" name="submitImageBonus" value="{t}Envoyer la nouvelle image{/t}" /></td></tr></table>
    <div class="optionsNomChampsOptionnel"><span style="font-size:small;">{t}(nb : l'image doit faire au plus 200 ko et sa taille ne doit pas dépasser 640*480 pixels){/t}</span></div>
    <div class="optionsNomChampsOptionnel"><span style="font-size:small;">{t}(nb : pour le concours : si ça ne marche pas, envoie l'image par email à contact@hamsteracademy.fr en précisant ton pseudo){/t}</span></div>
</div>
</div>
<br/>
<div class="blocOptions">
<div><strong>{t}Autres options :{/t} </strong></div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel"><input type="hidden" name="liste_rouge_chk" value="1" /><input type="checkbox" name="liste_rouge" {if ($liste_rouge == 1)} checked="checked"{/if} /></span><span class="optionsNomChampsOptionnel">{t}Etre en liste rouge (pas d'affichage dans l'annuaire){/t}</span>
</div>
<div>
    <span class="optionsNomChampsOptionnel"><input type="hidden" name="montrer_profil_chk" value="1" /><input type="checkbox" name="montrer_profil" {if ($montrer_profil == 1)} checked="checked"{/if} /></span><span class="optionsNomChampsOptionnel">{t}Montrer mon profil aux autres joueurs{/t}</span>
</div>
<div>
    <span class="optionsNomChampsOptionnel"><input type="hidden" name="prevenir_nouveau_message_chk" value="1" /><input type="checkbox" name="prevenir_nouveau_message" {if ($prevenir_nouveau_message == 1)} checked="checked"{/if} /></span><span class="optionsNomChampsOptionnel">{t}Me prévenir par mail en cas de nouveau message{/t}</span>
</div>
</div>

<div class="blocOptions">
<div><strong>{t}Partage :{/t} </strong></div>
<br/>
<div>
    <span class="optionsNomChampsOptionnel">{t}Voir mon profil tel qu'il est vu par les autres personnes :{/t} {$lienProfil}</span>
</div>
<div>
    <span class="optionsNomChampsOptionnel">{t}Lien de mon profil (à insérer dans ton blog, mail, signature, etc.) :{/t} <input type="text" name="lien_profil" size="60" value="{$lienVersProfil}" readonly="readonly" onclick="javascript:this.form.lien_profil.focus();this.form.lien_profil.select();" style="background:#fdde7b;" /></span>
</div>
</div>

<div style="clear:both;"></div>

<div style="margin:2em 0 0 15em;">
<input type="submit" name="submitProfil" 
    value="{t}Enregistrer le profil{/t}" />
</div>
</form>