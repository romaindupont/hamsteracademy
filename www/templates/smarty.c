/* ./annuaire.tpl */
gettext("Annuaire de Hamster Academy");

/* ./annuaire.tpl */
gettext("Tu cherches un mâle pour ton hamster");

/* ./annuaire.tpl */
gettext("Rechercher par pseudo de joueur : ");

/* ./annuaire.tpl */
gettext("Chercher !");

/* ./annuaire.tpl */
gettext("Rechercher par nom de hamster : ");

/* ./annuaire.tpl */
gettext("Chercher !");

/* ./annuaire.tpl */
gettext("ne voir que les hamsters mâles");

/* ./annuaire.tpl */
gettext("Pseudo du joueur");

/* ./annuaire.tpl */
gettext("son hamster");

/* ./annuaire.tpl */
gettext("ajouter à mes amis");

/* ./annuaire.tpl */
gettext("Avoir un bébé avec cet hamster mâle");

/* ./chezleveto.tpl */
gettext("Ton hamster est gravement malade, il a été envoyé chez le vétérinaire");

/* ./chezleveto.tpl */
gettext("Les soins intensifs coutent cher");

/* ./chezleveto.tpl */
gettext("pour le récupérer");

/* ./chezleveto.tpl */
gettext("Deux possibilités");

/* ./chezleveto.tpl */
gettext("Récupérer l'hamster maintenant");

/* ./chezleveto.tpl */
gettext("Payer le vétérinaire");

/* ./chezleveto.tpl */
gettext("Ou attendre encore quelques jours");

/* ./chezleveto.tpl */
gettext("Attendre");

/* ./chezleveto.tpl */
gettext("Mais attention, le véto ne garde les hamsters que 14 jours. Passé ce delai, il les envoie à la SPH et tu ne pourras plus récupérer ton hamster... Il te reste");

/* ./chezleveto.tpl */
gettext("jour");

/* ./chezleveto.tpl */
gettext("pour le récupérer");

/* ./footer.tpl */
gettext("Partenaires");

/* ./footer.tpl */
gettext("CGU");

/* ./footer.tpl */
gettext("Charte du forum/tchat");

/* ./footer.tpl */
gettext("Contrôle parental");

/* ./footer.tpl */
gettext("Aide");

/* ./footer.tpl */
gettext("Contact");

/* ./footer.tpl */
gettext("Jouer à d'autres jeux");

/* ./footer.tpl */
gettext("Tous droits réservés");

/* ./formulaireNouvelHamster.tpl */
gettext("Choisis un hamster que tu veux élever");

/* ./formulaireNouvelHamster.tpl */
gettext("Yeux");

/* ./formulaireNouvelHamster.tpl */
gettext("Pelage");

/* ./formulaireNouvelHamster.tpl */
gettext("Sélectionner");

/* ./formulaireNouvelHamster.tpl */
gettext("Accessible au niveau");

/* ./formulaireNouvelHamster.tpl */
gettext("Choisis maintenant son prénom, son sexe et ses deux principaux caractères (2 choix au maximum)");

/* ./formulaireNouvelHamster.tpl */
gettext("Prénom du hamster");

/* ./formulaireNouvelHamster.tpl */
gettext("Sexe du hamster");

/* ./formulaireNouvelHamster.tpl */
gettext("mâle");

/* ./formulaireNouvelHamster.tpl */
gettext("femelle");

/* ./formulaireNouvelHamster.tpl */
gettext("Caractères");

/* ./formulaireNouvelHamster.tpl */
gettext("coquet");

/* ./formulaireNouvelHamster.tpl */
gettext("fort");

/* ./formulaireNouvelHamster.tpl */
gettext("résistant");

/* ./formulaireNouvelHamster.tpl */
gettext("sympa avec tout le monde");

/* ./formulaireNouvelHamster.tpl */
gettext("dragueur");

/* ./formulaireNouvelHamster.tpl */
gettext("bâtisseur");

/* ./index.tpl */
gettext("Le meilleur jeu virtuel d'élevage de hamsters !");

/* ./index.tpl */
gettext("Le compte a été supprimé");

/* ./index.tpl */
gettext("Ce compte a été temporairement suspendu");

/* ./index.tpl */
gettext("jusqu'au");

/* ./index.tpl */
gettext("Tu peux consulter Hamster Academy pour savoir pourquoi");

/* ./index.tpl */
gettext("Erreur inconnue");

/* ./index.tpl */
gettext("Pseudo");

/* ./index.tpl */
gettext("Ce pseudo n'existe pas");

/* ./index.tpl */
gettext("Mot de passe");

/* ./index.tpl */
gettext("Mot de passe");

/* ./index.tpl */
gettext("Mot de passe incorrect");

/* ./index.tpl */
gettext("Si tu l'as oublié, clique sur le lien");

/* ./index.tpl */
gettext("retrouver mon mot de passe");

/* ./index.tpl */
gettext("retrouver mon mot de passe");

/* ./index.tpl */
gettext("Mot de passe perdu");

/* ./index.tpl */
gettext("Pas encore inscrit");

/* ./index.tpl */
gettext("Rejoins plus de 40 000 joueurs dont");

/* ./index.tpl */
gettext("connectés en ce moment même ! C'est gratuit");

/* ./index.tpl */
gettext("S'inscrire, c'est gratuit");

/* ./index.tpl */
gettext("S'inscrire, c'est gratuit");

/* ./index.tpl */
gettext("Entrer dans Hamster Academy");

/* ./index.tpl */
gettext("Entrer dans Hamster Academy");

/* ./index.tpl */
gettext("Hamster Academy, jeu d'élevage virtuel de hamsters");

/* ./index.tpl */
gettext("Un jeu complètement Hamster'Ouf !!");

/* ./index.tpl */
gettext("Jeux partenaires à visiter");

/* ./index.tpl */
gettext("Les nouveautés");

/* ./index.tpl */
gettext("nouveautés");

/* ./index.tpl */
gettext("Clique-ici pour voir toutes les dernières nouveautés du jeu");

/* ./index.tpl */
gettext("Toutes les autres nouveautés...");

/* ./index.tpl */
gettext("Autour du jeu");

/* ./index.tpl */
gettext("Clique sur la boîte pour y mettre une remarque ou une idée");

/* ./index.tpl */
gettext("Clique sur la boîte pour y mettre une remarque ou une idée");

/* ./index.tpl */
gettext("Boîte à idée");

/* ./index.tpl */
gettext("Clique sur la boîte à idée pour faire part de tes remarques et idées sur le jeu");

/* ./index.tpl */
gettext("Les règles du jeu");

/* ./index.tpl */
gettext("règles du jeu");

/* ./index.tpl */
gettext("Les règles du jeu");

/* ./index.tpl */
gettext("Les règles du jeu");

/* ./index.tpl */
gettext("astuces et de bonnes idées pour bien avancer dans le jeu");

/* ./index.tpl */
gettext("Forum du jeu");

/* ./index.tpl */
gettext("Forum du jeu");

/* ./index.tpl */
gettext("Le Forum du jeu");

/* ./index.tpl */
gettext("Astuces, amis, musique, solutions, jeux, blagues...");

/* ./index.tpl */
gettext("Jeux virtuels gratuits");

/* ./index.tpl */
gettext("Jeux virtuels gratuits");

/* ./index.tpl */
gettext("Jeux virtuels gratuits");

/* ./index.tpl */
gettext("Jeux virtuels gratuits");

/* ./index.tpl */
gettext("d'autres jeux autour des hamsters, conseillés par Hamster Academy ! A découvrir");

/* ./index.tpl */
gettext("Nos partenaires");

/* ./index.tpl */
gettext("Jeux partenaires à visiter");

/* ./inscription.tpl */
gettext("Inscription à Hamster Academy");

/* ./inscription.tpl */
gettext("L'inscription n'est pas encore finie, il faut corriger les problèmes ci-dessous");

/* ./inscription.tpl */
gettext("Bienvenue dans l'inscription de Hamster Academy");

/* ./inscription.tpl */
gettext("Choisis ton pseudo et mot de passe");

/* ./inscription.tpl */
gettext("Ton pseudo d'éleveur");

/* ./inscription.tpl */
gettext("Mot de passe");

/* ./inscription.tpl */
gettext("Adresse mail");

/* ./inscription.tpl */
gettext("Si tu as un parrain, indique son pseudo");

/* ./inscription.tpl */
gettext("Ton parrain est");

/* ./inscription.tpl */
gettext("Tu es");

/* ./inscription.tpl */
gettext("un garçon");

/* ./inscription.tpl */
gettext("une fille");

/* ./inscription.tpl */
gettext("J'ai pris connaissance du");

/* ./inscription.tpl */
gettext("règlement intérieur du jeu");

/* ./inscription.tpl */
gettext("et je l'accepte sans réserve");

/* ./inscription.tpl */
gettext("Finir l'inscription");

/* ./inscription_ok.tpl */
gettext("Félicitations");

/* ./inscription_ok.tpl */
gettext("inscription terminée");

/* ./inscription_ok.tpl */
gettext("L'inscription est finie");

/* ./inscription_ok.tpl */
gettext("Tu commences le jeu avec");

/* ./inscription_ok.tpl */
gettext("une cage");

/* ./inscription_ok.tpl */
gettext("Cage d'inscription");

/* ./inscription_ok.tpl */
gettext("un biberon");

/* ./inscription_ok.tpl */
gettext("une écuelle");

/* ./inscription_ok.tpl */
gettext("portions d'aliments");

/* ./inscription_ok.tpl */
gettext("Tu peux jouer dès maintenant !");

/* ./inscription_ok.tpl */
gettext("En cliquant ici");

/* ./inscription_ok.tpl */
gettext("jouer !");

