{if $lang == "en"}
{literal}
<style content="text/css">
.LN-indexHead {
    background: transparent url('../images/hamster_index-head_en.gif') top left no-repeat;
}
</style>
{/literal}
{/if}

{if isset($msgLangue)}
<div  style="background:orange; float:top; margin-top:40px; font-size:10pt;" >
    <div style="width:750px; margin-right:auto;margin-left:auto;">
        {$msgLangue}
    </div>
</div>
{/if}
    
<div class="LN-fond-indexHead">

  <div class="LN-indexHead">
  
    <div class="LN-logoImage">    
      <img src="images/logoHamsterAcademy-new.gif" alt="Hamster Academy" />
    </div>

    <div class="LN-indexLogin">
       {if $erreur == 3}
        <div class="txtErreur">{t}Le compte a été supprimé{/t}</div><br/>
       {elseif $erreur == 5}
        <div class="txtErreur">{t}L'ip a été bannie. Contacte Hamster Academy pour savoir pourquoi.{/t}</div><br/>
       {elseif $erreur == 4}
           <div class="txtErreur">{t}Ce compte a été temporairement suspendu{/t}. 
           {if $finSuspension > 1}
           &nbsp;({t}jusqu'au{/t} {$finSuspension|date_format:"%d/%m/%Y %H:%M"})
           {/if}.<br/>{t}Tu peux consulter Hamster Academy pour savoir pourquoi{/t}.</div><br/>
       {elseif $erreur == 6}
            <div style="text-align:left;">{t}Tu dois d'abord associer ton compte avec Facebook{/t}.
            <br/>{t}1) connecte-toi normalement{/t}
            <br/>2) {t}puis va dans "Mon Compte->Facebook"{/t} 
            </div>
       {/if}
       
       <form action="jeu.php" method="post" name="ha_login">
          <input type="hidden" name="login" value="1" />
          <input type="hidden" name="page" value="jeu" />
        {if $erreur > 6}
            <br/><div class=txtErreur>{t}Erreur inconnue{/t}.</div>
        {/if}
        <table style="border:0 ;" cellspacing="0" cellpadding="5">
          <tr>
            <td>
                <table border="0" cellpadding="2" align="center">
                <tr> 
                  <td>{t}Pseudo{/t} :</td>
                  <td><input type="text" name="htPseudo" alt="pseudo"/>
                  {if $erreur == 2} 
                    <br/><div class="txtErreur">{t}Ce pseudo n'existe pas{/t}.</div>{/if}
                  </td>
                </tr>
                <tr valign="top"> 
                  <td>{t}Mot de passe{/t} : </td>
                  <td><input type="password" name="htPasswd" alt="{t}Mot de passe{/t}" onkeydown="
                          {literal}
                          javascript: 
                          if (event.which == 13 || event.keyCode == 13) {document.forms.ha_login.submit();
                            return false;
                           } 
                          {/literal}
                         "/>
                        {if $erreur == 1} 
                            <br/><div class="txtErreur">{t}Mot de passe incorrect{/t} !<br/>&nbsp;<br/>{t}Si tu l'as oublié, clique sur le lien{/t} :<br/><a href="motdepasseperdu.php" class="txtErreur">{t}retrouver mon mot de passe{/t} !<a/><br/>&nbsp;<br/></div>
                        {else}
                            <br/><div style="font-size:9pt;margin-bottom:5px;"><a href="motdepasseperdu.php" title="{t}retrouver mon mot de passe{/t} !">{t}Mot de passe perdu{/t} ?</a></div>
                        {/if}
                        </td>
                </tr>
                {if $erreur != 6}
                <tr>
                <td colspan="2">
                {$connexion_facebook}
                </td>
                </tr>
                {/if}
              </table>
              <div align="center"><script type="text/javascript">
                      {literal}
                      //<![CDATA[
                                if ( ! navigator.cookieEnabled ) {
                                    document.write( "<span class='txtErreur'>Attention ! Ton navigateur n'accepte pas les cookies.<br/>Les cookies sont necessaires pour jouer.<\/span><br\/>" );
                                }
                                //]]>
                      {/literal}
                  </script>
              </div>
              </td>
          </tr>
        </table>
       </form>
    </div>
    
    <div class="LN-indexGo">
    {if $maintenance == true}
        {if $lang == "fr"}
        <strong>Jeu en maintenance pour la journée du mercredi. Désolé pour l'interruption.</strong>
        {else}
        <strong>The game is temporarily closed today for maintenance operations (wednesday). Sorry for any inconvenience.</strong>
        {/if}
        <!--
    {/if}
        <a href="jeu.php" title="{t}Entrer dans Hamster Academy{/t} !" onclick="javascript:document.forms.ha_login.submit(); return false;" ><img src="images/index/go-unlight.png" alt="{t}Entrer dans Hamster Academy{/t}" style="width:150px; height:148px;" onmouseover="javascript:this.src='images/index/go-light.png'; return false;" onmouseout="javascript:this.src='images/index/go-unlight.png'; return false;" /></a>
    {if $maintenance == true}
    -->
    {/if}
    </div>
    
    
    <div class="LN-indexPasInscrit">
      <br /><span class="LN-indexPasInscrit-gros">{t}Pas encore inscrit{/t} ?</span><br />
      {t}Rejoins plus de 40 000 joueurs dont{/t} {$nbConnectes} {t}connectés en ce moment même{/t} !<br />
      <span class="LN-indexPasInscrit-gros">{t}C'est gratuit{/t} !</span>
      
      </div>    
    
    <div class="LN-indexSinscrire">
      <a href="{t}inscription.php{/t}" 
title="{t}S'inscrire, c'est gratuit{/t} !"><img 
src="images/inscription_{$lang}-unlight.png" alt="S{t}S'inscrire, c'est gratuit{/t} !" onmouseover="javascript:this.src='images/inscription_{$lang}-light.png'; return 
false;" onmouseout="javascript:this.src='images/inscription_{$lang}-unlight.png'; 
return false;"></a>
    </div>  
    
    <div class="LN-lemeilleurjeu">    
        {t}Le meilleur jeu virtuel d'élevage de hamsters !{/t}
    </div>
    
    <div class="LN-photos-login">    
        <img src="images/photos-login.png" alt="Hamster Academy" />
    </div>
    
  </div>

</div>





<div class="LN-fond-indexBottom">

  <div class="LN-indexBottom">

    <div class="LN-indexbottom-col-gauche">
      
      <div class="cadreArrondi-top">
        <div class="cadreArrondi-top-left"></div>
        <div class="cadreArrondi-top-right"></div>
        <div class="cadreArrondi-top-center">{t}Les nouveautés{/t} !</div>        
      </div>
      <div class="cadreArrondi-contenu">
          <table border="0">
            <tr valign="top">
              <td width="300" align="left">{$nouveautes}
               <div style="font-size:small; font-weight:bold;"><a href="index.php?toutesnouveautes=1" title="{t}Clique-ici pour voir toutes les dernières nouveautés du jeu{/t} !">{t}Toutes les autres nouveautés...{/t}</a></div>
               </td>
            </tr>
          </table>
      </div>      
      <div class="cadreArrondi-bottom">
        <div class="cadreArrondi-bottom-left"></div>
        <div class="cadreArrondi-bottom-right"></div>
        <div class="cadreArrondi-bottom-center"></div>        
      </div> 
      
    </div>
    
    <div class="LN-indexbottom-col-droite">
    
      <div class="LN-index-autour-du-jeu">
        <div class="cadreArrondi-top">
          <div class="cadreArrondi-top-left"></div>
          <div class="cadreArrondi-top-right"></div>
          <div class="cadreArrondi-top-center">{t}Autour du jeu{/t}</div>        
        </div>     
        <div class="cadreArrondi-contenu">
        {literal}
            <!--<script type="text/javascript" src="http://static.ak.connect.facebook.com/connect.php/en_US"></script> 
            <script type="text/javascript">FB.init("6693502097");</script> 
            -->
            <fb:fan profile_id="6693502097" stream="0" connections="10" logobar="0" width="570" height="140" css="http://www.hamsteracademy.fr/css/fb.css?3" ></fb:fan>
        {/literal}
        <br/>&nbsp;<br/>
         <table cellpadding="0">
          <tr>
            <td align="center" width="100"><a href="{t}aide/start{/t}" title="{t}Les règles du jeu{/t}"><img src="images/regles.gif" alt="{t}règles du jeu{/t}" height="40" /></a></td>
            <td width="470" align="left"><a href="{t}aide/start{/t}" title="{t}Les règles du jeu{/t}"><span style="font-size:9pt;color:black;"><strong>{t}Les règles du jeu{/t}</strong> : </span></a><span style="font-size:9pt;">{t}astuces et de bonnes idées pour bien avancer dans le jeu{/t}.</span></td>
          </tr>
        </table>
         <table cellpadding="0">
          <tr>
            <td align="center" width="100"><a href="forum/index.php" title="{t}Forum du jeu{/t}"><img src="images/forum.gif" alt="r&egrave;gles du jeu" height="40" /></a></td>
            <td width="470" align="left"><a href="forum/index.php" title="{t}Forum du jeu{/t}"><span style="font-size:9pt;color:black;"><strong>{t}Le Forum du jeu{/t}</strong> : </span></a><span style="font-size:9pt;">{t}Astuces, amis, musique, solutions, jeux, blagues...{/t}</span></td>
          </tr>
        </table>
        <table cellpadding="0">
          <tr>
            <td align="center" width="100"><a href="autresjeux.php" title="{t}Jeux virtuels gratuits{/t}"><img src="jeux/hamtaro.jpg" alt="{t}Jeux virtuels gratuits{/t}" height="40" /></a></td>
            <td width="470" align="left"><a href="autresjeux.php" title="{t}Jeux virtuels gratuits{/t}"><span style="font-size:9pt;color:black;"><strong>{t}Jeux virtuels gratuits{/t}</strong></span></a><span style="font-size:9pt;">: {t}d'autres jeux autour des hamsters, conseillés par Hamster Academy ! A découvrir{/t} !</span></td>
          </tr>
        </table>
        </div>      
        <div class="cadreArrondi-bottom">
          <div class="cadreArrondi-bottom-left"></div>
          <div class="cadreArrondi-bottom-right"></div>
          <div class="cadreArrondi-bottom-center"></div>        
        </div>
      
      </div>
    
      <div class="LN-indexJeu">
      {if $lang == "fr"}
      <div class="LN-indexPartenairesHead">
      {t}Jeux partenaires à visiter{/t}<br /><!--<script language='JavaScript' src='http://www.clicjeux.net/banniere.php?id=1045'></script>-->
      </div> 
      {/if}
      </div>
      
      <div class="LN-index-cadres-bottom">      
        <div class="topeleveurs">
          <div class="cadreArrondi-top">
            <div class="cadreArrondi-top-left"></div>
            <div class="cadreArrondi-top-right"></div>
            <div class="cadreArrondi-top-center">{t}Top 5 des éleveurs{/t}</div>        
          </div>     
          <div class="cadreArrondi-contenu">
           {foreach from=$lstTop5Joueurs item=joueur}
           {$joueur}
           {/foreach}
          </div>      
          <div class="cadreArrondi-bottom">
            <div class="cadreArrondi-bottom-left"></div>
            <div class="cadreArrondi-bottom-right"></div>
            <div class="cadreArrondi-bottom-center"></div>        
          </div>
        </div>
        
        <div class="eleveurhasard">
          <div class="cadreArrondi-top">
            <div class="cadreArrondi-top-left"></div>
            <div class="cadreArrondi-top-right"></div>
            <div class="cadreArrondi-top-center">{t}Un éleveur au hasard{/t}</div>        
          </div>     
          <div class="cadreArrondi-contenu">
           {$joueurAleatoire}
          </div>      
          <div class="cadreArrondi-bottom">
            <div class="cadreArrondi-bottom-left"></div>
            <div class="cadreArrondi-bottom-right"></div>
            <div class="cadreArrondi-bottom-center"></div>        
          </div>        
        </div>        
      </div>
      <div style="clear:both;">&nbsp;</div> 
      <div class="topeleveurs" style="width:300px;">
          <div class="cadreArrondi-top">
            <div class="cadreArrondi-top-left"></div>
            <div class="cadreArrondi-top-right"></div>
            <div class="cadreArrondi-top-center">{t}Top 5 des groupes de zik{/t}</div>        
          </div>     
          <div class="cadreArrondi-contenu">
           {foreach from=$lstTop5groupesMusiques item=groupe}
           {$groupe}
           {/foreach}
          </div>      
          <div class="cadreArrondi-bottom">
            <div class="cadreArrondi-bottom-left"></div>
            <div class="cadreArrondi-bottom-right"></div>
            <div class="cadreArrondi-bottom-center"></div>        
          </div>
        </div>
        <div class="eleveurhasard" style="width:300px;">
          <div class="cadreArrondi-top">
            <div class="cadreArrondi-top-left"></div>
            <div class="cadreArrondi-top-right"></div>
            <div class="cadreArrondi-top-center">{t}Top 5 des équipes de foot{/t}</div>        
          </div>     
          <div class="cadreArrondi-contenu">
           {foreach from=$lstTop5equipesFoot item=equipe}
           {$equipe}
           {/foreach}
          </div>      
          <div class="cadreArrondi-bottom">
            <div class="cadreArrondi-bottom-left"></div>
            <div class="cadreArrondi-bottom-right"></div>
            <div class="cadreArrondi-bottom-center"></div>        
          </div>        
        </div>        
      </div>
    
    <div style="clear:both;">&nbsp;</div>
  
  </div>

</div>

{if $lang == "fr"}
<div align="center" class="LN-footer_partenaires"><strong>{t}Nos partenaires{/t} : </strong>

    <a href="http://www.jeuxvirtuels.com" target="_blank">JeuxVirtuels.com</a> &middot;
    <a href="http://www.moutonking.com?ref=2294" target="_blank" title="Mouton-King">Moutonking</a> &middot;   
    <a href="http://mitsou.oldiblog.com/" target="_blank">Les jeux d'élevage virtuel</a> &middot;
    <a href="http://www.meilleursjeux.net" target="_blank" title="Jeux gratuits">Jeux gratuits</a> &middot;
{* partenaires désactivés
    <a href= "http://www.poneyvallee.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=1&amp;sp=98'" title="Poney Vallee - Elevez votre poney" target="_blank">Poney Vallee</a> &middot;
    <a href= "http://www.ohmydollz.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=2&amp;sp=98'" title="Oh My Dollz - jeu de mode" target="_blank">OhMyDollz</a> &middot;
    <a href= "http://www.elevezundragon.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&amp;id=387&amp;idJ=5&amp;sp=98'" title="Elevez votre dragon !" target="_blank">Elevez un Dragon</a> &middot;    
    <a target="_blank" href="http://www.jeuxjeuxjeux.fr">Jeux</a> &middot;
    <a href="http://www.todoojeux.com" target="_blank">Todoo Jeux</a> &middot;
    <a href="http://www.desjeuxgratuits.fr" target="_blank" title="Jeux gratuits">Jeux Gratuits</a> &middot;
    <a href="http://www.lesjeuxvirtuels.com" target="_blank">Jeux Virtuels</a> &middot;
    <a href="http://www.virtuoflash.com" title="jeux gratuits">jeux gratuits</a> &middot;
    <a href="http://www.jeudunet.com/jeux-gratuits/6,jeux-d-elevage.html" title="jeux elevage" target="_blank">jeux elevage</a>&middot;
    <a href="http://www.jeuxy.com">jeux</a> &middot;
    <a href="http://www.annuairejeux.fr/categorie/12/Jeux-d_elevage" title="Jeux d'elevage" target="_blank">Jeux d'elevage</a> &middot;
    <a href="http://www.portaildesjeux.com" title="Jeux" target="_blank">Jeux gratuits</a> &middot;
    <a href="http://www.pochojeux.com"  title="jeux gratuits" target="_blank">Pochojeux</a>
    <a href="http://www.jeuxlol.com" title="jeux fille" target="_blank">jeux de fille</a> &middot;
*}
    <a href="http://www.jeu-virtuel.fr" target="_blank" title="Jeux virtuels : jeux, tests...">Jeux virtuels</a> &middot;
    <a href="http://www.jeu-gratuit.net" title="Jeux en ligne gratuits" target="_blank">Jeux gratuits</a> &middot;
    <a href="http://www.jeuxvideo-flash.com" title="Jeux Flash et Jeux sur internet" target="_blank">JeuxVideo-Flash</a> &middot;
    <a href="http://www.top-astuce.com" target="_blank" title="Jeux gratuit">Jeu Gratuit</a> &middot;
    <a href="http://www.sitacados.com"  title="Jeux gratuits" target="_blank">Jeux</a> &middot;
    <a href="http://www.jeux-en-ligne-gratuits.net" title="jeux en ligne gratuits">Jeux en ligne gratuits</a> &middot;
    <a href="http://www.cadomax.com/" target="_blank">Jeux et cadeaux</a> &middot;
    <a href="http://www.jeux-remuneres.com/" target="_blank" title="Jeux gratuits">Jeux en ligne</a> &middot;
    <a href="http://www.jeuxvirtuels.net/in.php?ids=344" target="_blank" title="JeuxVirtuels.net, L'annuaire des jeux virtuels gratuits">JeuxVirtuels.net</a> &middot;
    <a href="http://www.jeuxgratuit.org" title="jeux gratuits">jeux gratuits</a> &middot;
    
    <br/>&nbsp;<br/>
    <div align="center">
    {t}Jeux partenaires à visiter{/t} !
        <a href= "http://www.poneyvallee.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=1&sp=98'" title="poney et cheval virtuel" target="_blank"><img src='http://www.poneyvallee.com/pub/88.gif' border="0" alt="poney et cheval virtuel"/></a>
        <a href= "http://www.ohmydollz.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=2&sp=98'" title="jeu de filles" target="_blank"><img src='http://www.ohmydollz.com/pub/88.gif' border="0" alt="jeu de filles"/></a>
{* partenaires désactivés
        <a href= "http://www.elevezundragon.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=5&sp=98'" title="Elevage virtuel dragon" target="_blank"><img src='http://www.elevezundragon.com/img/eud88x31.gif' border="0" alt="Elevage virtuel dragon"/></a>
        <a href= "http://www.ilodino.com/" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=8&sp=98'" title="Elève tes dinosaures" target="_blank"><img src='http://www.ilodino.com/pub/88.gif' border="0"alt="Elève tes dinosaures"/></a>
        <a href= "http://www.piratefight.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=7&sp=98'" title="jeu virtuel pirate" target="_blank"><img src='http://partenaires.feerik.com/pub/pirate/88.jpg' border="0" alt="jeu virtuel pirate"/></a>
        <a href= "http://www.portesmonstrestresors.com" onclick="this.href='http://partenaires.feerik.com/inout.php?type=in&id=387&idJ=9&sp=98'" title="Portes Monstres Trésors" target="_blank"><img src='http://www.portesmonstrestresors.com/pub/88.gif' border="0" alt="Portes Monstres Trésors"/></a>
*}
        </div>
        
</div>
{else}
<div align="center" class="LN-footer_partenaires"><strong>{t}Nos partenaires{/t} : </strong>
    <a href="http://www.gamelinks.com">Free Online Games</a> &middot;
    <a href="http://www.besthamstersites.com">Hamster Directory</a>
</div>
{/if}

<div id='fb-root'></div>
    <!--  on charge la librairie javascript de facebook pour la connexion -->
{if $lang == "fr"}    
    <script src='http://connect.facebook.net/fr_FR/all.js'></script>
{else}
    <script src='http://connect.facebook.net/en_US/all.js'></script>
{/if}
     <!--  fonction qui envoie les informations à facebook connect -->
{literal}
    <script>
      FB.init({appId: 
{/literal}
      '{$cle_application_publique}'
{literal}
      , status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
        window.location.href="jeu.php?loginFacebook=1";
      });
    </script>
{/literal}