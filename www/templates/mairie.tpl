<div align="center">
	
	<div class="blocRounded" style="width:400px;">
	<h3>Carnet de mariages</h3>
	
	<div style="margin-bottom:1em; font-weight:bold;">Aujourd'hui : </div>
	    <div style="margin-bottom:1em; font-size:9pt;">
		<?php 
			$query = "SELECT date, texte FROM historique WHERE type = ".HISTORIQUE_MARIAGE." AND date > ".($dateActuelle-86400)." ORDER BY date DESC LIMIT 10" ; // 24 heures
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
			}
			$nbMariagesAujourdhui = $dbHT->sql_numrows($result) ;
			while($row=$dbHT->sql_fetchrow($result)) {
				echo "A ".date("G\hi", $row['date']).", ".$row['texte']."&nbsp;!<br/>";
			}
			$dbHT->sql_freeresult($result);

		echo "</div>";

		if ($nbMariagesAujourdhui < 10) {

			echo "<div style=\"margin-bottom:1em; font-weight:bold;\">Hier : </div>";
			echo "<div style=\"margin-bottom:1em; font-size:9pt;\">";

			$query = "SELECT date, texte FROM historique WHERE type = ".HISTORIQUE_MARIAGE." AND date < ".($dateActuelle-86400)." AND date > ".($dateActuelle-172800)." ORDER BY date DESC LIMIT 10" ; // 48 heures
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
			}
			while($row=$dbHT->sql_fetchrow($result)) {
				echo "A ".date("G\hi", $row['date']).", ".$row['texte']." !<br/>";
			}
			$dbHT->sql_freeresult($result);
	
		    echo "</div>";
	
		}
		?>

	</div>
	<div class="blocRounded" style="width:400px;">
	<h3>Carnet de naissances</h3>
	
	<div style="margin-bottom:1em; font-weight:bold;">Aujourd'hui : </div>
	<div style="margin-bottom:1em;  font-size:9pt;">
		<?php 
			$query = "SELECT date, texte FROM historique WHERE type = ".HISTORIQUE_NAISSANCE." AND date > ".($dateActuelle-86400)." ORDER BY date DESC LIMIT 10" ; // 24 heures
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
			}
			$nbMariagesAujourdhui = $dbHT->sql_numrows($result) ;
			while($row=$dbHT->sql_fetchrow($result)) {
				echo "A ".date("G\hi", $row['date']).", ".$row['texte']."&nbsp;!<br/>";
			}
			$dbHT->sql_freeresult($result);

			echo "</div>";

		if ($nbMariagesAujourdhui < 10) {
		
			echo "<div style=\"margin-bottom:1em; font-weight:bold;\">Hier : </div>";
			echo "<div style=\"margin-bottom:1em; font-size:9pt;\">";

			$query = "SELECT date, texte FROM historique WHERE type = ".HISTORIQUE_NAISSANCE." AND date < ".($dateActuelle-86400)." AND date > ".($dateActuelle-172800)." ORDER BY date DESC LIMIT 10" ; // 48 heures
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
			}
			while($row=$dbHT->sql_fetchrow($result)) {
				echo "A ".date("G\hi", $row['date']).", ".$row['texte']." !<br/>";
			}
			$dbHT->sql_freeresult($result);
			
			echo "</div>";
		}

		?>
        </div>
        <div style="clear:both;">&nbsp;</div>
        <div class="blocRounded" style="width:400px;">
            <img src="images/coupe.gif" alt="coupe" style="height:40px;"><br/>
		    <h2>Eleveurs les mieux notés : </h2>
            <div style="font-size:9pt;">
            <?php
                $nbJoueursPts = 10;
                if (isset($_GET['nbJoueursPts'])) {
                    $nbJoueursPts = intval($_GET['nbJoueursPts']);
                    if ($nbJoueursPts < 0)
                        $nbJoueursPts = 0;
                }
		        $query = "SELECT pseudo, note, joueur_id FROM joueurs WHERE joueur_id NOT IN (1,7,2,".HAMSTER_ACADEMY_ID.") ORDER BY note DESC LIMIT $nbJoueursPts";
		        if ( !($result = $dbHT->sql_query($query)) ){
			        message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
		        }
		        $i=1;
		        while($row=$dbHT->sql_fetchrow($result)) {
			        echo "<div>".$i.") <a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"Voir son profil\">".$row['pseudo']."</a> (".$row['note']." points)</div>";
			        $i++;
		        }
		        $dbHT->sql_freeresult($result);
            ?>
            <br/>
            <a href="jeu.php?mode=m_eleveurs&amp;univers=0&amp;nbJoueursPts=50">[Voir les 50 premiers]</a>
            </div>
        </div>
		
		<div class="blocRounded" style="width:400px;">
        <img src="images/coupe.gif" alt="coupe" style="height:40px;" /><br/>
		<h2>Eleveurs les plus riches : </h2>
        <div style="font-size:9pt;">
		<?php
            echo afficherObjet("oeufs",1);
            $nbJoueursPieces = 10;
            if (isset($_GET['nbJoueursPieces'])) {
                $nbJoueursPieces = intval($_GET['nbJoueursPieces']);
                if ($nbJoueursPieces < 0)
                    $nbJoueursPieces = 0;
            }
		    $query = "SELECT pseudo, nb_pieces, joueur_id FROM joueurs WHERE nb_pieces > 150 AND joueur_id NOT IN (1,7,2,".HAMSTER_ACADEMY_ID.") ORDER BY nb_pieces DESC LIMIT $nbJoueursPieces";
		    if ( !($result = $dbHT->sql_query($query)) ){
			    message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
		    }
		    $i=1;
		    while($row=$dbHT->sql_fetchrow($result)) {
			    echo "<div>".$i.") <a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"Voir son profil\">".$row['pseudo']."</a> (".$row['nb_pieces']." pièces)</div>";
			    $i++;
		    }
		    $dbHT->sql_freeresult($result);
		?>
        <br/>
        <a href="jeu.php?mode=m_eleveurs&amp;univers=0&amp;nbJoueursPieces=50">[Voir les 50 premiers]</a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
        <div class="blocRounded" style="width:800px;">
        <h2><?php echo T_("Aujourd'hui, c'est l'anniversaire de") ; ?> : </h2>
        <div style="font-size:9pt;">
        <?php
            $query = "SELECT pseudo, joueur_id, naissance_annee FROM joueurs WHERE (naissance_jour != 1 OR naissance_mois != 1 OR naissance_annee != 1940) AND naissance_jour = ".(date('j'))." AND naissance_mois = ".(date('n'))." ORDER BY pseudo ASC";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                $txt = $row['pseudo'].( ($row['naissance_annee'] == 1940)? "" : " (".(date('Y') - $row['naissance_annee'])." ".T_("ans").")");
                echo returnLienProfil($row['joueur_id'],$txt)." &middot; ";
            }
            $dbHT->sql_freeresult($result);
        ?>
        <br/>&nbsp;<br/>
        <a href="options.php?mode=m_profil"><?php echo T_("As-tu indiqué ta date de naissance ?");?></a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
        <div class="blocRounded" style="width:800px;">
        <h2><?php echo T_("Les joueurs VIP du mois de ").$mois_txt[$month] ; ?> : </h2>
        <div style="font-size:9pt;">
        <?php
            $query = "SELECT pseudo, joueur_id FROM joueurs WHERE vip = 1 ORDER BY pseudo ASC";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "<a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"".T_("Voir son profil")."\">".$row['pseudo']."</a> &middot; ";
            }
            $dbHT->sql_freeresult($result);
        ?>
        <br/>&nbsp;<br/>
        <a href="options.php?mode=m_vip"><?php echo T_("Pour tout savoir sur le status VIP, clique-ici !");?></a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
    </div>