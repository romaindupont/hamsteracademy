
<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">
  
      {if $msg neq ""}
        <div style="clear:both;">&nbsp;</div>
        {$msg}
        <br/>
      {/if}
      
      {if $txtActionsInventaire neq ""}
      <div class="blocRounded" style="width:820px;" align="center">
    
        <br />
        <img src="images/bac_reduc2.gif" alt="" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="titre-bloc-gros"><strong>{t}Actions dans l'inventaire{/t}</strong></span><br/>
        <br />
        {$txtActionsInventaire}
      </div>
    <div style="clear:both;">&nbsp;</div>
    {/if}
    
    <div class="blocRounded" style="width:820px;" align="center">
    <form action="jeu.php" method="post">
    <input type="hidden" name="mode" value="m_objet" />
    
    <br />
    <img src="images/bac_reduc2.gif" alt="" style="vertical-align:middle;" />&nbsp;&nbsp;<span class="titre-bloc-gros"><strong>{t}Dans ton bac fourre-tout :{/t}</strong></span><br/>
    <br />

    {foreach item=objet from=$lstObjetsFourreTout name=lstAcc}
        
        <div class="cornflex yellowbox" style="width:120px;">
                <div class="t">
                    <div class="c">
                    
                <div style="margin-bottom:15px;font-size:9pt;"><img src="images/{$objet.image}" alt="" style="vertical-align:middle; height:40px;" /> {if $lang=="fr"}{$objet.nom_fr}{else}{$objet.nom_en}{/if}<br/>&nbsp;<br/>
                  <div style="font-size:9pt;">
                  {t}Quantité{/t}: {$objet.quantite}<br/>            
                  {t}Sélection{/t}: <input type="checkbox" name="selectionObjets[]" value="{$objet.accessoire_id}" />
                </div>
                </div>
            </div>
            </div>                
            <div class="r"></div>
            <div class="b"></div>
            <div class="l"></div>
        </div>
        
        {if $smarty.foreach.lstAcc.iteration % 5  == 0}
            <div style="clear:both;">&nbsp;</div>
            {/if}
        {/foreach}
        <div style="clear:both;">&nbsp;</div>
        <div class="blocRounded" style="width:600px;">
        <div style="float:left; width:70px;"><br /><span class="titre-bloc-gros2"><strong>{t}Actions :{/t}</strong></span></div>
        <div style="float:left;"><br /><input type="submit" name="vendreObjets" value="{t}Vendre les objets sélectionnés (1 seule quantité à chaque fois){/t}" /><br/>
        <input type="submit" name="vendreTousObjets" value="{t}Vendre les objets sélectionnés (toutes les quantités){/t}" /><br/>
        {t}Déplacer dans{/t} : 
        <select name="deplacer_dans_cage_id" >
        {foreach item=cage from=$lstObjetsCage}
            <option value="{$cage.cage_id}">{$cage.nom}</option>
        {/foreach}
        </select>
        <input type="submit" name="deplacerObjets" value="ok" /><br/>
        {t}Donner à mon ami{/t} : 
        <select name="ami_id" >
        {foreach item=ami from=$lstAmis}
            <option value="{$ami.joueur_id}">{$ami.pseudo}</option>
        {/foreach}
        </select>
        {t}Quantité{/t} : <input type="text" size="2" name="quantiteDonObjet" value="1" />
        <input type="submit" name="donnerAAmis" value="ok" />
        </div>
        </div>
    </form>
    </div>
    <div style="clear:both;">&nbsp;</div>
    
    
    {foreach item=objetCage from=$lstObjetsCage}

    <div class="blocRounded" style="width:820px;" align="center">
        <form action="jeu.php" method="post">
        <input type="hidden" name="mode" value="m_objet" />
        <input type="hidden" name="precedenteCage_id" value="{$objetCage.cage_id}" />
        <br />
        <span class="titre-bloc-gros"><strong>{t}Dans la cage{/t} {$objetCage.nom} :</strong></span> <br/>
        <br />
        {foreach item=objet from=$objetCage.listeAccessoires name=lstAccCage}
        
            <div class="cornflex yellowbox" style="width:120px;">
                <div class="t">
                    <div class="c">
                        <div style="margin-bottom:15px;font-size:9pt;"><img src="images/{$objet.image}" alt="" style="vertical-align:middle; height:40px;" /> {if $lang=="fr"}{$objet.nom_fr}{else}{$objet.nom_en}{/if}<br/>&nbsp;<br/>
                        <div style="font-size:9pt;">
                        {t}Sélection{/t} : <input type="checkbox" name="selectionObjets[]" value="{$objet.accessoire_id}" />
                        </div>
                        </div>
                    </div>
                </div>                
                <div class="r"></div>
                <div class="b"></div>
                <div class="l"></div>
            </div>
            {if $smarty.foreach.lstAccCage.iteration % 5  == 0}
            <div style="clear:both;">&nbsp;</div>
            {/if}
        {/foreach}
        <div style="clear:both;">&nbsp;</div>
        <div class="blocRounded" style="width:600px;">
            <div style="float:left; width:70px;"><br /><span class="titre-bloc-gros2"><strong>{t}Actions :{/t}</strong></span> </div>
            <div style="float:left;"><br /><input type="submit" name="vendreObjets" value="{t}Vendre les objets sélectionnés{/t}" /><br/>
            {t}Déplacer dans{/t} : 
            <select name="deplacer_dans_cage_id" >            
            <option value="-1">{t}le bac fourre-tout{/t}</option>
            {foreach item=cage from=$objetCage.lstAutresCages}
                <option value="{$cage.cage_id}">{$cage.nom}</option>
            {/foreach}
            </select>
            <input type="submit" name="deplacerObjets" value="ok" />
            </div>
        </div>
        </form>
    </div>
    <div style="clear:both;">&nbsp;</div>
{/foreach}

{$ingredientNoisette}


  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>
