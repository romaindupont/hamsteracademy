{literal}
<link href="http://www.hamsteracademy.fr/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
$().ready(function() {
    $("#searchPseudo").autocomplete("listePseudos.php", {
        minChars: 0,
        width: 200,
        max: 100,
        selectFirst: false
    });

});
</script>
{/literal}

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full" style="text-align:center;">
  
  <h2>{t}Annuaire de Hamster Academy{/t}</h2>

{if $action == "avoirunbebe"}{t}Tu cherches un mâle pour ton hamster{/t} {$nomHamsterConcerne} ! <br/>&nbsp;<br/>{/if}

<form action="jeu.php" method="get">
<input type="hidden" name="mode" value="m_annuaire" />
{if $action == "avoirunbebe"}
<input type="hidden" name="action" value="{$action}" />
<input type="hidden" name="hamster_id_concerne" value="{$hamster_id_concerne}" />
{/if}
{t}Rechercher par pseudo de joueur : {/t} <input type="text" size="30" value="{$searchPseudo}" name="searchPseudo" id="searchPseudo" />

<input type="submit" name="submitSearchPseudo" value="{t}Chercher !{/t}" />
<br/>
{t}Rechercher par nom de hamster : {/t} <input type="text" size="30" value="{$searchHamster}" name="searchHamster" />

<input type="submit" name="submitSearchHamster" value="{t}Chercher !{/t}" /><br/>
&nbsp;<br/>
{t}ne voir que les hamsters mâles{/t} <input type="checkbox" name="quelesmales" {if $contrainteSexe != "" } checked="checked" {if $action == "avoirunbebe"}disabled="disabled"{/if} {/if}/>

</form>
<br/>&nbsp;<br/>
{$msg}
<br/>
{if sizeof($lstResultats) > 0 }
<div>
{foreach from=$lstResultats item=resultat}
    {t}Pseudo du joueur{/t} : {$resultat.lienprofil}, {t}son hamster{/t} : {$resultat.nom} 
    {if $action != "avoirunbebe"}| <a href="jeu.php?actionInteraction=ajouterAmi&amp;pseudoAmi={$resultat.pseudo}">{t}ajouter à mes amis{/t}</a>{/if}
    {if $action == "avoirunbebe"} | <a href="jeu.php?mode=m_hamster&amp;hamster_id={$hamster_id_concerne}&amp;bebe={$resultat.hamster_id}">{t}Avoir un bébé avec cet hamster mâle{/t}</a>{/if}
    <br/>
{/foreach}
</div>

{/if}

<div style="clear:both;">&nbsp;</div>

  </div>
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>