{t}Choisis un hamster que tu veux élever{/t} :<br/>

{foreach item=hamster from=$lstHamsters name=outerHamster}
        
<div id="boiteHamster_{$smarty.foreach.outerHamster.iteration}" class="blocRounded" style="width:220px;height:310px; { if $hamster.accessible eq false } opacity:0.3; {/if}">
            
    <div style="margin-bottom:15px;font-size:9pt;position:relative;">
        <div style="margin-left:auto;margin-right:auto;width:150px; text-align:center;">
            <img src="images/{$hamster.image}" alt="{$hamster.nom}" style="vertical-align:middle; height:110px;" /><br/>
            <strong>{$hamster.nom}</strong>
        </div>
        <br/>
        <div style="font-size:9pt;">
            {t}Yeux{/t} : {$hamster.yeux}<br/>            
            {t}Pelage{/t} : {$hamster.pelage}
        </div>
        <br/>
        <div>
            {$hamster.description}
        </div>
        <br/>
        <div align="center">
            { if $hamster.accessible eq true }
                {t}Sélectionner{/t} : <input type="radio" title="choix du hamster" name="typeHamster" value="{$hamster.type}" {if $hamster.already_checked eq true} checked="checked" {/if} 
                onclick='
                $("div[id^=boiteHamster_][id!=boiteHamster_{$smarty.foreach.outerHamster.iteration}]").fadeTo("fast",0.3); 
                $("#boiteHamster_{$smarty.foreach.outerHamster.iteration}").fadeTo("fast",1); 
                return true;'
                />
            {else }
                {t}Accessible au niveau{/t} {$hamster.niveau_min}
            {/if}
        </div>
    </div>
</div>

{if $smarty.foreach.outerHamster.iteration % 3 == 0 }
<div style="clear:both;">&nbsp;</div>
{/if}

{/foreach}

<div style="clear:both;">&nbsp;</div>

<div class="blocRounded" style="width:800px;">
<div align="left">{t}Choisis maintenant le prénom de ton hamster et son caractère (2 choix au maximum){/t} :<br/>&nbsp;<br/>
{t}Prénom du hamster{/t} : <input type="text" name="prenomHamster" size="30" value="{$prenomHamster}" /><br/>
{t}Sexe du hamster{/t} : <input type="radio" name="sexeHamster" value="0" {if $sexeHamster == 0}checked="checked"{/if}/>{t}mâle{/t} <input type="radio" name="sexeHamster" value="1" {if $sexeHamster == 1}checked="checked"{/if} />{t}femelle{/t}
<br/>&nbsp;<br/>
{$msgErreur}
{t}Caractères{/t} : 
<input type="checkbox" name="caractHamster[]" value="coquet"
{ if $prefHamster_1 == "coquet" || $prefHamster_2 == "coquet"}
 checked="checked" {/if} />{t}coquet{/t}
 
 <input type="checkbox" name="caractHamster[]" value="fort"
{ if $prefHamster_1 == "fort" || $prefHamster_2 == "fort"}
 checked="checked" {/if} />{t}fort{/t}
 
 <input type="checkbox" name="caractHamster[]" value="resistant"
{ if $prefHamster_1 == "resistant" || $prefHamster_2 == "resistant"}
 checked="checked" {/if} />{t}résistant{/t}
 
 <input type="checkbox" name="caractHamster[]" value="sociable"
{ if $prefHamster_1 == "sociable" || $prefHamster_2 == "sociable"}
 checked="checked" {/if} />{t}sympa avec tout le monde{/t}
 
 <input type="checkbox" name="caractHamster[]" value="coquet"
{ if $prefHamster_1 == "drague" || $prefHamster_2 == "drague"}
 checked="checked" {/if} />{t}dragueur{/t}

 <input type="checkbox" name="caractHamster[]" value="batisseur"
{ if $prefHamster_1 == "batisseur" || $prefHamster_2 == "batisseur"}
 checked="checked" {/if} />{t}bâtisseur{/t}
 
 </div>
 
  </div>