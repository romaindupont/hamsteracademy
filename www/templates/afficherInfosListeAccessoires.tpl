<div align="center"><strong>{t}Liste de tous les objets de Hamster Academy{/t}</strong></div><br/>
<br />
{foreach item=objet from=$listeAccessoires name=lstAcc}

    <div class="cornflex yellowbox" style="width:240px;">
        <div class="t">
            <div class="c">
                <div style="margin-bottom:15px;font-size:9pt;">
                    <img src="images/{$objet.image}" alt="" style="vertical-align:middle; height:{math equation = "min(x*s,y)" x = $objet.img_height s=$objet.echelle y=60}px;" /> 
                    &nbsp;&nbsp;<strong>{if $lang=="fr"}{$objet.nom_fr}{else}{$objet.nom_en}{/if}</strong><br/>&nbsp;<br/>
                    {if $lang=="fr"}{$objet.desc_fr}{else}{$objet.desc_en}{/if}<br/>&nbsp;<br/>
                    {t}Accessible au niveau{/t} {$objet.niveau}<br/>
                    {t}Prix{/t} : {$objet.prix} <img src="images/piece_reduc.gif" height="10" valign="baseline" alt="{if $lang=="fr"}{$objet.nom_fr}{else}{$objet.nom_en}{/if}"/><br/>
                    &nbsp;<br/>
                    {if $objet.rayon == -1 || $objet.rayon == -2}
                        {t}Indisponible en boutique{/t}
                    {else}
                        {t}Disponible au rayon{/t}
                        {if $objet.rayon == 1}
                            {t}Hamster{/t}
                        {elseif $objet.rayon == 2}
                            {t}Cage{/t}
                        {elseif $objet.rayon == 5}
                            {t}Sport{/t}
                        {elseif $objet.rayon == 6}
                            {t}Beauté{/t}
                        {elseif $objet.rayon == 10}
                            {t}Instruments{/t}
                        {elseif $objet.rayon == 11}
                            {t}Pharmacie{/t}
                        {/if}
                    {/if}
                </div>
            </div>
        </div>                
        <div class="r"></div>
        <div class="b"></div>
        <div class="l"></div>
    </div>
    {if $smarty.foreach.lstAcc.iteration % 3  == 0}
    <div style="clear:both;">&nbsp;</div>
    {/if}
{/foreach}
<div style="clear:both;">&nbsp;</div>