====== Aide de l'Univers Eleveur ======


===== Principe général =====

C'est l'Univers dans lequel tout le monde commence : c'est dans cet univers que tu dois élever ton hamster et construire sa cage. Pour pouvoir évoluer dans le jeu tu as dès le début un salaire de base (7 pièces/jour) qui te permet de faire les dépenses courantes. Tu es ensuite libre de t'inscrire à une formation qui te permettra d'avoir un métier et ainsi gagner plus d'argent pour pouvoir acheter tout ce dont tu auras besoin pour ton hamster.

===== Début du jeu =====

==== Quelle est la monnaie? ==== 

La monnaie locale est la pièce d'or. Tous les jours, chaque éleveur gagne au moins 7 pièces. Le jeu te permet de t'inscrire ensuite à une formation qui te permettra de gagner encore plus de pièces. A tout moment, tu pourras aller en ville et choisir un nouveau métier en achetant une formation.
Attention, il faut savoir qu'au-delà de 3 jours d'absence de l'éleveur, le salaire n'est plus versé. Il faut donc se connecter au moins 1 fois tous les 3 jours pour avoir son salaire !

{{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce d'or}}{{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce d'or}}{{http://www.hamsteracademy.fr/images/piece_reduc.gif|Pièce d'or}} 

==== Comment donner à boire au hamster ? ====

Pour que ton hamster puisse boire, il suffit de mettre un biberon dans sa cage. C'est indispensable sinon le hamster tombe vite malade et sa santé chute très rapidement. Dès l'achat de ta cage, il faut absolument acheter un biberon et le mettre dans la cage. Ensuite tu n'as plus besoin de changer l'eau.

==== Comment nourrir le hamster ? ====

Pour nourrir le hamster, il faut qu'il y ait une écuelle dans la cage et suffisamment de portions de nourriture. Les graines de tournesol constituent l'aliment de base. Il faut faire en sorte que le hamster n'ait pas faim et que son écuelle ne soit jamais complètement vide, sinon sa santé et son moral diminuent. Nourrir et donner à boire au hamster sont les deux actions les plus importantes à ne jamais oublier !

{{http://www.hamsteracademy.fr/images/ecuelle_reduc_sb.gif |Ecuelle vide}}
{{http://www.hamsteracademy.fr/images/ecuelle_pleine_reduc.gif |Ecuelle pleine}}


===== L'éleveur =====

==== Comment acheter des objets ? ====

Tu as la possibilité d'acheter des objets, aussi bien pour la cage que pour le hamster. Pour cela, il faut aller en ville puis dans la boutique. Tu peux ensuite te balader dans les différents rayons et choisir tes objets pour la cage, pour le hamster, et pour l'Academy. Seuls les hamsters de niveau 2 peuvent accéder à la boutique !

Certains objets comme les instruments de musique ne sont disponibles qu'à partir d'un certain niveau dans le jeu.

{{http://www.hamsteracademy.fr/images/banc_reduc.gif?50*50|Petit banc }}
{{http://www.hamsteracademy.fr/images/balance.gif?50*50|Balance }}
{{http://www.hamsteracademy.fr/images/miroir_noir.gif?25*25|Miroir noir }}
{{http://www.hamsteracademy.fr/images/balancoire.gif?50*50|Balançoire }}
{{http://www.hamsteracademy.fr/images/fontaine2.gif?50*50|Fontaine }}
{{http://www.hamsteracademy.fr/images/cachette.gif?25*25|Cachette en bois }}
{{http://www.hamsteracademy.fr/images/roue3.gif?50*50|Roue en bois }}
{{http://www.hamsteracademy.fr/images/piano.gif?50*50|Piano}}
{{http://www.hamsteracademy.fr/images/guitare_electrique.gif?50*50|Guitare électrique }}
{{http://www.hamsteracademy.fr/images/cabane.gif?50*50|Cabane }}
{{http://www.hamsteracademy.fr/images/boule_rose_reduc.gif?50*50|Boule rose }}

\\

La liste de tous les objets est donnée dans cette page : {{http://www.hamsteracademy.fr/afficherInfos.php?mode=m_liste_accessoires|Liste de tous les objets}}

==== Comment demander au hamster de construire son nid ? ====

Une des missions proposées au joueur consiste à construire son nid. Le hamster aura besoin de 3 éléments pour y arriver :
  * de la paille ;
  * du coton ;
  * des brindilles.
Tous ces éléments sont disponibles dans la boutique. Dès que tu les auras donnés au hamster, il pourra commencer à construire son nid! La durée de construction dépend de ses capacités de bâtisseur (un des caractères au choix lors de l'inscription). La durée peut varier de 24 heures à 1 semaine. Pour accélérer la construction du nid et pouvoir passer plus rapidement au niveau supérieur, il est aussi possible d'acheter une formation bâtisseur ou d'acheter 

un code **Allopass**, dans ce cas la construction est instantanée.

==== Comment demander au hamster de construire une cabane ? ====

La construction de la cabane fonctionne comme la construction du nid. Tu as besoin de 2 choses :

    * des rondins de bois ;
    * un plan de construction.

Une fois ces éléments achetés et donnés à ton hamster, la construction de la cabane commence aussitôt ! La durée de construction dépend également des capacités de bâtisseur du hamster.

==== Comment promener mon hamster ? ====

Pour pouvoir promener son hamster, il faut acheter une laisse spéciale pour les hamsters dans la boutique (accessible seulement aux éleveurs de niveau 4). Il est alors possible de promener son hamster une fois par jour maximum. La balade en forêt se passe généralement très bien et le hamster adore ce type de distraction (il retrouve son milieu naturel). En plus, il peut arriver que ton hamster trouve un gland sur le chemin (le moral augmente). Mais attention, il peut aussi attraper une maladie (les champignons vénéneux et les microbes sont fréquents dans la forêt).

==== Comment changer de métier ? ====

Si tu ne gagnes pas assez de pièces, tu as la possibilité de changer de métier. Cela te permet d'augmenter tes revenus quotidiens. Seuls les éleveurs au niveau 3 peuvent le faire. Il faut suivre une formation payante pour apprendre le nouveau métier que tu as choisi. Plus la formation coûte cher, plus le salaire sera élevé. Il est donc intéressant de payer plus cher au début pour être sûr de gagner beaucoup d'argent ensuite. Pour changer de métier, il faut aller en ville puis cliquer sur l'onglet [[http://www.hamsteracademy.fr/jeu.php?mode=m_metier|Mon métier]].

{{http://www.hamsteracademy.fr/images/pompier2.gif?50*50|Pompier }}
{{http://www.hamsteracademy.fr/images/hamburger.gif?50*50|Vendeur de hamburgers }}
{{http://www.hamsteracademy.fr/images/sauveteur.gif?50*50|Sauveteur en mer }}
{{http://www.hamsteracademy.fr/images/joueur_tennis.gif?50*50|Joueur de tennis }}
{{http://www.hamsteracademy.fr/images/Veterinaire_reduc.gif?50*50|Vétérinaire au zoo }}
{{http://www.hamsteracademy.fr/images/musician3.gif?50*50|Musicien }}
{{http://www.hamsteracademy.fr/images/rallye3.gif?50*50|Pilote de rallye }}
{{http://www.hamsteracademy.fr/images/detective.gif?50*50|Détective privé}}
{{http://www.hamsteracademy.fr/images/police.gif?50*50|Policier }}
{{http://www.hamsteracademy.fr/images/microscope.gif?50*50|Bonatiste }}
{{http://www.hamsteracademy.fr/images/crocodile.gif?50*50|Eleveur de crocodiles }}

==== Comment sont calculés les points de l'éleveur ? ====

Ils sont calculés toutes les 5 minutes et à chaque action.  Le classement est alors mis à jour.

Les règles de calcul du score sont : 
   * plus les hamsters ont des points, plus l'éleveur en a
   * plus le joueur possède de cages, plus il a de points. Il faut qu'elles soient toutes propres et les plus grandes possibles (en évitant les étages vides d'objet)
   * plus un joueur a d'objets, plus il a de points
   * si le joueur a un avatar, il y a des points en plus (+10%)

Par contre, si un ou plusieurs de ses hamsters sont malades, il perd des points (-25% par hamster malade).

[[#comment_sont_calcules_les_points_du_hamster|La section qui explique comment sont calculés les scores des hamsters est ici.]]

===== Mon hamster =====

==== Comment améliorer la santé du hamster ? ====

La santé du hamster évolue en fonction de nombreux paramètres. Les trois actions de base à savoir nourrir, donner à boire et changer les copeaux te permettent de conserver ton hamster dans un bon état. Si tu veux que ton hamster soit dans une très bonne forme, il existe de nombreux articles dans La Boutique qui te permettent d'améliorer sa santé. C'est le cas par exemple des vitamines. Il faut savoir aussi qu'un hamster qui pratique de la roue ou d'autres types d'activités physiques sera plus facilement dans un meilleur état de santé. Certains aliments comme la salade sont très bon pour la santé du hamster. En plus il adore ça ! Mais il faut faire attention à bien s'occuper du hamster. Si sa santé devient proche de 0, il sera envoyé chez le vétérinaire pour recevoir des soins intensifs. Cela lui évite de mourir, mais l'inconvénient pour l'éleveur est que ces soins coûtent très cher si tu veux ensuite récupérer le hamster.
Si ton hamster a une mauvaise santé, pense à vérifier que :

    * son écuelle n'est pas vide ;
    * il y a un biberon dans sa cage ;
    * les copeaux sont propres ;
    * sa force physique est suffisante.


==== Comment améliorer le moral du hamster ? ====

Si le moral de ton hamster est faible, cela signifie qu'il n'est pas heureux dans sa cage. Les principaux éléments qui affectent le moral du hamster sont causés par:

    * une écuelle vide ;
    * une mauvaise santé ;
    * trop d'hamsters présents dans la même cage (le hamster est un animal solitaire) ;
    * l'absence de biberon ;
    * le manque de soins (caresse, crème de soins...).

Pour améliorer le moral de ton hamster, pense à divertir ton hamster au maximum (stage de plein-air, sortie au rugby ou au foot, ...).

==== Comment améliorer la beauté du hamster ? ====

Un des paramètres importants de l'état général du hamster est son aspect esthétique. Cela a de l'importance surtout pour l'univers de l'Academy. Plus ton 
hamster est beau, plus il est populaire ! Il est dont indispensable de caresser régulièrement le hamster, de le brosser et de lui appliquer une crème de soin 
pour le pelage. La propreté de la cage influe sur sa beauté, un hamster qui vit dans une cage sale ne peut pas être propre. Les articles de beauté proposés En Ville te permettent d'améliorer la beauté de ton hamster.

==== Comment rendre son hamster sportif ? ====

Il est très important pour la santé et pour le moral de ton hamster qu'il fasse du sport. Sa forme physique en dépend ! Pour devenir plus fort et plus résistant, le hamster doit faire du sport et il a plusieurs possibilités :

    * faire de la roue dans sa cage ;
    * faire une séance de musculation (avec les haltères) ;
    * lui acheter des accessoires (vélo, balançoire, boule, ...). 

Mais attention, il ne faut pas qu'il fasse trop de sport, et plusieurs heures de repos sont nécessaires entre chaque séance de musculation !

NB : il faut plusieurs jours pour que ton hamster se muscle avec la roue ; avec les haltères les résultats sont immédiats !

==== Mon hamster est trop maigre ou trop gros, que faire ? ====

Le poids idéal pour le hamster est 120g. Si son poids est trop faible (inférieur à 105g) ou trop élevé (supérieur à 135g), sa santé, sa forme physique et sa beauté se dégradent. S'il est trop maigre, donne-lui à manger plus souvent et fais-lui faire du sport (pour qu'il se muscle) plus régulièrement. S'il grossit trop vite, vérifie qu'il possède bien une roue dans sa cage et qu'il fait suffisamment de sport, donne lui moins à manger et achète un maximum d'articles de sport.

==== Mon hamster est vraiment trop gros, je n'arrive pas à faire baisser son poids... ====

Il existe un moyen un peu tordu de faire perdre tous les kilos en trop au hamster d'un coup, sans utiliser de pilules amincissantes. Mais on vous laisse deviner comment y arriver ! Juste un indice : il faut lui faire de la roue, mais si possible, pas avec n'importe quelle roue, plutôt une roue trafiquée... 

(possible seulement pour les éleveurs de niveau 3)

==== Mon hamster ne veut pas faire de roue... ====

A partir du moment où tu as acheté une roue pour le hamster et que tu l'as mise dans sa cage, il en fait forcément. Les effets sur sa force musculaire et sur 
sa forme générale ne sont pas instantanés (contrairement aux haltères) et il faut attendre plusieurs jours pour voir les résultats.

==== Comment avoir des bébés ? ====

Comme tout animal de compagnie, le hamster peut avoir des bébés **dès que son âge dépasse 1 mois**. On ne peut faire reproduire que les hamsters femelles. On doit alors leur choisir un hamster mâle au sein de l'Hamster Academy pour pouvoir les accoupler. La grossesse dure 1 semaine environ et le nombre de petits est compris entre 1 et 3, aussi bien des mâles que des femelles. Leurs caractéristiques génétiques dépendent de celles de leurs parents. Attention ! Une fois les bébés nés, il faut s'en occuper ! Hors de question de les laisser mourir de faim, de soif ... Il est vivement conseillé de leur acheter une nouvelle cage 

pour ne pas qu'il passe leur temps à se disputer.


==== Comment sont calculés les points du hamster ? ====

Les points sont égaux à la somme de la santé, beauté, force, moral, expérience, popularité, danse, chant, humour et musique.

Ils augmentent aussi :
    * s'il a un instrument de musique
    * s'il est marié (+10%)
    * s'il a un groupe (il récupère les points du groupe)

Ils diminuent :
    * s'il est malade (division par 2)

[[www.hamsteracademy.fr/aide_academy#comment_sont_calcules_les_points_de_chaque_groupe|Le calcul des points du groupe sont expliqués ici.]]

===== Ma cage =====

==== Comment déplacer un objet dans la cage ou d'une cage à une autre? ====

La construction de la cage est un élément important du jeu. Chaque éleveur est libre de la personnaliser autant qu'il veut. Tu peux ajouter des étages, agrandir leur surface, changer la couleur et ajouter tous les objets que tu souhaites. Pour déplacer un objet dans la cage d'un étage à l'autre, il suffit de cliquer sur l'objet à déplacer (sans relâcher la souris) et de le déplacer à l'endroit de la cage où tu veux qu'il apparaisse. Tu peux également utiliser les flèches qui apparaissent dans le cadran à droite dans l'atelier.
En cas de difficultés, vous pouvez toujours aller sur le Forum ou laisser un message à [[mailto:contact@hamsteracademy.fr|contact@hamsteracademy.fr]].

Si tu veux enlever un objet de la cage, il faut le sélectionner dans l'inventaire et le déplacer dans le bac fourre-tout.

Si tu veux déplacer un objet d'une cage à une autre, il faut cliquer sur l'objet dans l'inventaire et sélectionner la cage dans laquelle tu veux le déplacer.

==== Mon hamster a rongé les barreaux de la cage... comment éviter ça ? ====

Tous les hamsters ont l'habitude de ronger les barreaux de leur cage. Cela leur permet d'user leurs dents qui poussent sans arrêt. Pour éviter que ton hamster ronge les barreaux, tu peux acheter dans la boutique une pierre à ronger pour que le hamster lime ses dents. Il en faut une par cage pour que les barreaux ne soient plus rongés (la pierre à ronger n'est disponible pour les éleveurs de niveau 4).

==== La roue fait du bruit et ça dérange les voisins... que faire ? ====

Il suffit d'acheter de l'huile ! Il suffit d'en avoir, elle est automatiquement appliquée sur la roue. Normalement, plus aucun voisin ne se plaindra !

==== Je pars en vacances et je ne peux pas m'occuper de mes hamsters... ====

Si tu n'as pas le temps de t'occuper de tes hamsters pendant quelques jours ou quelques semaines, tu peux les déposer au Refuge. Le prix est de 10 pièces pour tous les hamsters. Ils seront alors nourris et logés aussi longtemps que tu le souhaites et tu les récupères dans le même état de santé et de moral qu'avant de partir. Pendant que ton hamster est au refuge, tu ne gagnes pas d'argent.

==== Comment nettoyer ma cage ? ====

Chaque jour, les hamsters salissent un peu plus leur cage. C'est pourquoi il faut la nettoyer régulièrement (au moins une fois / semaine pour une cage avec 1 hamster) ! Tu pourras trouver des copeaux de bois dans la boutique, il suffit d'un litre pour changer une fois les copeaux. 

Attention, une cage sale favorise l'apparition de maladies et fait baisser l'état de santé du hamster. Plus il y a de hamsters dans la même cage, plus celle-ci se salit vite, donc plus il faut être vigilant ! 

{{http://www.hamsteracademy.fr/images/odeurs.gif}}

==== Le dessin des cages est bizarre, ou ne marche pas ? ====

Il peut arriver que cela ne marche pas. Il y a 2 solutions :

     - Rafraîchir complètement la page en appuyant sur la touche F5 ;
     - Si ça ne marche toujours pas, le navigateur (Internet Explorer, Firefox, Safari ou Opéra) peut être à l'origine du problème. Le navigateur idéal pour 

le jeu est Firefox. Si tu rencontres tout de même des difficultés, n'hésite pas à contacter Hamster Academy 

([[mailto:contact@hamsteracademy.fr|contact@hamsteracademy.fr]])!

===== Questions diverses =====

==== Pas assez de pièces en poche ? Que faire ? ====

Pour gagner de l'argent il existe plusieurs solutions dans le jeu. Tu peux déjà attendre de recevoir ton prochain salaire (versé une fois par jour). Sinon tu as d'autres possibilités :
  - jouer au quizz (questionnaire sur les hamsters, un seul essai par jour) ;
  - vendre des objets (il faut aller dans l'inventaire puis choisir l'objet à vendre) ;
  - demander au hamster de fabriquer un nid (ou une cabane) puis le revendre ;
  - parrainer un ami, c'est-à-dire l'inviter à jouer à Hamster Academy : ça te rapporte 20 pièces par ami inscrit. Pour parrainer, il faut cliquer sur 

[[http://www.hamsteracademy.fr/parrainage.php|ce lien]] et suivre les indications ;
  - acheter des pièces à la BANQUE, tu recevras entre 300 et 1000 pièces ([[http://www.hamsteracademy.fr/jeu.php?mode=m_banque|Aller à la Banque !]]).

==== Comment jouer au rugby et au football? ====

Jouer au rugby ou au football est une activité physique importante qui permet à l'éleveur d'améliorer la force et le moral du hamster. Faire un sport en groupe est toujours bon pour le moral ! Pour que le hamster puisse jouer avec d'autres hamsters, il a besoin de 2 choses :

    * un ballon : tu peux en trouver dans la boutique ;
    * une sortie : la sortie est payante (organisation du jeu, location du terrain, ...), il faut l'acheter dans La Boutique. Une sortie n'est valable qu'une seule fois. Si tu veux améliorer l'état de santé, de moral et de force du hamster, il est vivement conseillé de multiplier ce genre de sorties... A renouveler sans modération, tant qu'il reste de l'argent dans le portefeuille !

**NB : la sortie au rugby n'est possible que le samedi, la sortie au football est possible le mercredi et le samedi**.

==== Quel est l'agenda de la semaine ? ====

Les activités disponibles sont réparties tout au long de la semaine. Les sorties possibles sont :

    * Lundi : sortie dans la boule - musculation
    * Mardi : promenade en forêt - musculation
    * Mercredi : football - musculation - console de jeu
    * Jeudi : sortie dans la boule - musculation
    * Vendredi : promenade en forêt - musculation
    * Samedi : rugby - musculation
    * Dimanche : football - console de jeu - musculation

^   Lundi        ^   Mardi         ^   Mercredi           ^  Jeudi  ^  Vendredi  ^  Samedi  ^ Dimanche ^
|{{http://www.hamsteracademy.fr/images/boule_noire_reduc.gif|Sortie dans la boule}}\\ {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}} \\ {{http://www.hamsteracademy.fr/images/masque_catch.png}}|{{http://www.hamsteracademy.fr/images/laisse.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}|{{http://www.hamsteracademy.fr/images/ballon_foot.gif}}\\ {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}\\ {{http://www.hamsteracademy.fr/images/console.gif}} |{{http://www.hamsteracademy.fr/images/boule_noire_reduc.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}\\ {{http://www.hamsteracademy.fr/images/masque_catch.png}}|{{http://www.hamsteracademy.fr/images/laisse.gif}}{{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}|{{http://www.hamsteracademy.fr/images/ballon_rugby.gif}}{{http://www.hamsteracademy.fr/images/halteres_reduc.gif}}|{{http://www.hamsteracademy.fr/images/ballon_foot.gif}}{{http://www.hamsteracademy.fr/images/console.gif}} {{http://www.hamsteracademy.fr/images/halteres_reduc.gif}} |

==== Est-il possible de se séparer d'un ou plusieurs hamsters ? ====

Il est maintenant possible de se séparer d'un hamster si tu n'as plus le temps ou pas assez d'argent pour t'en occuper. Dans l'Accueil, tu peux aller à la SPH pour y laisser un hamster. La SPH (Société Protectrice des Hamsters) s'occupe des hamsters des éleveurs qui ne peuvent plus s'en occuper. Une fois que tu as déposé ton hamster, **tu ne peux plus le récupérer** ;  **ce choix est définitif !!** Laisser son hamster à la SPH est payant, cela leur permet de pouvoir s'en occuper au début.
La SPH est ouverte aux joueurs de niveau 3 et possédant au minimum 2 hamsters.

==== Comment se faire des amis ? ====

Le Forum, le Tchat et le jeu te permettent de te faire des amis. Sur la page d'Accueil, tu as la possibilité de créer une liste d'amis, en cliquant sur //Outils// {{http://www.hamsteracademy.fr/images/icone_outils.gif?20*20 }}, puis sur //Ajouter//.

==== Les défis : quel est le principe ? ====

Tout joueur peut défier un autre joueur à condition que son niveau soit au moins égal. Par exemple, un joueur de niveau 9 ne peut pas défier un joueur de niveau 8. 

    * Son défi peut comporter une mise : le gagnant remporte 2 fois la mise et le perdant perd la mise.
    * Son défi peut être refusé par l'autre joueur
    * Son défi est annulé au bout de 48 heures si le joueur défié n'a pas répondu

Qui gagne le défi ? Le joueur qui gagne est celui a le hamster le plus fort. S'il possède plusieurs hamsters, c'est un hamster au hasard qui est choisi parmi le siens. Donc, il est important que tous les hamsters soient forts et surtout non malades ! La force, la santé et le moral du hamster sont pris en compte pour déterminer qui est le plus fort. Il ne suffit pas d'être fort, il faut aussi être en pleine forme et avec un moral d'acier pour gagner !

==== Les métiers ====

[[www.hamsteracademy.fr/métier|Description des métiers]] (à venir)