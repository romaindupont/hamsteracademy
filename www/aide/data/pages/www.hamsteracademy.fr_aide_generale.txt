====== Questions generales ======

===== Mon compte =====

==== Est-il possible de supprimer son compte ? ====

Supprimer son compte permet de recommencer une partie depuis le début, sans créer de nouveau compte. Pour cela, il faut aller dans l'onglet "//Options//" puis ensuite dans la partie "//Mon Compte//", et cocher la case "//Supprimer mon compte//". Mais attention, **c'est une opération irréversible**. Si tu as besoin d'aide, n'hésite pas à contacter Hamster Academy ([[mailto:contact@hamsteracademy.fr|contact@hamsteracademy.fr]]) !

==== Hamster Academy peut-il supprimer mon compte ? ====

Si le compte d'un joueur est **inactif depuis plus de 3 mois**, il est automatiquement supprimé. Seuls les joueurs qui ont une adresse mail **validée** recevront un mail une semaine avant pour les prévenir.

==== Comment changer de mot de passe ou d'adresse mail ? ====

Si tu veux changer de mot de passe, il faut aller dans l'onglet "//Options//" situé en haut à droite de l'écran dans l'univers Eleveur. Le mot de passe doit contenir au minimum 6 caractères. L'adresse mail n'est pas obligatoire mais il est conseillé d'en ajouter une à son compte et de la valider. Cela permet de recevoir des cadeaux (pour la cage et le hamster) et de pouvoir récupérer son mot de passe si on l'a oublié. 
Pour ajouter ou modifier une adresse mail, il faut aller dans le menu des "//Options//" et saisir la nouvelle adresse mail dans le profil du joueur.

==== J'ai oublié mon mot de passe, comment faire pour le récupérer ? ====

Si tu as donné une adresse mail correcte à ton inscription et validée ensuite, tu peux le récupérer automatiquement en cliquant sur l'adresse suivante "[[http://www.hamsteracademy.fr/motdepasseperdu.php|Mot de passe perdu]]" . Si ça ne marche, ou si tu n'as pas d'adresse mail valide, il faut contacter [[mailto:contact@hamsteracademy.fr|contact@hamsteracademy.fr]] en expliquant le problème et en donnant le pseudo. Après vérification du pseudo, le mot de passe sera envoyé au joueur.

==== Je n'arrive pas à me connecter et je suis certain de mon mot de passe ! ====

Il faut essayer toutes les solutions ci-dessous

    * changer de navigateur en utilisant Firefox (lien disponible plus haut) au lieu d'Internet Explorer (ou vice-versa) ;
    * essayer sur un autre ordinateur ;
    * redémarrer le navigateur / l'ordinateur ;
    * vérifier que les cookies sont bien acceptés par le navigateur ;
    * vérifier que l'antivirus ou le firewall n'est pas trop sévère ;
    * vérifier que le filtre parental, s'il existe, n'est pas trop sévère.


==== Je suis suspendu, pourquoi ? ====

Les administrateurs du jeu et les modérateurs peuvent suspendre un joueur. Les raisons principales sont :
    * messages grossiers ou insultants à la poste ou dans le forum
    * tentative de triche
    * tentative de récupération de mot de passe d'un autre joueur
    * tentative de se faire passer pour un modérateur/administrateur
    * utilisation du multi-comptes pour gagner des pièces ou échanger des objets

Selon la gravité, elle est soit temporaire (48h ou 1 semaine), soit définitive.

Si le joueur essaie de contourner la suspension en créant un autre compte, c'est l'adresse IP qui peut être bloquée.

Si un joueur pense que la suspension n'est pas méritée (erreur sur la personne notamment), il peut envoyer un email à Hamster Academy.

===== Questions diverses sur le jeu =====

==== Qu'est ce que le parrainage, et comment parrainer un ami ? ====

Le principe du parrainage est d'inviter vos amis à jouer au jeu Hamster Academy en indiquant leur adresse mail. Chaque joueur invité de ta part qui s'inscrit devient ton "//filleul//". Il reçoit alors un mail avec un lien vers www.hamsteracademy.fr, et s'il s'inscrit en cliquant sur ce lien, tu recevras 20 pièces par nouveau joueur. 
Il est interdit de se parrainer soi-même : dans ce cas, le compte est suspendu et les pièces ne sont pas versées. Le parrainage est accessible en cliquant sur le lien [[http://www.hamsteracademy.fr/parrainage.php|parrainage]]. (Nombre de parrainages par joueur limité à 5). A noter que l'adresse mail doit être activée (voir l'aide correspondante).

==== Je n'arrive pas à me connecter au Tchat (dorénavant fermé) ====

Le tchat est dorénavant fermé. 

Si tu rencontres des difficultés pour te connecter au Tchat, essaye ces différentes possibilités :

    * le tchat est dorénavant fermé.
    * <del>changer de navigateur en utilisant Firefox ou Internet Explorer ;</del>
    * <del>vérifier que Flash est bien installé sur l'ordinateur ;</del>
    * <del>vérifier qu'il n'y a pas de contrôle parental activé. Si c'est le cas, il suffit de régler le logiciel de contrôle parental ;</del>
    * <del>si ton pseudo possède des caractères bizarres, il faut contacter Hamster Academy ;</del>
    * <del>tu as pu être banni par un modérateur et dans ce cas il faut également contacter Hamster Academy.</del>


<del>Le pseudo utilisé pour le Tchat doit être le même que dans le jeu, et il ne faut pas cocher la case "//invité//".</del>

==== Mon code Allopass/Paypal ne marche pas ou je n'ai pas été crédité des pièces promises ? ====

Ce sont des choses qui arrivent mais très rarement. Dans ce cas, il ne faut pas hésiter à contacter Hamster Academy pour le signaler. Après avoir vérifié que le paiement a bien été effectué, nous corrigerons rapidement le problème.

==== Qu'est-ce que l'activation de mon adresse mail ? ====

Il s'agit pour nous de vérifier que ton adresse mail est correcte. L'activation te permettra alors de participer aux concours, de recevoir des messages d'autres joueurs par mail, de recevoir des cadeaux, de parrainer, etc.
L'activation s'effectue dans le menu "//Options//". Après avoir cliqué sur le bouton "//Activer maintenant//", tu recevras un mail avec un lien permettant l'activation de l'adresse enregistrée. 

==== Le jeu est-il gratuit ? ====

Oui, il est possible de jouer à Hamster Academy et de ne pas payer. Pour les joueurs qui veulent gagner des pièces plus rapidement, il existe la possibilité d'acheter des pièces en allant à la [[http://www.hamsteracademy.fr/jeu.php?mode=m_banque|BANQUE]].

==== Existe-t-il un navigateur recommandé pour jouer à Hamster Academy? ====

Ce jeu a été conçu pour fonctionner sur la plupart des navigateurs classiques. La petite préférence d'Hamster Academy est Firefox (logiciel gratuit téléchargeable à l'adresse http://www.mozilla-europe.org/fr/ en cliquant sur "//Obtenir Firefox//"), mais le jeu fonctionne très bien avec Internet Explorer (version 6 et supérieur), Safari et Opéra. Si tu as des problèmes d'affichage ou que tu n'arrives pas à jouer, le navigateur peut être à l'origine du problème et il est alors conseillé d'essayer le jeu avec un autre navigateur.

==== Comment contacter Hamster Academy ? ====

Pour toutes questions ou commentaires sur le jeu, tu peux nous contacter en envoyant un mail à [[mailto:contact@hamsteracademy.fr|contact@hamsteracademy.fr]]. Tu recevras une réponse ou une aide dans les plus brefs délais. 
Si tu as une idée **innovante** à nous proposer, et que tu veux qu'elle soit intégrer au jeu, une boîte à idée est disponible sur la page d'inscription du jeu. Tu peux y accéder en cliquant sur l'onglet "//Quitter//", en haut à droite de l'écran. Toutes les remarques et idées sont les bienvenues.

==== Comment recevoir ou ne plus recevoir la lettre d'information ?  ====

La lettre d'information de Hamster Academy informe les joueurs des nouveautés, concours, évènements particuliers, cadeaux, etc. Nous l'envoyons en moyenne 1 fois par mois et au maximum 1 fois par semaine.

Lors d'évènements particuliers, anniversaires, nouveauté importante, on offre à l'occasion des cadeaux pour remercier les joueurs et seuls les joueurs ayant un email valide et inscrit à la lettre d'information y ont droit.

Pour recevoir ou ne plus recevoir la lettre d'information, il faut aller dans Options->Mon compte et cocher/décocher la case correspondante.