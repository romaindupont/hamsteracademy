<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

// ici, page de la Mairie

// dernière version du cache
$cache = 'cache/mairie_'.$lang.'.html' ;
$expire = $dateActuelle - 900;
if (file_exists($cache) && filemtime($cache) > $expire) {

    // affichage du cache 
    readfile($cache);
}
else {
    ob_start();

?>

<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">
  
  <h2 align="center"><?php echo T_("Bienvenue à la Mairie");?></h2>
    
    <div class="blocRounded" style="width:380px;">
    <h3><?php echo T_("Carnet de mariages");?></h3>
    
    <div style="margin-bottom:1em; font-weight:bold;"><?php echo T_("Aujourd'hui :");?></div>
        <div style="margin-bottom:1em; font-size:9pt;">
        <?php 
            $query = "SELECT date, texte FROM historique WHERE type = ".($lang == "fr" ? HISTORIQUE_MARIAGE : HISTORIQUE_MARIAGE_EN)." AND date > ".($dateActuelle-86400)." ORDER BY date DESC LIMIT 10" ; // 24 heures
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            $nbMariagesAujourdhui = $dbHT->sql_numrows($result) ;
            while($row=$dbHT->sql_fetchrow($result)) {
                echo T_("A ").date("G\hi", $row['date']).", ".$row['texte']."&nbsp;!<br/>";
            }
            $dbHT->sql_freeresult($result);

        echo "</div>";

        if ($nbMariagesAujourdhui < 10) {

            echo "<div style=\"margin-bottom:1em; font-weight:bold;\">".T_("Hier :")." </div>";
            echo "<div style=\"margin-bottom:1em; font-size:9pt;\">";

            $query = "SELECT date, texte FROM historique WHERE type = ".($lang == "fr" ? HISTORIQUE_MARIAGE : HISTORIQUE_MARIAGE_EN)." AND date < ".($dateActuelle-86400)." AND date > ".($dateActuelle-172800)." ORDER BY date DESC LIMIT 10" ; // 48 heures
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "A ".date("G\hi", $row['date']).", ".$row['texte']." !<br/>";
            }
            $dbHT->sql_freeresult($result);
    
            echo "</div>";
    
        }
        ?>

    </div>
    <div class="blocRounded" style="width:380px;">
    <h3><?php echo T_("Carnet de naissances");?></h3>
    
    <div style="margin-bottom:1em; font-weight:bold;"><?php echo T_("Aujourd'hui :");?></div>
    <div style="margin-bottom:1em;  font-size:9pt;">
        <?php 
            $query = "SELECT date, texte FROM historique WHERE type = ".($lang == "fr" ? HISTORIQUE_NAISSANCE : HISTORIQUE_NAISSANCE_EN)." AND date > ".($dateActuelle-86400)." ORDER BY date DESC LIMIT 10" ; // 24 heures
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            $nbMariagesAujourdhui = $dbHT->sql_numrows($result) ;
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "A ".date("G\hi", $row['date']).", ".$row['texte']."&nbsp;!<br/>";
            }
            $dbHT->sql_freeresult($result);

            echo "</div>";

        if ($nbMariagesAujourdhui < 10) {
        
            echo "<div style=\"margin-bottom:1em; font-weight:bold;\">".T_("Hier :")." </div>";
            echo "<div style=\"margin-bottom:1em; font-size:9pt;\">";

            $query = "SELECT date, texte FROM historique WHERE type = ".($lang == "fr" ? HISTORIQUE_NAISSANCE : HISTORIQUE_NAISSANCE_EN)." AND date < ".($dateActuelle-86400)." AND date > ".($dateActuelle-172800)." ORDER BY date DESC LIMIT 10" ; // 48 heures
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                echo T_("A ").date("G\hi", $row['date']).", ".$row['texte']." !<br/>";
            }
            $dbHT->sql_freeresult($result);
            
            echo "</div>";
        }

        ?>
        </div>
        <div style="clear:both;">&nbsp;</div>
        <div class="blocRounded" style="width:380px;">
            <img src="images/coupe.gif" alt="coupe" style="height:40px;"><br/>
            <h2><?php echo T_("Eleveurs les mieux notés :");?> </h2>
            <div style="font-size:9pt;">
            <?php
                $nbJoueursPts = 50;
                $query = "SELECT pseudo, note, joueur_id FROM joueurs WHERE note > 100000 AND joueur_id NOT IN (1,7,2,".HAMSTER_ACADEMY_ID.") AND langue = '$lang' ORDER BY note DESC LIMIT $nbJoueursPts";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
                }
                $i=1;
                echo "<div id=\"dixpremiersnotes\">";
                while($row=$dbHT->sql_fetchrow($result)) {
                    echo "<div>".$i.") <a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,800,600); return false;\" title=\"".T_("Voir son profil")."\">".$row['pseudo']."</a> (".$row['note']." points)</div>";
                    $i++;
                    if ($i == 11){
                        echo "</div>";
                        echo "<div id=\"lesautresnotes\" style=\"display:none;\">";
                    }
                }
                echo "</div>";
                $dbHT->sql_freeresult($result);
            ?>
            <br/>
            <a href="#" 
        onclick = '
                $("#lesautresnotes").show();
                return false;'
        >[<?php echo T_("Voir les 50 premiers");?>]</a></div>
        </div>
        
        <div class="blocRounded" style="width:380px;">
        <img src="images/coupe.gif" alt="coupe" style="height:40px;" /><br/>
        <h2><?php echo T_("Eleveurs les plus riches :");?></h2>
        <div style="font-size:9pt;">
        <?php
            echo afficherObjet("oeufs",1);
            $nbJoueursPieces = 50;
            $query = "SELECT pseudo, nb_pieces, joueur_id FROM joueurs WHERE nb_pieces > 100000 AND joueur_id NOT IN (1,7,2,".HAMSTER_ACADEMY_ID.")  AND langue = '$lang' ORDER BY nb_pieces DESC LIMIT $nbJoueursPieces";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            $i=1;
            echo "<div id=\"dixpremiers\">";
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "<div>".$i.") <a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,800,600); return false;\" title=\"".T_("Voir son profil")."\">".$row['pseudo']."</a> (".$row['nb_pieces']." ".IMG_PIECE.")</div>";
                $i++;
                if ($i == 11){
                    echo "</div>";
                    echo "<div id=\"lesautres\" style=\"display:none;\">";
                }
            }
            echo "</div>";
            $dbHT->sql_freeresult($result);
        ?>
        <br/>
        <a href="#" 
        onclick = '
                $("#lesautres").show();
                return false;'
        >[<?php echo T_("Voir les 50 premiers");?>]</a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
        <div class="blocRounded" style="width:800px;">
        <h2><?php echo T_("Aujourd'hui, c'est l'anniversaire de") ; ?> : </h2>
        <div style="font-size:9pt;">
        <?php
            $query = "SELECT pseudo, joueur_id, naissance_annee FROM joueurs WHERE (naissance_jour != 1 OR naissance_mois != 1 OR naissance_annee != 1940) AND naissance_jour = ".(date('j'))." AND naissance_mois = ".(date('n'))."  AND langue = '$lang' ORDER BY pseudo ASC";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                $txt = $row['pseudo'].( ($row['naissance_annee'] == 1940)? "" : " (".(date('Y') - $row['naissance_annee'])." ".T_("ans").")");
                echo returnLienProfil($row['joueur_id'],$txt)." &middot; ";
            }
            $dbHT->sql_freeresult($result);
        ?>
        <br/>&nbsp;<br/>
        <a href="options.php?mode=m_profil"><?php echo T_("As-tu indiqué ta date de naissance ?");?></a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
        <div class="blocRounded" style="width:800px;">
        <h2><?php echo T_("Les joueurs VIP du mois de ").$mois_txt[$month] ; ?> : </h2>
        <div style="font-size:9pt;">
        <?php
            $query = "SELECT pseudo, joueur_id FROM joueurs WHERE vip = 1 AND langue = '$lang' ORDER BY pseudo ASC";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining joueurs', '', __LINE__, __FILE__, $query);
            }
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "<a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,800,600); return false;\" title=\"".T_("Voir son profil")."\">".$row['pseudo']."</a> &middot; ";
            }
            $dbHT->sql_freeresult($result);
        ?>
        <br/>&nbsp;<br/>
        <a href="options.php?mode=m_vip"><?php echo T_("Pour tout savoir sur le status VIP, clique-ici !");?></a>
        </div>
        </div>
        <div style="clear:both;">&nbsp;</div>
        
        <?php 
        if ($lang == "fr") {
        ?>
        <div class="blocRounded" style="width:800px;">
        <h2><?php echo T_("Annuaire de Hamster Academy") ; ?></h2>
        <a href="jeu.php?mode=m_annuaire">
        <?php echo T_("Cliquer ici pour accéder à l'annuaire de Hamster Academy") ; ?>
        </a>
        </div>
        <div style="clear:both;">&nbsp;</div>
        <?php } ?>
        
    
  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 
  
  

</div>

<?
    $tampon = ob_get_contents();
    file_put_contents($cache, $tampon) ;
    ob_end_clean();
    echo $tampon;
}
?>