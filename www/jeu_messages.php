<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}
?>


<div style="width:940px;">

  <div class="hamBlocColonne-top">
     <div class="hamBlocColonne-top-left"></div>
     <div class="hamBlocColonne-top-right"></div>
     <div class="hamBlocColonne-top-center"></div>        
  </div>
    
  <div class="hamBlocColonne_full">


<h2 align="center"><?php echo T_("Bienvenue à la Poste");?></h2>

<div align="center">
<img src="images/urne.gif" alt="La Poste" width="72" />

<div style="width:700px; text-align:left; border-left:auto; border-right:auto;">

<?php 
if ($msg != "") {
    echo "<br/><div class=\"hamBlocColonne\">\n";
    echo $msg ;
    echo "</div>";
}

if (isset($_GET['envoyerMessage'])) {

?>
<!-- TinyMCE -->
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js"></script>
<script type="text/javascript">
    tinyMCE.init({
        mode : "textareas",
        theme : "advanced",
        plugins : "safari,advhr,advimage,advlink,emotions,iespell,media,contextmenu",

        // Theme options
        theme_advanced_buttons1 : "bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,blockquote,|,undo,redo,|,link,unlink,image,|,forecolor,backcolor,|,hr,emotions",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js"
    });
</script>
<!-- /TinyMCE -->

<div class="cornflex yellowbox">
<div class="t">
<div class="c">
<link href="http://www.hamsteracademy.fr/css/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
<div><strong><?php echo T_("Envoyer un message :");?></strong></div>

<br/>

<form action="jeu.php" method="post">
<input type="hidden" name="mode" value="m_messages" />
<input type="hidden" name="action" value="envoyerMessage" />

<?php 
// Saisie d'un nouveau message
// ---------------------------
// mode classique : pas de destinataire sélectionné

// un joueur a-t-il été choisi ?
if (isset($_GET['dest_joueur_id'])) {
    $dest_joueur_id = intval($_GET['dest_joueur_id']) ;

    echo "<input type=\"hidden\" name=\"messageDestJoueurId\" value=\"".$dest_joueur_id."\" />";
    $query = "SELECT pseudo FROM joueurs WHERE joueur_id = ".$dest_joueur_id." LIMIT 1";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
    }
    $row=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);
    echo T_("Destinataire du message : ").$row['pseudo'];     
}
else {
    echo "<script type=\"text/javascript\">
    $().ready(function() {
        $(\"#pseudoDestinataire\").autocomplete(\"listePseudos.php\", {
            minChars: 0,
            width: 200,
            max: 100,
            selectFirst: false
        });

    });
    </script>";
    echo T_("Destinataire du message :")." <input type=\"text\" id=\"pseudoDestinataire\" value=\"\" name=\"pseudoDestinataire\" />";
}
?>

<br/>&nbsp;<br/>
<textarea cols="75" rows="5" name="messageTxt"><?php echo T_("Ton message ici...");?></textarea>
<br/>
<font size="-1"><?php echo T_("Attention, tout message qui comporte des insultes ou des mots violents, agressifs ou choquants entrainera la suspension du joueur.");?></font><br/>&nbsp;<br/>
<input type="submit" value="<?php echo T_("Envoyer");?>" />
</form>

</div>

</div>
<div class="r"></div>
<div class="b"></div>
<div class="l"></div>
</div>

<?php 
}
else { //affichage des messages reçus/envoyés
?>

<div class="cornflex yellowbox">
<div class="t">
<div class="c">

<div><strong><?php echo T_("Mes messages reçus :");?></strong></div><br/>

<div style="padding-left:2em;">

<?php 
// Affichage des messages du joueur
// --------------------------------
$nbMessagesAffiches = 8;
if (isset($_GET['voirtouslesmessages']))
    $nbMessagesAffiches = 50;

$query = "SELECT * FROM messages WHERE dest_joueur_id=".$userdata['joueur_id']. " AND status != ".MESSAGE_SUPPRIME." ORDER BY date DESC LIMIT ".$nbMessagesAffiches;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
}
$nbMessages = $dbHT->sql_numrows($result) ;

if ($nbMessages == 0) 
    echo T_("Tu n'as pas de message.")."<br/>";
else {
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<div>";
            $query2 = "SELECT pseudo FROM joueurs WHERE joueur_id=".$row['exp_joueur_id']. " LIMIT 1";
            if ( !($result2 = $dbHT->sql_query($query2)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query2);
            }
            if ($dbHT->sql_numrows($result2) > 0) {
                $row2=$dbHT->sql_fetchrow($result2);
                if ($row['exp_joueur_id'] == 0)
                    echo "<strong>".T_("De : ").$row2['pseudo']."</strong> ";
                else
                    echo "<strong>".T_("De : ")."<a href=\"#\" onclick=\"javascript: pop('profil.php?joueur_id=".$row['exp_joueur_id']."',null,400,600); return false;\" title=\"".T_("Voir son profil")."\">".$row2['pseudo']."</a> </strong>";
                if ($row['status'] == MESSAGE_ENVOYE)
                    echo T_("(non lu)") ;
                echo "<br/>";
                echo "<strong>".T_("Date :")."</strong> ".format_date($row['date'],true)."<br/>"; 
                echo "<strong>".T_("Actions :")."</strong> <a href=\"jeu.php?mode=m_messages&amp;supprimerMsg=".$row['message_id']."\" title=\"Supprimer ce message\">".T_("Supprimer")." <img src=\"images/supprimer.gif\" alt=\"".T_("Supprimer")."\" style=\"vertical-align:middle;\" /></a>";
                if ($row['exp_joueur_id'] != HAMSTER_ACADEMY_ID)
                    echo " - <a href=\"jeu.php?mode=m_messages&amp;envoyerMessage=1&amp;dest_joueur_id=".$row['exp_joueur_id']."\" title=\"Répondre au joueur\">".T_("Répondre")." <img src=\"images/repondre.gif\" alt=\"".T_("Répondre")."\" style=\"vertical-align:middle;\" /></a>";
                echo " - <a href=\"alerter.php?alerte_lieu=laposte\" target=\"hblank\" title=\"".T_("Message insultant/grossier : alerter Hamster Academy")."\">".T_("Alerter Hamster Academy)")." <img src=\"images/alerter_cloche.gif\" alt=\"".T_("Alerter")."\" style=\"vertical-align:middle;\" /></a><br/>"; 
                echo "<div class=\"message_body\">".$row['message']."</div>"; 
                $dbHT->sql_freeresult($result2);
            }
            else {
                echo T_("L'expéditeur de ce message s'est désinscrit du jeu.");
            }
        echo "</div><br/>";
    }
}
$dbHT->sql_freeresult($result);

// on met à jour l'indicateur de message non lu et on passe tous les messages à "lu"
$query = "UPDATE messages SET status = ".MESSAGE_LU." WHERE dest_joueur_id=".$userdata['joueur_id']. " AND status = ".MESSAGE_ENVOYE;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
}
$dbHT->sql_freeresult($result);

$query = "UPDATE joueurs SET messageNonLu = 0 WHERE joueur_id=".$userdata['joueur_id']." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in updating joueur', '', __LINE__, __FILE__, $query);
}
$dbHT->sql_freeresult($result);
$userdata['messageNonLu'] = 0;

if (!isset($_GET['voirtouslesmessages']))
    echo "<a href=\"jeu.php?mode=m_messages&amp;univers=$univers&amp;voirtouslesmessages=1\" title=\"".T_("voir tous les autres messages")."\"><span style=\"font-size:9pt;color:grey;\">(<span style=\"font-weight:bold;\">+</span> ".T_("voir tous les autres messages").")</span></a>";

?>
</div>

</div>
</div>
<div class="r"></div>
<div class="b"></div>
<div class="l"></div>
</div>

<div class="cornflex yellowbox">
<div class="t">
<div class="c">

<div><strong><?php echo T_("Mes messages envoyés :");?></strong></div><br/>

<div style="padding-left:2em;">

<?php 
// Affichage des messages envoyés du joueur
// ----------------------------------------
$query = "SELECT * FROM messages WHERE exp_joueur_id=".$userdata['joueur_id']. "  AND status != ".MESSAGE_SUPPRIME."  ORDER BY date DESC LIMIT ".$nbMessagesAffiches;
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
}
$nbMessages = $dbHT->sql_numrows($result) ;

if ($nbMessages == 0) 
    echo T_("Tu n'as pas de message envoyé.")."<br/>";
else {
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<div>";
            $query2 = "SELECT pseudo FROM joueurs WHERE joueur_id=".$row['dest_joueur_id']. " LIMIT 1";
            if ( !($result2 = $dbHT->sql_query($query2)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query2);
            }
            $row2=$dbHT->sql_fetchrow($result2);
            echo "<strong>".T_("A :")." <a href=\"#\" onclick=\"javascript: pop('profil.php?joueur_id=".$row['dest_joueur_id']."',null,400,600); return false;\" title=\"".T_("Voir son profil")."\">".$row2['pseudo']."</a> </strong>";
            if ($row['status'] == MESSAGE_ENVOYE)
                echo T_("(non encore lu par le destinataire)") ;
            echo "<br/>";
            echo "<strong>".T_("Date :")."</strong> ".format_date($row['date'],true)."<br/>"; 
            echo "<div class=\"message_body\">".$row['message']."</div>"; 
            $dbHT->sql_freeresult($result2);
        echo "</div><br/>";
    }
}
$dbHT->sql_freeresult($result);
if (!isset($_GET['voirtouslesmessages']))
    echo "<a href=\"jeu.php?mode=m_messages&amp;univers=$univers&amp;voirtouslesmessages=1\" title=\"voir tous les autres messages\"><span style=\"font-size:9pt;color:grey;\">(<span style=\"font-weight:bold;\">+</span> ".T_("voir tous les autres messages").")</span></a>";
?>
</div>

</div>
</div>
<div class="r"></div>
<div class="b"></div>
<div class="l"></div>
</div>

<br/>&nbsp;<br/>

<div class="cornflex yellowbox" style="width:660px;">
    <div class="t">
        <div class="c" style="text-align:center;">
            <div><strong><a href="jeu.php?mode=m_messages&amp;envoyerMessage=1"><?php echo T_("Envoyer un message !");?></a></strong></div>
        </div>
    </div>
    <div class="r"></div>
    <div class="b"></div>
    <div class="l"></div>
</div>

<?php 

} // fin du else "envoi d'un message / visionnage des messages reçus ou envoyés

echo afficherObjet("oeufs",7);  ?>
<div align="right">&nbsp;<?php echo insererLienIngredient(6); ?></div>

</div>
</div>




  <div style="clear:both;">&nbsp;</div>

  </div>
    
  <div class="hamBlocColonne-bottom">
     <div class="hamBlocColonne-bottom-left"></div>
     <div class="hamBlocColonne-bottom-right"></div>
     <div class="hamBlocColonne-bottom-center"></div>        
  </div> 

</div>