<?php

if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

$slogans = array(
    0 => 'Elève ton hamster<br/>Apprends-lui à devenir une star...<br/>Personnalise sa cage ...<br/>Trouve-lui des amis ...<br/>... et un groupe à l\'Academy !',
    1 => 'Hamster Academy,<br/>apprentis hamsters...<br/>pour futurs ham\'stars !<br/><i>--cecileuh10</i>',
    2 => 'Hamster Academy,<br/>on fait pipi tellement qu\'on rit !<br/><i>--lulucoeur83</i>',
    3 => 'Hamster Academy,<br/>tout est permis !!!<br/>Les hamsters sont du tonnerre!!!<br/>Les hamsters ne manquent pas d\'air !!!<br/>Les hamsters ont un mauvais caractère!!!<br/><i>--fashion collection</i> ',
    4 => 'Hamster Academy,<br/>Les hamsters ne touchent pas terre !!!<br/>Les hamsters sont tête en l\'air!!!<br/><i>--fashion collection</i>',
    5 => 'Hamster Academy<br/>le seul jeu où<br/>tes hamsters peuvent prendre<br/>50 grammes par jour !<br/><i>--Samounet</i>',
    6 => 'Hamster Academy,<br/>faites pas de conneries,<br/>il y a un jury !!!<br/><i>--kim2190</i>',
    7 => 'La Hamster Academy,<br/>c\'est comme le persil,<br/>ça laisse des trucs entre les dents<br/>qui se voient quand tu ris!<br/><i>--ely03</i>',
    8 => 'Hamsters de tous pays,<br/>Academissons-nous <br/><i>--XD</i>',
    9 => 'Hamster Academy,<br/>élevez des hamsters à l\'infini !<br/><i>--minane</i>',
    10 => 'Hamster Academy !!!<br/>Dix fois mieux que la Star Academy !!!<br/>--<i>minane</i>'
);

$nbSlogans = 11;
?>
