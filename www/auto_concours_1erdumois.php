<?php 

define('IN_HT', true);

include "common.php";

error_reporting( E_ALL );

// s'il y a le concours mensuel de la cage
if (getConfig("concours_cage_auto_active") == 1) {

    $query = "UPDATE joueurs SET inscritConcours = 0";

    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining calendrier_data', '', __LINE__, __FILE__, $query);
    }

    $dbHT->sql_freeresult($result);

    // mail bilan pour l'admin
    $TO = "contact@hamsteracademy.fr";
    $h  = "From: contact@hamsteracademy.fr";
    $subject = "Concours: remise à zéro des inscriptions" ;
    $message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
    $mail_sent = @mail($TO, $subject, $message, $h);
}
?>