<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

return;

$modeConcours = MODE_EN_COURS ;

if ($modeConcours == MODE_RESULTAT) {
    
    $msg .= "<div class=ham1Nouvelle><h2 align=center>Résultats du concours de la plus belle cage</h2>";
    
    $query = "SELECT l.*, j.joueur_id, j.pseudo FROM lst_cages_concours l, joueurs j WHERE j.joueur_id = l.joueur_proprietaire_id ORDER BY (l.note + nb_accessoires*5) DESC";
    
    $msg .= "Ceci est le résultat des votes de tous les joueurs du jeu.<br/>&nbsp;<br/>";
    
    $msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"Coupe\"></div><br/>";
    $msg .= "<strong>Félicitations au vainqueur erwan79 ! Il gagne 500 pièces et un diplôme !</strong><br/>     Le deuxième joueur (loops1979) gagne 200 pièces (bravo à lui aussi !)<br/>...<br/>Jusqu'au dixième qui gagne 50 pièces. Les autres gagnants ont aussi leur cadeau (des pièces et des bonbons pour leurs hamsters).<br/>&nbsp;<br/>";
    $msg .= "En tout cas, félicitations à tous ! On ne s'attendait pas à d'autant si belles cages ! Bravo tout le monde ! ";
    $msg .= "Notez dès maintenant que d'autres concours vont arriver, vous allez pouvoir retenter votre chance ! Bon, trève de bavardages, voici le classement !";

    if (!isset($_GET['tous_les_joueurs'])) {
        $msg .= "<h3 align=center>Les 100 premiers</h3>";
        $query .= " LIMIT 100";
    }
    else
        $msg .= "<h3 align=center>Tous les joueurs</h3>";

    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
    }
    
    $msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>Classement</strong></td><td><strong>Nom du joueur</strong></td><td align=center><strong>Voir sa cage</strong></td><td align=center><strong>Note / 10</strong></td></tr>";
    $classement = 1;
    $precNote = -1;
    while ($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = round( 5+( (($row['note']+$row['nb_accessoires']*5) * 5) / 230), 1 );
        
        $msg .= "<tr><td align=center>";
        if ($noteArrondie == $precNote)
            $msg .= "ex-aequo";
        else
            $msg .= $classement;
            
        
        $msg .= "</td><td>".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td><td align=center>".returnLienCage($row['cage_id'],"Voir sa cage")."</td><td align=center>".$noteArrondie."</td></tr>";

        $classement ++;
        $precNote = $noteArrondie;
    }
    $dbHT->sql_freeresult($result);
    $msg .= "</table><br/>";
    
    $msg .= "<div align=center><a href=\"jeu.php?mode=m_concours&tous_les_joueurs=1\">Voir le classement de tous les autres joueurs</a></div>" ;
    
    $msg .= "</div>";
}
// gestion de l'inscription au concours
else if ($modeConcours == MODE_EN_COURS) {
    if ($userdata['inscritConcours'] > 1 && $nbCages > 1) {
        $msg .= "<div class=ham1Nouvelle><u>Concours</u> : Tu es inscrit(e) au concours et tu as choisi ta cage (nom de la cage \"".$lst_cages[cageCorrespondante($userdata['inscritConcours'])]['nom']."\") ! </div>";
    }
    else if ($userdata['inscritConcours'] == 1 && $nbCages > 1) { // désactivé
        $msg .= "<div class=ham1Nouvelle><u>Concours</u> : c'est bon, tu es inscrit(e) au concours ! <strong>Par contre</strong>, vu qu'il y a beaucoup de joueurs inscrits, <strong>tu dois maintenant choisir la cage</strong> qui sera votée par les autres joueurs.</div>";
        $msg .= "<br/><div><u>Choisis ta cage pour le concours</u> (clique sur son nom) : ";
        for($cage=0;$cage<$nbCages;$cage++) {
            $msg .= "<br/><a href=\"jeu.php?mode=m_accueil&action=inscrireCageConcours&cage_id=".$lst_cages[$cage]['cage_id']."\" title=\"Choisir cette cage\">".$lst_cages[$cage]['nom']."</a>";
        }
        $msg .= "</div>";
        
    }
}
else if ($modeConcours == MODE_VOTE) {
    if ($userdata['inscritConcours'] == 1){
        $msg .= "<div class=ham1Nouvelle><u>Concours</u> : tu es inscrit(e) au concours ! Les votes commenceront autour du 15 décembre ! Prépare tes alliés pour qu'ils votent pour toi !</div>";
    }
    else {
        $msg .= "<div class=ham1Nouvelle><strong>L'inscription au concours est terminé</strong>. Les votes ont commencé (fin le 20 décembre). Il y aura un cadeau pour chaque joueur qui aura voté !</div>" ;
        
        //$msg .= "<div class=ham1Nouvelle><strong>Appel aux éleveurs de Hamster Academy ! :-)</strong><br/>&nbsp;<br/><u>Le concours de la plus belle cage est ouvert</u> ! Tu as jusqu'au 12 décembre pour t'inscrire, dépêche-toi si tu veux participer !<br/>&nbsp;<br/>" ;
        //$msg .= "<u>Règles</u> :<br/> - Une fois les inscriptions terminées, chaque joueur pourra voter les 10 cages qu'il préfère. <br/>- Le vainqueur aura 500 pièces et un diplôme par courrier. Le deuxième aura 200 pièces ... le dixième 50 pièces. Les 100 premiers joueurs auront 20 pièces.<br/>";
        //$msg .= "- Les cages peuvent être modifiées et aménagées jusqu'au 12 décembre, y compris après l'inscription.<br/>";
        //$msg .= "- Coût d'inscription au concours : 10 pièces. Bonne chance !<br/>&nbsp;<br/>";
        //$msg .= "<table cellpadding = 0 cellspacing = 0><tr valign=baseline><td>Je m'inscris (10 ".IMG_PIECE.") :&nbsp;&nbsp;</td><td><form action=\"jeu.php\" method=get><input type=hidden name=mode value=m_accueil><input type=hidden name=action value=inscrireConcours><input type=submit name=inscriptionConcours value=\"S'inscire au concours !\"></form></td></tr></table></div>";
    }

    // gestion du vote pour le concours
    // --------------------------------
    if ($userdata['a_vote'] == 1) {
        $msg .= "<div class=ham1Nouvelle><u>Concours</u> <strong>Ton vote pour le concours</strong> a été pris en compte ! L'Hamster Academy te remercie (tu auras 1 cadeau à la fin du concours). Les résultats de tous les votes seront disponibles le 20 décembre !</div>";
    }
    else {
        if (!isset($_GET['vote'])) {
            // le joueur veux voter
            $msg .= "<div class=ham1Nouvelle><u>Concours</u> <strong>Le vote pour le concours</strong> est lancé ! Tu as jusqu'au 20 décembre pour le faire. <a href=\"jeu.php?mode=m_accueil&vote=1\">Veux-tu le faire maintenant (clique ici) ? </a> Bon à savoir : chaque joueur ayant voté aura un cadeau pour le remercier !</div>";
        }
        else {
            
            // est-ce que le joueur vient de voter ?
            $bulletin_valide = 0;
            if (isset($_GET['a_vote']) && ! isset($_GET['validPseudo1']) && ! isset($_GET['validPseudo2']) ) {
                
                // on récupère la liste des cages à voter
                $msg = " ";
                require "concoursLstCages.php";
                
                // liste des cages votées
                while($row=$dbHT->sql_fetchrow($result)) {
                    $noteCage = 0;
                    
                    if (isset($_GET['cageNotee_'.$row['cage_id'].''])) {
                        $noteCage = intval($_GET['cageNotee_'.$row['cage_id'].'']);
                        if ($noteCage < 0)
                            $noteCage = 0;
                        else if ($noteCage > 5)
                            $noteCage = 5;
                        
                        // on récupère la précédente note de la cage
                        $query15 = "SELECT cage_id, note FROM lst_cages_concours WHERE cage_id=".$row['cage_id']." LIMIT 1" ;
                        if ( !($result15 = $dbHT->sql_query($query15)) ){
                            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query15);
                        }
                        if ($dbHT->sql_numrows($result15) == 1){
                            $row15=$dbHT->sql_fetchrow($result15);
                            $nouvelleNoteCage = $row15['note'] + $noteCage ;
                            $dbHT->sql_freeresult($result15);
                            
                            // mise à jour de la note
                            $query16 = "UPDATE lst_cages_concours SET note = ".$nouvelleNoteCage." WHERE cage_id=".$row15['cage_id']." LIMIT 1" ;
                            if ( !($dbHT->sql_query($query16)) ){
                                message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query16);
                            }
                        }
                    }
                }
                
                $bulletin_valide = 1;
                
                echo "<strong>Ton vote a bien été pris en compte !</strong> <br/>&nbsp;<br/>Pour te remercier, tu auras un cadeau dès que le vote sera terminé le 20 décembre." ;
                $query16 = "UPDATE joueurs SET a_vote = 1 WHERE joueur_id=".$userdata['joueur_id']." LIMIT 1" ;
                if ( !($dbHT->sql_query($query16)) ){
                    message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query16);
                }
                echo "<div align=center><a href=\"jeu.php?mode=m_accueil\">Clique-ici pour revenir au jeu</a></div>";
            }
            
            if ($bulletin_valide == 0 || ! isset($_GET['a_vote']) || isset($_GET['validPseudo1']) || isset($_GET['validPseudo2'])) { // si le joueur n'a pas encore voté, ou s'il y a un pb ou un chgt de nom
            
                require "graphique.php" ;    
                echo "<h2 align=center>Ton bulletin de vote pour le concours</h2>";
                $msg = " ";
                
                // on récupère la liste des cages à voter
                require "concoursLstCages.php";
                
                echo "<div align=center>Donne une note pour ces ".$nbCages." cages : </div><br/>";
                
                // début du formulaire
                echo "<form action=\"jeu.php\" method=get>";
                echo "<input type=hidden name=mode value=m_accueil>";
                echo "<input type=hidden name=vote value=1>";
                echo "<input type=hidden name=a_vote value=1>";
                
                // on affiche la cage i
                while($row=$dbHT->sql_fetchrow($result)) {
                    $row['proprete']=10;
                    
                    // liste des accessoires pour cette cage i
                    // ---------------------------------------
                    $query5 = "SELECT * FROM accessoires WHERE cage_id=".$row['cage_id']." ORDER BY type" ;
                    if ( !($result5 = $dbHT->sql_query($query5)) ){
                        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
                    }
                    $nbAccessoires = $dbHT->sql_numrows($result5) ;
                    $lst_accessoires=array();
                    while($row5=$dbHT->sql_fetchrow($result5)) {
                        array_push($lst_accessoires,$row5);
                        $row['nbAccessoires'] ++ ;
                    }
                    $dbHT->sql_freeresult($result5);
                    
                    afficherCage($row, false) ;
                    
                    echo "<br>Note (notation : 1=> pas terrible, 5 => superbe cage) : ";
                    echo "<select name=cageNotee_".$row['cage_id'].">";
                    $optionDejaSelectionnee = -1;
                    if (isset($_GET['cageNotee_'.$row['cage_id'].'']))
                        $optionDejaSelectionnee = intval($_GET['cageNotee_'.$row['cage_id'].'']);
                    
                    echo "<option value=\"1\" ".( ($optionDejaSelectionnee == 1)? "selected" : "" )." >1</option>\n";
                    echo "<option value=\"2\" ".( ($optionDejaSelectionnee == 2)? "selected" : "" )." >2</option>\n";
                    echo "<option value=\"3\" ".( ($optionDejaSelectionnee == 3)? "selected" : "" )." >3</option>\n";
                    echo "<option value=\"4\" ".( ($optionDejaSelectionnee == 4)? "selected" : "" )." >4</option>\n";
                    echo "<option value=\"5\" ".( ($optionDejaSelectionnee == 5)? "selected" : "" )." >5</option>\n";
                    echo "</select>";
                    echo "<div align=center><strong>".$row['nom']."</strong></div>";
                    echo "<hr/>";
                    echo "<br/>";
                }
                $dbHT->sql_freeresult($result);
                
                echo "Tu peux ajouter une cage de ton choix à la liste. Il faut indiquer le pseudo du joueur qui possède cette cage (à condition qu'il soit inscrit au concours bien sûr) :<br/>" ;
                if ($pseudoCageSupp1 != "")
                    echo "Actuellement, c'est la cage de <strong>".$pseudoCageSupp1. "</strong> que tu as choisie. Si tu veux changer la cage, choisis un autre joueur dont tu veux noter sa cage : ";
                echo "<input type=text size=50 name=pseudoCageSupp1 value=\"".$pseudoCageSupp1."\"><input type=submit name=validPseudo1 value=\"Ajouter la cage de ce joueur\"><br/>&nbsp;<br/>";
                
                echo "Tu peux ajouter une deuxième cage de ton choix à la liste. Il faut indiquer le pseudo du joueur qui possède cette cage (à condition qu'il soit inscrit au concours bien sûr) :<br/>" ;
                if ($pseudoCageSupp2 != "")
                    echo "Actuellement, c'est la cage de <strong>".$pseudoCageSupp2. "</strong> que tu as choisie. Si tu veux changer la cage, choisis un autre joueur dont tu veux noter sa cage : ";
                echo "<input type=text size=50 name=pseudoCageSupp2 value=\"".$pseudoCageSupp2."\"><input type=submit name=validPseudo2 value=\"Ajouter la cage de ce joueur\"><br/>";
                
                echo "<hr/><br/>";
                echo "<div align=center><strong>Ne valide le vote que si tu as choisi les deux cages supplémentaires (non obligatoire) et, surtout, que tu les a toutes notées !</strong></div>";
                echo "<br><div align=center><input type=submit name=validerBulletin value=\"Valider le bulletin de vote\"></div>";
                echo "</form>";
            }
        }
    }
}
