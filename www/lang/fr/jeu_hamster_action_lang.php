<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}


if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'CREME_HAMSTEROPOIL_VIDE' => 'Tu n\'as pas de cr�me HamsterOPoil pour le hamster... Il faut en acheter !',
	'CREME_HAMSTEROPOIL_OK' => 'Superbe ! Ton hamster a repris du <i>poil de la b�te</i> comme on dit !',
	'CREME_HAMSTEROPOIL_TROPTOT' => 'Attends un peu avant de remettre de la cr�me, il faut qu\'elle soit absorb�e par le poil pendant quelques heures...',
	'DON_LIVRE_BEAUTE_OK' => 'Ton hamster a lu tout le livre et a appris tous les trucs ! (ouf) Regarde son niveau de coquetterie, il atteint 5 sur 5 !',
	'DON_LIVRE_BEAUTE_VIDE' => 'Tu n\'as pas de Trait� de beaut�. Il faut en acheter un pour chaque hamster !',
    'TROUVE_BOIS' => 'Ton hamster a trouv� du bois en for�t !',
    'TROUVE_BOIS_MAIS_TROP_FAIBLE' => 'Ton hamster a trouv� du bon bois en for�t mais il est encore trop peu muscl� pour en ramener... peut-�tre un peu plus tard dans le jeu...',
    'TROUVE_HAMSTER' => 'Tu as retrouv� ton hamster, il �tait en for�t !',
    'PAS_DE_RAQUETTE' => 'Il faut une raquette et des balles de tennis pour jouer !',
    'PAS_DE_SORTIE_TENNIS' => 'Il faut que tu ach�tes une sortie au tennis pour jouer !',
    'SORTIE_TENNIS_OK' => 'Le match de tennis s\'est super bien pass� ! M�me Yannick Noah est venu voir ton hamster ! Il faut continuer � progresser, �tre toujours plus fort, et surtout, s\'amuser ! ',
    'COURS_BLAGUE_MAUVAIS_MORAL' => 'Le cours de blague est toujours int�ressant mais le moral de ton hamster est si bas que finalement, il n\'a fait rire personne tellement il est triste...'
));

?>
