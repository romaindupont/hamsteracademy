<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}


if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'COMPTE_SUPPRIME' => 'Le compte a �t� supprim�',
	'COMPTE_SUSPENDU' => 'Ce compte a �t� temporairement suspendu.<br/>Consulte-vite l\'Hamster Academy pour savoir pourquoi',
	'INDEX_DESCRIPTIF' => 'El�ve ton hamster<br>Apprends-lui &agrave; devenir une star...<br>Personnalise sa cage ...<br>Trouve-lui des amis ... <br>... et un groupe � l\'Academy !',
	'DEJA_INSCRIT' => 'D�j� inscrit ? Saisis ton pseudo et ton mot de passe pour jouer !',
	'PSEUDO' => 'Pseudo',
	'PASSWORD' => 'Mot de passe',
	'BAD_PASSWORD' => 'Mot de passe incorrect',
	'LIEN_PASSWORD' => 'Si tu l\'as oubli�, clique sur le lien :',
	'FIND_PASSWORD' => 'retrouver mon mot de passe ',
	'ENTRE_HA' => 'Entrer dans l\'Hamster Academy',
	'SINON_INSCRIT' => 'Sinon, inscris-toi',
	'VOIR_DEMO' => 'voir la d�mo',
	'SINSCRIRE' => 'S\'inscrire',
	'LAST_NEWS' => 'Les derni�res nouveaut�s',
	'LINK_NEWS_TITLE' => 'Clique-ici pour voir toutes les derni�res nouveaut�s du jeu !',
	'LINK_NEWS_TEXT' => 'Et toutes les autres nouveaut�s...',
	'LINK_IDEE_TITLE' => 'Clique sur la bo�te pour y mettre une remarque ou une id�e !',
	'TITRE_IDEE' => 'Bo�te � id�e',
	'DESC_IDEE' => 'Clique sur la bo�te � id�e pour faire part de tes remarques et id�es. <br/>&nbsp;<br/>R�compense aux meilleures id�es !',
	'LINK_REGLES_TITLE' => 'Les r�gles du jeu',
	'TITRE_REGLES' => 'Les r�gles du jeu',
	'DESC_REGLES' => 'Les r�gles du jeu sont remplies d\'astuces et de bonnes id�es pour bien avancer dans le jeu.',
	'LINK_FORUM_TITLE' => 'Forum du jeu',
	'LINK_FORUM_TITLE' => 'Le Forum du jeu',
	'DESC_FORUM' => 'Pour discuter avec les autres joueurs, pour d�velopper des strat�gies de groupes, trouver des solutions, etc. Mais seuls les joueurs inscrits peuvent y laisser un message.',
	'LINK_BLOG_TITLE' => 'Le Blog du jeu',
	'TITRE_BLOG' => 'Le Journal du jeu (blog)',
	'DESC_BLOG' => 'Toutes les derni�res nouveaut�s sur le jeu, plein de bonus sur les hamsters, des photos, des vid�os... ',
	'ALSO' => 'Et aussi',
	'DEMO_JEU' => 'D�monstration du jeu',
	'BONUS' => 'Bonus',
	'PARTENAIRE' => 'Partenaires',
	'CREDITS' => 'Cr�dits',
	'CONTACT' => 'Pour tout contact',
	'PASSWORD_LOST' => 'Mot de passe perdu',
	'FIND_IT' => 'le retrouver',
	'ALL_RIGHTS'=>'Tous droits r�serv�s'
));

?>
