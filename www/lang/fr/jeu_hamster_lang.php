<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}


if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'DON_LIVRE_BEAUTE' => "Donner le Trait� de beaut�",
	'STAGE_PLEIN_AIR_OK' => "Ton hamster est parti en stage de plein-air � la mer ! Son moral est reparti � la hausse !",
	'STAGE_PLEIN_AIR_ACHAT' => "Il faut que tu ach�tes un stage de plein-air avant de pouvoir l'utiliser.",
    'DON_FIOLE' => "Tu as donn� la fiole d'�nergie � ton hamster ! Le voil� plein d'�nergie. A toi d'en faire bon usage !",
    'HAMSTER_EVADE' => "Ton hamster s'est �vad� ! Retrouve-le vite avant qu'il disparaisse pour de bon dans la nature !",
    'HAMSTER_EVADE_DEUIS_VETO' => "Ton hamster s'est �vad� du v�to ! Retrouve-le vite avant qu'il disparaisse pour de bon dans la nature !",
    'A_FAIT_DU_VELO' => "#1 a fait du v�lo aujourd'hui ! Quel sportif !"
));

?>
