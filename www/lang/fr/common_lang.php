<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}


if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$lang = array_merge($lang, array(
	'LANGUE'	=> 'Francais',
	'default_dateformat'	=> 'D M d, Y g:i a', // Mon Jan 01, 2007 1:37 pm
	'HEADER_TITRE_INDEX' => 'Hamster Academy - Jeu d\'�levage virtuel de hamsters',
	'HEADER_DESCRIPTION_GENERIC' => 'Hamster Academy, Jeu virtuel d\'�levage de hamsters ! Adopte un hamster, personnalise sa cage, trouve-lui des amis, apprends-lui � jouer de la musique et � danser puis fonde ton groupe ! Jeu gratuit !',
	'HEADER_KEYWORDS' => 'Hamster, Academy, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer',
	'ERREUR_INCONNUE' => 'Erreur inconnue',
	'HAMSTER_AU_REFUGE' => 'Les hamsters sont au Refuge de l\'Academy. Pour les r�cup�rer, va dans la page d\'accueil et clique sur "R�cuperer les hamsters"',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => ''
));

?>
