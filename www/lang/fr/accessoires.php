<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}


if (empty($lang) || !is_array($lang))
{
	$lang = array();
}

$accessoire_desc = array(
	'0'	=> 'Francais',
	'1'	=> 'D M d, Y g:i a',
	'2' => 'Hamster Academy - Jeu d\'�levage virtuel de hamsters',
	'3' => 'Hamster Academy, Jeu virtuel d\'�levage de hamsters ! Adopte un hamster, personnalise sa cage, trouve-lui des amis, apprends-lui � jouer de la musique et � danser puis fonde ton groupe ! Jeu gratuit !',
	'4' => 'Hamster, Academy, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer',
	'5' => 'Erreur inconnue',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => '',
	'' => ''
);

?>
