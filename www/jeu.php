<?php 

define('IN_HT', true);

include "common.php";
include "lstAccessoires.php";
include "lstMessages.php";
include "lstHamsters.php" ;
include "lstNiveaux.php" ;
include "gestion.php";
include "facebook.php";

error_reporting( E_ALL );

// login du joueur
$login = 0;
if (isset($_POST['login']) || isset($_GET['login']) || isset($_GET['loginFacebook']) ) {
    
    $login = 1;

    if (isset($_GET['loginFacebook']) && $sessionFacebook) {
        
        try {
            $compteFacebook = $facebook->api('/me');
        } 
        catch (FacebookApiException $e) {
            error_log($e);
        }
        $joueur_id = getJoueurIdFromFacebookId($compteFacebook['id']) ;
        if ($joueur_id == -1) {
            redirectHT("index.php?err=6");
        }
        
        // récupération de la date de dernière connexion
        $sql = "SELECT user_lastvisit FROM joueurs WHERE joueur_id = ".$joueur_id;
        $result = $dbHT->sql_query($sql);
        $row = $dbHT->sql_fetchrow($result);
        $derniereConnexion = $row['user_lastvisit'];
        $dbHT->sql_freeresult($result);
        
        $session_id = session_begin($joueur_id, $user_ip);
        if( $session_id ){
                    
            // arrivé ici = joueur correctement loggé, on récupère le userdata
            $query = "SELECT u.*, s.*
                FROM sessions s, joueurs u
                WHERE s.session_id = '$session_id'
                    AND u.joueur_id = s.session_user_id";
            if ( !($resultUserdata = $dbHT->sql_query($query)) )
            {
                message_die(CRITICAL_ERROR, 'Error doing DB query userdata row fetch', '', __LINE__, __FILE__, $query);
            }
            
            $userdata = $dbHT->sql_fetchrow($resultUserdata);
            $dbHT->sql_freeresult($resultUserdata);
            
            // connection au forum punBB
            define('PUN_ROOT', './forum/');
            require PUN_ROOT.'include/common.php';

            $form_password_hash = pun_hash($userdata['passwd']);
            $expire = time() + 31536000 ;
            pun_setcookie($joueur_id, $form_password_hash, $expire);

            // on sauve un cookie pour remémorrer le pseudo du joueur en page de login
            setcookie ("HamsterAcademyPseudo", $userdata['pseudo'], time()+2592000, '/' );                    
            
            // pour la suite du programme
            $dbHT->sql_activate();
        }
        else
            redirectHT("index.php?err=session_impossible");
    }
    else {
        require "login.php";
    }
 
    // à la connexion du joueur, on met à jour son user_lastvisit et son ip
    $queryIP = "UPDATE joueurs SET user_lastvisit = ".$dateActuelle.", last_ip = '".$user_ip."' WHERE joueur_id = ".$joueur_id." LIMIT 1";
    if ( ! $dbHT->sql_query($queryIP)){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $queryIP);
    }
}
else
    $userdata = session_pagestart($user_ip);

if( ! $userdata['session_logged_in'] ) { 
    echo T_("Erreur de connection, contacte Hamster Academy si le problème persiste.");
    redirectHT("index.php?err=session_expiree");
}
if($userdata['suspendu'] > 0) {  // si le joueur a été suspendu, on le renvoit à la page d'accueil si c'est trop tôt
    
    if ($userdata['suspendu'] == 1) // l'équivalent d'une durée indéterminée
        redirectHT("index.php?err=4");
        
    else if ($dateActuelle > $userdata['suspendu']) { 
        // fin de la suspension
        $query = "UPDATE joueurs SET suspendu = 0 WHERE joueur_id = ".$userdata['joueur_id']. " LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }        
    }
    else 
        redirectHT("index.php?err=4&amp;finSuspension=".$userdata['suspendu']);
}

$univers = UNIVERS_ELEVEUR;
$erreurAction = 0;
$infoAction = 0;
$constructionNidCommencee = false;
$constructionCabaneCommencee = false;
$action = "";
$msg = "" ; // messages adressés au joueur
$salaireVerse = false;
$aideNiveau1 = 0;

// liste des hamsters
// ----------------------
$lst_groupes = array();
$query = "SELECT * FROM hamster WHERE joueur_id=".$userdata['joueur_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamsters = $dbHT->sql_numrows($result) ;
$lst_hamsters=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($lst_hamsters,$row);
    
    if ($row['groupe_id'] != -1)
        array_push($lst_groupes,$row['groupe_id']);
}
$dbHT->sql_freeresult($result);

// liste des cages
// ----------------------
$query = "SELECT * FROM cage WHERE joueur_id=".$userdata['joueur_id'];
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;
$lst_cages=array();
while($row=$dbHT->sql_fetchrow($result)) {
    array_push($row,0);
    $row['nbAccessoires'] = 0;
    array_push($lst_cages,$row);
}
$dbHT->sql_freeresult($result);

// liste des accessoires pour les cages
// ------------------------------------
updateListeAccessoires();

// quel menu ?
$modeAffichage="m_accueil";
if (isset($_GET['mode']))
    $modeAffichage=mysql_real_escape_string($_GET['mode']);
else if (isset($_POST['mode']))
    $modeAffichage=mysql_real_escape_string($_POST['mode']);

if (isset($_GET['univers'])){
    $univers = intval($_GET['univers']);
}
else {
    
    if ($modeAffichage == "m_accueil" || $modeAffichage == "m_hamster" || $modeAffichage == "m_cage" || $modeAffichage == "m_metier" || $modeAffichage == "m_aide" || $modeAffichage == "m_mariages") 
        $univers = UNIVERS_ELEVEUR;
    else if ($modeAffichage == "m_classements" || $modeAffichage == "m_aide_a"  || $modeAffichage == "m_groupe")
        $univers = UNIVERS_ACADEMY;
}
    
// récupération de la cage/hamster/accessoire courant (id et index : id = dans la BDD, index = en memoire)
$pagePrecedente = "m_accueil";
if (isset($_GET['pagePrecedente']))
    $pagePrecedente = mysql_real_escape_string($_GET['pagePrecedente']);
    
if ($univers == UNIVERS_ACADEMY)
    require_once "lstInstruments.php" ;
    
$hamster_id = -1 ;
$hamsterIndex = -1;
if ($nbHamsters > 0) {
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']);
        if ($hamster_id == -1) {
            $hamsterIndex=0 ; // par defaut, on affiche le premier hamster
            $hamster_id = $lst_hamsters[0]['hamster_id'] ;
        }
        else
            $hamsterIndex = hamsterCorrespondant($hamster_id) ;
    }
    else {
        if (isset($_POST['hamster_id'])) {
            $hamster_id = intval($_POST['hamster_id']);
            $hamsterIndex = hamsterCorrespondant($hamster_id) ;
        }
        else {
            $hamsterIndex=0 ; // par defaut, on affiche le premier hamster
            $hamster_id = $lst_hamsters[0]['hamster_id'] ;
        }
    }
}

$cage_id = -1 ;
$cageIndex = -1;
if (isset($_GET['cage_id'])) {
    $cage_id = intval($_GET['cage_id']);
    $cageIndex = cageCorrespondante($cage_id) ;
}
else { //  choisit la 1ère cage par défaut
    $cageIndex = 0;
    $cage_id = $lst_cages[$cageIndex]['cage_id'];
}

$accessoire_id = -1;
$accessoireIndex = -1;
if (isset($_GET['accessoire_id'])){
    $accessoire_id = intval($_GET['accessoire_id']) ;
    $accessoireIndex = accessoireCorrespondant($accessoire_id);
}

// verification sur hamster_id, accessoire_id et cage_id
if ($cage_id != -1 && $cageIndex == -1) 
    redirectHT("index.php?triche=1");
if ($hamster_id != -1 && $hamsterIndex == -1)
    redirectHT("index.php?triche=2");
if ($accessoire_id != -1 && $accessoireIndex == -1)
    redirectHT("index.php?triche=3");
    
// Oeufs de Paques
if (isset($_GET['oeufpaques'])) {
    $oeuf = intval($_GET['oeufpaques']) ;
    
    for($oeufIndex=0;$oeufIndex < 10;$oeufIndex++){
        if ($lstOeufs[$oeufIndex] == $oeuf) {
            // le joueur a trouvé un oeuf
            $statut = statutObjetTrouve($userdata['lstObjetsTrouves'],$oeufIndex) ;
            // l'oeuf n'a pas encore été trouvé
            if ($statut == false) {
                
                // on met à jour le statut
                $msg .= "<div><img src=\"images/oeufs/oeufs".$oeufIndex.".gif\" align=absmiddle> ".T_("Tu as trouvé un oeuf")." ! </div>";
                
                updateObjetsTrouvesJoueur(10,$oeufIndex,true);
                                
            }
            break;
        }
    }
}

// évolution du niveau
verifierMissions();

// vérification que le joueur a bien fini ses missions (en cas de bug, rajout de missions, etc.)
if ($modeAffichage == "m_accueil")
    updateNiveauJoueur(-1,false);

// message(s) à la poste
if ($userdata['messageNonLu'] == 1 && $modeAffichage != "m_messages") 
    $msg .= "<div><span style=\"margin-right:10px;\"><a href=\"jeu.php?mode=m_messages\"><img src=\"images/enveloppe.gif\" align=\"absmiddle\" alt=\"\" /></a></span> <span><a href=\"jeu.php?mode=m_messages\">".T_("Tu as un ou plusieurs message(s). Aller à la Poste")." !</a></span></div>";

// évolution de l'énergie
for($ham=0;$ham < $nbHamsters ; $ham++) {
    if ($dateActuelle - $lst_hamsters[$ham]['majEnergie'] > 60) {
        crediterEnergie( ($dateActuelle - $lst_hamsters[$ham]['majEnergie']) / $intervalleMajEnergie,$ham);
    }
}

// débit des pièces pour l'éolienne
$eolienneEnRoute = false;
for($c=0;$c < $nbCages ; $c++) {
    if ($lst_cages[$c]['eolienne_activee'] > 0){
        
        // on vérifie qu'il y a une bien une éolienne dans la cage
        if (yatilAccessoireDansLaCage($lst_cages[$c]['cage_id'],ACC_EOLIENNE_ENERGIE) != -1) {
    
            $eolienneEnRoute = true;
            if ( ($dateActuelle - $lst_cages[$c]['eolienne_activee']) > 60 ) {
                
                $nbPieces = min(100,round( ($dateActuelle - $lst_cages[$c]['eolienne_activee']) / 60 ));
                
                if ($nbPieces >= 100 && $userdata['nb_pieces'] < 1000) 
                    envoyerMessagePrive($userdata['joueur_id'],T_("(Message automatique) Aurais-tu oublié d'éteindre l'éolienne ?"));
                
                $resultat = debiterPieces(min($nbPieces,$userdata['nb_pieces']),"eolienne") ;
                if ($resultat == 1) {
                    $query = "UPDATE cage SET eolienne_activee = ".$dateActuelle." WHERE cage_id=".$lst_cages[$c]['cage_id']; 
                     $dbHT->sql_query($query);
                }
                else{
                    $query = "UPDATE cage SET eolienne_activee = 0 WHERE cage_id=".$lst_cages[$c]['cage_id']; 
                    $dbHT->sql_query($query);
                }
            }
        }
        else {
            $query = "UPDATE cage SET eolienne_activee = 0 WHERE cage_id=".$lst_cages[$c]['cage_id']; 
            $dbHT->sql_query($query);
        }
    }
}
    
if (isset($_GET['inscription'])) { // si inscription du joueur ...
    $inscription = intval($_GET['inscription']);
    majEtapeInscription($inscription) ;
}
else { // on attend la fin de l'inscription pour faire quoique ce soit ...
    
    // update salaire (tous les jours et seulement si le joueur se trouve sur la place accueil)
    // --------------------------------
    $salaireVerse = false;
    if ($modeAffichage == "m_accueil") {
        
        require_once "lstMetiers.php";
        
        if (($dateActuelle - $userdata['date_dernier_salaire']) > 86400) {
            $nbSalaires = floor(($dateActuelle - $userdata['date_dernier_salaire']) / 86400);
            
            $nbSalaires = min($nbSalaires,$nbJoursSalairesSansConnexion); // x jours de salaire versé max sans se connecter
            
            $nbPiecesCreditees = $nbSalaires * $lstMetiers[$userdata['metier']][METIER_SALAIRE] ;
            crediterPieces($nbPiecesCreditees,true) ;
            $salaireVerse = true;
            $msgTmp = T_("Ton salaire a été versé")." ($nbPiecesCreditees ".IMG_PIECE.")";
            if ($nbSalaires > 1) {
                $msgTmp .= " (<span ".tooltip("<div align='left'>".str_replace("#1",$nbJoursSalairesSansConnexion,T_("Rappel sur le salaire :<br/>Au delà de #1 jours sans connexion, seuls #1 jours de salaire sont versés.<br/>Il est donc conseillé se connecter au moins une fois tous les #1 jours"))." !</div>").">".str_replace("#1",$nbSalaires,T_("correspondant à #1 jours de salaire"))."</span>)";
            }
            $msgTmp = "<div><img src=\"images/coffre_pieces.gif\" align=absmiddle>".$msgTmp.".</div>";
            $msg .= $msgTmp. "<br/>" ;
            
            // on profite du versement du salaire pour mettre à jour le compteur de connexions depuis le debut du mois.
            $queryNbConnexions = "UPDATE joueurs SET nbConnectionsDansLeMois = ".($userdata['nbConnectionsDansLeMois']+1)." WHERE joueur_id = ".$userdata['joueur_id']." LIMIT 1";
            $dbHT->sql_query($queryNbConnexions);
        }
    }
    
    $actionEffectuee = true;
    
    // Achat
    if (isset($_GET['achat'])) {
        $objet_a_acheter = intval($_GET['achat']);
        require("achat.php");
    }    
    // Action
    else if (isset($_GET['action'])) {
        $action = mysql_real_escape_string($_GET['action']);
        require("action.php");
    }
    else if (isset($_POST['action'])) {
        $action = mysql_real_escape_string($_POST['action']);
        require("action.php");
    }
    else if (isset($_GET['actionAcademy'])) {
        $action = mysql_real_escape_string($_GET['actionAcademy']);
        require("actionAcademy.php");
    }
    else if (isset($_POST['actionAcademy'])) {
        $action = mysql_real_escape_string($_POST['actionAcademy']);
        require("actionAcademy.php");
    }
    else if (isset($_GET['actionInteraction'])) {
        $action = mysql_real_escape_string($_GET['actionInteraction']);
        require("actionInteraction.php");
    }
    else if (isset($_POST['actionInteraction'])) {
        $action = mysql_real_escape_string($_POST['actionInteraction']);
        require("actionInteraction.php");
    }
    else if(isset($_GET['bebe'])) {
        require("actionBebe.php");
        
        $msgTmp="";
        if ($erreurAction == 0) 
            $msgTmp=T_("La grossesse est partie. Il faut attendre environ 1 semaine pour voir les bébés")."&nbsp;!";
        else if ($erreurAction == 2) 
            $msgTmp = T_("Hum ! Ta femelle est déjà enceinte !");
        else if ($erreurAction == 3) 
            $msgTmp = T_("Il faut un nid dans la cage pour accueillir les futurs bébés !");
        else if ($erreurAction == 1) 
            $msgTmp = T_("Hum ! Tu as choisi une femelle pour avoir des bébés avec ta femelle ! Ce n'est pas possible !");
        $msg .= afficherNouvelle("couffin.gif",$msgTmp);
    }
    else if(isset($_GET['supprimerMsg'])) {
        $messageId = intval($_GET['supprimerMsg']);
        $query = "UPDATE messages SET status = ".MESSAGE_SUPPRIME." 
            WHERE message_id = ".$messageId." AND dest_joueur_id=".$userdata['joueur_id']." LIMIT 1";    
        if ( !($dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error deleting message ', '', __LINE__, __FILE__, $query);
        }    
    }
    else
        $actionEffectuee = false;

    if ($actionEffectuee) {
        $userdata['note'] = miseAJourPointsJoueur($userdata['joueur_id']);
    }
    
    // on fait évoluer les hamsters toutes les 24 heures (s'ils ne sont pas en mode pause)
    if ($nbHamsters > 0 && ($dateActuelle - $userdata['date_maj_hamster'] > 86400) )
        miseAJourQuotidienne($userdata['joueur_id']);
        
    // on fait évoluer le score toutes les 5 minutes
    if ($dateActuelle - $userdata['date_maj_note'] > 300)
        $userdata['note'] = miseAJourPointsJoueur($userdata['joueur_id']);
    
    // Si le niveau du joueur vaut 1, on lui rappelle qu'il doit effectuer ses missions
    if ($userdata['niveau'] == NIVEAU_NOURRIR_COPEAUX && getObjectifNiveau($userdata['niveau_objectif'], 0) && $action == "" ){
        $aideNiveau1 = 1;
        $msg .= "<div style=\"border:1px solid blue; padding:10px 10px 10px 10px ;\">".T_("Rappel : <strong>tu es au niveau 1 du jeu.</strong> Au niveau 2, tu pourras acheter des objets par exemple.<br/>Pour y arriver, <strong>regarde la liste des objectifs</strong> à atteindre dans la page")." <a href=\"jeu.php?mode=m_accueil&aff_niveau=1\">".T_("Accueil")."</a>. ".T_("Là, regarde bien la colonne de gauche (Niveau). Bonne chance")." !<br/></div>"; 
    }
}

$temps_mid = microtime_float(); 

$classement = calculerClassement($userdata['note'],"joueurs");

if (!isset($cage_a_afficher)) {
    $cage_a_afficher=0;
    if (isset($_GET['cage_index'])) 
        $cage_a_afficher=intval($_GET['cage_index']);
    else if (isset($_GET['cage_id'])) 
        $cage_a_afficher=cageCorrespondante(intval($_GET['cage_id']));
}

$pagetitle = "Hamster Academy - ".$lstUnivers[$lang][$univers] ;
$description = T_("Bienvenue dans le meilleur jeu gratuit d'élevage virtuel de hamsters : Hamster Academy ! Pour pouvoir y jouer, il suffit de s'inscrire (gratuitement) et on peut alors rejoindre la communauté des éleveurs de Hamsters...");
$keywords = T_("Hamster, Academy, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer");

$liste_styles = "style.css,cornflex-common.css,styleDialog.css";
if ($univers == UNIVERS_ELEVEUR)
    $liste_styles .= ",styleEleveur.css";
else if ($univers == UNIVERS_ACADEMY)
    $liste_styles .= ",styleAcademy.css,style_groupe.css";    
else if ($univers == UNIVERS_FOOT)    
    $liste_styles .= ",styleFoot.css,style_groupe.css";    

$liste_scripts = "tooltip.js,gestionHamster.js,jquery-1.4.2.min.js,jquery-ui-1.8.1.custom.min.js,jquery.cornflex.js,yellowbox.js,jquery-impromptu.js,jquery.autocomplete.min.js";

// barre d'entête
// --------------
$topbar_texte = T_("Bienvenue ").$userdata['pseudo']. " <span class=\"deconnection\"><a href=\"deconnecter.php\">(".T_("se déconnecter").")</a></span>" ;

if ($userdata['vip'])
    $topbar_texte .= "<span style=\"margin-left:30px;\"><a href=\"options.php?mode=m_vip\" title=\"".T_("Voir les informations VIP")."\" style=\"color:black;\">VIP</a></span>";

    $topbar_texte .= "<span style=\"margin-left:30px;\">".$userdata['nb_pieces']." ".T_("pièces")." &nbsp; &nbsp; ".T_("Classement")." : ".$classement.T_("ème")." (".$userdata['note']." pts) </span>";

$topbar_texte_under = "<span style=\"margin-left:30px;\" class=\"topbartxt_under\" >| <a href=\"options.php?mode=m_profil\" title=\"".T_("Voir mon profil")."\">".T_("Mon compte")."</a> | <a href=\"jeu.php?mode=m_achat&amp;univers=$univers\" title=\"".T_("Aller dans la boutique")."\">".T_("La boutique")."</a>| <a href=\"forum/index.php\" title=\"Forum\">Forum</a>|</span>";//<a href=\"options.php?mode=m_vip\" title=\"".T_("Voir les informations VIP")."\">VIP</a>|</span>";

//if ($lang == "en")
//    $topbar_texte_under .= " <a href=\"http://".$serveur_url_fr."jeu.php?lang=fr\" title=\"Jeu en français\" ><img src=\"images/lang/francais.gif\" alt=\"Francais\" width=\"16\" height=\"10\" border=\"0\"></a>";
if ($userdata['droits'] >= DROITS_MOD)
    $topbar_texte_under .= "<span style=\"margin-left:30px;\" class=\"topbartxt_under\" >|<a href=\"tableaudebord.php\">Mod</a> |</span>";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('keywords', $keywords);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('topbar_texte', $topbar_texte);
$smarty->assign('topbar_texte_under', $topbar_texte_under);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

// changement du macaron
if ($lang == "en"){
    
    $imageMacaron = "macaron-en.png";
    if ($univers == UNIVERS_ACADEMY)
        $imageMacaron = "macaron_ac-en.png";
    else if ($univers == UNIVERS_FOOT)
        $imageMacaron = "macaron_foot-en.png";
    echo "<style type=\"text/css\">
        .macaron {
            background: transparent url('../images/$imageMacaron');
        }
        </style>";
}

?>

<div class="logoMain">
    <div class="logoImage">     
        <img src="images/logoHamsterAcademy<?php if ($univers==UNIVERS_ACADEMY) echo "_ac" ; ?>.gif" alt="Hamster Academy" />
    </div>
    <div class="macaron"></div>
</div>

<!-- <div class="imageDansLeCoinGauche"></div> -->

<div class="header">
  <ul>          
<?php               
    if ($univers == UNIVERS_ELEVEUR) {
        echo "<li class=\"menuPrincipal\"".($modeAffichage == "m_accueil" ? " id=\"current\"" : "") ;
        echo "><a href=\"jeu.php?mode=m_accueil&amp;univers=$univers\">".T_("Accueil")."</a></li>";
    }
    if ($userdata['etape_inscription'] > 5) { // on attend la fin de l'inscription pour tout afficher
        echo "<li class=\"menuPrincipal\" ";
        if ($modeAffichage == "m_hamster") echo "id=\"current\"" ;
        echo " >";
        if ($userdata['etape_inscription'] > 5)
            echo "<a href=\"jeu.php?mode=m_hamster&amp;univers=$univers&amp;hamster_id=$hamster_id\">";
        else
              echo "<a href=# >";
        if ($nbHamsters < 2) 
            echo T_("Mon hamster"); 
        else
            echo T_("Mes hamsters");
        echo "</a></li>" ;
    
        if ($univers == UNIVERS_ELEVEUR) {
            echo "<li class=\"menuPrincipal\" ";
            if ($modeAffichage == "m_cage") echo "id=\"current\"" ;
            echo " >";
            if ($userdata['etape_inscription'] > 5)
                echo "<a href=\"jeu.php?mode=m_cage&amp;cage_id=$cage_id\">";
            else
                  echo "<a href=# >";
            if ($nbCages == 1) echo T_("Ma cage") ; else echo T_("Mes cages") ;
            echo "</a></li>" ;
        }
        else {
            echo "<li class=\"menuPrincipal\" ";
            if ($modeAffichage == "m_groupe") 
                echo "id=\"current\"" ;
            $universHamster = $univers;
            if ($lst_hamsters[$hamsterIndex]['inscrit_academy'] == 2) 
                $universHamster = UNIVERS_FOOT;
            else
                $universHamster = UNIVERS_ACADEMY;
            echo " ><a href=\"jeu.php?mode=m_groupe&amp;hamster_id=".$hamster_id."&amp;univers=$universHamster\">";
            if ($nbHamsters == 1)
                echo T_("Le groupe");
            else
                echo T_("Les groupes");
            echo "</a></li>";
        }
        if ($univers == UNIVERS_ELEVEUR) {
            echo "<li class=\"menuPrincipal\" ";
            if ($modeAffichage == "m_objet") echo "id=\"current\"" ;
            echo " >";
            if ($userdata['etape_inscription'] > 5)
                echo "<a href=\"jeu.php?mode=m_objet&amp;univers=$univers\">";
            else
                  echo "<a href=# >";
            echo T_("Inventaire")."</a></li>";
        }
        if ($userdata['niveau'] > 1 ) {
            echo "<li class=\"menuPrincipalDecal\" " ;
            if ($modeAffichage == "m_achat" || $modeAffichage == "m_eleveurs" || $modeAffichage == "m_messages" || $modeAffichage == "m_pass")  echo "id=\"current\"" ;
                echo " ><a href=\"jeu.php?mode=m_achat&amp;univers=$univers\">".T_("En ville")."</a></li>" ;
        }                    
        
        $nouveauMode = "m_hamster";
        if ($userdata['niveau'] >= $niveauMinimalAccesAcademy) {
            if ($modeAffichage == "m_cage")
                $nouveauMode = "m_groupe";
            else if ($modeAffichage == "m_groupe")
                $nouveauMode = "m_cage";
            if ($modeAffichage == "m_eleveurs")
                $nouveauMode = "m_classements";
            else if ($modeAffichage == "m_classements")
                $nouveauMode = "m_eleveurs";
                
            if ($univers == UNIVERS_ELEVEUR) {
                echo "<li class=\"MenuDroiteBleu\"><a href=\"jeu.php?mode=$nouveauMode";
                if ($userdata['niveau'] >= $niveauMinimalAccesAcademy){
                    $nouvelUnivers = UNIVERS_ACADEMY;
                    if ($lst_hamsters[$hamsterIndex]['inscrit_academy'] == 2)
                        $nouvelUnivers = UNIVERS_FOOT;
                    echo "&amp;univers=".$nouvelUnivers."&amp;hamster_id=$hamster_id\" title=\"".T_("Aller à l'Academy")." !\">";
                }
                else
                    echo "&amp;univers=".UNIVERS_ELEVEUR."\" ".tooltip(T_("Accessible à partir du niveau 4")). ">" ;
                echo "<img src=\"images/etoile.gif\" alt=\"Academy\" style=\"height:16px; vertical-align: middle;\" /> Academy</a></li> " ;
            }
            else {
                echo "<li class=\"menuPrincipalDroite\"><a href=\"jeu.php?mode=$nouveauMode&amp;univers=".UNIVERS_ELEVEUR."&amp;hamster_id=$hamster_id\"><img src=\"images/hamster_reduc.gif\" style=\"height:16px; vertical-align:middle;\" alt=\"\" /> ".T_("Eleveur")."</a></li> " ;
            }
        }
    }
    echo "<li class=\"menuPrincipalDroite\" ";
    if ($modeAffichage == "m_aide") echo "id=\"current\"" ;
    $lienAide = "aide";
    if ($lang != "fr")
        $lienAide = "help";
    echo " ><a href=\"$lienAide/doku.php?id=".( ($univers == UNIVERS_ELEVEUR) ? "start" : "www.hamsteracademy.fr_aide_academy")."\">".T_("Aide")."</a></li>";
?>
  </ul>
</div>       

<!-- fin du header -->



<?php 

if ($modeAffichage == "m_hamster" && $nbHamsters > 1) {   // on affiche tous les hamsters :
    echo "
    <div class=\"SubMenu\" ".( ($nbHamsters > 10) ? "
  style=\"height:80px; overflow-y:auto;\" " : "" )." >
         <ul>\n";
         for($h=0;$h<$nbHamsters;$h++) {
          $hamRow = $lst_hamsters[$h];
          echo "<li class=\"menuPrincipal\" ";
          if ($hamsterIndex == $h) 
              echo "id=\"current\"";
          $universHamster = $univers;
          if ($univers != UNIVERS_ELEVEUR){
            if ($hamRow['inscrit_academy'] == 2) 
                $universHamster = UNIVERS_FOOT;
            else
                $universHamster = UNIVERS_ACADEMY;
          }
          echo "><a href=\"jeu.php?mode=m_hamster&amp;univers=$universHamster&amp;hamster_id=".$lst_hamsters[$h]['hamster_id']."\"><img src=\"images/hamster_reduc".($hamRow['inscrit_academy']!= 2 ? "" : "_foot" ).".gif\" alt=\"\" style=\"vertical-align:middle;\" border=\"0\" />".$hamRow['nom']."</a></li>";
        }            
      echo "</ul>
            </div>";
}
else if ($modeAffichage == "m_cage" && $nbCages > 1) {   // on affiche toutes les cages :
    echo "
    <div class=\"SubMenu\"".( ($nbCages > 10) ? "
  style=\"height:80px; overflow-y:auto;\" " : "" ).">
         <ul>\n";
         for($c=0;$c<$nbCages;$c++) {
          $cageRow = $lst_cages[$c];
          echo "<li class=\"menuPrincipal\" ";
          if ($cage_a_afficher == $c) 
              echo "id=\"current\"";
          echo "><a href=\"jeu.php?mode=m_cage&amp;cage_id=".$cageRow['cage_id']."&amp;univers=$univers\"><img src=\"images/cage1.gif\" alt=\"\" style=\"vertical-align:middle;\"  border=\"0\" height=\"20\" /> ".$cageRow['nom']."</a></li>";
        }
      echo "</ul>
    </div>";
}
else if ($modeAffichage == "m_achat" || $modeAffichage == "m_eleveurs" || $modeAffichage == "m_messages" || $modeAffichage == "m_forum" || $modeAffichage == "m_concours" || $modeAffichage == "m_mariages" || $modeAffichage == "m_classements" || $modeAffichage == "m_pass") {   // en ville !
    echo "<div class=\"SubMenu\"><ul>\n";
    if ($userdata['niveau'] >= NIVEAU_GESTION_ETAGE ) {
          echo "<li class=\"menuPrincipal\" " ;
          if ($modeAffichage == "m_achat")  echo "id=\"current\"" ;
          echo " ><a href=\"jeu.php?mode=m_achat&amp;univers=$univers\">".T_("La Boutique")."</a></li> " ;
      }
        if ($userdata['niveau'] >= NIVEAU_METIER_NID ) {
            echo "<li class=\"menuPrincipal\" " ;
            if ($modeAffichage == "m_messages") echo "id=\"current\"" ;
            echo " ><a href=\"jeu.php?mode=m_messages&amp;univers=$univers\">".T_("La Poste")."</a></li> ";
        }    
        if ($userdata['niveau'] > 1 ) {
            echo "<li class=\"menuPrincipal\" " ;
            if ($modeAffichage == "m_forum") 
                echo "id=\"current\"" ;
            $lien_forum = "jeu.php?mode=m_forum";
            if ($userdata['email_active']==1 || $lang == "en")
                $lien_forum = "forum/index.php";
            echo " ><a href=\"$lien_forum\">".T_("Le Forum")."</a></li> ";
        }    
         
        echo "<li class=\"menuPrincipalDecal\" ";
        if ($univers == UNIVERS_ELEVEUR){
            if ($modeAffichage == "m_eleveurs") echo "id=\"current\"" ;
              echo " ><a href=\"jeu.php?mode=m_eleveurs&amp;univers=$univers\">".T_("La Mairie")."</a></li>";
        }
        else  {
            if ($modeAffichage == "m_classements") echo "id=\"current\"" ;
            echo " ><a href=\"jeu.php?mode=m_classements&amp;univers=$univers\">".T_("Classements")."</a></li>";
        }
         echo "<li class=\"menuPrincipal\" ";
              if ($modeAffichage == "m_concours") echo "id=\"current\"" ;
          echo " ><a href=\"jeu.php?mode=m_concours&amp;univers=$univers\">".T_("Concours")."</a></li>";

         if ($userdata['niveau'] >= NIVEAU_MARIAGE && $univers == UNIVERS_ELEVEUR) {
            echo "<li class=\"menuPrincipal\" ";
              if ($modeAffichage == "m_mariages") echo "id=\"current\"" ;
            echo " ><a href=\"jeu.php?mode=m_mariages&amp;univers=$univers\">".T_("Mariages")."</a></li>";
         }
        if ($userdata['niveau'] > 1 ) {
            echo "<li class=\"MenuVert\">";
            if (1 || $lang == "fr")
                echo "<a href=\"jeu.php?mode=m_pass&amp;univers=$univers\">".T_("La Banque")."</a></li> ";
            else
                echo "<a href=\"jeu.php?mode=m_banque&amp;univers=$univers\">".T_("La Banque")."</a></li> ";
        }          
      
     echo "</ul></div>";
}
else if ($modeAffichage == "m_groupe" ) { 
    $selectMenuHamster = true;
    if (isset($_GET['voirtouslesgroupes']))
        $selectMenuHamster = false;
    echo "
    <div class=\"SubMenu\"".( ($nbHamsters > 10) ? "
  style=\"height:80px; overflow-y:auto;\" " : "" ).">
         <ul>\n";
         for($h=0;$h<$nbHamsters;$h++) {
          $hamRow = $lst_hamsters[$h];
          if ($hamRow['inscrit_academy'] > 0) {
              echo "<li class=\"menuPrincipal\" ";
              if ($selectMenuHamster && $hamsterIndex == $h) 
                  echo "id=\"current\"";
              $universHamster = $univers;
              if ($hamRow['inscrit_academy'] == 2) 
                $universHamster = UNIVERS_FOOT;
              else
                $universHamster = UNIVERS_ACADEMY;
              echo "><a href=\"jeu.php?mode=".$modeAffichage."&amp;hamster_id=".$hamRow['hamster_id']."&amp;univers=$universHamster\">".$hamRow['nom']."</a></li>";
          }
        }            
        echo "<li class=\"menuPrincipalDecal\" ";
        if (! $selectMenuHamster) 
          echo "id=\"current\"";
        echo "><a href=\"jeu.php?mode=m_groupe&amp;voirtouslesgroupes=1&amp;tri=activite&amp;desc=1\">".T_("Voir tous les autres groupes")."</a></li>";
      echo "</ul></div>";
}
?>

<div class="mainPageUnderMenu">
<br/>
      
<?php 
switch($modeAffichage){
    case "" :
    case "m_hamster":
        require "jeu_hamster.php";
        break;
    case "m_cage":
        require "jeu_cage.php";
        break;
    case "m_objet":
        require "jeu_objets.php";
        break;
    case "m_achat":
        require "jeu_achat.php";
        break;
    case "m_metier":
        require "jeu_metier.php";
        break;
    case "m_accueil":
        require "jeu_accueil.php";
        break;
    case "m_aide" :
    case "m_aide_a":
        require "jeu_aide.php";
        break;
    case "m_entrainement":
        require "jeu_entrainement.php";
        break;
    case "m_classements":
        require "jeu_classements.php";
        break;
    case "m_groupe":
        require "jeu_groupe.php";
        break;
    case "m_eleveurs":
        require "jeu_eleveurs.php";
        break;
    case "m_messages":
        require "jeu_messages.php";
        break;
    case "m_banque":
        $pagePrecUrl = "jeu.php?mode=m_banque";
        require "formulaireBanque.php";
        break;
    case "m_pass":
        require "jeu_pass.php";
        break;
    case "m_forum":
        $msg .= T_("Pour participer au forum, chaque joueur doit d'abord valider son adresse email").". <br/>&nbsp;<br/>".T_("Pour cela, tu dois aller dans")." <a href=\"options.php\">".T_("Mon compte")."</a> ".T_("et cliquer sur le bouton <strong>Activer maintenant !</strong> Et reviens-vite dans le forum !");
        ?>
        <div style="width:940px;">

          <div class="hamBlocColonne-top">
             <div class="hamBlocColonne-top-left"></div>
             <div class="hamBlocColonne-top-right"></div>
             <div class="hamBlocColonne-top-center"></div>        
          </div>
            
          <div class="hamBlocColonne_full">

          <?php echo "<div>".$msg."<br/>&nbsp;<br/><br/>&nbsp;<br/><br/>&nbsp;<br/><br/>&nbsp;<br/><br/>&nbsp;<br/><br/>&nbsp;<br/></div>" ; ?>
          
          <div style="clear:both;">&nbsp;</div>
        
    
          </div>
            
          <div class="hamBlocColonne-bottom">
             <div class="hamBlocColonne-bottom-left"></div>
             <div class="hamBlocColonne-bottom-right"></div>

             <div class="hamBlocColonne-bottom-center"></div>        
          </div> 
          </div>
          <?php
        break;
    case "m_concours":
        require "concours.php";
        break;
    case "m_mariages":
        require "jeu_mariages.php";
        break;
    case "m_annuaire":
        require "jeu_annuaire.php";
        break;
}    
?>

</div> <!-- fin du mainPageUnderMenu -->

<?php 

require "footer.php" ; 

?>
 
