<?php 
define('IN_HT', true);

require "common.php";

//$userdata = session_pagestart($user_ip);

$user_lang = "fr";

require "lang/".$user_lang."/common_lang.php";

//if( ! $userdata['session_logged_in'] ) 

$pagetitle = "Ecrire à Hamster Academy" ;
$univers = UNIVERS_INDEX;

require "header.php";

?>

<div style="width:850px; margin-left: auto; margin-right: auto;">
    <div align="center">
        <h1>Ecrire à Hamster Academy</h1>
        <br>
        
        <img src="images/enveloppe.gif" alt="Ecrire" />
        
        <br>&nbsp;<br/>
        
        <?php 
            if (! isset($_GET['lettre_submit'])) {
        ?>
        
            <br/>&nbsp;<br/>
            
            <form action="contact.php" method="get">
                
                <table>
                    <tr>
                        <td>Nom est <br/>(pour qu'on puisse te tenir informé) : </td><td><?php
                                if ($qui_alerte == "")
                                    echo "<input type=textbox name=qui_alerte size=50 value=\"".$qui_alerte."\"></td>";
                                else
                                    echo $qui_alerte ;
                            ?></td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td><td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Quel est le joueur qui insulte<br>ou qui dit des grossiertés : </td><td><input type=textbox name=alerte_qui size=50 value="<?php echo $alerte_qui; ?>"></td>
                    </tr>
                    <tr>
                        <td>Où : </td><td><input type=textbox name=alerte_lieu size=50 value="<?php echo $alerte_lieu; ?>"></td>
                    </tr>                
                    <tr>
                        <td>Précisions<br>(ce qui a été dit par exemple...)</td><td><textarea name=alerte_quoi cols=50 rows=4><?php echo $alerte_quoi; ?></textarea></td>
                    </tr>            
                </table>
                
                <input type=submit value="Envoyer à Hamster Academy" name=alerte_submit>
            
            </form>
            
            <?php 
        }
        else { ?>
            <br/>&nbsp;<br/>
            Merci de nous avoir écrit ! On te tient au courant.
            <br/>&nbsp;<br/>
            <a href="#" onClick="javascript:window.close();">Fermer cette fenêtre</a>
            <br/>&nbsp;<br/>
        <?php } ?>

<br/>&nbsp;<br/>


<?php 

require "footer.php" ; 

?>

