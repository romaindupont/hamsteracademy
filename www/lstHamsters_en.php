<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

// colonnes : 0) race <-> 1)image <-> 2) Couleurs yeux <-> 3) Couleurs pelage , 4) description
$infosHamsters = array(
0 => array('Hamster angora','hamster_angora.jpg','black','white and beige','Smart hamster, he likes hiding! The more you will put hiding places in his cage, the more he will be happy!'), 
1 => array('Short-haired Hamster','hamster_poilras.jpg','black','brown and white','Enduring and alert, here is a big sportsman! Wheel and barbells are compulsory in the cage!'),
2 => array('Russian Hamster','hamster_russe.jpg','black','white','Thanks to his small size, he creeps under everything. He likes hiding places and tunnels.'),
3 => array('Affectionate Hamster','hamster14.jpg','black','light grey','This hamster likes braids and cosy places! And of course, he loves cuddles!'),
4 => array('Tough Hamster','hamster16.jpg','black','white and cream','That, for a tough hamster, it is a tough hamster! He hase a real personnality! We do not risk to be bored with him! Give him accessories to make-sport and play and he will be delighted!'),
5 => array('Naughty Hamster','hamster17.jpg','black','cream, white belly','Always sweet, always quiet, even when he plays with the wheel ! And that, all the breeders dream about it! He is really so sweet!'),
6 => array('Sweet Hamster','hamster_mignon.jpg','black','Marron cream','Hamster Academy team loves this hamster! He is so cute! Soft, cuddly, cute, nice, neat... A must-have!'),
7 => array('Grunge Hamster','HamsterGrunge.jpg','black glasses','suntanned','Plus rebelle, tu meurs ! Il a le style, il a la pêche, il fait fureur ! Mais attention, il n\'est pas du genre facile à vivre, il n\'aime pas être contrarié. Mais il sait être super attachant et adore faire plaisir à son éleveur&nbsp;!'),
8 => array('Food-loving Hamster','hamster_gourmand.jpg','black','cream','It is a well-known fact that hamsters like their food, more so than other animals! Hamsters are very cute, very clever (especially for finding seeds out in the forest). They love treats, so don’t hesitate to give them some!'),
9 => array('Loved Hamster','hamster_choupette.jpg','black','white-grey','Very cuddly, very pretty, adorable, he\'s great, it\'s most favorite of scrunchies! Give him the best cage ever!'),
10 => array('Toofoo Hamster','hamster_tarzan.jpg','black','cream','He\'s moving like Tarzan! He is crazy, his mind is always in the trees! Put the cage \'s upside down, he will love it! And don\'t forget to put a swing in the cage and his daily walk in the forest!'),
11 => array('Chantilly Hamster','hamster_chantilly.jpg','black','cream','He\'s soft like chantilly! He is also a very curious hamster, docile and very cuddly! A must-have!'),
12 => array('Timid Hamster','chomik18.jpg','black','Soft Cream','He is a very timid hamster, he is afraid of the slightest noise and new people. Protect him well, put his cage in a quiet place and he will be happy! Isn\'t he so cute?')
);

?>