11 mars 2011
------------

- Fusion des bases de données
- Le tchat gère la suspension des joueurs

5 mars 2011
-----------

Nouveautés :
- possibilité de modifier le texte d'accueil via le bouton mod
- image de profil non limitée en taille, redimensionnement automatique en 100*100
- campagne adwords réactivée

25 décembre 2010
----------------

Nouveautés :
- simplification des menus de la partie Academy
- applications FB approuvées
- nouvelle maladie et mission 4 : fièvre
- nouvels objets : thermometre et menthe de glace
- tchat ouvert

Bugs corrigés :
- changer d'academy ne remet pas à zéro (sauf 1ère inscription)

24 décembre 2010
----------------

Nouveauté :
- publication des cages sur Facebook

23 décembre 2010
----------------

Nouveautés :
- connexion via facebook
- amélioration du dessin offline des cages

20 décembre 2010
----------------

Nouveautés :
- cage neige 
- piscine qui change de couleur selon la propreté de la cage et du status enneigé ou pas

15 décembre 2010
----------------

Nouveauté :
- rajout des sapins de noel + cadeaux

7 décembre 2010
---------------

Actualité:
- résultat du concours de la meilleure progression

6 décembre 2010
----------------

Nouveautés : 
- objet éolienne d'énergie
- énergie pour la danse, etc : passage de 3 à 1 par activité

2 décembre 2010
----------------

Nouveautés : 
- objet cristal de neige et modif graphique.php pour l'afficher correctement
- calendrier de l'avent actif

28 novembre 2010
----------------

Nouveautés : 
- nouvel hamster (craintif)

Bugs corrigés :
- correction traduction des hamsters en anglais
- correction du niveau accessible pour les hamsters (et affichage de ce niveau)
- cours de danse, chant, foot, etc. pondéré par maxForce, maxSante, etc.
- + garde-fous en cas de concours où il y a no-limit sur la force. A répercuter aussi sur les actions de foot.

27 novembre 2010
----------------

Nouveautés : 
- actions dans l'academy sujets à l'énergie

14 novembre 2010
----------------

Nouveautés : 
- système de Pass
- affichage de tous les objets du jeu dans une page à part + rajout dans les sitemap
- ajout du lien Forum à la place de VIP dans la topBar, plus utile
- nouvel objet : Amulette de Sport

Bugs corrigés :
- grossesses => modif pour éviter les grossesses multiples. A suivre.
- affichage des 50 premiers à la mairie

11 novembre 2010
----------------

Nouveautés
- mise en cache de la page de la mairie + page des anniversaires au login
- benchmarking sql et php
- passage au gettext natif de php/apache (extension gettext)

2 novembre 2010
--------------

Nouveautés
- dessin de cage offline
- traduction en anglais du concours

8 octobre 2010
--------------

Nouveautés
- traduction missions
- passage de la durée de la grossesse de 1 semaine à 4 jours
- nouveaux objets : 2 porte-buches

27 septembre 2010
------------

Nouveautés
- système de maladie pour le hamster
- niveau 7 et 16 : guéris ton hamster

24 septembre 2010
------------

Modification
- niveau 8 scindé en 2 (niveau 8 et 9)

16 septembre 2010
------------

Bug corrigé :
- affichage des erreurs pour la reproduction depuis la page du mariage

2 juillet 2010
------------

Bug corrigé :
- bannissement dans le forum : pb dans le chemin du cache du fichier de bannissement

27 juin 2010
------------

Nouveauté :
- 3 semaines sans leader implique un nouveau leader

Bug corrigé : 
- test pour la possibilité de passer l'examen lorsque le hamster est "genie" => suppression du +1
- groupe sans leader depuis longtemps ne peut pas virer un membre.

16 mai 2010
-------------

Nouveautés :
- choix du pseudo plus facile à la poste et défi (ajaxisé)
- mise en place du système de défi

15 mai 2010
-------------

Nouveautés:
- nouvels objets chinois pour la cage
- déplacement d'objet dans la cage revu
- mission n°13 : décoration chinoise

06 mai 2010
-------------

Nouveautés:
- upload d'image dans le forum
- nouvel objet : le muguet

Bug corrigé :
- bug du muguet corrigé (remise à 10 du moral)


03 mai 2010
-------------

Nouveautés:
- poursuite de la traduction en anglais 
- label ICRA : http://www.hamsteracademy.com/labels.rdf


22 avril 2010
-------------

Nouveauté:
- affichage de l'équipe de foot revu et bouton "virer joueur" bien visible

21 avril 2010
-------------

Nouveauté:
- page d'accueil : affichage des meilleurs groupes et équipes
- page d'accueil : lien vers Facebook
- Facebook : changement du logo

20 avril 2010
-------------

Nouveauté:
- nouveau sport : le catch américain
- oeuf de paques : remise en route pour la fin du mois d'avril

10 avril 2010
-------------

Nouveauté:
- possibilité de vendre au vétérinaire 20 noix de cocos stockées dans une caisse en bois. Rapporte 30 pièces

Bugs corrigés :
- bug de création de groupe de musique
- bug du passage de niveau pour les groupes
- bug de l'affichage du niveau à partir duquel un manager peut être donné à un groupe (3 => 4)

9 avril 2010
------------

Nouveauté:
- affichage du manager dans la liste des groupes

4 avril 2010
------------

Nouveautés :
- Manager disponible pour les groupes de musique : +20% de points
- nouvelle présentation des groupes

Bugs corrigés :
- bug d'affichage des hamsters dans les groupes
- propreté des cages qui évoluent quotidiennement

21 mars 2010
------------

Nouveautés:

- possibilité de bannir une ip pour empêcher toute nouvelle inscription (pour les modérateurs)
- nouvelle règle : si un leader est absent depuis 2 semaines, tous les membres deviennent "leader". Le leader d'origine reste leader et retrouve tous ses pouvoirs à son retour.
- niveaux pour les groupes de musique (raz de l'expérience)
- Academy de Foot + nouvelles spécialités liées au foot
- catch


Bugs corrigés :
- refuge et mise à jour quotidienne
- erreur affichage lorsque les hamsters sont au refuge
- affichage du nbre d'inscrits à un concert ou répétition


A tester:
- nouvelle règle : si un leader est absent depuis 2 semaines, tous les membres deviennent "leader". Le leader d'origine reste leader et retrouve tous ses pouvoirs à son retour.
- refuge ok ?
