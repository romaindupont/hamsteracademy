<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

// on récupère l'adresse d'où on vient
$pagePrecUrl = "jeu.php?mode=m_achat"; // par défaut
$actionBanque = 0;

if ($systeme_bancaire == ALLOPASS) {

    if (isset($_GET['DATAS'])) 
        $pagePrec = mysql_real_escape_string($_GET['DATAS']);

    $pageTxt = substr($pagePrec,0,3) ;
    $pageParam = intval(substr($pagePrec,4,strlen($pagePrec))) ;
}
else if ($systeme_bancaire == PAYPAL) {
    
}
else
    return;

if ($pagePrec != "") {
    if ($pageTxt == "obj") 
        $pagePrecUrl = "jeu.php?mode=m_achat&amp;objet=".$pageParam; // si le joueur voulait acheter un objet, le type de cet objet est la valeur absolue de pagePrec
    else if ($pageTxt == "cag")
        $pagePrecUrl = "jeu.php?mode=m_cage&amp;cage_id=".$pageParam;
    else if ($pageTxt == "ach"){
        if ($pageParam == 0)
            $pageParam = 1; // pour éviter le rayon vide
        $pagePrecUrl = "jeu.php?mode=m_achat&amp;rayon=".$pageParam;
    }
    else if ($pageTxt == "pie")
        $pagePrecUrl = "jeu.php?mode=m_achat&amp;rayon=9";        
    else if ($pageTxt == "ham")
        $pagePrecUrl = "jeu.php?mode=m_hamster&amp;hamster_id=".$pageParam;
    else if ($pageTxt == "ent")
        $pagePrecUrl = "jeu.php?mode=m_entrainement&hamster_id=".$pageParam;
    else if ($pageTxt == "nid") // mode = construction accélérée du nid        
        $pagePrecUrl = "jeu.php?mode=m_hamster&hamster_id=".$pageParam;
    else if ($pageTxt == "cab") // mode = construction accélérée de la cabane        
        $pagePrecUrl = "jeu.php?mode=m_hamster&hamster_id=".$pageParam;
    else if ($pageTxt == "gro") // mode = construction accélérée de la cabane        
        $pagePrecUrl = "jeu.php?mode=m_hamster&hamster_id=".$pageParam;        
    else if ($pageTxt == "niv") // mode = changement de niveau
        $pagePrecUrl = "jeu.php?mode=m_accueil";        
    else if ($pageTxt == "vip") // mode = passage en vip
        $pagePrecUrl = "options.php?mode=m_vip";          
}

$message_erreur = ""; // à personnaliser selon allopass ou paypal

$erreur = -1;
$paiement = PAIEMENT_NUL;

if ($systeme_bancaire == ALLOPASS)
    require "gestionPaiementAllopass.php";
else if ($systeme_bancaire == PAYPAL)
    require "gestionPaiementPaypal.php";
else
    return;
    
if ($paiement != PAIEMENT_NUL)
    joueurVientdePayer($paiement);
?>
