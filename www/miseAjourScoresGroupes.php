<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";
include "lstAccessoires.php";

$depuis24H = $dateActuelle-3600*24;

// on sélectionne tous les groupes qui n'ont pas eu de mise à jour de scores depuis 1 jour
$query = "SELECT groupe_id FROM groupes";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbGroupes = $dbHT->sql_numrows($result) ;

while($row=$dbHT->sql_fetchrow($result)) {
    miseAJourPointsGroupe($row['groupe_id']);
}

$dbHT->sql_freeresult($result);

echo "nb de groupes avec le score mis à jour : " . $nbGroupes. "<br/>";

?>
