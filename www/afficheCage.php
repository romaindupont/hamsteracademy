<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";
include "graphique.php";
include "lstAccessoires.php";

// pour affichage public de la cage
$cage_id = -1 ;
if (isset($_GET['cage_id'])) {
	$cage_id = intval($_GET['cage_id']);
}

// on recupere la cage
$query = "SELECT * FROM cage WHERE cage_id='".$cage_id."' LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;
if ($nbCages != 1) {
	echo T_("Il n'y a aucune cage avec cet id. Verifier l'id de la cage et recommencer.") ;
	return ;
}
$cage=$dbHT->sql_fetchrow($result);
$cage['nbAccessoires'] = 0;
$dbHT->sql_freeresult($result);

// liste des accessoires pour les cages
// ------------------------------------
$query = "SELECT a.*, c.* FROM accessoires a, config_accessoires c WHERE a.cage_id=".$cage_id." AND a.type = c.type";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
}
$nbAccessoires = $dbHT->sql_numrows($result) ;
$lst_accessoires=array();
$cage['nbAccessoires'] = 0;
while($row=$dbHT->sql_fetchrow($result)) {
	array_push($lst_accessoires,$row);
	$cage['nbAccessoires'] ++ ;
}
$dbHT->sql_freeresult($result);

// on recupere qq infos sur le joueur
$query = "SELECT * FROM joueurs WHERE joueur_id=".$cage['joueur_id']." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining joueur_data', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;
if ($nbJoueurs != 1) {
	echo T_("Il n'y a aucun joueur associe a la cage") ;
}
$userdata=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

$pagetitle = "Hamster Academy - Cage : ".$cage['nom'] ;
$description = "Hamster Academy - ".T_("voir la cage")." : ".$cage['nom'] . ". ".T_("Inscris-toi sur Hamster Academy et personnalise ta propre cage !");

$liste_styles = "style.css,styleEleveur.css";
$liste_scripts = "pngfix.js";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

?>

<div align="center">
	
	<h2>Hamster Academy - Cage</h2>
	
	<h3><?php echo $cage['nom'] ; ?></h3>
	
	<?php 
		echo afficherCage($cage, false, $lst_accessoires, $nbAccessoires);
	?>

<br/>&nbsp;<br/>


<a href="#" onclick="javascript:window.close();"><?php echo T_("Fermer"); ?></a>

</div>


<br/>&nbsp;<br/>

<?php require "footer.php" ; ?>
