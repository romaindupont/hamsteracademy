<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$email = "contact@hamsteracademy.fr"; 

$message_erreur = "";

//mail($email, "Paiement paypal recu !", "Cf le mail de paypal" . "\n\n"); 

if (isset($_GET['err'])) {
    
    $erreur = intval($_GET['err']) ;
}
else {

    error_reporting(E_ALL ^ E_NOTICE); 
    
    $header = ""; 
    $emailtext = ""; 

    // Read the post from PayPal and add 'cmd' 
    $req = 'cmd=_notify-validate'; 
    if(function_exists('get_magic_quotes_gpc')) 
    {  
        $get_magic_quotes_exits = true; 
    } 
    foreach ($_POST as $key => $value) 
    // Handle escape characters, which depends on setting of magic quotes 
    {  
        if($get_magic_quotes_exists == true && get_magic_quotes_gpc() == 1){  
            $value = urlencode(stripslashes($value)); 
        } else { 
            $value = urlencode($value); 
        } 
        $req .= "&$key=$value"; 
    } 
    // Post back to PayPal to validate 
    $header .= "POST /cgi-bin/webscr HTTP/1.0\r\n"; 
    $header .= "Content-Type: application/x-www-form-urlencoded\r\n"; 
    $header .= "Content-Length: " . strlen($req) . "\r\n\r\n"; 
    //$fp = fsockopen ('ssl://www.sandbox.paypal.com', 443, $errno, $errstr, 30); 
    $fp = fsockopen ('ssl://www.paypal.com', 443, $errno, $errstr, 30); 
     
     
    // Process validation from PayPal 
    // TODO: This sample does not test the HTTP response code. All 
    // HTTP response codes must be handles or you should use an HTTP 
    // library, such as cUrl 
     
    if (!$fp) { // HTTP ERROR 
        mail($email, "Http error !", "" . "\n\n"); 
    } 
    else { 
        // NO HTTP ERROR 
        fputs ($fp, $header . $req); 
        while (!feof($fp)) { 
            $res = fgets ($fp, 1024); 
            if (strcmp ($res, "VERIFIED") == 0) { 
                // TODO: 
                // Check the payment_status is Completed 
                if ($_POST['payment_status'] != "Completed") {
                    mail($email, "Paiement paypal => payment_status invalide", "payment_status =  "  . $_POST['payment_status'] . "\n\n"); 
                }
                // Check that txn_id has not been previously processed 
                // Check that receiver_email is your Primary PayPal email
                if ($_POST['receiver_email'] != "contact@hamsteracademy.fr") {
                    mail($email, "Paiement paypal => receiver_email invalide", "receiver_email =  "  . $_POST['receiver_email'] . "\n\n"); 
                }
                // Check that payment_amount/payment_currency are correct
                if ($_POST['mc_gross'] != "3.00" && $_POST['mc_gross'] != "5.00" && $_POST['mc_gross'] != "10.00" && $_POST['mc_gross'] != "20.00") {
                    mail($email, "Paiement paypal => mc_gross invalide", "mc_gross =  "  . $_POST['mc_gross'] . "\n\n"); 
                }
                
                // Process payment 
                // If 'VERIFIED', send an email of IPN variables and values to the 
                // specified email address 
                foreach ($_POST as $key => $value){ 
                    $emailtext .= $key . " = " .$value ."\n\n"; 
                } 
                mail($email, "Live-VERIFIED IPN", $emailtext . "\n\n" . $req); 
                
                // on crédite 300 euros
                if (is_numeric($_POST['custom'])){
                    $joueur_id = intval($_POST['custom']);
                    $nbPass = 1;
                    if ($_POST['mc_gross'] == "3.00")
                        $nbPass = 1;
                    else if ($_POST['mc_gross'] == "5.00")
                        $nbPass = 2;
                    else if ($_POST['mc_gross'] == "10.00")
                        $nbPass = 5;
                    else if ($_POST['mc_gross'] == "20.00")
                        $nbPass = 15;
                    else
                        mail($email, "Paiement paypal => mc_gross non reconnu : ".$_POST['mc_gross']."\n\n"); 
                        
                    if ($joueur_id > 0) {
                        ajouterPass($joueur_id,$nbPass);
                        mail($email, "Paiement paypal => ".$nbPass." pass", "Au joueur "  . $joueur_id . "\n\n"); 
                    }
                    else
                        mail($email, "Paiement paypal => joueur_id non valide : ".$joueur_id."\n\n"); 
                }
                else
                    mail($email, "Paiement paypal => custom non valide : ".$_POST['custom']."\n\n"); 
                
            } else if (strcmp ($res, "INVALID") == 0) { 
                // If 'INVALID', send an email. TODO: Log for manual investigation. 
                foreach ($_POST as $key => $value){ 
                $emailtext .= $key . " = " .$value ."\n\n"; 
                } 
                mail($email, "Live-INVALID IPN", $emailtext . "\n\n" . $req); 
            }     
        } 
        fclose ($fp); 
    }
}
?> 