<?php
  if ($action == "demandeMariage") {
    
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']);
        
        // on vérifie que cet hamster n'a pas déjà une demande en cours
        $query0 = "SELECT demandeur_id FROM interactions WHERE demandeur_id=".$hamster_id." AND type=".DEMANDE_MARIAGE. " LIMIT 1";
        if ( !($result0 = $dbHT->sql_query($query0)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
        }
        $nbDemandes = $dbHT->sql_numrows($result0) ;
        if ($nbDemandes == 0) {
        
            if (isset($_GET['nomHamster'])) {
                $nomHamster = mysql_real_escape_string($_GET['nomHamster']) ;
            
                if (isset($_GET['nomJoueur'])) {
                    $nomJoueur = mysql_real_escape_string($_GET['nomJoueur']) ;
                    
                    if ($nomJoueur != "") {
                    
                        // le joueur a fourni toutes les infos, on cherche le hamster correspondant
                        $query2 = "SELECT joueur_id FROM joueurs WHERE pseudo='".$nomJoueur."' LIMIT 1";
                        if ( !($result2 = $dbHT->sql_query($query2)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query2);
                        }
                        $nbJoueurs = $dbHT->sql_numrows($result2) ;
                        
                        // on vérifie que le joueur cible choisi existe bien
                        if ($nbJoueurs > 0){
                            $rowJoueur = $dbHT->sql_fetchrow($result2);
                                
                            $query = "SELECT hamster_id, nom, compagnon_id FROM hamster WHERE joueur_id = ".$rowJoueur['joueur_id']. " AND nom='".$nomHamster."'";
                            if ( !($result = $dbHT->sql_query($query)) ){
                                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
                            }
                            $nbHamstersSollicites = $dbHT->sql_numrows($result) ;
                            // on vérifie que le hamster cible choisi existe bien
                            if ($nbHamstersSollicites > 0) {
                                $rowHamsterSollicite = $dbHT->sql_fetchrow($result);
                                // oui ! on vérifie qu'il n'est pas lui-même et non marié
                                if ($rowHamsterSollicite['hamster_id'] == $hamster_id)
                                    $erreurAction = 4;
                                else if ($rowHamsterSollicite['compagnon_id'] > 0) 
                                    $erreurAction = 6;
                                else { // ouf !
                                    
                                    // on vérifie qu'il a assez d'argent
                                    $resultat = debiterPieces($coutDemandeMariage,"demande en mariage");
                                    
                                    if ($resultat == 1) {
                                    
                                        ajouterInteraction($hamster_id, $rowHamsterSollicite['hamster_id'], DEMANDE_MARIAGE,"Demande en mariage",0) ;
                                        
                                        // on prévient le sollicité
                                        $message_texte = str_replace("#1",$rowHamsterSollicite['nom'],T_("(Message automatique) Ton hamster #1 a été demandé en mariage par #2. Pour accepter la demande, va en Ville puis en Mairie : tu y trouveras un bouton pour accepter !"));
                                        $message_texte = str_replace("#2",$lst_hamsters[hamsterCorrespondant($hamster_id)]['nom'],$message_texte);
                                        envoyerMessagePrive($rowJoueur['joueur_id'], $message_texte, $userdata['joueur_id'], true);
                                        
                                        // mise à jour du niveau
                                        if ($userdata['niveau'] == NIVEAU_MARIAGE)// && $rowJoueur['joueur_id'] != $userdata['joueur_id']) 
                                            updateNiveauJoueur(0,true);
                                    }
                                    else {
                                        $erreurAction = 8;
                                    }
                                }
                            }
                            else {
                                $erreurAction = 2; // pas de hamster à ce nom
                            }
                        }
                        else{
                            $erreurAction = 1; // pas de joueur à ce nom
                        }
                    }
                    else {
                        $erreurAction = 7; // pas de nom saisi
                    }                    
                }
            }
        }
        else {
            $erreurAction = 3 ; // demande déjà en cours
        }
    }
}
else if ($action == "accepterMariage") {
    if (isset($_GET['hamster_sollicite_id'])) {
        $hamster_sollicite_id = intval($_GET['hamster_sollicite_id']);
    
        if (isset($_GET['hamster_solliciteur_id'])) {
            $hamster_solliciteur_id = intval($_GET['hamster_solliciteur_id']);
            
            // on vérifie qu'il y a bien une demande de mariage dans ce sens
            $query0 = "SELECT demandeur_id FROM interactions WHERE demandeur_id=".$hamster_solliciteur_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_sollicite_id." LIMIT 1";
            if ( !($result0 = $dbHT->sql_query($query0)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            }
            $nbDemandes = $dbHT->sql_numrows($result0) ;
            if ($nbDemandes == 0) 
                $erreurAction = 1; // pas de demande => triche ?
            else {
                // on marie les joueurs
                $query = "UPDATE hamster SET compagnon_id = ".$hamster_sollicite_id." WHERE hamster_id = ".$hamster_solliciteur_id."  LIMIT 1";
                if ( !($dbHT->sql_query($query)) ){
                    $erreurAction = 2; // peut être que le joueur solliciteur a disparu
                }
                else {
                    
                    $resultat = debiterPieces($coutDemandeMariage,"demande en mariage"); 
                    if ($resultat == 1) {
                        $lst_hamsters[hamsterCorrespondant($hamster_sollicite_id)]['compagnon_id'] = $hamster_solliciteur_id;
                        
                        // au cas où le joueur marie ses hamsters entre eux
                        $hamsterSolliciteurIndex = hamsterCorrespondant($hamster_solliciteur_id); 
                        if ($hamsterSolliciteurIndex != -1)
                            $lst_hamsters[$hamsterSolliciteurIndex]['compagnon_id'] = $hamster_sollicite_id;
    
                        $query2 = "UPDATE hamster SET compagnon_id = ".$hamster_solliciteur_id." WHERE hamster_id = ".$hamster_sollicite_id."  LIMIT 1";
                        $dbHT->sql_query($query2);
                        
                        // les deux joueurs sont maintenant mariés, on supprime la demande en mariage
                        $query3 = "DELETE FROM interactions WHERE demandeur_id=".$hamster_solliciteur_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_sollicite_id;
                        $dbHT->sql_query($query3);
                        // on efface éventuellement la demande en mariage dans l'autre sens au cas où ...
                        $query4 = "DELETE FROM interactions WHERE demandeur_id=".$hamster_sollicite_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_solliciteur_id;
                        $dbHT->sql_query($query4);
                        
                        // prévenir le compagnon par mail
                        $query4 = "SELECT joueur_id, nom, sexe FROM hamster WHERE hamster_id=".$hamster_solliciteur_id." LIMIT 1";
                        $result4 = $dbHT->sql_query($query4);
                        $rowHamsterSolliciteur = $dbHT->sql_fetchrow($result4);
                        
                        if ($lst_hamsters[hamsterCorrespondant($hamster_sollicite_id)]['sexe'] == 0)
                            $message_texte = T_("(Message automatique) Le nouveau mari");
                        else
                            $message_texte = T_("(Message automatique) La nouvelle femme");
                        $message_texte .= str_replace("#1",$rowHamsterSolliciteur['nom'],T_(" de ton hamster #1 a accepté la demande en mariage de ton hamster&nbsp;! Va vite en Ville puis à la Mairie pour voir les deux nouveaux mariés ! Félicitations aux deux mariés ! Ils ont chacun 3 cadeaux : des bonbons, de la salade et des vitamines ! Idéal pour partir sur un bon pied !"));
                        
                        // il faut récupérer l'id du joueur solliciteur
                        envoyerMessagePrive($rowHamsterSolliciteur['joueur_id'], $message_texte,$userdata['joueur_id'], true);
                        
                        // cadeau aux mariés : bonbon, salade, vitamines
                        ajouterAccessoireDansBDD(ACC_BONBON, $rowHamsterSolliciteur['joueur_id'], -1, -1, -1, -1, 1);
                        ajouterAccessoireDansBDD(ACC_SALADE, $rowHamsterSolliciteur['joueur_id'], -1, -1, -1, -1, 3);
                        ajouterAccessoireDansBDD(ACC_VITAMINES, $rowHamsterSolliciteur['joueur_id'], -1, -1, -1, -1, 1);
                        
                        ajouterAccessoireDansBDD(ACC_BONBON, $userdata['joueur_id'], -1, -1, -1, -1, 1);
                        ajouterAccessoireDansBDD(ACC_SALADE, $userdata['joueur_id'], -1, -1, -1, -1, 3);
                        ajouterAccessoireDansBDD(ACC_VITAMINES, $userdata['joueur_id'], -1, -1, -1, -1, 1);
                        
                        $query5 = "INSERT INTO historique VALUES(".HISTORIQUE_MARIAGE.",".$dateActuelle.",'".mysql_real_escape_string($rowHamsterSolliciteur['nom'])." s\'est marié".($rowHamsterSolliciteur['sexe']==0?"":"e")." avec ".mysql_real_escape_string($lst_hamsters[hamsterCorrespondant($hamster_sollicite_id)]['nom'])."')";;
                        if ( !($dbHT->sql_query($query5)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query5);
                        }
                        
                        // mise à jour du niveau
                        if ($userdata['niveau'] == NIVEAU_MARIAGE) 
                            updateNiveauJoueur(1,true);
                    }
                    else {
                        $erreurAction = 3;
                    }
                }
            }
        }
    }
}
else if ($action == "refuserMariage") {
    if (isset($_GET['hamster_sollicite_id'])) {
        $hamster_sollicite_id = intval($_GET['hamster_sollicite_id']);
    
        if (isset($_GET['hamster_solliciteur_id'])) {
            $hamster_solliciteur_id = intval($_GET['hamster_solliciteur_id']);
            
            // on vérifie qu'il y a bien une demande de mariage dans ce sens
            $query0 = "SELECT demandeur_id FROM interactions WHERE demandeur_id=".$hamster_solliciteur_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_sollicite_id." LIMIT 1";
            if ( !($result0 = $dbHT->sql_query($query0)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            }
            $nbDemandes = $dbHT->sql_numrows($result0) ;
            if ($nbDemandes == 0) 
                $erreurAction = 1; // pas de demande => triche ?
            else {
                // on supprime la demande en mariage
                $query3 = "DELETE FROM interactions WHERE demandeur_id=".$hamster_solliciteur_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_sollicite_id;
                $dbHT->sql_query($query3);
                // on efface éventuellement la demande en mariage dans l'autre sens au cas où ...
                $query4 = "DELETE FROM interactions WHERE demandeur_id=".$hamster_sollicite_id." AND type=".DEMANDE_MARIAGE. " AND receveur_id=".$hamster_solliciteur_id;
                $dbHT->sql_query($query4);
                
                // prévenir le compagnon par mail
                $query4 = "SELECT joueur_id, nom FROM hamster WHERE hamster_id=".$hamster_solliciteur_id." LIMIT 1";
                $result4 = $dbHT->sql_query($query4);
                $rowHamsterSolliciteur = $dbHT->sql_fetchrow($result4);
                
                $message_texte = str_replace("#1",$coutDemandeMariage,T_("(Message automatique) Désolé, ta demande en mariage a été refusée. Si tu veux savoir pourquoi, demande au joueur qui a refusé. Les #1 pièces que tu avais dépensé t'ont été remboursées."));
                
                // il faut récupérer l'id du joueur solliciteur
                envoyerMessagePrive($rowHamsterSolliciteur['joueur_id'], $message_texte,$userdata['joueur_id'], true);
                        
                // et lui redonner ses pièces
                crediterPiecesBDD($rowHamsterSolliciteur['joueur_id'], $coutDemandeMariage);
            }
        }
    }
}
else if ($action == "rompreMariage") {
    
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']);
        
        // on vérifie que le joueur a bien les sous
        $resultat = debiterPieces($coutDivorce,"divorce mariage") ;
        
        if ($resultat == 1){
        
            // on récupère qq infos sur le hamster qui divorce
            $query = "SELECT h.sexe, h.compagnon_id, j.joueur_id FROM hamster h, joueurs j WHERE h.hamster_id=".$hamster_id." AND h.joueur_id = j.joueur_id LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query);
            }
            $row = $dbHT->sql_fetchrow($result);
            if ($row['compagnon_id'] == 0) 
                $erreurAction = 2; // déjà divorcé
            else {
                // divorce !
                $query2 = "UPDATE hamster SET compagnon_id = 0 WHERE hamster_id IN (".$hamster_id.",".$row['compagnon_id'].")";
                if ( !($dbHT->sql_query($query2)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query2);
                }
                $lst_hamsters[hamsterCorrespondant($hamster_id)]['compagnon_id'] = 0;
                
                $hamsterCompagnonIndex = hamsterCorrespondant($row['compagnon_id']); // au cas où les 2 hamsters appartenaient au même joueur
                if ($hamsterCompagnonIndex != -1)
                    $lst_hamsters[$hamsterCompagnonIndex]['compagnon_id'] = 0;
                
                // prévenir le compagnon par mail
                if ($row['sexe'] == 0)
                    $message_texte = T_("La femme");
                else
                    $message_texte = T_("Le mari");
                $message_texte .= T_(" de ton hamster a décidé de divorcer... Nous sommes désolé pour cette mauvaise nouvelle. Dépêche-toi de le remarier avec un autre hamster !");
                
                envoyerMessagePrive($row['joueur_id'], $message_texte,$userdata['joueur_id'], true);
            }
        }
        else
            $erreurAction = 1;
    }
}
else if ($action == "annulerDemandeMariage") {
    if (isset($_GET['hamster_id'])) {
        $hamster_id = intval($_GET['hamster_id']);
    
        $query3 = "DELETE FROM interactions WHERE demandeur_id=".$hamster_id." AND type=".DEMANDE_MARIAGE;
        $dbHT->sql_query($query3);
        
        crediterPieces($coutDemandeMariage, false);
    }
}
else if ($action == "ajouterBulletinMariage") {
    
    $hamRow = $lst_hamsters[$hamsterIndex] ;
    
    if ($hamRow['compagnon_id'] == 0)
        return;
    
    $query = "SELECT MAX(bulletin_id) as bulletin_id FROM mariage_bulletin";
    $result = $dbHT->sql_query($query);
    if ( ! $result ) {
        message_die(GENERAL_ERROR, 'Error in obtaining max bulletin id', '', __LINE__, __FILE__, $query);
    }
    $row = $dbHT->sql_fetchrow($result);
    $nouveauId = $row[0] + 1;
    $dbHT->sql_freeresult($result);

    $texte = "";
    if (isset($_GET['bulletinTexte']))
        $texte = mysql_real_escape_string(stripslashes($_GET['bulletinTexte']));

    if ($texte != "") {
        $query = "INSERT INTO mariage_bulletin VALUES ( ".$nouveauId." , ".$hamRow['hamster_id']." , 
        '".$texte."', ".$dateActuelle.", '".$hamRow['nom']."'
        ) ";
        $result = $dbHT->sql_query($query);
            
        if ( ! $result ) {
            echo "Erreur SQL : ".$query."<br>" ;
            return ;
        }
    }
}
else if ($action=="ajouterAmi") {

    if (isset($_GET['pseudoAmi'])) {
        
        $pseudo = mysql_real_escape_string($_GET['pseudoAmi']);
        $joueur_ami_id = getJoueurIdFromPseudo($pseudo);
        if ($joueur_ami_id == -1) {
            $msg .= str_replace("#1",$pseudo,T_("<div>Désolé, il n'y a pas de joueur avec le pseudo #1. Vérifie l'orthographe...</div>"));
            $erreurAction = 1;
        }
        else if ($joueur_ami_id == $userdata['joueur_id']) {
            $msg .= T_("<div>Tu ne peux pas ajouter toi-même en tant qu'ami !</div>");
            $erreurAction = 2;
        }
        else{
            // 4 cas à gérer : 1) 1ère demande, 2) déjà demandé, 3) le joueur sollicité a déjà fait une demande, 4) le couple d'amis existe déjà
            
            // cas 3 : on vérifie qu'une demande du joueur sollicité n'est pas déjà en cours
            $query = "SELECT sollicite_id FROM amis_attente WHERE sollicite_id = ".$userdata['joueur_id']." AND demandeur_id = ".$joueur_ami_id." LIMIT 1";
                        
            if (getNbResults($query) > 0) {
                // on ajoute l'ami à la base de données
                ajouterAmi($userdata['joueur_id'],$joueur_ami_id) ;                
            }
            else {
                // cas 2 : déjà une demande
                $query = "SELECT sollicite_id FROM amis_attente WHERE demandeur_id = ".$userdata['joueur_id']." AND sollicite_id = ".$joueur_ami_id." LIMIT 1";
                        
                if (getNbResults($query) > 0) {
                    $msg .= str_replace("#1",$pseudo,T_("<div>Tu as déjà demandé au joueur #1 à faire partie de tes amis...</div>"));
                    $erreurAction = 4;
                }
                else {
                    // cas 4 : déjà un couple d'amis
                    $query = "SELECT joueur_id FROM amis WHERE joueur_id = ".$userdata['joueur_id']." AND ami_de = ".$joueur_ami_id." LIMIT 1";
                            
                    if (getNbResults($query) > 0) {
                        $pseudo = getPseudoFromId($joueur_ami_id);
                        $msg .= "<div>".T_("Tu es déjà ami(e) avec ").$pseudo."</div>";
                        $erreurAction = 5;
                    }
                    else {
                        // cas 1 : le joueur fait une première demande
                        $query = "INSERT INTO amis_attente VALUES(".$userdata['joueur_id'].",".$joueur_ami_id.")";
                        $dbHT->sql_query($query);
                        
                        $message_texte = str_replace("#1",$userdata['pseudo'],T_("(Message automatique) #1 a demandé à être ton ami. Pour accepter ou refuser, va sur la page des amis")." (<a href=\"options.php?mode=m_amis\">".T_("Mon Compte->Mes Amis")."</a>).");
                        envoyerMessagePrive($joueur_ami_id, $message_texte);
                    }
                }
            }
        }
    }
}
else if ($action == "enleverAmi") {
    if (isset($_GET['ami_id'])) {
        $ami_id = intval($_GET['ami_id']) ;
        
        $query = "DELETE FROM amis WHERE (joueur_id = ".$userdata['joueur_id']. " AND ami_de = ".$ami_id.") OR (joueur_id = ".$ami_id. " AND ami_de = ".$userdata['joueur_id'].") LIMIT 2";
        if ($dbHT->sql_query($query)) {
            $message_texte = str_replace("#1",$userdata['pseudo'],T_("(Message automatique) #1 a décidé de ne plus être ton ami..."));
            envoyerMessagePrive($ami_id, $message_texte);
        }
        else
            echo $query;
    }
}
else if ($action == "accepterDemandeAmi") {
    if (isset($_GET['demandeur_id'])) {
        $demandeur_id = intval($_GET['demandeur_id']) ;
        
        // on vérifie qu'il y a bien une demande en cours
        $query = "SELECT sollicite_id FROM amis_attente WHERE sollicite_id = ".$userdata['joueur_id']." AND demandeur_id = ".$demandeur_id." LIMIT 1";
                        
        if (getNbResults($query) > 0) {
            ajouterAmi($userdata['joueur_id'],$demandeur_id) ;                
        }
    }
}
else if ($action == "refuserDemandeAmi") {

    if (isset($_GET['demandeur_id'])) {
        $demandeur_id = intval($_GET['demandeur_id']) ;
        
        // on vérifie qu'il y a bien une demande en cours
        $query = "SELECT sollicite_id FROM amis_attente WHERE sollicite_id = ".$userdata['joueur_id']." AND demandeur_id = ".$demandeur_id." LIMIT 1";
                        
        if (getNbResults($query) > 0) {
            
            // on supprime cette demande et on envoie un message au demandeur
            $query = "DELETE FROM amis_attente WHERE demandeur_id=".$demandeur_id." AND sollicite_id=".$userdata['joueur_id'];
            if ($dbHT->sql_query($query)) {
                $message_texte = str_replace("#1",$userdata['pseudo'],T_("(Message automatique) #1 ne veut pas être ton ami..."));
                envoyerMessagePrive($demandeur_id, $message_texte);
            }
        }
    }
}
else if ($action == "enleverDemandeAmi") {
    
    if (isset($_GET['sollicite_id'])) {
        $sollicite_id = intval($_GET['sollicite_id']) ;
        
        // on supprime cette demande et on envoie un message au demandeur
        $query = "DELETE FROM amis_attente WHERE demandeur_id=".$userdata['joueur_id']." AND sollicite_id=".$sollicite_id;
        if (!$dbHT->sql_query($query))
            $erreurAction = 1;
    }
}
else if ($action=="defierJoueur") {

    if (isset($_GET['pseudoDefie'])) {
        
        $pseudo = mysql_real_escape_string($_GET['pseudoDefie']);
        $joueur_defie_id = getJoueurIdFromPseudo($pseudo);
        
        if ($joueur_defie_id == -1) {
            $msg .= str_replace("#1",$pseudo,T_("<div>Désolé, il n'y a pas de joueur avec le pseudo #1. Vérifie l'orthographe...</div>"));
            $erreurAction = 1;
        }
        else if ($joueur_defie_id == $userdata['joueur_id']) {
            $msg .= T_("<div>Tu ne peux pas te défier toi-même !</div>");
            $erreurAction = 2;
        }
        else{
            
            // le défié a t-il le niveau suffisant ?
            $query = "SELECT niveau, nb_pieces FROM joueurs WHERE joueur_id = $joueur_defie_id LIMIT 1";
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
            }
            $row=$dbHT->sql_fetchrow($result);
            $dbHT->sql_freeresult($result);
            if ($row['niveau'] < 8) {
                $msg .= "<div>".T_("Le joueur défié n'a pas encore atteint le niveau 8 pour pouvoir faire des défis.")."</div>";
                $erreurAction = 3;
            }
            else if ($userdata['niveau'] > $row['niveau'] + 1) {
                $msg .= "<div>".T_("Le joueur défié doit au moins avoir le niveau ").($row['niveau'] + 1)."</div>";
                $erreurAction = 3;
            }
            else {
                // quelle mise
                $mise = 1;
                if (isset($_GET['mise'])) {
                    $mise = max(1,intval($_GET['mise']));
                    $mise = min($mise,$userdata['nb_pieces']);
                    if ($mise > $row['nb_pieces'])
                        $msg .= "<div>".T_("Le joueur défié n'a que .").$row['nb_pieces']." ".T_("pièces. Le défi est limité à cette somme.")."</div>";
                    $mise = min($mise,$row['nb_pieces']);
                    $mise = min($mise,100);
                }
                
                ajouterInteraction($userdata['joueur_id'],$joueur_defie_id,TYPE_INTERACTION_DEFI,"",$mise);
                
                $message_texte = str_replace("#1",$userdata['pseudo'],T_("(Message automatique) #1 t\'a défié ! Va en ")."<a href=\"jeu.php?mode=m_concours\">".T_("Ville->Concours")."</a> ".T_("pour accepter ou refuser !"));
                envoyerMessagePrive($joueur_defie_id, $message_texte, HAMSTER_ACADEMY_ID,false);
            }
        }
    }
}
else if ($action == "refuserDefi") {

    if (isset($_GET['defi_id'])) {
        $defi_id = intval($_GET['defi_id']);
        $defieur_id = intval($_GET['defieur_id']); 
        
        // on supprime cette demande et on envoie un message au defieur
        $query = "DELETE FROM interactions WHERE interaction_id=".$defi_id." AND demandeur_id=".$defieur_id." AND receveur_id=".$userdata['joueur_id'];
        
        if ($dbHT->sql_query($query)) {
            $message_texte = str_replace("#1",$userdata['pseudo'],T_("(Message automatique) #1 a refusé ton défi."));
            envoyerMessagePrive($defieur_id, $message_texte, HAMSTER_ACADEMY_ID,false);
        }
    }
}
else if ($action == "annulerDefi") {

    if (isset($_GET['defi_id'])) {
        $defi_id = intval($_GET['defi_id']);
        $query = "DELETE FROM interactions WHERE interaction_id=".$defi_id." AND demandeur_id=".$userdata['joueur_id'];
        $dbHT->sql_query($query);
    }
}
else if ($action == "accepterDefi") {

    if (isset($_GET['defi_id'])) {
        $defi_id = intval($_GET['defi_id']);
        
        $query = "SELECT * FROM interactions WHERE interaction_id = $defi_id AND (receveur_id = ".$userdata['joueur_id']." OR demandeur_id = ".$userdata['joueur_id']." ) LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
        }
        
        $nbDefis = $dbHT->sql_numrows($result);
        if ($nbDefis < 1)
            return ;
        
        $rowDefi=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        
        // s'il y a mise, on débite
        $mise = $rowDefi['status'];
        if ($mise > 0) {
            debiterPiecesBDD($rowDefi['demandeur_id'],$mise);
            debiterPiecesBDD($rowDefi['receveur_id'],$mise);
        }
        
        // défi !
        // quel est le meilleur hamster joueur 1
        $meilleurScoreHamster1 = 0;
        $joueurDefieur = $rowDefi['demandeur_id'];
        $joueurDefie = $rowDefi['receveur_id'];
        $query = "SELECT note, nom, hamster_id FROM hamster WHERE joueur_id=".$joueurDefieur;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbHamsters = $dbHT->sql_numrows($result) ;
        $hamsterChoisi1 = rand(1,$nbHamsters);
        
        // ce n'est plus le meilleur hamster qu'on choisit mais un au hasard
        $i=1;
        $nomHamster1 = "";
        $idHamster1 = 0;
        $meilleurScoreHamster1 = 0;
        while($row=$dbHT->sql_fetchrow($result)) {
            
            if ($i == $hamsterChoisi1) {
                $meilleurScoreHamster1 = $row['note'];
                $nomHamster1 = $row['nom'];
                $idHamster1 = $row['hamster_id'];
                break;
            }
            $i++;
        }
        $dbHT->sql_freeresult($result);
        
        // quel est le meilleur hamster joueur 2
        $meilleurScoreHamster2 = 0;
        $query = "SELECT note, nom, hamster_id FROM hamster WHERE joueur_id=".$joueurDefie;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
        }
        $nbHamsters = $dbHT->sql_numrows($result) ;
        $hamsterChoisi2 = rand(1,$nbHamsters);
        
        // ce n'est plus le meilleur hamster qu'on choisit mais un au hasard
        $i=1;
        $nomHamster2 = "";
        $idHamster2 = 0;
        while($row=$dbHT->sql_fetchrow($result)) {
            if ($i == $hamsterChoisi2) {
                $meilleurScoreHamster2 = $row['note'];
                $nomHamster2 = $row['nom'];
                $idHamster2 = $row['hamster_id'];
                break;
            }
            $i++;
        }
        $dbHT->sql_freeresult($result);

        $ponderationHamster1 = (rand(1,20)-10)/10 + 1 ;
        $ponderationHamster2 = (rand(1,20)-10)/10 + 1 ;
        
        $gagnant_id = 0;
        $hamsterVainqueur = 0;
        if ($meilleurScoreHamster2*$ponderationHamster1 < $meilleurScoreHamster1*$ponderationHamster2){
            $gagnant_id = $joueurDefieur;
            $hamsterVainqueur = 1;
        }
        else{
            $gagnant_id = $joueurDefie;        
            $hamsterVainqueur = 2;
        }
        
        $perdant_id = $rowDefi['demandeur_id'];
        if ($perdant_id == $gagnant_id)
            $perdant_id = $rowDefi['receveur_id'];
            
        $pseudoGagnant = getPseudoFromId($gagnant_id);
        $pseudoPerdant = getPseudoFromId($perdant_id);
            
        if ($mise > 0)
            crediterPiecesBDD($gagnant_id,$mise*2);
        
        $query = "UPDATE joueurs SET nb_defis_gagnes = nb_defis_gagnes + 1, nb_defis_jour = nb_defis_jour + 1 WHERE joueur_id=".$gagnant_id;
        $dbHT->sql_query($query);

        $query = "UPDATE joueurs SET nb_defis_perdus = nb_defis_perdus + 1 WHERE joueur_id=".$perdant_id;
        $dbHT->sql_query($query);
       
        // résultat à envoyer
        $message_texte = str_replace("#1",$pseudoPerdant,T_("(Message automatique) Bravo ! Tu as gagné le défi contre #1 !"));
        if ($mise > 0)
            $message_texte .= " ".T_("Tu récupères sa mise :")." ".$mise." ".T_(" pièces").".";
        if ($gagnant_id == $userdata['joueur_id']) {
            $msg .= "<div>".$message_texte."</div>";
            
            if ($userdata['niveau'] == NIVEAU_DEFIS5_18 && $userdata['nb_defis_gagnes'] >= 4) // 4 au lieu de 5 car userdata n'est pas mis à jour au dernier coup
                updateNiveauJoueur(1,true);
            else if ($userdata['niveau'] == NIVEAU_CHINE && $userdata['nb_defis_gagnes'] >= 14)// 14 au lieu de 15 (idem)
                updateNiveauJoueur(2,true);
        }
        else
            envoyerMessagePrive($gagnant_id, $message_texte, HAMSTER_ACADEMY_ID,false);
        
        $message_texte = str_replace("#1",$pseudoGagnant,T_("(Message automatique) Désolé, tu as perdu le défi contre #1. Retente ta chance avec un autre joueur !"));
        if ($perdant_id == $userdata['joueur_id'])
            $msg .= "<div>".$message_texte."</div>";
        else
            envoyerMessagePrive($perdant_id, $message_texte, HAMSTER_ACADEMY_ID,false);
        
        // suppression du défi
        $query = "DELETE FROM interactions WHERE interaction_id=".$defi_id." AND receveur_id=".$userdata['joueur_id'];
        $dbHT->sql_query($query);
        
        // en mode combat
        $combatId = ajouterCombat($rowDefi['demandeur_id'],$rowDefi['receveur_id']);
        $txtResultat = T_("Résultat du combat :")." ".returnLienProfilHamster($idHamster1,$nomHamster1)." ";
        if ($hamsterVainqueur == 1)
            $txtResultat .= T_("a battu");
        else
            $txtResultat .= T_("a perdu face à");
        $txtResultat .= " ".returnLienProfilHamster($idHamster2,$nomHamster2);
        $txtResultat .= "<br/>".T_("C\'est le joueur")." ".$pseudoGagnant." ".T_("qui remporte le combat!");
        $txtResultat = addslashes($txtResultat);
        
        $query = "UPDATE combats SET vainqueur_id = ".$gagnant_id.", resultat_txt = '".$txtResultat."' WHERE combat_id =".$combatId;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining message', '', __LINE__, __FILE__, $query);
        }
    }
}
?>
