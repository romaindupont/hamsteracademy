<?php

define ('IN_HT', true);
define ('NO_SMARTY',true);

include "common.php";
include "gestion.php";
include "facebook.php";

$userdata = session_pagestart($user_ip);

//error_reporting(E_ALL);

if( ! $userdata['session_logged_in'] ){
    return;
}

$action = "";
$id = -1;

if (isset($_POST['action'])){
    
    $action = mysql_real_escape_string($_POST['action']);
    if (isset($_POST['action_id'])){
        $id = intval($_POST['action_id']);
    }
}
else if (isset($_GET['action'])){
    
    $action = mysql_real_escape_string($_GET['action']);
    if (isset($_GET['action_id'])){
        $id = intval($_GET['action_id']);
    }
}

if ($action == "publishCage") {
    
    $cageId_demandee = $id;
    
    $query = "SELECT nom FROM cage WHERE cage_id=".$cageId_demandee;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    $rowCage=$dbHT->sql_fetchrow($result);
    $dbHT->sql_freeresult($result);

    $nomCage = utf8_decode($rowCage['nom']);
    $message = $nomCage;
    
    define ('NO_OUTPUT_CAGE',true);
    require_once "graphiqueOffline.php";
        
    try
    {
        $args = array(
          'message' => $message,
        );
        $nomArg = $nomCage.'.png';
        $args[$nomArg] = '@'.realpath('images/cage/joueurs/'.$cageId_demandee.'_full_full.png');
        
        $tok = $sessionFacebook['access_token'];
        $url = 'https://graph.facebook.com/me/photos?access_token='.$tok;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $args);
        $data = curl_exec($ch);
        curl_close($ch);
        
        ajouterStats($userdata['joueur_id'],"publierCage",$cageId_demandee,$dateActuelle);

        echo T_("Cage publiée !");
    }
    catch (Exception $e)
    {
        $client_id    = CLE_PUBLIQUE;
        $display      = 'page';
        $scope         = 'publish_stream';
        $redirect_url = "http://apps.facebook.com/hamsteracademy/";
        $oauth_url    = 'https://graph.facebook.com/oauth/authorize?client_id=' . $client_id . '&redirect_uri=' . $redirect_url . '&type=web_server&display=' . $display . '&scope=' . $scope;
        $text = "<script type=\"text/javascript\">\ntop.location.href = \"$oauth_url\";\n</script>";
        echo $text;
    }
}

?>