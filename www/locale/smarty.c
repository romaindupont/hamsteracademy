/* ../templates//footer.tpl */
gettext("Partenaires");

/* ../templates//footer.tpl */
gettext("CGU");

/* ../templates//footer.tpl */
gettext("Charte du forum/tchat");

/* ../templates//footer.tpl */
gettext("Contr�le parental");

/* ../templates//footer.tpl */
gettext("Aide");

/* ../templates//footer.tpl */
gettext("Contact");

/* ../templates//footer.tpl */
gettext("Jouer � d'autres jeux");

/* ../templates//footer.tpl */
gettext("Tous droits r�serv�s");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Choisis un hamster que tu veux �lever");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Yeux");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Pelage");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("S�lectionner");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Accessible au niveau");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Choisis maintenant son pr�nom, son sexe et ses deux principaux caract�res (2 choix au maximum)");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Pr�nom du hamster");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Sexe du hamster");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("m�le");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("femelle");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("Caract�res");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("coquet");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("fort");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("r�sistant");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("sympa avec tout le monde");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("dragueur");

/* ../templates//formulaireNouvelHamster.tpl */
gettext("b�tisseur");

/* ../templates//index.tpl */
gettext("Le meilleur jeu virtuel d'�levage de hamsters !");

/* ../templates//index.tpl */
gettext("Le compte a �t� supprim�");

/* ../templates//index.tpl */
gettext("Ce compte a �t� temporairement suspendu");

/* ../templates//index.tpl */
gettext("jusqu'au");

/* ../templates//index.tpl */
gettext("Tu peux consulter Hamster Academy pour savoir pourquoi");

/* ../templates//index.tpl */
gettext("Erreur inconnue");

/* ../templates//index.tpl */
gettext("Pseudo");

/* ../templates//index.tpl */
gettext("Ce pseudo n'existe pas");

/* ../templates//index.tpl */
gettext("Mot de passe");

/* ../templates//index.tpl */
gettext("Mot de passe");

/* ../templates//index.tpl */
gettext("Mot de passe incorrect");

/* ../templates//index.tpl */
gettext("Si tu l'as oubli�, clique sur le lien");

/* ../templates//index.tpl */
gettext("retrouver mon mot de passe");

/* ../templates//index.tpl */
gettext("retrouver mon mot de passe");

/* ../templates//index.tpl */
gettext("Mot de passe perdu");

/* ../templates//index.tpl */
gettext("Pas encore inscrit");

/* ../templates//index.tpl */
gettext("Rejoins plus de 40 000 joueurs dont");

/* ../templates//index.tpl */
gettext("connect�s en ce moment m�me ! C'est gratuit");

/* ../templates//index.tpl */
gettext("S'inscrire, c'est gratuit");

/* ../templates//index.tpl */
gettext("S'inscrire, c'est gratuit");

/* ../templates//index.tpl */
gettext("Entrer dans Hamster Academy");

/* ../templates//index.tpl */
gettext("Entrer dans Hamster Academy");

/* ../templates//index.tpl */
gettext("Hamster Academy, jeu d'�levage virtuel de hamsters");

/* ../templates//index.tpl */
gettext("Un jeu compl�tement Hamster'Ouf !!");

/* ../templates//index.tpl */
gettext("Jeux partenaires � visiter");

/* ../templates//index.tpl */
gettext("Les nouveaut�s");

/* ../templates//index.tpl */
gettext("nouveaut�s");

/* ../templates//index.tpl */
gettext("Clique-ici pour voir toutes les derni�res nouveaut�s du jeu");

/* ../templates//index.tpl */
gettext("Toutes les autres nouveaut�s...");

/* ../templates//index.tpl */
gettext("Autour du jeu");

/* ../templates//index.tpl */
gettext("Clique sur la bo�te pour y mettre une remarque ou une id�e");

/* ../templates//index.tpl */
gettext("Clique sur la bo�te pour y mettre une remarque ou une id�e");

/* ../templates//index.tpl */
gettext("Bo�te � id�e");

/* ../templates//index.tpl */
gettext("Clique sur la bo�te � id�e pour faire part de tes remarques et id�es sur le jeu");

/* ../templates//index.tpl */
gettext("Les r�gles du jeu");

/* ../templates//index.tpl */
gettext("r�gles du jeu");

/* ../templates//index.tpl */
gettext("Les r�gles du jeu");

/* ../templates//index.tpl */
gettext("Les r�gles du jeu");

/* ../templates//index.tpl */
gettext("astuces et de bonnes id�es pour bien avancer dans le jeu");

/* ../templates//index.tpl */
gettext("Forum du jeu");

/* ../templates//index.tpl */
gettext("Forum du jeu");

/* ../templates//index.tpl */
gettext("Le Forum du jeu");

/* ../templates//index.tpl */
gettext("Astuces, amis, musique, solutions, jeux, blagues...");

/* ../templates//index.tpl */
gettext("Jeux virtuels gratuits");

/* ../templates//index.tpl */
gettext("Jeux virtuels gratuits");

/* ../templates//index.tpl */
gettext("Jeux virtuels gratuits");

/* ../templates//index.tpl */
gettext("Jeux virtuels gratuits");

/* ../templates//index.tpl */
gettext("d'autres jeux autour des hamsters, conseill�s par Hamster Academy ! A d�couvrir");

/* ../templates//index.tpl */
gettext("Nos partenaires");

/* ../templates//index.tpl */
gettext("Jeux partenaires � visiter");

/* ../templates//inscription.tpl */
gettext("Inscription � Hamster Academy");

/* ../templates//inscription.tpl */
gettext("L'inscription n'est pas encore finie, il faut corriger les probl�mes ci-dessous");

/* ../templates//inscription.tpl */
gettext("Bienvenue dans l'inscription de Hamster Academy");

/* ../templates//inscription.tpl */
gettext("Choisis ton pseudo et mot de passe");

/* ../templates//inscription.tpl */
gettext("Ton pseudo d'�leveur");

/* ../templates//inscription.tpl */
gettext("Mot de passe");

/* ../templates//inscription.tpl */
gettext("Adresse mail");

/* ../templates//inscription.tpl */
gettext("Si tu as un parrain, indique son pseudo");

/* ../templates//inscription.tpl */
gettext("Ton parrain est");

/* ../templates//inscription.tpl */
gettext("Tu es");

/* ../templates//inscription.tpl */
gettext("un gar�on");

/* ../templates//inscription.tpl */
gettext("une fille");

/* ../templates//inscription.tpl */
gettext("J'ai pris connaissance du");

/* ../templates//inscription.tpl */
gettext("r�glement int�rieur du jeu");

/* ../templates//inscription.tpl */
gettext("et je l'accepte sans r�serve");

/* ../templates//inscription.tpl */
gettext("Finir l'inscription");

/* ../templates//inscription_ok.tpl */
gettext("F�licitations");

/* ../templates//inscription_ok.tpl */
gettext("inscription termin�e");

/* ../templates//inscription_ok.tpl */
gettext("L'inscription est finie");

/* ../templates//inscription_ok.tpl */
gettext("Tu commences le jeu avec");

/* ../templates//inscription_ok.tpl */
gettext("une cage");

/* ../templates//inscription_ok.tpl */
gettext("Cage d'inscription");

/* ../templates//inscription_ok.tpl */
gettext("un biberon");

/* ../templates//inscription_ok.tpl */
gettext("une �cuelle");

/* ../templates//inscription_ok.tpl */
gettext("portions d'aliments");

/* ../templates//inscription_ok.tpl */
gettext("Tu peux jouer d�s maintenant !");

/* ../templates//inscription_ok.tpl */
gettext("En cliquant ici");

/* ../templates//inscription_ok.tpl */
gettext("jouer !");

