<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$smarty->assign('passwd', $passwd);
if ($erreurPasswd > 0) {
    $msgPasswdErreur = "";
    if ($erreurPasswd == 1)
      $msgPasswdErreur = T_("Choisis un mot de passe !");
    else if ($erreurPasswd == 2)
       $msgPasswdErreur = T_("Le mot de passe doit faire au moins 6 caractères et ne contenir que des chiffres et des lettres.");
    else if ($erreurPasswd == 3)
      $msgPasswdErreur = T_("Tu as saisi 2 mots de passe différents.");
    else
      $msgPasswdErreur = T_("Problème de mot de passe, re-essaye.");
    $smarty->assign('msgPasswdErreur', $msgPasswdErreur);
}
$smarty->assign('email', $email);
$smarty->assign('email_active', $userdata['email_active']);

$msgMailErreur = "";
if ($erreurMail == 1) 
    $msgMailErreur = T_("L'adresse mail ").$mailErrone.T_(" est invalide.");
else if ($erreurMail == 2)
    $msgMailErreur = T_("L'adresse mail ").$mailErrone.T_(" est déjà choisie par un autre joueur. Choisis-en une autre !");
else if ($erreurMail == 3)
    $msgMailErreur = T_("Tu as saisi 2 adresses mails différentes !");
else {
  if ($mode == "options" && $userdata['email_active'])
      $msgMailErreur =  T_("Activée !");
  else
      $msgMailErreur =  T_("non activée...");
}
$smarty->assign('msgMailErreur', $msgMailErreur);

if ($mode == "options") {
    if ($userdata['email_active'] || $erreurMail > 0)
        $msgMailActivation = "&nbsp;";
    else{
        $msgMailActivation .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;=> ".T_("Attention, l'adresse email n'est pas activée : l'activation permet de recevoir les cadeaux, de participer au concours, etc.");
        $msgMailActivation .= "<br/>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".T_("Veux-tu activer maintenant ton adresse email ? Clique ici :")." <input type=\"submit\" name=\"demanderActivation\" value=\"".T_("Activer maintenant !")."\" />" ;
        $msgMailActivation .= "<br/>&nbsp;<br/>";
        if ($statutActivation == 1) {
            $msgMailActivation .= "<span class=\"optionsValeurChampsErreur\">".T_("Un email a été envoyé à l'adresse mail :")." ".$email.". ".T_("Tu dois cliquer sur le lien inclus dans ce mail pour valider ton adresse mail").".<br/>&nbsp;<br/>".T_("Conseil : le mail peut arriver dans plusieurs heures. En attendant, vérifie que ton adresse mail est correcte.")."</span>";
            $msgMailActivation .= "<br/>&nbsp;<br/>";
        }
        else if ($statutActivation == 2) {
            $msgMailActivation .= "<span class=\"optionsValeurChampsErreur\">".T_("L'adresse email que tu as saisi")." (".$email.") ".T_("est invalide").".</span>";            
            $msgMailActivation .= "<br/>&nbsp;<br/>";
        }
    }
    $smarty->assign('msgmailActivation', $msgMailActivation);
}

$txtDateNaissance = "<select name=\"naissance_jour\">";

$i = 1;
while($i <= 31)
{
    $txtDateNaissance .= '<option value="'. $i .'" '.(($i == $naissance_jour)?'selected="selected"':'').'>'. $i .'</option>'."\n";
    $i++;
}
$txtDateNaissance .= "</select>
        <select name=\"naissance_mois\">";
$i = 1;
while($i <= 12)
{
    $txtDateNaissance .= '<option value="'. $i .'" '.(($i == $naissance_mois)?'selected="selected"':'').'>'. $mois_txt[$i] .'</option>'."\n";
    $i++;
}
$txtDateNaissance .= "</select>
        <select name=\"naissance_annee\">";
        
$i = 1940;
while($i <= date('Y')) 
{
    $txtDateNaissance .= '<option value="'. $i .'" '.(($i == $naissance_annee)?'selected="selected"':'').'>'. $i .'</option>'."\n";
    $i++;
}
$txtDateNaissance .= "</select>";
$smarty->assign('txtDateNaissance', $txtDateNaissance);    

$smarty->assign('sexe', $sexe);

$queryPays = "SELECT * FROM config_pays WHERE flag_id = ".$userdata['pays']." LIMIT 1";
if ( !($resultPays = $dbHT->sql_query($queryPays)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining pays', '', __LINE__, __FILE__, $queryPays);
}
$row=$dbHT->sql_fetchrow($resultPays);
$flag_image = $row['flag_image'];
$dbHT->sql_freeresult($resultPays);

$smarty->assign('flag_image', $flag_image);

$lst_pays = "";
$queryPays = "SELECT * FROM config_pays";
if ( !($resultPays = $dbHT->sql_query($queryPays)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining pays', '', __LINE__, __FILE__, $queryPays);
}
while($row=$dbHT->sql_fetchrow($resultPays)) {
    $lst_pays .= "<option ".( ($row['flag_id'] == $userdata ['pays'])? "selected=\"selected\"" : "" )." value=\"".$row['flag_id']."\">".$row['flag_name']."</option>\n";
}
$dbHT->sql_freeresult($resultPays);

$smarty->assign('lst_pays', $lst_pays);
$smarty->assign('lang', $lang);
$smarty->assign('ville', $ville);
$smarty->assign('blog', $blog);
$smarty->assign('presentation', stripslashes($presentation));

$lien_avatar = "";
if ($userdata['image'] > 0){
    $extensionImage = "jpg";
    if ($userdata['image'] == 2) // extension gif
        $extensionImage = "gif";
    
    $lien_avatar = "<img src=\"images/joueurs/".$userdata['joueur_id'].".".$extensionImage."\" alt=\"".$userdata['pseudo']."\" class=\"optionsNomChampsOptionnel\" /><br/>";
}
else
    $lien_avatar = "<img src=\"images/grande_silhouette.gif\" alt=\"".$userdata['pseudo']."\" class=\"optionsNomChampsOptionnel\" /><br/>";

$smarty->assign('lien_avatar', $lien_avatar);

$smarty->assign('oeuf7', afficherObjet("oeufs",7));

if ($userdata['image_bonus'] > 0 ){
    $image_bonus = "<img src=\"images/joueurs/".$userdata['joueur_id']."_bonus.jpg\" alt=\"".$userdata['pseudo']."\" class=\"optionsNomChampsOptionnel\"/><br/>";
    $smarty->assign('image_bonus', $image_bonus);
}

$smarty->assign('liste_rouge', $liste_rouge);
$smarty->assign('montrer_profil', $montrer_profil);
$smarty->assign('prevenir_nouveau_message', $prevenir_nouveau_message);
$smarty->assign('lienProfil', returnLienProfil($userdata['joueur_id'],T_("Voir !")));
$smarty->assign('lienVersProfil', "http://www.hamsteracademy.".( ($lang == "fr") ? "fr" : "com" )."/profil.php?joueur_id=".$userdata['joueur_id']);

$smarty->display('edit_profil.tpl');