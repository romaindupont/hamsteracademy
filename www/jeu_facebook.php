<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

// on récupère les informations du visiteur
try {
    $compteFacebook = $facebook->api('/me');
} catch (FacebookApiException $e) {
    error_log($e);
}

echo "<div style=\"font-size:9pt;\"><strong>".T_("Lie ton compte à Facebook ! Avantages : connexion automatique, retrouve tes amis, application facebook, etc.")."</strong><br/>&nbsp;<br/>";

//echo "<br/>".T_("Rappel : une des conditions pour devenir ")."<a href=\"jeu.php?mode=m_vip\">VIP</a> ".T_("est d'avoir 2 parrainages")."</div><br/>";
echo "</div><br/>";

echo "<div class=\"blocOptions\">";

if ($sessionFacebook && isset($compteFacebook) && $compteFacebook) { 
    
    // on checke si un joueur_id y est associé
    $query = "SELECT * FROM facebook WHERE joueur_id=".$userdata['joueur_id']." AND facebook_id = ".$compteFacebook['id'];
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbComptes = $dbHT->sql_numrows($result) ;
    
    if ($nbComptes == 0) {
        // on propose d'associer ce compte
        // une association facebook est dispo
        echo T_("Veux-tu associer ton compte Facebook")." (".$compteFacebook['name'].") ".T_("avec ton compte Hamster Academy ?");
    
        echo "<br/>&nbsp;<br/>";
        echo "<form method=\"get\" action=\"options.php\">";
        echo "<input type=\"hidden\" name=\"mode\" value=\"m_facebook\" />";
        echo "<input type=\"hidden\" name=\"facebook_id\" value=\"".$compteFacebook['id']."\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"associerCompteFacebook\" />";
        echo "<input type=\"submit\" value=\"".T_("Associer mon compte avec Facebook")."\" />";
        echo "</form>";
    }
    else {
        
        // options liées à FB
        $row=$dbHT->sql_fetchrow($result) ;
        echo T_("Ton compte Hamster Academy est lié au compte Facebook")." <a href=\"".$compteFacebook['link']."\" target=\"_blank\" title=\"".T_("Voir mon profil Facebook")."\"> ".$compteFacebook['name']."</a>.";
        
        echo "<br/>&nbsp;<br/>";
//        echo T_("Inviter mes amis à venir sur Hamster Academy :");
//        
//        echo "<a href=\"https://graph.facebook.com/oauth/authorize?
//            client_id=".CLE_PUBLIQUE."&
//            redirect_uri=http://www.hamsteracademy.fr/options.php?mode=m_facebook&
//            scope=read_friendlists\">lien</a>";
//        
//        $user = json_decode(file_get_contents(
//        'https://graph.facebook.com/me?access_token=' .
//        $cookie['access_token']))->id;
//                
//        $friends = json_decode(file_get_contents(
//            'https://graph.facebook.com/me/friends?access_token=' .
//            $cookie['access_token']), true);
//        
//        foreach ($friends as $lstFriend)

//        {
//            
//            
//            foreach ($lstFriend as $friend){
//                echo "un ami : " ;
//                echo $friend['id'] ." " ;
//               $friend_email = json_decode(file_get_contents(
//                    'https://graph.facebook.com/'.$friend['id'].'/email?access_token=' .
//                    $cookie['access_token']), true);
//                print_r($friend_email);
//                break;
//            }
//        }

        echo "<br/>&nbsp;<br/>";
        echo T_("Tu ne veux plus associer ton compte Facebook")." (".$compteFacebook['name'].") ".T_("avec ton compte Hamster Academy?");
    
        echo "<br/>&nbsp;<br/>";
        echo "<form method=\"get\" action=\"options.php\">";
        echo "<input type=\"hidden\" name=\"mode\" value=\"m_facebook\" />";
        echo "<input type=\"hidden\" name=\"action\" value=\"supprimerCompteFacebook\" />";
        echo "<input type=\"submit\" value=\"".T_("Supprimer l'association avec mon compte avec Facebook")."\" />";
        echo "</form>";
    }
    $dbHT->sql_freeresult($result);
    
}
else {
    echo T_("Connecte toi d'abord à Facebook :");
    echo "<fb:login-button perms=\"publish_stream,email\"></fb:login-button>";
    echo "
    <div id='fb-root'></div>";
    if ($lang == "fr")
        echo "<script src='http://connect.facebook.net/fr_FR/all.js'></script>";
    else
        echo "<script src='http://connect.facebook.net/en_US/all.js'></script>";
    echo "<script>
      FB.init({appId: '".CLE_PUBLIQUE."', status: true,
               cookie: true, xfbml: true});
      FB.Event.subscribe('auth.login', function(response) {
        window.location.reload();
      });
    </script>";

}


echo "</div>";

