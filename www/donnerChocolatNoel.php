<?php 

define('IN_HT', true);

include "common.php";
include "lstAccessoires.php";
include "gestion.php";

// au cas où on oublie de débrancher l'algo !
if ($month != 12 || $day >= 26)
    return ;

// on récupère la liste des joueurs qui possèdent le calendrier de l'Avent
$query = "SELECT joueur_id, quantite FROM accessoires WHERE type = ".ACC_CALENDRIER_AVENT;

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining calendrier_data', '', __LINE__, __FILE__, $query);
}
$nbChocolats = $dbHT->sql_numrows($result) ;

$lst_joueurs=array();
$type = ACC_CALENDRIER_CHOCOLAT;
$cage_id = -1;
$posX = $etage = $posY = 0;
$quantite = 1;

while($row=$dbHT->sql_fetchrow($result)) {

    $joueur_id = $row['joueur_id'] ;
    $quantite = $row['quantite'];
    
    // on donne un chocolat à chaque joueur pour chaque boite
    ajouterAccessoireDansBDD($type, $joueur_id, $cage_id, $posX, $etage, $posY, $quantite) ;

}

$dbHT->sql_freeresult($result);

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$h  = "From: contact@hamsteracademy.fr";
$subject = "Les chocolats ont été envoyés !" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= "nb de chocolat envoyés : ".$nbChocolats."\n";
$mail_sent = @mail($TO, $subject, $message, $h);

?>
