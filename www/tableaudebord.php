<?php 
define('IN_HT', true);
include "common.php";
include "lstAccessoires.php";
include "lstMetiers.php";
include "lstNiveaux.php";
include "gestion.php";
include "gestionUser.php";

error_reporting( E_ALL );

$univers = UNIVERS_ADMIN;

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo "Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

$userdata = session_pagestart($user_ip);
    
if( (! $userdata['session_logged_in'])){ 
    echo "Erreur de connection, veuillez contacter l'Hamster Academy si le problème persiste";
    redirectHT("index.php");
}

if ($userdata['droits'] < 1) {
    echo "Erreur de connection, veuillez contacter l'Hamster Academy si le problème persiste";
    redirectHT("index.php");
}

$msg = "";

// actions
$joueur_id = -1;
$cage_id = -1;
$hamster_id = -1;
$cadeauOk = 0;
$email="";
$pseudo="";
$mail_sent = 0;
$joueurData = array();

if (isset($_GET['cage_id']))
    $cage_id = intval($_GET['cage_id']);
if (isset($_GET['hamster_id']))
    $hamster_id = intval($_GET['hamster_id']);

if(isset($_GET['bannir_ip'])) {
    $ip_a_bannir = mysql_real_escape_string($_GET['bannir_ip']);
    
    $query = "INSERT INTO ip_bannies VALUES ('".$ip_a_bannir."')";
    $dbHT->sql_query($query);
}
if(isset($_GET['debannir_ip'])) {
    $ip_a_bannir = mysql_real_escape_string($_GET['debannir_ip']);
    
    $query = "DELETE FROM ip_bannies WHERE ip = '".$ip_a_bannir."'";
    $dbHT->sql_query($query);
}
    
$action = "";    
    
if(isset($_GET['action'])) {
    $action = mysql_real_escape_string($_GET['action']);
}
else if(isset($POST['action'])) {
    $action = mysql_real_escape_string($POST['action']);
}

if ($action == "lancerSauvegarde" && $userdata['droits'] >= DROITS_SUPER_ADMIN ) {
    include "sauvegarde_2512.php";
}
else if ($action == "ajouterTodo" && $userdata['droits'] >= DROITS_SUPER_ADMIN ) {
    
    if (isset($_GET['todo_texte'])) {
            
        $query = "SELECT MAX(todo_id) as todo_is FROM todo";
        $result = $dbHT->sql_query($query);
        if ( ! $result ) {
            message_die(GENERAL_ERROR, 'Error in obtaining max todo id', '', __LINE__, __FILE__, $query);
        }
        $row = $dbHT->sql_fetchrow($result);
        $nouveauId = $row[0] + 1;
        $dbHT->sql_freeresult($result);
    
        $todo_texte = mysql_real_escape_string($_GET['todo_texte']);
        $todo_jour_limite = mysql_real_escape_string($_GET['todo_jour_limite']);
        $todo_mois_limite = mysql_real_escape_string($_GET['todo_mois_limite']);
        $todo_annee_limite = mysql_real_escape_string($_GET['todo_annee_limite']);
        $timestamp = 0;    
        if ($todo_jour_limite > 0)
            $timestamp = mktime(0,0,1,$todo_mois_limite,$todo_jour_limite,$todo_annee_limite);
        
        $query = "INSERT INTO todo VALUES (".$nouveauId.",'".$todo_texte."',0,".$timestamp.")";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
    }    
}
else if ($action == "changerStatusTodo" && $userdata['droits'] >= DROITS_SUPER_ADMIN ) {
    
    if (isset($_GET['todo_id'])) {
        
        $todo_id = intval($_GET['todo_id']);
        
        $query = "UPDATE todo SET status = 1 WHERE todo_id = ".$todo_id;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "effacerTodo" && $userdata['droits'] >= DROITS_SUPER_ADMIN ) {
    
    if (isset($_GET['todo_id'])) {
        
        $todo_id = intval($_GET['todo_id']);
        
        $query = "DELETE FROM todo WHERE todo_id = ".$todo_id;
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
    }
}
else if ($action == "donnerObjetJoueursValides" && $userdata['droits'] >= DROITS_SUPER_ADMIN ) {
    
    if (isset($_GET['objetType'])) {
        $objetType = intval($_GET['objetType']) ;
        $query = "SELECT joueur_id FROM joueurs WHERE email_active = 1 AND newsletter=1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
        }
       $nbObjetsDonnes = 0;
        while($row=$dbHT->sql_fetchrow($result)) {
            ajouterAccessoireDansBDD($objetType,$row['joueur_id'],-1,0,0,0,1);
            $nbObjetsDonnes++;
        }
        $dbHT->sql_freeresult($result);
        $msg .= "<div>Nb d'objets donnes : ".$nbObjetsDonnes."</div>";
    }
}
if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['changerSystemeAmis'])) {
    $query = "SELECT * FROM amis";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result)) {
        $query2 = "INSERT INTO amis_attente VALUES(".$row['joueur_id'].",".$row['ami_de'].")";
        if ( ! $dbHT->sql_query($query2) ){
            message_die(GENERAL_ERROR, 'Error', '', __LINE__, __FILE__, $query);
        }
    }
    $dbHT->sql_freeresult($result);
}
elseif ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['copierBDD'])) {
    
    $table = "pun_topics";
    $query = "SELECT * FROM ".$table;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result,MYSQL_NUM)) {
        $row = array_map("addslashes",$row);
        $query2 = "INSERT INTO ".$table." VALUES('".implode("','",$row)."')";
        if ( !($dbForum->sql_query($query2)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query2);
        }
    }    
}

// si l'admin a noté les poemes
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['note_poeme'])) {
    
    // on récupère les joueurs
    $query = "SELECT * FROM lst_poemes_concours";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result)) {
    
        $notePoeme = 0;
        $noteChanson = 0;

        if (isset($_GET['notePoeme_'.$row['joueur_id'].''])) {
            $notePoeme = $_GET['notePoeme_'.$row['joueur_id'].''];
            $noteChanson = $_GET['noteChanson_'.$row['joueur_id'].''];
            
            // mise à jour des notes si différence
            if ($notePoeme != $row['notePoeme'] || $noteChanson != $row['noteChanson']) {
                $query16 = "UPDATE lst_poemes_concours SET notePoeme = '".$notePoeme."', noteChanson = '".$noteChanson."' WHERE joueur_id=".$row['joueur_id']." LIMIT 1" ;
                echo "query16 = ".$query16."<br/>";
                $dbHT->sql_query($query16) ;
            }
        }
    }
}
// si l'admin a noté les slogans
else if ($userdata['droits'] >= DROITS_ADMIN && (isset($_POST['note_slogan']) || isset($_GET['note_slogan']))) {
    
    // on récupère les joueurs
    $query = "SELECT * FROM lst_slogans_concours";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result)) {
    
        $note_slogan_fun = 0;
        $note_slogan_debile = 0;

        if (isset($_POST['note_slogan_fun_'.$row['joueur_id'].''])) {
            $note_slogan_fun = $_POST['note_slogan_fun_'.$row['joueur_id'].''];
            $note_slogan_debile = $_POST['note_slogan_debile_'.$row['joueur_id'].''];
            
            // mise à jour des notes si différence
            if ($note_slogan_fun != $row['notePoeme'] || $note_slogan_debile != $row['noteChanson']) {
                $query16 = "UPDATE lst_slogans_concours SET note_slogan_fun = '".$note_slogan_fun."', note_slogan_debile = '".$note_slogan_debile."' WHERE joueur_id=".$row['joueur_id']." LIMIT 1" ;
                //echo "query16 = ".$query16."<br/>";
                $dbHT->sql_query($query16) ;
            }
        }
    }
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['note_dessin'])) {
    
    // on récupère les joueurs
    $query = "SELECT * FROM joueurs WHERE inscritConcours > 0";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result)) {
    
        $noteDessin = 0;

        if (isset($_GET['noteDessin_'.$row['joueur_id'].''])) {
            $noteDessin_ = $_GET['noteDessin_'.$row['joueur_id'].''];
            
            // mise à jour des notes si différence
            if ($noteDessin_ != ($row['noteDessin_']-10) ) {
                $query16 = "UPDATE joueurs SET inscritConcours = '".($noteDessin_+10)."' WHERE joueur_id=".$row['joueur_id']." LIMIT 1" ;
                echo "query16 = ".$query16."<br/>";
                $dbHT->sql_query($query16) ;
            }
        }
    }
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['envoyerMailInterruption'])) {
    
    $query = "SELECT joueur_id, pseudo, email, date_inscription, user_lastvisit FROM joueurs WHERE date_inscription > ". mktime(22, 0, 0,2, 18,2008). " AND date_inscription < ". mktime(9, 0, 0,2, 20,2008). " AND user_lastvisit > ". mktime(9, 0, 0,2, 20,2008) ;

    if ( !($dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    
    $subject = "Les hamsters virtuels ont été retrouvés !" ;
            
    while($row=$dbHT->sql_fetchrow($result)) {
        if ($row['email'] != "") {
            
            $TO = $row['email'];
            
            $message = "Bonjour ".$row['pseudo'].",\n\n";
            $message .= "On a retrouvé ton hamster ! Une partie de nos hamsters virtuels se sont échappés lundi soir et mardi ... Ils sont de retour !\n\n";
            $message .= "Reviens-vite jouer dans l'Hamster Academy, tout le monde t'attend !\n\n";
            $message .= "A bientôt !\n\n--\nL'Hamster Academy";
            
            $mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);

            echo "mail envoyé à ".$row['email']." <br/>";
            echo "message = ".$message."<br/>&nbsp;<br/>";
        }
    }
            
    $row['email'] = "dupont.romain@gmail.com";
    $TO = $row['email'];
    
    $message = "Bonjour ".$row['pseudo'].",\n\n";
    $message .= "On a retrouvé ton hamster ! Une partie de nos hamsters virtuels se sont échappés lundi soir et mardi ... Ils sont de retour !\n\n";
    $message .= "Reviens-vite jouer dans l'Hamster Academy, tout le monde t'attend !\n\n";
    $message .= "A bientôt !\n\n--\nL'Hamster Academy";
    
    $mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);

    echo "mail envoyé à ".$row['email']." <br/>";
    echo "message = ".$message."<br/>&nbsp;<br/>";
    
    $dbHT->sql_freeresult($result);
    exit();
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['envoyerMessageAToutLeMonde'])) {
    
    $query = "SELECT joueur_id FROM joueurs LIMIT 48345,100000";

    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
    }
    
    $subject = addslashes("Bonjour, on t'envoie un message pour annoncer que le système des amis a changé. Dorénavant, ton ami(e) doit accepter d'être ton ami(e) ! Va vite sur la <a href=\"options.php?mode=m_amis\">page des amis</a> pour mettre à jour la liste de tes amis !") ;
    $nb_personnes = 0;
    
    while($row=$dbHT->sql_fetchrow($result)) {
        envoyerMessagePrive($row['joueur_id'],$subject,HAMSTER_ACADEMY_ID,false);
        $nb_personnes++;
    }
    
    $dbHT->sql_freeresult($result);
    echo "Message envoyé à ".$nb_personnes." : <br/>".$subject;
    exit();
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['voirMessages'])) {
    $query = "SELECT * FROM messages ORDER BY message_id DESC";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "#".$row['message_id']." : de ".$row['exp_joueur_id']." à ".$row['dest_joueur_id']." : ".stripslashes($row['message'])."<br/>";
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['miseAJourCageConcours'])) {
    // code transféré dans auto_concours_fininscription.php
    echo "code transféré dans auto_xxxx.php";
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauNoel'])) {
    $query = "SELECT joueur_id FROM accessoires WHERE type = ".ACC_CADEAU_NOEL. " ORDER BY joueur_id";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $prec_joueur_id = -1;
    $cadeau = 1;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {

        if ($prec_joueur_id == $row['joueur_id']) {
            $cadeau ++;
        }
        else {
            $prec_joueur_id = $row['joueur_id'];
            $cadeau = 1;
        }    
        
        // petit cadeau : 1 console de jeu
        if ($cadeau == 1) {
            $type = 92 ; // plante d'intérieur
            ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 1);
            envoyerMessagePrive($row['joueur_id'], "Joyeux Noël ! L hamster Academy t offre ce joli cadeau : une plante d intérieur ! A bientot ! :-)", $joueur_exp_id, false);
            echo "Cadeau (plante d'intérieur) envoyé au joueur ".$row['joueur_id']."<br/>";
        }
        else if ($cadeau == 2 || $cadeau > 3) { // ici cactus
            $type = ACC_CACTUS ;
            ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 1);
            envoyerMessagePrive($row['joueur_id'], "Joyeux Noël ! L hamster Academy t offre ce joli cadeau : un cactus ! Mets le vite dans la cage (Va dans l inventaire pour le voir) ! A bientot ! :-)", $joueur_exp_id, false);
            echo "Cadeau (cactus) envoyé au joueur ".$row['joueur_id']."<br/>";
        }
        else if ($cadeau == 3) { // ici arbre
            $type = ACC_CONSOLE_JEU ;
            ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 1);
            envoyerMessagePrive($row['joueur_id'], "Joyeux Noël ! L hamster Academy t offre ce joli cadeau : une console de jeu ! Mets le vite dans la cage (Va dans l inventaire pour le voir) ! A bientot ! :-)", $joueur_exp_id, false);
            echo "Cadeau (console de jeu) envoyé au joueur ".$row['joueur_id']."<br/>";
        }        
    }
    $dbHT->sql_freeresult($result);

    $query = "DELETE FROM accessoires WHERE type = ".ACC_CADEAU_NOEL;
    
    //if ( !($result = $dbHT->sql_query($query)) ){
    //    message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    //}
    
    //$dbHT->sql_freeresult($result);
    
    return ;
}
else if (0 && $userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauVotant'])) {
    $query = "SELECT joueur_id FROM joueurs WHERE a_vote = 1";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        // petit cadeau : 5 feuilles de salade
        $type = ACC_SALADE ;
        ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
        envoyerMessagePrive($row['joueur_id'], "Comme promis, on t offre un petit cadeau pour avoir voté au concours : de la salade !! :-) Pour les hamsters, c est super ! Ils adorent ça et c est super bon pour la santé ! A bientôt pour un prochain concours ! :-) ", $joueur_exp_id, false);
        echo "Salade envoyée au joueur ".$row['joueur_id']."<br/>";
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
// concours de la plus belle cage
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcours'])) {
    $query = "SELECT l.*, j.joueur_id, j.pseudo FROM lst_cages_concours l, joueurs j WHERE j.joueur_id = l.joueur_proprietaire_id ORDER BY (l.note + nb_accessoires*5) DESC";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $noteMax = 179;
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = round( 5+( ($row['note'] * 5) / $noteMax), 1 );
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 400;
        else if ($dernierClassementNonEx >= 2 && $dernierClassementNonEx < 11)
            $nb_pieces_donnees = 200;
        else if ($dernierClassementNonEx > 10 && $dernierClassementNonEx < 101)
            $nb_pieces_donnees = 50;
            
        $nb_pieces_donnees = round($nb_pieces_donnees,0);
        
        $precNote = $noteArrondie;
        
        if ($nb_pieces_donnees > 0) {
            echo $classement." : " .$row['pseudo']." a reçu ".$nb_pieces_donnees." pieces. ".returnLienCage($row['cage_id'],"Voir sa cage !")."<br/>\n" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$classement." au classement du concours de la plus belle cage ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! A bientôt ! :-)", $joueur_exp_id, false);
        }
        else{
            echo $classement." : " .$row['pseudo']." a reçu des bonbons ".returnLienCage($row['cage_id'],"Voir sa cage !")."<br/>\n" ;
            // petit cadeau : 5 bonbons
            $type = ACC_BONBON ;
            //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$classement." au classement du concours de la plus belle cage. En récompense de ta participation au concours, tu gagnes 5 bonbons pour ton ou tes hamster(s) ! A bientôt pour un nouveau concours où tu pourras retenter ta chance ! :-) ", $joueur_exp_id, false);
        }
        $classement ++;
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursGroupe'])) {
    //$query = "SELECT j.joueur_id, j.pseudo, g.groupe_id, g.note, h.hamster_id FROM joueurs j, groupes g, hamster h WHERE j.joueur_id = h.joueur_id AND h.groupe_id = g.groupe_id AND inscritConcours > 0 GROUP BY j.joueur_id ORDER BY g.note DESC";
    $query = "SELECT joueur_id, pseudo FROM joueurs WHERE inscritConcours > 0 ";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $nb_pieces_donnees = 100;
        
        if ($nb_pieces_donnees > 0) {
            echo $row['pseudo']." a reçu ".$nb_pieces_donnees." pieces (note=".$row['note'].")<br/>" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Concours du meilleur groupe à l Academy ! Les récompenses arrivent enfin ! Regarde le nombre de pièces que tu possèdes, tu devrais avoir au moins 100 pièces de plus ! (les premiers ont 500 pièces, les seconds 300 et les autres 100 pièces). A bientôt sur Hamster Academy !", $joueur_exp_id, true);
        }
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
// création de la table lst_cages_concours pour le concours
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['creer_table_concours'])) {

// code transféré au fichier auto_concours_fininscription.php
echo "code transféré au fichier auto_xxxx.php";

}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['cadeau'])) {
    $cadeau = intval($_GET['cadeau']);
    
    if ($cadeau > 0) {
        
        // on donne des vitamines
        $type = ACC_VITAMINES;
        $quantite = 5;
        $etage = -1;
        $posX = -1;
        $posY = -1;
        ajouterAccessoireDansBDD($type, $joueur_id, $cage_id, $posX, $etage, $posY, $quantite);

        // on donne un jouet pour hamster (cachette)
        $type = ACC_CACHETTE;
        $posX = 200;
        
        if ($cadeau == 1) {
            $type = ACC_CACHETTE;
            $posX = 200;
        }
        else if ($cadeau == 2){
            $posX = 150;
            $type = ACC_BALANCOIRE ;
        }
        
        $quantite = 1;
        $etage = 0;
        $posY = 0;
        ajouterAccessoireDansBDD($type, $joueur_id, $cage_id, $posX, $etage, $posY, $quantite);
        
        if ($email != "") {
            $TO = $email;
            $subject = "Deux cadeaux pour te remercier pour ta remarque glissée dans la Boîte à idées" ;
            $message = "Bonjour\n\nL'Hamster Academy a le plaisir de te donner un lot de 5 vitamines pour ton hamster ainsi qu'un nouveau jouet pour sa cage pour te remercier pour ta remarque glissée dans la Boîte à Idées. \n\n";
            $message .= "J'espère qu'ils te plairont ! Va vite les découvrir dans le jeu : http://www.hamsteracademy.fr ! \n\n";
            $message .= "L'équipe Hamster Academy\n";
            $mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);
        }
        $cadeauOk = 1;
    }
}

$tri="joueurId";
if (isset($_GET['tri'])) 
    $tri = mysql_real_escape_string($_GET['tri']);
    
$nbJours = 1;
if (isset($_GET['nbJours'])) 
    $nbJours = intval($_GET['nbJours']);

$pagetitle = "Hamster Academy - Tableau de bord";
$univers = UNIVERS_ADMIN;
$description = "";

$liste_styles = "style.css,styleAdmin.css";
$liste_scripts = "tooltip.js,gestionHamster.js,jquery.js,jquery-ui.min.js,jquery.cornflex.js,yellowbox.js";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->assign('pageWidth',1280);
$smarty->display('header.tpl');

?>
<script type="text/javascript" src="tinymce/jscripts/tiny_mce/tiny_mce_gzip.js"></script>
<script type="text/javascript">
// This is where the compressor will load all components, include all components used on the page here
tinyMCE_GZ.init({
plugins : 'save,advhr,advimage,advlink,emotions,iespell,media,contextmenu,preview',
themes : 'advanced',
disk_cache : true,
debug : false
});
</script>
<script type="text/javascript">
tinyMCE.init({
        mode : "exact",
        elements: "mail_html_saisi,texteAccueil,texteConcours",
        theme : "advanced",
        convert_urls : false,
        plugins : "save,advhr,advimage,advlink,emotions,iespell,media,contextmenu,preview",

        // Theme options
        theme_advanced_buttons1 : "save,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,fontselect,fontsizeselect",
        theme_advanced_buttons2 : "cut,copy,paste,pastetext,pasteword,|,blockquote,|,undo,redo,|,link,unlink,image,|,forecolor,backcolor,|,hr,emotions,code",
        theme_advanced_buttons3 : "",
        theme_advanced_toolbar_location : "top",
        theme_advanced_toolbar_align : "left",
        theme_advanced_statusbar_location : "bottom",
        theme_advanced_resizing : true,

        // Drop lists for link/image/media/template dialogs
        template_external_list_url : "lists/template_list.js",
        external_link_list_url : "lists/link_list.js",
        external_image_list_url : "lists/image_list.js"
    });
</script>
<!-- /TinyMCE -->

<h1 align=center>Administration</h1>


<div align=left>

<?php
if ($action == "editerListeAccessoires" && $userdata['droits'] >= DROITS_SUPER_ADMIN){
    
    // on applique les modifs sur le bouton submit a été pressé
    if (isset($_POST['submitEditerAccessoire'])) {
    
        for($acc_type=0;$acc_type<1000;$acc_type++) {
            
           if (isset($_POST['nom_fr_'.$acc_type])) {
               
               $nom_fr = mysql_real_escape_string($_POST['nom_fr_'.$acc_type]);                       
               $nom_en = mysql_real_escape_string($_POST['nom_en_'.$acc_type]);
               $desc_fr = mysql_real_escape_string($_POST['desc_fr_'.$acc_type]);
               $desc_en = mysql_real_escape_string($_POST['desc_en_'.$acc_type]);
               
               //echo $nom_fr."_".$nom_en."_".$desc_fr."_".$desc_en."<br/>";
               if (1 || $acc_type == 0) {
                $queryAcc = "UPDATE config_accessoires SET nom_fr = '$nom_fr', nom_en = '$nom_en', desc_fr = '$desc_fr', desc_en = '$desc_en' WHERE type=$acc_type";
                 if ( ! $dbHT->sql_query($queryAcc) ){
                        message_die(GENERAL_ERROR, 'Error in obtaining ', '', __LINE__, __FILE__, $queryAcc);
                 }
               }
           }
        }
    }
    
    $query = "SELECT * FROM config_accessoires";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining ', '', __LINE__, __FILE__, $query);
    }
    $nbAcc = $dbHT->sql_numrows($result) ;
    
    $index = 0;
    echo "<div>";
    echo "<table width=80% cellpadding=0 cellspacing=0 border=1 style=\"font-size:9pt;\"><tr><td>Type</td><td align=left>nom_fr</td><td align=left>nom_en</td><td align=left>desc_fr</td><td align=left>desc_en</td><td align=left>Actions</td></tr>";
    echo "<tr><td>&nbsp;</td><td align=left>&nbsp;</td><td align=left>&nbsp;</td><td align=left>&nbsp;</td><td align=left>&nbsp;</td><td align=left>&nbsp;</td></tr>";
    while($row=$dbHT->sql_fetchrow($result)) {
        
        if (isset($_POST['editerAcc_'.$index]) || isset($_GET['editerAcc_'.$index])) {
            
            echo "<form method=post><input type=hidden name=\"action\" value=\"editerListeAccessoires\" />";
            echo "<input type=hidden name=\"editerAcc_".$index."\" value=\"1\" />";
            echo "<tr><td>";
            echo $row['type'];
            echo "</td>";
            
            echo "<td><input type=text value=\"";
            echo $row['nom_fr'];
            //echo "</td>";
            echo "\" name=\"nom_fr_".$row['type']."\" /></td>";
            
            echo "<td><input type=text value=\"";
            echo $row['nom_en'];
            echo "\" name=\"nom_en_".$row['type']."\" /></td>";
            
            echo "<td><textarea cols=50 rows=4 name=\"desc_fr_".$row['type']."\" >";
            echo $row['desc_fr'];
            //echo "</td>";
            echo "</textarea></td>";
            
            echo "<td><textarea cols=50 rows=4 name=\"desc_en_".$row['type']."\" >";
            echo $row['desc_en'];
            echo "</textarea></td>";
            echo "<td align=center><input type=submit value=\"Enregistrer\" name=\"submitEditerAccessoire\" />";
            echo "</form></td></tr>\n";
        }
        else {
            echo "<tr><td>";
            echo $row['type'];
            echo "</td>";
            
            echo "<td>".$row['nom_fr']."</td>";
            echo "<td>".$row['nom_en']."</td>";
            echo "<td>".$row['desc_fr']."</td>";
            echo "<td>".$row['desc_en']."</td>";
            echo "<td><a href=\"tableaudebord.php?editerAcc_$index=1&amp;action=editerListeAccessoires\">Editer</a></td></tr>\n";
        }
        $index++;
    }
    echo "</table>";
    echo "</div>";
    $dbHT->sql_freeresult($result);
}
?>

    <?php  if ($userdata['droits'] >= DROITS_ADMIN) {
        
        if (isset($_GET['mailing']) || isset($_POST['mailing'])) {
        
            $mailing = 0;
            if (isset($_GET['mailing']))
                $mailing = intval($_GET['mailing']);
            if (isset($_POST['mailing']))
                $mailing = intval($_POST['mailing']);
            
            //if (isset($_POST['mail_texte']))
             //   $mail_texte = htmlentities(stripcslashes($_POST['mail_texte']));
            if (isset($_POST['mail_html_saisi']))
                $mail_html = stripslashes($_POST['mail_html_saisi']);                
            if (isset($_POST['mail_subject']))
                $mail_subject = htmlentities(stripslashes($_POST['mail_subject']),ENT_COMPAT,"UTF-8");
            if (isset($_POST['destinataire_mailing']))
                $destinataire_mailing = mysql_real_escape_string($_POST['destinataire_mailing']);
            if (isset($_POST['emails_choisis']))
                $emails_choisis = mysql_real_escape_string($_POST['emails_choisis']);               
            
            $nbJoueursConcernes = 0;
            
            if (isset($_GET['preparerMailing'])) {
                
                // on vide la précédente liste des joueurs qui auront le prochain mailing   
                $query = "TRUNCATE TABLE mailing";
                $dbHT->sql_query($query);
               
               // on ajoute les joueurs concernés
               $nbJoueursAjoutes = 0;
               $query = "SELECT joueur_id FROM joueurs j WHERE email_active = 1 AND newsletter = 1 AND langue=\"fr\"";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining joueur info', '', __LINE__, __FILE__, $query);
                }
                while($row=$dbHT->sql_fetchrow($result)) {
                    $queryInsertJoueur = "INSERT INTO mailing VALUES(".$row['joueur_id'].",1)";
                    $dbHT->sql_query($queryInsertJoueur);
                    $nbJoueursAjoutes++;
                }
                $dbHT->sql_freeresult($result);
                
                echo "<br/>Préparation de la mailing liste, nb de joueurs ajoutés : $nbJoueursAjoutes<br/>";
            }
            
            $query = "SELECT COUNT(j.joueur_id) as joueur_id FROM joueurs j, mailing m WHERE j.email_active=1 AND j.newsletter=1 AND j.joueur_id = m.joueur_id AND m.statut = 1";
            if ( ! $result = $dbHT->sql_query($query))
                echo "Pb de requete : $query<br/>";
            $row    = $dbHT->sql_fetchrow($result);
            $nbJoueursConcernes = $row[0];
            $dbHT->sql_freeresult($result);
            
            echo "<u>Mailing-Liste</u>";
            
            // saisie d'un mail
            if ($mailing == 1) {
                echo "<form action=\"tableaudebord.php\" method=\"post\" accept-charset =\"utf-8\">   ";
                echo "<input type=hidden name=mailing value=2 /><br/>";
                echo "<input type=textbox name=\"mail_subject\" value=\"Sujet du mail...\" size=\"80\" /><br/>";
                //echo "<table border=1><tr valign=top><td width=\"50%\" align=center><b>Texte alternatif</b></td><td width=\"50%\" align=center><b>Html</b> </td></tr>";
                //echo "<tr><td><textarea cols=50 rows=20 name=\"mail_texte\">Mail...</textarea></td>";
                echo "<textarea cols=80 rows=20 name=\"mail_html_saisi\">Mail en HTML...</textarea>";
                echo "<br/>";
                echo "Envoyer à :<br/>";
                echo "&nbsp;&nbsp;&nbsp;<input type=radio name=\"destinataire_mailing\" value=\"mailing_emails_choisis\" checked=\"checked\" />";
                echo "<input type=textbox name=\"emails_choisis\" value=\"contact@hamsteracademy.fr\" size=50 /> (pour plusieurs mails, les séparer par un \";\" )<br/>";
                echo "&nbsp;&nbsp;&nbsp;<input type=radio name=\"destinataire_mailing\" value=\"mailing_emails_valides\" /> tous les joueurs avec un email valide (au nombre de ".$nbJoueursConcernes.") (<a href=\"tableaudebord.php?mailing=1&amp;preparerMailing=1\">Préparer mailing-liste</a>)<br/>";
                echo "&nbsp;&nbsp;&nbsp;<input type=radio name=\"destinataire_mailing\" value=\"mailing_interne\" /> tous les joueurs via la poste (messagerie interne)<br/>";
                echo "<input type=submit name=submit_mailing value=\"Envoyer (confirmation préalable)\" />";
                echo "</form>";
            }            
            
            // validation d'un mail
            if ($mailing == 2) {
                ?>
                <script type="text/javascript">
                
                function envoiMailingAjax($indiceEnvoi) {
                    
                    var mail_html = $('#mail_html').html();
                    var mail_subject = $('#mail_subject').html();
                    var destinataire_mailing = "<?php echo $destinataire_mailing; ?>";
                    var emails_choisis = "<?php echo $emails_choisis; ?>";                       
                    
                    $.ajax({
                        type : 'POST', url : 'envoyerMailing.php', dataType : 'text',
                        data :{mail_html:mail_html,
                        mail_subject:mail_subject,
                        destinataire_mailing:destinataire_mailing,
                        emails_choisis:emails_choisis,
                        indiceEnvoi:$indiceEnvoi},
                        success : function(retour){
                            $tab = retour.split(',');
                            $continueEnvoi = $tab[0];
                            $nbMailsEnvoyes = $tab[1];
                            //alert($continueEnvoi+','+$nbMailsEnvoyes);
                            $('#resultat_envoi').html($('#resultat_envoi').html()+'<br/>Envoi '+$indiceEnvoi+', mails envoyes : '+$nbMailsEnvoyes) ;
                            if ($continueEnvoi == 1) {
                                envoiMailingAjax($indiceEnvoi+1); // suite du mailing
                            }
                            else {
                                $('#resultat_envoi').html($('#resultat_envoi').html()+'<br/>--Fin') ;
                            }
                        }
                    });
                    return false;
                };
                
                </script>
                <?php 
                echo "<div style=\"border:1px dashed gray; padding:15px 15px 15px 15px;\"><form action=\"tableaudebord.php\" method=\"post\">   ";
                echo "<input type=hidden name=mailing value=3 />";
                echo "<br/><u>Mail qui sera envoyé</u> :<br/>&nbsp;<br/>
                <div id=\"mail_subject\">$mail_subject</div><br/>";
                echo "<div style=\"border:1px dashed gray;\" id=\"mail_html\">$mail_html</div><br/>";
                
                if ($destinataire_mailing == "mailing_emails_choisis")
                    echo "<u>Destinataire</u> : ".$emails_choisis;
                else if ($destinataire_mailing == "mailing_emails_valides")
                    echo "<u>Destinataire</u> : $nbJoueursConcernes joueurs avec email valide";
                else  if ($destinataire_mailing == "mailing_interne")
                    echo "<u>Destinataire</u> : mailing interne";
                else
                    echo "<u>Destinataire</u> : inconnu";
                echo "<br/>&nbsp;<br/>";
                echo "<input type=submit name=submit_mailing value=\"Envoyer (définitif)\" onclick=\"envoiMailingAjax(0); return false;\" />";
                echo "</form><div id=\"resultat_envoi\">&nbsp;</div></div>";
            }
            
            // résultat de l'envoi
            if ($mailing == 3) {
                echo "<br/>&nbsp;<br/>Mailing envoyé !" ;   
            }
            echo "<br/>&nbsp;<br/>";
        }
        
        ?>
    <?php if (isset($_GET['donnerCadeauNoel'])) { ?>
        <u>Cadeaux</u> :
        <form action="tableaudebord.php" method="get">
        <br/>- donner 1x 
        <input type="hidden" name="action" value="donnerObjetJoueursValides">
        <select name="objetType">
        <?php 
        $query = "SELECT type, nom_fr FROM config_accessoires";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining objets', '', __LINE__, __FILE__, $query);
        }
        while($row=$dbHT->sql_fetchrow($result)) {
            echo "<option value=\"".$row['type']."\">".$row['nom_fr']."</option>\n";
        }
        $dbHT->sql_freeresult($result);
        ?>
        </select> à tous les joueurs (avec email valide) <input type=submit value="OK" /></form>    
    <?php } ?>
    <br><u>Actions</u> : <br/>- <a href="images/joueurs/afficherPhotos.php">voir toutes les images des joueurs</a> 
    <br/>- <a href="images/groupes/afficherPhotos.php">voir toutes les images des groupes</a> 
    <?php if ($userdata['droits'] >= DROITS_SUPER_ADMIN) { ?>
    - <a href="tableaudebord.php?stats=1">Statistiques</a>
    <br/>- <a href="tableaudebord.php?gerer_accessoires=1">Gérer les accessoires</a>
    <br/>- <a href="tableaudebord.php?mailing=1">Envoyer une mailing-liste</a>
    <br/>- <a href="tableaudebord.php?action=editerListeAccessoires">Editer liste accessoires</a>
    <br/>- <a href="filtrerEmails2512.php">Filtrer une liste d'emails invalides (OVH-Mail erreurs)</a>   
    <br>- <a href="tableaudebord.php?action=lancerSauvegarde">Lancer une sauvegarde des sources php css js html txt</a>
    <?php } ?>
    <br>- <a href="tableaudebord.php">Rafraîchir</a> 
    <br>- <a href="forum/admin_index.php">Tableau de bord du forum punBB</a>
    <br>&nbsp;<br/><u>Lien automatique vers la gazette</u> :<br/>
    <?php
    if (isset($_GET['numeroGazette'])){
        $numeroGazette = intval($_GET['numeroGazette']);
        updateConfig("numero_gazette",$numeroGazette);
    }
    if (isset($_GET['forumGazette'])){
        $forumGazette = intval($_GET['forumGazette']);
        updateConfig("forum_gazette",$forumGazette);
    }
    $numeroGazette = getConfig("numero_gazette");
    $forumGazette = getConfig("forum_gazette");
    echo "numéro actuel de la gazette : ".$numeroGazette." (<a href=\"\" onClick=\"javascript: var numeroGazette = prompt('Nouveau numero : ') ; document.location.href='tableaudebord.php?numeroGazette='+numeroGazette ;return false;\" >changer</a>)<br/>\n";
    echo "numéro <i>forum_id</i> où se trouve la gazette actuelle : ".$forumGazette." (<a href=\"\" onClick=\"javascript: var forumGazette = prompt('Nouveau forum_id : ') ; document.location.href='tableaudebord.php?forumGazette='+forumGazette ;return false;\" >changer</a>)<br/>\n";
    echo "<a href=\"forum/viewtopic.php?id=".$forumGazette."\" title=\"la Gazette des Modérateurs n°".$numeroGazette."\" target=_blank >Voir la Gazette n°".$numeroGazette." (test du lien)</a><br/>&nbsp;<br/>";
    ?>
    <br>&nbsp;<br/><u>Message d'accueil (qui apparaît de temps en temps)</u> :<br/>
    <?php
    if (isset($_GET['texteAccueil'])){
        $texteAccueil = mysql_real_escape_string($_GET['texteAccueil']);
        updateConfig("texteAccueil",$texteAccueil);
    }
    $texteAccueil = getConfig("texteAccueil");
    echo "<form method=get action=\"tableaudebord.php\"><textarea cols=50 rows=4 name=\"texteAccueil\" id=\"texteAccueil\" >".$texteAccueil."</textarea><input type=submit value=\"enregistrer\" /></form><br/>";
    ?>
    <br>&nbsp;<br/><u>Message pour le concours </u> :<br/>
    <?php
    if (isset($_GET['texteConcours'])){
        $texteConcours = mysql_real_escape_string($_GET['texteConcours']);
        updateConfig("texteConcours",$texteConcours);
    }
    $texteConcours = getConfig("texteConcours");
    echo "<form method=get action=\"tableaudebord.php\"><textarea cols=50 rows=4 name=\"texteConcours\" id=\"texteConcours\" >".$texteConcours."</textarea><input type=submit value=\"enregistrer\" /></form><br/>";
    ?>
    <br><u>Purger</u> :
    <br>- <a href="tableaudebord.php?purger=1">Purger vieux comptes (inactif depuis 3 mois avec adresse mail non validée)</a>
    <br>- <a href="tableaudebord.php?purgerMessages=1">Purger vieux messages (< 3 mois)</a>
    <br>- <a href="tableaudebord.php?purgerHistorique=1">Purger historique (naissance,mariage,etc) (< 1 mois)</a>
    <br>- <a href="tableaudebord.php?purgerGroupes=1">Purger groupes inactifs (< 4 semaines)</a>
    <?php 
    }
    ?>
    <br>- <a href="jeu.php">Revenir au jeu</a> - <a href="deconnecter.php">Quitter</a></div>

<?php 

if ($msg != "")
    echo "<div style=\"background:blue;\">".$msg."</div>";

if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['gerer_accessoires'])) {

// pour transférer le fichier lstAccessoires dans la BDD
//for($i=0;$i<=87;$i++){
//    $objet = $infosObjets[$i];
//    $query = "INSERT INTO config_accessoires VALUES ($i,'".addslashes($objet[0])."','".addslashes($objet[1])."',$objet[3],$objet[4],$objet[5],'".addslashes($objet[6])."',$objet[9],$objet[10],$objet[11],$objet[12],$objet[8],$objet[7],$objet[13]) ";
//    echo $query."<br/>";
//    if ( !($result = $dbHT->sql_query($query)) ){
//        message_die(GENERAL_ERROR, 'Error in obtaining accessoires', '', __LINE__, __FILE__, $query);
//    }
//}    
echo "<br/><form action=tableaudebord.php method=post><table cellpadding=0 cellspacing=0 border=1>";
echo "<tr><td>Type</td><td>image</td><td>Nom fr</td><td>Prix</td><td>Quantite</td><td>DansLaCage</td><td>Description fr</td><td>Rayon</td><td>Decal X</td><td>decal Y</td><td>Largeur</td><td>Hauteur</td><td>echelle</td></tr>";
$query = "SELECT * FROM config_accessoires";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining accessoires', '', __LINE__, __FILE__, $query);
}
$type=0;
while($row=$dbHT->sql_fetchrow($result)) {
    echo "<tr><td>$type</td><td><img src=\"images/".$row['image']."\" width=40></td><td>".$row['nom_fr']."</td><td>".$row['prix']."</td><td>".$row['quantite']."</td><td>".$row['danslacage']."</td><td>".$row['desc_fr']."</td><td>".$row['rayon']."</td><td></td><td></td></tr>";
    $type++;
}
echo"</table><br><input type=submit value=sauvegarder></form>";
}
// interface de notation pour les poemes
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['voirpoemes'])) {
    $query = "SELECT * FROM lst_poemes_concours ORDER BY noteChanson, notePoeme LIMIT 200";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    $indiceChanson = 0 ;
    echo "<div style=\"max-width:100%; float:left;  \">";
    echo "<form action=\"tableaudebord.php\" method=get><input type=hidden name=note_poeme value=1><input type=hidden name=voirpoemes value=1>";
    echo "<table border=1 width=100%><tr nowrap><td width=600>Poeme / chanson</td><td width=400>Note</td></tr>";
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<tr";
        if ($row['notePoeme'] == "-1" && $row['noteChanson'] == "-1")
            echo " " ;
        else
            echo " style=\"background-color:rgb(0,200,50);\" " ;
        echo "><td width=600 nowrap=nowrap><strong>".$row['pseudo']."</strong>";
        if (strlen($row['poeme']) > 22)
            echo "<br/><u>Poeme</u> :<br>".nl2br(strip_tags($row['poeme']))."<br/>&nbsp;";    
        if (strlen($row['chanson']) > 21)
            echo "<br/><u>Chanson</u> :<br>".nl2br(strip_tags($row['chanson']));
        echo "</td><td width=400 nowrap=nowrap>\nNote pour le poeme : <br/>";
        echo "<input type=hidden name=joueur_id value=\"".$row['joueur_id']."\">\n<select name=notePoeme_".$row['joueur_id'].">\n";
        echo "<option value=\"P\"".( ($row['notePoeme'] == "P") ? " selected " : "" ).">Plagiat</option>\n";
        echo "<option value=\"-1\"".( ($row['notePoeme'] == "-1") ? " selected" : "" ).">Non noté</option>\n";
        echo "<option value=\"0\"".( ($row['notePoeme'] == "0") ? " selected " : "" ).">0</option>\n";
        echo "<option value=\"1\"".( ($row['notePoeme'] == "1") ? " selected " : "" ).">1</option>\n";
        echo "<option value=\"2\"".( ($row['notePoeme'] == "2") ? " selected " : "" ).">2</option>\n";
        echo "<option value=\"3\"".( ($row['notePoeme'] == "3") ? " selected " : "" ).">3</option>\n";
        echo "<option value=\"4\"".( ($row['notePoeme'] == "4") ? " selected " : "" ).">4</option>\n";
        echo "<option value=\"5\"".( ($row['notePoeme'] == "5") ? " selected " : "" ).">5</option>\n";
        echo "<option value=\"6\"".( ($row['notePoeme'] == "6") ? " selected " : "" ).">6</option>\n";
        echo "<option value=\"7\"".( ($row['notePoeme'] == "7") ? " selected " : "" ).">7</option>\n";
        echo "<option value=\"8\"".( ($row['notePoeme'] == "8") ? " selected " : "" ).">8</option>\n";
        echo "<option value=\"9\"".( ($row['notePoeme'] == "9") ? " selected " : "" ).">9</option>\n";
        echo "</select>\n<br/>&nbsp;<br/>";
        echo "Note pour la chanson : <br/><select name=noteChanson_".$row['joueur_id'].">\n";
        echo "<option value=\"P\"".( ($row['noteChanson'] == "P") ? " selected " : "" ).">Plagiat</option>\n";
        echo "<option value=\"-1\"".( ($row['noteChanson'] == "-1") ? " selected" : "" ).">Non noté</option>\n";
        echo "<option value=\"0\"".( ($row['noteChanson'] == "0") ? " selected " : "" ).">0</option>\n";
        echo "<option value=\"1\"".( ($row['noteChanson'] == "1") ? " selected " : "" ).">1</option>\n";
        echo "<option value=\"2\"".( ($row['noteChanson'] == "2") ? " selected " : "" ).">2</option>\n";
        echo "<option value=\"3\"".( ($row['noteChanson'] == "3") ? " selected " : "" ).">3</option>\n";
        echo "<option value=\"4\"".( ($row['noteChanson'] == "4") ? " selected " : "" ).">4</option>\n";
        echo "<option value=\"5\"".( ($row['noteChanson'] == "5") ? " selected " : "" ).">5</option>\n";
        echo "<option value=\"6\"".( ($row['noteChanson'] == "6") ? " selected " : "" ).">6</option>\n";
        echo "<option value=\"7\"".( ($row['noteChanson'] == "7") ? " selected " : "" ).">7</option>\n";
        echo "<option value=\"8\"".( ($row['noteChanson'] == "8") ? " selected " : "" ).">8</option>\n";
        echo "<option value=\"9\"".( ($row['noteChanson'] == "9") ? " selected " : "" ).">9</option>\n";
        echo "</select>\n<br/>&nbsp;<br/>";
        
        echo "</td></tr>";
        $indiceChanson++;
    }
    $dbHT->sql_freeresult($result);
    echo "</table>";
    echo "<input type=submit value=\"valider les notes\"></form>";
    echo "</div>";
}
// interface de notation pour les slogans
else if ($userdata['droits'] >= DROITS_ADMIN && (isset($_GET['voirslogans']) || isset($_POST['voirslogans']))) {
    $query = "SELECT * FROM lst_slogans_concours ORDER BY note_slogan_fun, note_slogan_debile LIMIT 1000";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    $indiceChanson = 0 ;
    echo "<div style=\"max-width:100%; float:left;  \">";
    echo "<form action=\"tableaudebord.php\" method=\"post\"><input type=hidden name=note_slogan value=1><input type=hidden name=voirslogans value=1>";
    echo "<table border=1 width=100%><tr nowrap><td width=600>Slogans</td><td width=400>Note</td></tr>";
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<tr";
        if ($row['note_slogan_fun'] == "-1" && $row['note_slogan_debile'] == "-1")
            echo " " ;
        else
            echo " style=\"background-color:rgb(0,200,50);\" " ;
        echo "><td width=600 nowrap=nowrap><strong>".$row['pseudo']."</strong>";
        if (strlen($row['slogan_fun']) > 22)
            echo "<br/><u>Slogan fun</u> :<br>".nl2br(strip_tags($row['slogan_fun']))."<br/>&nbsp;";    
        if (strlen($row['slogan_debile']) > 21)
            echo "<br/><u>Slogan débile</u> :<br>".nl2br(strip_tags($row['slogan_debile']));
        echo "</td><td width=400 nowrap=nowrap>\nNote pour le slogan fun : <br/>";
        echo "<input type=hidden name=joueur_id value=\"".$row['joueur_id']."\">\n<select name=note_slogan_fun_".$row['joueur_id'].">\n";
        echo "<option value=\"P\"".( ($row['note_slogan_fun'] == "P") ? " selected " : "" ).">Copieur</option>\n";
        echo "<option value=\"-1\"".( ($row['note_slogan_fun'] == "-1") ? " selected" : "" ).">Non noté</option>\n";
        echo "<option value=\"0\"".( ($row['note_slogan_fun'] == "0") ? " selected " : "" ).">0</option>\n";
        echo "<option value=\"1\"".( ($row['note_slogan_fun'] == "1") ? " selected " : "" ).">1</option>\n";
        echo "<option value=\"2\"".( ($row['note_slogan_fun'] == "2") ? " selected " : "" ).">2</option>\n";
        echo "<option value=\"3\"".( ($row['note_slogan_fun'] == "3") ? " selected " : "" ).">3</option>\n";
        echo "<option value=\"4\"".( ($row['note_slogan_fun'] == "4") ? " selected " : "" ).">4</option>\n";
        echo "<option value=\"5\"".( ($row['note_slogan_fun'] == "5") ? " selected " : "" ).">5</option>\n";
        echo "<option value=\"6\"".( ($row['note_slogan_fun'] == "6") ? " selected " : "" ).">6</option>\n";
        echo "<option value=\"7\"".( ($row['note_slogan_fun'] == "7") ? " selected " : "" ).">7</option>\n";
        echo "<option value=\"8\"".( ($row['note_slogan_fun'] == "8") ? " selected " : "" ).">8</option>\n";
        echo "<option value=\"9\"".( ($row['note_slogan_fun'] == "9") ? " selected " : "" ).">9</option>\n";
        echo "</select>\n<br/>&nbsp;<br/>";
        echo "Note pour le slogan debile : <br/><select name=note_slogan_debile_".$row['joueur_id'].">\n";
        echo "<option value=\"P\"".( ($row['note_slogan_debile'] == "P") ? " selected " : "" ).">Copieur</option>\n";
        echo "<option value=\"-1\"".( ($row['note_slogan_debile'] == "-1") ? " selected" : "" ).">Non noté</option>\n";
        echo "<option value=\"0\"".( ($row['note_slogan_debile'] == "0") ? " selected " : "" ).">0</option>\n";
        echo "<option value=\"1\"".( ($row['note_slogan_debile'] == "1") ? " selected " : "" ).">1</option>\n";
        echo "<option value=\"2\"".( ($row['note_slogan_debile'] == "2") ? " selected " : "" ).">2</option>\n";
        echo "<option value=\"3\"".( ($row['note_slogan_debile'] == "3") ? " selected " : "" ).">3</option>\n";
        echo "<option value=\"4\"".( ($row['note_slogan_debile'] == "4") ? " selected " : "" ).">4</option>\n";
        echo "<option value=\"5\"".( ($row['note_slogan_debile'] == "5") ? " selected " : "" ).">5</option>\n";
        echo "<option value=\"6\"".( ($row['note_slogan_debile'] == "6") ? " selected " : "" ).">6</option>\n";
        echo "<option value=\"7\"".( ($row['note_slogan_debile'] == "7") ? " selected " : "" ).">7</option>\n";
        echo "<option value=\"8\"".( ($row['note_slogan_debile'] == "8") ? " selected " : "" ).">8</option>\n";
        echo "<option value=\"9\"".( ($row['note_slogan_debile'] == "9") ? " selected " : "" ).">9</option>\n";
        echo "</select>\n<br/>&nbsp;<br/>";
        
        echo "</td></tr>";
        $indiceSlogan++;
    }
    $dbHT->sql_freeresult($result);
    echo "</table>";
    echo "<input type=submit value=\"valider les notes\"></form>";
    echo "</div>";
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['voirdessins'])) {
    $query = "SELECT * FROM joueurs WHERE inscritConcours > 0 ORDER BY joueur_id";
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
    }
    $indiceDessin = 0 ;
    echo "<div style=\"max-width:100%; float:left;  \">";
    echo "<form action=\"tableaudebord.php\" method=get><input type=hidden name=note_dessin value=1><input type=hidden name=voirdessins value=1>";
    echo "<table border=1 width=100%><tr nowrap><td width=600>Dessin</td><td width=400>Note</td></tr>";
    while($row=$dbHT->sql_fetchrow($result)) {
        
        if ( ! file_exists  ( "images/joueurs/".$row['joueur_id']."_bonus.jpg" ))
            continue;
        
        echo "<tr";
        if ($row['inscritConcours'] < "11")
            echo " " ;
        else
            echo " style=\"background-color:rgb(0,200,50);\" " ;
        echo "><td width=600 nowrap=nowrap><strong>".$row['pseudo']." </strong> (id".$row['joueur_id'].")";
        echo "<br/><u>Dessin</u> :<br><img src=\"images/joueurs/".$row['joueur_id']."_bonus.jpg\" style=\"max-width:600px;\"><br/>&nbsp;";    
        echo "</td><td width=400 nowrap=nowrap>\nNote pour le dessin : <br/>";
        echo "<input type=hidden name=joueur_id value=\"".$row['joueur_id']."\">\n<select name=noteDessin_".$row['joueur_id'].">\n";
        echo "<option value=\"0\"".( ($row['inscritConcours'] == 10) ? " selected " : "" ).">Non noté</option>\n";
        echo "<option value=\"1\"".( ($row['inscritConcours'] == 11) ? " selected " : "" ).">1</option>\n";
        echo "<option value=\"2\"".( ($row['inscritConcours'] == 12) ? " selected " : "" ).">2</option>\n";
        echo "<option value=\"3\"".( ($row['inscritConcours'] == 13) ? " selected " : "" ).">3</option>\n";
        echo "<option value=\"4\"".( ($row['inscritConcours'] == 14) ? " selected " : "" ).">4</option>\n";
        echo "<option value=\"5\"".( ($row['inscritConcours'] == 15) ? " selected " : "" ).">5</option>\n";
        echo "<option value=\"6\"".( ($row['inscritConcours'] == 16) ? " selected " : "" ).">6</option>\n";
        echo "<option value=\"7\"".( ($row['inscritConcours'] == 17) ? " selected " : "" ).">7</option>\n";
        echo "<option value=\"8\"".( ($row['inscritConcours'] == 18) ? " selected " : "" ).">8</option>\n";
        echo "<option value=\"9\"".( ($row['inscritConcours'] == 19) ? " selected " : "" ).">9</option>\n";
        echo "</select>\n<br/>&nbsp;<br/>";        
        echo "</td></tr>";
        $indiceDessin++;
    }
    $dbHT->sql_freeresult($result);
    echo "</table>";
    echo "<input type=submit value=\"valider les notes\"></form>";
    
    echo "<br/><a href=\"tableaudebord.php?donnerCadeauConcoursDessin=1\">Envoyer les cadeaux</a><br/>";
    echo "</div>";
}
else if ($cadeauOk == 1 && $userdata['droits'] >= DROITS_SUPER_ADMIN ){
    echo "<div align=center>Les cadeaux ont été donnés au joueur id=".$joueur_id." pour la cage id=".$cage_id." et l'hamster id=".$hamster_id.".";
    if ($mail_sent == 0)
        echo " Son email (".$email.") n'a pas marché...";
    else
        echo " Son email (".$email.") a été envoyé avec succès...";
    echo "</div>";
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['stats'])) {

    // stats sur les métiers
    echo "Stats sur les métiers : <br><table border=1 cellpadding=2><tr><td>Métier</td><td>Nb de joueurs</td><td>salaire</td><td>formation</td><td>ratio (formation/salaire)(plus c'est élevé, moins c'est intéressant)</td></tr>";
    for($metier=0;$metier<$nbMetiers;$metier++) {
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE metier = ".$metier;
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<tr><td>".$lstMetiers[$metier][METIER_NOM]."</td><td>".$row[0]."</td><td>".$lstMetiers[$metier][METIER_SALAIRE]."</td><td>".$lstMetiers[$metier][METIER_COUT_FORMATION]."</td><td>".($lstMetiers[$metier][METIER_COUT_FORMATION] / $lstMetiers[$metier][METIER_SALAIRE])."</td></tr>";
        $dbHT->sql_freeresult($result);
    }
    echo "</table><br>";

    // stats sur les accessoires
    if (0) {
        echo "Stats sur les accessoires : <br><table border=1 cellpadding=2><tr><td>Image</td><td>Accessoire</td><td>Nb de joueurs</td><td>Depuis 4 semaines</td><td>Depuis 2 semaines</td></tr>";
        
        $query = "SELECT * FROM config_accessoires";
        $resultAcc = $dbHT->sql_query($query);
        
        while ($rowAcc = $dbHT->sql_fetchrow($resultAcc)){
            
            $query = "SELECT COUNT(joueur_id) as joueur_id FROM accessoires WHERE type = ".$rowAcc['type'];
            $result = $dbHT->sql_query($query);
            $row = $dbHT->sql_fetchrow($result);
            echo "<tr><td><img src=\"images/".$rowAcc['image']."\" width=50></td><td>".$rowAcc['nom_fr']."</td><td>".$row[0]."</td>";
            $dbHT->sql_freeresult($result);
            
            $query = "SELECT COUNT(joueur_id) as joueur_id FROM accessoires WHERE type = ".$rowAcc['type']." AND date_acquisition > ".($dateActuelle - (3600*24*7*4)) ;
            $result = $dbHT->sql_query($query);
            $row = $dbHT->sql_fetchrow($result);
            echo "<td>".$row[0]."</td>";
            $dbHT->sql_freeresult($result);

            $query = "SELECT COUNT(joueur_id) as joueur_id FROM accessoires WHERE type = ".$rowAcc['type']." AND date_acquisition > ".($dateActuelle - (3600*24*7*2)) ;
            $result = $dbHT->sql_query($query);
            $row = $dbHT->sql_fetchrow($result);
            echo "<td>".$row[0]."</td>";
            $dbHT->sql_freeresult($result);
                    
            echo "</tr>";
            
        }
        $dbHT->sql_freeresult($resultAcc);
        echo "</table><br/>";
    }
    
    // stats sur les achats après paiements
    echo "Stats sur les achats après paiements : <br><table border=1 cellpadding=2><tr><td>action</td><td>date</td></tr>";
    $query = "SELECT action, date FROM stats WHERE valeur = 2 ORDER BY date DESC LIMIT 50" ;
    $result = $dbHT->sql_query($query);
    while ( $row = $dbHT->sql_fetchrow($result)) {
        echo "<tr><td>".$row['action']."</td><td>".date("D M j G:i:s",$row['date'])."</td></tr>";        
    }
    $dbHT->sql_freeresult($result);
    echo "</table>";
    
    // stats sur les niveaux des joueurs
    echo "Stats sur les niveaux (fr) : <br><table border=1 cellpadding=2><tr><td>niveau</td><td>nombre</td><td>stagné depuis 1 semaine</td><td>stagné depuis 1 mois</td></tr>";
    for($niveau=1;$niveau<$nbNiveaux;$niveau++) {
        echo "<tr><td>$niveau</td>";
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'fr' ";
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'fr' AND user_lastvisit < ".($dateActuelle-3600*24*7);
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'fr' AND user_lastvisit < ".($dateActuelle-3600*24*30);
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        echo "</tr>";
    }
    echo "</table>";
    
    echo "Stats sur les niveaux (en) : <br><table border=1 cellpadding=2><tr><td>niveau</td><td>nombre</td><td>stagné depuis 1 semaine</td><td>stagné depuis 1 mois</td></tr>";
    for($niveau=1;$niveau<$nbNiveaux;$niveau++) {
        echo "<tr><td>$niveau</td>";
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'en' ";
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'en'  AND user_lastvisit < ".($dateActuelle-3600*24*7);
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        $query = "SELECT COUNT(joueur_id) as joueur_id FROM joueurs WHERE niveau = $niveau AND langue = 'en'  AND user_lastvisit < ".($dateActuelle-3600*24*30);
        $result = $dbHT->sql_query($query);
        $row = $dbHT->sql_fetchrow($result);
        echo "<td>".$row[0]."</td>";
        $dbHT->sql_freeresult($result);
        
        echo "</tr>";
    }
    echo "</table>";
}
else if (isset($_GET['purger'])) {
    
    $query = "SELECT joueur_id, pseudo, niveau, date_inscription, user_lastvisit, email_active FROM joueurs WHERE user_lastvisit < ".($dateActuelle - 3600*24*30*3). " AND email_active = 0 AND joueur_id > 1 LIMIT 1000";
    $result = $dbHT->sql_query($query);
    
    echo "nombre de comptes : ".$dbHT->sql_numrows($result) ." <br/>";
    
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<div>".$row['joueur_id']." => ".$row['pseudo']." : inscription le ".date("d/m/Y H:i:s",$row['date_inscription']).", derniere connection = ".date("d/m/Y H:i:s",$row['user_lastvisit']).", email_active = ".$row['email_active'].", niveau = ".$row['niveau']."</div>";
        supprimerCompte($row['joueur_id'], false);
    }
    $dbHT->sql_freeresult($result);
}
else if (isset($_GET['purgerMessages'])) {
    
    // suppression des messages de La Poste (3 mois d'ancienneté)
    $query = "SELECT message_id, date FROM messages WHERE date < ".($dateActuelle - 3600*24*30*3);
    $result = $dbHT->sql_query($query);    
    echo "nombre de messages supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    $dbHT->sql_freeresult($result);
    
    $query = "DELETE FROM messages WHERE date < ".($dateActuelle - 3600*24*30*3);
    $dbHT->sql_query($query);
    
    // suppression des messages des bulletins de groupes
    $query = "SELECT bulletin_id, date FROM groupe_bulletin WHERE date < ".($dateActuelle - 3600*24*30);
    $result = $dbHT->sql_query($query);    
    echo "nombre de messages de bulletins de groupe supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    $dbHT->sql_freeresult($result);
    
    $query = "DELETE FROM groupe_bulletin WHERE date < ".($dateActuelle - 3600*24*30);
    $dbHT->sql_query($query);
    
    // suppression des messages des bulletins de mariés
    $query = "SELECT bulletin_id, date FROM mariage_bulletin WHERE date < ".($dateActuelle - 3600*24*30);
    $result = $dbHT->sql_query($query);    
    echo "nombre de messages de bulletins de mariés supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    $dbHT->sql_freeresult($result);
    
    $query = "DELETE FROM mariage_bulletin WHERE date < ".($dateActuelle - 3600*24*30);
    $dbHT->sql_query($query);        
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['purgerParrainages'])) {
    
    // suppression des parrainages
    $query = "SELECT groupe_id, date_parrainage FROM parrainage WHERE date_parrainage < ".($dateActuelle - 3600*24*30*3);
    $result = $dbHT->sql_query($query);    
    echo "nombre de parrainages supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    $dbHT->sql_freeresult($result);
    
    $query = "DELETE FROM parrainage WHERE date_parrainage < ".($dateActuelle - 3600*24*30*3);
    $dbHT->sql_query($query);
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['purgerHistorique'])) {
    
    // suppression des parrainages
    $query = "SELECT date FROM historique WHERE date < ".($dateActuelle - 3600*24*30);
    $result = $dbHT->sql_query($query);    
    echo "nombre d'historiques supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    $dbHT->sql_freeresult($result);
    
    $query = "DELETE FROM historique WHERE date < ".($dateActuelle - 3600*24*30);
    $dbHT->sql_query($query);
}
else if ($userdata['droits'] >= DROITS_ADMIN && isset($_GET['purgerGroupes'])) {
    
    // suppression des groupes
    $query = "SELECT * FROM groupes WHERE derniere_activite < ".($dateActuelle - 3600*24*31);
    $result = $dbHT->sql_query($query);    
    echo "nombre de groupes supprimés : ".$dbHT->sql_numrows($result) ." <br/>";
    
    while($row=$dbHT->sql_fetchrow($result)) {
        echo "<div>".$row['groupe_id']." => ".$row['nom']." : derniere activite le ".date("d/m/Y H:i:s",$row['derniere_activite'])."</div>";
        dissoudreGroupe($row['groupe_id']);
    }
    $dbHT->sql_freeresult($result);
}else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursDessin'])) {
    $query = "SELECT * FROM joueurs WHERE inscritConcours > 0 ORDER BY inscritConcours DESC";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = $row['inscritConcours'];
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 500;
        else if ($dernierClassementNonEx == 2)
            $nb_pieces_donnees = 300;
        else if ($noteArrondie > 10)
            $nb_pieces_donnees = 100;
            
        $precNote = $noteArrondie;
        echo "\n";
        if ($nb_pieces_donnees > 0) {
            echo $dernierClassementNonEx.") <span ".tooltip('<img src=\'http://www.hamsteracademy.fr/images/joueurs/'.$row['joueur_id'].'_bonus.jpg\' alt=\'Dessin de '.$row['pseudo'].'\' style=\'vertical-align:middle; max-width:400px; max-height:400px;margin-top:2px; margin-bottom:2px;\' />')."><a href=\"http://www.hamsteracademy.fr/images/joueurs/".$row['joueur_id']."_bonus.jpg\"><img src=\"http://www.hamsteracademy.fr/images/joueurs/".$row['joueur_id']."_bonus.jpg\" alt=\"Dessin de \"".$row['pseudo']." style=\"vertical-align:middle; height:50px; margin-top:2px; margin-bottom:2px;\" /></a></span>";
            echo  $row['pseudo']." a reçu ".$nb_pieces_donnees." pieces (note=".$row['inscritConcours']."/20)<br/>\n" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du plus beau dessin ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! Félicitations pour ton dessin ! A bientôt ! :-)", $joueur_exp_id, true);
        }
        $classement ++;
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursForce'])) {
    $query = "SELECT h.joueur_id, h.puissance, h.hamster_id, h.nom, j.joueur_id, j.pseudo, j.inscritConcours 
        FROM hamster h, joueurs j 
        WHERE j.joueur_id = h.joueur_id AND j.inscritConcours = 1
        ORDER BY h.puissance DESC ";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = $row['puissance'];
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 500;
        else if ($dernierClassementNonEx == 2)
            $nb_pieces_donnees = 300;
        else if ($dernierClassementNonEx > 2 && $dernierClassementNonEx < 11)
            $nb_pieces_donnees = 100;
        else
            $nb_pieces_donnees = 0;
            
        $nb_pieces_donnees = round($nb_pieces_donnees,0);
        
        $precNote = $noteArrondie;
        
        if ($nb_pieces_donnees > 0) {
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu ".$nb_pieces_donnees." pieces. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a une force de ".$row['puissance']." ! <br/>" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster le plus sportif ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! A bientôt ! :-)", $joueur_exp_id, false);
        }
        else{
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu des bonbons. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a une force de ".$row['puissance']." ! <br/>" ;
            // petit cadeau : 5 bonbons
            $type = ACC_BONBON ;
            //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster le plus sportif. En récompense de ta participation au concours, tu gagnes 5 bonbons pour ton ou tes hamster(s) ! A bientôt pour un nouveau concours où tu pourras retenter ta chance ! :-) ", $joueur_exp_id, false);
        }
        $classement ++;
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursSlogans'])) {
    
    // on calcule d'abord la note maximale
    $query = "SELECT * FROM lst_slogans_concours";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    while($row=$dbHT->sql_fetchrow($result)) {
        $query = "UPDATE lst_slogans_concours SET note_max = ".(max($row['note_slogan_fun'],$row['note_slogan_debile']))." WHERE joueur_id = ".$row['joueur_id'];
        $dbHT->sql_query($query);
    }
    
    $dbHT->sql_freeresult($result);
        
    $query = "SELECT * FROM lst_slogans_concours WHERE note_max > 0 ORDER BY note_max DESC";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = 10+$row['note_max'];
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 500;
        else if ($dernierClassementNonEx > 1 && $dernierClassementNonEx < 11)
            $nb_pieces_donnees = 300;
        else if ($dernierClassementNonEx > 10 && $dernierClassementNonEx < 101)
            $nb_pieces_donnees = 100;
            
        $nb_pieces_donnees = round($nb_pieces_donnees,0);
        
        $precNote = $noteArrondie;
        
        if ($nb_pieces_donnees > 0) {
            if ($dernierClassementNonEx == $classement)
                echo $dernierClassementNonEx." : " ;
            else
                echo $dernierClassementNonEx." (ex-aequo) : " ;
            echo returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu ".$nb_pieces_donnees." pieces. Sa note pour son slogan est ".$noteArrondie." ! <br/>" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours des slogans ! Tu as eu ".$noteArrondie." / 20 ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! A bientôt ! :-)", $joueur_exp_id, false);
        }
        else{
            echo $dernierClassementNonEx." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu des bonbons. Sa note pour son slogan est ".$noteArrondie." ! <br/>" ;
            // petit cadeau : 5 bonbons
            $type = ACC_BONBON ;
            //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours des slogans. Tu as eu ".$noteArrondie." / 20 ! En récompense de ta participation au concours, tu gagnes 5 bonbons pour ton ou tes hamster(s) ! A bientôt pour un nouveau concours où tu pourras retenter ta chance ! :-) ", $joueur_exp_id, false);
        }
        $classement ++;
        echo "\n";
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursBeaute'])) {
    $query = "SELECT h.joueur_id, h.beaute, h.hamster_id, h.nom, j.joueur_id, j.pseudo, j.inscritConcours 
        FROM hamster h, joueurs j 
        WHERE j.joueur_id = h.joueur_id AND j.inscritConcours = 1
        ORDER BY h.beaute DESC ";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = $row['beaute'];
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 500;
        else if ($dernierClassementNonEx == 2)
            $nb_pieces_donnees = 300;
        else if ($dernierClassementNonEx > 2 && $dernierClassementNonEx < 11)
            $nb_pieces_donnees = 100;
        else
            $nb_pieces_donnees = 0;
            
        $nb_pieces_donnees = round($nb_pieces_donnees,0);
        
        $precNote = $noteArrondie;
        
        if ($nb_pieces_donnees > 0) {
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu ".$nb_pieces_donnees." pieces. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a une beauté de ".$row['beaute']." ! <br/>" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster le plus beau ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! A bientôt ! :-)", $joueur_exp_id, false);
        }
        else{
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu des bonbons et 1 stage de plein-air. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a une beauté de ".$row['beaute']." ! <br/>" ;
            // petit cadeau : 2 bonbons + stage
            $type = ACC_BONBON ;
            $type2 = ACC_STAGE_PLEIN_AIR;
            //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 2);
            //ajouterAccessoireDansBDD($type2, $row['joueur_id'], -1, -1, -1, -1, 1);
            //envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster le plus beau. En récompense de ta participation au concours, tu gagnes des bonbons pour ton ou tes hamster(s) ainsi qu un stage de plein-air pour son moral ! A bientôt pour un nouveau concours où tu pourras retenter ta chance ! :-) ", $joueur_exp_id, false);
        }
        $classement ++;
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}
else if ($userdata['droits'] >= DROITS_SUPER_ADMIN && isset($_GET['donnerCadeauConcoursMoral'])) {
    $query = "SELECT h.joueur_id, h.moral, h.hamster_id, h.nom, j.joueur_id, j.pseudo, j.inscritConcours 
        FROM hamster h, joueurs j 
        WHERE j.joueur_id = h.joueur_id AND j.inscritConcours = 1 AND h.moral > 10
        ORDER BY h.moral DESC ";
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining concours', '', __LINE__, __FILE__, $query);
    }
    
    $classement=1;
    $dernierClassementNonEx = 1;
    $precNote = -1;
    $nb_pieces_donnees=0;
    $joueur_exp_id = 1371; // HamsterAcademy
    
    while($row=$dbHT->sql_fetchrow($result)) {
        
        $noteArrondie = $row['moral'];
        $nb_pieces_donnees=0;

        if ($noteArrondie != $precNote){
            $dernierClassementNonEx    = $classement;
        }
        
        if ($dernierClassementNonEx == 1)
            $nb_pieces_donnees = 500;
        else if ($dernierClassementNonEx >= 2 && $dernierClassementNonEx < 11)
            $nb_pieces_donnees = 200;
        else if ($dernierClassementNonEx >= 11 && $dernierClassementNonEx < 101)
            $nb_pieces_donnees = 50;
        else
            $nb_pieces_donnees = 0;
            
        $nb_pieces_donnees = round($nb_pieces_donnees,0);
        
        $precNote = $noteArrondie;
        
        if ($nb_pieces_donnees > 0) {
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu ".$nb_pieces_donnees." pieces. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a un super moral de ".$row['moral']." ! <br/>\n" ;
            //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
            envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster au meilleur moral ! Bravo ! En récompense, tu gagnes ".$nb_pieces_donnees." pièces ! A bientôt ! :-)", $joueur_exp_id, false);
        }
        else{
            echo $classement." : " .returnLienProfil($row['joueur_id'], $row['pseudo'])." a reçu des bonbons et 1 stage de plein-air. Son hamster ".returnLienProfilHamster($row['hamster_id'],$row['nom'])." a un moral de ".$row['moral']." ! <br/>\n" ;
            // petit cadeau : 2 bonbons + sortie foot
            $type = ACC_BONBON ;
            $type2 = ACC_FOOT_JEU;
            //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 2);
            //ajouterAccessoireDansBDD($type2, $row['joueur_id'], -1, -1, -1, -1, 1);
            envoyerMessagePrive($row['joueur_id'], "Résultat du concours ! Tu es ".$dernierClassementNonEx." au classement du concours du hamster au meilleur moral. En récompense de ta participation au concours, tu gagnes des bonbons pour ton ou tes hamster(s) ainsi qu une sortie au foot ! A bientôt pour un nouveau concours où tu pourras retenter ta chance ! :-) ", $joueur_exp_id, false);
        }
        $classement ++;
    }
    $dbHT->sql_freeresult($result);
    
    return ;
}


if ($userdata['droits'] >= DROITS_MOD) {

    if (isset($_GET['pseudo']) && $_GET['pseudo'] != "")
        $pseudo = mysql_real_escape_string($_GET['pseudo']) ;
    if (isset($_GET['email']) && $_GET['email'] != "")
        $email = mysql_real_escape_string($_GET['email']) ;
    
    // si un joueur_id a été spécifié directement
    if (isset($_GET['joueur_id']) && ! isset($_GET['recherche']))
            $joueur_id = intval($_GET['joueur_id']);
    
    echo "<hr><div>Chercher un joueur : ";
    echo "<form action=\"tableaudebord.php\" method=get><input type=hidden name=recherche value=1>";
    echo "<table><tr><td>joueur id : </td><td><input type=textbox name=joueur_id value=\"\"></td>";
    echo "<td>pseudo : </td><td><input type=textbox name=pseudo value=\"".$pseudo."\"></td>";
    echo "<td>email : </td><td><input type=texbox name=email value=\"".$email."\"></td>";
    echo "<td><input type=submit value=\"Chercher\"></td></tr></table></form>";
    echo "<span style=\"font-size:8pt;\">(rappel : le caractere * permet de faire une recherche élargie)</span>";
    echo "</div><hr>";
        
    if ($joueur_id == -1 && isset($_GET['recherche'])){
        // on récupère tous les champs de la recherche
        
        // si aucun joueur id n'a été donné, on regarde le pseudo
        if ($_GET['joueur_id'] == "") {
            
            $query = ""; // en fonction si l'admin a choisi le pseudo ou l'email pour la recherche
            if ($_GET['pseudo'] != "") {
                $pseudoSoumis = $pseudo;
                $pseudoSoumis = str_replace("*","%",$pseudoSoumis);
                
                $query = "SELECT joueur_id, pseudo, email FROM joueurs WHERE pseudo LIKE '".$pseudoSoumis."' ORDER BY pseudo";
            }
            else {
                $mailSoumis = $email;
                $mailSoumis = str_replace("*","%",$mailSoumis);
                
                $query = "SELECT joueur_id, pseudo, email FROM joueurs WHERE email LIKE '".$mailSoumis."' ORDER BY pseudo";
            }
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error  ', '', __LINE__, __FILE__, $query);
            }
            $nbJoueursTrouves = $dbHT->sql_numrows($result);
            if ($nbJoueursTrouves == 1) {
                $row = $dbHT->sql_fetchrow($result);
                $joueur_id = $row['joueur_id'] ;
            }
            else if ($nbJoueursTrouves == 0) {
                echo "Aucun joueur trouvé";
            }
            else {
                echo $nbJoueursTrouves." joueurs trouvés, choix possibles :<br/>";
                while($row=$dbHT->sql_fetchrow($result)) {
                    echo "<a href=\"tableaudebord.php?joueur_id=".$row['joueur_id']."\">id : ".$row['joueur_id'].", pseudo : ".$row['pseudo'].", email : ".$row['email']."</a><br/>" ;
                }
            }
        }
        else {
            $joueur_id = intval($_GET['joueur_id']) ; // si l'id a été spécifié via la recherche
        }
    }
    
    // si un joueur a été trouvé ou déterminé
    if ($joueur_id != -1) {
    
        // infos sur le joueur sélectionné
        $query = "SELECT * FROM joueurs WHERE joueur_id=".$joueur_id." LIMIT 1";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
        }
        if ($dbHT->sql_numrows($result) == 1) {
            $joueurData=$dbHT->sql_fetchrow($result);
            $email = $joueurData['email'];
            $dbHT->sql_freeresult($result);    
        
            // on exécute les commandes sur ce joueur
            if (isset($_GET['suspendre'])) {
                
                // on dé-suspend le joueur
                if (intval($_GET['suspendre']) == 0) {
                    $query = "UPDATE joueurs SET suspendu = 0 WHERE joueur_id=".$joueur_id;
                    echo "Suspension du joueur ".$joueur_id." annulée<hr>";
                    $joueurData['suspendu'] = 0;
                    if ($userdata['droits'] < DROITS_ADMIN)
                        envoyerMessagePrive(HAMSTER_ACADEMY_ID,$userdata['pseudo']." a annulé la suspension de ".$joueurData['pseudo'],HAMSTER_ACADEMY_ID,false);
                }
                else { // on suspend le joueur
                    $query = "UPDATE joueurs SET suspendu = ".intval($_GET['suspendre'])." WHERE joueur_id=".$joueur_id;
                    echo "Joueur ".$joueur_id." suspendu<hr>";
                    $joueurData['suspendu'] = intval($_GET['suspendre']);
                    if ($userdata['droits'] < DROITS_ADMIN)
                        envoyerMessagePrive(HAMSTER_ACADEMY_ID,$userdata['pseudo']." a suspendu ".$joueurData['pseudo'],HAMSTER_ACADEMY_ID,false);
                }
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
                }
                
                $dbHT->sql_freeresult($result);
            }
            if (isset($_GET['suspendreChat'])) {
                
                if (intval($_GET['suspendreChat']) == 1) {
                   $nbVal = nbValeursBD("ajax_chat_bans","userID",$joueur_id);
                   if ($nbVal == 0){
                        $ipJoueur = 0;
                        if(function_exists('inet_pton')) {
                            // ipv4 & ipv6:
                            $ipJoueur = @inet_pton(decode_ip($joueurData['last_ip']));
                        }
                        // Only ipv4:
                        else
                            $ipJoueur = @pack('N',@ip2long(decode_ip($joueurData['last_ip'])));
                        $querySuspension = "INSERT INTO ajax_chat_bans VALUES($joueur_id,'".$joueurData['pseudo']."',NOW(),'$ipJoueur')";
                        $dbHT->sql_query($querySuspension);
                       $querySuspension = "DELETE FROM ajax_chat_online WHERE userID = $joueur_id";
                       $dbHT->sql_query($querySuspension);
                       $querySuspension = "DELETE FROM ajax_chat_messages WHERE userID = $joueur_id";
                       $dbHT->sql_query($querySuspension);
                   }
                }
                else if (intval($_GET['suspendreChat']) == 0) {
                    $nbVal = nbValeursBD("ajax_chat_bans","userID",$joueur_id);
                   if ($nbVal == 1){
                       $querySuspension = "DELETE FROM ajax_chat_bans WHERE userID = $joueur_id";
                       $dbHT->sql_query($querySuspension);
                   }
                }
            }
            else if (isset($_GET['crediter'])) {
                $credit = intval($_GET['crediter']);
                crediterPiecesBDD($joueur_id,$credit);
                $joueurData['nb_pieces'] += $credit;
            }
            else if (isset($_GET['nouveauNiveau'])) {
                $nouveauNiveau = intval($_GET['nouveauNiveau']);
                if ($nouveauNiveau < 1)
                    $nouveauNiveau  = 1;
                else if ($nouveauNiveau >= $nbNiveaux)
                    $nouveauNiveau  = $nbNiveaux-1;
                $query = "UPDATE joueurs SET niveau = $nouveauNiveau WHERE joueur_id=".$joueur_id;
                 if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
                }
                    
                $joueurData['niveau'] = $nouveauNiveau;
            }
            else if (isset($_GET['nouveauDroit'])) {
                if ($userdata['droits'] == DROITS_SUPER_ADMIN) {
                    $nouveauDroit = intval($_GET['nouveauDroit']);
                    if ($nouveauDroit == DROITS_SUPER_ADMIN)
                        $nouveauDroit  = DROITS_JOUEUR;
                    $query = "UPDATE joueurs SET droits = $nouveauDroit WHERE joueur_id=".$joueur_id;
                     if ( !($dbHT->sql_query($query)) ){
                        message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
                    }
                }
                    
                $joueurData['droits'] = $nouveauDroit;
            }
            else if (isset($_GET['resetPlaintes'])) {
                
                // on dé-suspend le joueur
                $query = "UPDATE joueurs SET nb_plaintes = 0 WHERE joueur_id=".$joueur_id;
                echo "Plaintes du joueur ".$joueur_id." remises à zéro<hr>";
                $joueurData['nb_plaintes'] = 0;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
                }
                $dbHT->sql_freeresult($result);
            }
            else if (isset($_GET['mettreVIP'])) {
                
                $query = "UPDATE joueurs SET vip = 1 WHERE joueur_id=".$joueur_id;
                echo "Le joueur ".$joueur_id." est vip<hr>";
                $joueurData['vip'] = 1;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating joueur_data', '', __LINE__, __FILE__, $query);
                }
                $dbHT->sql_freeresult($result);
            }          
        
            // on affiche les informations sur ce joueur
            echo "<div><b>Joueur trouvé : id ".$joueur_id."</b><br>&nbsp;<br/>";
            echo "pseudo: <a href=\"profil.php?joueur_id=".$joueurData['joueur_id']."\">".$joueurData['pseudo']."</a><br/>\n";
            if ($userdata['droits'] >= DROITS_SUPER_ADMIN) {
                echo "mot de passe: ".$joueurData['passwd']."<br/>\n";
            }
            echo "email : <a href=\"mailto:".$email."\">".$email."</a> (".($joueurData['email_active']==1?"validée":"non validée").")<br/>\n";
            
            // suspension
            echo "suspendu du jeu : " ;
            if ($joueurData['suspendu'] == 0){
                echo "non (suspendre <a href=\"tableaudebord.php?suspendre=".($dateActuelle+3600*48)."&amp;joueur_id=".$joueur_id."\">48 heures</a>";
                //if ($userdata['droits'] >= DROITS_ADMIN)
                    echo ", <a href=\"tableaudebord.php?suspendre=".($dateActuelle+3600*24*7)."&amp;joueur_id=".$joueur_id."\">1 semaine</a>";
                    if ($userdata['droits'] >= DROITS_SUPER_ADMIN)
                        echo ", <a href=\"tableaudebord.php?suspendre=1&amp;joueur_id=".$joueur_id."\">à durée indéterminée</a>";
                echo ")<br/>\n" ;
            }
            else if ($joueurData['suspendu'] > 0){
                if ($joueurData['suspendu'] == 1)
                    echo "oui, à durée indéterminée";
                else 
                    echo "oui, jusqu'au ".date("d/m/Y H:i:s",$joueurData['suspendu']);
            
                echo " (<a href=\"tableaudebord.php?suspendre=0&joueur_id=".$joueur_id."\">annuler</a>)<br/>\n";;
            }
            echo "suspendu du forum : " ;
            $nbVal = nbValeursBD("pun_bans","id",$joueur_id);
                        
            if ($nbVal == 0){
                echo "non (<a href=\"http://www.hamsteracademy.fr/forum/profile.php?section=admin&id=$joueur_id\">suspendre</a>)<br/>\n" ;
            }
            else 
                echo "oui (<a href=\"http://www.hamsteracademy.fr/forum/admin_bans.php\">annuler</a>)<br/>\n";
            echo "suspendu du chat : " ;
            $nbVal = nbValeursBD("ajax_chat_bans","userID",$joueur_id);
                        
            if ($nbVal == 0){
                echo "non (<a href=\"tableaudebord.php?suspendreChat=1&joueur_id=".$joueur_id."\">suspendre</a>)<br/>\n" ;
            }
            else 
                echo "oui (<a href=\"tableaudebord.php?suspendreChat=0&joueur_id=".$joueur_id."\">annuler</a>)<br/>\n";
            
            if ($userdata['droits'] >= DROITS_ADMIN) {
                echo "nb pieces : ".($joueurData['nb_pieces'])." (<a href=\"\" onClick=\"javascript: var nbPieces = prompt('Crediter : ') ; document.location.href='tableaudebord.php?joueur_id=".$joueur_id."&crediter='+nbPieces ;return false;\" >crediter</a>)<br/>\n";
                echo "niveau : ".($joueurData['niveau'])." (<a href=\"\" onClick=\"javascript: var niveau = prompt('Nouveau niveau : ') ; document.location.href='tableaudebord.php?joueur_id=".$joueur_id."&nouveauNiveau='+niveau ;return false;\" >changer</a>)<br/>\n";
                if ($userdata['droits'] == DROITS_SUPER_ADMIN)
                    echo "droits : ".($joueurData['droits'])." ( = ".$droits_txt[$joueurData['droits']].") (<a href=\"\" onClick=\"javascript: var droit = prompt('Nouveau droit : ') ; document.location.href='tableaudebord.php?joueur_id=".$joueur_id."&nouveauDroit='+droit ;return false;\" >changer</a>)<br/>\n";
            }
            echo "dernière connexion : ".date("d/m/Y H:i:s",$joueurData['user_lastvisit'])."<br/>\n";
            echo "dernière adresse ip : ".decode_ip($joueurData['last_ip']);
            
            // l'ip est-elle bannie ?
            $query = "SELECT * FROM ip_bannies WHERE ip = '".$joueurData['last_ip']."' LIMIT 1" ;
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbIp = $dbHT->sql_numrows($result) ;
            $dbHT->sql_freeresult($result);
            if ($nbIp == 1) 
                echo " (ip bannie : <a href=\"tableaudebord.php?joueur_id=".$joueurData['joueur_id']."&amp;debannir_ip=".($joueurData['last_ip'])."\">debannir</a>)";
            else
                echo " (<a href=\"tableaudebord.php?joueur_id=".$joueurData['joueur_id']."&amp;bannir_ip=".($joueurData['last_ip'])."\">bannir</a>) (empêche toute nouvelle inscription depuis cette ip)";
            
            echo "<br/>\n";
            echo "vip : ".$joueurData['vip']." (<a href=\"tableaudebord.php?mettreVIP=1&amp;joueur_id=".$joueur_id."\">activer son status vip</a>)<br/>\n";
            echo "nb plaintes : ".$joueurData['nb_plaintes']." (<a href=\"tableaudebord.php?resetPlaintes=1&amp;joueur_id=".$joueur_id."\">réinitialiser</a>)<br/>\n";
            $query_p = "SELECT * FROM stats WHERE joueur_id=".$joueur_id;
            if ( !($result_p = $dbHT->sql_query($query_p)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query_p);
            }
            if ($userdata['droits'] >= DROITS_SUPER_ADMIN) {
                $nb_paiements = $dbHT->sql_numrows($result_p);
                echo "nb paiements : ".$nb_paiements ;
                if ($nb_paiements > 0) {
                    while($row_p=$dbHT->sql_fetchrow($result_p)) {
                        echo " (".$row_p['action']." ".date("d/m/Y H:i:s",$row_p['date']). ")";
                    }
                }
                $dbHT->sql_freeresult($result_p);
            }
            echo "<br/>&nbsp;<br/>";            
            
            // liste des messages envoyés par le joueur
            if ($userdata['droits'] >= DROITS_SUPER_ADMIN) {
                echo "<b>messages envoyés</b> (";        
                $query = "SELECT * FROM messages WHERE exp_joueur_id=".$joueur_id." ORDER BY date ASC";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
                }
                echo $dbHT->sql_numrows($result).") : <br/>\n" ;
                while($row=$dbHT->sql_fetchrow($result)) {
                    echo "à id:<a href=\"tableaudebord.php?joueur_id=".$row['dest_joueur_id']."\">".$row['dest_joueur_id']."</a> : ".$row['message']."<br/>";
                }
                $dbHT->sql_freeresult($result);
        
                // liste des messages reçus par le joueur
                echo "<br/>&nbsp;<br/><b>messages reçus</b> (";        
                $query = "SELECT * FROM messages WHERE dest_joueur_id=".$joueur_id." ORDER BY date ASC";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
                }
                echo $dbHT->sql_numrows($result).") : <br/>" ;
                while($row=$dbHT->sql_fetchrow($result)) {
                    echo "de id:<a href=\"tableaudebord.php?joueur_id=".$row['exp_joueur_id']."\">".$row['exp_joueur_id']."</a> : ".$row['message']."<br/>";
                }
                $dbHT->sql_freeresult($result);        
                echo "</div>";
            }
        }
        else {
            echo "<div>aucun joueur avec cet identifiant : ".$joueur_id."</div>";
            $dbHT->sql_freeresult($result);
        }
        
    
    }
}
if ($userdata['droits'] >= DROITS_SUPER_ADMIN) {
?>
<br/>&nbsp;<br/>
    <u>Todo liste</u> : <br/>
    <?php 
        $query = "SELECT * FROM todo ORDER BY status";
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error in obtaining accessoires', '', __LINE__, __FILE__, $query);
        }
        $nbTodos = $dbHT->sql_numrows($result) ;
        if ($nbTodos > 0) {
            echo "<table border=0 cellspacing=0 cellpadding=0><tr><td align=\"left\">Todo</td><td align=\"center\">Status</td><td align=\"center\">Date limite</td></tr>";
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "<tr><td>".$row['texte']."</td>";
                if ($row['status'] == 0)
                    echo "<td><a href=\"tableaudebord.php?action=changerStatusTodo&amp;todo_id=".$row['todo_id']."\" title=\"effectué !\">à faire</a></td>";
                else
                    echo "<td><a href=\"tableaudebord.php?action=effacerTodo&amp;todo_id=".$row['todo_id']."\" title=\"supprimer !\">effectué</a></td>";
                if ($row['date_limite'] > 0) {
                    $dateStr = date("d/m/Y",$row['date_limite']) ;
                    if ($row['date_limite'] < $dateActuelle + (3600*24)) 
                        echo "<td align=\"center\"><span class=\"txtErreur\">".$dateStr."</span></td></tr>";
                    else
                        echo "<td align=\"center\">".$dateStr."</td></tr>";
                }
                else
                    echo "<td align=\"center\">-</td></tr>";
            }
            echo "</table>";
        }
        ?>
        <form method="get" action="tableaudebord.php">
        <input type="hidden" name="action" value="ajouterTodo" />
        <input type="textbox" name="todo_texte" size="100" />, au plus tard le : 
        <input type="textbox" name="todo_jour_limite" value="0" size="2" />/
        <input type="textbox" name="todo_mois_limite" value="0" size="2" />/
        <input type="textbox" name="todo_annee_limite" value="2008" size="4" />
        <input type="submit" name="submit_todo" value="Ajouter" />
        </form>
<?php } ?>

</div>

<br/>

</body>
</html>

<?php $dbHT->sql_close(); ?>
 
