<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";
include "lstAccessoires.php";
include "lstInstruments.php";
include "lstHamsters.php";

// pour affichage public de l'hamster
$hamster_id = -1 ;
$cage_id = -1 ;
if (isset($_GET['hamster_id'])) {
    $hamster_id = intval($_GET['hamster_id']);
}

// mode d'affichage : reduit = sans page web et infos condensees, sinon normal
$modeAffichage = "normal";
if (isset($_GET['modeAffichage'])) {
    $modeAffichage = mysql_real_escape_string($_GET['modeAffichage']);
}

// on recupere l'hamster
$query = "SELECT * FROM hamster WHERE hamster_id=".$hamster_id." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$nbHamsters = $dbHT->sql_numrows($result) ;
if ($nbHamsters != 1) {
    echo T_("Il n'y a aucun hamster avec cet id. Verifier l'id et recommencer.") ;
    return ;
}
$hamster=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

// on recupere qq infos sur le joueur
$query = "SELECT * FROM joueurs WHERE joueur_id=".$hamster['joueur_id']." LIMIT 1";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining joueur_data', '', __LINE__, __FILE__, $query);
}
$nbJoueurs = $dbHT->sql_numrows($result) ;
if ($nbJoueurs != 1) {
    echo T_("Il n'y a aucun joueur associe au hamster") ;
}
$userdata=$dbHT->sql_fetchrow($result);
$dbHT->sql_freeresult($result);

$faim = faim($hamster['dernier_repas']) ;

$pagetitle = "Hamster Academy - Hamster : ".$hamster['nom'] ;
$description = "Hamster Academy - ".T_("voir le hamster ").$hamster['nom'] . ". ".T_("Inscris-toi sur Hamster Academy et adopte ton propre hamster !");
$liste_styles = "style.css,styleEleveur.css";
$univers = UNIVERS_ELEVEUR;
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

if (possedeAccessoire(ACC_AMULETTE_FORCE,$hamster['joueur_id']) != -1 )
    $hamster['puissance'] *= $puissanceAmuletteForce;
?>

<div align="center">

<h2 align=center>Hamster Academy</h2>
<h3><?php echo $hamster['nom'] ;?></h3>
<img src="images/<?php echo $infosHamsters[$hamster['type']][HAMSTER_IMAGE] ; ?>" width=150><br/>

<table border=0 align=center>
    <tr>
        <td>Aliments : </td><td><?php dessinerTableauNote($faim,10) ; ?></td>
        <td><?php echo round($faim)."/10 " ; ?></td>
    </tr>
    <tr>
        <td>Moral : </td><td><?php dessinerTableauNote($hamster['moral'],10) ; ?></td>
        <td><?php echo $hamster['moral']."/".$hamster['maxMoral']." " ; ?></td>
    </tr>
    <tr>
        <td>Santé : </td><td><?php dessinerTableauNote($hamster['sante'],10) ; ?></td>
        <td><?php echo $hamster['sante']."/".$hamster['maxSante']." " ; ?></td>
    </tr>
    <tr>
        <td>Force : </td><td><?php dessinerTableauNote($hamster['puissance'],10) ; ?></td>
        <td><?php echo $hamster['puissance']."/".$hamster['maxForce']." " ; ?></td>
    </tr>
    <tr>
        <td>Beauté : </td><td><?php dessinerTableauNote($hamster['beaute'],10) ; ?></td><td><?php echo $hamster['beaute']."/".$hamster['maxBeaute']." " ; ?></td>
    </tr>
    <tr>
        <td>Poids : </td><td><?php dessinerTableauPoids($hamster['poids']) ; ?></td><td><?php echo $hamster['poids']." gr." ; ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <tr>
        <td>Specialité : </td><td><?php 
$instrument = $hamster['specialite'];
if ($instrument != -1){
    echo "<img src=\"images/".getImageAccessoire($lstInstruments[$instrument][INSTRUMENT_REF_OBJET])."\" align=absmiddle height=30> ".$lstInstruments[$instrument][INSTRUMENT_SPECIALITE];
}
else
    echo "aucune";
?></td>
    </tr>
    <tr>
        <td>Niveau : </td><td><?php dessinerTableauNote($hamster['niveau'],6) ; ?></td>
        <td><?php echo $hamster['niveau']; ?></td>
    </tr>        
    <tr>
        <td>Popularité : </td><td><?php dessinerTableauNote($hamster['popularite'],10) ; ?></td>
        <td><?php echo $hamster['popularite']; ?></td>
    </tr>        
    <tr>
        <td>Expérience : </td><td><?php dessinerTableauNote($hamster['experience'],10) ; ?></td>
        <td><?php echo $hamster['experience']; ?></td>
    </tr>
    <tr>
        <td>Danse : </td><td><?php dessinerTableauNote($hamster['danse'],10) ; ?></td>
        <td><?php echo round($hamster['danse'],1); ?></td>
    </tr>                    
    <tr>
        <td>Chant : </td><td><?php dessinerTableauNote($hamster['chant'],10) ; ?></td>
        <td><?php echo round($hamster['chant'],1); ?></td>
    </tr>
    <tr>
        <td>Bon copain : </td><td><?php dessinerTableauNote($hamster['sociabilite'],10) ; ?></td>
        <td><?php echo round($hamster['sociabilite'],1); ?></td>
    </tr>
    <tr>
        <td>Humour : </td><td><?php dessinerTableauNote($hamster['humour'],10) ; ?></td>
        <td><?php echo round($hamster['humour'],1); ?></td>
    </tr>
    <tr>
        <td>&nbsp;</td><td>&nbsp;</td>
    </tr>
    <?php 
        // si le hamster est marié, on affiche son compagnon
        if ($hamster['compagnon_id'] > 0) {
            
            echo "<tr><td>";
            
            $query2 = "SELECT nom,hamster_id,type FROM hamster WHERE hamster_id=".$hamster['compagnon_id']. " LIMIT 1";
            if ( !($result2 = $dbHT->sql_query($query2)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query2);
            }
            if ($dbHT->sql_numrows($result2)>0) {
                $compagnon_row=$dbHT->sql_fetchrow($result2);
                if ($hamster['sexe'] == 0) 
                    echo "Marié avec "  ; 
                else  
                    echo "Mariée avec "  ;
            }
            $dbHT->sql_freeresult($result2);
            
            echo "</td><td><a href=\"afficheHamster.php?hamster_id=".$compagnon_row['hamster_id']."\">".tronquerTxt($compagnon_row['nom'],15)."</td></tr>";
            echo "<tr><td>&nbsp;</td><td>&nbsp;</td></tr>    ";
        }
    ?>
    <tr>
        <td><strong>Nb total de points</strong> : </td><td><?php echo $hamster['note']; ?></td>
    </tr>
</table>

<br/>&nbsp;<br/>
<a href="#" onClick="javascript:window.close();">Fermer</a>
<br/>&nbsp;<br/>

</div>

<?php require "footer.php"; ?>