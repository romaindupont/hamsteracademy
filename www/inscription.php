<?php 
define('IN_HT', true);

//    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="refresh" content="3; url=index.php" /><title>Patientez svp ...</title></head><body>';
//    echo "<div align=center>Les inscriptions sont closes jusqu'à dimanche soir.</div>" ;
//    echo '</body></html>';

//    exit;

include "common.php";
include "lstHamsters.php";
include "lstAccessoires.php";
include "gestion.php";
include "gestionUser.php";
include "facebook.php";

error_reporting(E_ALL);

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo T_("Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email")." : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

// information sur l'inscription
$univers = UNIVERS_ELEVEUR;
$erreur = 0;
$erreurPrenomHamster = 0;
$erreurCaracteres = 0;
$erreurPseudo = 0;
$erreurPasswd = 0;
$erreurMail = 0;
$erreurCharte = 0;
$erreurIp = 0;
$message = '';
$erreurMsg = '';
$inscriptionOk = false;
$mode = "inscription";
$facebookTxt = "";
$facebook_id = -1;

// information sur le joueur
$id = -1;
$pseudo = "";
$passwd = "";
$email = "";
$age = 0;
$sexe = rand(0,1);
$pays = 83;
if ($lang == "en")
    $pays = 246 ; // états unis
$parrain_id = -1;
$parrain_pseudo = "";
$charte = 0;
$okPourEmail = 0;

if (isset($_POST['parrain']))
    $parrain_id = intval($_POST['parrain']);

if (isset($_POST['parrain_pseudo']))
    $parrain_pseudo = mysql_real_escape_string($_POST['parrain_pseudo']);
    
// on cherche le pseudo du parrain
if ($parrain_id > 0 && $parrain_pseudo == "") {
    $queryParrain = "SELECT pseudo FROM joueurs WHERE joueur_id = ".$parrain_id;
    if ( !($resultParrain = $dbHT->sql_query($queryParrain)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $queryParrain);
    }
    $nbParrains = $dbHT->sql_numrows($resultParrain) ;
                                
    if ($nbParrains == 1) {
        $rowParrain = $dbHT->sql_fetchrow($resultParrain);
        $parrain_pseudo = $rowParrain['pseudo'];
    }
    $dbHT->sql_freeresult($resultParrain);
}

if ($sessionFacebook) {
    
    // on récupère les informations du visiteur
    try {
        $compteFacebook = $facebook->api('/me');
        
        if (isset($_GET['facebookPerms'])) {
            $client_id    = CLE_PUBLIQUE;
            $display      = 'page';
            $scope         = 'email,user_birthday';
            $redirect_url = $base_site_url."/inscription.php";
            $oauth_url    = 'https://graph.facebook.com/oauth/authorize?client_id=' . $client_id . '&redirect_uri=' . $redirect_url . '&type=web_server&display=' . $display . '&scope=' . $scope;
            $text = "<script type=\"text/javascript\">\ntop.location.href = \"$oauth_url\";\n</script>";
            $facebookTxt .= $text;
        }

        if (isset($compteFacebook['email'] ) ){
            $email = $compteFacebook['email'];
            $okPourEmail = 1;
        }
        if (isset($compteFacebook['gender'] )){
            if ($compteFacebook['gender'] == "male")
                $sexe = 0;
            else
                $sexe = 1;
        }
        if (isset($compteFacebook['id'] )){
            $facebook_id = $compteFacebook['id'];
        }
    } catch (FacebookApiException $e) {
        error_log($e);
    }
}

// information sur l'hamster
$typeHamster = 0; 
$sexeHamster = rand(0,1);
$prenomHamster = "";
$prefHamster_1 = "coquet";
$prefHamster_2 = "fort";

if (isset($_POST['submitInscription'])) {
    require "verifHamsterInscription.php" ;
    require "verifPseudo.php" ;
    
    // on vérifie que l'ip est valide
    $query = "SELECT * FROM ip_bannies WHERE ip = '".$user_ip."' LIMIT 1" ;
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
    }
    $nbIp = $dbHT->sql_numrows($result) ;
    $dbHT->sql_freeresult($result);
    if ($nbIp == 1){
        $erreur++;
        $erreurIp = 1;
        redirectHT("index.php?err=5");
    }    

    if ($erreur == 0) {

        $etape_inscription  = 4;

        // on rajoute le nouveau joueur
        $joueur_id = ajouterJoueurDansBDD($pseudo, $passwd, $email, $sexe, $age, $lang, $pays, $etape_inscription) ;
            
        if ($joueur_id == -1) {
            echo "erreur inscription joueur " ;
        }
        else {
            
            if ($facebook_id != -1) {
                $query = "INSERT INTO facebook VALUES ( ".$joueur_id.",".$facebook_id." ) ";
                if ( !($dbHT->sql_query($query)) ){
                    //echo 'Error adding facebook : '.$query;
                }
            }
            
            // on rajoute la cage et quelques objets
            $nomCage = T_("Cage de ").$prenomHamster;
            $cage_id = ajouterCageDansBDD($joueur_id, $typeCageInscription, $etagesInscription, $fondInscription, $propreteInscription, $toitInscription, $colonnesInscription, $profondeursInscription, $nomCage);

            if ($cage_id == -1) {
                echo "Erreur ajout de cage <br>" ;
            }
            else {
                
                // on ajoute un objet (biberon)
                $acc_id = ajouterAccessoireDansBDD(1,$joueur_id, $cage_id,230,0,0,1);
                $acc_id2 = ajouterAccessoireDansBDD(ACC_ECUELLE,$joueur_id, $cage_id,157,0,0,1);
                
                // on ajoute l'hamster
                $hamster_id = ajouterHamsterDansBDD($prenomHamster, $sexeHamster, $prefHamster_1, $prefHamster_2, $typeHamster, $cage_id, $joueur_id, 604800); // 1 semaine d'age

                if ($hamster_id == -1) {
                    echo "Erreur ajout d'hamster<br>" ;
                }
                else {                        
                    // si on arrive ici, c'est que l'inscription SQL s'est bien passée
                    
                    $inscriptionOk = true;
                    
                    // envoi d'un mail au joueur s'il a donne une adresse mail
                    if ($email != "") {
                        $TO = $email;
                        $subject = T_("Bienvenue dans Hamster Academy !") ;
                        $message = T_("Félicitations ").$pseudo." ! \n\n".T_("Tu viens de t'inscrire à Hamster Academy !")."\n\n";
                        $message .= T_("Rappel : ton mot de passe pour jouer est")." \"".$passwd."\" \n\n";
                        $message .= T_("Astuce : pour participer aux concours, recevoir des emails d'autres joueurs, des cadeaux, etc., tu dois valider ton adresse email. Pour cela, clique sur le lien ci-dessous :")." \n\n";
                        $message .= $base_site_url."/activer.php?joueur_id=".$joueur_id."&cle=".calculCodeActivation($joueur_id, $email);
                        $message .= "\n\n".T_("A bientôt dans le jeu !")."\n\n";
                        $message .= "--\n".T_("L'équipe de Hamster Academy")."\n";
                        envoyerMail($TO,$subject,$message,"inscription");
                    }
                    
                    // on regarde s'il s'agit d'un parrainage
                    if (isset($parrain_pseudo) && $parrain_pseudo != "") {
                        
                        // on vérifie qu'il y a bien parrainage toujours valide
                        $query = "SELECT joueur_id, nb_parrainages FROM joueurs WHERE pseudo = \"".$parrain_pseudo."\" LIMIT 1";    
                        if ( !($result = $dbHT->sql_query($query)) ){
                            message_die(GENERAL_ERROR, 'Error in obtaining', '', __LINE__, __FILE__, $query);
                        }
                        $nbParrain = $dbHT->sql_numrows($result) ;

                        if ($nbParrain > 0) {
                            
                            $rowParrain = $dbHT->sql_fetchrow($result);
                            $dbHT->sql_freeresult($result);
                            
                            $query = "UPDATE joueurs 
                            SET nb_parrainages = ".($rowParrain['nb_parrainages']+1)." 
                            WHERE joueur_id=".$rowParrain['joueur_id'];    
                            if ( !($dbHT->sql_query($query)) ){
                                message_die(GENERAL_ERROR, 'Error updating parrain', '', __LINE__, __FILE__, $query);
                            }
                        }
                    }
                }
            }
        }
    }
}

$pagetitle = T_("Hamster Academy - Inscription") ;
$description = T_("Inscription à Hamster Academy : saisis vite un pseudo et un mot de passe, choisis un hamster et go !");
$keywords = T_("Hamster, Academy, Inscription, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer");

if ($inscriptionOk) {
    $pagetitle .= T_(" finie") ;
}

$liste_styles = "style.css,styleEleveur.css,cornflex-common.css";
$liste_scripts = "gestionHamster.js,jquery.js,jquery-ui.min.js,jquery.cornflex.js,yellowbox.js";

$topbar_texte = "<a href=\"http://".$serveur_url_fr."inscription.php?lang=fr\" title=\"Inscription à Hamster Academy en Francais\"><img src=\"images/lang/francais.gif\" alt=\"Francais\" width=\"20\" height=\"14\" border=\"1\" /></a> <a href=\"http://".$serveur_url_en."inscription.php?lang=en\" title=\"Register to the game in English !\"><img src=\"images/lang/english.gif\" alt=\"English\" width=\"20\" height=\"14\" border=\"1\" /></a>";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('keywords', $keywords);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('liste_scripts', $liste_scripts);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('topbar_texte', $topbar_texte);
if ($inscriptionOk)
    $smarty->assign('pageWidth', 600);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');

// 2 cas, inscription en cours, ou inscription validée
if ($inscriptionOk) {
    $smarty->assign('pseudo', $pseudo);
    $smarty->assign('passwd', $passwd);
    $smarty->assign('lang', $lang);
    $smarty->assign('nbPiecesInscription', $nbPiecesInscription);
    $smarty->assign('IMG_PIECE', IMG_PIECE);
    $smarty->assign('alimentsInscription', $alimentsInscription);
    
    $smarty->display('inscription_ok.tpl');
    
    require "footer.php";
    exit();
}

// liste des hamsters achetables
$lstHamsters = array();
for($hamsterIndex=0; $hamsterIndex < 10; $hamsterIndex++) {
    
    if ($hamsterIndex < 7) 
        $accessible = true;
    else
        $accessible = false;
        
    if ($hamsterIndex == 7)
        continue; // on enlève le hamster grunge
    
    $hamster = array(
        "accessible" => $accessible,
        "image" => $infosHamsters[$hamsterIndex][1],
        "nom" => $infosHamsters[$hamsterIndex][0],
        "yeux" => $infosHamsters[$hamsterIndex][2],
        "pelage" => $infosHamsters[$hamsterIndex][3],
        "description" => $infosHamsters[$hamsterIndex][4],
        "already_checked" => ( $typeHamster == $hamsterIndex ),
        "type" => $hamsterIndex
    );
    array_push($lstHamsters,$hamster);
}

$smarty->assign('lstHamsters', $lstHamsters);

$msgErreur = "";
if ($erreurCaracteres > 0) {
    $msgErreur = "<font color=\"#AA0000\">".T_("Attention, choisis <strong>deux</strong> caractéristiques maximum pour l'hamster (ex : puissant et dragueur)")."</font><br/>&nbsp;<br/>\n";
}
$smarty->assign('msgErreur', $msgErreur);
$smarty->assign('prefHamster_1', $prefHamster_1);
$smarty->assign('prefHamster_2', $prefHamster_2);
$smarty->assign('prenomHamster', $prenomHamster);
$smarty->assign('sexeHamster', $sexeHamster);
$smarty->assign('parrain_pseudo', $parrain_pseudo);
$smarty->assign('parrain_id', $parrain_id);
$smarty->assign('pseudo', $pseudo);
$smarty->assign('email', $email);
$smarty->assign('passwd', $passwd);
$smarty->assign('charte', $charte);
$smarty->assign('okPourEmail', $okPourEmail);
$smarty->assign('nbErreurs', $erreur);
$smarty->assign('cle_application_publique', CLE_PUBLIQUE);

if ($erreurPseudo > 0) {
    if ($erreurPseudo == 1)
        $erreurPseudoMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Choisis un pseudo")." !</span>";
    else if ($erreurPseudo == 2) 
        $erreurPseudoMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Ce pseudo est déjà pris. Choisis-en un autre").".</span>";
    else if ($erreurPseudo == 3) 
        $erreurPseudoMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Ton pseudo doit commencer par une lettre").".</span>";
    else if ($erreurPseudo == 4) 
        $erreurPseudoMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Ton pseudo")." :<ul>
                <li>".T_("ne doit pas contenir d'accents, ni de guillements, ni d'espace, ni de symboles particuliers")."</li>
                <li>".T_("doit contenir uniquement des caractères de l'alphabet, des chiffres et 2 symboles autorisés")." : _ . </li>
                <li>".T_("doit contenir au moins 3 caracteres")."</li>
            </ul>
            </span>";     
    else
        $erreurPseudoMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Problème de pseudo, re-essaye").".</span>";
    $smarty->assign('erreurPseudoMsg', $erreurPseudoMsg);
}

if ($erreurPasswd > 0) {
  if ($erreurPasswd == 1) 
      $erreurPasswdMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Choisis un mot de passe !")."</span>";
  else if ($erreurPasswd == 2)
      $erreurPasswdMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Le mot de passe doit faire au moins 6 caractères et ne contenir que des chiffres et des lettres. Pas de ponctuation, de symbole, etc.").".</span>";
  else if ($erreurPasswd == 3)
      $erreurPasswdMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Tu n'as pas saisi 2 fois le même mot de passe.").".</span>";      
  else
      $erreurPasswdMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Problème de mot de passe, re-essaye").".</span>";
  $smarty->assign('erreurPasswdMsg', $erreurPasswdMsg);
}
if ($erreurMail > 0){
    if ($erreurMail == 1) 
      $erreurMailMsg = "<span class=\"optionsValeurChampsErreur\">".T_("L'adresse mail")." => ".$email." <= ".T_("est invalide").".<br/>".T_("Rappel : elle n'est pas obligatoire, tu peux ne rien mettre")."<br/>&nbsp;<br/></span>";
    else if ($erreurMail == 2)
      $erreurMailMsg = "<span class=\"optionsValeurChampsErreur\">".T_("L'adresse mail")." ".$email." ".T_("est déjà choisie par un autre joueur. Choisis-en une autre")." !</span>";
    else if ($erreurMail == 3)
      $erreurMailMsg = "<span class=\"optionsValeurChampsErreur\">".T_("Tu n'as pas saisi 2 fois la même adresse mail.")." !</span>";      
    $smarty->assign('erreurMailMsg', $erreurMailMsg);
}
$erreurHamsterMsg = "";
if ($erreurCaracteres > 0 || $erreurPrenomHamster > 0) {
    if ($erreurCaracteres > 0)
        $erreurHamsterMsg .= "<span class=\"optionsValeurChampsErreur\">".T_("Tu dois choisir deux caractéristiques maximum pour ton hamster")." !</span><br/>";      
    if ($erreurPrenomHamster > 0)
        $erreurHamsterMsg .= "<span class=\"optionsValeurChampsErreur\">".T_("Choisis un prénom pour ton hamster")." !</span>";      
    $smarty->assign('erreurHamsterMsg', $erreurHamsterMsg);
}
$smarty->assign('lang', $lang);
$smarty->assign('age', $age);
$smarty->assign('sexe', $sexe);

if ($erreurCharte == 1) {
    $erreurCharteMsg = "<span class=\"optionsValeurChampsErreur\" style=\"clear:both;\">".T_("Tu dois accepter la charte").".</span>";
    $smarty->assign('erreurCharteMsg', $erreurCharteMsg);
}

if (! $sessionFacebook){
    $facebookTxt .= T_("Si tu as Facebook, tu peux t'inscrire plus rapidement : clique d'abord sur le lien ci-dessous :")."<br/>";
    $facebookTxt .= "<fb:login-button perms=\"email,user_birthday\"></fb:login-button>"; 
}
else
    $facebookTxt .= T_("Tu t'inscris avec ton compte Facebook :")." <a href=\"".$compteFacebook['link']."\" target=\"_blank\" title=\"".T_("Voir mon profil Facebook")."\"> ".$compteFacebook['name']."</a>.";
$smarty->assign('facebookTxt', $facebookTxt);

$smarty->display('inscription.tpl');

require "footer.php"
?> 
