<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

$remplacement_copeaux_ok = false;
	
$quantiteActuelle = $userdata['copeaux'] ;

if ($quantiteActuelle > 0) {
	
	$nouvelleQuantite = $quantiteActuelle - 1; // 1 litre de consommation
	
	// mise à jour du paquet de copeaux
	$userdata['copeaux'] = $nouvelleQuantite;
	$query = "UPDATE joueurs SET copeaux = ".$nouvelleQuantite." WHERE joueur_id = ".$userdata['joueur_id'] ;
	if ( !($result = $dbHT->sql_query($query)) ){
			message_die(GENERAL_ERROR, 'Error in modifying joueur (remplacerCopeaux)', '', __LINE__, __FILE__, $query);
	}
	
	// la propreté de la cage passe à 100% 
	// on cherche la cage correspondante
	$cage = cageCorrespondante($cage_id);
	if ($cage == -1) {
		message_die(GENERAL_ERROR, 'Remplacer copeaux : pas de cage associee', '', __LINE__, __FILE__, $query);
	}
	
	$lst_cages[$cage]['proprete'] = 10;
	
	$query = "UPDATE cage SET proprete = 10 WHERE cage_id = ".$cage_id;
	if ( !($result = $dbHT->sql_query($query)) ){
		message_die(GENERAL_ERROR, 'Error in changing cage safety (remplacerCopeaux)', '', __LINE__, __FILE__, $query);
	}
	$remplacement_copeaux_ok = true;
	if ($userdata['niveau']==NIVEAU_NOURRIR_COPEAUX) {
		updateNiveauJoueur(1,true) ;
	}
}
else {
	$remplacement_copeaux_ok = false;
}

?>