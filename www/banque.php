<?php 
define('IN_HT', true);
include "common.php";
include "gestion.php";
include "lstMessages.php";
include "lstNiveaux.php";

$userdata = session_pagestart($user_ip);
    
if( ! $userdata['session_logged_in'] ) { 
    echo "Erreur de connection";
    redirectHT("index.php?err=1",3);
}

$univers = UNIVERS_ELEVEUR;
$systeme_bancaire = ALLOPASS;

if (isset($_GET['univers']))
    $univers = intval($_GET['univers']);

$pagePrecUrl = "jeu.php"; // par défaut

$pagetitle = "Hamster Academy - ".T_("Banque") ;
$description = T_("Tu manques d'argent ou tu veux gagner du temps ? Tu peux avoir ici plein de pièces d'un coup. C'est payant mais pas très cher, ça vaut le coup !");

$liste_styles = "style.css,styleEleveur.css";
     
// paramètres d'entete
$smarty->assign('pagetitle', $pagetitle);
$smarty->assign('description', $description);
$smarty->assign('univers', $univers);
$smarty->assign('liste_styles', $liste_styles);
$smarty->assign('base_site_url', $base_site_url);
$smarty->assign('lang', $lang);
$smarty->display('header.tpl');
?>

<div align=center>
    
    <h1>Hamster Academy - <?php echo T_("Banque"); ?></h1>
    
    <?php 
        require "formulaireBanque.php" ;
    ?>
    
<br/>&nbsp;<br/>
<a href="<?php echo $pagePrecUrl ; ?>"><?php echo T_("Revenir au jeu");?></a>

<br/>&nbsp;<br/>

</div>

<?php require "footer.php"; ?>
