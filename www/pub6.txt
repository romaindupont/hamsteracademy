Hamster Academy : jeu gratuit d'élevage virtuel de Hamsters ! Missions, amis, forum, tchat, concours, cage à décorer, élève ton hamster dans un univers super sympa !

Hamster Academy is a very complete and funny online game where you have breed and take care of your hamster. You can fit out and decorate its cage, buy items and toys, participate to competitions and talk in the forum.