<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

$erreur = 0;
$erreurMsg = "";
$mode="jeu";
$action="nouveau";
	
$typeHamster = 0; // informations relatives stockees dans infosHamsters
$sexeHamster = 0;
$prenomHamster = "Prenom";
$prefHamster_1 = "coquet";
$prefHamster_2 = "fort";
$erreurPrenomHamster = 0;
$erreurCaracteres = 0;

if (isset($_POST['mode'])) 
	$mode=$_POST['mode'];
if (isset($_POST['action'])) 
	$action=$_POST['action'];
if (isset($_POST['typeHamster'])) 
	$typeHamster=$_POST['typeHamster'];	
if (isset($_POST['sexeHamster'])) 
	$sexeHamster=$_POST['sexeHamster'];	
if (isset($_POST['prenomHamster'])) 
	$prenomHamster=$_POST['prenomHamster'];
if (isset($_POST['prefHamster_1'])) 
	$prefHamster_1=$_POST['prefHamster_1'];
if (isset($_POST['prefHamster_2'])) 
	$prefHamster_2=$_POST['prefHamster_2'];
if (isset($_POST['caractHamster'])){
	$options = $_POST['caractHamster'] ;
if (isset($_POST['cageHamster']))
	$cage_id = $_POST['cageHamster'] ;
	
	$nbOpt = 0;
	foreach ($options as $choix) {
		$nbOpt = $nbOpt + 1;
		if ($nbOpt == 1)
			$prefHamster_1 = $choix;
		else if ($nbOpt == 2)
			$prefHamster_2 = $choix;
		else {
			$erreur = $erreur + 1;
			$erreurMsg = $erreurMsg.$erreur.") tu dois choisir deux caractéristiques maximum<br/>\n";
		}
	}
	if ($nbOpt != 2){
		$erreur = $erreur + 1;
		$erreurMsg = $erreurMsg.$erreur.") choisir deux caractéristiques pour l'hamster (ex : puissant et dragueur) <br/>\n";
	}
}
	
if ($action == "valider") {

	// verif pseudo
	if ($prenomHamster == "" || strlen($prenomHamster) < 3) {
		$erreur = $erreur + 1;
		$erreurMsg = $erreurMsg.$erreur.") choisir un prénom pour l'hamster <br/>\n";
	}

	// si erreur, pas de redirect 
	if ($erreur == 0) {
				
		ajouterHamster($prenomHamster,$sexeHamster, $prefHamster_1, $prefHamster_2,$typeHamster,  $cage_id,$userdata['joueur_id']);	
		redirectHT("jeu.php?mode=m_hamster&msg=4&hamster_index=".$nbHamsters,2);
	}
}	

if ($erreur == 0) 
	echo "<p>Choisis un nouvel hamster</p>\n";
else {
	echo "<p>Attention, il faut : <br/>\n".$erreurMsg."</p>\n";
}


require "formulaireNouvelHamster.php";

?>