<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

// meilleurs hamsters
$query = "SELECT h.nom, h.note, u.pseudo, u.joueur_id FROM hamster h, joueurs u WHERE u.joueur_id = h.joueur_id ORDER BY note DESC LIMIT 50 ";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$i=1;
$lstHamstersNotes = array();
while($row=$dbHT->sql_fetchrow($result)) {
    // on affiche la note du joueur
    $txt = $i.") ".$row['nom']." (".$row['note']." points) (<a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"Voir son profil\">".$row['pseudo']."</a>)";
    array_push($lstHamstersNotes,$txt);
    $i++;
}
$dbHT->sql_freeresult($result);

// meilleurs groupes
$query = "SELECT nom, note, groupe_id FROM groupes ORDER BY note DESC LIMIT 20 ";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$i=1;
$lstGroupesNotes = array();
while($row=$dbHT->sql_fetchrow($result)) {
    $txt = $i.") ".returnLienProfilGroupe($row['groupe_id'], $row['nom'])." (".$row['note']." points)";
    $i++;
    array_push($lstGroupesNotes,$txt);
}
$dbHT->sql_freeresult($result);

// hamsters les plus expérimentés
$query = "SELECT h.nom, h.experience, u.pseudo, u.joueur_id FROM hamster h, joueurs u WHERE u.joueur_id = h.joueur_id ORDER BY experience DESC LIMIT 50 ";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$i=1;
$lstHamstersExp = array();
while($row=$dbHT->sql_fetchrow($result)) {
    $txt = $i.") ".$row['nom']." (expérience : ".$row['experience'].") (<a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"Voir son profil\">".$row['pseudo']."</a>)";
    $i++;
    array_push($lstHamstersExp,$txt);
}
$dbHT->sql_freeresult($result);

// hamsters les plus populairess
$query = "SELECT h.nom, h.popularite, u.pseudo, u.joueur_id FROM hamster h, joueurs u WHERE u.joueur_id = h.joueur_id  ORDER BY popularite DESC LIMIT 50 ";
if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
}
$i=1;
$lstHamstersPop = array();
while($row=$dbHT->sql_fetchrow($result)) {
    $txt = $i.") ".$row['nom']." (popularité : ".$row['popularite'].") (<a href=\"#\" onClick=\"javascript: pop('profil.php?joueur_id=".$row['joueur_id']."',null,400,600); return false;\" title=\"Voir son profil\">".$row['pseudo']."</a>)";
    $i++;
    array_push($lstHamstersPop,$txt);
}
$dbHT->sql_freeresult($result);
        
$smarty->assign('lstHamstersNotes', $lstHamstersNotes);
$smarty->assign('lstGroupesNotes', $lstGroupesNotes);
$smarty->assign('lstHamstersExp', $lstHamstersExp);
$smarty->assign('lstHamstersPop', $lstHamstersPop);
$smarty->display('classements.tpl');
?>