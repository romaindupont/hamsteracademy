<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require_once "gestionUser.php";

//$site_id = "xxx";
$site_id = "xxx";
//$doc_id = "xxx"; // module Banque de Allopass
//$doc_id = "xxx"; // module Banque_FR de Allopass
$doc_id = "xxx"; // module Banque_FR de Allopass
//$adv_id = "xxx";
$adv_id = "xxx";
$erreurPaiement = 0;

$message_erreur = "<font color=\"#AA0000\">".T_("Le code saisi est invalide.</font> Vérifie-le et réessaye. Sinon, contacte ")."<a href=\"mailto:".$email_contact."\">Hamster Academy</a>.";

if (isset($_GET['paiement']) || isset($_GET['RECALL'])) {
    
    // le joueur a payé, on vérifie son code
    $RECALL = mysql_real_escape_string($_GET['RECALL']);

    if( trim($RECALL) == "" )
    {
        // La variable RECALL est vide, renvoi de l'internaute
        // vers une page d'erreur
        $erreurPaiement = 2; 
    }
    else {
        // $RECALL contient le code d'accès
        $RECALL = urlencode( $RECALL );

        // $AUTH doit contenir l'identifiant de VOTRE document
        $AUTH = "";
        if (isset($_GET['paiement']))
          $AUTH = urlencode( $site_id."/".$doc_id."/".$adv_id );
        /**
        * envoi de la requête vers le serveur AlloPAss
        * dans la variable $r[0] on aura la réponse du serveur
        * dans la variable $r[1] on aura le code du pays d'appel de l'internaute
        * (FR,BE,UK,DE,CH,CA,LU,IT,ES,AT,...)
        * Dans le cas du multicode, on aura également $r[2],$r[3] etc...
        * contenant à chaque fois le résultat et le code pays.
        */

        $r = @file( "http://www.allopass.com/check/vf.php4?CODE=$RECALL&AUTH=$AUTH" );

        // on teste la réponse du serveur

        if( substr( $r[0],0,2 ) != "OK" ) 
        {
            // Le serveur a répondu ERR ou NOK : l'accès est donc refusé
            $erreurPaiement = 3 ;
        }
    }
    
    if ($erreurPaiement == 0) {
        ajouterPass($userdata['joueur_id']);
        ajouterStats($userdata['joueur_id'],"paiement_tel",1,$dateActuelle);
        $msg .= "<div style=\"font-weight:bold;\">Le paiement a été validé : tu as reçu 1 Pass ! Félicitations !</div>";
    }
    else {
        $msg .= $message_erreur;
    }
}

if ($erreurAction == 0) {
    
    // action succès
    
    if ($action == "echangerPassPieces") {
        
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/pieces_bonus.gif\" alt=\"\" height=\"100\" /><br/>&nbsp;<br/>";
        $msg .= T_("Félicitations, tu viens de récupérer ").$nbPiecesParAchatBanque." " .IMG_PIECE." !<br/>&nbsp;<br/>".T_("Tu possèdes maintenant ").$userdata['nb_pieces']." ".IMG_PIECE." !" ;
        $msg .= "</div>";
    }
    else if ($action == "echangerPassGrossesse") {
        $msg .= "<div align=\"center\"><img src=\"images/reproduction.gif\" alt=\"\" /><br/>&nbsp;<br/>";
        $msg .= T_("Grâce à l'achat de la couveuse ultra-rapide, la grossesse est terminée. Les bébés sont prêts à naître !")."<br/>&nbsp;<br/>";
        $msg .= "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".(intval($_GET['hamsterConcerne']))."\">".T_("Voir ton hamster et ses petits !")."</a></div>" ;
    }
    else if ($action == "echangerPassCabane") {
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/maison2.gif\" alt=\"\" /><br/>&nbsp;<br/>";
        $msg .= T_("Grâce à ton achat, la construction de la cabane est terminée").".<br/>&nbsp;<br/>";
        $msg .= "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".(intval($_GET['hamsterConcerne']))."\">".T_("Voir le hamster et sa nouvelle cabane !")."</a>" ;
        $msg .= "</div>";
    }
    else if ($action == "echangerPassNid") {
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/nid.gif\" alt=\"\" /><br/>&nbsp;<br/>";
        $msg .= T_("Grâce à ton achat, la construction du nid est terminée").".<br/>&nbsp;<br/>";
        $msg .= "<a href=\"jeu.php?mode=m_hamster&amp;hamster_id=".(intval($_GET['hamsterConcerne']))."\">".T_("Voir le hamster et son nouveau nid !")."</a>" ;
        $msg .= "</div>";
    }
    else if ($action == "echangerPassNiveauSuperieur") {
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/hamster_inscription.gif\" alt=\"\" />".T_("Félicitations, tu viens de passer au niveau suivant !")."<br/>&nbsp;<br/>" ;
        $msg .= "</div>";
    }
    else if ($action == "echangerPassVIP") {
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/hamster_inscription.gif\" alt=\"VIP\" />".str_replace("#1",$mois_txt[$month],T_("Félicitations, tu es maintenant VIP ! Tu vas pouvoir profiter pour tout le mois de #1 de plein d'avantages !"))."<br/>&nbsp;<br/>" ;
        $msg .= "</div>";
    }
    else if ($action == "offrirPass") {
        $msg .= "<div align=\"center\">";
        $msg .= "<img src=\"images/cadeauNoel.gif\" alt=\"\" /> ".T_("Ton ami a bien reçu son cadeau. Nous lui avons envoyé un message pour le prévenir qu'il vient de ta part !")."<br/>&nbsp;<br/>" ;
        $msg .= "</div>";
    }
}
else if ($erreurAction == 1) {
    $msg .= T_("Désolé, tu ne possèdes pas de pass pour faire cette action...");
}
else if ($erreurAction == 2 && $action == "offrirPass" ) {
    $msg .= str_replace("#1",$pseudo,T_("<div>Désolé, il n'y a pas de joueur avec le pseudo #1. Vérifie l'orthographe...</div>"));
}

$smarty->assign('nbPass',$userdata['nb_pass']) ;
$smarty->assign('univers',$univers) ;
$smarty->assign('msg',$msg) ;
$smarty->assign('joueur_id',$userdata['joueur_id']) ;
$smarty->assign('site_id',$site_id) ;
$smarty->assign('doc_id',$doc_id) ;
$smarty->assign('adv_id',$adv_id) ;
$smarty->assign('erreurPaiement',$erreurPaiement) ;

$listeHamstersGrossesse = array();
$listeHamstersCabane = array();
$listeHamstersNid = array();

for($ha = 0;$ha <$nbHamsters;$ha++) {
    
    if ($lst_hamsters[$ha]['sexe'] > 1)
        array_push($listeHamstersGrossesse,$lst_hamsters[$ha]);
    if ($lst_hamsters[$ha]['construction_cabane'] > 2)
        array_push($listeHamstersCabane,$lst_hamsters[$ha]);
    if ($lst_hamsters[$ha]['construction_nid'] > 2)
        array_push($listeHamstersNid,$lst_hamsters[$ha]);
}

$smarty->assign('vip',$userdata['vip']) ;
$smarty->assign('listeHamstersGrossesse',$listeHamstersGrossesse) ;
$smarty->assign('listeHamstersCabane',$listeHamstersCabane) ;
$smarty->assign('listeHamstersNid',$listeHamstersNid) ;
if ($userdata['niveau'] < $nbNiveaux-1)
    $smarty->assign('nouveauNiveau',$userdata['niveau'] + 1) ;
else
    $smarty->assign('nouveauNiveau',-1) ;

$smarty->display('pass.tpl');  

?>