<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

// colonnes : 0) race <-> 1)image <-> 2) Couleurs yeux <-> 3) Couleurs pelage , 4) description
$infosHamsters = array(
0 => array('Hamster angora','hamster_angora.jpg','noirs','blanc et beige','Hamster futé, il adore se cacher ! Plus vous lui mettrez de cachettes dans sa cage, plus il sera heureux !'), 
1 => array('Hamster à poil ras','hamster_poilras.jpg','noirs','marron et blanc','Vivace et alerte, voilà un grand sportif ! Roue et haltères de rigueur&nbsp;!'),
2 => array('Hamster russe','hamster_russe.jpg','noirs','blanc','Grâce à sa petite taille, il se faufile partout. Il adore les cachettes et les tunnels.'),
3 => array('Hamster tout câlin','hamster14.jpg','noirs','gris clair','Cet hamster adore les couettes et les coins douillets ! Il faut lui faire plein de câlins !'),
4 => array('Hamster coriace','hamster16.jpg','noirs','Blanc et crème','Ca, pour un coriace, c\'est un coriace ! Voilà un hamster qui a du caractère ! On ne risque pas de s\'ennuyer avec lui ! Donne-lui de quoi faire du sport et jouer et il sera ravi&nbsp;!'),
5 => array('Hamster coquin','hamster17.jpg','noirs','Crème, ventre blanc','Il n\'y pas plus doux que cet hamster. Toujours gentil, toujours calme, même quand il fait de la roue, on ne l\'entend pas ! Et ça, tous les éleveurs en rêvent !'),
6 => array('Hamster mignon','hamster_mignon.jpg','noirs','Marron crème','Toute l\'Hamster Academy craque devant cet hamster. Il est vraiment trop chou !! Doux, câlin, mignon, gentil, soigné, il a tout pour lui ! Une valeur sûre !'),
7 => array('Hamster grunge','HamsterGrunge.jpg','lunettes noires','bronzage permanent','Plus rebelle, tu meurs ! Il a le style, il a la pêche, il fait fureur ! Mais attention, il n\'est pas du genre facile à vivre, il n\'aime pas être contrarié. Mais il sait être super attachant et adore faire plaisir à son éleveur&nbsp;!'),
8 => array('Hamster gourmand','hamster_gourmand.jpg','noirs','crème','Il est bien connu que les hamsters sont gourmands, mais celui-là, plus que les autres&nbsp;! Il est adorable, très malin (surtout pour trouver des graines en forêt). Il adore les friandises alors il ne faut pas hésiter à lui en donner !'),
9 => array('Hamster chouchou','hamster_choupette.jpg','noirs','blanc-gris','Très câlin, très coquet, adorable, il est génial, c\'est le plus chouchou des chouchoux ! Il mérite la plus belle cage chez vous !'),
10 => array('Hamster touffou','hamster_tarzan.jpg','noirs','crème','Renversant, un vrai Tarzan ! Il est tout fou, la tête dans les arbres ! Mettez la cage à l\'envers, il va adorer !! Et n\'oubliez pas la balançoire et les sorties en forêt!'),
11 => array('Hamster chantilly','hamster_chantilly.jpg','noirs','blanc-gris','Une vraie crème ! Plus doux qu\'une plume, c\'est un hamster curieux, docile et très câlin ! '),
12 => array('Hamster craintif','chomik18.jpg','noirs','crème','C\'est un hamster très craintif, il a peur du moindre bruit et des nouvelles personnes. Protégez-le bien, mettez sa cage dans un endroit calme et il sera heureux ! N\'est-t-il pas trop choux ?')
);

?>