<?php 

/**
 * creates an array based on a query, this function is a very common pattern
 * used here
 *
 * @param string $query
 * @param string $fieldname
 * @return array
 */
function array_from_query($query, $fieldname)
{
  $array = array();
  
  $result = pwg_query($query);
  while ($row = mysql_fetch_array($result))
  {
    array_push($array, $row[$fieldname]);
  }

  return $array;
}

// my_error returns (or send to standard output) the message concerning the
// error occured for the last mysql query.
function my_error($header)
{
  $error = '<pre>';
  $error.= $header;
  $error.= '[mysql error '.mysql_errno().'] ';
  $error.= mysql_error();
  $error.= '</pre>';
  die ($error);
}

function pwg_query($query)
{
  $start = get_moment();
  $result = mysql_query($query) or my_error($query."\n");
  $time = get_moment() - $start;

  return $result;
}


?>