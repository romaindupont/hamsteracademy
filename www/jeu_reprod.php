<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

echo '<h2 align="center">'.T_("Avoir des bébés").'</h2>
    <div align="center">
    <img src="images/reproduction.gif" alt="cigogne, Lampron 06" title="Dessin de Lampron">
    <br/>';

if ($msg != "" ) {
    echo $msg ;
}

// 1) grossesse est finie, on affiche les bébés et la possibilité de choisir leurs prénoms
if (isset($_GET['voirbebes'])){
    
    if ($hamsterIndex != -1) {
        $hamRow = $lst_hamsters[$hamsterIndex] ;
        
        // on vérifie (au cas où il y a triche) que la femelle a bien fini sa grossesse !
        if ( $hamRow['sexe'] > 1 && ($hamRow['sexe']+$dureeGrossesse) < $dateActuelle) {

            // combien de nouveaux hamsters ?
            srand($hamRow['hamster_id']);
            $nbNouveauxHamsters = rand(2,3) ;    
            
            // le joueur est-il en mode choix du prénom ? Si oui, on valide leurs prénoms et on crée les bébés
            if (isset($_GET['choisirPrenomsBebes'])){
                
                // on retrouve le mâle
                $maleRow = array();
                $query = "SELECT type, hamster_id FROM hamster WHERE hamster_id=".$hamRow['fecondateur_id']." LIMIT 1";
                if ( !($result = $dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
                }
                $nbHamstersQuery = $dbHT->sql_numrows($result) ;
                
                // si le mâle a disparu (cas rarissime), on en génère un faux !
                if ($nbHamstersQuery == 0){
                    $maleRow = array(
                    'type' => 0,
                    'hamster_id' => -1
                    ) ;
                }
                else {
                    $maleRow=$dbHT->sql_fetchrow($result);
                }
                $dbHT->sql_freeresult($result);

                // on remet à zéro le sexe de la mère
                $hamRow['sexe'] = 1;
                $hamRow['fecondateur_id'] = -1;
                $query = "UPDATE hamster 
                    SET sexe = 1,
                    fecondateur_id = -1
                    WHERE hamster_id=".$hamster_id;
                if ( !($dbHT->sql_query($query)) ){
                    message_die(GENERAL_ERROR, 'Error updating hamster', '', __LINE__, __FILE__, $query);
                }
                
                // on crée les bébés
                $annonceNaissance = mysql_real_escape_string($hamRow['nom'])." vient de mettre au monde ".$nbNouveauxHamsters." bébés";
                for($hamster=0;$hamster<$nbNouveauxHamsters;$hamster++) {
                    $choixType = rand(0,1);
                    $typeBebe=$caract1Bebe=$caract2Bebe=0;
                    if ($choixType == 0) // si 0, on prend les caractéristiques de la mère
                        $typeBebe = $hamRow['type'];
                    else
                        $typeBebe = $maleRow['type'];
                        
                    $caract1Bebe = "fort";
                    $caract2Bebe = "sociable";
                    
    //                $choixType = rand(0,1);
    //                if ($choixType == 0) // si 0, on prend les caractéristiques de la mère
    //                    $caract1Bebe = $hamRow['type'];
    //                else
    //                    $caract1Bebe = $maleRow['type'];
    //                    
    //                $choixType = rand(0,1);
    //                if ($choixType == 0) // si 0, on prend les caractéristiques de la mère
    //                    $caract2Bebe = $hamRow['type'];
    //                else
    //                    $caract2Bebe = $maleRow['type'];
                        
                    $prenom = "";
                    if ($hamster == 0){
                        $prenom = mysql_real_escape_string($_GET['prenom1']);
                    }
                    else if ($hamster == 1){
                        $prenom = mysql_real_escape_string($_GET['prenom2']);
                    }
                    else if ($hamster == 2){
                        $prenom = mysql_real_escape_string($_GET['prenom3']);
                    }
                    
//                    $annonceNaissance .= mysql_real_escape_string($prenom);
//                    if ($hamster == 0 && $nbNouveauxHamsters == 2)
//                        $annonceNaissance .= " et ";
//                    else if ($hamster == 0 && $nbNouveauxHamsters == 3)
//                        $annonceNaissance .= ", ";
//                    else if ($hamster == 1 && $nbNouveauxHamsters == 3)
//                        $annonceNaissance .= " et ";
                    $age = 0;
                    ajouterHamsterDansBDD($prenom, rand(0,1) , $caract1Bebe, $caract2Bebe, $typeBebe, $hamRow['cage_id'], $userdata['joueur_id'], $age, $maleRow['hamster_id'], $hamRow['hamster_id'],$userdata['niveau']);
                }
                
                // on met une annonce dans l'historique
                $query5 = "INSERT INTO historique VALUES(".HISTORIQUE_NAISSANCE.",".$dateActuelle.",'".$annonceNaissance."')";
                $dbHT->sql_query($query5);
                
                echo "<div align=\"center\">".T_("C'est bon, leurs prénoms sont enregistrés")." ! <br/>&nbsp;<br/><a href=\"jeu.php?mode=m_hamster&hamster_id=".$hamRow['hamster_id']."\">".T_("Voir maintenant les bébés ! <strong>Attention</strong>, ils viennent de naître ! Ils ne sont donc pas très beaux car ils n'ont pas de poil ! Leurs poils arrivent au bout de 3 jours...")."</a></div>";
                
                if ($userdata['niveau'] == NIVEAU_CABANE_BEBE_AVATAR)
                    updateNiveauJoueur(1,true);
            }
            else { // ou non => indication du nombre de hamsters nés et leur sexe
            
                // on propose ici de choisir leurs noms        
                echo "<div>".sprintf(T_("Ton hamster est épuisé mais il a eu ... %d beaux bébés !"),$nbNouveauxHamsters)."</div>";
                echo "<div>".T_("Choisis leur prénom maintenant !")."</div>";
                echo "<form method=get action=\"jeu.php\">";
                echo "<input type=hidden name=mode value=m_hamster>";
                echo "<input type=hidden name=hamster_id value=".$hamRow['hamster_id'].">";
                echo "<input type=hidden name=voirbebes value=1>";
                echo "<input type=hidden name=choisirPrenomsBebes value=1>";
                
                if ($nbNouveauxHamsters >= 2) {
                    echo T_("Prénom du 1er bébé")." : <input type=textbox name=prenom1 value=\"\"><br/>";
                    echo T_("Prénom du 2ème bébé")." : <input type=textbox name=prenom2 value=\"\"><br/>";
                }
                if ($nbNouveauxHamsters == 3) {
                    echo T_("Prénom du 3ème bébé")." : <input type=textbox name=prenom3 value=\"\"><br/>";
                }
                
                echo "<input type=submit value=\"".T_("Donner ces prénoms aux bébés")."\">";
                echo "</form>";
            }
        }
    }
}
// 2) le joueur veut avoir des bébés
else {
    if ($hamRow['sexe'] == 0) {
        echo T_("Seules les femelles peuvent avoir des bébés ! ");
    }
    else if ( ($dateActuelle - $hamRow['date_naissance']) < 3600*24*10) 
            echo $hamRow['nom']." ".T_("est trop jeune pour se reproduire. Elle doit avoir au moins 10 jours."); 
            
    else if ($hamRow['sexe'] != 1){ // bébé en cours ! le "sexe" indique le début de la grossesse
        
    }
    else {
        if (yatilAccessoireDansLaCage($hamRow['cage_id'],ACC_NID,$userdata['joueur_id']) == -1){
            echo "<br/>".$hamRow['nom']." ".T_("est assez agée pour avoir des bébés mais il faut un nid dans sa cage pour les accueillir")." ! <br/>&nbsp;<br/>"  ;
        }
        else {
            echo "<br/>".$hamRow['nom']." ".T_("est assez agée pour avoir des bébés")." ! <br/>&nbsp;<br/>"  ;     
            
            if ($hamRow['compagnon_id'] != 0) {                
                echo "<a href=\"jeu.php?mode=m_hamster&hamster_id=".$hamRow['hamster_id']."&amp;bebe=".$row['compagnon_id']."\">".T_("Avoir un bébé avec son mari")." ".getNomHamsterFromId($hamRow['compagnon_id'])."</a><br/>".T_("Ou")."<br/>";
            }
            
            echo T_("Pour trouver un mâle tu peux :")."<br/>";
            echo "<ul><li><a href=\"jeu.php?mode=m_annuaire&amp;action=avoirunbebe&amp;hamster_id_concerne=".$hamRow['hamster_id']."\">".T_("Soit utiliser l'annuaire de Hamster Academy")."</a></li>";
            echo "<li>".T_("Soit choisir ci-dessous, parmi une sélection des hamsters mâles")." : </li></ul>";
            echo "<div style=\"width:700px;text-align:left; margin-left:auto ; margin-right:auto;\">" ;
                
            // liste des hamsters mâles
            $query = "SELECT type, hamster_id, nom, sante, puissance, beaute, poids FROM hamster WHERE sexe=0 AND date_naissance < ".($dateActuelle - 2592000). " AND maxSante >= 20 LIMIT 200"; // 2592000 = 30 jours (en secondes)
            if ( !($result = $dbHT->sql_query($query)) ){
                message_die(GENERAL_ERROR, 'Error in obtaining hamster_data', '', __LINE__, __FILE__, $query);
            }
            $nbHamstersMales = $dbHT->sql_numrows($result) ;
            while($row=$dbHT->sql_fetchrow($result)) {
                echo "<div><span style=\"vertical-align:middle;\"><img src=\"images/".$infosHamsters[$row['type']][HAMSTER_IMAGE]."\" height=50></span><span style=\"vertical-align:middle;\"> ".$row['nom']." : ".T_("santé").":".round($row['sante'],1).", ".T_("force").":".round($row['puissance'],1).", ".T_("beauté").":".round($row['beaute'],1).", ".T_("poids").":".$row['poids']."gr :  <a href=\"jeu.php?mode=m_hamster&hamster_id=".$hamRow['hamster_id']."&bebe=".$row['hamster_id']."\" title=\"".T_("Choisir cet hamster pour le futur papa")."\">".T_("choisir ce mâle")."</a></span></div>";        
            }
            $dbHT->sql_freeresult($result);
        }
    }
}
?>
</div>

<br/>&nbsp;<br/>
