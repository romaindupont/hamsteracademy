<?php
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

define('METIER_NOM',0) ;
define('METIER_SALAIRE',1) ;
define('METIER_COUT_FORMATION',2) ;
define('METIER_IMAGE_SRC',3) ;
define('METIER_DESCRIPTION',4) ;
define('METIER_NIVEAU_ACCESSIBLE',5) ;

// 0=nom, 1=salaire hebdo, 2=cout formation, 3=image, 4=description
$lstMetiers = array(
0 => array(T_('Salaire de base'),7,0,'metier_base.jpg',T_('Chaque joueur a droit, chaque jour, à un salaire de base.'),0),
1 => array(T_('Artiste de rue'),14,80,'jongleur.jpg',T_('Apprends à jongler, à faire du diabolo, monte un spectacle et attire du public dans les rues !'),3),
2 => array(T_('Peintre'),8,42,'peintre3.gif',T_('Apprends la peinture et vends tes plus belles toiles.'),2),
3 => array(T_('Pompier bénévole'),10,70,'pompier2.gif',T_('Aide les pompiers dans leurs missions quotidiennes. Affronter le feu, aider les personnes en difficultés... Une formation passionnante est nécessaire.'),3),
4 => array(T_('Sauveteur en mer'),30,300,'sauveteur.gif',T_('Sauve les personnes en danger dans l\'océan : natation, massage cardiaque, hélitreillage, plongée ... font partie de tes missions quotidiennes.'),4),
5 => array(T_('Joueur de tennis'),20,150,'joueur_tennis.gif',T_('Apprends à jouer au tennis, à obtenir un coup droit et un revers foudroyants ! Mais l\'entrainement est long et coûteux...'),4),
6 => array(T_('Vétérinaire au zoo'),80,1000,'Veterinaire_reduc.gif',T_('Soigne les lions, les loups, les ours, les flamands roses, les rhinocéros au zoo. Face à ces animaux dangereux, il faut tout apprendre sur leurs comportements. C\'est une formation longue et couteûse mais ... payante !'),5),
7 => array(T_('Musicien'),15,95,'musician3.gif',T_('Apprends à lire les notes pour composer tes propres morceaux afin de jouer devant un public.'),4),
8 => array(T_('Marin'),25,160,'pecheur3.gif',T_('Tu n\'as pas le mal de mer ? C\'est mieux pour devenir marin ! Il va falloir apprendre à lire les vagues, le vent, les marées et savoir pêcher ! En route pour l\'océan !'),5),
9 => array(T_('Pilote de rallye'),50,500,'rallye3.gif',T_('Piloter une voiture ultra-puissante, sauter à chaque bosse, soulever un nuage de poussière à chaque virage... Cette formation de pilote de rallye est une vraie porte pour le paradis des sensations fortes !'),6),
10 => array(T_('Policier'),28,280,'police.gif',T_('Apprends à manier ton arme, pars arrêter les dangereux criminels et fais respecter la loi. Mais ca ne t\'empêche pas d\'aider les vieilles dames à traverser la rue ! Ce métier est risqué, mais le salaire est élevé.'),5),
11 => array(T_('Détective'),50,500,'detective.gif',T_('Si tu es perspicace, fonce à l\'agence des détectives pour en devenir un ! Les enigmes mystérieuses, les meurtres non élucidés, les enquêtes à rebondissements, tel sera ton quotidien !'),6),
12 => array(T_('Footballeur'),100,1200,'footballer.gif',T_('Qui n\'a jamais revé de devenir footballer professionnel. Une occasion pour toi de porter les couleur de ton équipe préférée.'),8),
13 => array(T_('Skateur professionnel'),15,95,'skater.gif',T_('Après la formation et l\'apprentissage des rudiments du skate, envole toi sur les rampes et réalise tes plus beaux sauts.'),4),
14 => array(T_('Botaniste'),12,75,'microscope.gif',T_('Le botaniste partage son temps entre des activités de terrain et des analyses en laboratoire. Pars établir des inventaires de fleurs ou encore des cartographies botaniques dans des régions vierges et difficiles d\'accès. Un métier peu risqué, réservé aux amoureux de la nature !'),3),
15 => array(T_('Vendeur de Hamburgers'),9,55,'hamburger.gif',T_('Tu es le roi des Fast-Foods, une feuille de salade, une tomate, du fromage, un steak entre deux pains et le tour est joué !'),2),
16 => array(T_('Mannequin'),17,130,'mannequin.jpg',T_('Quoi de plus tranquille que le métier de mannequin ? C\'est plutôt bien payé, la formation est facile, et on doit juste se trémousser sur un podium. Mais attention à bien surveiller sa ligne !'),4),
17 => array(T_('Explorateur de l\'Amazonie'),23,155,'botaniste.gif',T_('Pars découvrir la plus grande forêt du monde et toutes les richesses qu\'elle cache. Mais attention, les parties les plus lointaines renferment certains des animaux les plus dangereux. Gare aux piqures de serpent !'),4),
18 => array(T_('Manucure de stars'),27,230,'manucure.jpg',T_('Dans le salon le plus réputé, tu dois t\'occuper des soins de la main pour tes clientes fortunées.  Crème adoucissante, pose de vernis sur les ongles, un vrai moment de détente.'),5),
19 => array(T_('Danseuse étoile'),45,400,'danseuse.gif',T_('Si tu veux devenir danseuse étoile à l\'opéra ou intégrer un groupe de danse moderne, cette formation est pour toi. Elle est chère mais le salaire est garanti !'),6),
20 => array(T_('Eleveur de crocodiles'),9,60,'crocodile.gif',T_('Si tu n\'as pas peur des puissantes machoires des crocodiles et que tu aimes les animaux sauvages, ce métier est fait pour toi ! Tous les jours, il faut nourrir une centaine de crocodiles élevés dans une ferme spéciale : les marécages artificiels et les rivières naturelles de cette ferme permettent aux crocodiles de grandir comme dans la vraie nature ! Au fait, qui n\'a pas encore mangé de viande de crocodile ?'),4)
);

$nbMetiers = sizeof($lstMetiers);

?>