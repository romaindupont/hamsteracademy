<?php 

define('IN_HT', true);
define('NO_SMARTY',true);
$dateActuelle = time();

include "../common.php";
include "../gestion.php";
error_reporting(E_ALL);

$login = isset($_POST['login']) ? $_POST['login'] : "";
$mdp = isset($_POST['password']) ? $_POST['password'] : "";

//$login = 'mister_gom';
//$mdp = 'gomgom';

//$login = 'Poes';
//$mdp = 'Bokje18';

$user="hamsteracademy";
$password="SKdMyPMk";
$database="hamsteracademy";
mysql_connect("mysql51-30.bdb",$user,$password);
@mysql_select_db($database) or die( "Unable to select database");
$query="SELECT * FROM  `joueurs` j WHERE 
	j.`pseudo` = '".$login."' 
	AND j.`passwd` = '".$mdp."' ;";
//echo $query."<br>";

$result = mysql_query($query);
$nbResult = mysql_num_rows($result);

if($nbResult != 1){
	die("");
}else{
	$row = mysql_fetch_array($result);
	$xmlRetour = '<?xml version="1.0" encoding="UTF-8" ?>';
	$xmlRetour .= "<user>";
	
    $xmlRetour .= "<idJoueur>".$row["joueur_id"]."</idJoueur>";
    $xmlRetour .= "<login>".$row["pseudo"]."</login>";
    $xmlRetour .= "<avatar>".$row["image"]."</avatar>";
    $xmlRetour .= "<nbPieces>".$row["nb_pieces"]."</nbPieces>";
    $xmlRetour .= "<copeaux>".$row["copeaux"]."</copeaux>";
    $xmlRetour .= "<aliments>".$row["aliments"]."</aliments>";
    $xmlRetour .= "<points>".$row["note"]."</points>";
    $xmlRetour .= "<classement>".calculerClassement($row['note'],"joueurs")."</classement>";
    
    // Hamster
    $xmlRetour .= "<hamsters>";
    $queryHamsters="SELECT * FROM  `hamster` h WHERE 
	h.`joueur_id` = '".$row["joueur_id"]."' ;";
	//echo $queryHamsters."<br>";
	$resultHamsters = mysql_query($queryHamsters);
	while ($rowHamsters = mysql_fetch_array($resultHamsters)) {
		$xmlRetour .= "<hamster>";
		$xmlRetour .= "<idHamster>".$rowHamsters['hamster_id']."</idHamster>";
		$xmlRetour .= "<image>".$rowHamsters['type']."</image>";
		$xmlRetour .= "<nom>".$rowHamsters['nom']."</nom>";
		$xmlRetour .= "<faim>".faim($rowHamsters['dernier_repas'])."</faim>";
		$xmlRetour .= "<maxSante>".$rowHamsters['maxSante']."</maxSante>";
		$xmlRetour .= "<sante>".$rowHamsters['sante']."</sante>";
		$xmlRetour .= "<maxMoral>".$rowHamsters['maxMoral']."</maxMoral>";
		$xmlRetour .= "<moral>".$rowHamsters['moral']."</moral>";
		$xmlRetour .= "<maxForce>".$rowHamsters['maxForce']."</maxForce>";
		$xmlRetour .= "<force>".$rowHamsters['puissance']."</force>";
		$xmlRetour .= "<maxBeaute>".$rowHamsters['maxBeaute']."</maxBeaute>";
		$xmlRetour .= "<beaute>".$rowHamsters['beaute']."</beaute>";
		$xmlRetour .= "<poids>".$rowHamsters['poids']."</poids>";
		$xmlRetour .= "<isNourrirAllowed>1</isNourrirAllowed>";
		$xmlRetour .= "<isCaresserAllowed>1</isCaresserAllowed>";
		$xmlRetour .= "<isSoignerAllowed>1</isSoignerAllowed>";
		$xmlRetour .= "<isMusclerAllowed>1</isMusclerAllowed>";
		$xmlRetour .= "</hamster>";
	}
	mysql_free_result($resultHamsters);
	$xmlRetour .= "</hamsters>";
	
	// Cage
	$xmlRetour .= "<cages>";
	$queryCages="SELECT * FROM  `cage` c WHERE 
	c.`joueur_id` = '".$row["joueur_id"]."' ;";
	//echo $queryCages."<br>";
	$resultCages = mysql_query($queryCages);
	while ($rowCages = mysql_fetch_array($resultCages)) {
		$xmlRetour .= "<cage>";
		$xmlRetour .= "<idCage>".$rowCages['cage_id']."</idCage>";
		$xmlRetour .= "<nom>".$rowCages['nom']."</nom>";
		$xmlRetour .= "<proprete>".$rowCages['proprete']."</proprete>";
		$xmlRetour .= "<isNettoyerAllowed>1</isNettoyerAllowed>";
		$xmlRetour .= "</cage>";
	}
	mysql_free_result($resultCages);
	$xmlRetour .= "</cages>";
	
	// Message
	$xmlRetour .= "<messages>";
	$queryMessages="SELECT * FROM  `messages` m,`joueurs` j WHERE 
	m.`dest_joueur_id` = '".$row["joueur_id"]."' 
	AND j.`joueur_id` = m.`exp_joueur_id` ORDER BY `date` LIMIT 0,10
	;";
	//echo $queryMessages."<br>";
	$resultMessages = mysql_query($queryMessages);
	while ($rowMessages = mysql_fetch_array($resultMessages)) {
		$xmlRetour .= "<message>";
		$xmlRetour .= "<idMessage>".$rowMessages['message_id']."</idMessage>";
		$xmlRetour .= "<status>".$rowMessages['status']."</status>";
		$xmlRetour .= "<sender>".$rowMessages['pseudo']."</sender>";
		$xmlRetour .= "<idSender>".$rowMessages['exp_joueur_id']."</idSender>";
		$body = str_ireplace(">", "_______",str_ireplace("<", "______", $rowMessages['message']));
		$xmlRetour .= "<messageBody>".$body."</messageBody>";
		$xmlRetour .= "<subject>".substr($rowMessages['pseudo']." - ".$body,0,23)."...</subject>";
		$xmlRetour .= "</message>";
	}
	mysql_free_result($resultMessages);
	$xmlRetour .= "</messages>";
	
	// Amis
	
	$queryAmis="SELECT * FROM  `amis` a,`joueurs` j WHERE 
	a.`joueur_id` = '".$row["joueur_id"]."' AND j.`joueur_id` = a.`ami_de`
	;";
	$xmlRetour .= "<amis>";
	//echo $queryAmis."<br>";
	$resultAmis = mysql_query($queryAmis);
	while ($rowAmis = mysql_fetch_array($resultAmis)) {
		$xmlRetour .= "<ami>";
		$xmlRetour .= "<idUser>".$rowAmis['ami_de']."</idUser>";
		$xmlRetour .= "<nom>".$rowAmis['pseudo']."</nom>";
		$xmlRetour .= "</ami>";
	}
	mysql_free_result($resultAmis);
	$xmlRetour .= "</amis>";
	$xmlRetour .= "</user>";
	
	header ("content-type: text/xml");
	echo $xmlRetour;	
}

mysql_free_result($result);

mysql_close();

//if($login == "admin" && $password == "test"){


/*
?>
<user>
	<login><?php echo $_POST['login']; ?></login>
	<avatar>http://www.webelicious.fr/hamsteracademy/grande_silhouette.gif</avatar>
	<or>168</or>
	<copeaux>7</copeaux>
	<aliments>9</aliments>
	<points>7987</points>
	<classement>348</classement>
	<hamsters>
		<hamster>
			<image>hamster0.jpg</image>
			<nom>Georges</nom>
			<faim>2</faim>
			<sante>11</sante>
			<moral>10</moral>
			<force>2</force>
			<beaute>11</beaute>
			<poids>100</poids>
		</hamster>
		<hamster>
			<image>hamster2.jpg</image>
			<nom>Lulu</nom>
			<faim>6</faim>
			<sante>8</sante>
			<moral>9</moral>
			<force>13</force>
			<beaute>6</beaute>
			<poids>230</poids>
		</hamster>
	</hamsters>
	<cages>
		<cage>
			<idCage>30053</idCage>
			<nom>Eden</nom>
			<proprete>0.6</proprete>
		</cage>
		<cage>
			<idCage>303034</idCage>
			<nom>Babylon</nom>
			<proprete>0.3</proprete>
		</cage>
		<cage>
			<idCage>30055</idCage>
			<nom>127.0.0.1</nom>
			<proprete>0.8</proprete>
		</cage>
	</cages>
	<messages>
		<message>
			<idMessage>30053</idMessage>
			<status>1</status>
			<sender>Admin</sender>
			<idSender>2</idSender>
			<messageBody>Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// Test sur la longueur du message et l'encodage éè ! à $*^¨loool !! /// </messageBody>
		</message>
		<message>
			<idMessage>30054</idMessage>
			<status>2</status>
			<sender>Jerem35</sender>
			<idSender>2</idSender>
			<messageBody>Faut que tu m'envoie ton hamster !!</messageBody>
		</message>
	</messages>
	<amis>
		<ami>
			<idUser>30053</idUser>
			<nom>Celestin</nom>
		</ami>
		<ami>
			<idUser>30054</idUser>
			<nom>Bridou justin</nom>
		</ami>
		<ami>
			<idUser>30055</idUser>
			<nom>TG toi même</nom>
		</ami>
	</amis>
</user>
<?php }*/?>