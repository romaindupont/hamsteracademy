<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}
// format_date returns a formatted date for display. The date given in
// argument can be a unixdate (number of seconds since the 01.01.1970) or an
// american format (2003-09-15). By option, you can show the time. The
// output is internationalized.
//
// format_date( "2003-09-15", 'us', true ) -> "Monday 15 September 2003 21:52"
function format_date($date, $show_time = false)
{

  if ($show_time)
      return date("d/m/Y - H:i",$date) ;      
    else
        return date("d/m/Y",$date) ;
}

/**
Validate an email address.
Provide email address (raw input)
Returns true if the email address has the email 
address format and the domain exists.
*/
function validEmail($email)
{
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex)
   {
      $isValid = false;
   }
   else
   {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if (strpos($domain,".") === false)
      {
         // domain part has no dot
         $isValid = false;
      }
      else if ($domain == "msn.fr" || $domain == "video.com" || $domain == "msn.ca" || $domain == "freel.fr" || $domain == "livel.fr" || $domain == "hotmai.com" || $domain == "otmail.com" || $domain == "hotemail.com" || $domain == "hotmeil.com")
        $isValid = false;
      else if
(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
        //domain not found in DNS
         $isValid = false;
      }
      // attempts a socket connection to mail server
//      if($isValid && !fsockopen($domain,25,$errno,$errstr,30)) {
//         $isValid = false;
//      }
   }
   return $isValid;
}


// validate_mail_address verifies whether the given mail address has the
// right format. ie someone@domain.com "someone" can contain ".", "-" or
// even "_". Exactly as "domain". The extension doesn't have to be
// "com". The mail address can also be empty.
// If the mail address doesn't correspond, an error message is returned.
function validate_mail_address( $mail_address )
{
    $mail_address = trim($mail_address);
    
  if ( $mail_address == '' )
  {
    return 0;
  }
  if (validEmail($mail_address)) {
      return 1;
  }
  else
    return 0;
//  $regex = '/^[\w-]+(\.[\w-]+)*@[\w-]+(\.[\w-]+)*\.[a-z]+$/';
//  if ( !preg_match( $regex, $mail_address ) )
//  {
//    return 0;
//  }
//  else {
//      $dns="www.".substr(strchr($mail_address,'@'),1);
//      return (gethostbyname($dns)!=$dns)?1:0;
//  }
}

function message_die($msg_code, $msg_text = '', $msg_title = '', $err_line = '', $err_file = '', $sql = '')
{
    echo "<html>\n<body>\n" . $msg_title . "\n<br /><br />\n" . $msg_text . "<br />Sql query : ".$sql."<br/>File : ".$err_file."<br/>Line : ".$err_line."</body>\n</html>";
    exit;
}

function escapeRequest($val) {
    
    return preg_replace("/[^_A-Za-z0-9-\.&=]/i",'', $val);  
}


function redirectHT($url,$time_delay=0)
{
    global $dbHT, $board_config;

    if (!empty($dbHT))
    {
        $dbHT->sql_close();
    }

    if (strstr(urldecode($url), "\n") || strstr(urldecode($url), "\r"))
    {
        message_die(GENERAL_ERROR, 'Tried to redirect to potentially insecure url.');
    }

    $server_protocol = 'http://';
    $server_name = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($board_config['server_name']));
    $server_port = ($board_config['server_port'] <> 80) ? ':' . trim($board_config['server_port']) : '';
    $script_name = preg_replace('#^\/?(.*?)\/?$#', '\1', trim($board_config['script_path']));
    $script_name = ($script_name == '') ? $script_name : '/' . $script_name;
    $url = preg_replace('#^\/?(.*?)\/?$#', '/\1', trim($url));

    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=utf-8" /><meta http-equiv="refresh" content="'.$time_delay.' ; url=' . $server_protocol . $server_name . $server_port . $script_name . $url . '" /><title>Patientez svp ...</title></head><body>';
    echo '</body></html>';
        
    // Behave as per HTTP/1.1 spec for others
//header('Location: ' . $server_protocol . $server_name . $server_port . $script_name . $url);
    exit;

}

function redirectMailHT($email,$subject,$texte,$time_delay=0)
{
    global $dbHT;

    if (!empty($dbHT))
    {
        $dbHT->sql_close();
    }

    $subject = utf8_encode($subject);
    $texte = str_replace("'","`",$texte);
    $texte = utf8_encode($texte);
    $texte = str_replace("\n","%0A",$texte);

    echo '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"><html><head><meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" /><meta http-equiv="refresh" content="'.$time_delay.' ; url=\'mailto:' . $email . '?subject='.$subject.'&body='.$texte.'" \' />
    <link rel="stylesheet" href="css/style.css" type="text/css">
    <link rel="stylesheet" href="css/styleEleveur.css" type="text/css">
    <title>Redirect mail</title></head><body>';
    echo 'Il faut envoyer maintenant le mail (dans la fenêtre qui devrait apparaître) ! <br/>&nbsp;<br/>Une fois que cela est fait : <a href="parrainage.php">Revenir au jeu ! </a>' ;
    echo '</body></html>';
        
    exit;
}

function dessinerTableauNote($note, $noteMax, $hauteurCase=5, $largeurCase=10) {
    
    $couleur = "bgcolor=\"black\"";
    if ($note < $noteMax/3)
        $couleur = "bgcolor=\"red\"";
    else if ($note < 3*$noteMax/4)
        $couleur = "class=\"barresNiveauxOrange\" " ;
    else
        $couleur = "class=\"barresNiveauxVert\" " ;
        
    echo "<table width=\"".($largeurCase*10)."\" style=\"border:1px solid #bbbbbb; border-spacing:0px; border-collapse: collapse;\"><tr><td>" ;
    echo "<table width=\"".($largeurCase*10)."\" style=\"border:0px; border-spacing:0px; border-collapse: collapse;\"><tr>" ;
    $echelle = $noteMax / 10;
    for($i=0;$i<10;$i++ ){
        
        if ($i*$echelle < $note ) 
            echo "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" ".$couleur."></td>";
        else
            echo "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" bgcolor=\"white\"></td>";
            
    }
    echo "</tr></table>";
    echo "</td></tr></table>";
}

function dessinerTableauPoids($poids, $hauteurCase=5, $largeurCase=10) {
    
    global $poidsMoyen;
    
    $couleur = "bgcolor=\"black\"";
    if ( abs($poids - $poidsMoyen) > 30) {
        $couleur = "bgcolor=\"red\"";
    }
    else if (abs($poids - $poidsMoyen) > 15) {
        $couleur = "class=\"barresNiveauxOrange\" " ;
    }
    else
        $couleur = "class=\"barresNiveauxVert\" " ;
    
    
    echo "<table width=\"".(10*$largeurCase)."\" style=\"border:1px solid #bbbbbb; border-spacing:0px; border-collapse: collapse;\"><tr><td>" ;
    echo "<table width=\"".(10*$largeurCase)."\" style=\"border:0px; border-spacing:0px; border-collapse: collapse;\"><tr>" ;
    for($i=0;$i<10;$i++ ){
        
        if ($i < (($poids-79)/8) )
            echo "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" ".$couleur."></td>";
        else
            echo "<td height=\"".$hauteurCase."\" width=\"".$largeurCase."\" bgcolor=\"white\"></td>";
            
    }
    echo "</tr></table>";
    echo "</td></tr></table>";
}

function afficherDuree($duree) {
    
    if ($duree < 60)
        return $duree." ".T_("seconde(s)");
    else if ($duree < 3600)
        return round($duree/60)." ".T_("minute(s)");
    else if ($duree < 86400)
        return round($duree/3600)." ".T_("heure(s)");        
    else if ($duree < 2613600)
        return round($duree/86400)." ".T_("jour(s)");
    else if ($duree < 31536000)
        return round($duree/2613600)." ".T_("mois");            
    else {
        $nbAnnees = round($duree/31536000);
        if ($nbAnnees == 1)
            return T_("1 an et ").round(($duree-31536000)/2613600)." ".T_("mois");
        else
            return $nbAnnees." ".T_("ans et ").round(($duree-31536000)/2613600)." ".T_("mois");
    }
}

// affiche une nouvelle (résultat d'une action, évènement, etc.) (utilisée par jeu_hamster.php, jeu_cage.php, etc.)
function afficherNouvelle($imageSrc, $nouvelle, $imageAlt="", $imageWidth=-1, $imageHeight=-1, $float=true) {
    
    if ($float)
        $txt = "<div class=\"ham1Nouvelle\">";
    else
        $txt = "";
    $txt .= "<div class=\"hamNouvellesImg\"><img src=\"images/".$imageSrc."\" alt=\"".$imageAlt."\" ";
    if ($imageWidth > 0)
        $txt .= " width=\"".$imageWidth."\"";
    if ($imageHeight > 0)
        $txt .= " height=\"".$imageHeight."\"";
    $txt .= " /></div><div class=\"hamBlocTxt\">".$nouvelle."</div>";
    if ($float)
        $txt .= "</div>\n" ;
    else
        $txt .= "\n";
    
    return $txt;
}

function afficherNouvelleDansCadre($imageSrc, $nouvelle, $imageAlt="", $imageWidth=-1, $imageHeight=-1) {
    
    $txt = "<div class=\"hamBlocColonne-top\">
         <div class=\"hamBlocColonne-top-left\"></div>
         <div class=\"hamBlocColonne-top-right\"></div>
         <div class=\"hamBlocColonne-top-center\"></div>        
      </div>
      <div class=\"hamBlocColonne_full\">";
    
    $txt .= afficherNouvelle($imageSrc, $nouvelle, $imageAlt, $imageWidth, $imageHeight,false);
    
    $txt .= "</div>
          <div class=\"hamBlocColonne-bottom\">
             <div class=\"hamBlocColonne-bottom-left\"></div>
             <div class=\"hamBlocColonne-bottom-right\"></div>
             <div class=\"hamBlocColonne-bottom-center\"></div>        
          </div> ";
    
    return $txt;
}

function tronquerTxt($txt, $maxCaracteres) {
    if (strlen($txt) > $maxCaracteres)
        return ( (substr($txt,0,$maxCaracteres))."..." ) ;
    else
        return $txt ;
}

function tooltip($text)
{
    $retval =' onmouseover="return tooltip.show(this);" onmouseout="tooltip.hide(this);" ';

    if (!empty($text)) {
      $retval .= ' title="'.$text.'"';
    }
        return $retval;
}

function returnLienProfil($joueur_id, $texte, $avecImage = false, $enColonne = true) {
    
    $imageTxt = "";
    if ($avecImage){
        $imageTxt = "<img src=\"images/joueurs/".$joueur_id.".jpg\" alt=\"\" style=\"height:".( $enColonne ? "40":"20" )."px;vertical-align:middle;\" /> ";
        if ($enColonne)
            $imageTxt .= "<br/>";
    }
    return "<a href=\"profil.php?joueur_id=".$joueur_id."\" onclick=\"javascript: pop('profil.php?joueur_id=".$joueur_id."',null,600,800); return false;\" title=\"".T_("Voir son profil")."\">".$imageTxt.$texte."</a>";
}

function afficherProfilAleatoire($enColonne = true) {
 
    global $dbHT, $dateActuelle, $lang;
    
    $lstProfilsAleatoires = array();
    $query = "SELECT ValeurTxt FROM config WHERE parametre = 'profilsAleatoires_".$lang."'";
    if ($result = $dbHT->sql_query($query) ){
        $row=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        $val = explode(";",$row['ValeurTxt']);
            
        $nbJoueursProfil = sizeof($val) / 2;
        $profilChoisi = rand(0,$nbJoueursProfil-1);
        
        $profil_id = $val[$profilChoisi*2];
        $profil_pseudo = $val[$profilChoisi*2+1];
        
        return "<div ".( $enColonne ? "align=\"center\"" : "").">".returnLienProfil($profil_id,$profil_pseudo,true,$enColonne)."</div>"; 
    }
}

function returnLienProfilGroupe($groupe_id, $texte) {
    return "<a href=\"afficheGroupe.php?groupe_id=".$groupe_id."\" onclick=\"javascript: pop('afficheGroupe.php?groupe_id=".$groupe_id."',null,800,800); return false;\" title=\"".T_("Voir le groupe")."\">".$texte."</a>";
}
function afficherLienProfilGroupe($groupe_id, $texte) {
    echo returnLienProfilGroupe($groupe_id, $texte);
}

function getConfig($parametre)
{
    global $dbHT;
    
    $query = "SELECT ValeurInt,ValeurTxt FROM config WHERE parametre = '$parametre'";
    
    if ($result = $dbHT->sql_query($query) ){
        $row=$dbHT->sql_fetchrow($result);
        $dbHT->sql_freeresult($result);
        
        if ($row['ValeurTxt'] != "")
            return $row['ValeurTxt'];
        else
            return $row['ValeurInt'];
    }
}
function updateConfig($parametre, $valeur)
{
    global $dbHT;
    
    $query = "";
    if (is_numeric($valeur))
        $query = "UPDATE config SET ValeurInt = $valeur WHERE parametre = '$parametre'";
    else
        $query = "UPDATE config SET ValeurTxt = '$valeur' WHERE parametre = '$parametre'";
    
    $dbHT->sql_query($query);
}

function nbValeursBD($table, $champ, $valeurChamp, $autreContraintes = ""){

    global $dbHT;
    
    $query = "";
    if (is_numeric($valeurChamp))
        $query = "SELECT $champ FROM $table WHERE $champ=$valeurChamp";
    else
        $query = "SELECT $champ FROM $table WHERE $champ='$valeurChamp'";
    
    if ($autreContraintes != "")
        $query .= " AND ".$autreContraintes;
    
    if ( !($result = $dbHT->sql_query($query)) ){
        message_die(GENERAL_ERROR, 'Error : ', '', __LINE__, __FILE__, $query);
    }
    $nbValeurs = $dbHT->sql_numrows($result) ;   
    return $nbValeurs;
}

?>