<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

echo "<div style=\"font-size:9pt;\"><strong>".T_("Hamster Academy te plait ? Parle-en à tes amis !")."</strong><br/>&nbsp;<br/>";

echo "</div><br/>";

echo "<div class=\"blocOptions\">";

echo T_("Saisis le prénom de ton ami et son adresse email ! Il recevra une invitation à jouer à Hamster Academy et tu seras son parrain !");

echo "<br/>&nbsp;<br/>";

echo "
<form method=\"POST\">
        <table>
            <tr>
                <td><label for=\"user\">".T_("L'adresse email de ton ami")." : </label></td>
                <td><input type=\"text\" name=\"email\" id=\"user\" value=\"\" /></td>
            </tr>
            <tr>
                <td><label for=\"pass\">".T_("Le prénom de ton ami")." : </label></td>
                <td><input type=\"password\" name=\"prenom\" id=\"pass\"/></td>
            </tr>
            <tr><td>&nbsp;</td><td>&nbsp;</td></tr>
            <tr>
                <td></td>
                <td><input type=\"submit\" value=\"".T_("Envoyer une invitation !")."\"></td>
            </tr>
        </table>
    </form>";

?>

</div>