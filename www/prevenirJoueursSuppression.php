<?php 

define('IN_HT', true);

include "common.php";
include "gestion.php";
include "gestionUser.php";

error_reporting(E_ALL);

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$subject = "Avis de suppression de comptes : début" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
//envoyerMail($TO, $subject, $message,"prevenirsuppression0");

$dbForum = new sql_db($serveurForum, $userForum, $passwordForum, $baseForum, false);
if(!$dbForum->db_connect_id)
{
   echo "Oups ! Petit problème avec la BDD du forum... Appuie sur F5 pour le relancer. Si le problème persiste, contacte Hamster Academy à l'adresse email : contact@hamsteracademy.fr .<br/>" ;
   exit(1);
}

$dbHT->sql_activate();

// on récupère la liste des joueurs qui ne jouent pas depuis un bon bout de temps (depuis 3 mois)
$query = "SELECT joueur_id, pseudo, user_lastvisit, date_dernier_salaire FROM joueurs WHERE joueur_id > 1 AND user_lastvisit < ".($dateActuelle - ((3600*24*90)));

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining joueur data', '', __LINE__, __FILE__, $query);
}

$nbJoueurs= $dbHT->sql_numrows($result) ;

echo "Suppression des comptes :<br/>";

$nbJoueursReellementSupprimes = 0;
$listeJoueursSupp = "";

while($row=$dbHT->sql_fetchrow($result)) {

    $joueur_id = $row['joueur_id'] ;

    // on vérifie avec la date du dernier salaire s'il n'y a pas un pb avec user_lastvisit
    if ($row['date_dernier_salaire'] < ($dateActuelle - ((3600*24*(91))))) {
        echo $row['pseudo']." , ";
        
        $nbJoueursReellementSupprimes++ ;
    
        supprimerCompte($row['joueur_id'], false);
        
        $listeJoueursSupp .= $row['pseudo'].",";
        
    }
}

echo "nb joueurs = ".$nbJoueurs."<br/>" ;
echo "nb joueurs supprimés = ".$nbJoueursReellementSupprimes."<br/>" ;

$dbHT->sql_freeresult($result);

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$subject = "Avis de suppression de comptes" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= "nb de comptes : ".$nbJoueursReellementSupprimes." sur ".$nbJoueurs." potentiels : \n";
$message .= $listeJoueursSupp ;
envoyerMail($TO, $subject, $message,"prevenirsuppression1");

// on récupère la liste des joueurs qui ne jouent pas depuis un bon bout de temps (depuis 3 mois moins 1 semaine)
// (ie. on prévient une semaine avant 3 mois que le compte va être supprimé)
$query = "SELECT joueur_id, pseudo, passwd, email, nb_pieces FROM joueurs WHERE user_lastvisit > ".($dateActuelle - ((3600*24*90)))." AND user_lastvisit < ".($dateActuelle - ((3600*24)*82))." AND email_active = 1";

if ( !($result = $dbHT->sql_query($query)) ){
    message_die(GENERAL_ERROR, 'Error in obtaining joueur data', '', __LINE__, __FILE__, $query);
}

$nbJoueurs= $dbHT->sql_numrows($result) ;
$listeJoueursRelances = "";

echo "Mail envoyé à <br/>";

while($row=$dbHT->sql_fetchrow($result)) {

    $joueur_id = $row['joueur_id'] ;
    
    // on donne 150 pièces pour aider le joueur à repartir !
    crediterPiecesBDD($joueur_id,150) ;

    $TO = $row['email'];
    $subject = T_("Hamster Academy : reviens vite !") ;
    $message = T_("Bonjour").",\n\n" ;
    $message .= T_("Cela fait 3 mois que nous n'avons plus de nouvelles de toi... Si tu veux garder ton compte, il faut absolument que tu reviennes d'ici une semaine !")."\n\n";
    $message .= T_("Pour t'aider à bien repartir et pour découvrir les nouveautés du jeu, on t'offre 150 pièces !!! Viens vite découvrir les nouveautés : forum, tchat pour discuter entre amis, mariage, famille, bébés, concours, nouveaux objets...")."\n\n";
    $message .= T_("On te rappelle tes coordonnées pour y jouer")." :\n".T_("pseudo :")." ".$row['pseudo']."\n".T_("mot de passe :")." ".$row['passwd']."\n\n";
    $message .= T_("Pour te connecter au jeu, clique sur le lien suivant")." : http://www.hamsteracademy.fr\n\n";
    $message .= T_("A bientôt sur Hamster Academy !")." :-)";
    envoyerMail($TO, $subject, $message,"prevenirsuppression2");
    
    echo $row['pseudo']." , ";
    $listeJoueursRelances .= $row['pseudo'].",";
}

echo "nb joueurs = ".$nbJoueurs ;

$dbHT->sql_freeresult($result);

// mail bilan pour l'admin
$TO = "contact@hamsteracademy.fr";
$subject = "Avis d'envoi d'email groupé (prévention de suppression de comptes)" ;
$message = "Heure : ".date('l dS \of F Y h:i:s A')."\n";
$message .= "nb de comptes : ".$nbJoueurs." : \n";
$message .= $listeJoueursRelances;
$mail_sent = @mail($TO, $subject, $message, $headerMailFromHamsterAcademy);

?>