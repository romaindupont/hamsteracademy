<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$dernieresNouveautes = '
<div class="newsBloc"><div class="newsDate">Monday, December, 27th</div>
<div class="newsTxt">- New mission 3 : treat your hamster</div>
<div class="newsTxt">- Facebook: connect with your facebook account and publish your cage on your wall</div>
</div>
<div class="newsBloc"><div class="newsDate">Thursday, December 2nd</div>
<div class="newsTxt">- <img src="images/flocon.gif" width="40" alt="Snow" align="middle" /> Snow has arrived on Hamster Academy! Buy Snow Crystal at the shop!</div>
</div>
<div class="newsBloc"><div class="newsDate">Sunday, November 28th</div>
<div class="newsTxt">- <img src="images/chomik18.jpg" width="40" alt="Timid Hamster" align="middle" /> New hamster, available at the Shop!</div>
</div>
<div class="newsBloc"><div class="newsDate">Saturday, November 27th</div>
<div class="newsTxt">- <img src="images/coupe.gif" width="20" alt="Contest" align="middle" /> New contest! Train your hamster again and again and you will be rewarded! Go in Town->Competition!</div>
</div>
<div class="newsBloc"><div class="newsDate">Monday, November 15th</div>
<div class="newsTxt">- <img src="images/amulette.png" width="40" alt="Contest" align="middle" /> New object: Amulet of Strength! Go into the Shop to buy it!</div>
</div>
<div class="newsBloc"><div class="newsDate">Tuesday, November, 2nd</div>
<div class="newsTxt">- <img src="images/halteres_reduc.png" width="40" alt="Contest" align="middle" /> Contest of the strongest hamster! Go in Town->Competition!</div>
</div>
<div class="newsBloc"><div class="newsDate">Friday, October, 8th</div>
<div class="newsTxt">- New objects: <img src="images/porte_buches2.png" width="30" alt="log carrier" /> <img src="images/porte_buches3.png" width="30" alt="log carrier" /></div>
</div>
<div class="newsBloc"><div class="newsDate">Monday, September, 27th</div>
<div class="newsTxt">- New missions 7 and 16: treat your hamster</div>
<div class="newsTxt">- New objects: <img src="images/cheminee_newage.png" width="15" alt="fireplace" /> <img src="images/ustensiles_cheminee.png" width="10" alt="fireplace" /></div>
</div>
<div class="newsBloc"><div class="newsDate">Saturday, July, 22th</div>
<div class="newsTxt">- <img src="images/FaceBook_48x48.png" width="30" alt="Facebook" align="middle" /> - <a href="http://www.facebook.com/hamsteracademy" title="Become a Fan !" >Become a fan of HA on Facebook!</a></div>
</div>
<div class="newsBloc"><div class="newsDate">Tuesday, May, 4th</div>
<div class="newsTxt">- <img src="images/muguet.png" alt="Lily of the valley" width="30" /> Buy a lily of the valley at the shop (available at level 6) and make your hamsters happier!</div>
</div>
<div class="newsBloc"><div class="newsDate">Saturday, January, 9th</div>
<div class="newsTxt">- <img src="images/part_galette.gif" alt="King cake" width="30" /> The King cake has arrived! Go to the Shop to buy it!</div>
</div>
<div class="newsBloc"><div class="newsDate">Monday, May 4th</div>
<div class="newsTxt">- Be careful, the veterinarian keeps your hamster only 2 weeks...</div>
</div>
<div class="newsBloc"><div class="newsDate">Saturday, May 2nd</div>
<div class="newsTxt">- <a href="options.php?mode=m_vip">Become a VIP on Hamster Academy</a> ! </div>
</div>
';

if (defined ('PLUS_DE_NOUVEAUTES')) {

    $dernieresNouveautes .= '
    <div class="newsBloc"><div class="newsDate">Wednesday, April 8th</div>
    <div class="newsTxt">- A new mission !</div>
    </div>
    ';
    
    if (defined ('TOUTES_LES_NOUVEAUTES')) {
    
    $dernieresNouveautes .= '';
    }
}