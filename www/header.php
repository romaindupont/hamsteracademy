<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

echo "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" ; ?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="fr" lang="fr">

<head>
<title><?php echo $pagetitle ; ?></title>
    <meta name="description" content="Bienvenue dans le meilleur jeu gratuit d'élevage virtuel de hamsters : Hamster Academy ! Pour pouvoir y jouer, il suffit de s'inscrire (gratuitement) et on peut alors rejoindre la communauté des éleveurs de Hamsters..." />
    <meta name="keywords" content="Hamster, Academy, Academie, Elever, virtuel, jeu gratuit, simulation,  jeu de role, role, virtual, mission, cage, fabriquer" />
    <link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
    <base href="<?php echo $base_site_url ;?>"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="Content-Language" content="fr_FR" />
    <meta http-equiv="Content-Script-Type" content="text/javascript" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <link href="style.css" rel="stylesheet" type="text/css" />
<?php 
if ($univers == UNIVERS_ELEVEUR){
    echo "<link rel=\"stylesheet\" href=\"styleEleveur.css\" type=\"text/css\" />\n";   
    echo "<link rel=\"stylesheet\" href=\"cornflex-common.css\" type=\"text/css\" />\n";
}
else if ($univers == UNIVERS_ACADEMY) {
    echo "<link rel=\"stylesheet\" href=\"styleAcademy.css\" type=\"text/css\" />\n";
    echo "<link rel=\"stylesheet\" href=\"style_groupe.css\" type=\"text/css\" />\n";
    echo "<link rel=\"stylesheet\" href=\"cornflex-common.css\" type=\"text/css\" />\n";
}
else if ($univers == UNIVERS_ADMIN) {
    echo "<link rel=\"stylesheet\" href=\"styleAdmin.css\" type=\"text/css\" />";
}
else if ($univers == UNIVERS_INDEX) {
    echo "<link rel=\"stylesheet\" href=\"styleIndex.css\" type=\"text/css\" />";
}
?>
<script type="text/javascript" src="scripts/gestionHamster.js"></script>
<script type="text/javascript" src="scripts/tooltip.js"></script>
<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/jquery-ui.min.js"></script>
<script type="text/javascript" src="scripts/jquery.cornflex.js"></script>
<script type="text/javascript">
        $(document).ready(function(){
                $("div.blocRounded").cornflex('yellowbox', {
                        omega: ((20)),                   // rounded corner's radius
                        image_t: 'images/cadres/yellowbox_t.png',      // path to top image (relative OR url)
                        image_r: 'images/cadres/yellowbox_r.png',      // right image
                        image_b: 'images/cadres/yellowbox_b.png',      // bottom image
                        image_l: 'images/cadres/yellowbox_l.png',      // left image
                        alpha: ((12)),                   // alpha value (cf. schema)
                        beta: ((18)),                    // beta value
                        gamma: ((24)),                   // gamma value
                        delta: ((18))                    // delta value
                        //width: ((270))
                });
        });
</script>
<style type="text/css">
.droppable-active {
}
.droppable-hover {
    opacity:0.8;
}
</style>

</head>

<body>

<div id="tooltip"></div>

<?php 
    if (! isset($pageWidth))
        echo "<div class=\"mainPage\">";
    else
        echo "<div class=\"mainPage\" style=\"width:".$pageWidth."px;\" >";    
?>

