<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

srand($userdata['joueur_id']) ;

$queryClassement =  "SELECT COUNT( cage_id ) FROM lst_cages_concours";
$resultClassement = $dbHT->sql_query($queryClassement);
if ( ! $resultClassement ) {
    echo "Erreur SQL : ".$queryClassement."<br>" ;
}
$row = $dbHT->sql_fetchrow($resultClassement);
$nbCagesTotal = $row[0] ;

$nbCagesANoter = 5;
				
$cageSupp1 = $cageSupp2 = -1 ;
$pseudoCageSupp1 = $pseudoCageSupp2 = "" ;
if (isset($_GET['pseudoCageSupp1']))
	$pseudoCageSupp1 = mysql_real_escape_string($_GET['pseudoCageSupp1']);
if (isset($_GET['pseudoCageSupp2']))
	$pseudoCageSupp2 = mysql_real_escape_string($_GET['pseudoCageSupp2']);
	
// on essaye de récupérer les noms des 2 cages supp
if ($pseudoCageSupp1 != "") {
	// 1er joueur
	$query7 = "SELECT joueur_id FROM joueurs WHERE pseudo='".$pseudoCageSupp1. "' AND inscritConcours > 0 LIMIT 1";
	if ( !($result7 = $dbHT->sql_query($query7)) ){
		message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query7);
	}
	$joueur1OK = $dbHT->sql_numrows($result7) ;
	// s'il y a bien un joueur inscrit derrière
	if ($joueur1OK == 1) {
		$row7=$dbHT->sql_fetchrow($result7);
		$joueur_proprietaire_id=$row7['joueur_id'] ;
		$query8 = "SELECT cage_id FROM lst_cages_concours WHERE joueur_proprietaire_id=".$joueur_proprietaire_id. " LIMIT 1";
		if ( !($result8 = $dbHT->sql_query($query8)) ){
			message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query8);
		}
        if ($dbHT->sql_numrows($result8) == 1) {
		    $row8 =$dbHT->sql_fetchrow($result8);
		    $cageSupp1 = $row8['cage_id'];
        }
        else {
            echo "<div class=txtErreur align=center>".T_("Attention : Le joueur ").$pseudoCageSupp1.T_(" n'a pas de cage valide pour le concours").".</div><br/>";
        }
	}
	else {
		echo "<div class=txtErreur align=center>".T_("Attention : Le joueur ").$pseudoCageSupp1.T_(" n'existe pas ou n'est pas inscrit au concours").".</div><br/>";
		$pseudoCageSupp1 = "";
	}
	$dbHT->sql_freeresult($result7);
}

if ($pseudoCageSupp2 != "") {
	// 2ème joueur
	$query7 = "SELECT joueur_id FROM joueurs WHERE pseudo='".$pseudoCageSupp2. "' AND inscritConcours > 0 LIMIT 1";
	if ( !($result7 = $dbHT->sql_query($query7)) ){
		message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query7);
	}
	$joueur2OK = $dbHT->sql_numrows($result7) ;
	// s'il y a bien un joueur inscrit derrière
	if ($joueur2OK == 1) {
		$row7=$dbHT->sql_fetchrow($result7);
		$joueur_proprietaire_id=$row7['joueur_id'] ;
		$query8 = "SELECT cage_id FROM lst_cages_concours WHERE joueur_proprietaire_id=".$joueur_proprietaire_id. " LIMIT 1";
		if ( !($result8 = $dbHT->sql_query($query8)) ){
			message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query8);
		}
		if ($dbHT->sql_numrows($result8) == 1) {
            $row8 =$dbHT->sql_fetchrow($result8);
		    $cageSupp2 = $row8['cage_id'];
        }
        else {
            echo "<div class=txtErreur align=center>".T_("Attention : Le joueur ").$pseudoCageSupp2.T_(" n'a pas de cage valide pour le concours").".</div><br/>";
        }
        
	}
	else{
		echo "<div class=txtErreur align=center>".T_("Attention : Le joueur ").$pseudoCageSupp2.T_(" n'existe pas ou n'est pas inscrit au concours").".</div><br/>";
		$pseudoCageSupp2 = "";
	}
	$dbHT->sql_freeresult($result7);
}

// on dresse la liste des cages à noter
$lstIndicesCages = "";
for($i=0;$i<$nbCagesANoter;$i++) {
	$indiceCage = rand(0,$nbCagesTotal-1) ;
	
	$query = "SELECT cage_id FROM lst_cages_concours WHERE indice=".$indiceCage. " LIMIT 1";
	if ( !($result = $dbHT->sql_query($query)) ){
		message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
	}
	if ($row=$dbHT->sql_fetchrow($result)) {	
		$lstIndicesCages .= $row['cage_id'];
		if ($i < ($nbCagesANoter-1) )
			$lstIndicesCages .= ",";
	}
	$dbHT->sql_freeresult($result);
}
					
// on rajoute les deux cages choisies par le joueur
$lstIndicesCages .= ",".$cageSupp1.",".$cageSupp2;

// on récupère leurs infos
$query = "SELECT * FROM cage WHERE cage_id IN (".$lstIndicesCages.")";

if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
}
$nbCages = $dbHT->sql_numrows($result) ;