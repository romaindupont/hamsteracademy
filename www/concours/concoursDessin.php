<?php 
	

$msg .= "<div class=ham1Nouvelle><h2 align=center>Résultats du concours du meilleur dessin</h2>";

$query = "SELECT pseudo, joueur_id, inscritConcours FROM joueurs WHERE inscritConcours > 10 ORDER BY inscritConcours DESC";

$msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"Coupe\"></div><br/>";
$msg .= "<div align=center><strong>Félicitations au vainqueur Coolmam ! Elle gagne 500 pièces et un diplôme !</strong><br/>&nbsp;<br/></div>";

if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
}

$msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>Classement</strong></td><td><strong>Nom du joueur</strong></td><td align=center><strong>Note / 20</strong></td><td><strong>Le dessin</strong></td></tr>";
$classement = 1;
$classementDecal = 1;
$precNote = -1;
while ($row=$dbHT->sql_fetchrow($result)) {
	
	$noteArrondie = round($row['inscritConcours']);
	
	$msg .= "<tr><td align=center>";
	if ($noteArrondie == $precNote)
		$msg .= "ex-aequo";
	else {
		$msg .= $classement;
		$classementDecal = $classement;
	}
	
	$msg .= "</td><td ".nl2br(tooltip("<img src=images/joueurs/".$row['joueur_id']."_bonus.jpg style=\"max-width:400px;\">")).">".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td>";
	$msg .= "<td align=center>".$noteArrondie."</td>";
	$msg .= "<td><a href=\"images/joueurs/".$row['joueur_id']."_bonus.jpg\" title=\"Clique pour voir le dessin en grand\" target=_blank><img src=\"images/joueurs/".$row['joueur_id']."_bonus.jpg\" align=absmiddle height=50></a></td></tr>";

	$classement ++;
	$precNote = $noteArrondie;
}
$dbHT->sql_freeresult($result);
$msg .= "</table><br/>";

$msg .= "<div align=center>Si tu ne retrouves pas ton nom, c'est que nous n'avons reçu ton dessin (contacte vite l'Hamster Academy dans ce cas)...</div>" ;
$msg .= "</div>";

echo $msg;

?>