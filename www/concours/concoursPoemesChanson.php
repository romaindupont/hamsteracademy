<?php 
define('MODE_AUCUN',0);
define('MODE_EN_COURS',1);
define('MODE_EN_ATTENTE',3);
define('MODE_RESULTAT',2);

$modeConcours = MODE_RESULTAT ;

if ($modeConcours == MODE_RESULTAT) {
	

	$msg .= "<div class=ham1Nouvelle><h2 align=center>Résultats du concours du meilleur poème</h2>";
	
	$query = "SELECT pseudo, joueur_id, poeme, notePoeme FROM lst_poemes_concours WHERE notePoeme != 'P' AND notePoeme != '-1' ORDER BY notePoeme DESC";
	
	$msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"Coupe\"></div><br/>";
	$msg .= "<div align=center><strong>Félicitations au vainqueur Lumina ! Elle gagne 1000 pièces et un diplôme !</strong><br/>&nbsp;<br/>Tous les participants recevront leurs cadeaux bientôt...<br/>&nbsp;<br/></div>";

	if ( !($result = $dbHT->sql_query($query)) ){
		message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
	}
	
	$msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>Classement</strong></td><td><strong>Nom du joueur</strong></td><td align=center><strong>Note / 20</strong></td></tr>";
	$classement = 1;
	$classementDecal = 1;
	$precNote = -1;
	while ($row=$dbHT->sql_fetchrow($result)) {
		
		$noteArrondie = round(10 + $row['notePoeme']);
		
		$msg .= "<tr><td align=center>";
		if ($noteArrondie == $precNote)
			$msg .= "ex-aequo";
		else {
			$msg .= $classement;
			$classementDecal = $classement;
		}
			
		
		$msg .= "</td><td ".nl2br(tooltip(trim($row['poeme']))).">".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td><td align=center>".$noteArrondie."</td></tr>";

		// envoi du cadeau
		if ($userdata['pseudo'] == "HamsterAcademy") {
			$nb_pieces_crediter = 0;
			if ($classementDecal == 1) {
				$nb_pieces_crediter = 1000;
			}
			else if ($classementDecal == 2) {
				$nb_pieces_crediter = 600;
			}
			else if ($classementDecal == 3) {
				$nb_pieces_crediter = 300;
			}
			else if ($classementDecal < 11) {
				$nb_pieces_crediter = 100;
			}
			else
				$nb_pieces_crediter = 20;
				
			//$message_texte = "Félicitations ".$row['pseudo']." ! Tu es arrivé(e) n°".$classementDecal." au concours du meilleur poème. Pour te recompenser, tu reçois ".$nb_pieces_crediter." pièces ! Bonne continuation dans le jeu !";
			//crediterPiecesBDD($row['joueur_id'],$nb_pieces_crediter) ;
			//envoyerMessagePrive($row['joueur_id'], $message_texte, HAMSTER_ACADEMY_ID, true) ;
			//echo $message_texte." <br/>";
		}
		
		$classement ++;
		$precNote = $noteArrondie;
	}
	$dbHT->sql_freeresult($result);
	$msg .= "</table><br/>";
	
	$msg .= "<div align=center>Si tu ne retrouves pas ton nom, c'est que ton poème existe déjà sur Internet...</div>" ;
	
	$msg .= "</div>";
	
	$msg .= "<hr></br><div class=ham1Nouvelle><h2 align=center>Résultats du concours de la meilleure chanson</h2>";
	
	$query = "SELECT pseudo, joueur_id, chanson, noteChanson FROM lst_poemes_concours WHERE noteChanson != 'P' AND noteChanson != '-1' ORDER BY noteChanson DESC";
	
	$msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"Coupe\"></div><br/>";
	$msg .= "<div align=center><strong>Félicitations au vainqueur Malou ! Elle gagne 1000 pièces et un diplôme !</strong><br/>&nbsp;<br/></div>";

	if ( !($result = $dbHT->sql_query($query)) ){
		message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
	}
	
	$msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>Classement</strong></td><td><strong>Nom du joueur</strong></td><td align=center><strong>Note / 20</strong></td></tr>";
	$classement = 1;
	$classementDecal = 1;
	$precNote = -1;
	while ($row=$dbHT->sql_fetchrow($result)) {
		
		$noteArrondie = round(10 + $row['noteChanson']);
		
		$msg .= "<tr><td align=center>";
		if ($noteArrondie == $precNote)
			$msg .= "ex-aequo";
		else {
			$msg .= $classement;
			$classementDecal = $classement;
		}
		
		$msg .= "</td><td ".nl2br(tooltip(trim($row['chanson']))).">".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td><td align=center>".$noteArrondie."</td></tr>";

		// envoi du cadeau
		if ($userdata['pseudo'] == "HamsterAcademy") {
			$nb_pieces_crediter = 0;
			if ($classementDecal == 1) {
				$nb_pieces_crediter = 1000;
			}
			else if ($classementDecal == 2) {
				$nb_pieces_crediter = 600;
			}
			else if ($classementDecal == 3) {
				$nb_pieces_crediter = 300;
			}
			else if ($classementDecal < 11) {
				$nb_pieces_crediter = 100;
			}
			else
				$nb_pieces_crediter = 20;
				
			//$message_texte = "Félicitations ".$row['pseudo']." ! Tu es arrivé(e) n°".$classementDecal." au concours de la meilleure chanson. Pour te recompenser, tu reçois ".$nb_pieces_crediter." pièces ! Bonne continuation dans le jeu !";
			//crediterPiecesBDD($row['joueur_id'],$nb_pieces_crediter) ;
			//envoyerMessagePrive($row['joueur_id'], $message_texte, HAMSTER_ACADEMY_ID, true) ;
		//	echo $message_texte." <br/>";
		}

		$classement ++;
		$precNote = $noteArrondie;
	}
	$dbHT->sql_freeresult($result);
	$msg .= "</table><br/>";
	
	$msg .= "<div align=center>Si tu ne retrouves pas ton nom, c'est que ta chanson existe déjà sur Internet...</div>" ;
	
	$msg .= "</div>";	
}
// gestion de l'inscription au concours
else if ($modeConcours == MODE_EN_COURS) {
	if ($userdata['inscritConcours'] >= 1) {
		
		if ( ! (isset($_GET['saisir_poeme_chanson']) || isset($_POST['saisir_poeme_chanson'])) ) {
			$msg .= "<div class=ham1Nouvelle><u>Concours</u> : Attention, ce sont les derniers jours pour envoyer ton poème ou ta chanson si ce n'est pas déjà fait ! <br/> Tu peux écrire ton poème/chanson (ou revoir ton poème/chanson déjà écrit). Pour cela, clique sur le lien ci-dessous : <br/>&nbsp;<br/>=> <a href=\"jeu.php?mode=m_concours&saisir_poeme_chanson=1\">Envoyer son poème et/ou sa chanson</a></div>";
		}
		else{
			
			$poeme = "Ton poème ci-après :";
			$chanson = "Ta chanson ci-après :";
			
			if (isset($_POST['validerPoeme'])) {
				
				// on récupère le poème et la chanson
				if (isset($_POST['poeme']))
					$poeme = mysql_real_escape_string(stripslashes($_POST['poeme'])) ;
				if (isset($_POST['chanson']))
					$chanson =mysql_real_escape_string(stripslashes($_POST['chanson'])) ;
				
				// on enregistre le poeme / chanson
				$query = "SELECT * FROM lst_poemes_concours WHERE joueur_id=".$userdata['joueur_id'];
				if ( !($result = $dbHT->sql_query($query)) ){
					message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
				}
				$nbJoueurs = $dbHT->sql_numrows($result) ;
				if ($nbJoueurs == 0) {
					// le joueur n'a pas encore saisi son poème, on l'insère
					$query = "INSERT INTO lst_poemes_concours VALUES ( ".$userdata['joueur_id']." , '".$userdata['pseudo']."' , '".$poeme."' , '".$chanson."') ";
					$result = $dbHT->sql_query($query);
				}
				else {
					// le joueur a juste modifié son poème, on l'insère
					$query = "UPDATE lst_poemes_concours SET poeme = '".$poeme."' , chanson =  '".$chanson."' WHERE joueur_id = ".$userdata['joueur_id'] ;
					$result = $dbHT->sql_query($query);
				}
				$msg .= "<span class=txtOk><img src=\"images/smiley_smile.gif\" align=absmiddle> Ton poème/chanson a bien été enregistré(e) ! Tu peux les modifier jusqu'au 25 janvier si tu le souhaites.<br/>&nbsp;<br/></span>";
			}
			
			$query = "SELECT * FROM lst_poemes_concours WHERE joueur_id=".$userdata['joueur_id'];
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
			}
			$nbJoueurs = $dbHT->sql_numrows($result) ;
			if ($nbJoueurs == 1) {
				$row=$dbHT->sql_fetchrow($result);
				$poeme = $row['poeme'];
				$chanson = $row['chanson'];
			}
			$dbHT->sql_freeresult($result);
			
			$msg .= "<div class=ham1Nouvelle><table><tr><td><img src=\"images/regles.gif\" align=absmiddle></td><td><u>Concours</u> : Pour envoyer ton poème et/ou ta chanson, tu dois les écrire ci-dessous. Si ça ne marche pas, envoie un mail à <a href=\"mailto:contact@hamsteracademy.fr\">contact@hamsteracademy.fr</a>.</td></tr></table><br/>&nbsp;<br/>";
			$msg .= "<form method=POST action=\"jeu.php\">";
			$msg .= "<input type=hidden name=mode value=m_concours>";
			$msg .= "<input type=hidden name=saisir_poeme_chanson value=1>";
			$msg .= "Saisis ci-dessous ton poème (facultatif) : <br/>";
			$msg .= "<textarea name=poeme cols=80 rows=30>".$poeme."</textarea><br/>&nbsp;<br/>";
			$msg .= "Saisis ci-dessous ta chanson (facultatif) : <br/>";
			$msg .= "<textarea name=chanson cols=80 rows=30>".$chanson."</textarea><br/>&nbsp;<br/>";
			$msg .= "<input type=submit name=validerPoeme value=\"Envoyer le poème et/ou la chanson\"> (tu pourras les modifier plus tard si tu veux les compléter ou les modifier)(jusqu'au 25 janvier)";
			$msg .= "</form>";
			$msg .= "</div>";
		}
	}
	else if ($action != "inscrireConcours") {
		//$msg .= "<div class=ham1Nouvelle><strong>L'inscription au concours est terminé</strong>. Les votes ont commencé (fin le 20 décembre). Il y aura un cadeau pour chaque joueur qui aura voté !</div>" ;
		
		$msg .= "<div class=ham1Nouvelle><strong>Appel aux joueurs de Hamster Academy ! :-)</strong><br/>&nbsp;<br/><u>Le concours du plus beau poème ou de la plus belle chanson est ouvert</u>&nbsp;! Tu as jusqu'au 25 janvier pour t'inscrire et écrire ton poème ou ta chanson, dépêche-toi si tu veux participer&nbsp;!<br/>&nbsp;<br/>" ;
		$msg .= "Toutes les règles du jeu sont dans le forum à cette adresse : <a href=\"http://www.hamsteracademy.fr/forum/viewtopic.php?id=214\" title=\"Voir les règles du concours\">lien</a>.<br/>";
		$msg .= "&nbsp;<br/><u>Petit résumé</u> : <br>- Le vainqueur aura 1000 pièces et un diplôme par courrier. Le deuxième aura 600 pièces. Les 100 premiers joueurs auront 20 pièces.<br/>";
		$msg .= "- Coût d'inscription au concours : 5 pièces. Bonne chance !<br/>";
		$msg .= "- On peut s'inscrire jusqu'au dernier moment.<br/>&nbsp;<br/>";
		$msg .= "<table cellpadding = 0 cellspacing = 0><tr valign=baseline><td>Je m'inscris (5 ".IMG_PIECE.") :&nbsp;&nbsp;</td><td><form action=\"jeu.php\" method=get><input type=hidden name=mode value=m_accueil><input type=hidden name=action value=inscrireConcours><input type=submit name=inscriptionConcours value=\"S'inscire au concours !\"></form></td></tr></table></div>";
	}
}
else if ($modeConcours == MODE_EN_ATTENTE) {
	$msg .= "<div class=ham1Nouvelle><strong>Concours</strong> : c'est fini pour envoyer les poèmes et chansons ! Nous commençons à lire vos poèmes et nous proclamerons les résultats bientôt (dans quelques jours) !</div>";
}

?>