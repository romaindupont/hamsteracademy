<?php 

if ( !defined('IN_HT') )
{
	die("Hacking attempt");
}

if ($userdata['inscritConcours'] >= 1) {
		
	if ( ! (isset($_GET['saisir_slogan']) || isset($_POST['saisir_slogan'])) ) {
		echo "<div class=ham1Nouvelle><br/>&nbsp;<br/>=> <a href=\"jeu.php?mode=m_concours&saisir_slogan=1\">Ecrire et envoyer son ou ses slogans (ou les modifier)</a></div>";
	}
	else{
		
		$slogan_fun = "Ton slogan fun :";
		$slogan_debile = "Ton slogan débile ci-après :";
		
		if (isset($_POST['validerSlogan'])) {
			
			// on récupère le poème et la chanson
			if (isset($_POST['slogan_fun']))
				$slogan_fun = mysql_real_escape_string(stripslashes($_POST['slogan_fun'])) ;
			if (isset($_POST['slogan_debile']))
				$slogan_debile =mysql_real_escape_string(stripslashes($_POST['slogan_debile'])) ;
			
			// on enregistre les slogans
			$query = "SELECT * FROM lst_slogans_concours WHERE joueur_id=".$userdata['joueur_id'];
			if ( !($result = $dbHT->sql_query($query)) ){
				message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
			}
			$nbJoueurs = $dbHT->sql_numrows($result) ;
			if ($nbJoueurs == 0) {
				// le joueur n'a pas encore saisi son slogan, on l'insère
				$query = "INSERT INTO lst_slogans_concours VALUES ( ".$userdata['joueur_id']." , '".$userdata['pseudo']."' , '".$slogan_fun."' , '".$slogan_debile."',-1,-1) ";
				$result = $dbHT->sql_query($query);
			}
			else {
				// le joueur a juste modifié son slogan, on l'insère
				$query = "UPDATE lst_slogans_concours SET slogan_fun = '".$slogan_fun."' , slogan_debile =  '".$slogan_debile."' WHERE joueur_id = ".$userdata['joueur_id'] ;
				$result = $dbHT->sql_query($query);
			}
			echo "<br/>&nbsp;<br/><span class=txtOk><img src=\"images/smiley_smile.gif\" align=absmiddle> Tes slogans ont bien été enregistrés ! Tu peux les modifier jusqu'au 10 juillet si tu le souhaites.<br/>&nbsp;<br/></span>";
		}
		
		$query = "SELECT * FROM lst_slogans_concours WHERE joueur_id=".$userdata['joueur_id'];
		if ( !($result = $dbHT->sql_query($query)) ){
			message_die(GENERAL_ERROR, 'Error in obtaining cage_data', '', __LINE__, __FILE__, $query);
		}
		$nbJoueurs = $dbHT->sql_numrows($result) ;
		if ($nbJoueurs == 1) {
			$row=$dbHT->sql_fetchrow($result);
			$slogan_fun = $row['slogan_fun'];
			$slogan_debile = $row['slogan_debile'];
		}
		$dbHT->sql_freeresult($result);
		
		echo "<div class=ham1Nouvelle><table><tr><td><img src=\"images/regles.gif\" align=absmiddle></td><td><u>Concours</u> : Pour envoyer tes slogans, tu dois les écrire ci-dessous. Si ça ne marche pas, envoie un mail à <a href=\"mailto:concours@hamsteracademy.fr\">contact@hamsteracademy.fr</a>.</td></tr></table><br/>&nbsp;<br/>";
		echo"<form method=POST action=\"jeu.php\">";
		echo "<input type=hidden name=mode value=m_concours>";
		echo "<input type=hidden name=saisir_slogan value=1>";
		echo "Saisis ci-dessous ton slogan fun / original (facultatif) : <br/>";
		echo "<textarea name=slogan_fun cols=80 rows=5>".$slogan_fun."</textarea><br/>&nbsp;<br/>";
		echo "Saisis ci-dessous ton slogan débile (facultatif) : <br/>";
		echo "<textarea name=slogan_debile cols=80 rows=5>".$slogan_debile."</textarea><br/>&nbsp;<br/>";
		echo "<input type=submit name=validerSlogan value=\"Envoyer les slogans\"> (tu pourras les modifier plus tard si tu veux les compléter ou les modifier)(jusqu'au 10 juillet)";
		echo "</form>";
		echo "</div>";
	}
}
?>