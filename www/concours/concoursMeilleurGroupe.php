<?php 
	

$msg .= "<div class=ham1Nouvelle><h2 align=center>Résultats du concours du meilleur groupe</h2>";

$query = "SELECT j.joueur_id, j.pseudo, g.groupe_id, g.note, h.hamster_id FROM joueurs j, groupes g, hamster h WHERE j.joueur_id = h.joueur_id AND h.groupe_id = g.groupe_id AND inscritConcours > 0 GROUP BY j.joueur_id ORDER BY g.note DESC";


$msg .="<div align=center><img src=\"images/coupe.gif\" alt=\"Coupe\"></div><br/>";
$msg .= "<div align=center><strong>Félicitations aux vainqueurs Lis et Dude ! Ils gagnent 500 pièces chacun et un diplôme !</strong><br/>&nbsp;<br/></div>";

if ( !($result = $dbHT->sql_query($query)) ){
	message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
}

$msg .= "<table cellpadding=5 align=center><tr><td align=center><strong>Classement</strong></td><td><strong>Nom du joueur</strong></td><td align=center><strong>Nombre de points du groupe</strong></td></tr>";
$classement = 1;
$classementDecal = 0;
$precNote = -1;
while ($row=$dbHT->sql_fetchrow($result)) {
	
	$noteArrondie = $row['note'];
	
	$msg .= "<tr><td align=center>";
	if ($noteArrondie == $precNote)
		$msg .= "ex-aequo";
	else {
		$classementDecal ++;
		$msg .= $classementDecal;
		
	}
	
	$msg .= "</td><td>".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td>";
	$msg .= "<td align=center>".$noteArrondie."</td>";
	$msg .= "</tr>";

	$classement ++;
	$precNote = $noteArrondie;
}
$dbHT->sql_freeresult($result);
$msg .= "</table><br/>";

$msg .= "</div>";

echo $msg;

?>