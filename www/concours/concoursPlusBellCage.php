<?php 
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

$modeConcours = MODE_AUCUN ;

function noteConcoursCage($noteVotes,$noteAccessoires,$meilleurVote,$meilleurAccessoires) {
    
    $meilleurScore = $meilleurVote+$meilleurAccessoires/2;
    
    $noteArrondie = round( 5+( (($noteVotes+$noteAccessoires/2) * 5) / $meilleurScore), 1 );
    
    return $noteArrondie;
}

if ($day >= 2 && $day <= 7 || ($day == 1 && $heure > 2))
    $modeConcours = MODE_INSCRIPTION_PARTICIPATION ;
else if ($day >= 8 && $day <= 14)
    $modeConcours = MODE_VOTE ;
else
    $modeConcours = MODE_RESULTAT ;


if ($modeConcours == MODE_RESULTAT) {
    
    if (! isset($_GET['voir_resultat'])) {
        echo "<div align=center><a href=\"jeu.php?mode=m_concours&voir_resultat=1\">".T_("Voir les résultats du concours !")." </a></div>" ;
    }
    else {
    
        // faut-il envoyer les cadeaux ?
        $queryConfig = "SELECT valeurInt FROM config WHERE parametre = 'cadeauConcoursEnvoye'";
        $joueur_exp_id = 1371; // HamsterAcademy
        
        if ( !($resultConfig = $dbHT->sql_query($queryConfig)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $queryConfig);
        }
        
        $rowConfig = $dbHT->sql_fetchrow($resultConfig);
        $envoyerCadeau = false;
        if ($rowConfig['valeurInt'] == 1) {
            // si oui, on met le tag à envoyer les cadeaux et on change la valeur cadeauConcoursEnvoye pour ne pas envoyer 2 fois les cadeaux !
            $envoyerCadeau = true;
            
            $queryEnvoyerCadeau = "UPDATE config SET valeurInt = 0 WHERE parametre = 'cadeauConcoursEnvoye' ";
            if ( ! $dbHT->sql_query($queryEnvoyerCadeau) ){
                message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $queryEnvoyerCadeau);
            }
        }
        $dbHT->sql_freeresult($resultConfig);
        
        echo "<div class=ham1Nouvelle><h2 align=center>".T_("Résultats du concours de la plus belle cage")."</h2>";
        
        $query = "SELECT l.*, j.joueur_id, j.pseudo FROM lst_cages_concours l, joueurs j WHERE j.joueur_id = l.joueur_proprietaire_id ORDER BY (l.note + nb_accessoires/2) DESC";
        
        if ( !($result = $dbHT->sql_query($query)) ){
            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query);
        }
        
        $nbParticipants = $dbHT->sql_numrows($result) ;
        $rowVainqueur=$dbHT->sql_fetchrow($result);
        $rowSecond=$dbHT->sql_fetchrow($result);
        
        echo "<div align=center><img src=\"images/coupe.gif\" alt=\"".T_("Coupe")."\"><br/>&nbsp;<br/>";
        echo "<strong>".T_("Félicitations au vainqueur")." ".$rowVainqueur['pseudo']." ! ".T_("Il gagne 400 pièces et un diplôme !")."</strong><br/>&nbsp;<br/>".$rowSecond['pseudo']." ".T_("est second et gagne 200 pièces ! Voici le classement pour tous les joueurs !")."</div>";

        if (!isset($_GET['tous_les_joueurs'])) {
            echo "<h3 align=center>".T_("Les 100 premiers")."</h3>";
            $query .= " LIMIT 100";
        }
        else
            echo "<h3 align=center>".T_("Tous les joueurs")."</h3>";

        echo "<table cellpadding=5 align=center><tr><td align=center><strong>".T_("Classement")."</strong></td><td><strong>".T_("Nom du joueur")."</strong></td><td align=center><strong>".T_("Voir sa cage")."</strong></td><td align=center><strong>".T_("Note")." / 10</strong></td></tr>";
        
        $classement = 1;
        $precNote = -1;
        $dernierClassementNonEx = 1;
        
        for($participant=0;$participant<$nbParticipants;$participant++){
        
            if ($participant == 0)
                $row = $rowVainqueur;
            else if ($participant == 1)
                $row = $rowSecond;
            else
                $row=$dbHT->sql_fetchrow($result);
            
            $noteArrondie = noteConcoursCage($row['note'],$row['nb_accessoires'],$rowVainqueur['note'],$rowVainqueur['nb_accessoires']);
            $nb_pieces_donnees=0;
            if ($noteArrondie != $precNote){
                $dernierClassementNonEx = $classement;
            }
            
            echo "<tr><td align=center>";
            if ($noteArrondie == $precNote)
                echo T_("ex-aequo");
            else
                echo $classement;            
            
            echo "</td><td>".returnLienProfil($row['joueur_id'],tronquerTxt($row['pseudo'],20))."</td><td align=center>".returnLienCage($row['cage_id'],T_("Voir sa cage"))."</td><td align=center>".$noteArrondie."</td></tr>";

            // on envoie les cadeaux si demandé
            if ($envoyerCadeau) {
                
                if ($dernierClassementNonEx == 1)
                    $nb_pieces_donnees = 400;
                else if ($dernierClassementNonEx >= 2 && $dernierClassementNonEx < 11)
                    $nb_pieces_donnees = 200;
                else if ($dernierClassementNonEx > 10 && $dernierClassementNonEx < 101)
                    $nb_pieces_donnees = 50;
                else
                    $nb_pieces_donnees = 0;
                    
                $nb_pieces_donnees = round($nb_pieces_donnees,0);
                if ($nb_pieces_donnees > 0) {
                    echo $classement." : " .$row['pseudo']." ".T_("a reçu")." ".$nb_pieces_donnees." ".T_("pieces").". ".returnLienCage($row['cage_id'],T_("Voir sa cage !"))."<br/>\n" ;
                    //crediterPiecesBDD($row['joueur_id'],$nb_pieces_donnees);
                    envoyerMessagePrive($row['joueur_id'], T_("Résultat du concours ! Tu es")." ".$classement." ".T_("au classement du concours de la plus belle cage ! Bravo ! En récompense, tu gagnes")." ".$nb_pieces_donnees." ".T_("pièces !"), $joueur_exp_id, false);
                }
                else{
                    echo $classement." : " .$row['pseudo']." ".T_("a reçu des bonbons").". ".returnLienCage($row['cage_id'],T_("Voir sa cage !"))."<br/>\n" ;
                    // petit cadeau : 5 bonbons
                    $type = ACC_BONBON ;
                    //ajouterAccessoireDansBDD($type, $row['joueur_id'], -1, -1, -1, -1, 5);
                    envoyerMessagePrive($row['joueur_id'], T_("Résultat du concours ! Tu es")." ".$classement." ".T_("au classement du concours de la plus belle cage. En récompense de ta participation au concours, tu gagnes 5 bonbons pour ton ou tes hamster(s) ! A bientôt pour un nouveau concours où tu pourras retenter ta chance !"), $joueur_exp_id, false);
                }
            }
            
            $classement ++;
            $precNote = $noteArrondie;
        }
        $dbHT->sql_freeresult($result);
        echo "</table><br/>";
        
        echo "<div align=center><a href=\"jeu.php?mode=m_concours&tous_les_joueurs=1&voir_resultat=1\">".T_("Voir le classement de tous les autres joueurs")."</a></div>" ;
        
        echo "</div>";
    }
}

// gestion de l'inscription / participation au concours
// ----------------------------------------------------
else if ($modeConcours == MODE_INSCRIPTION_PARTICIPATION) {

    if ($userdata['inscritConcours'] == 0){
        echo "<div><table><tr valign=baseline><td valign=baseline><strong>".T_("Je m'inscris")."</strong> (20 ".IMG_PIECE.") :&nbsp;&nbsp;</td><td valign=baseline><form action=\"jeu.php\" method=get><input type=hidden name=mode value=m_concours><input type=hidden name=action value=inscrireConcours><input type=submit name=inscriptionConcours value=\"".T_("S'inscire au concours !")."\"></form></td></tr></table></div>";
    }
    else {
        echo "<div><strong>".T_("Tu es inscrit au concours !")."</strong> ".T_("Tu as jusqu'au 7")." ".$mois_txt[$month]." ".T_("inclus pour décorer ta cage !");
        
        // pas de cage choisie alors que le joueur en possède plusieurs
        if ($userdata['inscritConcours'] == 1 && $nbCages > 1) {
            echo "<div><br/><b>".T_("Tu as plusieurs cages :")." </b><u>".T_("choisis ta cage pour le concours")."</u> (".T_("clique sur son nom) (attention, tu ne pourras pas changer)")." : ";
            for($cage=0;$cage<$nbCages;$cage++) {
                echo "<br/><a href=\"jeu.php?mode=m_concours&action=inscrireCageConcours&cage_id=".$lst_cages[$cage]['cage_id']."\" title=\"".T_("Choisir cette cage")."\">".$lst_cages[$cage]['nom']."</a>";
            }
        }
        if ($userdata['inscritConcours'] > 1 && $nbCages > 1) {
            echo "<br/>&nbsp;<br/>La cage </i> ".$lst_cages[cageCorrespondante($userdata['inscritConcours'])]['nom']."</i> ".T_("a été sélectionnée pour le concours !");
        }
        echo "</div>";
    }
}

else if ($modeConcours == MODE_VOTE) {
    
    if ($userdata['inscritConcours'] > 1 && $nbCages > 1) {
        $cageIndex = cageCorrespondante($userdata['inscritConcours']);
        if ($cageIndex == -1){ // bug qu'on essaie de corriger ici....
             $query = "UPDATE joueurs SET inscritConcours = ".$lst_cages[0]['cage_id']." WHERE joueur_id=".$userdata['joueur_id'];
             $dbHT->sql_query($query);
             $cageIndex = 0;
        }
            
        echo "<br/>La cage </i> ".$lst_cages[$cageIndex]['nom']."</i> ".T_("a été sélectionnée pour le concours !")."<br/>&nbsp;<br/>";
    }
    
    // gestion du vote pour le concours
    // --------------------------------
    if ($userdata['a_vote'] == 1) {
        echo "<div class=ham1Nouvelle><u>".T_("Concours")."</u> <strong>".T_("Ton vote pour le concours")."</strong> ".T_("a été pris en compte ! L'Hamster Academy te remercie (tu auras 1 cadeau à la fin du concours)").".<br/>&nbsp;<br/>".T_("Les résultats de tous les votes seront disponibles le 15")." ".$mois_txt[$month]." !</div>";
    }
    else {
        if (!isset($_GET['vote'])) {
            //  on propose au joueur de voter
            echo "<div class=ham1Nouvelle>
            <strong>".T_("Le vote pour le concours de la plus belle cage</strong> est lancé ! Tu as jusqu'au 14")." ".$mois_txt[$month]." ".T_("(inclus) pour voter").".<br/><div align=\"center\"><br/>=> <a href=\"jeu.php?mode=m_concours&vote=1\">".T_("Vote maintenant : clique ici !")."</a> <=</div><br/>".T_("Bon à savoir :")." <strong>".T_("chaque joueur ayant voté aura un cadeau")."</strong> !</div>";
        }
        else {
            
            // le joueur a voté ou veut le faire
            
            // est-ce que le joueur vient de voter ?
            $bulletin_valide = 0;
            if (isset($_GET['a_vote']) && ! isset($_GET['validPseudo1']) && ! isset($_GET['validPseudo2']) ) {
                
                // on récupère la liste des cages à voter
                $msg = " ";
                require "concoursLstCages.php";
                
                // liste des cages votées
                while($row=$dbHT->sql_fetchrow($result)) {
                    $noteCage = 0;
                    
                    if (isset($_GET['cageNotee_'.$row['cage_id'].''])) {
                        $noteCage = intval($_GET['cageNotee_'.$row['cage_id'].'']);
                        if ($noteCage < 0)
                            $noteCage = 0;
                        else if ($noteCage > 5)
                            $noteCage = 5;
                        
                        // on récupère la précédente note de la cage
                        $query15 = "SELECT cage_id, note FROM lst_cages_concours WHERE cage_id=".$row['cage_id']." LIMIT 1" ;
                        if ( !($result15 = $dbHT->sql_query($query15)) ){
                            message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query15);
                        }
                        if ($dbHT->sql_numrows($result15) == 1){
                            $row15=$dbHT->sql_fetchrow($result15);
                            $nouvelleNoteCage = $row15['note'] + $noteCage ;
                            $dbHT->sql_freeresult($result15);
                            
                            // mise à jour de la note
                            $query16 = "UPDATE lst_cages_concours SET note = ".$nouvelleNoteCage." WHERE cage_id=".$row15['cage_id']." LIMIT 1" ;
                            if ( !($dbHT->sql_query($query16)) ){
                                message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query16);
                            }
                        }
                    }
                }
                
                $bulletin_valide = 1;
                
                echo "<strong>".T_("Ton vote a bien été pris en compte !")."</strong> <br/>&nbsp;<br/>".T_("Pour te remercier, tu auras un cadeau dès que le vote sera terminé le 14")." ".$mois_txt[$month]."." ;
                $query16 = "UPDATE joueurs SET a_vote = 1 WHERE joueur_id=".$userdata['joueur_id']." LIMIT 1" ;
                if ( !($dbHT->sql_query($query16)) ){
                    message_die(GENERAL_ERROR, 'Error ', '', __LINE__, __FILE__, $query16);
                }
                echo "<br/>&nbsp;<br/><div align=center><a href=\"jeu.php?mode=m_concours\">".T_("Clique-ici pour revenir au jeu")."</a></div>";
            }
            
            if ($bulletin_valide == 0 || ! isset($_GET['a_vote']) || isset($_GET['validPseudo1']) || isset($_GET['validPseudo2'])) { // si le joueur n'a pas encore voté, ou s'il y a un pb ou un chgt de nom
            
                require "graphique.php" ;    
                echo "<h2 align=center>".T_("Ton bulletin de vote pour le concours")."</h2>";
                $msg = " ";
                
                // on récupère la liste des cages à voter
                require "concoursLstCages.php";
                
                echo "<div align=center>".T_("Donne une note pour ces")." ".$nbCages." ".T_("cages")." : </div><br/>";
                
                // début du formulaire
                echo "<form action=\"jeu.php\" method=get>";
                echo "<input type=hidden name=mode value=m_concours>";
                echo "<input type=hidden name=vote value=1>";
                echo "<input type=hidden name=a_vote value=1>";
                
                // on affiche la cage i
                while($row=$dbHT->sql_fetchrow($result)) {
                    $row['proprete']=10;
                    
                    // liste des accessoires pour cette cage i
                    // ---------------------------------------
                    $query5 = "SELECT a.*, c.* FROM accessoires a, config_accessoires c WHERE a.cage_id=".$row['cage_id']." AND a.type = c.type";
                    if ( !($result5 = $dbHT->sql_query($query5)) ){
                        message_die(GENERAL_ERROR, 'Error in obtaining accessoires_data', '', __LINE__, __FILE__, $query5);
                    }
                    $nbAccessoires = $dbHT->sql_numrows($result5) ;
                    $lstAccessoiresDeLaCage=array();
                    $row['nbAccessoires'] = 0;
                    while($row5=$dbHT->sql_fetchrow($result5)) {
                        array_push($lstAccessoiresDeLaCage,$row5);
                        $row['nbAccessoires'] ++ ;
                    }
                    $dbHT->sql_freeresult($result5);
                    
                    echo afficherCage($row, false, $lstAccessoiresDeLaCage, $nbAccessoires) ;
                    
                    echo "<br><div align=\"center\">Note (<span style=\"font-size:9pt;\">1 = ".T_("pas terrible").",..., 5 = ".T_("superbe cage")."</span>) : ";
                    echo "<select name=cageNotee_".$row['cage_id'].">";
                    $optionDejaSelectionnee = -1;
                    if (isset($_GET['cageNotee_'.$row['cage_id'].'']))
                        $optionDejaSelectionnee = intval($_GET['cageNotee_'.$row['cage_id'].'']);
                    
                    echo "<option value=\"1\" ".( ($optionDejaSelectionnee == 1)? "selected" : "" )." >1</option>\n";
                    echo "<option value=\"2\" ".( ($optionDejaSelectionnee == 2)? "selected" : "" )." >2</option>\n";
                    echo "<option value=\"3\" ".( ($optionDejaSelectionnee == 3)? "selected" : "" )." >3</option>\n";
                    echo "<option value=\"4\" ".( ($optionDejaSelectionnee == 4)? "selected" : "" )." >4</option>\n";
                    echo "<option value=\"5\" ".( ($optionDejaSelectionnee == 5)? "selected" : "" )." >5</option>\n";
                    echo "</select></div>";
                    echo "<div align=center><strong>".$row['nom']."</strong></div>";
                    echo "<hr/>";
                    echo "<br/>";
                }
                $dbHT->sql_freeresult($result);
                
                echo T_("Tu peux ajouter une cage de ton choix à la liste. Il faut indiquer le pseudo du joueur qui possède cette cage (à condition qu'il soit inscrit au concours bien sûr)")." :<br/>" ;
                if ($pseudoCageSupp1 != "")
                    echo T_("Actuellement, c'est la cage de")." <strong>".$pseudoCageSupp1. "</strong> ".T_("que tu as choisie. Si tu veux changer la cage, choisis un autre joueur dont tu veux noter sa cage")." : ";
                echo "<input type=text size=50 name=pseudoCageSupp1 value=\"".$pseudoCageSupp1."\"><input type=submit name=validPseudo1 value=\"".T_("Ajouter la cage de ce joueur")."\"><br/>&nbsp;<br/>";
                
                echo T_("Tu peux ajouter une deuxième cage de ton choix à la liste. Il faut indiquer le pseudo du joueur qui possède cette cage (à condition qu'il soit inscrit au concours bien sûr)")." :<br/>" ;
                if ($pseudoCageSupp2 != "")
                    echo T_("Actuellement, c'est la cage de <strong>".$pseudoCageSupp2. "</strong> que tu as choisie. Si tu veux changer la cage, choisis un autre joueur dont tu veux noter sa cage")." : ";
                echo "<input type=text size=50 name=pseudoCageSupp2 value=\"".$pseudoCageSupp2."\"><input type=submit name=validPseudo2 value=\"".T_("Ajouter la cage de ce joueur")."\"><br/>";
                
                echo "<hr/><br/>";
                echo "<div align=center><strong>".T_("Ne valide le vote que si tu as choisi les deux cages supplémentaires (non obligatoire) et, surtout, que tu les a toutes notées !")."</strong></div>";
                echo "<br><div align=center><input type=submit name=validerBulletin value=\"".T_("Valider le bulletin de vote")."\"></div>";
                echo "</form>";
                echo "<br/>&nbsp;<br/>";
            }
        }
    }
}
