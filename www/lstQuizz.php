<?php
if ( !defined('IN_HT') )
{
    die("Hacking attempt");
}

// quizz = 1 question, 4 réponses possibles (dont une "conne"), le numéro de la bonne réponse et un commentaire éventuel sur la réponse
$lstQuizz = array(
0 => array("Quel hamster possède le meilleur sens de
l'orientation","L'hamster Sauvage","L'hamster Doré","L'hamster
Nain","L'hamster Lost",1,""),
1 => array("Quel hamster possède la gestation (grossesse) la plus
courte","L'hamster Sauvage","L'hamster doré de Syrie","L'hamster nain de
Russie","L'hamsteroïde",2,"La gestation dure ... 16 jours max !"),
2 => array("De quelle famille sont issus les hamsters","La famille des
Morts-vivants","La famille des Geomyidés","La famille des Pedetidés","La
famille des Muridés",4,""),
3 => array("D'où vient le mot \"hamster\"","Un peu plus de 1000 ans qu'une
centaine d'historiens se sont penchés dessus, et on ne sait toujours pas ...
","De l'anglais","De l'allemand","D'Amsterdam",3,"Le mot \"hamster\" vient
du mot allemand <i>hamstern</i> qui signifie <i>faire des réserves</i>."),
4 => array("Comment s'appellent les poches que possèdent l'hamster au niveau
des joues pour stocker sa nourriture","Les abajoues","Les abajours","Les
abrajoues","Les arbajoues",1,""),
5 => array("Combien de temps dure la gestation (grossesse)","Entre 5 et 10
jours","Entre 11 et 15 jours","Entre 16 et 25 jours","Entre 26 et 35
jours",3,"La grossesse dure en moyenne 18 jours et varient selon les espèces
(jusqu'à 21 jours pour l'hamster chinois)"),
6 => array("Quel est le poids du bébé hamster à la naissance (pour le
hamster doré)","1 gr", "2 à 5 gr","6 à 10 gr","11 à 15 gr",2,""),
7 => array("Combien de petits peut avoir un hamster (pour le hamster
doré)","1 à 3","3 à 6","5 à 10","7 à 20",3,""),
8 => array("Au bout de combien de mois un hamster doré peut avoir des
bébés","1","2","3","4",2,""),
9 => array("Au bout de combien de mois un hamster russe peut avoir des
bébés","1","2","3","4",1,""),
10 => array("L'hamster est un animal ... ","solitaire","exclusivement
carnivore","spécialiste dans l'utilisation d'outils","amateur de
musique",1,""),
11 => array("Quel est le poids moyen d'un hamster doré","60 à 80 gr","80 à
120 gr","120 à 180 gr","1,2 à 3 kilos",3,""),
12 => array("Qui est un cousin de l'hamster","Le gorille","Le cobaye","Le
castor","Le furet",2,""),
13 => array("Le hamster est un animal","Plutôt du matin","Nocturne","Diurne
(vit le jour)","Fan de coucher de soleil",2,""),
14 => array("Le hamster mange, chaque jour","5 gr de nourriture","10 gr de
nourriture","15 gr de nourriture","20 gr de nourriture",3,""),
15 => array("Le hamster mange parfois des animaux vivants. Quel est sa
préférence","Des fourmis !","Des petits vers","Des petits scorpions","Des
rats !",2,""),
16 => array("Combien de temps vit un hamster","environ 1 an","1 an et
demi","2 à 3 ans","pas moins de 94 608 000 secondes !",3,""),
17 => array("Quelle est la température idéal pour le hamster","Comme nous :
autour de 20-25 degrés","Plutôt plus frais, autour des 15 à 20
degrés","Plutôt chaud, entre 25 et 30 degrés","Par -10 degrés",1,""),
18 => array("Où a été découvert le hamster","Dans la région du Cap en
Afrique du Sud","A Paris en France","Dans la région d'Alep en Syrie","Dans
la région de New York au US",3,""),
19 => array("Comment se traduit Hamster en
anglais","Hamster","Hamstair","Rodent","Woodnut",1,"Comme en français !"),
20 => array("En quelle année a été découvert le
hamster","1810","1790","1904","1932",4,""),
21 => array("Quelle compagnie d'aviation a été obligé de faire une escale
imprévue à cause d'un hamster évadé","Air France","China Airline","Austrian
Airline","Pegasus Airline",3,"AFP : Un appareil de la compagnie Austrian
Airlines a été contraint à une escale imprévue vendredi après qu'un passager
ait laissé échapper un hamster.Le vol en provenance de Palma de Majorque et
à destination de la ville autrichienne de Graz a dû faire escale à Innsbruck
pour partir à sa recherche et s'assurer que l'animal n'avait rongé aucun
câble, précise la compagnie dans un communiqué.Le vol a été dérouté
lorsqu'un passager a informé l'équipage de la disparition du rongeur. Les
passagers ont évacué l'avion et certains ont été transportés en bus jusqu'à
Graz. En milieu d'après-midi, le hamster n'avait toujours pas été
retrouvé.La compagnie a souligné que l'appareil resterait cloué au sol
jusqu'à ce que l'animal soit retrouvé par mesure de sécurité."),
22 => array("Combien existe-t-il de races de hamster","3","5","7","9",2,"les
hamsters syriens, les hamsters nains russes, les hamsters nains de
Roborovsky, les hamsters nains de Campbell, les hamsters nains chinois."),
23 => array("Quels sont les 4 types de pelage les plus courants ?","angora,
satin, poil ras, rex","angora, satin, nu, poil ras","poil ras, nu, rex,
angora","nu, poil ras, angora, satin",1,""),
24 => array("Si mon hamster a le nez humide, cela signifie ","Qu’il est en
bonne santé","Qu’il a froid","Qu’il est malade","Qu’il vient de
boire",1,""),
25 => array("Combien de petits dans une même portée un hamster doré peut-il
avoir au maximum","6","8","10","12",3,""),
26 => array("La femelle qui vient d'accoucher possède","Entre 2 et 5 paires
de mamelles","Entre 5 et 7 paires de mamelles","Entre 7 à 11 paires de
mamelles","Aucune paire",3,""),
27 => array("Pour une bonne alimentation du hamster, il faut lui
donner","des pommes de terre","des fruits trop mûrs ","des courgettes","des
oignons",3,""),
28 => array("Les hamsters communiquent entre eux à l'aide","de
sifflements","d'ultrasons et petits cris","de ses pattes","des vibrations de
leur moustache",2,""),
29 => array("Quelle proposition correspond le mieux au hamster","le hamster
a une vue très sensible et une ouïe développée","le hamster a une vue assez
faible et un odorat très développé","le hamster est sourd muet","le hamster
a une vue très performante mais est quasiment sourd",2,""),
30 => array("Combien de grammes de nourriture un hamster peut-il conserver
dans ses abajoues","1 gramme","6 grammes","12 grammes","18 grammes",4,""),
31 => array("En 2007 un film sorti au cinéma raconte l'histoire d'un petit
rongeur qui cuisine pour un grand restaurant parisien, celui-ci est","un
rat","un hamster","un cochon d'inde ","une souris",1,"Voir le film
Ratatouille !"),
32 => array("Quel film sorti en 2007 raconte l'histoire d'un petit rongeur
qui devient le plus grand cuisinier de tous les temps?","Aubergines","Souris
au pot","Ratatouille","Hamsternouille",3,""),
33 => array("Quelle est la race de hamster qui est la souvent considérée
comme celle d'un animal domenstique ?","Le hamster syrien","Le hamster
russe","Le hamster domesticus","Le phodopus",1,"Le hamster syrien est la
race la plus courante en tant qu'animal domestique. Elle comprend la hamster
doré, hamster au poil ras, le hamster crème..."),
34 => array("Qu'est-ce le phodopus ?","Une race de hamster","L'organe du
hamster qui lui permet d'avoir des abajoues très souples","Une maladie qui
touche les hamsters sauvages","Un champignon qui décolore la peau du
hamster",1,"Le phodopus est une race de hamsters nains à queue courte, on
les rencontre souvent élevés en captivité."),
35 => array("Où trouve-t-on le plus de hamsters sauvages ?","En
Pantagonie","Chez Truffaut","En Europe du Sud / Asie mineure","En
Australie",3,"Les premiers hamsters ont été capturés en Syrie autour de
1930. Ils vivent dans les zones semi-arides au sud de l'Europe et en Asie
mineure"),
36 => array("Les hamsters sont-ils de bons animaux domestiques ?","Non, car
ils sont petits et bon courage pour les retrouver sous les meubles !","Oui,
car ils sont propres et gentils.","Non, car ils font du bruit toute la nuit
!","Non, car il mange tout ce qu'il trouve, y compris la moquette toute
neuve !",2,"Le hamster est un animal adorable ! Mais il est fragile et
demande une attention de tous les jours."),
37 => array("Quelle est la particularité des dents du hamster ?","Elles sont
au nombre de 18","Elles poussent sans arrêt","Elles tombent une vingtaine de
fois durant la vie du hamster","Le hamster doit brosser ses dents entre
chaque graîne, bonjour les caries sinon",2,"Les dents poussent non-stop,
c'est pour ça qu'il faut donner une pierre à ronger, des bouts de bois,
etc."),
38 => array("Quel est le nombre idéal de hamster par cage
?","0","1","2","plus il y a de fous, plus on rit !",2,"Les hamsters sont des
animaux solitaires ! Un par cage est l'idéal !"),
39 => array("Si Coco mon hamster est plus jeune que Cracotte de 6 mois, et
que Cracotte est née il y a 15 mois, quel âge a mon hamster Coco ?","6
mois","7 mois","8 mois","9 mois",4,""),
40 => array("Si j'ai 5 hamsters dont 2 femelles qui ont chacune 4 bébés,
combien de hamsters il y a-t-il dans la cage ?","7","9","11","13",4,""),
41 => array("J'achète un objet 5 pièces et je le revends 7 pièces. Puis je
le rachète 5 pièces et le revends 7 pièces. Combien ai-je gagner ?","2
pièces","4 pièces","6 pièces","8 pièces",2,""),
42 => array("J'ai 4 cages avec 2 hamsters dans chacune. 3 femelles donnent
naissance à 2 bébés chacune, et 1 hamster s'est échappé de la cage. Combien
reste-t-il de hamsters dans les cages?","12 hamsters","13 hamsters","14
hamsters","15 hamsters",2,""),
43 => array("Avec mes 200 pièces j'achète une cage à 50 pièces, deux
hamsters à 5 pièces, et 3 maisons à 20 pièces, combien de pièces me
reste-til?","80 pièces","100 pièces","120 pièces","140 pièces",1,""),
44 => array("Combien de joueurs sont inscrits en ce moment à Hamster
Academy?","1 joueur c'est toi !","1000 joueurs et c'est déjà pas mal
!","environ 40 000 joueurs","1 000 000 de joueurs c'est énorme !",3,""),
45 => array("Combien de joueurs au maximum peut contenir un groupe à
l'Academy?","5","6","7","8",4,""),
46 => array("Quelle sortie n'existe pas dans Hamster Academy ?","La sortie
tennis","La sortie piscine","La sortie football","La sortie rugby",2,""),
47 => array("En moyenne combien de joueurs se connectent sur Hamster Academy
en 10 minutes ?","1 joueur","10 joueurs","100 joueurs","10 000
joueurs",3,""),
48 => array("Que veut dire VIP en anglais ?","Very Important Person","Very
Interesting People","Vrut Inch Progh","J'en sais rien c'est de l'anglais !
(attention ce n'est pas la bonne réponse)",1,""),
49 => array("Quel cours n'existe pas à l'Academy
?","Musique","Chant","Mathématiques","Danse",3,""),
50 => array("Si j'ai 5/10 en Chant, 4/10 en Piano et 9/10 en Musique, quelle
est ma moyenne générale (chaque épreuve à le même coefficient) ?","0/10
c'est nul !","2/10... peut mieux faire","4/10 c'est presque la
moyenne","6/10 ça commence à être pas mal !",4,""),
51 => array("Combien de métiers peut-on choisir dans Hamster Academy
?","Entre 5 et 9","Entre 10 et 14","Entre 15 et 18","Plus de 19",4,"")
);

$nbQuizz = sizeof($lstQuizz);

define ('QUIZZ_QUESTION',0) ;
define ('QUIZZ_REPONSE1',1) ;
define ('QUIZZ_REPONSE2',2) ;
define ('QUIZZ_REPONSE3',3) ;
define ('QUIZZ_REPONSE4',4) ;
define ('QUIZZ_BONNE_REPONSE',5) ;
define ('QUIZZ_COMMENTAIRE',6) ;
?>