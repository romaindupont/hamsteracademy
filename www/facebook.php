<?php

if ( !defined('IN_HT') )
{
    die("Hacking attempt");
    exit;
}

require 'facebook/facebook-php-sdk/src/facebook.php';

if ($lang == "fr") {
    define('CLE_PUBLIQUE', '103152776403315');
    define('CLE_SECRETE', '54d85b7d6159814ca838d16a6f616573');
}
else {
    define('CLE_PUBLIQUE', '76e7b577fd18b10d2b4a12914c1b37a1');
    define('CLE_SECRETE', '6dae8fe68c293e7aeba5cfc969c570da');   
}

// on cr�� une intance de facebook
$facebook = new Facebook(array(
    'appId'  => CLE_PUBLIQUE,
    'secret' => CLE_SECRETE,
  'cookie' => true,
));

function get_facebook_cookie($app_id, $application_secret) {
  $args = array();
  parse_str(trim($_COOKIE['fbs_' . $app_id], '\\"'), $args);
  ksort($args);
  $payload = '';
  foreach ($args as $key => $value) {
    if ($key != 'sig') {
      $payload .= $key . '=' . $value;
    }
  }
  if (md5($payload . $application_secret) != $args['sig']) {
    return null;
  }
  return $args;
}

$cookie = get_facebook_cookie(CLE_PUBLIQUE, CLE_SECRETE);


// on r�cup�re les identifiants du visiteur
$sessionFacebook = $facebook->getSession();

?>