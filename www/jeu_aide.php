<?php 
if ( !defined('IN_HT') )
{
	die("Hacking attempt");
	exit;
}

if (defined('IN_HT_AIDE_PHP')) {
	require "gestion.php";
}

$univers=0;
if (isset($_GET['univers'])) {
	$univers = intval($_GET['univers']);
	if ($univers > 3)
		$univers = 0;
	else if ($univers < 0)
		$univers = 0;
}

if ($msg != "")
	echo "<div>".$msg."</div>";

?>

<div class="aideSection">
	<div id="header">
	  <ul>
	  	<?php 
	  	if (defined('IN_HT_AIDE_PHP')) {
	  		?>
	  	<li class="menuPrincipal" <?php if ($univers == 0) echo "id=\"current\"" ; ?>><a href="aide.php?univers=0">Univers de l'Eleveur</a></li>
	  	<li class="menuPrincipal" <?php if ($univers == 1) echo "id=\"current\"" ; ?>><a href="aide.php?univers=1">Univers de l'Academy</a></li>
	  	<li class="menuPrincipal" <?php if ($univers == 2) echo "id=\"current\"" ; ?>><a href="aide.php?univers=2">Questions générales</a></li>
	  <?php 
	  }
	  else {
	  	?>
	  	<li class="menuPrincipal" <?php if ($univers == 0) echo "id=\"current\"" ; ?>><a href="jeu.php?mode=m_aide&univers=0">Univers de l'Eleveur</a></li>
	  	<li class="menuPrincipal" <?php if ($univers == 1) echo "id=\"current\"" ; ?>><a href="jeu.php?mode=m_aide&univers=1">Univers de l'Academy</a></li>
	  	<li class="menuPrincipal" <?php if ($univers == 2) echo "id=\"current\"" ; ?>><a href="jeu.php?mode=m_aide&univers=2">Questions générales</a></li>
	  <?php 
	  } 
	  ?>	  		  	
		</ul>  		
	</div>
	<br/>&nbsp;<br/>
</div>

<?php 
if ($univers == 0) {
?>

<div class="aideSection">
		<div class="aideTitreSection">Principe général du jeu</div>
		Il y a plusieurs objectifs dans le jeu	: 
		<ul>
			<li>Elever un ou plusieurs hamsters</li>
			<li>Construire la plus belle cage</li>
			<li>Etre parmi les meilleurs de l'Hamster Academy !</li>
		</ul>
		Tu peux évoluer dans deux univers différents : l'univers des Eleveurs, et l'univers de l'Academy. 
Dans <a href="aide.php?univers=0">l'Univers des Eleveurs</a>, tu dois principalement t'occuper de tes hamsters et de leur état général (santé, moral, force, poids et beauté). 
Dès le début du jeu, tu peux choisir un métier qui te permettra de gagner de l'argent et de t'occuper de ton (ou de tes) hamster(s). 
Quand tu as atteint un certain niveau d'expérience, ton hamster peut accéder à l'<a href="aide.php?univers=1">Univers de l'Academy</a> et ton hamster se transforme en véritable star. L'objectif est de devenir le meilleur et tu peux pour cela prendre des cours de musique, de danse, de chant etc. Tu peux ensuite évoluer dans ton propre groupe de musique, rencontrer des amis pour jouer avec toi dans le groupe, te spécialiser dans un style musical.
Ces deux univers sont bien sûr liés. Plus ton hamster sera en bonne santé, meilleur il sera dans l'Academy. Donc l'éleveur ne doit surtout pas négliger la santé du hamster s'il veut qu'il devienne un jour le meilleur Hamster Académicien.
</div>

<div class="aideSection">
		<div class="aideTitreSection">L'Univers des Eleveurs</div>
	      <p>C'est l'Univers où tout le monde commence : c'est dans cet univers que tu dois élever ton hamster et construire sa cage. Pour pouvoir évoluer dans le jeu tu as dès le début un salaire de base (7pièces/jour) qui te permet de faire les dépenses courantes. Tu es ensuite libre de t'inscrire à une formation qui te permettra d'avoir un métier et ainsi gagner plus d'argent pour pouvoir acheter tout ce dont tu auras besoin pour ton hamster. </p>
		  <p align="center"><a href="images/demo1.png"><img src="images/demo1_reduc.png" border="0" alt="demo" /></a></p>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Quelle est la monnaie ? </div>
		  <div class="aideReponse">La monnaie locale est la pièce d'or <?php echo IMG_PIECE ; ?>. Tous les jours, chaque éleveur gagne au moins 7 <?php echo IMG_PIECE; ?>. Le jeu te permet de t'inscrire ensuite à une formation, qui selon le métier que tu choisiras, te permettra de gagner encore plus de pièces. Si le métier que tu as choisis ne te suffit pas, tu peux à tout moment aller dans la partie En Ville et choisir un nouveau métier.
		  <br>Il faut savoir qu'au-delà de 3 semaines d'absence de l'éleveur, le salaire n'est plus versé.
		  </div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment donner à boire au hamster ?</a></div><div class="aideReponse">Pour que ton hamster puisse boire, il suffit de mettre un biberon dans sa cage. C'est indispensable sinon le hamster tombe vite malade, sa santé chute très rapidement. Dès l'achat de ta cage, il faut absolument acheter un biberon et le mettre dans la cage.</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment nourrir le hamster ?</a></div><div class="aideReponse">Pour nourrir le hamster, il faut qu'il y ait une écuelle dans la cage et des portions de nourriture en stock. Il faut faire en sorte que le hamster n'ait pas faim et que son écuelle ne soit jamais complètement vide, sinon sa santé et son moral diminuent. Nourrir et donner à boire au hamster sont les deux actions les plus importantes à ne jamais oublier !</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment acheter des objets ?</a></div><div class="aideReponse">Tout au long du jeu, tu as la possibilité d'acheter des objets, aussi bien pour la cage que pour l'hamster. Pour cela, il faut aller En Ville puis dans La Boutique <img src="images/boutique.gif" alt="" align="absmiddle" alt="boutique" /> . Tu peux ensuite te balader dans les rayons et choisir tes objets, pour la cage, pour le hamster, des articles de sport etc. Mais attention, pour pouvoir accéder à La Boutique, il faut être au niveau 2. Pour cela, il faudra que tu réussisses les objectifs qui te sont demandés : 1) donner à manger au hamster et 2) changer les copeaux (voir la page d'accueil <img src="images/accueil.gif" alt="" align="absmiddle" alt="accueil" />). Certains objets comme les instruments de musique ne sont disponibles qu'à partir d'un certain niveau dans le jeu.</div>

		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment demander au hamster de construire son nid ?</a></div>
		  	<div class="aideReponse"><img src="images/nid.gif" align=absmiddle alt="" width="50" /> Une des premières missions proposée au joueur consiste à construire son nid. Le hamster aura besoin de 3 éléments pour y arriver :
		  		<ul>
		  			<li>de la paille</li>
		  			<li>du coton</li>
		  			<li>des brindilles</li>
		  		</ul>
		  		Pour te procurer tous ces matériaux, il faut te rendre En Ville et accéder à La Boutique. Dès que tu les auras donnés au hamster, il pourra commencer à construire son nid! La durée de construction dépend de ses capacités de bâtisseur (un des caractères au choix lors de l'inscription). La durée peut varier de 24 heures à 1 semaine. Pour accélérer la construction du nid et pouvoir passer plus rapidement au niveau supérieur, il est aussi possible d'acheter une formation bâtisseur ou d'acheter un code Allopass (dans ce cas la construction est ultra rapide).
		  		</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment demander au hamster de construire une cabane ?</a></div>
		  	<div class="aideReponse"><img src="images/maison2.gif" align=absmiddle alt="" width="50" /> La construction de la cabane fonctionne comme la construction du nid. Tu as besoin de 2 choses :
		  		<ul>
		  			<li>des rondins de bois</li>
		  			<li>un plan de construction</li>
		  		</ul>
		  		Tu pourras trouver tous ces éléments dans La Boutique (il faut un plan de construction par hamster mais il peut le garder à vie) et les donner au hamster. La construction de la cabane commence aussitôt ! La durée de construction dépend également des capacités de bâtisseur du hamster.
		  		</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment nettoyer ma cage ?</a></div><div class="aideReponse">
		  	<img src="images/odeurs.gif" align=absmiddle alt="cage sale (odeurs)" > Chaque jour, les hamsters salissent un peu leur cage. C'est pourquoi il faut la nettoyer régulièrement! Tu pourras trouver des copeaux de bois dans La Boutique, il suffit d'un litre pour changer une fois les copeaux. Pour l'hygiène du hamster, il est important de les changer au moins une fois par semaine. Une cage sale favorise l'apparition de maladies et fait baisser l'état de santé du hamster. Plus il y a de hamsters dans la même cage, plus celle-ci se salit vite, donc plus il faut être vigilent ! Pour connaître la propreté de la cage, il y a une barre d'évolution sur la page Ma Cage, dans la partie Son Etat. Si celle-ci commence à devenir rouge, c'est qu'il faut changer les copeaux.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment améliorer la santé du hamster ?</a></div><div class="aideReponse">La santé du hamster évolue en fonction de nombreux paramètres. Les trois actions de base à savoir nourrir, donner à boire et changer les copeaux te permettent de conserver ton hamster dans un bon état. Si tu veux que ton hamster soit dans une très bonne forme, il existe de nombreux articles dans La Boutique qui te permettent d'améliorer sa santé. C'est le cas par exemple des vitamines. Il faut savoir aussi qu'un hamster qui pratique de la roue ou d'autres types d'activités physiques sera plus facilement dans un meilleur état de santé. Certains aliments comme la salade sont très bon pour la santé du hamster. En plus il adore ça !
Mais il faut faire attention à bien s'occuper du hamster. Si sa santé devient proche de 0, il sera envoyé chez le vétérinaire pour recevoir des soins intensifs. Cela lui évite de mourir, mais l'inconvénient pour l'éleveur est que ces soins coûtent très cher si tu veux ensuite récupérer le hamster.<br/>
		  	Si ton hamster a une mauvaise santé, pense à vérifier que :
		  	<ul>
		  		<li>son écuelle n'est pas vide</li>
		  		<li>il y a un biberon dans sa cage</li>
		  		<li>les copeaux sont propres</li>
		  		<li>sa force physique est suffisante</li>
		  	</ul>
		
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment améliorer le moral du hamster ?</a></div><div class="aideReponse">Si ton hamster a un moral faible, cela signifie qu'il n'est pas heureux dans sa cage. Les principaux éléments qui affectent le moral du hamster sont causés par:
		  	<ul>
		  		<li>une écuelle vide</li>
		  		<li>une mauvaise santé</li>
		  		<li>trop d'hamsters présents dans la même cage (le hamster est un animal solitaire)</li>
		  		<li>l'absence de biberon</li>
		  		<li>le manque de soins (caresse, crème de soins...)</li>
		  		<li>...</li>		  	
		  	</ul>
		  	Si tu veux améliorer le moral de ton hamster, pense à bien vérifier les éléments cités plus haut. Il est important de savoir que plus tu divertis ton hamster, plus il sera content. Tu peux donc offrir à ton hamster un stage de plein-air ou une sortie (rugby par exemple). Son moral gagne quelques points d'un coup !
		  	</div>
		  </div>

 		<div class="aideSousSection">
		  <div class="aideQuestion">Comment améliorer la beauté du hamster ?</a></div><div class="aideReponse">Un des paramètres importants de l'état général du hamster est son aspect esthétique. Cela a de l'importance surtout pour l'univers de l'Academy. Plus ton hamster est beau, plus il est populaire ! Il est dont indispensable de caresser régulièrement le hamster, de le brosser et de lui appliquer une crème de soin pour le pelage. La propreté de la cage influe sur sa beauté, un hamster qui vit dans une cage sale ne peut pas être propre. Les articles de beauté proposés En Ville te permettent d'améliorer la beauté de ton hamster.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment rendre son hamster sportif ?</a></div><div class="aideReponse">Il est très important pour la santé et pour le moral de ton hamster de lui faire faire du sport. Sa force physique en dépend ! Pour prendre du muscle, le hamster doit faire du sport et il a plusieurs possibilités :
		  	<ul>
		  		<li>faire de la roue dans sa cage</li>
		  		<li> faire une séance de musculation (il faut lui acheter des haltères à La Boutique) 
					<li> lui acheter des accessoires (vélo, balançoire en vois, boule...).	Mais attention, il ne faut pas qu'il fasse trop de sport, et plusieurs heures de repos sont nécessaires entre chaque séance de musculation !</li>
		  	</ul>
		  	Ne pas oublier de lui donner à manger et à boire en quantité suffisante. Attention, la roue n'est pas à effet immédiat : il faut plusieurs jours pour que l'hamster se muscle avec la roue. Par contre, avec les haltères, les résultats sont immédiats.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment promener mon hamster ?</a></div>
		  	<div class="aideReponse">
		  		<img src="images/foret.gif" align=absmiddle alt="Promenade"> Pour pouvoir promener son hamster, il faut acheter une laisse pour hamster dans La Boutique (accessible seulement aux éleveurs de niveau 4). Il est alors possible de promener son hamster une fois par jour maximum. La balade en forêt se passe généralement très bien et le hamster adore ça car il retrouve son milieu naturel (le moral augmente). Parfois il peut arriver que ton hamster trouve un gland qu'il peut conserver dans ses abajoues et ramène jusque dans sa cage (l'éleveur économise des graines). Mais parfois, il peut aussi tomber sur des champignons vénéneux ou des microbes et attraper une maladie... Il faut alors vite le soigner !
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="poids">Mon hamster est trop maigre ou trop gros, que faire ?</a></div><div class="aideReponse">Le poids idéal pour le hamster est 120 gr. Si son poids est trop faible (inférieur à 105 gr) ou trop élevé (supérieur à 135 gr), sa santé, sa forme physique et sa beauté se dégradent. S'il est trop maigre, donne-lui à manger plus souvent et fais-lui faire du sport (pour qu'il se muscle) plus régulièrement. S'il grossit trop vite, vérifie qu'il possède bien une roue dans sa cage et qu'il fait suffisamment de sport, donne lui moins à manger et achète un maximum d'articles de sport.</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="poids">Mon hamster est vraiment trop gros, je n'arrive pas à faire baisser son poids...</a></div>
		  <div class="aideReponse">Il existe un moyen un peu tordu de faire perdre tous les kilos en trop au hamster d'un coup, sans utiliser de pilules amincissantes. Mais on vous laisse déviner comment y arriver ! Juste un indice : il faut lui faire de la roue, mais si possible, pas avec n'importe quelle roue, plutôt une roue trafiquée... (possible à partir du niveau 3)</div>
		  </div>		  
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="metier">Changer de métier ?</a></div><div class="aideReponse">Si tu ne gagnes pas assez de pièces, tu as la possibilité de changer de métier. Cela te permet d'augmenter tes revenus quotidiens. Seuls les éleveurs au niveau 3 peuvent le faire. Il faut aussi suivre une formation (payante) pour apprendre le nouveau métier que tu as choisi. Plus la formation coûte cher, plus le salaire sera élevé. Il est donc intéressant de payer plus cher au début pour être sûr de gagner beaucoup d'argent ensuite. Pour changer de métier, il faut aller En Ville et ensuite cliquer sur l'onglet <a href="jeu.php?mode=m_metier"><img src="images/monmetier.png" align=absmiddle></a></div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment déplacer un objet dans la cage</div><div class="aideReponse">La construction de la cage est un élément important du jeu. Chaque éleveur est libre de la personnaliser autant qu'il veut. Tu peux ajouter des étages, agrandir leur surface, changer la couleur et ajouter tous les objets que tu souhaites. <?php 
		  	//echo afficherObjet("oeufs",4); 
		  	?> Pour déplacer un objet  dans la cage d'un étage à l'autre, il suffit de cliquer sur l'objet à déplacer (sans relâcher la souris) et de le déplacer à l'endroit de la cage où tu veux qu'il apparaisse. Tu peux faire ca uniquement d'étage en étage. Si tu as du mal comme ca, tu peux également utiliser les flèches qui apparaissent dans le cadran à droite dans l'atelier. <img src=images/haut.gif><img src=images/bas.gif>. Il n'est pas possible de mettre des objets hors de la cage. Si tu veux enlever un objet de la cage, il faut aller dans Mon Inventaire, cliquer sur l'objet à enlever et le déplacer dans le bac fourre-tout. </div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment déplacer un objet d'une cage à une autre</div><div class="aideReponse">Si tu veux déplacer un objet entre deux cages, il faut aller dans le menu Mon Inventaire, cliquer sur l'objet à déplacer et sélectionner la cage dans laquelle tu veux le mettre à l'aide du bandeau déroulant. 
		  </div>
			</div>

		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="plusdargent">Pas assez de pièces en poche ? Que faire ?</a></div><div class="aideReponse">Pour gagner de l'argent il existe plusieurs solutions dan le jeu. Tu peux déjà attendre de recevoir ton prochain salaire (versé toutes les 24 heures, soit une fois par jour). Sinon, pour gagner du temps, il y a aussi d'autres possibilités :
		  	<ol>
		  		<li>jouer au <a href="quizz.php?pagePrecedente=m_aide">quizz</a> (questionnaire sur les hamsters et un seul essai par jour)</li>
		  		<li>vendre des objets (il faut aller dans Mon Inventaire puis choisir l'objet à vendre)</li>
		  		<li>demander au hamster de fabriquer un nid (ou une cabane) puis le revendre. Le prix de revente est supérieur au prix d'achat des matériaux (paille, coton, bois etc.)</li>
		  		<li>parrainer un ami, c'est-à-dire l'inviter à jouer à Hamster Academy : ça te rapporte 20 <?php echo IMG_PIECE; ?> par ami inscrit. Pour parrainer, il faut aller sur la page <a href="parrainage.php">parrainage</a>, sinon tout est expliqué <a href="#parrainage">ici</a></li>
					<li>acheter des pièces dans La Boutique au rayon Banque, tu recevras 300 pièces (<a href="banque.php">Aller à la Banque !</a>).</li>
		  	</ol>
		  	</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Mon hamster ne veut pas faire de roue...</a></div><div class="aideReponse">
		  	<img src="images/roue3.gif" align=absmiddle alt="" width=50> A partir du moment où le hamster possède une roue dans sa cage, il en fait forcément. Les effets sur la force musculaire ne sont pas instantanés (contrairement aux haltères) et il faut attendre plusieurs jours pour en voir les bénéfices. 
		  	</div>
		  </div>	
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Mon hamster a rongé les barreaux de la cage... comment éviter ça ?</a></div><div class="aideReponse">
		  	<img src="images/pierre_ronger.gif" align=absmiddle alt="" width=50> Tous les hamsters ont l'habitude ronger les barreaux de leur cage. Leurs dents poussent toue leur vie donc ca leur permet de les user. Pour éviter que ton hamster ronge tes barreaux et que tu doives repeindre la cage à chaque fois, tu as la possibilité d'acheter dans La Boutique une pierre à ronger pour que le hamster lime ses dents. Il en faut une par cage pour que les barreaux ne soient plus rongés.<br>Note : la pierre à ronger n'est disponible pour les éleveurs qu'à partir du niveau 4.
		  	</div>
		  </div>		  	  
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Je pars une semaine en vacances, comment je fais pour surveiller et m'occuper de mes hamsters ?</a></div><div class="aideReponse">
		  	<img src="images/refuge.gif" align=absmiddle alt="refuge">Si tu n'as pas le temps de t'occuper de tes hamsters pendant quelques jours ou quelques semaines, tu peux les déposer au Refuge. Le prix est de 10 <?php echo IMG_PIECE; ?> pour tous les hamsters. Ils seront alors nourris et logés aussi longtemps que tu le souhaites et tu les récupères dans le même état de santé et de moral qu'avant de partir. Pour déposer tes hamsters au refuge, il faut aller dans l'Accueil et cliquer dans la partie Le Refuge.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment avoir des bébés ?</a></div><div class="aideReponse">
		  	<img src="images/couffin.gif" align=absmiddle alt="couffin" width=70>
		  	Comme tout animal de compagnie, le hamster peut avoir des bébés dès que son âge dépasse 1 mois. On ne peut faire reproduire que les hamsters femelles. On doit alors leur choisir un hamster mâle au sein de l'Hamster Academy pour pouvoir les accoupler. La grossesse dure 1 semaine environ et le nombre de petits est compris entre 1 et 3, aussi bien des mâles que des femelles. Leurs caractéristiques génétiques dépendent de celles de leurs parents.
		  	Attention ! Une fois les bébés nés, il faut s'en occuper ! Hors de question de les laisser mourir de faim, de soif ... Il est vivement conseillé de leur acheter une nouvelle cage pour ne pas qu'il passe leur temps à se disputer.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment jouer au rugby ?</a></div><div class="aideReponse">
		  	<img src="images/ballon_rugby.gif" align=absmiddle alt="rugby"  width=50> Jouer au rugby est une activité physique importante qui permet à l'éleveur d'améliorer la force et le moral du hamster. Faire un sport en groupe est toujours bon pour le moral ! Pour que le hamster puisse jouer avec d'autres hamsters, il a besoin de 2 choses : 
		  	<ul>
		  		<li>un ballon de rugby : tu peux en trouver dans La Boutique.</li>
		  		<li>une sortie de rugby : la sortie est payante, il faut l'acheter dans La Boutique. Il faut payer l'organisation, la location du terrain et les frais de santé (on se blesse souvent au rugby). Une sortie n'est valable qu'une seule fois. Si tu veux améliorer l'état de santé, de moral et de force du hamster, il est vivement conseillé de multiplier ce genre de sorties... A renouveler sans modération, tant qu'il reste des pièces dans le portefeuille !</li>
		  		</ul>
		  		Nb : la sortie au rugby n'est possible que le <u>samedi</u>.
		  	</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment jouer au football ?</a></div><div class="aideReponse">
		  	<img src="images/ballon_foot.gif" align=absmiddle alt="foot"  width=50> Jouer au football est une activité physique ludique et bon pour la forme. Pour que le hamster puisse jouer avec d'autres hamsters, il a besoin de 2 choses : 
		  	<ul>
		  		<li>un ballon de foot : tu peux en trouver dans La Boutique.</li>
		  		<li>une sortie au club de foot : la sortie est payante, il faut l'acheter dans La Boutique. Car, comme pour le rugby, il faut payer l'organisation, la location du terrain et les frais de santé (on se blesse souvent au foot : foulures, crampes etc.). Une sortie n'est valable qu'une seule fois. Si tu veux améliorer l'état de santé, de moral et de force du hamster, il est vivement conseillé de multiplier ce genre de sorties... A renouveler sans modération, tant qu'il reste des pièces dans le portefeuille !</li>
		  		</ul>
		  		Nb : la sortie au rugby n'est possible que le <u>mercredi</u> et le <u>dimanche</u>.
		  	</div>
		  </div>		  

		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="agenda">Quel est l'agenda de la semaine ?</a></div><div class="aideReponse">
		  	<img src="images/agenda.gif" align=absmiddle alt="agenda"> Certaines activités ne sont possibles que certains jours de la semaine. Les sorties possibles sont&nbsp;:
		  	<ul>
		  		<li>Lundi : sortie dans la boule - musculation</li>
		  		<li>Mardi : promenade en forêt</li>
		  		<li>Mercredi : football - musculation - console de jeu</li>
		  		<li>Jeudi : sortie dans la boule</li>
		  		<li>Vendredi : promenade en forêt</li>
		  		<li>Samedi : rugby</li>
		  		<li>Dimanche : football - console de jeu</li>
		  	</ul>
		  	</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Le Calendrier de l'Avent, comment ça marche ?</a></div><div class="aideReponse">
		  	<img src="images/calendrier_avent_red.jpg" align=absmiddle alt="Calendrier de l'Avent" width=70>
		  	Pendant les fêtes de Noël tu peux acheter un calendrier de l'Avent. C'est la tradition à Noël, du 1er décembre au 24 décembre, chaque jour, il y a un chocolat à découvrir dans le Calendrier de l'Avent ! Les hamsters adorent ça et leur moral monte (mais pas le poids, c'est Noël !). Note : si tu as plusieurs hamsters il faut acheter un calendrier par hamster pour ne pas faire de jaloux !
		  	</div>
		  </div>
		  
  		<div class="aideSousSection">
		  <div class="aideQuestion">Je veux me séparer d'un ou plusieurs hamsters, est-ce possible ?</a></div><div class="aideReponse">
		  	Il est maintenant possible de te séparer d'un hamster si tu n'as plus le temps ou pas assez d'argent pour t'en occuper. Dans l'Accueil, tu pourras trouver un volet La SPH qui te permet d'y laisser un hamster. La SPH (Société Protectrice des Hamsters) est un organisme qui a pour but de s'occuper des hamsters des éleveurs qui ne peuvent pas s'en occuper assez bien. Une fois que tu as déposé ton hamster, tu n'as PLUS la possibilité de le récupérer, donc ce choix est définitif. Laisser son hamster à la SPH est payant, cela leur permet de pouvoir s'en occuper au début. <br/>Bon à savoir, la SPH n'est ouverte qu'aux joueurs ayant atteint le niveau 3 et possédant au moins 2 hamsters.
		  	</div>
		  </div>

  		<div class="aideSousSection">
		  <div class="aideQuestion">Peut-on vendre ses hamsters ?</a></div><div class="aideReponse">
		  	Oui, c'est possible ! Il faut cependant que les 4 conditions suivantes soient respectées :
		  	<ul>
		  		<li>que tu sois de niveau 4 minimum</li>
		  		<li>que le hamster soit né il y a plus de 3 semaines (il ne doit pas être séparé de sa mère trop jeune)</li>
		  		<li>qu'il ne soit pas au refuge</li>
		  		<li>et qu'il ne soit pas malade (s'il est chez le véto, personne ne l'achète...)</li>
		  	</ul>
		  	Si les 4 conditions sont respectées, tu peux vendre ton ou tes hamster(s) via la page d'Accueil, en bas à droite. Le prix de vente est fixé à <?php echo $prixVenteHamster. " ".IMG_PIECE ; ?> par hamster.
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Le dessin des cages est bizarre, ou ne marche pas ?</a></div><div class="aideReponse">
		  	Il peut arriver que cela ne marche pas. Il y a 2 solutions :
		  	<ol>
		  		<li>Rafraîchir complètement la page en appuyant sur la touche F5</li>
		  		<li>Si ça ne marche pas encore, le navigateur (Internet Explorer ou Firefox ou Safari ou Opéra) peut être à l'origine du problème. L'idéal est de changer de navigateur et Firefox fonctionne très bien. Sinon n'hésite pas à contacter l'Hamster Academy (contact@hamsteracademy.fr), elle est là pour ça et t'aidera à résoudre tous types de problèmes!</li>
		  	</ol>
		  	</div>
		  </div>
</div>

<?php
}
else if ($univers == 1) {
?>

<div class="aideSection">
		<div class="aideTitreSection">L'Univers de l'Academy</div>
<p> Cet univers fantastique dans lequel ton hamster évolue pendant que son éleveur dort, est une Academy dans laquelle  ton hamster devient une véritable star. Ton hamster évolue au sein de l'Academy et gagne de l'expérience en fonction de ses compétences en musique, en chant et en danse. Plus ton hamster est expérimenté, plus il est populaire et meilleur il sera. Le but final est de créer son groupe de musique, rock, jazz ou funk, il y en a pour tous les goûts. Alors prépare-toi à monter ton propre groupe, invite tes amis ou rencontre de nouveaux hamsters pour devenir le meilleur groupe que l'Academy ait connu ! </p>

  		<div class="aideSousSection">
		  <div class="aideQuestion">Comment intégrer l'Academy ?</div><div class="aideReponse"> L'univers de l'Academy est ouvert uniquement aux éleveurs de niveau 4. Une fois que tu as atteint ce niveau, tu as la possibilité d'y inscrire un ou plusieurs hamsters de ton choix. L'inscription coûte 5 PIECES par hamster.  </p>
				</ul>
		  </div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment être populaire ?</div><div class="aideReponse">La popularité est régulièrement évaluée en fonction des capacités en chant, danse et humour de chaque hamster. Plusieurs choses peuvent aussi faire évoluer la popularité :
		  	<ul>
		  		<li>réussir un examen</li>
		  		<li>raconter des blagues à ses camarades dans le couloir de l'Academy. Si le Directeur surprend le hamster en train de le faire alors que c'est interdit (voir le règlement intérieur), il se prend un savon. Mais les gens adorent ça, alors la popularité monte ! A l'inverse, si par malheur un camarade te "casse" avec une blague plus drôle (c'est presque l'humiliation...), la popularité baisse.</li>
<li>prendre des cours d'improvisation et apprendre des blagues est aussi un bon moyen de gagner en popularité auprès de tes copains et des tes professeurs. </li>
		  	</ul>
		  		<?php 
		  		//echo afficherObjet("oeufs",7); 
		  		?>
		  </div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment prendre des cours de danse ?</div><div class="aideReponse">
		  	<img src="images/danse.jpg" align=absmiddle alt="">Pour pouvoir prendre des cours de danse, il faut d'abord être Académicien Confirmé. Par conséquent, il faut d'abord réussir à passer ses examens. Ils ne prennent bien évidemment pas en compte les performances en danse tant que tu n'es pas Confirmé !<br>Il est proposé aux Académiciens de pratiquer deux types de danse : la danse de base (techniques générales) et la danse Tecktonik. Celle-ci coûte un peu plus cher mais rapporte plus de points et d'expérience (elle est assez difficile)!
		  </div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment fonctionne les examens ?</div><div class="aideReponse"> Tous les 3 jours, tu as le droit de participer à une nouvelle session d'examens. Tu as intérêts à t'entrainer entre chaque passage parce que le jury est sévère. Le jury est composé de 4 professeurs (un professeur de chant, un professeur de musique, un humoriste et le directeur de l'Academy) et chacun te note dans son domaine d'expertise.  
		  	Tes notes dépendent essentiellement du nombre de cours que tu as pris et des points d'expérience que tu as gagné. A chaque examen tu as la possibilité d'évoluer au niveau supérieur si la moyenne de tes notes est supérieure à ce qui est attendu. Par exemple il faut atteindre 10 de moyenne générale à un Académicien Débutant pour passer au statut de Confirmé. Mais attention, un hamster en mauvaise santé sera moins performant lors de son passage à l'examen donc il ne faut pas négliger le travail de l'éleveur !
		  	
		  	</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion">Où trouver le règlement intérieur ?</div><div class="aideReponse">Il est affiché à l'entrée de l'Academy à l'Accueil, là où se font les inscriptions. Les principales règles qui y sont évoquées sont :
		  	<ul>
		  		<li>L'inscription est obligatoire pour entrer à l'Academy</li>
		  		<li>Il est interdit de distraire ses camarades dans les couloirs de l'Academy (pas de discussion, pas de blagues, rien !). L'Academy est un lieu sérieux où tout le monde travaille pour devenir le meilleur.</li>
					<li>L'examen peut être passé quand le hamster le souhaite mais il doit y avoir 3 jours entre 2 passages d'examens. Il faut bien travailler avant !</li>
					<li>Il est interdit de draguer les camarades</li>
					<li>...</li>
		  </div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Qu'est-ce que l'expérience et comment en gagner ? </div><div class="aideReponse">Tout le temps que tu passes à l'Academy te fait gagner en expérience. A chaque fois que tu prends des cours, que tu racontes des blagues ou que tu passes un examen, des points sont ajoutés à ton expérience. Donc pour avoir un maximum d'expérience, motive toi pour prendre pleins de cours, devient un bon copain en racontant des blagues, soit bon aux examens. Avoir de l'expérience est très important. Cela te permettre aussi de trouver plus facilement un groupe à intégrer, car plus tu es bon, plus ton groupe sera fort !</div>
		  </div>

		  <div class="aideSousSection">
		  <div class="aideQuestion"><a name="points">Comment sont calculés les points ?</a></div><div class="aideReponse">Le calcul des points se fait tous les jours. Les points sont calculés en fonction de beaucoup des paramètres du jeu. 
		  	<ul>
					<li> Le rôle de l'éleveur est très important dans le calcul des points. L'état général du hamster est pris en compte. Un hamster en bonne santé et avec un bon moral aura plus de points qu'un hamster mal soigné. La cage du hamster influe aussi, plus ta cage comporte d'étages, d'objets et plus elle est propre régulièrement, plus ton hamster gagne des points. </li>
					<li>Dans l'Academy, toute l'expérience acquise permet aussi de gagner un maximum de points. Un hamster confirmé gagne plus de points qu'un hamster débutant. </li>
					<!-- <li> Les concours te permettent aussi de gagner des points. A chaque concours organisé par Hamster Academy, des points bonus sont attribués automatiquement aux gagnants. </li> 
					-->
				</ul>
		  	</div>
		  </div>
		  
		  <div class="aideSousSection">
		  <div class="aideQuestion">Qu'est-ce qu'un groupe ? </div><div class="aideReponse">Une fois que tu as inscris un hamster dans l'Academy, tu as la possibilité de lui associer un instrument de musique si tu en as acheté un En Ville dans La Boutique. Cet instrument que tu choisis définitivement permet au hamster de se spécialiser et ca lui donne la possibilité de prendre des cours de cet instrument, et aussi d'intégrer ou de créer un groupe de musique. Plusieurs hamsters peuvent se réunir au sein d'un groupe pour unifier leurs forces. Le fait d'appartenir à un groupe rapporte des points au classement général. Il ne peut y avoir que 8 membres au maximum dans un même groupe.  Les places dans les meilleurs groupes sont donc précieuses !</div>
		  </div>		  

		  <div class="aideSousSection">
		  <div class="aideQuestion">Comment sont calculés les points de chaque groupe ? </div><div class="aideReponse">Le nombre de points d'un groupe est calculé en fonction des points de chaque membre. Plus les hamsters sont bons, plus le groupe a des points.<br/>Pour gagner quelques points supplémentaires, il existe quelques extras appelés « Combo ». Un Combo est réalisé lorsque qu'un groupe correspond à un type de groupe prédéfini. Par exemple, un groupe constitué d'1 batterie, de 2 guitares, d'1 contrebasse et d'1 chanteur correspond à un Combo « groupe de Rock » ce qui rapporte 50% de points en plus ! A vous de découvrir les autres ! Il est donc intéressant d'accepter dans son groupe des Académiciens qui ont beaucoup de points et dont l'instrument peut rapporter un bonus Combo, </div>
		  </div>		  

		  <div class="aideSousSection">
		  <div class="aideQuestion">Combien de membres peut contenir un groupe ? </div><div class="aideReponse">Il peut y avoir jusqu'à 8 membres par groupe.</div>
		  </div>
		  
		  <div class="aideSousSection">
			  <div class="aideQuestion"><a name="repetitions">A quoi servent les répétitions ?</div>
			  <div class="aideReponse">
			  	Dès qu'un groupe contient au moins 3 musiciens, ils peuvent organiser des concerts. Pour s'y préparer, il faut que le groupe fasse des répétitions. Le groupe acquiert alors de l'expérience. Dès 20 points d'expérience, des concerts peuvent être organisés.
			  	Les règles concernant les répétitions sont :<ul>
				  	<li>il faut au moins 3 inscrits pour faire une répétition</li>
						<li>plus il y a d'inscrits, plus le groupe aura d'expérience : il vaut donc mieux attendre qu'il y ait le plus de joueurs inscrits pour faire la répétition</li>
						<li>seuls les hamsters inscrits à la répétition verront aussi leurs points d'expérience augmenter</li>
						<li>une répétition apporte 1 point d'expérience à chaque musicien qui y a participé, ainsi que 1 point en musique.</li>
					</ul>
			  </div>
		  </div>			  

		  <div class="aideSousSection">
			  <div class="aideQuestion"><a name="concerts">Les concerts, comment ça marche ?</div>
			  <div class="aideReponse">
			  	Dès qu'un groupe contient au moins 3 musiciens, ils peuvent organiser des concerts. Pour s'y préparer, il faut que le groupe fasse des répétitions. Le groupe acquiert alors de l'expérience. Dès 20 points d'expérience, des concerts peuvent être organisés.
			  	Les règles concernant les concerts sont :<ul>
						<li>il faut au moins 3 inscrits pour faire un concert</li>
						<li>plus il y a d'inscrits, plus le concert sera bon, plus le groupe aura d'expérience : il vaut donc mieux attendre qu'il y ait le plus de joueurs inscrits pour faire le concert</li>
						<li>un concert rapporte des pièces aux musiciens qui y ont participé : selon le niveau d'expérience du groupe, à partir de 10 pièces par musicien.</li>
						<li>seuls les hamsters inscrits au concert verront aussi leurs points d'expérience augmenter et les pièces gagnées lors du concert</li>
						<li>plus un groupe a de l'expérience, plus un concert est cher à organiser. En contrepartie, il rapporte plus de pièces (toujours plus que l'inscription elle-même)</li>
						<li>on ne peut faire qu'un concert par jour</li>
						</ul>
			  </div>
		  </div>	
</div>

<?php
}
else if ($univers == 2) {
?>

<div class="aideSection">
		<div class="aideTitreSection">Questions générales</div>
		
		 <div class="aideSousSection">
		  <div class="aideQuestion">Le jeu est-il gratuit ?</div><div class="aideReponse">Oui, il est possible de jouer à Hamster Academy et de ne pas payer. Pour les joueurs qui veulent gagner des pièces plus rapidement, il existe la possibilité d'acheter des pièces (300) en allant En Ville à la Banque via Allopass (voir la <a href="#plusdargent">question "Pas assez de pièces en poche ?"</a>).</div>
		  </div>

		 <div class="aideSousSection">
		  <div class="aideQuestion">Il y a un navigateur recommandé pour jouer à Hamster Academy?</div><div class="aideReponse">Ce jeu a été conçu pour fonctionner sur la plupart des navigateurs classiques. La petite préférence d'Hamster Academy est Firefox (logiciel gratuit téléchargeable à l'adresse <a href="http://www.mozilla-europe.org/fr/" target=_blank>http://www.mozilla-europe.org/fr/</a>) mais le jeu fonctionne très bien avec Internet Explorer (version 6 et supérieur) et Safari (et Opéra a priori). Si tu as des problèmes d'affichage ou que tu n'arrives pas à jouer, le navigateur peut être à l'origine du problème et il est alors conseillé d'essayer le jeu avec un autre navigateur.</div>
		  </div>
		  
		 <div class="aideSousSection">
		  <div class="aideQuestion">Comment changer son mot de passe ou son adresse mail ?</div><div class="aideReponse">Si tu veux changer de mot de passe, il faut aller dans  l'onglet Options situé en haut à droite de l'écran dans l'univers Eleveur. Le mot de passe doit contenir au minimum 6 caractères. L'adresse mail n'est pas obligatoire mais il est conseillé d'en ajouter une à son compte. Cela permet de recevoir des cadeaux (pour la cage et l'hamster) et de pouvoir récupérer son mot de passe si on l'a oublié. Pour ajouter ou modifier une adresse mail, il faut aller dans le menu des Options.</div>
		  </div>

		 <div class="aideSousSection">
		  <div class="aideQuestion"><a name="parrainage">
		  	Qu'est ce que le parrainage, et comment parrainer un ami ?</a></div>
		  	<div class="aideReponse"> Le principe du parrainage est d'inviter vos amis à jouer au jeu Hamster Academy en indiquant leur adresse mail. Chaque joueur invité de ta part qui s'inscrit devient ton "filleul". Il reçoit alors un mail avec un lien vers www.hamsteracademy.fr, et s'il s'inscrit via ce lien, tu recevras 20 pièces par nouveau joueur.
		  		Il est interdit de se parrainer soi-même : dans ce cas, le compte est suspendu et les pièces ne seront pas versées. Le parrainage est accessible en cliquant sur le lien <a href="parrainage.php">parrainage</a>. (Nombre de parrainages par joueur limité à 5). A noter que l'adresse email doit être <u>activée</u> (voir l'aide correspondante)</div>
		  </div>

		 <div class="aideSousSection">
		  <div class="aideQuestion">Est-il possible de supprimer son compte ?</div><div class="aideReponse">Pour supprimer un compte, il faut aller dans l'onglet Options puis ensuite dans la partie « Mon Compte », et cocher la case « supprimer mon compte ». Mais attention, c'est une opération irréversible. Pour une aide particulière, n'hésite pas à contacter l'Hamster Academy (contact@hamsteracademy.fr), elle est là pour ça !</div>
		  </div>

		 <div class="aideSousSection">
		 <div class="aideQuestion">Peut-on me supprimer mon compte ?</div><div class="aideReponse">Si le compte est inactif depuis plus de 3 mois, il est automatiquement supprimé. Seuls les joueurs qui ont une adresse email validée auront un mail une semaine avant pour les prévenir.</div>
		 </div>
		 
		 <div class="aideSousSection">
		  <div class="aideQuestion">J'ai oublié mon mot de passe, comment je fais pour le récupérer ?</div>
		  <div class="aideReponse">Si tu as donné une adresse email correcte à ton inscription, tu peux le récupérer automatiquement en cliquant sur l'adresse suivante => <a href="motdepasseperdu.php">mot de passe perdu</a> <= . Si ça ne marche, ou si tu n'as pas d'adresse email valide, il faut contacter l'Hamster Academy en expliquant le problème et en donnant le pseudo. Nous ferons alors des vérifications puis nous donnerons le mot de passe.</div>
		  </div>		  

		 <div class="aideSousSection">
		 <div class="aideQuestion">Je n'arrive pas à me connecter et je suis certain de mon mot de passe !</div><div class="aideReponse">Il faut essayer toutes les solutions ci-dessous
		 	<ul>
		 		<li>changer de navigateur en utilisant firefox (ou Opera) par exemple au lieu d'internet explorer (ou vice-versa)</li>
		 		<li>essayer sur un autre ordinateur</li>
		 		<li>redémarrer le navigateur / l'ordinateur</li>
		 		<li>vérifier que les cookies sont bien acceptés par le navigateur</li>
		 	</ul>
		 	</div>
		 </div>		 
		  
		 <div class="aideSousSection">
		 <div class="aideQuestion">Je n'arrive pas à me connecter au tchat</div><div class="aideReponse">Il faut essayer toutes les solutions ci-dessous
		 	<ul>
		 		<li>changer de navigateur en utilisant firefox (ou Opera) par exemple au lieu d'internet explorer (ou vice-versa)</li>
		 		<li>essayer sur un autre ordinateur</li>
		 		<li>redémarrer le navigateur / l'ordinateur</li>
		 		<li>vérifier que Flash est bien installé sur l'ordinateur</li>
		 		<li>vérifier qu'il n'y a pas de contrôle parental activé. Si c'est le cas, il suffit de régler le logiciel de contrôle parental</li>
		 		<li>mon pseudo possède des caractères bizarres : il faut contacter Hamster Academy</li>
		 		<li>peut-être ai-je été banni par un modérateur ?</li>
		 	</ul>
		 	A noter que je suis obligé de choisir le même pseudo que dans le jeu. Et que je ne peux pas être "invité".
		 	</div>
		 </div>				  
		  
		 <div class="aideSousSection">
		  <div class="aideQuestion">Mon code allopass ne marche pas ou je n'ai pas été crédité des x pièces promises ?</div>
		  <div class="aideReponse">Ce sont des choses qui arrivent (très rarement). Donc pas de panique, il ne faut pas hésiter à contacter l'Hamster Academy pour le signaler. Nous corrigerons rapidement le problème (après vérification que l'appel téléphonique ou l'envoi du sms ait bien été effectué).</div>
		  </div>		  

		 <div class="aideSousSection">
		  <div class="aideQuestion">Qu'est-ce que l'activation de mon adresse email ?</div>
		  <div class="aideReponse">Il s'agit pour nous de vérifier que ton adresse email est correcte. Pour cela, tu dois l'activer. L'activation te permettra alors de participer aux concours, de recevoir des messages des autres joueurs par mail, de recevoir des cadeaux, de parrainer, etc.<br>L'activation s'effectue dans le menu <a href="options.php">"Options"</a>. Après avoir cliqué sur le bouton "Activer maintenant", tu recevras un email à ton adresse mail qui contient un lien sur lequel tu devras cliquer.<br>Si ça ne marche, contacte l'Hamster Academy.</div>
		 </div>
		
		<div class="aideSousSection">
		  <div class="aideQuestion">J'ai des commentaires sur le jeu ou des questions, comment contacter l'Hamster Academy? </div><div class="aideReponse">Tu peux nous contacter en envoyant un mail à <a href="mailto:contact@hamsteracademy.fr">contact@hamsteracademy.fr</a>. Dans les 48h suivant le mail, tu recevras une réponse à ta question ou une aide si tu rencontres un problème avec le jeu. 
Si tu as une idée innovante à nous proposer, et que tu veux qu'elle soit intégrer au jeu, une boîte à idée est disponible sur la page d'inscription du jeu. Tu peux y accéder en cliquant sur l'onglet Quitter, en haut à droite de l'écran. Toutes les remarques et idées sont examinées. Uniquement pour les très bonnes idées, Hamster Academy offre deux petits cadeaux. </div>
		  </div>
</div>

<?php 
}
?>

<div style="clear:both;">&nbsp;</div>